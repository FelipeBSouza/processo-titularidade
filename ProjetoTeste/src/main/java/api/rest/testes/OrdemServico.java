package api.rest.testes;

public class OrdemServico {

	private String numeroOS;

	public String getNumeroOS() {
		return numeroOS;
	}

	public void setNumeroOS(String numeroOS) {
		this.numeroOS = numeroOS;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroOS == null) ? 0 : numeroOS.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdemServico other = (OrdemServico) obj;
		if (numeroOS == null) {
			if (other.numeroOS != null)
				return false;
		} else if (!numeroOS.equals(other.numeroOS))
			return false;
		return true;
	}
	
	
}
