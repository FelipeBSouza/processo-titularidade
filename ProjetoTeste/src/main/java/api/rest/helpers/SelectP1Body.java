package api.rest.helpers;

public class SelectP1Body {
    private String reference;

    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
