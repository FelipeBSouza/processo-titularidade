package api.rest.helpers;

public class ListaParametrosEntrada {
  
   // @Pattern(regexp = "(^ALL$)|(^MUDANCA_PONTO$)|(^INS_ROTAC$)|(^COMO_ROTAC$)|(^ALG_ROTMESH$)|(^INS_FTTH$)|"
   //         + "(^TV$)|(^COMO_SWITCH$)|(^INS_PONTO_ADIC$)|(^ALG_SWITCH$)|(^MUDANCA_SETBOX$)|(^COMO_ROTMESH$)"
  //          + "|(^MUDANCA_ONT$)|(^INS_ROTMESH$)|(^BLQ_CHAMADA$)|(^INS_TV$)|(^ASS_FTTH$)|(^SIGAME$)|(^FONE$)"
  //          + "|(^SVA$)|(^IP_FIXO$)|(^TRANS_PONTO$)|(^PACOTE_TV$)|(^TRANS_TV$)|(^INS_SWITCH$)|(^TRANS_END$)"
  //          + "|(^NET$)|(^ALG_ROTAC$)", message = "Tipo de Produto invalido")
	
    private String tp_produto;
    private String idPromocao;   
    private String fidelidade;
    private String cidade;
    private String bairro;	
    private String categoria;
    
	public String getTp_produto() {
		return tp_produto;
	}

	public void setTp_produto(String tp_produto) {
		this.tp_produto = tp_produto;
	}

	public String getIdPromocao() {
		return idPromocao;
	}

	public void setIdPromocao(String idPromocao) {
		this.idPromocao = idPromocao;
	}

	public String getFidelidade() {
		return fidelidade;
	}

	public void setFidelidade(String fidelidade) {
		this.fidelidade = fidelidade;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

       
}
