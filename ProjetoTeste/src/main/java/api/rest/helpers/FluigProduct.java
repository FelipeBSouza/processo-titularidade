package api.rest.helpers;

public class FluigProduct {

    private String reference;
    private String category;
    private String description;
    private String amount;
    private String productType;
    private String unitValue;
    private String fullValue;
    private String monthlyValue;
    private String discount;
    private String exemption;
    private String installments;
    private String key;

    public String getProductType() {
        return this.productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUnitValue() {
        return this.unitValue;
    }

    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    public String getFullValue() {
        return this.fullValue;
    }

    public void setFullValue(String fullValue) {
        this.fullValue = fullValue;
    }

    public String getMonthlyValue() {
        return this.monthlyValue;
    }

    public void setMonthlyValue(String monthlyValue) {
        this.monthlyValue = monthlyValue;
    }

    public String getDiscount() {
        return this.discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getExemption() {
        return this.exemption;
    }

    public void setExemption(String exemption) {
        this.exemption = exemption;
    }

    public String getInstallments() {
        return this.installments;
    }

    public void setInstallments(String installments) {
        this.installments = installments;
    }

}
