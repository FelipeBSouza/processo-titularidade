/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.auditoria;


import api.rest.model.integrador.Cliente;
import api.rest.model.seguranca.Usuario;
import api.rest.model.telefonia.Numero;
import api.rest.model.telefonia.Planos;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "auditoria", name = "auditoria_telefone")
public class AuditaTelefone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @ManyToOne
    @JoinColumn(name = "cd_usuario")
    private Usuario usuario;
    @ManyToOne
    @JoinColumn(name = "cd_cliente")
    private Cliente cliente;
    @ManyToOne
    @JoinColumn(name = "numero", referencedColumnName = "numero")
    private Numero numero;
    @ManyToOne
    @JoinColumn(name = "cd_plano_antigo")
    private Planos planoAntigo;
    @Column(name = "vl_franquia_antiga")
    private Double vlFranquiaAntiga;
    @Column(name = "vl_franquia_antigaldn")
    private Double vlFranquiaAntigaLdn;
    @Column(name = "vl_franquia_antigamovel")
    private Double vlFranquiaAntigaMovel;
    @Column(name = "vl_limite_antigo")
    private Double vlLimiteAntigo;
    @ManyToOne
    @JoinColumn(name = "cd_plano_novo")
    private Planos planoNovo;
    @Column(name = "vl_franquia_nova")
    private Double vlFranquiaNova;
    @Column(name = "vl_franquia_novaldn")
    private Double vlFranquiaNovaLdn;
    @Column(name = "vl_franquia_novamovel")
    private Double vlFranquiaNovaMovel;
    @Column(name = "vl_limite_novo")
    private Double vlLimiteNovo;
    @Column(name = "acao", length = 1)
    private String acao = "A";
    @Column(name = "data_atualizacao")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAtualizacao = new Date();

    public AuditaTelefone() {
    }

    public AuditaTelefone(Usuario usuario, Cliente cliente, Numero numero) {
        this.usuario = usuario;
        this.cliente = cliente;
        this.numero = numero;
    }

    public AuditaTelefone(Usuario usuario, Cliente cliente, Numero numero, Planos planoAntigo) {
        this.usuario = usuario;
        this.cliente = cliente;
        this.numero = numero;
        this.planoAntigo = planoAntigo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Numero getNumero() {
        return numero;
    }

    public void setNumero(Numero numero) {
        this.numero = numero;
    }

    public Planos getPlanoAntigo() {
        return planoAntigo;
    }

    public void setPlanoAntigo(Planos planoAntigo) {
        this.planoAntigo = planoAntigo;
    }

    public Planos getPlanoNovo() {
        return planoNovo;
    }

    public void setPlanoNovo(Planos planoNovo) {
        this.planoNovo = planoNovo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Double getVlFranquiaAntiga() {
        return vlFranquiaAntiga;
    }

    public void setVlFranquiaAntiga(Double vlFranquiaAntiga) {
        this.vlFranquiaAntiga = vlFranquiaAntiga;
    }

    public Double getVlFranquiaAntigaLdn() {
        return vlFranquiaAntigaLdn;
    }

    public void setVlFranquiaAntigaLdn(Double vlFranquiaAntigaLdn) {
        this.vlFranquiaAntigaLdn = vlFranquiaAntigaLdn;
    }

    public Double getVlFranquiaAntigaMovel() {
        return vlFranquiaAntigaMovel;
    }

    public void setVlFranquiaAntigaMovel(Double vlFranquiaAntigaMovel) {
        this.vlFranquiaAntigaMovel = vlFranquiaAntigaMovel;
    }

    public Double getVlFranquiaNova() {
        return vlFranquiaNova;
    }

    public void setVlFranquiaNova(Double vlFranquiaNova) {
        this.vlFranquiaNova = vlFranquiaNova;
    }

    public Double getVlFranquiaNovaLdn() {
        return vlFranquiaNovaLdn;
    }

    public void setVlFranquiaNovaLdn(Double vlFranquiaNovaLdn) {
        this.vlFranquiaNovaLdn = vlFranquiaNovaLdn;
    }

    public Double getVlFranquiaNovaMovel() {
        return vlFranquiaNovaMovel;
    }

    public void setVlFranquiaNovaMovel(Double vlFranquiaNovaMovel) {
        this.vlFranquiaNovaMovel = vlFranquiaNovaMovel;
    }

    public Double getVlLimiteAntigo() {
        return vlLimiteAntigo;
    }

    public void setVlLimiteAntigo(Double vlLimiteAntigo) {
        this.vlLimiteAntigo = vlLimiteAntigo;
    }

    public Double getVlLimiteNovo() {
        return vlLimiteNovo;
    }

    public void setVlLimiteNovo(Double vlLimiteNovo) {
        this.vlLimiteNovo = vlLimiteNovo;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getAcaoFormat() {
        switch (acao) {
            case "I":
                return "Inclusão";
            case "A":
                return "Alteração";
            default:
                throw new IllegalArgumentException("Unknown" + acao);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuditaTelefone other = (AuditaTelefone) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.id;
        return hash;
    }
}
