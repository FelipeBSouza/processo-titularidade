/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.fibra;

import api.rest.model.integrador.equipamento.EquipamentoYate;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "porta_splitter")
public class PortaSplitter implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_porta", nullable = false)
    private int cd_porta;
    @Column(name = "ds_porta")
    private String ds_porta;
    @Column(name = "nr_porta")
    private Integer nr_porta;
    //
    @ManyToOne
    @JoinColumn(name = "cd_splitter")
    private Splitter splitter;
    @JsonIgnore
    @JoinColumn(name = "cd_cor_tubo")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cor corTubo;
    @JsonIgnore
    @JoinColumn(name = "cd_cor_fibra")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cor corFibra;
    @OneToOne(mappedBy = "portaSplitter", fetch = FetchType.LAZY)
    private EquipamentoYate equipamento;

    public PortaSplitter() {
    }

    public PortaSplitter(Splitter splitter) {
        this.splitter = splitter;
    }

    public int getCd_porta() {
        return cd_porta;
    }

    public void setCd_porta(int cd_porta) {
        this.cd_porta = cd_porta;
    }

    public Cor getCorFibra() {
        return corFibra;
    }

    public void setCorFibra(Cor corFibra) {
        this.corFibra = corFibra;
    }

    public Cor getCorTubo() {
        return corTubo;
    }

    public void setCorTubo(Cor corTubo) {
        this.corTubo = corTubo;
    }

    public String getDs_porta() {
        return ds_porta;
    }

    public void setDs_porta(String ds_porta) {
        this.ds_porta = ds_porta;
    }

    public EquipamentoYate getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(EquipamentoYate equipamento) {
        this.equipamento = equipamento;
    }

    public Splitter getSplitter() {
        return splitter;
    }

    public void setSplitter(Splitter splitter) {
        this.splitter = splitter;
    }

    public Integer getNr_porta() {
        return nr_porta;
    }

    public void setNr_porta(Integer nr_porta) {
        this.nr_porta = nr_porta;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PortaSplitter other = (PortaSplitter) obj;
        if (this.cd_porta != other.cd_porta) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.cd_porta;
        return hash;
    }

    @Override
    public String toString() {
        return "P:" + nr_porta + "; T:" + corTubo + "; F:" + corFibra;
    }
}
