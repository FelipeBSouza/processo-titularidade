/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.equipamento;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "CAD_MARCA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class Marca implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_marca", nullable = false)
    private int cd_marca;
    @Column(name = "ds_marca")
    private String ds_marca;
    /*
     *
     */
    @JsonIgnore
    @OneToMany(mappedBy = "marca")
    private List<Modelo> modelos;

    public Marca() {
    }

    public int getCd_marca() {
        return cd_marca;
    }

    public void setCd_marca(int cd_marca) {
        this.cd_marca = cd_marca;
    }

    public String getDs_marca() {
        return ds_marca;
    }

    public void setDs_marca(String ds_marca) {
        this.ds_marca = ds_marca;
    }

    public List<Modelo> getModelos() {
        return modelos;
    }

    public void setModelos(List<Modelo> modelos) {
        this.modelos = modelos;
    }

    @Override
    public String toString() {
        return ds_marca;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Marca other = (Marca) obj;
        if (this.cd_marca != other.cd_marca) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.cd_marca;
        return hash;
    }
}
