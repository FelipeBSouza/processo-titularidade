package api.rest.model.integrador;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "CONTAS_RECEBER")
public class ContasReceber implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CD_CONTAS", nullable = false)
    private int CD_CONTAS;
    @Column(name = "VL_RECEBER")
    private double VL_RECEBER;
    @Column(name = "DT_RECEBER")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date DT_RECEBER;
    @Column(name = "DT_PAGAMENTO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date DT_PAGAMENTO;
    @Column(name = "TP_CONTAS")
    private char TP_CONTAS; // TRANSFERENCIA OU BOLETO
    @Column(name = "IN_SITUACAO")
    private char IN_SITUACAO; // PAGO OU ABERTO   
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "CD_BOLETO")
    private BoletoClass boleto;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Cliente cliente;
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Fatura_cliente fatura_cliente;

    public ContasReceber() {
    }

    public int getCD_CONTAS() {
        return CD_CONTAS;
    }

    public void setCD_CONTAS(int CD_CONTAS) {
        this.CD_CONTAS = CD_CONTAS;
    }

    public Date getDT_PAGAMENTO() {
        return DT_PAGAMENTO;
    }

    public void setDT_PAGAMENTO(Date DT_PAGAMENTO) {
        this.DT_PAGAMENTO = DT_PAGAMENTO;
    }

    public Date getDT_RECEBER() {
        return DT_RECEBER;
    }

    public void setDT_RECEBER(Date DT_RECEBER) {
        this.DT_RECEBER = DT_RECEBER;
    }

    public char getIN_SITUACAO() {
        return IN_SITUACAO;
    }

    public void setIN_SITUACAO(char IN_SITUACAO) {
        this.IN_SITUACAO = IN_SITUACAO;
    }

    public char getTP_CONTAS() {
        return TP_CONTAS;
    }

    public void setTP_CONTAS(char TP_CONTAS) {
        this.TP_CONTAS = TP_CONTAS;
    }

    public double getVL_RECEBER() {
        return VL_RECEBER;
    }

    public void setVL_RECEBER(double VL_RECEBER) {
        this.VL_RECEBER = VL_RECEBER;
    }

    public BoletoClass getBoleto() {
        return boleto;
    }

    public void setBoleto(BoletoClass boleto) {
        this.boleto = boleto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Fatura_cliente getFatura_cliente() {
        return fatura_cliente;
    }

    public void setFatura_cliente(Fatura_cliente fatura_cliente) {
        this.fatura_cliente = fatura_cliente;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContasReceber other = (ContasReceber) obj;
        if (this.CD_CONTAS != other.CD_CONTAS) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.CD_CONTAS;
        return hash;
    }
}
