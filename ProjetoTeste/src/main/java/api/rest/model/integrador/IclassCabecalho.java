package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "integrador", name = "iclass_cabecalho")
public class IclassCabecalho implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_cabecalho;
	
	private String numos;
	private String codcli;
	private String loja;
	private String atendente;
	private String cusuario;
	private String porta;
	private String caixa;
	private String nr_solicitacao;
	private String fluxo;
	private String cod_encerramento;
	private String status;	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getId_cabecalho() {
		return id_cabecalho;
	}
	public void setId_cabecalho(Integer id_cabecalho) {
		this.id_cabecalho = id_cabecalho;
	}
	public String getNumos() {
		return numos;
	}
	public void setNumos(String numos) {
		this.numos = numos;
	}
	public String getCodcli() {
		return codcli;
	}
	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}
	public String getLoja() {
		return loja;
	}
	public void setLoja(String loja) {
		this.loja = loja;
	}
	public String getAtendente() {
		return atendente;
	}
	public void setAtendente(String atendente) {
		this.atendente = atendente;
	}
	public String getCusuario() {
		return cusuario;
	}
	public void setCusuario(String cusuario) {
		this.cusuario = cusuario;
	}
	public String getPorta() {
		return porta;
	}
	public void setPorta(String porta) {
		this.porta = porta;
	}
	public String getCaixa() {
		return caixa;
	}
	public void setCaixa(String caixa) {
		this.caixa = caixa;
	}
	public String getNr_solicitacao() {
		return nr_solicitacao;
	}
	public void setNr_solicitacao(String nr_solicitacao) {
		this.nr_solicitacao = nr_solicitacao;
	}
	public String getFluxo() {
		return fluxo;
	}
	public void setFluxo(String fluxo) {
		this.fluxo = fluxo;
	}
	public String getCod_encerramento() {
		return cod_encerramento;
	}
	public void setCod_encerramento(String cod_encerramento) {
		this.cod_encerramento = cod_encerramento;
	}	
	
}
