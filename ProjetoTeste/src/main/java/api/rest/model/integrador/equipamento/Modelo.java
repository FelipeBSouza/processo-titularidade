/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.equipamento;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "CAD_MODELO")
public class Modelo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_modelo", nullable = false)
    private int cd_modelo;
    @Column(name = "ds_modelo") 
    private String ds_modelo;
    @Column(name = "qt_porta_telefone")
    private int qt_porta_telefone;
    @Column(name = "qt_porta_internet")
    private int qt_porta_internet;
    @Column(name = "ds_formatado")
    private String ds_formatado;
    @Column(name = "wifi")
    private boolean wifi;
    /*
     *
     */
   
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_marca")
    private Marca marca;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "cd_tipo_equipamento")
//    private TipoEquipamento tipoEquipamento;
//    @OneToMany(mappedBy = "modelo", fetch = FetchType.LAZY)
//    private List<EquipamentoYate> equipamentos;

    /*@OneToMany(mappedBy = "modelo")
     private List<Caixa> caixas;*/
    public Modelo() {
    }

    public int getCd_modelo() {
        return cd_modelo;
    }

    public void setCd_modelo(int cd_modelo) {
        this.cd_modelo = cd_modelo;
    }

    public String getDs_modelo() {
        return ds_modelo;
    }

    public void setDs_modelo(String ds_modelo) {
        this.ds_modelo = ds_modelo;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public int getQt_porta_internet() {
        return qt_porta_internet;
    }

    public void setQt_porta_internet(int qt_porta_internet) {
        this.qt_porta_internet = qt_porta_internet;
    }

    public int getQt_porta_telefone() {
        return qt_porta_telefone;
    }

    public void setQt_porta_telefone(int qt_porta_telefone) {
        this.qt_porta_telefone = qt_porta_telefone;
    }

    public String getDs_formatado() {
        return ds_formatado;
    }

    public void setDs_formatado(String ds_formatado) {
        this.ds_formatado = ds_formatado;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

//    public TipoEquipamento getTipoEquipamento() {
//        return tipoEquipamento;
//    }
//
//    public void setTipoEquipamento(TipoEquipamento tipoEquipamento) {
//        this.tipoEquipamento = tipoEquipamento;
//    }
//
//    public List<EquipamentoYate> getEquipamentos() {
//        return equipamentos;
//    }
//
//    public void setEquipamentos(List<EquipamentoYate> equipamentos) {
//        this.equipamentos = equipamentos;
//    }
    @Override
    public String toString() {
        return ds_modelo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Modelo other = (Modelo) obj;
        if (this.cd_modelo != other.cd_modelo) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + this.cd_modelo;
        return hash;
    }
}
