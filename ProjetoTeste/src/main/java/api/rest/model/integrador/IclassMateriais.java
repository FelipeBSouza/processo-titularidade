package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "integrador", name = "iclass_materiais")
public class IclassMateriais implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_materiais;
	
	private String numos;
	private String cdpro;
	private String descpro;
	private String qtde;
	private String vlrunit;
	private String vlrtotal;
	private String cdservico;
	private String  armazem;
	
	
	public Integer getId_materiais() {
		return id_materiais;
	}
	public void setId_materiais(Integer id_materiais) {
		this.id_materiais = id_materiais;
	}
	public String getNumos() {
		return numos;
	}
	public void setNumos(String numos) {
		this.numos = numos;
	}
	public String getCdpro() {
		return cdpro;
	}
	public void setCdpro(String cdpro) {
		this.cdpro = cdpro;
	}
	public String getDescpro() {
		return descpro;
	}
	public void setDescpro(String descpro) {
		this.descpro = descpro;
	}
	public String getQtde() {
		return qtde;
	}
	public void setQtde(String qtde) {
		this.qtde = qtde;
	}
	public String getVlrunit() {
		return vlrunit;
	}
	public void setVlrunit(String vlrunit) {
		this.vlrunit = vlrunit;
	}
	public String getVlrtotal() {
		return vlrtotal;
	}
	public void setVlrtotal(String vlrtotal) {
		this.vlrtotal = vlrtotal;
	}
	public String getCdservico() {
		return cdservico;
	}
	public void setCdservico(String cdservico) {
		this.cdservico = cdservico;
	}
	public String getArmazem() {
		return armazem;
	}
	public void setArmazem(String armazem) {
		this.armazem = armazem;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
	
}
