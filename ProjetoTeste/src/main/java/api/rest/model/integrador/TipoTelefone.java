package api.rest.model.integrador;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(schema = "integrador",name = "tipo_telefone")
public class TipoTelefone implements Serializable{
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name="cd_tipo", nullable=false)
    private int cd_tipo;
    
    @Column(name="ds_tipo", length=60)
    private String ds_tipo;
    
    @OneToMany(mappedBy = "cd_tipo_telefone")
    private List<Telefone> listaTelefone;

    public TipoTelefone() {
    }

   
    public int getCd_tipo() {
        return cd_tipo;
    }

    public void setCd_tipo(int cd_tipo) {
        this.cd_tipo = cd_tipo;
    }

    public String getDs_tipo() {
        return ds_tipo;
    }

    public void setDs_tipo(String ds_tipo) {
        this.ds_tipo = ds_tipo;
    }

    public List<Telefone> getListaTelefone() {
        return listaTelefone;
    }

    public void setListaTelefone(List<Telefone> listaTelefone) {
        this.listaTelefone = listaTelefone;
    }

   
    @Override
    public String toString(){
        return this.getDs_tipo();
    }
    
    
}
