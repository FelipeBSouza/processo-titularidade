/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.equipamento;

import api.rest.model.auditoria.AuditaSplitter;
import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.Endereco;
import api.rest.model.integrador.fibra.PortaSplitter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "CAD_EQUIPAMENTO")
public class EquipamentoYate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_equipamento", nullable = false)
    private int cd_equipamento;
    @Column(name = "ip_http")
    private String ip_http;
    @Column(name = "login")
    private String login;
    @Column(name = "senha")
    private String senha;
    @Column(name = "mac")
    private String mac;
    @Column(name = "serial")
    private String serial;
    @Column(name = "posicao_mxk")
    private String posicao_mxk;
    /*
     *
     */
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "cd_tipo_equipamento")
//    private TipoEquipamento tipoEquipamento;
    @Column(name = "cd_tipo_equipamento")
    private int cd_tipo_equipamento = 1;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_endereco")
    private Endereco endereco;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "cd_modelo")
//    private Modelo modelo;
    //
    @Column(name = "cd_modelo")
    private int cd_modelo = 5;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(schema = "integrador", name = "equipamento_cliente",
    joinColumns =
    @JoinColumn(name = "cd_equipamento", referencedColumnName = "cd_equipamento"),
    inverseJoinColumns =
    @JoinColumn(name = "cd_cliente", referencedColumnName = "cd_cliente"))
    private List<Cliente> clientes;
//    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "cd_porta_splitter")
    private PortaSplitter portaSplitter;
    @JsonIgnore
    @OneToMany(mappedBy = "equipamento", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AuditaSplitter> auditaSplitters = new ArrayList<>();

    public EquipamentoYate() {
    }

    public int getCd_equipamento() {
        return cd_equipamento;
    }

    public void setCd_equipamento(int cd_equipamento) {
        this.cd_equipamento = cd_equipamento;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getIp_http() {
        return ip_http;
    }

    public void setIp_http(String ip_http) {
        this.ip_http = ip_http;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getPosicao_mxk() {
        return posicao_mxk;
    }

    public void setPosicao_mxk(String posicao_mxk) {
        this.posicao_mxk = posicao_mxk;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getCd_tipo_equipamento() {
        return cd_tipo_equipamento;
    }

    public void setCd_tipo_equipamento(int cd_tipo_equipamento) {
        this.cd_tipo_equipamento = cd_tipo_equipamento;
    }

    public int getCd_modelo() {
        return cd_modelo;
    }

    public void setCd_modelo(int cd_modelo) {
        this.cd_modelo = cd_modelo;
    }

    public PortaSplitter getPortaSplitter() {
        return portaSplitter;
    }

    public void setPortaSplitter(PortaSplitter portaSplitter) {
        this.portaSplitter = portaSplitter;
    }

    public List<AuditaSplitter> getAuditaSplitters() {
        return auditaSplitters;
    }

    public void setAuditaSplitters(List<AuditaSplitter> auditaSplitters) {
        this.auditaSplitters = auditaSplitters;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EquipamentoYate other = (EquipamentoYate) obj;
        if (this.cd_equipamento != other.cd_equipamento) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.cd_equipamento;
        return hash;
    }
}
