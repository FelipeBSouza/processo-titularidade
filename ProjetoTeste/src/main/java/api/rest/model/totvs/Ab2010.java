/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(name = "AB2010", schema = "dbo")
public class Ab2010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AB2_FILIAL")
    private String ab2Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AB2_ITEM")
    private String ab2Item;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AB2_TIPO")
    private String ab2Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AB2_CLASSI")
    private String ab2Classi;
    @JoinColumns({
        @JoinColumn(name = "AB2_NUMSER", referencedColumnName = "AA3_NUMSER", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "AB2_CODPRO", referencedColumnName = "AA3_CODPRO", nullable = false, insertable = false, updatable = false)})
    @ManyToOne
    private Aa3010 aa3;
    @JoinColumn(name = "AB2_CODPRB", referencedColumnName = "AAG_CODPRB", nullable = false, insertable = false, updatable = false)
    @ManyToOne
    private Aag010 aag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB2_NUMOS")
    private String ab2Numos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB2_NUMORC")
    private String ab2Numorc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AB2_STATUS")
    private String ab2Status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB2_MEMO")
    private String ab2Memo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AB2_CODFAB")
    private String ab2Codfab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AB2_LOJAFA")
    private String ab2Lojafa;
    @ManyToOne
    @JoinColumn(name = "AB2_NRCHAM", referencedColumnName = "AB1_NRCHAM", nullable = false, insertable = false, updatable = false)
    private Ab1010 ab1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AB2_CODCLI")
    private String ab2Codcli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AB2_LOJA")
    private String ab2Loja;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB2_EMISSA")
    private String ab2Emissa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB2_BXDATA")
    private String ab2Bxdata;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "AB2_BXHORA")
    private String ab2Bxhora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB2_NUMHDE")
    private String ab2Numhde;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AB2_CODFNC")
    private String ab2Codfnc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AB2_FNCREV")
    private String ab2Fncrev;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;

    public Ab2010() {
    }

    public Ab2010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAb2Filial() {
        return ab2Filial;
    }

    public void setAb2Filial(String ab2Filial) {
        this.ab2Filial = ab2Filial;
    }

    public String getAb2Item() {
        return ab2Item;
    }

    public void setAb2Item(String ab2Item) {
        this.ab2Item = ab2Item;
    }

    public String getAb2Tipo() {
        return ab2Tipo;
    }

    public void setAb2Tipo(String ab2Tipo) {
        this.ab2Tipo = ab2Tipo;
    }

    public String getAb2Classi() {
        return ab2Classi;
    }

    public void setAb2Classi(String ab2Classi) {
        this.ab2Classi = ab2Classi;
    }

    public Aag010 getAag() {
        return aag;
    }

    public void setAag(Aag010 aag) {
        this.aag = aag;
    }

    public String getAb2Numos() {
        return ab2Numos;
    }

    public void setAb2Numos(String ab2Numos) {
        this.ab2Numos = ab2Numos;
    }

    public String getAb2Numorc() {
        return ab2Numorc;
    }

    public void setAb2Numorc(String ab2Numorc) {
        this.ab2Numorc = ab2Numorc;
    }

    public String getAb2Status() {
        return ab2Status;
    }

    public void setAb2Status(String ab2Status) {
        this.ab2Status = ab2Status;
    }

    public String getAb2Memo() {
        return ab2Memo;
    }

    public void setAb2Memo(String ab2Memo) {
        this.ab2Memo = ab2Memo;
    }

    public String getAb2Codfab() {
        return ab2Codfab;
    }

    public void setAb2Codfab(String ab2Codfab) {
        this.ab2Codfab = ab2Codfab;
    }

    public String getAb2Lojafa() {
        return ab2Lojafa;
    }

    public void setAb2Lojafa(String ab2Lojafa) {
        this.ab2Lojafa = ab2Lojafa;
    }

    public Ab1010 getAb1() {
        return ab1;
    }

    public void setAb1(Ab1010 ab1) {
        this.ab1 = ab1;
    }

    public String getAb2Codcli() {
        return ab2Codcli;
    }

    public void setAb2Codcli(String ab2Codcli) {
        this.ab2Codcli = ab2Codcli;
    }

    public String getAb2Loja() {
        return ab2Loja;
    }

    public void setAb2Loja(String ab2Loja) {
        this.ab2Loja = ab2Loja;
    }

    public String getAb2Emissa() {
        return ab2Emissa;
    }

    public void setAb2Emissa(String ab2Emissa) {
        this.ab2Emissa = ab2Emissa;
    }

    public String getAb2Bxdata() {
        return ab2Bxdata;
    }

    public void setAb2Bxdata(String ab2Bxdata) {
        this.ab2Bxdata = ab2Bxdata;
    }

    public String getAb2Bxhora() {
        return ab2Bxhora;
    }

    public void setAb2Bxhora(String ab2Bxhora) {
        this.ab2Bxhora = ab2Bxhora;
    }

    public String getAb2Numhde() {
        return ab2Numhde;
    }

    public void setAb2Numhde(String ab2Numhde) {
        this.ab2Numhde = ab2Numhde;
    }

    public String getAb2Codfnc() {
        return ab2Codfnc;
    }

    public void setAb2Codfnc(String ab2Codfnc) {
        this.ab2Codfnc = ab2Codfnc;
    }

    public String getAb2Fncrev() {
        return ab2Fncrev;
    }

    public void setAb2Fncrev(String ab2Fncrev) {
        this.ab2Fncrev = ab2Fncrev;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public Aa3010 getAa3() {
        return aa3;
    }

    public void setAa3(Aa3010 aa3) {
        this.aa3 = aa3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ab2010)) {
            return false;
        }
        Ab2010 other = (Ab2010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Ab2010[ rECNO=" + rECNO + " ]";
    }
}
