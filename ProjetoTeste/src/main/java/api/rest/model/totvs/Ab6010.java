/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "AB6010", schema = "dbo")
@XmlRootElement
public class Ab6010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Size(min = 1, max = 4)
    @Column(name = "AB6_FILIAL")
    private String ab6Filial;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB6_NUMOS")
    private String ab6Numos;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "AB6_CODCLI", referencedColumnName = "A1_COD", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "AB6_LOJA", referencedColumnName = "A1_LOJA", nullable = false, insertable = false, updatable = false),})
    private Sa1010 sa1;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB6_EMISSA")
    private String ab6Emissa;
    @Basic(optional = false)
    @Size(min = 1, max = 25)
    @Column(name = "AB6_ATEND")
    private String ab6Atend;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB6_STATUS")
    private String ab6Status;
    @Basic(optional = false)
    @Size(min = 1, max = 3)
    @Column(name = "AB6_CONPAG")
    private String ab6Conpag;
    @Basic(optional = false)
    @Column(name = "AB6_DESC1")
    private double ab6Desc1;
    @Basic(optional = false)
    @Column(name = "AB6_DESC2")
    private double ab6Desc2;
    @Basic(optional = false)
    @Column(name = "AB6_DESC3")
    private double ab6Desc3;
    @Basic(optional = false)
    @Column(name = "AB6_DESC4")
    private double ab6Desc4;
    @Basic(optional = false)
    @Size(min = 1, max = 3)
    @Column(name = "AB6_TABELA")
    private String ab6Tabela;
    @Basic(optional = false)
    @Column(name = "AB6_PARC1")
    private double ab6Parc1;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB6_DATA1")
    private String ab6Data1;
    @Basic(optional = false)
    @Column(name = "AB6_PARC2")
    private double ab6Parc2;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB6_DATA2")
    private String ab6Data2;
    @Basic(optional = false)
    @Column(name = "AB6_PARC3")
    private double ab6Parc3;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB6_DATA3")
    private String ab6Data3;
    @Basic(optional = false)
    @Column(name = "AB6_PARC4")
    private double ab6Parc4;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB6_DATA4")
    private String ab6Data4;
    @Basic(optional = false)
    @Size(min = 1, max = 2)
    @Column(name = "AB6_OK")
    private String ab6Ok;
    @Basic(optional = false)
    @Size(min = 1, max = 5)
    @Column(name = "AB6_HORA")
    private String ab6Hora;
    @Basic(optional = false)
    @Size(min = 1, max = 3)
    @Column(name = "AB6_REGIAO")
    private String ab6Regiao;
    @Basic(optional = false)
    @Size(min = 1, max = 200)
    @Column(name = "AB6_MSG")
    private String ab6Msg;
    @Basic(optional = false)
    @Column(name = "AB6_MOEDA")
    private double ab6Moeda;
    @Basic(optional = false)
    @Column(name = "AB6_TXMOED")
    private double ab6Txmoed;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB6_NUMLOJ")
    private String ab6Numloj;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB6_TPCONT")
    private String ab6Tpcont;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "AB6_CONTRT")
    private String ab6Contrt;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB6_UDTVIG")
    private String ab6Udtvig;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB6_UNUMCT")
    private String ab6Unumct;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB6_USITU1")
    private String ab6Usitu1;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB6_UCODAT")
    private String ab6Ucodat;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB6_USITU2")
    private String ab6Usitu2;
    @Basic(optional = false)
    @Size(min = 1, max = 20)
    @Column(name = "AB6_UREPRO")
    private String ab6Urepro;
    @Basic(optional = false)
    @Size(min = 1, max = 17)
    @Column(name = "AB6_USERGI")
    private String ab6Usergi;
    @Basic(optional = false)
    @Size(min = 1, max = 17)
    @Column(name = "AB6_USERGA")
    private String ab6Userga;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @OneToMany(mappedBy = "ab6", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Ab7010> ab7010s;

    public Ab6010() {
    }

    public Ab6010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAb6Filial() {
        return ab6Filial;
    }

    public void setAb6Filial(String ab6Filial) {
        this.ab6Filial = ab6Filial;
    }

    public String getAb6Numos() {
        return ab6Numos;
    }

    public void setAb6Numos(String ab6Numos) {
        this.ab6Numos = ab6Numos;
    }

    public Sa1010 getSa1() {
        return sa1;
    }

    public void setSa1(Sa1010 sa1) {
        this.sa1 = sa1;
    }

    public String getAb6Emissa() {
        return ab6Emissa;
    }

    public void setAb6Emissa(String ab6Emissa) {
        this.ab6Emissa = ab6Emissa;
    }

    public String getAb6Atend() {
        return ab6Atend;
    }

    public void setAb6Atend(String ab6Atend) {
        this.ab6Atend = ab6Atend;
    }

    public String getAb6Status() {
        return ab6Status;
    }

    public void setAb6Status(String ab6Status) {
        this.ab6Status = ab6Status;
    }

    public String getAb6Conpag() {
        return ab6Conpag;
    }

    public void setAb6Conpag(String ab6Conpag) {
        this.ab6Conpag = ab6Conpag;
    }

    public double getAb6Desc1() {
        return ab6Desc1;
    }

    public void setAb6Desc1(double ab6Desc1) {
        this.ab6Desc1 = ab6Desc1;
    }

    public double getAb6Desc2() {
        return ab6Desc2;
    }

    public void setAb6Desc2(double ab6Desc2) {
        this.ab6Desc2 = ab6Desc2;
    }

    public double getAb6Desc3() {
        return ab6Desc3;
    }

    public void setAb6Desc3(double ab6Desc3) {
        this.ab6Desc3 = ab6Desc3;
    }

    public double getAb6Desc4() {
        return ab6Desc4;
    }

    public void setAb6Desc4(double ab6Desc4) {
        this.ab6Desc4 = ab6Desc4;
    }

    public String getAb6Tabela() {
        return ab6Tabela;
    }

    public void setAb6Tabela(String ab6Tabela) {
        this.ab6Tabela = ab6Tabela;
    }

    public double getAb6Parc1() {
        return ab6Parc1;
    }

    public void setAb6Parc1(double ab6Parc1) {
        this.ab6Parc1 = ab6Parc1;
    }

    public String getAb6Data1() {
        return ab6Data1;
    }

    public void setAb6Data1(String ab6Data1) {
        this.ab6Data1 = ab6Data1;
    }

    public double getAb6Parc2() {
        return ab6Parc2;
    }

    public void setAb6Parc2(double ab6Parc2) {
        this.ab6Parc2 = ab6Parc2;
    }

    public String getAb6Data2() {
        return ab6Data2;
    }

    public void setAb6Data2(String ab6Data2) {
        this.ab6Data2 = ab6Data2;
    }

    public double getAb6Parc3() {
        return ab6Parc3;
    }

    public void setAb6Parc3(double ab6Parc3) {
        this.ab6Parc3 = ab6Parc3;
    }

    public String getAb6Data3() {
        return ab6Data3;
    }

    public void setAb6Data3(String ab6Data3) {
        this.ab6Data3 = ab6Data3;
    }

    public double getAb6Parc4() {
        return ab6Parc4;
    }

    public void setAb6Parc4(double ab6Parc4) {
        this.ab6Parc4 = ab6Parc4;
    }

    public String getAb6Data4() {
        return ab6Data4;
    }

    public void setAb6Data4(String ab6Data4) {
        this.ab6Data4 = ab6Data4;
    }

    public String getAb6Ok() {
        return ab6Ok;
    }

    public void setAb6Ok(String ab6Ok) {
        this.ab6Ok = ab6Ok;
    }

    public String getAb6Hora() {
        return ab6Hora;
    }

    public void setAb6Hora(String ab6Hora) {
        this.ab6Hora = ab6Hora;
    }

    public String getAb6Regiao() {
        return ab6Regiao;
    }

    public void setAb6Regiao(String ab6Regiao) {
        this.ab6Regiao = ab6Regiao;
    }

    public String getAb6Msg() {
        return ab6Msg;
    }

    public void setAb6Msg(String ab6Msg) {
        this.ab6Msg = ab6Msg;
    }

    public double getAb6Moeda() {
        return ab6Moeda;
    }

    public void setAb6Moeda(double ab6Moeda) {
        this.ab6Moeda = ab6Moeda;
    }

    public double getAb6Txmoed() {
        return ab6Txmoed;
    }

    public void setAb6Txmoed(double ab6Txmoed) {
        this.ab6Txmoed = ab6Txmoed;
    }

    public String getAb6Numloj() {
        return ab6Numloj;
    }

    public void setAb6Numloj(String ab6Numloj) {
        this.ab6Numloj = ab6Numloj;
    }

    public String getAb6Tpcont() {
        return ab6Tpcont;
    }

    public void setAb6Tpcont(String ab6Tpcont) {
        this.ab6Tpcont = ab6Tpcont;
    }

    public String getAb6Contrt() {
        return ab6Contrt;
    }

    public void setAb6Contrt(String ab6Contrt) {
        this.ab6Contrt = ab6Contrt;
    }

    public String getAb6Udtvig() {
        return ab6Udtvig;
    }

    public void setAb6Udtvig(String ab6Udtvig) {
        this.ab6Udtvig = ab6Udtvig;
    }

    public String getAb6Unumct() {
        return ab6Unumct;
    }

    public void setAb6Unumct(String ab6Unumct) {
        this.ab6Unumct = ab6Unumct;
    }

    public String getAb6Usitu1() {
        return ab6Usitu1;
    }

    public void setAb6Usitu1(String ab6Usitu1) {
        this.ab6Usitu1 = ab6Usitu1;
    }

    public String getAb6Ucodat() {
        return ab6Ucodat;
    }

    public void setAb6Ucodat(String ab6Ucodat) {
        this.ab6Ucodat = ab6Ucodat;
    }

    public String getAb6Usitu2() {
        return ab6Usitu2;
    }

    public void setAb6Usitu2(String ab6Usitu2) {
        this.ab6Usitu2 = ab6Usitu2;
    }

    public String getAb6Urepro() {
        return ab6Urepro;
    }

    public void setAb6Urepro(String ab6Urepro) {
        this.ab6Urepro = ab6Urepro;
    }

    public String getAb6Usergi() {
        return ab6Usergi;
    }

    public void setAb6Usergi(String ab6Usergi) {
        this.ab6Usergi = ab6Usergi;
    }

    public String getAb6Userga() {
        return ab6Userga;
    }

    public void setAb6Userga(String ab6Userga) {
        this.ab6Userga = ab6Userga;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getdELET() {
        return dELET;
    }

    public void setdELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getrECNO() {
        return rECNO;
    }

    public void setrECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getrECDEL() {
        return rECDEL;
    }

    public void setrECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public List<Ab7010> getAb7010s() {
        return ab7010s;
    }

    public void setAb7010s(List<Ab7010> ab7010s) {
        this.ab7010s = ab7010s;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ab6010)) {
            return false;
        }
        Ab6010 other = (Ab6010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Ab6010[ rECNO=" + rECNO + " ]";
    }
}
