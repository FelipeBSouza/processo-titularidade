/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "dbo", name = "ADA010")
public class Ada010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "ADA_FILIAL")
    private String adaFilial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_NUMCTR")
    private String adaNumctr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ADA_EMISSA")
    private String adaEmissa;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "ADA_CODCLI", referencedColumnName = "A1_COD", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "ADA_LOJCLI", referencedColumnName = "A1_LOJA", nullable = false, insertable = false, updatable = false),})
    @JsonBackReference(value = "Cliente")
    private Sa1010 sa1010;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ADA_CONDPG")
    private String adaCondpg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ADA_TABELA")
    private String adaTabela;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_DESC1")
    private double adaDesc1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_DESC2")
    private double adaDesc2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_DESC3")
    private double adaDesc3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_DESC4")
    private double adaDesc4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_VEND1")
    private String adaVend1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_VEND2")
    private String adaVend2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_VEND3")
    private String adaVend3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_VEND4")
    private String adaVend4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_VEND5")
    private String adaVend5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_COMIS1")
    private double adaComis1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_COMIS2")
    private double adaComis2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_COMIS3")
    private double adaComis3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_COMIS4")
    private double adaComis4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_COMIS5")
    private double adaComis5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_MOEDA")
    private double adaMoeda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ADA_TIPLIB")
    private String adaTiplib;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ADA_STATUS")
    private String adaStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_SAFRA")
    private String adaSafra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "R_E_C_D_E_L_")
//    private int rECDEL;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ADA_MSBLQL")
    private String adaMsblql;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ADA_UNRCOB")
    private String adaUnrcob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADA_UDIAFE")
    private double adaUdiafe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ADA_UDTFEC")
    private String adaUdtfec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ADA_UCTANT")
    private String adaUctant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ADA_ULIBER")
    private String adaUliber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ADA_UNFIMP")
    private String adaUnfimp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ADA_UDTBLQ")
    private String adaUdtblq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ADA_UTIPO")
    private String adaUtipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ADA_UDTLIB")
    private String adaUdtlib;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADA_UCALLC")
    private String adaUcallc;
    @Size(min = 1, max = 6)
    @Column(name = "ADA_UIDYAT")
    private String adaUYate;
    @Column(name = "ADA_UVALI")
    private int adaUVali;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ADA_UIDAGA", referencedColumnName = "AGA_CODIGO", nullable = false, insertable = false, updatable = false)
    private Aga010 aga010;
    @OneToMany(mappedBy = "Ada010", cascade = CascadeType.ALL)
    private List<Adb010> adb010s;
    @OneToMany(mappedBy = "ada010", fetch = FetchType.LAZY)
    private List<Agb010> agb010s;

    public Ada010() {
    }

    public Ada010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAdaEmissa() {
        return adaEmissa;
    }

    public void setAdaEmissa(String adaEmissa) {
        this.adaEmissa = adaEmissa;
    }

    public Sa1010 getSa1010() {
        return sa1010;
    }

    public void setSa1010(Sa1010 sa1010) {
        this.sa1010 = sa1010;
    }

    public String getAdaCondpg() {
        return adaCondpg;
    }

    public void setAdaCondpg(String adaCondpg) {
        this.adaCondpg = adaCondpg;
    }

    public String getAdaTabela() {
        return adaTabela;
    }

    public void setAdaTabela(String adaTabela) {
        this.adaTabela = adaTabela;
    }

    public double getAdaDesc1() {
        return adaDesc1;
    }

    public void setAdaDesc1(double adaDesc1) {
        this.adaDesc1 = adaDesc1;
    }

    public double getAdaDesc2() {
        return adaDesc2;
    }

    public void setAdaDesc2(double adaDesc2) {
        this.adaDesc2 = adaDesc2;
    }

    public double getAdaDesc3() {
        return adaDesc3;
    }

    public void setAdaDesc3(double adaDesc3) {
        this.adaDesc3 = adaDesc3;
    }

    public double getAdaDesc4() {
        return adaDesc4;
    }

    public void setAdaDesc4(double adaDesc4) {
        this.adaDesc4 = adaDesc4;
    }

    public String getAdaVend1() {
        return adaVend1;
    }

    public void setAdaVend1(String adaVend1) {
        this.adaVend1 = adaVend1;
    }

    public String getAdaVend2() {
        return adaVend2;
    }

    public void setAdaVend2(String adaVend2) {
        this.adaVend2 = adaVend2;
    }

    public String getAdaVend3() {
        return adaVend3;
    }

    public void setAdaVend3(String adaVend3) {
        this.adaVend3 = adaVend3;
    }

    public String getAdaVend4() {
        return adaVend4;
    }

    public void setAdaVend4(String adaVend4) {
        this.adaVend4 = adaVend4;
    }

    public String getAdaVend5() {
        return adaVend5;
    }

    public void setAdaVend5(String adaVend5) {
        this.adaVend5 = adaVend5;
    }

    public double getAdaComis1() {
        return adaComis1;
    }

    public void setAdaComis1(double adaComis1) {
        this.adaComis1 = adaComis1;
    }

    public double getAdaComis2() {
        return adaComis2;
    }

    public void setAdaComis2(double adaComis2) {
        this.adaComis2 = adaComis2;
    }

    public double getAdaComis3() {
        return adaComis3;
    }

    public void setAdaComis3(double adaComis3) {
        this.adaComis3 = adaComis3;
    }

    public double getAdaComis4() {
        return adaComis4;
    }

    public void setAdaComis4(double adaComis4) {
        this.adaComis4 = adaComis4;
    }

    public double getAdaComis5() {
        return adaComis5;
    }

    public void setAdaComis5(double adaComis5) {
        this.adaComis5 = adaComis5;
    }

    public double getAdaMoeda() {
        return adaMoeda;
    }

    public void setAdaMoeda(double adaMoeda) {
        this.adaMoeda = adaMoeda;
    }

    public String getAdaTiplib() {
        return adaTiplib;
    }

    public void setAdaTiplib(String adaTiplib) {
        this.adaTiplib = adaTiplib;
    }

    public String getAdaStatus() {
        return adaStatus;
    }

    public void setAdaStatus(String adaStatus) {
        this.adaStatus = adaStatus;
    }

    public String getAdaSafra() {
        return adaSafra;
    }

    public void setAdaSafra(String adaSafra) {
        this.adaSafra = adaSafra;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }
//
//    public int getRECDEL() {
//        return rECDEL;
//    }
//
//    public void setRECDEL(int rECDEL) {
//        this.rECDEL = rECDEL;
//    }

    public String getAdaMsblql() {
        return adaMsblql;
    }

    public void setAdaMsblql(String adaMsblql) {
        this.adaMsblql = adaMsblql;
    }

    public String getAdaUnrcob() {
        return adaUnrcob;
    }

    public void setAdaUnrcob(String adaUnrcob) {
        this.adaUnrcob = adaUnrcob;
    }

    public double getAdaUdiafe() {
        return adaUdiafe;
    }

    public void setAdaUdiafe(double adaUdiafe) {
        this.adaUdiafe = adaUdiafe;
    }

    public String getAdaUdtfec() {
        return adaUdtfec;
    }

    public void setAdaUdtfec(String adaUdtfec) {
        this.adaUdtfec = adaUdtfec;
    }

    public String getAdaUctant() {
        return adaUctant;
    }

    public void setAdaUctant(String adaUctant) {
        this.adaUctant = adaUctant;
    }

    public String getAdaUliber() {
        return adaUliber;
    }

    public void setAdaUliber(String adaUliber) {
        this.adaUliber = adaUliber;
    }

    public String getAdaUnfimp() {
        return adaUnfimp;
    }

    public void setAdaUnfimp(String adaUnfimp) {
        this.adaUnfimp = adaUnfimp;
    }

    public String getAdaUdtblq() {
        return adaUdtblq;
    }

    public void setAdaUdtblq(String adaUdtblq) {
        this.adaUdtblq = adaUdtblq;
    }

    public String getAdaUtipo() {
        return adaUtipo;
    }

    public void setAdaUtipo(String adaUtipo) {
        this.adaUtipo = adaUtipo;
    }

    public String getAdaUdtlib() {
        return adaUdtlib;
    }

    public void setAdaUdtlib(String adaUdtlib) {
        this.adaUdtlib = adaUdtlib;
    }

    public String getAdaUcallc() {
        return adaUcallc;
    }

    public void setAdaUcallc(String adaUcallc) {
        this.adaUcallc = adaUcallc;
    }

    public List<Adb010> getAdb010s() {
        return adb010s;
    }

    public List<Adb010> getAdb010sNotDel() {
        List<Adb010> lista = new ArrayList<>();
        for (Adb010 a : adb010s) {
            if (a.getDELET().trim().isEmpty()) {
                lista.add(a);
            }
        }
        return lista;
    }

    //Retonar apenas o Itens de ADB que são do grupo 0101 TELEFONE
    public List<Adb010> getAdb010sGrupo0101() {
        List<Adb010> lista = new ArrayList<>();
        for (Adb010 adb : getAdb010sNotDel()) {
            String b1Grupo = adb.getSb1().getB1Grupo();
            if (b1Grupo.equalsIgnoreCase("0101")) {
                lista.add(adb);
            }
        }
        return lista;
    }

    public List<Adb010> getAdb010sGrupo0102() {
        List<Adb010> lista = new ArrayList<>();
        for (Adb010 adb : getAdb010sNotDel()) {
            String b1Grupo = adb.getSb1().getB1Grupo();
            if (b1Grupo.equalsIgnoreCase("0102")) {
                lista.add(adb);
            }
        }
        return lista;
    }

    public List<Adb010> getAdb010sGrupo0103() {
        List<Adb010> lista = new ArrayList<>();
        for (Adb010 adb : getAdb010sNotDel()) {
            String b1Grupo = adb.getSb1().getB1Grupo();
            if (b1Grupo.equalsIgnoreCase("0103")) {
                lista.add(adb);
            }
        }
        return lista;
    }

    public List<Adb010> getAdb010sIns() {
        List<Adb010> lista = new ArrayList<>();
        for (Adb010 adb : getAdb010sNotDel()) {
            String b1Grupo = adb.getSb1().getB1Grupo();
            if (adb.getSb1().getB1Cod().equalsIgnoreCase("020001")) {
                lista.add(adb);
            }
        }
        return lista;
    }

    public List<Agb010> getAgb010s() {
        return agb010s;
    }

    public void setAgb010s(List<Agb010> agb010s) {
        this.agb010s = agb010s;
    }

    public List<Agb010> getAgb010sNotDel() {
        List<Agb010> lista = new ArrayList<>();
        for (Agb010 a : agb010s) {
            if (a.getDELET().trim().isEmpty()) {
                lista.add(a);
            }
        }
        return lista;
    }

    public List<Agb010> getAgb010sYate() {
        List<Agb010> lista = new ArrayList<>();
        for (Agb010 agb : getAgb010sNotDel()) {
            String b1Grupo = agb.getAgbUtipo2();
            if (b1Grupo.equalsIgnoreCase("N") || b1Grupo.equalsIgnoreCase("P")) {
                lista.add(agb);
            }
        }
        return lista;
    }

    public Double calculaDescNet() {
        Double cont = 0.0;
        for (Adb010 adb : getAdb010sGrupo0101()) {
            cont += adb.getAdbDesc();
        }
        return cont;
    }

    public Double calculaTotalNet() {
        Double cont = 0.0;
        for (Adb010 adb : getAdb010sGrupo0101()) {
            cont += adb.getAdbTotal();
        }
        return cont;
    }

    public void setAdb010s(List<Adb010> adb010s) {
        this.adb010s = adb010s;
    }

    public String getAdaFilial() {
        return adaFilial;
    }

    public void setAdaFilial(String adaFilial) {
        this.adaFilial = adaFilial;
    }

    public String getAdaNumctr() {
        return adaNumctr;
    }

    public void setAdaNumctr(String adaNumctr) {
        this.adaNumctr = adaNumctr;
    }

    public String getAdaUYate() {
        return adaUYate.trim();
    }

    public void setAdaUYate(String adaUYate) {
        this.adaUYate = adaUYate;
    }

    public String getdELET() {
        return dELET;
    }

    public void setdELET(String dELET) {
        this.dELET = dELET;
    }

    public int getAdaUVali() {
        return adaUVali;
    }

    public void setAdaUVali(int adaUVali) {
        this.adaUVali = adaUVali;
    }

    public Integer getrECNO() {
        return rECNO;
    }

    public void setrECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Aga010 getAga010() {
        return aga010;
    }

    public void setAga010(Aga010 aga010) {
        this.aga010 = aga010;
    }

//
//    public int getrECDEL() {
//        return rECDEL;
//    }
//
//    public void setrECDEL(int rECDEL) {
//        this.rECDEL = rECDEL;
//    }   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ada010)) {
            return false;
        }
        Ada010 other = (Ada010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Ada010[ rECNO=" + rECNO + " ]";
    }
}
