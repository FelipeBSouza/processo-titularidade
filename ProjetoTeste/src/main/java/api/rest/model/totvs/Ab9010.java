/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(name = "AB9010")
@XmlRootElement
public class Ab9010 implements Serializable { 

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Size(min = 1, max = 4)
    @Column(name = "AB9_FILIAL")
    private String ab9Filial;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB9_NUMOS")
    private String ab9Numos;
    @Basic(optional = false)
    @Size(min = 1, max = 2)
    @Column(name = "AB9_SEQ")
    private String ab9Seq;
    @Basic(optional = false)
    @Size(min = 1, max = 14)
    @Column(name = "AB9_CODTEC")
    private String ab9Codtec;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB9_DTCHEG")
    private String ab9Dtcheg;
    @Basic(optional = false)
    @Size(min = 1, max = 5)
    @Column(name = "AB9_HRCHEG")
    private String ab9Hrcheg;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB9_DTSAID")
    private String ab9Dtsaid;
    @Basic(optional = false)
    @Size(min = 1, max = 5)
    @Column(name = "AB9_HRSAID")
    private String ab9Hrsaid;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB9_DTINI")
    private String ab9Dtini;
    @Basic(optional = false)
    @Size(min = 1, max = 5)
    @Column(name = "AB9_HRINI")
    private String ab9Hrini;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "AB9_DTFIM")
    private String ab9Dtfim;
    @Basic(optional = false)
    @Size(min = 1, max = 5)
    @Column(name = "AB9_HRFIM")
    private String ab9Hrfim;
    @Basic(optional = false)
    @Size(min = 1, max = 5)
    @Column(name = "AB9_TRASLA")
    private String ab9Trasla;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_CODPRB")
    private String ab9Codprb;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_GARANT")
    private String ab9Garant;
    @Basic(optional = false)
    @Column(name = "AB9_ACUMUL")
    private double ab9Acumul;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_TIPO")
    private String ab9Tipo;
    @Basic(optional = false)
    @Size(min = 1, max = 9)
    @Column(name = "AB9_CODCLI")
    private String ab9Codcli;
    @Basic(optional = false)
    @Size(min = 1, max = 3)
    @Column(name = "AB9_LOJA")
    private String ab9Loja;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "AB9_CODPRO")
    private String ab9Codpro;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_MEMO1")
    private String ab9Memo1;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_TOTFAT")
    private String ab9Totfat;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_NUMORC")
    private String ab9Numorc;
    @Basic(optional = false)
    @Column(name = "AB9_CUSTO")
    private double ab9Custo;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_BENENV")
    private String ab9Benenv;
    @Basic(optional = false)
    @Size(min = 1, max = 10)
    @Column(name = "AB9_TAREFA")
    private String ab9Tarefa;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_STATAR")
    private String ab9Statar;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "AB9_CODFNC")
    private String ab9Codfnc;
    @Basic(optional = false)
    @Size(min = 1, max = 2)
    @Column(name = "AB9_FNCREV")
    private String ab9Fncrev;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_TMKLST")
    private String ab9Tmklst;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "AB9_CONTRT")
    private String ab9Contrt;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_CODGRP")
    private String ab9Codgrp;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_MPONTO")
    private String ab9Mponto;
    @Basic(optional = false)
    @Size(min = 1, max = 12)
    @Column(name = "AB9_ATAUT")
    private String ab9Ataut;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL = 0;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_USITU2")
    private String ab9Usitu2;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_ATUPRE")
    private String ab9Atupre;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_ITAPUR")
    private String ab9Itapur;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "AB9_ADIENV")
    private String ab9Adienv;
    @Basic(optional = false)
    @Size(min = 1, max = 6)
    @Column(name = "AB9_CODTWZ")
    private String ab9Codtwz;

    public Ab9010() {
    }

    public Ab9010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Ab9010(Integer rECNO, String ab9Filial, String ab9Numos, String ab9Seq, String ab9Codtec, String ab9Dtcheg, String ab9Hrcheg, String ab9Dtsaid, String ab9Hrsaid, String ab9Dtini, String ab9Hrini, String ab9Dtfim, String ab9Hrfim, String ab9Trasla, String ab9Codprb, String ab9Garant, double ab9Acumul, String ab9Tipo, String ab9Codcli, String ab9Loja, String ab9Codpro, String ab9Memo1, String ab9Totfat, String ab9Numorc, double ab9Custo, String ab9Benenv, String ab9Tarefa, String ab9Statar, String ab9Codfnc, String ab9Fncrev, String ab9Tmklst, String ab9Contrt, String ab9Codgrp, String ab9Mponto, String ab9Ataut, String dELET, int rECDEL, String ab9Usitu2, String ab9Atupre, String ab9Itapur, String ab9Adienv, String ab9Codtwz) {
        this.rECNO = rECNO;
        this.ab9Filial = ab9Filial;
        this.ab9Numos = ab9Numos;
        this.ab9Seq = ab9Seq;
        this.ab9Codtec = ab9Codtec;
        this.ab9Dtcheg = ab9Dtcheg;
        this.ab9Hrcheg = ab9Hrcheg;
        this.ab9Dtsaid = ab9Dtsaid;
        this.ab9Hrsaid = ab9Hrsaid;
        this.ab9Dtini = ab9Dtini;
        this.ab9Hrini = ab9Hrini;
        this.ab9Dtfim = ab9Dtfim;
        this.ab9Hrfim = ab9Hrfim;
        this.ab9Trasla = ab9Trasla;
        this.ab9Codprb = ab9Codprb;
        this.ab9Garant = ab9Garant;
        this.ab9Acumul = ab9Acumul;
        this.ab9Tipo = ab9Tipo;
        this.ab9Codcli = ab9Codcli;
        this.ab9Loja = ab9Loja;
        this.ab9Codpro = ab9Codpro;
        this.ab9Memo1 = ab9Memo1;
        this.ab9Totfat = ab9Totfat;
        this.ab9Numorc = ab9Numorc;
        this.ab9Custo = ab9Custo;
        this.ab9Benenv = ab9Benenv;
        this.ab9Tarefa = ab9Tarefa;
        this.ab9Statar = ab9Statar;
        this.ab9Codfnc = ab9Codfnc;
        this.ab9Fncrev = ab9Fncrev;
        this.ab9Tmklst = ab9Tmklst;
        this.ab9Contrt = ab9Contrt;
        this.ab9Codgrp = ab9Codgrp;
        this.ab9Mponto = ab9Mponto;
        this.ab9Ataut = ab9Ataut;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
        this.ab9Usitu2 = ab9Usitu2;
        this.ab9Atupre = ab9Atupre;
        this.ab9Itapur = ab9Itapur;
        this.ab9Adienv = ab9Adienv;
        this.ab9Codtwz = ab9Codtwz;
    }

    public String getAb9Filial() {
        return ab9Filial;
    }

    public void setAb9Filial(String ab9Filial) {
        this.ab9Filial = ab9Filial;
    }

    public String getAb9Numos() {
        return ab9Numos;
    }

    public void setAb9Numos(String ab9Numos) {
        this.ab9Numos = ab9Numos;
    }

    public String getAb9Seq() {
        return ab9Seq;
    }

    public void setAb9Seq(String ab9Seq) {
        this.ab9Seq = ab9Seq;
    }

    public String getAb9Codtec() {
        return ab9Codtec;
    }

    public void setAb9Codtec(String ab9Codtec) {
        this.ab9Codtec = ab9Codtec;
    }

    public String getAb9Dtcheg() {
        return ab9Dtcheg;
    }

    public void setAb9Dtcheg(String ab9Dtcheg) {
        this.ab9Dtcheg = ab9Dtcheg;
    }

    public String getAb9Hrcheg() {
        return ab9Hrcheg;
    }

    public void setAb9Hrcheg(String ab9Hrcheg) {
        this.ab9Hrcheg = ab9Hrcheg;
    }

    public String getAb9Dtsaid() {
        return ab9Dtsaid;
    }

    public void setAb9Dtsaid(String ab9Dtsaid) {
        this.ab9Dtsaid = ab9Dtsaid;
    }

    public String getAb9Hrsaid() {
        return ab9Hrsaid;
    }

    public void setAb9Hrsaid(String ab9Hrsaid) {
        this.ab9Hrsaid = ab9Hrsaid;
    }

    public String getAb9Dtini() {
        return ab9Dtini;
    }

    public void setAb9Dtini(String ab9Dtini) {
        this.ab9Dtini = ab9Dtini;
    }

    public String getAb9Hrini() {
        return ab9Hrini;
    }

    public void setAb9Hrini(String ab9Hrini) {
        this.ab9Hrini = ab9Hrini;
    }

    public String getAb9Dtfim() {
        return ab9Dtfim;
    }

    public void setAb9Dtfim(String ab9Dtfim) {
        this.ab9Dtfim = ab9Dtfim;
    }

    public String getAb9Hrfim() {
        return ab9Hrfim;
    }

    public void setAb9Hrfim(String ab9Hrfim) {
        this.ab9Hrfim = ab9Hrfim;
    }

    public String getAb9Trasla() {
        return ab9Trasla;
    }

    public void setAb9Trasla(String ab9Trasla) {
        this.ab9Trasla = ab9Trasla;
    }

    public String getAb9Codprb() {
        return ab9Codprb;
    }

    public void setAb9Codprb(String ab9Codprb) {
        this.ab9Codprb = ab9Codprb;
    }

    public String getAb9Garant() {
        return ab9Garant;
    }

    public void setAb9Garant(String ab9Garant) {
        this.ab9Garant = ab9Garant;
    }

    public double getAb9Acumul() {
        return ab9Acumul;
    }

    public void setAb9Acumul(double ab9Acumul) {
        this.ab9Acumul = ab9Acumul;
    }

    public String getAb9Tipo() {
        return ab9Tipo;
    }

    public void setAb9Tipo(String ab9Tipo) {
        this.ab9Tipo = ab9Tipo;
    }

    public String getAb9Codcli() {
        return ab9Codcli;
    }

    public void setAb9Codcli(String ab9Codcli) {
        this.ab9Codcli = ab9Codcli;
    }

    public String getAb9Loja() {
        return ab9Loja;
    }

    public void setAb9Loja(String ab9Loja) {
        this.ab9Loja = ab9Loja;
    }

    public String getAb9Codpro() {
        return ab9Codpro;
    }

    public void setAb9Codpro(String ab9Codpro) {
        this.ab9Codpro = ab9Codpro;
    }

    public String getAb9Memo1() {
        return ab9Memo1;
    }

    public void setAb9Memo1(String ab9Memo1) {
        this.ab9Memo1 = ab9Memo1;
    }

    public String getAb9Totfat() {
        return ab9Totfat;
    }

    public void setAb9Totfat(String ab9Totfat) {
        this.ab9Totfat = ab9Totfat;
    }

    public String getAb9Numorc() {
        return ab9Numorc;
    }

    public void setAb9Numorc(String ab9Numorc) {
        this.ab9Numorc = ab9Numorc;
    }

    public double getAb9Custo() {
        return ab9Custo;
    }

    public void setAb9Custo(double ab9Custo) {
        this.ab9Custo = ab9Custo;
    }

    public String getAb9Benenv() {
        return ab9Benenv;
    }

    public void setAb9Benenv(String ab9Benenv) {
        this.ab9Benenv = ab9Benenv;
    }

    public String getAb9Tarefa() {
        return ab9Tarefa;
    }

    public void setAb9Tarefa(String ab9Tarefa) {
        this.ab9Tarefa = ab9Tarefa;
    }

    public String getAb9Statar() {
        return ab9Statar;
    }

    public void setAb9Statar(String ab9Statar) {
        this.ab9Statar = ab9Statar;
    }

    public String getAb9Codfnc() {
        return ab9Codfnc;
    }

    public void setAb9Codfnc(String ab9Codfnc) {
        this.ab9Codfnc = ab9Codfnc;
    }

    public String getAb9Fncrev() {
        return ab9Fncrev;
    }

    public void setAb9Fncrev(String ab9Fncrev) {
        this.ab9Fncrev = ab9Fncrev;
    }

    public String getAb9Tmklst() {
        return ab9Tmklst;
    }

    public void setAb9Tmklst(String ab9Tmklst) {
        this.ab9Tmklst = ab9Tmklst;
    }

    public String getAb9Contrt() {
        return ab9Contrt;
    }

    public void setAb9Contrt(String ab9Contrt) {
        this.ab9Contrt = ab9Contrt;
    }

    public String getAb9Codgrp() {
        return ab9Codgrp;
    }

    public void setAb9Codgrp(String ab9Codgrp) {
        this.ab9Codgrp = ab9Codgrp;
    }

    public String getAb9Mponto() {
        return ab9Mponto;
    }

    public void setAb9Mponto(String ab9Mponto) {
        this.ab9Mponto = ab9Mponto;
    }

    public String getAb9Ataut() {
        return ab9Ataut;
    }

    public void setAb9Ataut(String ab9Ataut) {
        this.ab9Ataut = ab9Ataut;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getAb9Usitu2() {
        return ab9Usitu2;
    }

    public void setAb9Usitu2(String ab9Usitu2) {
        this.ab9Usitu2 = ab9Usitu2;
    }

    public String getAb9Atupre() {
        return ab9Atupre;
    }

    public void setAb9Atupre(String ab9Atupre) {
        this.ab9Atupre = ab9Atupre;
    }

    public String getAb9Itapur() {
        return ab9Itapur;
    }

    public void setAb9Itapur(String ab9Itapur) {
        this.ab9Itapur = ab9Itapur;
    }

    public String getAb9Adienv() {
        return ab9Adienv;
    }

    public void setAb9Adienv(String ab9Adienv) {
        this.ab9Adienv = ab9Adienv;
    }

    public String getAb9Codtwz() {
        return ab9Codtwz;
    }

    public void setAb9Codtwz(String ab9Codtwz) {
        this.ab9Codtwz = ab9Codtwz;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ab9010)) {
            return false;
        }
        Ab9010 other = (Ab9010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.liguetelecom.model.totvs.Ab9010[ rECNO=" + rECNO + " ]";
    }
}
