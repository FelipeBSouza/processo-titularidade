package api.rest.model.telefonia;
// Generated 11/12/2012 10:01:38 by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(schema = "telefonia")
public class Planos implements java.io.Serializable {

    @Id
    private int id;
    private String nome;
    private Double intrarede;
    private Double fixo_local;
    private Double fixo_ddd;
    private Double movel_local;
    private Double movel_ddd;
    private Double internacional;
    private int local_min;
    private int local_fra;
    private int ldn_min;
    private int ldn_fra;
    private int ldi_min;
    private int ldi_fra;
    private int movel_min;
    private int movel_fra;
    private Double vr_mensal;
    private Integer franquia_local;
    private Boolean deduzir_local;
    private Boolean deduzir_ldn;
    private Boolean deduzir_movel;
    private Double franquia_celular;
    private Double franquia_ldn;
    @OneToMany(mappedBy = "plano")
    private List<Numero> numeros;
//    @OneToMany(mappedBy = "plano")
//    private List<Cdr> cdrs;

    public Planos() {
    }

    public Boolean getDeduzir_ldn() {
        return deduzir_ldn;
    }

    public void setDeduzir_ldn(Boolean deduzir_ldn) {
        this.deduzir_ldn = deduzir_ldn;
    }

    public Boolean getDeduzir_local() {
        return deduzir_local;
    }

    public void setDeduzir_local(Boolean deduzir_local) {
        this.deduzir_local = deduzir_local;
    }

    public Boolean getDeduzir_movel() {
        return deduzir_movel;
    }

    public void setDeduzir_movel(Boolean deduzir_movel) {
        this.deduzir_movel = deduzir_movel;
    }

    public Double getFixo_ddd() {
        return fixo_ddd;
    }

    public String getFixo_dddFormat() {
        return String.format("%.2f", this.fixo_ddd);
    }

    public void setFixo_ddd(Double fixo_ddd) {
        this.fixo_ddd = fixo_ddd;
    }

    public Double getFixo_local() {
        return fixo_local;
    }

    public String getFixo_localFormat() {
        return String.format("%.2f", this.fixo_local);
    }

    public void setFixo_local(Double fixo_local) {
        this.fixo_local = fixo_local;
    }

    public Double getFranquia_celular() {
        return franquia_celular;
    }

    public void setFranquia_celular(Double franquia_celular) {
        this.franquia_celular = franquia_celular;
    }

    public Double getFranquia_ldn() {
        return franquia_ldn;
    }

    public void setFranquia_ldn(Double franquia_ldn) {
        this.franquia_ldn = franquia_ldn;
    }

    public Integer getFranquia_local() {
        return franquia_local;
    }

    public void setFranquia_local(Integer franquia_local) {
        this.franquia_local = franquia_local;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getInternacional() {
        return internacional;
    }

    public void setInternacional(Double internacional) {
        this.internacional = internacional;
    }

    public Double getIntrarede() {
        return intrarede;
    }

    public void setIntrarede(Double intrarede) {
        this.intrarede = intrarede;
    }

    public int getLdi_fra() {
        return ldi_fra;
    }

    public void setLdi_fra(int ldi_fra) {
        this.ldi_fra = ldi_fra;
    }

    public int getLdi_min() {
        return ldi_min;
    }

    public void setLdi_min(int ldi_min) {
        this.ldi_min = ldi_min;
    }

    public int getLdn_fra() {
        return ldn_fra;
    }

    public void setLdn_fra(int ldn_fra) {
        this.ldn_fra = ldn_fra;
    }

    public int getLdn_min() {
        return ldn_min;
    }

    public void setLdn_min(int ldn_min) {
        this.ldn_min = ldn_min;
    }

    public int getLocal_fra() {
        return local_fra;
    }

    public void setLocal_fra(int local_fra) {
        this.local_fra = local_fra;
    }

    public int getLocal_min() {
        return local_min;
    }

    public void setLocal_min(int local_min) {
        this.local_min = local_min;
    }

    public Double getMovel_ddd() {
        return movel_ddd;
    }

    public void setMovel_ddd(Double movel_ddd) {
        this.movel_ddd = movel_ddd;
    }

    public String getMovel_dddFormat() {
        return String.format("%.2f", this.movel_ddd);
    }

    public int getMovel_fra() {
        return movel_fra;
    }

    public void setMovel_fra(int movel_fra) {
        this.movel_fra = movel_fra;
    }

    public Double getMovel_local() {
        return movel_local;
    }

    public void setMovel_local(Double movel_local) {
        this.movel_local = movel_local;
    }

    public String getMovel_localFormat() {
        return String.format("%.2f", this.movel_local);
    }

    public int getMovel_min() {
        return movel_min;
    }

    public void setMovel_min(int movel_min) {
        this.movel_min = movel_min;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Numero> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Numero> numeros) {
        this.numeros = numeros;
    }

    public Double getVr_mensal() {
        return vr_mensal;
    }

    public void setVr_mensal(Double vr_mensal) {
        this.vr_mensal = vr_mensal;
    }

    public Double retornaLimite() {
        if (this.getFranquia_local() + this.getFranquia_ldn() < 200) {
            return this.getFranquia_local() + this.getFranquia_ldn() + 200;
        } else {
            return (this.getFranquia_local() + this.getFranquia_ldn()) * 2;
        }
    }

    @Override
    public String toString() {
        return this.getNome();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Planos other = (Planos) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
