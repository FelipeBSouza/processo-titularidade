/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.internet;

/**
 *
 * @author ROBSON
 */
import api.rest.model.integrador.Internet;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@XmlRootElement
@Table(schema = "internet", name = "radusergroup")
public class RadUserGroup implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int id;
    private String username;
    private String groupname;
    private int priority = 0;
    @JsonIgnore
    @OneToOne(mappedBy = "radUserGroup")
    private Internet internet;

    public RadUserGroup() {
    }

    public RadUserGroup(String username, String groupname) {
        this.username = username;
        this.groupname = groupname;
    }

    public String getGroupname() {
        return groupname;
    } 

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Internet getInternet() {
        return internet;
    }

    public void setInternet(Internet internet) {
        this.internet = internet;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RadUserGroup other = (RadUserGroup) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        return hash;
    }
}
