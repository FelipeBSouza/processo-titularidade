package api.rest.model.internet;

import api.rest.model.integrador.Internet;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement
@Entity
@Table(schema = "internet")
public class Radcheck implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String attribute;
    private String op = ":=";
    private String value;
    @JsonIgnore
    @OneToMany(mappedBy = "cd_radcheck_senha", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Internet> internetSenha;
    @JsonIgnore
    @OneToMany(mappedBy = "cd_radcheck_mac", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Internet> internetMac;

    public Radcheck() {
    }

    public Radcheck(String attribute) {
        this.attribute = attribute;
    }

    public Radcheck(String username, String attribute, String value) {
        this.username = username;
        this.attribute = attribute;
        this.value = value;
    }

   
    
    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Internet> getInternetMac() {
        return internetMac;
    }

    public void setInternetMac(List<Internet> internetMac) {
        this.internetMac = internetMac;
    }

    public List<Internet> getInternetSenha() {
        return internetSenha;
    }

    public void setInternetSenha(List<Internet> internetSenha) {
        this.internetSenha = internetSenha;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Radcheck other = (Radcheck) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
