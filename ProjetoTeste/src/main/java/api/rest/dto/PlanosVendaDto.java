package api.rest.dto;


public class PlanosVendaDto{

	private Integer cd_prod_venda;
	private String cod_prod_totvs;
	private String descricao;
	private String valor_cheio;
	private String combo_1;
	private String combo_2;
	private String combo_3;
	private String qtd_mes_ini;
	private String qtd_mes_cob;
	private String prod_obrig_1;
	private String prod_obrig_2;
	private String prod_obrig_3;
	private String prod_obrig_4;
	private String prod_obrig_5;
	private String tp_produto;
	private String altera_desconto;
	private String id_uf;
	private String id_cidade;
	private String id_bairro;
	private String dt_ini_promocao;
	private String dt_fim_promocao;

	private String grupo;
	private String upc01;
	private String upc02;
	private String referencia;	
	private String combo;	
	private String download;
	private String upload;
	private String visivel;

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Integer getCd_prod_venda() {
		return cd_prod_venda;
	}

	public void setCd_prod_venda(Integer cd_prod_venda) {
		this.cd_prod_venda = cd_prod_venda;
	}

	public String getCod_prod_totvs() {
		return cod_prod_totvs;
	}

	public void setCod_prod_totvs(String cod_prod_totvs) {
		this.cod_prod_totvs = cod_prod_totvs;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor_cheio() {
		return valor_cheio;
	}

	public void setValor_cheio(String valor_cheio) {
		this.valor_cheio = valor_cheio;
	}

	public String getCombo_1() {
		return combo_1;
	}

	public void setCombo_1(String combo_1) {
		this.combo_1 = combo_1;
	}

	public String getCombo_2() {
		return combo_2;
	}

	public void setCombo_2(String combo_2) {
		this.combo_2 = combo_2;
	}

	public String getCombo_3() {
		return combo_3;
	}

	public void setCombo_3(String combo_3) {
		this.combo_3 = combo_3;
	}

	public String getQtd_mes_ini() {
		return qtd_mes_ini;
	}

	public void setQtd_mes_ini(String qtd_mes_ini) {
		this.qtd_mes_ini = qtd_mes_ini;
	}

	public String getQtd_mes_cob() {
		return qtd_mes_cob;
	}

	public void setQtd_mes_cob(String qtd_mes_cob) {
		this.qtd_mes_cob = qtd_mes_cob;
	}

	public String getProd_obrig_1() {
		return prod_obrig_1;
	}

	public void setProd_obrig_1(String prod_obrig_1) {
		this.prod_obrig_1 = prod_obrig_1;
	}

	public String getProd_obrig_2() {
		return prod_obrig_2;
	}

	public void setProd_obrig_2(String prod_obrig_2) {
		this.prod_obrig_2 = prod_obrig_2;
	}

	public String getProd_obrig_3() {
		return prod_obrig_3;
	}

	public void setProd_obrig_3(String prod_obrig_3) {
		this.prod_obrig_3 = prod_obrig_3;
	}

	public String getProd_obrig_4() {
		return prod_obrig_4;
	}

	public void setProd_obrig_4(String prod_obrig_4) {
		this.prod_obrig_4 = prod_obrig_4;
	}

	public String getProd_obrig_5() {
		return prod_obrig_5;
	}

	public void setProd_obrig_5(String prod_obrig_5) {
		this.prod_obrig_5 = prod_obrig_5;
	}

	public String getTp_produto() {
		return tp_produto;
	}

	public void setTp_produto(String tp_produto) {
		this.tp_produto = tp_produto;
	}

	public String getAltera_desconto() {
		return altera_desconto;
	}

	public void setAltera_desconto(String altera_desconto) {
		this.altera_desconto = altera_desconto;
	}

	public String getId_uf() {
		return id_uf;
	}

	public void setId_uf(String id_uf) {
		this.id_uf = id_uf;
	}

	public String getId_cidade() {
		return id_cidade;
	}

	public void setId_cidade(String id_cidade) {
		this.id_cidade = id_cidade;
	}

	public String getId_bairro() {
		return id_bairro;
	}

	public void setId_bairro(String id_bairro) {
		this.id_bairro = id_bairro;
	}

	public String getDt_ini_promocao() {
		return dt_ini_promocao;
	}

	public void setDt_ini_promocao(String dt_ini_promocao) {
		this.dt_ini_promocao = dt_ini_promocao;
	}

	public String getDt_fim_promocao() {
		return dt_fim_promocao;
	}

	public void setDt_fim_promocao(String dt_fim_promocao) {
		this.dt_fim_promocao = dt_fim_promocao;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getUpc01() {
		return upc01;
	}

	public void setUpc01(String upc01) {
		this.upc01 = upc01;
	}

	public String getUpc02() {
		return upc02;
	}

	public void setUpc02(String upc02) {
		this.upc02 = upc02;
	}

	public String getCombo() {
		return combo;
	}

	public void setCombo(String combo) {
		this.combo = combo;
	}

	public String getDownload() {
		return download;
	}

	public void setDownload(String download) {
		this.download = download;
	}

	public String getUpload() {
		return upload;
	}

	public void setUpload(String upload) {
		this.upload = upload;
	}

	public String getVisivel() {
		return visivel;
	}

	public void setVisivel(String visivel) {
		this.visivel = visivel;
	}	
	
	
	
}
