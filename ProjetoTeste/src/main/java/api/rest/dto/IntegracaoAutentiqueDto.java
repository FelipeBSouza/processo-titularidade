package api.rest.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

public class IntegracaoAutentiqueDto {
	
	private Integer cd_integracao_autentique;	
	private String id_contrato;
	private String nr_solicitacao;
	private String ctr_ass;
	private Integer atividade;
	private String contrato_assinado;
	private String mov_sol;
	private String cod_retorno;
	private String desc_retorno;
	
	public Integer getCd_integracao_autentique() {
		return cd_integracao_autentique;
	}
	public void setCd_integracao_autentique(Integer cd_integracao_autentique) {
		this.cd_integracao_autentique = cd_integracao_autentique;
	}
	public String getId_contrato() {
		return id_contrato;
	}
	public void setId_contrato(String id_contrato) {
		this.id_contrato = id_contrato;
	}
	public String getNr_solicitacao() {
		return nr_solicitacao;
	}
	public void setNr_solicitacao(String nr_solicitacao) {
		this.nr_solicitacao = nr_solicitacao;
	}
	public String getCtr_ass() {
		return ctr_ass;
	}
	public void setCtr_ass(String ctr_ass) {
		this.ctr_ass = ctr_ass;
	}
	public Integer getAtividade() {
		return atividade;
	}
	public void setAtividade(Integer atividade) {
		this.atividade = atividade;
	}
	public String getContrato_assinado() {
		return contrato_assinado;
	}
	public void setContrato_assinado(String contrato_assinado) {
		this.contrato_assinado = contrato_assinado;
	}
	public String getMov_sol() {
		return mov_sol;
	}
	public void setMov_sol(String mov_sol) {
		this.mov_sol = mov_sol;
	}
	public String getCod_retorno() {
		return cod_retorno;
	}
	public void setCod_retorno(String cod_retorno) {
		this.cod_retorno = cod_retorno;
	}
	public String getDesc_retorno() {
		return desc_retorno;
	}
	public void setDesc_retorno(String desc_retorno) {
		this.desc_retorno = desc_retorno;
	}	
		
}
