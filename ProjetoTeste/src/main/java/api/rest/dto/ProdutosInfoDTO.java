package api.rest.dto;

import java.util.*;

public class ProdutosInfoDTO {

    private int codigo;
    private String mensagem;
    private List<ProdutosDTO> produtos;
    
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public List<ProdutosDTO> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<ProdutosDTO> produtos) {
		this.produtos = produtos;
	}
  
}
