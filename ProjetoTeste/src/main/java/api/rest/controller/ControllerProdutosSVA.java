package api.rest.controller;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import api.rest.model.integrador.Products;
import api.rest.planos.ParametroTpPLano;
import api.rest.planos.ParametrosPlanoCorrespondente;
import api.rest.planos.PlanoParametroReferencia;
import api.rest.planos.PlanoParametroTipo;
import api.rest.planos.ProdutosObrigatorios;
import api.rest.helpers.*;
import api.rest.dao.DaoProdutosSVA;
import api.rest.dao.Utils;
import api.rest.repositoy.PlanosCorrespondentesRepository;
import api.rest.repositoy.ProdutosSVARepository;
import api.rest.response.ResponseProdutoUnico;
import api.rest.response.ResponseProdutosFluig;
import api.rest.response.ResponseProdutosObrigatoriosFluig;
import api.rest.response.ResponseProdutosObrigatoriosRenovacaoFluig;
import api.rest.response.ResponseTpPlano;

@CrossOrigin(origins = "*")
@RestController
@Validated
@RequestMapping(value = "/produtossva")
@Api(value = "Product API")
public class ControllerProdutosSVA {

	@Autowired
	private ProdutosSVARepository productsRep;
	@Autowired
	private PlanosCorrespondentesRepository planosCorrespondentesRepository;

	Utils utils = new Utils();
	DaoProdutosSVA daoProdutosSVA;

	@ApiOperation(value = "Consulta planos")
    @PostMapping(value = "/listaProdutos", produces = "application/json")
	 public ResponseEntity<ResponseProdutosFluig> listProdutos(@RequestBody ListaParametrosEntrada dadosParametro) {
		
		List<Products> listFinal = new ArrayList<>();
		ResponseProdutosFluig responseProdutosFluig = new ResponseProdutosFluig();
		
		int cont_erros = 0;
		int cod_erro = 200;
		String msg_erro = "OK";
		
			try {
				
				// caso o bairro passado seja diferente de "TODOS"
				if(!dadosParametro.getBairro().equals("TODOS")) {
					
					System.out.println("BAIRRO INVALIDO");	
					cod_erro = 400;
					msg_erro = "Bairro invalido";
					
					cont_erros++;
				}else if(!dadosParametro.getCategoria().equals("P1") && !dadosParametro.getCategoria().equals("P2")) {
					
					cod_erro = 400;
					msg_erro = "Categoria invalida";
					
					cont_erros++;
					
				}else if(dadosParametro.getCategoria().equals("P2") && dadosParametro.getTp_produto().equals("")) {
					
					cod_erro = 400;
					msg_erro = "Informar tipo do produto";
					
					cont_erros++;
					
				}
								
				// se todos os parametros estiverem corretos entra no if para fazer a consulta dos planos
				if(cont_erros <= 0) {					

					listFinal = productsRep.listaProdutosP1Geral(dadosParametro.getCidade(), dadosParametro.getBairro(), dadosParametro.getTp_produto(), dadosParametro.getIdPromocao(), dadosParametro.getFidelidade(), dadosParametro.getCategoria());
					
					// se encontrado algum registro
					if(listFinal.size() > 0) {
						responseProdutosFluig.setCodigo(cod_erro);
						responseProdutosFluig.setMensagem(msg_erro);
						responseProdutosFluig.setProdutos(listFinal);
						
					}else {// se nenhum registro for encontrado
						
						responseProdutosFluig.setCodigo(404);
						responseProdutosFluig.setMensagem("Nenhum registro encontrado");
						responseProdutosFluig.setProdutos(listFinal);
					}					
					
					
				}else { // retorna o erro ocorrido caso houver
					
					responseProdutosFluig.setCodigo(cod_erro);
					responseProdutosFluig.setMensagem(msg_erro);
					responseProdutosFluig.setProdutos(listFinal);
					
				}

				
			} catch (Exception e) {

				System.out.println("deu erro, " + e.getMessage());
				e.printStackTrace();
				
				responseProdutosFluig.setCodigo(500);
				responseProdutosFluig.setMensagem(e.getMessage());
				responseProdutosFluig.setProdutos(listFinal);
			}			
			
			return new ResponseEntity<ResponseProdutosFluig>(responseProdutosFluig, HttpStatus.OK);
		}
	
	
	@ApiOperation(value = "Retorna todos os planos com seus servicos obrigatorios ja calculados os valores de desconto")
    @PostMapping(value = "/retornaPlanosServicos", produces = "application/json")
	 public ResponseEntity<ResponseProdutosObrigatoriosFluig> retornaPlanosServicos(@Valid @RequestBody ListaParametrosEntradaPlanosExistentes dadosParametro) {
		
		List<ProdutosObrigatorios> listPlanosResponse = new ArrayList<>();
		ResponseProdutosObrigatoriosFluig produtosObrigatoriosFluig = new ResponseProdutosObrigatoriosFluig();
		ProdutosObrigatorios produtosObrigatorios;
		
		int qtd_net = 0;
		int qtd_fone = 0;
		int qtd_tv = 0;
		int qtd_net_500_megas = 0;
		int qtd_ins_ponto_adicional_tv = 0;
		int qtd_planos_net = 0;
		int qtd_planos_fone_validacao = 0;
		int qtd_pacote_tv = 0;
		
		int combo = 0; // diz qual combo e, se e combo 1, combo 2 ou combo 3
		int qtd_planos_fone = 0; //para saber a quantidade de assinaturas que deverao ser adicionadas
				
		List<String> listReferencias = new ArrayList<>(); // contem as referencias dos planos P1 passados por parametro
		List<String> listReferenciasProdutosObrigatorios = new ArrayList<>(); // contem as referencias dos produtos obrigatorios
		String servicosObrigatorios = ""; // lista com todos os servicos obrigatorios
		String desconto = ""; // desconto final conforme o combo, desconto que sera retornado na api
		String msg_retorno = "OK"; // usada para dar retorno para o fluig
		
		int i = 0; // contador laco de repeticao
		int x = 0; // contador laco repeticao
		int y = 0; // contador laco repeticao
		int j = 0; // contador laco repeticao
		int u = 0; // contador laco repeticao
		int b = 0; // contador laco de repeticao
		
		// verificacao se e combo 1, combo 2 ou combo 3
		
		if(dadosParametro.getPlanosExistentes().size() > 0) {
			
			while(i < dadosParametro.getPlanosExistentes().size()) {
				
				if(dadosParametro.getPlanosExistentes().get(i).getTp_plano().equals("NET")) {
					qtd_net = 1;
					
					Products produto = productsRep.buscaPlanoPorReferencia(dadosParametro.getPlanosExistentes().get(i).getReferencia_produto());	
					
					// se tiver plano de 500 megas de internet
					if(produto.getCod_totvs().equals("030026")) {
						qtd_net_500_megas++;
					}
				}
				if(dadosParametro.getPlanosExistentes().get(i).getTp_plano().equals("FONE")) {
					qtd_fone = 1;
					qtd_planos_fone ++;
				}
				if(dadosParametro.getPlanosExistentes().get(i).getTp_plano().equals("TV")) {
					qtd_tv = 1;
								
					// verifica a quantidade de pontos adicionais que deve ser adicionado de forma automatica
					if(dadosParametro.getPlanosExistentes().get(i).getReferencia_produto().replaceAll("[^0-9]", "").length() > 0) {
						qtd_ins_ponto_adicional_tv = Integer.parseInt(dadosParametro.getPlanosExistentes().get(i).getReferencia_produto().replaceAll("[^0-9]", "")) - 1;
					}
				}
				
				if(dadosParametro.getPlanosExistentes().get(i).getTp_plano().equals("PACOTE_TV")) {
					qtd_pacote_tv ++;
				}
				
				Products produto = productsRep.buscaPlanoPorReferencia(dadosParametro.getPlanosExistentes().get(i).getReferencia_produto());
				
				// aqui adiciona as referencias dos planos P1 passados por parametro para posteriormente buscar os servicos obrigatorios
				if(produto.getCategoria().equals("P1")) {
					listReferencias.add(dadosParametro.getPlanosExistentes().get(i).getReferencia_produto());
				}						
				
				i++;
			}
			
		}
		
		combo = qtd_net + qtd_fone + qtd_tv; // define qual combo sera
				
		// aqui vai buscar os servicos obrigatorios
		if(listReferencias.size() > 0) {
			
			while(x < listReferencias.size()) {
				
				Products produtos = productsRep.buscaPlanoPorReferencia(listReferencias.get(x));	
								
				servicosObrigatorios = produtos.getReferencia_obg() + ";" + produtos.getReferencia_sva();
								
				listReferenciasProdutosObrigatorios.addAll(Arrays.asList(servicosObrigatorios.split(";")));	
								
				x++;
			}			
						
		}				
		
		// remove itens duplicados da lista
	    List<String> listReferenciasProdutosObrigatoriosLimpo = listReferenciasProdutosObrigatorios.stream().distinct().collect(Collectors.toList());	    
	    
	    listReferenciasProdutosObrigatoriosLimpo.addAll(listReferencias); // adiciona os planos P1 vindos por parametro para ficar a lista completa de todos os planos + servicos obrigatorios
	    
	    // adiciona a instalacao dos pontos adicionais de tv
	    if(qtd_ins_ponto_adicional_tv > 0 &&  qtd_tv > 0) {
	    	// busca a referencia do ponto adicional de tv conforme a cidade
	    	String referenciaInstalacaoPontoAdicionalPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "INS_PONTO_ADIC", dadosParametro.getIdPromocao(), dadosParametro.getFidelidade());
	    	
	    	while (u < qtd_ins_ponto_adicional_tv - 1) {
	    		
	    		listReferenciasProdutosObrigatoriosLimpo.add(referenciaInstalacaoPontoAdicionalPorCidade); // adiciona as instalacoes de ponto adicional de tv
	    		
	    		u++;
	    	}	    	
	    	
	    }
	    
	    // adiciona assinatura para cada linha de telefone
	    if(qtd_planos_fone > 1) {
	    	
	    	// busca a referencia da assinatura de telefone conforme a cidade
	    	String referenciaAssinaturaFtthPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "ASS_FTTH", dadosParametro.getIdPromocao(), dadosParametro.getFidelidade());
	    	
	    	while(j < qtd_planos_fone - 1) {
	    		
	    		listReferenciasProdutosObrigatoriosLimpo.add(referenciaAssinaturaFtthPorCidade); // adiciona assinatura para cada linha de telefone
	    		
	    		j++;
	    	}
	    	
	    }
	    int qtd_ata =(int)qtd_planos_fone/2;
	    int w = 0;
 		System.out.println("qtd de ata: " + qtd_ata);
 		
 		if(qtd_ata > 0) {
 			
 			String referenciaATAPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "SERV_ATA", "", dadosParametro.getFidelidade());
 			
 			while(w < qtd_ata) {
 	 			
 				listReferenciasProdutosObrigatoriosLimpo.add(referenciaATAPorCidade); // adiciona assinatura para cada linha de telefone
 				System.out.println("add servico ata");
 	 			w++;
 	 		}
 			
 		}
 		
	    
	    // verifica se precisa adicionar servico ATA
// 		if(qtd_planos_fone > 2 && qtd_planos_fone <= 5) {
// 			
// 			// busca a referencia do servico ata de telefone conforme a cidade
// 			String referenciaATAPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "SERV_ATA", dadosParametro.getIdPromocao(), dadosParametro.getFidelidade());
// 			
// 			listReferenciasProdutosObrigatoriosLimpo.add(referenciaATAPorCidade); // adiciona assinatura para cada linha de telefone
// 			
// 		} 		 		
	 		
	    // aqui adiciona todos os planos e servicos obrigatorios que serao retornados pela api
	    if(listReferenciasProdutosObrigatoriosLimpo.size() > 0) {
	    	
	    	while(y < listReferenciasProdutosObrigatoriosLimpo.size()) {
				
				System.out.println("obrigatorios limpo " + listReferenciasProdutosObrigatoriosLimpo.get(y));
				
				// aqui e feito consulta de cada plano para buscar seus dados e retornar a lista de todos os planos com seus respectivos valores e descontos
				Products produto = productsRep.buscaPlanoPorReferencia(listReferenciasProdutosObrigatoriosLimpo.get(y));	
				
				if(combo == 1) {
					desconto = produto.getDesc_single();
				}else if(combo == 2) {
					desconto = produto.getDesc_dual();
				}else if(combo == 3) {
					desconto = produto.getDesc_triple();
				}else {
					desconto = produto.getDesc_single();
					combo = 1;
				}
								
				produtosObrigatorios = new ProdutosObrigatorios();
				produtosObrigatorios.setCategoria(produto.getCategoria());
				
				produtosObrigatorios.setCod_totvs(produto.getCod_totvs());
				produtosObrigatorios.setDesconto(desconto);
				produtosObrigatorios.setDescricao(produto.getDescricao());
				produtosObrigatorios.setDownload(produto.getDownload());
				produtosObrigatorios.setFidelidade(produto.getFidelidade());
				produtosObrigatorios.setId_promocao(produto.getId_promocao());
				produtosObrigatorios.setQtd_mes_cob(produto.getQtd_mes_cob());
				produtosObrigatorios.setQtd_mes_ini(produto.getQtd_mes_ini());
				produtosObrigatorios.setReferencia(produto.getReferencia());
				produtosObrigatorios.setReferencia_obg(produto.getReferencia_obg());
				produtosObrigatorios.setReferencia_sva(produto.getReferencia_sva());
				produtosObrigatorios.setRepetivel(produto.getRepetivel());
				produtosObrigatorios.setTp_produto(produto.getTp_produto());
				produtosObrigatorios.setUpload(produto.getUpload());
				produtosObrigatorios.setValorCheio(produto.getValor());
				produtosObrigatorios.setVisivel(produto.getVisivel());
				produtosObrigatorios.setEquivalencia(produto.getEquivalencia());
				produtosObrigatorios.setValorMensal(String.format("%.2f", Float.parseFloat(produto.getValor()) - Float.parseFloat(desconto)));	
				
				// se for combo 2 ou combo 3 a assinatura ftth e isenta por 12 meses
//				if(combo != 1 && produto.getTp_produto().equals("ASS_FTTH")) {
//					produtosObrigatorios.setQtd_mes_ini("12");
//					produtosObrigatorios.setDesconto("0");
//					produtosObrigatorios.setValorMensal(produto.getValor());
//				}
				
				// se o plano de internet for de 500 megas, o servico de instalacao e isento, ou seja, 100% de desconto
//				if(qtd_net_500_megas > 0 && produto.getTp_produto().equals("INS_FTTH")) {
//					produtosObrigatorios.setDesconto(produto.getValor());
//					produtosObrigatorios.setValorMensal("0");	
//				}	
				
				if(produto.getTp_produto().equals("INS_PONTO_ADIC")) {
					produtosObrigatorios.setQtd_mes_cob("1");
				}
				
				
											
				// validacao para nao deixar contratar planos repetidos que nao podem ser repetidos
//				if(produtosObrigatorios.getRepetivel().equals("N")) {
//					
//					if(produto.getTp_produto().equals("NET")) {
//						qtd_planos_net++;
//						
//						// se o produto nao existir na lista entao e adicionado
//						if(!listPlanosResponse.contains(produtosObrigatorios) && qtd_planos_net <= 1) {
//							listPlanosResponse.add(produtosObrigatorios);
//						}else {
//							if(!produtosObrigatorios.getTp_produto().equals("SERV_ATA")) {
//								//System.out.println("Não é possivel inserir mais de um plano " + produtosObrigatorios.getDescricao() + " no mesmo contrato" );
//								msg_retorno = "Não é possivel inserir planos duplicados no mesmo contrato" ;	
//							}else {
//								listPlanosResponse.add(produtosObrigatorios);
//							}
//						}
//					}
//									
////					// se o produto nao existir na lista entao e adicionado
//					if(!listPlanosResponse.contains(produtosObrigatorios) && qtd_planos_net <= 1) {
//						listPlanosResponse.add(produtosObrigatorios);
//					}else {
//						if(!produtosObrigatorios.getTp_produto().equals("SERV_ATA")) {
//							//System.out.println("Não é possivel inserir mais de um plano " + produtosObrigatorios.getDescricao() + " no mesmo contrato" );
//							msg_retorno = "Não é possivel inserir planos duplicados no mesmo contrato" ;	
//						}else {
//							listPlanosResponse.add(produtosObrigatorios);
//						}
//					}
////					else {
////						//System.out.println("Não é possivel inserir mais de um plano " + produtosObrigatorios.getDescricao() + " no mesmo contrato" );
////						msg_retorno = "Não é possivel inserir planos duplicados no mesmo contrato" ;
////					}
//					
//					
//				}
				
				// validacao para nao deixar contratar planos repetidos que nao podem ser repetidos
				if(produtosObrigatorios.getRepetivel().equals("N")) {
					
					if(produto.getTp_produto().equals("NET")) {
						qtd_planos_net++;
					}
														
					// se o produto nao existir na lista entao e adicionado
					if(!listPlanosResponse.contains(produtosObrigatorios) && qtd_planos_net <= 1) {
					//if(!listPlanosResponse.contains(produtosObrigatorios)) {
						listPlanosResponse.add(produtosObrigatorios);
					}
					else {
						
						if(!produtosObrigatorios.getTp_produto().equals("SERV_ATA")) {
							//System.out.println("Não é possivel inserir mais de um plano " + produtosObrigatorios.getDescricao() + " no mesmo contrato" );
							msg_retorno = "Não é possivel inserir planos duplicados no mesmo contrato" ;	
						}else {
							listPlanosResponse.add(produtosObrigatorios);
						}
																
					}
					
				}else {
					
					// validacao para nao deixar contratar planos diferentes de telefone na mesma venda
					if(produtosObrigatorios.getTp_produto().equals("FONE")) {					
						
						qtd_planos_fone_validacao++;
						
						if(qtd_planos_fone_validacao <= 1) {
							listPlanosResponse.add(produtosObrigatorios);
						}else {
							if(listPlanosResponse.contains(produtosObrigatorios)) {
								listPlanosResponse.add(produtosObrigatorios);
							}else {
								
								listPlanosResponse = removePlanoArray(listPlanosResponse, "ASS_FTTH");								
								
								msg_retorno = "Não é possivel inserir planos diferentes de telefone no mesmo contrato" ;		
							}
						}
						
						
					}else {
						listPlanosResponse.add(produtosObrigatorios);
					}					
				}
				
				// aqui verifica se tem o ATA na lista de produtos, caso tenha é validado se realmente precisa do ATA, caso nao precise ele e removido
				if(produtosObrigatorios.getTp_produto().equals("SERV_ATA")) {					
										
					listPlanosResponse = validaServicoATA(listPlanosResponse);
				}	
				
				// nao deixa contratar pacotes a la carte senao tiver adicionado plano de tv
				if(qtd_pacote_tv > 0  && qtd_tv <= 0) {
					
					listPlanosResponse = removePlanoArray(listPlanosResponse, "PACOTE_TV");								
					
					msg_retorno = "Não é possivel contratar A LA CARTE sem um plano de TV" ;	
					
				}
								
				y++;
				
			}	
	    	
		    produtosObrigatoriosFluig.setCodigo("201");
		    produtosObrigatoriosFluig.setMensagem(msg_retorno);		    
		    produtosObrigatoriosFluig.setCombo(Integer.toString(combo));
		    produtosObrigatoriosFluig.setPlanos(listPlanosResponse);
		    	    	
	    }else {
	    	 produtosObrigatoriosFluig.setCodigo("404");
			 produtosObrigatoriosFluig.setMensagem("Nenhum registro encontrado");
			 produtosObrigatoriosFluig.setPlanos(listPlanosResponse);
	    }  	    
		
		return new ResponseEntity<ResponseProdutosObrigatoriosFluig>(produtosObrigatoriosFluig, HttpStatus.OK);
		
	}
	
	
	@ApiOperation(value = "Consulta dados tipo produto")
    @PostMapping(value = "/listProdutosPorTipo", produces = "application/json")
	public ResponseEntity<ResponseProdutoUnico> produtoUnico(@RequestBody PlanoParametroTipo dadosParametro) {
		
		ResponseProdutoUnico responseProdutoUnico = new ResponseProdutoUnico();
		String desconto = "";
		String valida_prod_repetido = "";
		
		int i = 0;
		int qtd_tv = 0;
		int controle_a_la_carte = 0;
		int qtd_pacotes_tv = 0;
				
		try {
			
			System.out.println("referencia produto principal: " + dadosParametro.getReferencia());
			System.out.println("equivalencia produto principal: " + dadosParametro.getEquivalencia());
			
			while(i < dadosParametro.getPlanosExistentes().size()) {
				
				System.out.println("referencia produto: " + dadosParametro.getPlanosExistentes().get(i).getReferencia_produto());
				System.out.println("equivalencia: " + dadosParametro.getPlanosExistentes().get(i).getEquivalencia());
				
				if(dadosParametro.getPlanosExistentes().get(i).getTp_plano().equals("TV")) {
					qtd_tv ++;
					
					System.out.println("referencia venda: " + dadosParametro.getPlanosExistentes().get(i).getReferencia_produto());
					
					if(dadosParametro.getPlanosExistentes().get(i).getReferencia_produto().indexOf("bronze") != -1 || dadosParametro.getPlanosExistentes().get(i).getReferencia_produto().indexOf("prata") != -1 || dadosParametro.getPlanosExistentes().get(i).getReferencia_produto().indexOf("ouro") != -1) {
						controle_a_la_carte++;
					}
					
					
				}
				if(dadosParametro.getPlanosExistentes().get(i).getTp_plano().equals("PACOTE_TV")) {
					qtd_pacotes_tv ++;
				}
				
				i++;
			}
			
			System.out.println("controle_a_la_carte venda " + controle_a_la_carte);
						
			// nao deixa contratar pacote a la carte sem ter um plano de tv
			if(qtd_pacotes_tv > 0 && controle_a_la_carte <= 0) {
				responseProdutoUnico.setCodigo("506");
				responseProdutoUnico.setMensagem("Para Contratar A LA CARTE é necessário ter ao menos um plano de TV Bronze");				
				
			}else {
				
				valida_prod_repetido = validaQtdPlanos(dadosParametro);
				
				if(valida_prod_repetido.equals("201")) {
					
					Products produto =  productsRep.buscaPlanoPorReferencia(dadosParametro.getReferencia());
					
					if(dadosParametro.getCombo().isEmpty() || dadosParametro.getCombo().equals(null) || dadosParametro.getCombo().equals("")) {
						desconto = produto.getDesc_single();
					}else if(dadosParametro.getCombo().equals("1")) {
						desconto = produto.getDesc_single();
					}else if(dadosParametro.getCombo().equals("2")) {
						desconto = produto.getDesc_dual();
					}else if(dadosParametro.getCombo().equals("3")) {
						desconto = produto.getDesc_triple();
					}
					
					responseProdutoUnico.setCategoria(produto.getCategoria());		
					responseProdutoUnico.setCod_totvs(produto.getCod_totvs());
					responseProdutoUnico.setDesconto(desconto);
					responseProdutoUnico.setDescricao(produto.getDescricao());
					responseProdutoUnico.setDownload(produto.getDownload());
					responseProdutoUnico.setFidelidade(produto.getFidelidade());
					responseProdutoUnico.setId_promocao(produto.getId_promocao());
					responseProdutoUnico.setQtd_mes_cob(produto.getQtd_mes_cob());
					responseProdutoUnico.setQtd_mes_ini(produto.getQtd_mes_ini());
					responseProdutoUnico.setReferencia(produto.getReferencia());
					responseProdutoUnico.setReferencia_obg(produto.getReferencia_obg());
					responseProdutoUnico.setReferencia_sva(produto.getReferencia_sva());
					responseProdutoUnico.setRepetivel(produto.getRepetivel());
					responseProdutoUnico.setTp_produto(produto.getTp_produto());
					responseProdutoUnico.setUpload(produto.getUpload());
					responseProdutoUnico.setValorCheio(produto.getValor());
					responseProdutoUnico.setVisivel(produto.getVisivel());
					responseProdutoUnico.setValorMensal(String.format("%.2f", Float.parseFloat(produto.getValor()) - Float.parseFloat(desconto)));	
					
					responseProdutoUnico.setCodigo("201");
					responseProdutoUnico.setMensagem("OK");
					
				}else {
					
					responseProdutoUnico.setCodigo("505");
					responseProdutoUnico.setMensagem("Não é permitido mais de uma quantidade desse plano");					
					
				}
				
			}			
			
		}catch(Exception e) {
			
			System.out.println("deu erro na funcao produtoUnico");
			e.printStackTrace();
			
			responseProdutoUnico.setCodigo("500");
			responseProdutoUnico.setMensagem("ERRO");
			
		}		
		
		return new ResponseEntity<ResponseProdutoUnico>(responseProdutoUnico, HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "Retorna plano correspondente, utilizado nos casos de renovação de planos")
    @PostMapping(value = "/planoCorrespondente", produces = "application/json")
	 public ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig> retornaPlanoCorrespondente(@Valid @RequestBody ParametrosPlanoCorrespondente dadosParametro) {
		
		daoProdutosSVA = new DaoProdutosSVA(productsRep, planosCorrespondentesRepository);
		
		return daoProdutosSVA.retornaPlanoCorrespondente(dadosParametro);
	}
	
	@ApiOperation(value = "Retorna planos com seus serviços obrigatorios processo renovacao-retenção")
    @PostMapping(value = "/obrigatoriosRenovacaoRetencao", produces = "application/json")
	 public ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig> retornaListaPlanosComObrigatorios(@Valid @RequestBody ParametrosPlanoCorrespondente dadosParametro) {
		
		daoProdutosSVA = new DaoProdutosSVA(productsRep, planosCorrespondentesRepository);
		
		return daoProdutosSVA.servicosObrigatoriosRenovacao(dadosParametro);
	}
	
	@ApiOperation(value = "Retorna o tipo do plano")
    @PostMapping(value = "/tipoPlano", produces = "application/json")
	 public ResponseEntity<ResponseTpPlano> retornaTipoPlano(@Valid @RequestBody ParametroTpPLano dadosParametro) {
		
		ResponseTpPlano responseTpPlano = new ResponseTpPlano();
		
		String retorno = planosCorrespondentesRepository.retornaTipoPlano(dadosParametro.getCod_plano(), dadosParametro.getQtd_pontos_adicionais());
		
		responseTpPlano.setTp_plano(retorno);
		
		return new ResponseEntity<ResponseTpPlano>(responseTpPlano, HttpStatus.OK);
	}
	
		
	// funcao que verifica se tem plano repetido, para evitar, por exemplo, que seja adiciona mais de um sva igual
	public String validaQtdPlanos(PlanoParametroTipo dados) {
		
		System.out.println("entrou funcao validaQtdPlanos");
		
		List<PlanoParametroReferencia> list = dados.getPlanosExistentes();
		String retorno = "201";
		
		int qtd_equivalencia = 0;
		int qtd_referencia = 0;
		int qtd_net = 0;
		
		int i = 0; // contador lacao repeticao
		
		if (dados.getRepetivel().equals("N")) {

			while (i < list.size()) {

				if (dados.getEquivalencia().equals(list.get(i).getReferencia_produto())) {
					qtd_equivalencia++;
				}

				if (dados.getReferencia().equals(list.get(i).getReferencia_produto())) {
					qtd_referencia++;
				}				

				i++;
			}
		}
				
		if(qtd_equivalencia + qtd_referencia  > 1 ) {
			retorno = "505";
		}
		
		
		return retorno;
	}
	
	// essa funcao recebe a lista de planos que contem o plano a ser removido, e tbm recebe como parametro o plano que deseja que seja removido do array
	public List<ProdutosObrigatorios> removePlanoArray(List<ProdutosObrigatorios> listPlanosParametro, String tpPlano) {
		
		List<ProdutosObrigatorios> listPlanosResponse = new ArrayList<>();
		
		int i = 0;
		int contRemocao = 0; // controle para remover somente um item do array e nao todos que tenham aquela referencia
				
		while(i < listPlanosParametro.size()) {
						
			if(listPlanosParametro.get(i).getTp_produto().equals(tpPlano) && contRemocao <= 0) {
				
				contRemocao++;					
				
			}else {
				listPlanosResponse.add(listPlanosParametro.get(i));
			}
			
			i++;
		}
		
		return listPlanosResponse;
	}
	
	// essa funcao recebe a lista de planos e valida se o servico ATA realmente deve existir nessa lista, caso nao precise do ATA entao ele e removido
	public List<ProdutosObrigatorios> validaServicoATA(List<ProdutosObrigatorios> listPlanosParametro) {
		
		List<ProdutosObrigatorios> listPlanosResponse = new ArrayList<>();
		
		int i = 0;
		int contRemocao = 0; // controle para remover somente um item do array e nao todos que tenham aquela referencia
		int contFone = 0;
		int contATA = 0;
				
		while(i < listPlanosParametro.size()) {
						
			if(listPlanosParametro.get(i).getTp_produto().equals("FONE") && contRemocao <= 0) {
				
				contFone++;			
				
			}
			
			if(listPlanosParametro.get(i).getTp_produto().equals("SERV_ATA") && contRemocao <= 0) {
				
				contATA++;			
				
			}
			
			i++;
		}
		
		int qtd_ata_validacao=(int)contFone/2;
		int h = 0;
		
		System.out.println("qtd_ata_validacao: " + qtd_ata_validacao + " contATA: " + contATA);
		
		if(contATA > qtd_ata_validacao) {
			
			while(h < contATA - qtd_ata_validacao) {
				
				listPlanosResponse = removePlanoArray(listPlanosParametro, "SERV_ATA");
				
				h++;
			}
			
		}else {
			listPlanosResponse = listPlanosParametro;
		}
		
		// nesse caso o ATA deve ser removido
//		if(contFone < 3 && contATA > 0) {
//			
//			listPlanosResponse = removePlanoArray(listPlanosParametro, "SERV_ATA");
//			
//		}else {
//			listPlanosResponse = listPlanosParametro;
//		}
		
		return listPlanosResponse;
	}	
			
	
//	@ApiOperation(value = "Compara lista de OS")
//    @GetMapping(value = "/comparaListaDeOS", produces = "application/json")
//	public void comparaListaDeOS() {
//		
//		try {
//			Reader reader = Files.newBufferedReader(Paths.get("C:\\Users\\Robson\\Desktop\\revisao os iclass\\LIST_PRODUTOS.csv"));
//			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(0).build();
//			
//			Produto produto;
//			Produto produtoFinal;
//			List<Produto> produtos = new ArrayList<>();
//			Set<Produto> setListaFinal = new HashSet<Produto>();
//			
//			List<String[]> linhas = csvReader.readAll();
//			
//			for (String[] linha : linhas) {
//
//				 produto = new Produto();
//				// produto.setArmazem(linha[0].toString());
//				 produto.setCodigo(linha[0].toString());
//				 produto.setDescricao(linha[1].toString());
//				 produto.setQtd(linha[2].toString());
//				 
//				 produtos.add(produto);
//			}	
//			
//			int i = 0;
//			int x = 0;
//			int qtd = 0;
//			
//			
//			// pegar a quantidade de cada produto
//			while(i < produtos.size()) {
//				
//				x = 0;
//				qtd = 0;
//				
//				while(x < produtos.size()) {
//					
//					if(produtos.get(i).getCodigo().equals(produtos.get(x).getCodigo())) {
//						qtd += Integer.parseInt(produtos.get(x).getQtd());
//					}
//					
//					x++;
//				}
//				
//				produtoFinal = new Produto();
//				produtoFinal.setArmazem(produtos.get(i).getArmazem());
//				produtoFinal.setCodigo(produtos.get(i).getCodigo());
//				produtoFinal.setDescricao(produtos.get(i).getDescricao());
//				produtoFinal.setQtd(String.valueOf(qtd));
//				
//				setListaFinal.add(produtoFinal);			
//				
//				i++;
//			}			
//						
//			// listagem final
//			for(Produto p: setListaFinal) {
//				
//				System.out.println("produto: " + p.getCodigo() + " quantidade: " + p.getQtd());				
//				
//			}										
//			
//		} catch (IOException e) {
//			
//			e.printStackTrace();
//		}
//
//	}	
	
//	@ApiOperation(value = "Compara lista de OS")
//	@GetMapping(value = "/comparaListaDeOS", produces = "application/json")
//	public void comparaListaDeOS() {
		
//		
//		OrdemServico ordemServico;
//		List<OrdemServico> listOSProblema = new ArrayList<>();
//		List<OrdemServico> listOSTotvs = new ArrayList<>();
//		
//		List<OrdemServico> listOSFinal = new ArrayList<>();
//
//		try {
//			Reader reader = Files.newBufferedReader(Paths.get("C:\\Users\\Robson\\Desktop\\OS COM PROBLEMA\\OS EM ABERTO ICLASS.csv"));
//			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(0).build();
//
//			List<String[]> linhas = csvReader.readAll();
//
//			for (String[] linha : linhas) {
//				
//				ordemServico = new OrdemServico();
//				ordemServico.setNumeroOS(linha[0].toString().substring(0,6));
//				
//				//System.out.println("OS: " + linha[0].toString().substring(0,6));
//				
//				listOSProblema.add(ordemServico);
//
//			}
//			
//			// OS nao encerradas totvs
//			
//			Reader reader2 = Files.newBufferedReader(Paths.get("C:\\Users\\Robson\\Desktop\\OS COM PROBLEMA\\OS DO TOTVS SEM ENCERRAR.csv"));
//			CSVReader csvReader2 = new CSVReaderBuilder(reader2).withSkipLines(0).build();
//
//			List<String[]> linhas2 = csvReader2.readAll();
//
//			for (String[] linha : linhas2) {
//				
//				ordemServico = new OrdemServico();
//				ordemServico.setNumeroOS(linha[0].toString());
//				
//				if(!listOSProblema.contains(ordemServico)) {
//					System.out.println(linha[0].toString()+",");
//				}
//
//			}			
			

//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}

//	}

	
	
	@ApiOperation(value = "Consulta todos os produtos do banco products")
    @PostMapping(value = "/listaTodosProdutos", produces = "application/json")
	 public ResponseEntity<ResponseProdutosFluig> listtotalProdutos(@RequestBody ListaParametrosTotal dadosParametro) {
		
		DaoProdutosSVA prodSva = new DaoProdutosSVA(productsRep, planosCorrespondentesRepository);	
		
		ResponseProdutosFluig responseProdutosFluig = new ResponseProdutosFluig();
		
		responseProdutosFluig = prodSva.verificaParametrosTotal(dadosParametro);
		
		if(responseProdutosFluig.getCodigo() == 200) {
		
			try {	
				responseProdutosFluig = prodSva.retornaTodosProdutos(dadosParametro);
			} catch (Exception e) {

				System.out.println("deu erro, " + e.getMessage());
				e.printStackTrace();
				
				responseProdutosFluig.setCodigo(500);
				responseProdutosFluig.setMensagem(e.getMessage());
				responseProdutosFluig.setProdutos(null);
			}			
		}
		
		return new ResponseEntity<ResponseProdutosFluig>(responseProdutosFluig, HttpStatus.OK);
	}
	
}
