package api.rest.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;
import java.util.Optional;

import javax.persistence.LockModeType;
import javax.print.attribute.standard.Media;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;
import api.rest.repositoy.NumeroRepository;
import api.rest.repositoy.TitularidadeRepository;
import api.rest.response.ResponseDaoLigueApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/numero")
@Api(value = "API RESERVA DE NUMEROS")
public class ControllerNumero {
	
	@Autowired
	private NumeroRepository numeroRepository;
	
	@Autowired
	private TitularidadeRepository titularidadeRepository;
			
// PRODUCAO V1	
//	@ApiOperation(value = "Realiza Reserva/Liberacao dos Numeros")	
//	@PutMapping(value = "/alterar", produces = "application/json") 	
//	@Lock(LockModeType.READ)
//	public String alterar(@RequestParam (value = "cd_titular") int cd_titular, @RequestParam (value = "fone") String fone, @RequestParam (value = "status") String status) {						
//				
//		System.out.println("chamou funcao de liberar/reservar numero");
//		
//		try {
//			
//			Optional<Titularidade> titularidade = titularidadeRepository.findById(cd_titular);// busca dados titular cadastrado
//			
//			Numero numero = new Numero();
//			int cd_titularRetornado = 0;
//			
//			Numero numeroCarregado = numeroRepository.buscaPorNumero(fone); // consulta dados do numero pelo numero do telefone
//			numero = numeroCarregado;
//			
//			System.out.println("status do fone: " + numero.getStatus());	
//			
//			// se o status do numero for tiver como reservado, e o status passado pro parametro tbm for pra reservar numero, faz entao uma 
//			// consulta pra ver se o titular q possui a reserva e o mesmo q ta solicitando a reserva novamente
//			if (numero.getStatus().equals("R") && status.equals("R")) {
//				cd_titularRetornado = numeroCarregado.getTitular().getCd_titular();
//			}		
//					
//			System.out.println("TITULAR retornado: " + cd_titularRetornado);		
//			System.out.println("TITULAR parametro: " + cd_titular);		
//			
//			// se o numero ja estiver reservado para outro titular
//			if (numero.getStatus().equals("R") && status.equals("R") && cd_titularRetornado != cd_titular) {
//				
//				System.out.println("NUMERO JA RESERVADO");
//				
//				return "RESERVADO";
//				
//			}else {
//				
//				if (!numero.getStatus().equals("A")) {
//					
//					// seta campos a serem alterados
//					numero.setNumero(fone);
//					numero.setStatus(status);
//										
//					if (status.equals("L")) {
//						numero.setTitular(null); // limpa o titular do cadastro de numero
//						//System.out.println("entrou no L");
//					}else if (status.equals("R")){
//						numero.setTitular(titularidade.get()); // vincula numero ao titular
//						//System.out.println("entrou no R");
//					}					
//										
//					numeroRepository.save(numero);  // faz as alteracoes na tabela de numeros
//					
//				}
//				
//				return "OK";
//				
//			}
//			
//			
//		}catch(Exception e) {
//			
//			System.out.println("ERRO RESERVA/LIBERACAO DE NUMERO: " + e);
//			return "ERRO";
//			
//		}
//		
//				
//	}
	
	// PRODUCAO V2
	@ApiOperation(value = "Realiza Reserva/Liberacao dos Numeros")	
	@PutMapping(value = "/alterar", produces = "application/json") 	
	@Lock(LockModeType.READ)
	public String alterar(@RequestParam (value = "cd_titular") int cd_titular, @RequestParam (value = "fone") String fone, @RequestParam (value = "status") String status) {						
				
		System.out.println("chamou funcao de liberar/reservar numero: " + fone);
						
		
		try {			
			
			Optional<Titularidade> titularidade = titularidadeRepository.findById(cd_titular);// busca dados titular cadastrado
			
			Numero numero = new Numero();
			
			int cd_titularRetornado = 0;
			int i = 0;
			int j = 0;
			int cont_reserva = 0;
			
			String[] arrayfones = fone.split(",");
			Numero numeroCarregado = null;
			
			System.out.println("qtd array: " + arrayfones.length);
			
			//laco para ver se algum dos numeros esta reservado
			while (i < arrayfones.length) {
				
				numeroCarregado = numeroRepository.buscaPorNumero(arrayfones[i]); // consulta dados do numero pelo numero do telefone				
				
				// se o status do numero for tiver como reservado, e o status passado pro parametro tbm for pra reservar numero, faz entao uma 
				// consulta pra ver se o titular q possui a reserva e o mesmo q ta solicitando a reserva novamente
				if (numeroCarregado.getStatus().equals("R") && status.equals("R")) {
					cd_titularRetornado = numeroCarregado.getTitular().getCd_titular();
				}	
				
			//	System.out.println("TITULAR retornado: " + cd_titularRetornado);		
			//	System.out.println("TITULAR parametro: " + cd_titular);	
				
			//	System.out.println("status do fone: " + numeroCarregado.getStatus());
			//	System.out.println("numero do array: " + arrayfones[i]);
				
				if (numeroCarregado.getStatus().equals("R") && status.equals("R") && cd_titularRetornado != cd_titular) {
					
					cont_reserva++;	
					
				}
				
				i++;
			}
			
			if (cont_reserva > 0) {
				
				return "RESERVADO";
				
			}else { // se todos os numeros estao disponiveis entao faz a reserva logo abaixo
				
				while (j < arrayfones.length) {
					
				//	System.out.println("entrou no segundo laco");
					System.out.println("fone no segundo laco: '" + arrayfones[j] + "'");
					
					numeroCarregado = numeroRepository.buscaPorNumero(arrayfones[j]); 
					numero = numeroCarregado;
					
					if (!numero.getStatus().equals("A")) {																	
						
						if (status.equals("L") && numeroCarregado.getTitular().getCd_titular() == cd_titular) { // valida para liberar fone somente se o titular do fone for igual ao titular passado por parametro
							
							System.out.println("entrou no if e fez a liberacao");
							System.out.println("titular parametro: " + cd_titular);
							System.out.println("titular do numero que vai ser liberado: " + numeroCarregado.getTitular().getCd_titular());
							
							// seta campos a serem alterados
							numero.setNumero(arrayfones[j]);
							numero.setStatus(status);
							
							numero.setTitular(null); // limpa o titular do cadastro de numero
							numeroRepository.save(numero);  // faz as alteracoes na tabela de numeros
							//System.out.println("entrou no L");
						}else if (status.equals("R")){
							// seta campos a serem alterados
							numero.setNumero(arrayfones[j]);
							numero.setStatus(status);
							numero.setTitular(titularidade.get()); // vincula numero ao titular
							numeroRepository.save(numero);  // faz as alteracoes na tabela de numeros
							//System.out.println("entrou no R");
						}				
						
						
					}
					
					
					j++;
				}
				
				return "OK";
				
			}
			
			
		}catch(Exception e) {
			
			System.out.println("ERRO RESERVA/LIBERACAO DE NUMERO: " + e);
			return "ERRO";
			
		}	
		
				
	}
	
	@ApiOperation(value = "Consulta se o numero esta liberado para uso")	
	@GetMapping(value = "/consulta", produces = "application/json")
	public String consultaNumeroLiberado(@RequestParam (value = "cd_titular") int cd_titular, @RequestParam (value = "fone") String fone, @RequestParam (value = "status") String status) {
		
		try {
			
			Optional<Titularidade> titularidade = titularidadeRepository.findById(cd_titular);// busca dados titular cadastrado
			
			Numero numero = new Numero();
			
			int cd_titularRetornado = 0;
			int i = 0;
			int j = 0;
			int cont_reserva = 0;
			
			String[] arrayfones = fone.split(",");
			Numero numeroCarregado = null;
			
			System.out.println("qtd array: " + arrayfones.length);
			System.out.println("numero de busca: " + arrayfones[0]);
			
			//laco para ver se algum dos numeros esta reservado
			while (i < arrayfones.length) {
				
				System.out.println("Numero que sera pesquisado: " + arrayfones[i]);
				numeroCarregado = numeroRepository.buscaPorNumero(arrayfones[i]); // consulta dados do numero pelo numero do telefone				
				System.out.println("numeroCarregado: " + numeroCarregado.getNumero());

				// se o status do numero for tiver como reservado, e o status passado pro parametro tbm for pra reservar numero, faz entao uma 
				// consulta pra ver se o titular q possui a reserva e o mesmo q ta solicitando a reserva novamente
				if (numeroCarregado.getStatus().equals("R") && status.equals("R")) {
					cd_titularRetornado = numeroCarregado.getTitular().getCd_titular();
				}	
				
				System.out.println("TITULAR retornado: " + cd_titularRetornado);		
				System.out.println("TITULAR parametro: " + cd_titular);	
//				
//				System.out.println("status do fone: " + numeroCarregado.getStatus());
//				System.out.println("numero do array: " + arrayfones[i]);
				
				if (numeroCarregado.getStatus().equals("R") && status.equals("R") && cd_titularRetornado != cd_titular) {
					
					cont_reserva++;	
					
				}
				
				i++;
			}
			
			if (cont_reserva > 0) {
				
				return "RESERVADO";
				
			}else {
				return "OK";
			}			
			
		}catch(Exception e) {
			return "ERRO";
		}
			
	}
	
	@ApiOperation(value = "Altera Numero")
	@PutMapping(value = "/altera", produces = "application/json")	
	public ResponseEntity<ResponseDaoLigueApi> alterarIclass(@RequestParam (value = "jsonNumero") String jsonNumero) {			
				
		System.out.println("entrou funcao alterar numero alteraIclass");
		ResponseDaoLigueApi responseDaoLigueApi;
		
		try {
			
			Numero numero = new Gson().fromJson(jsonNumero, Numero.class);			
			
			Numero numeroCadastrado = numeroRepository.save(numero);
			
			responseDaoLigueApi = new ResponseDaoLigueApi();
			responseDaoLigueApi.setCODCLI("" + numeroCadastrado.getId());
			responseDaoLigueApi.setMSGERRO("Numero Cadastrado com Sucesso");
			responseDaoLigueApi.setCODERRO("201");
			
		}catch(Exception e) {
			responseDaoLigueApi = new ResponseDaoLigueApi();	
			responseDaoLigueApi.setCODCLI("");
			responseDaoLigueApi.setMSGERRO(e.getMessage());
			responseDaoLigueApi.setCODERRO(e.getCause().toString());
		}		
						
		return new ResponseEntity<ResponseDaoLigueApi>(responseDaoLigueApi, HttpStatus.OK);		
				
	}
	
	
	/*
	 * public ResponseEntity alterar(@RequestParam (value = "cd_titular") int cd_titular, @RequestParam (value = "fone") String fone, @RequestParam (value = "status") String status) {						
				
		System.out.println("chamou funcao de liberar/reservar numero");
		
		try {
			
			Optional<Titularidade> titularidade = titularidadeRepository.findById(cd_titular);// busca dados titular cadastrado
			
			Numero numero = new Numero();
			
			Numero numeroCarregado = numeroRepository.buscaPorNumero(fone); // consulta dados do numero pelo numero do telefone
			numero = numeroCarregado;
			
			System.out.println("status do fone: " + numero.getStatus());
			
			if (!numero.getStatus().equals("A")) {
				
				// seta campos a serem alterados
				numero.setNumero(fone);
				numero.setStatus(status);
									
				if (status.equals("L")) {
					numero.setTitular(null); // limpa o titular do cadastro de numero
					//System.out.println("entrou no L");
				}else if (status.equals("R")){
					numero.setTitular(titularidade.get()); // vincula numero ao titular
					//System.out.println("entrou no R");
				}
				
				numeroRepository.save(numero);  // faz as alteracoes na tabela de numeros
				
			}
			
			
			return new ResponseEntity(HttpStatus.OK);
			
		}catch(Exception e) {
			
			System.out.println("ERRO RESERVA/LIBERACAO DE NUMERO: " + e.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
			
		}
				
	}
	 */
//	@ApiOperation(value = "Consulta se o numero esta liberado para uso")	
//	@GetMapping(value = "/{fone}", produces = "application/json")
//	public String consultaNumeroLiberado(@PathVariable (value = "fone") String fone) {
//		
//		try {
//			
//			Numero numero = numeroRepository.buscaPorNumero(fone);
//			
//			System.out.println("retorno status numero: " + numero.getStatus());
//			
//			if (numero != null) {
//				return numero.getStatus();
//			}else {
//				return "ERRO";
//			}
//			
//			
//		}catch(Exception e) {
//			return "ERRO";
//		}
//			
//	}
	
//	@ApiOperation(value = "Consulta Numeros")	
//	@GetMapping(value = "/", produces = "application/json")
//	public ResponseEntity<List<Numero>> numeros(){
//		
//		List<Numero> list = (List<Numero>) numeroRepository.findAll();
//		
//		return new ResponseEntity<List<Numero>>(list, HttpStatus.OK);
//	}

}
