package api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.rest.model.integrador.CacheSerasa;
import api.rest.repositoy.CacheSerasaRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/serasa")
@Api(value = "API REST CADASTRO CACHE")
public class ControllerCacheSerasa {

    @Autowired
    private CacheSerasaRepository cacheRepository;

    @ApiOperation(value = "Cadastra uma entrada no Cache")
    @PostMapping(value = "/cadastrar", produces = "application/json")
    public ResponseEntity<CacheSerasa> cadastrar(@RequestParam List<String> data) {

        String[] array = data.toArray(new String[data.size()]);

        CacheSerasa cacheSerasa = new CacheSerasa();

        // Novos campos
        cacheSerasa.setAtendente(array[0]);
        cacheSerasa.setCache_hit(array[1]);
        cacheSerasa.setCelular_contato(array[2]);
        cacheSerasa.setConfirmado(array[3]);
        cacheSerasa.setCpf_cnpj(array[4]);
        cacheSerasa.setData_busca_cache(array[5]);
        cacheSerasa.setData_busca_serasa(array[6]);
        cacheSerasa.setEmail(array[7]);
        cacheSerasa.setEndereco(array[8]);
        cacheSerasa.setHora(array[9]);
        cacheSerasa.setNome(array[10]);
        cacheSerasa.setObservacao(array[11]);
        cacheSerasa.setProcesso(array[12]);
        cacheSerasa.setSituacao_documento(array[13]);
        cacheSerasa.setStatus(array[14]);
        cacheSerasa.setTelefone_contato(array[15]);
        cacheSerasa.setTotal_cheques(array[16]);
        cacheSerasa.setTotal_divida(array[17]);
        cacheSerasa.setTotal_ocorrencias(array[18]);
        cacheSerasa.setTotal_origens(array[19]);
        cacheSerasa.setTotal_protestos(array[20]);
        cacheSerasa.setVariaveis_reprovacao(array[21]);

        CacheSerasa csSalvo = cacheRepository.save(cacheSerasa);

        return new ResponseEntity<CacheSerasa>(csSalvo, HttpStatus.OK);
    }

    @ApiOperation(value = "Altera o cache")
    @PutMapping(value = "/alterar", produces = "application/json")
    public ResponseEntity<CacheSerasa> alterar(@RequestParam List<String> data) {

        CacheSerasa cacheSerasa = new CacheSerasa();

        String[] array = data.toArray(new String[data.size()]);

        cacheSerasa.setCd_cache_serasa(Integer.parseInt(array[22]));
        cacheSerasa.setNr_solicitacao(array[23]);

        // Novos campos
        cacheSerasa.setAtendente(array[0]);
        cacheSerasa.setCache_hit(array[1]);
        cacheSerasa.setCelular_contato(array[2]);
        cacheSerasa.setConfirmado(array[3]);
        cacheSerasa.setCpf_cnpj(array[4]);
        cacheSerasa.setData_busca_cache(array[5]);
        cacheSerasa.setData_busca_serasa(array[6]);
        cacheSerasa.setEmail(array[7]);
        cacheSerasa.setEndereco(array[8]);
        cacheSerasa.setHora(array[9]);
        cacheSerasa.setNome(array[10]);
        cacheSerasa.setObservacao(array[11]);
        cacheSerasa.setProcesso(array[12]);
        cacheSerasa.setSituacao_documento(array[13]);
        cacheSerasa.setStatus(array[14]);
        cacheSerasa.setTelefone_contato(array[15]);
        cacheSerasa.setTotal_cheques(array[16]);
        cacheSerasa.setTotal_divida(array[17]);
        cacheSerasa.setTotal_ocorrencias(array[18]);
        cacheSerasa.setTotal_origens(array[19]);
        cacheSerasa.setTotal_protestos(array[20]);
        cacheSerasa.setVariaveis_reprovacao(array[21]);

        CacheSerasa csSalvo = cacheRepository.save(cacheSerasa);

        return new ResponseEntity<CacheSerasa>(csSalvo, HttpStatus.OK);

    }

    @ApiOperation(value = "Consulta titular pelo cpf ou cnpj")
    @GetMapping(value = "/buscaPorDoc", produces = "application/json")
    public ResponseEntity<List<CacheSerasa>> cachePorcpf(@RequestParam("cpf_cnpj") String cpf_cnpj) {

        List<CacheSerasa> list = null;

        try {
            list = (List<CacheSerasa>) cacheRepository.buscaPorCpfCnpj(cpf_cnpj);

        } catch (Exception e) {
            // TODO: handle exception
        }

        return new ResponseEntity<List<CacheSerasa>>(list, HttpStatus.OK);
    }

}
