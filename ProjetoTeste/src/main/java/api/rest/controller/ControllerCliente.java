package api.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import api.rest.model.integrador.Cliente;
import api.rest.repositoy.ClienteRepository;
import api.rest.response.ResponseDaoLigueApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/cliente")
@Api(value = "API REST CADASTRO CLIENTE")
public class ControllerCliente {

    @Autowired
    private ClienteRepository clienteRepository;

    @ApiOperation(value = "Realiza Cadastro do Cliente")
    @PostMapping(value = "/cadastrar", produces = "application/json")
    public ResponseEntity<ResponseDaoLigueApi> cadastrar(@RequestParam(value = "jsonCliente") String jsonCliente) {

        System.out.println("entrou funcao cadastrar cliente");
        ResponseDaoLigueApi responseCliente;

        try {
            Cliente cliente = new Gson().fromJson(jsonCliente, Cliente.class);

            System.out.println("cliente: " + cliente.getCd_totvs());

            // Cliente clienteCadastrado = clienteRepository.save(cliente);

            responseCliente = new ResponseDaoLigueApi();
            // responseCliente.setCd_cliente(clienteCadastrado.getCd_cliente().toString());
            responseCliente.setMSGERRO("Cliente Cadastrado com Sucesso");
            responseCliente.setCODERRO("201");

        } catch (Exception e) {
            responseCliente = new ResponseDaoLigueApi();
            responseCliente.setCODCLI("");
            responseCliente.setMSGERRO(e.getMessage());
            responseCliente.setCODERRO(e.getCause().toString());
        }

        return new ResponseEntity<ResponseDaoLigueApi>(responseCliente, HttpStatus.OK);

    }

}
