package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;


@Repository
public interface NumeroRepository extends CrudRepository<Numero, Long> {
	
	@Query(value = "select n from Numero n where n.numero = ?1")
	public Numero buscaPorNumero(String fone);	
	
	@Query(value = "SELECT * FROM telefonia.numero WHERE id = (SELECT MIN(id) FROM telefonia.numero WHERE area = ?1 and status = 'L')", nativeQuery = true)
	public Numero buscaPorArea(String area);
	
	//@Query(value = "SELECT * FROM telefonia.numero WHERE numero = ?1 and status in ('L', 'R')", nativeQuery = true)
	@Query(value = "SELECT * FROM telefonia.numero WHERE numero = ?1 and status in ('L', 'R')", nativeQuery = true)
	public Numero buscaIclassNumero(String fone);
}
