package api.rest.repositoy;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.integrador.IclassCabecalho;

@Repository
public interface IclassCabecalhoRepository extends CrudRepository<IclassCabecalho, Integer> {
	
	@Transactional
	@Modifying
	@Query(value = "update integrador.iclass_cabecalho set nr_solicitacao = ?1, fluxo = ?2, status = ?3 where numos = ?4", nativeQuery = true)
	public void atualizaStatus(String nr_solicitacao, String fluxo, String status, String nr_os);	
	
}
