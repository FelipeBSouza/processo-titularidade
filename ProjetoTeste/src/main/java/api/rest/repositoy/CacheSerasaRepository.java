package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.CacheSerasa;

@Repository
public interface CacheSerasaRepository extends CrudRepository<CacheSerasa, Integer> {

	@Query(value = "select * from integrador.cache_serasa cs where cs.cpf_cnpj = ?1 order by cd_cache_serasa DESC", nativeQuery = true)
	public List<CacheSerasa> buscaPorCpfCnpj(String cpf_cnpj);
}