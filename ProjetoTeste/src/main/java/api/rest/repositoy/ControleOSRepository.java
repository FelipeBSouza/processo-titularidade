package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.ControleOS;
import api.rest.model.integrador.Funcionario;
import api.rest.model.integrador.TabInternet;


@Repository
public interface ControleOSRepository extends CrudRepository<ControleOS, Long> {
	
	@Query(value = "SELECT * FROM integrador.controle_os WHERE nr_os_sequencial = ?1", nativeQuery = true)
	public List<ControleOS> consultaSeExisteOS(String nr_os_sequencial);	
	
	@Query(value = "SELECT * FROM integrador.controle_os WHERE nr_os = ?1 and sequencial = (select max(sequencial) from integrador.controle_os WHERE nr_os = ?1)", nativeQuery = true)
	public List<ControleOS> consultaSequencialAtualOS(String nr_os);
	
	@Query(value = "SELECT * FROM integrador.controle_os WHERE nr_os = ?1 and id = (select max(id) from integrador.controle_os where nr_os = ?1)", nativeQuery = true)
	public List<ControleOS> proximaSequencia(String nr_os);	
}
