package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.Funcionario;
import api.rest.model.integrador.TabInternet;


@Repository
public interface FuncionarioRepository extends CrudRepository<Funcionario, Long> {
	
	@Query(value = "SELECT * FROM integrador.funcionario WHERE cd_funcionario = ?1", nativeQuery = true)
	//@Query(value = "select f from Funcionario f where f.cd_funcionario =?1")
	public Funcionario consultaFuncionarioCodigo(int cod_funcionario);		
}
