package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;

@Repository
public interface TitularidadeRepository extends CrudRepository<Titularidade, Integer> {
	
	@Query(value = "select n from Titularidade n where n.nr_cpf_cnpj = ?1")  
	public List<Titularidade> buscaPorCpfCnpj(String cpf_cnpj);	

}
