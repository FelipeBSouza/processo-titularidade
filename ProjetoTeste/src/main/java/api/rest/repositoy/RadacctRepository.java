package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.internet.Radacct;

@Repository
public interface RadacctRepository extends CrudRepository<Radacct, Long> {
	
	@Query(value = "SELECT * FROM internet.radacct WHERE username = ?1 order by username", nativeQuery = true)
	public List<Radacct> buscaPorUserName(String username);	
	
}
