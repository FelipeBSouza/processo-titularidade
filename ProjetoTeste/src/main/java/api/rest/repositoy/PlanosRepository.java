package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.TipoFinalidade;
import api.rest.model.integrador.fibra.PortaSplitter;
import api.rest.model.telefonia.Planos;


@Repository
public interface PlanosRepository extends CrudRepository<Planos, Long> {
	
	@Query(value = "SELECT * FROM telefonia.planos WHERE id = ?1", nativeQuery = true)
	public Planos consultaPlanoId(int id_plano);		
}
