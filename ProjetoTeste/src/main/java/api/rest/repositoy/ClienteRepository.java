package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.Funcionario;
import api.rest.model.integrador.TabInternet;


@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long> {
	
}
