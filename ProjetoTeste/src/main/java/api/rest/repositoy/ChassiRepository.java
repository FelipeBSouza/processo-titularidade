package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.Chassi;
import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;

@Repository
public interface ChassiRepository extends CrudRepository<Chassi, Integer> {
	
	@Query(value = "SELECT * FROM integrador.chassi", nativeQuery = true)  
	public List<Chassi> consultaChassi();	

}
