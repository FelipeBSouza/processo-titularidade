package api.rest.response;

public class ResponseProdutoUnico {	
	
	private String codigo;
	private String mensagem;
	
	private String cod_totvs;
    private String referencia;
    private String descricao;
    private String valorCheio;
    private String valorMensal;
    private String desconto;
    private String qtd_mes_ini;
    private String qtd_mes_cob;
    private String referencia_obg;
    private String referencia_sva;
    private String tp_produto;   
    private String visivel;
    private String categoria;
    private String repetivel;
    private String download;
    private String upload;
    private String id_promocao;
    private String fidelidade;
    
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getCod_totvs() {
		return cod_totvs;
	}
	public void setCod_totvs(String cod_totvs) {
		this.cod_totvs = cod_totvs;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getValorCheio() {
		return valorCheio;
	}
	public void setValorCheio(String valorCheio) {
		this.valorCheio = valorCheio;
	}
	public String getValorMensal() {
		return valorMensal;
	}
	public void setValorMensal(String valorMensal) {
		this.valorMensal = valorMensal;
	}
	public String getDesconto() {
		return desconto;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	public String getQtd_mes_ini() {
		return qtd_mes_ini;
	}
	public void setQtd_mes_ini(String qtd_mes_ini) {
		this.qtd_mes_ini = qtd_mes_ini;
	}
	public String getQtd_mes_cob() {
		return qtd_mes_cob;
	}
	public void setQtd_mes_cob(String qtd_mes_cob) {
		this.qtd_mes_cob = qtd_mes_cob;
	}
	public String getReferencia_obg() {
		return referencia_obg;
	}
	public void setReferencia_obg(String referencia_obg) {
		this.referencia_obg = referencia_obg;
	}
	public String getReferencia_sva() {
		return referencia_sva;
	}
	public void setReferencia_sva(String referencia_sva) {
		this.referencia_sva = referencia_sva;
	}
	public String getTp_produto() {
		return tp_produto;
	}
	public void setTp_produto(String tp_produto) {
		this.tp_produto = tp_produto;
	}
	public String getVisivel() {
		return visivel;
	}
	public void setVisivel(String visivel) {
		this.visivel = visivel;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getRepetivel() {
		return repetivel;
	}
	public void setRepetivel(String repetivel) {
		this.repetivel = repetivel;
	}
	public String getDownload() {
		return download;
	}
	public void setDownload(String download) {
		this.download = download;
	}
	public String getUpload() {
		return upload;
	}
	public void setUpload(String upload) {
		this.upload = upload;
	}
	public String getId_promocao() {
		return id_promocao;
	}
	public void setId_promocao(String id_promocao) {
		this.id_promocao = id_promocao;
	}
	public String getFidelidade() {
		return fidelidade;
	}
	public void setFidelidade(String fidelidade) {
		this.fidelidade = fidelidade;
	}
    
}
