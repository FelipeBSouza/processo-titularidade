package api.rest.response;

import java.util.List;

import api.rest.helpers.FluigProduct;
import api.rest.model.integrador.Products;

public class ResponseProdutosFluig {

    private int codigo;
    private String mensagem;
    private List<Products> produtos;
    
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public List<Products> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<Products> produtos) {
		this.produtos = produtos;
	}		
   
}
