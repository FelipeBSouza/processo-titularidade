package api.rest.planos;

public class ParametroTpPLano {

	private String cod_plano;
	private String qtd_pontos_adicionais;
	
	public String getCod_plano() {
		return cod_plano;
	}
	public void setCod_plano(String cod_plano) {
		this.cod_plano = cod_plano;
	}
	public String getQtd_pontos_adicionais() {
		return qtd_pontos_adicionais;
	}
	public void setQtd_pontos_adicionais(String qtd_pontos_adicionais) {
		this.qtd_pontos_adicionais = qtd_pontos_adicionais;
	}	
}
