package api.rest.planos;

public class AuxiliarObrigatoriosRenovacao {
	
	private String referencia;
	private String cod_prod_antigo;
	private String id_item_contrato;
	private String descricao_prod_antigo;
	private String situacao;
	private String obs_item;
	private String novo_antigo;
			
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getDescricao_prod_antigo() {
		return descricao_prod_antigo;
	}
	public void setDescricao_prod_antigo(String descricao_prod_antigo) {
		this.descricao_prod_antigo = descricao_prod_antigo;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getCod_prod_antigo() {
		return cod_prod_antigo;
	}
	public void setCod_prod_antigo(String cod_prod_antigo) {
		this.cod_prod_antigo = cod_prod_antigo;
	}
	public String getId_item_contrato() {
		return id_item_contrato;
	}
	public void setId_item_contrato(String id_item_contrato) {
		this.id_item_contrato = id_item_contrato;
	}
	public String getObs_item() {
		return obs_item;
	}
	public void setObs_item(String obs_item) {
		this.obs_item = obs_item;
	}
	public String getNovo_antigo() {
		return novo_antigo;
	}
	public void setNovo_antigo(String novo_antigo) {
		this.novo_antigo = novo_antigo;
	}
		
}
