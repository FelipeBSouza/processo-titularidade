package api.rest.planos;

public class AuxiliarPlanosCorrespondente {
	
	private String id_item_contrato;
	private String cod_antigo;
	private String situacao;
	private String descricao_plano;	
		
	public String getDescricao_plano() {
		return descricao_plano;
	}
	public void setDescricao_plano(String descricao_plano) {
		this.descricao_plano = descricao_plano;
	}
	public String getId_item_contrato() {
		return id_item_contrato;
	}
	public void setId_item_contrato(String id_item_contrato) {
		this.id_item_contrato = id_item_contrato;
	}
	public String getCod_antigo() {
		return cod_antigo;
	}
	public void setCod_antigo(String cod_antigo) {
		this.cod_antigo = cod_antigo;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
}
