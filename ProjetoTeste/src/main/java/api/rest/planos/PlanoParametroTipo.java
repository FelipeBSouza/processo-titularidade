package api.rest.planos;

import java.util.List;

public class PlanoParametroTipo {

	private String tp_produto;
	private String idPromocao;
	private String fidelidade;
	private String cidade;
	private String bairro;
	private String categoria;
	private String referencia;
	private String combo;
	private String equivalencia;
	private String repetivel;
	
	private List<PlanoParametroReferencia> planosExistentes;	
		
	public List<PlanoParametroReferencia> getPlanosExistentes() {
		return planosExistentes;
	}
	public void setPlanosExistentes(List<PlanoParametroReferencia> planosExistentes) {
		this.planosExistentes = planosExistentes;
	}
	public String getEquivalencia() {
		return equivalencia;
	}
	public void setEquivalencia(String equivalencia) {
		this.equivalencia = equivalencia;
	}
	public String getRepetivel() {
		return repetivel;
	}
	public void setRepetivel(String repetivel) {
		this.repetivel = repetivel;
	}
	public String getTp_produto() {
		return tp_produto;
	}
	public void setTp_produto(String tp_produto) {
		this.tp_produto = tp_produto;
	}
	public String getIdPromocao() {
		return idPromocao;
	}
	public void setIdPromocao(String idPromocao) {
		this.idPromocao = idPromocao;
	}
	public String getFidelidade() {
		return fidelidade;
	}
	public void setFidelidade(String fidelidade) {
		this.fidelidade = fidelidade;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getCombo() {
		return combo;
	}
	public void setCombo(String combo) {
		this.combo = combo;
	}	

}
