package api.rest.response;

public class ResponseClientesStatusAdd {

	public Integer status;
	public String msg;
	
	public ResponseClientesStatusAdd(Integer status, String msg) {
		this.status = status;
		this.msg = msg;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
