package api.rest.response;

public class ResponseAtivacao {
	
	private String mac;
	private String cod_erro;
	private String msg_erro;	
	private String tp_erro;	
	private String msg_erro_sistema;	
	
	public ResponseAtivacao() {
		
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getCod_erro() {
		return cod_erro;
	}

	public void setCod_erro(String cod_erro) {
		this.cod_erro = cod_erro;
	}

	public String getMsg_erro() {
		return msg_erro;
	}

	public void setMsg_erro(String msg_erro) {
		this.msg_erro = msg_erro;
	}

	public String getTp_erro() {
		return tp_erro;
	}

	public void setTp_erro(String tp_erro) {
		this.tp_erro = tp_erro;
	}

	public String getMsg_erro_sistema() {
		return msg_erro_sistema;
	}

	public void setMsg_erro_sistema(String msg_erro_sistema) {
		this.msg_erro_sistema = msg_erro_sistema;
	}		
		
}
