package api.rest.response;

public class ResponseClientesStatus {
	
	public String tipo;
	public String numctr;
	public String erro;
	public String descricao;
	
	public ResponseClientesStatus(String tipo, String numctr, String erro, String descricao) {
		this.tipo = tipo;
		this.numctr = numctr;
		this.erro = erro;
		this.descricao = descricao;
	}

	public String getNumctr() {
		return numctr;
	}

	public void setNumctr(String numctr) {
		this.numctr = numctr;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
