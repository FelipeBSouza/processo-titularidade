package api.rest.response;

import api.rest.agendamento.RestJsonAgendamento;

public class ResponseAgendamento {
	
	private String msg;
	private String cod_erro;
	private RestJsonAgendamento agendamento;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCod_erro() {
		return cod_erro;
	}
	public void setCod_erro(String cod_erro) {
		this.cod_erro = cod_erro;
	}
	public RestJsonAgendamento getAgendamento() {
		return agendamento;
	}
	public void setAgendamento(RestJsonAgendamento agendamento) {
		this.agendamento = agendamento;
	}
	
}
