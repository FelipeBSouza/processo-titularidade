package api.rest.response;

import java.util.List;

public class ResponseMaterialInventario {
	
	private List<ResponseMaterial> vetor;

	public List<ResponseMaterial> getVetor() {
		return vetor;
	}

	public void setVetor(List<ResponseMaterial> vetor) {
		this.vetor = vetor;
	}
		
}
