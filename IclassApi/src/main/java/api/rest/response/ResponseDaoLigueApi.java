package api.rest.response;

public class ResponseDaoLigueApi {
	
	private String CODCLI;
	private String CODERRO;
	private String MSGERRO;
	private String TPERRO;
	
	public ResponseDaoLigueApi() {
		
	}
	
	public String getCODCLI() {
		return CODCLI;
	}
	public void setCODCLI(String cODCLI) {
		CODCLI = cODCLI;
	}
	public String getCODERRO() {
		return CODERRO;
	}
	public void setCODERRO(String cODERRO) {
		CODERRO = cODERRO;
	}
	public String getMSGERRO() {
		return MSGERRO;
	}
	public void setMSGERRO(String mSGERRO) {
		MSGERRO = mSGERRO;
	}

	public String getTPERRO() {
		return TPERRO;
	}

	public void setTPERRO(String tPERRO) {
		TPERRO = tPERRO;
	}
	
	
	
}
