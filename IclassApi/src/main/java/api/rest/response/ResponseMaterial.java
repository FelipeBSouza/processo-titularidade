package api.rest.response;

public class ResponseMaterial {
	
	private String descricao_erro;
	private String codigo_erro;
	private String tipo_erro;
	private String cod_armazem;
	private String cod_prod;
	
	public ResponseMaterial() {
		
	}
	
	public String getDescricao_erro() {
		return descricao_erro;
	}
	public void setDescricao_erro(String descricao_erro) {
		this.descricao_erro = descricao_erro;
	}
	public String getCodigo_erro() {
		return codigo_erro;
	}

	public void setCodigo_erro(String codigo_erro) {
		this.codigo_erro = codigo_erro;
	}
	public String getTipo_erro() {
		return tipo_erro;
	}
	public void setTipo_erro(String tipo_erro) {
		this.tipo_erro = tipo_erro;
	}

	public String getCod_armazem() {
		return cod_armazem;
	}

	public void setCod_armazem(String cod_armazem) {
		this.cod_armazem = cod_armazem;
	}

	public String getCod_prod() {
		return cod_prod;
	}

	public void setCod_prod(String cod_prod) {
		this.cod_prod = cod_prod;
	}
	
	
	

}
