package api.rest.response;

public class ResponseOS {
	
	private String response;
	private String id;
	private String OS;
	
	public ResponseOS() {
		
	}
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getOS() {
		return OS;
	}

	public void setOS(String oS) {
		OS = oS;
	}
	
	

}
