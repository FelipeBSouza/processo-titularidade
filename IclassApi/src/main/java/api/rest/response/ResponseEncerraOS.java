package api.rest.response;

public class ResponseEncerraOS {
	
	private String STATUS;
	private String MENSAGEM;
	private String SISTEMA;
	private String OS;	
	
	public ResponseEncerraOS() {
		
	}

	public String getSISTEMA() {
		return SISTEMA;
	}

	public void setSISTEMA(String sISTEMA) {
		SISTEMA = sISTEMA;
	}

	public String getOS() {
		return OS;
	}

	public void setOS(String oS) {
		OS = oS;
	}

	public String getSTATUS() {
		return STATUS;
	}


	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}


	public String getMENSAGEM() {
		return MENSAGEM;
	}


	public void setMENSAGEM(String mENSAGEM) {
		MENSAGEM = mENSAGEM;
	}
}
