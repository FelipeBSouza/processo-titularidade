package api.rest.retencao.sva;

public class PlanosContratoRetencaoSVA {
	
	private String P1CODP;
	private String P1ITEM;
	private String P1CODEND;
	private String P1IDENT;
	private String P1CODPN;
	private String P1CODPAA;
	
	public String getP1CODP() {
		return P1CODP;
	}
	public void setP1CODP(String p1codp) {
		P1CODP = p1codp;
	}
	public String getP1ITEM() {
		return P1ITEM;
	}
	public void setP1ITEM(String p1item) {
		P1ITEM = p1item;
	}
	public String getP1CODEND() {
		return P1CODEND;
	}
	public void setP1CODEND(String p1codend) {
		P1CODEND = p1codend;
	}
	public String getP1IDENT() {
		return P1IDENT;
	}
	public void setP1IDENT(String p1ident) {
		P1IDENT = p1ident;
	}
	public String getP1CODPN() {
		return P1CODPN;
	}
	public void setP1CODPN(String p1codpn) {
		P1CODPN = p1codpn;
	}
	public String getP1CODPAA() {
		return P1CODPAA;
	}
	public void setP1CODPAA(String p1codpaa) {
		P1CODPAA = p1codpaa;
	}

}
