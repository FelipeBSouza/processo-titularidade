package api.rest.retencao;

import java.util.List;

public class CabecalhoRetencao {
	
	private String TPPESSOA;
	private String NCTR;
	private String CDCLI;
	private String LOJA;
	private String VEND1;
	private String OBS_OS1;
	private String OBS_OS2;
	private String OPERADOR;
	private String CDPGTO;
	private String OPERACAO;
	private String MIDIA;
	private String TMK;
	private String TPCLI;
	private String TPCTR;
	private String PRAZO;
	private String VALCTR;
	private String UDIAFE;
	private String ASSCLI;
	private String OBSSUA;
	private String CODENDC;
	private String CX;
	private String PT;
	private String CCGCLI;
	private String NRSOLIC;
	private String URLGRV;
	private String FTDIGTAL;
	private String CODINS;
	private String OBSLAUDO;
	private String MOVTO;
	private String DTSOLINF;
	
	private List<FoneTb> _DADOS0155;
	
	private List<ItemTb1> _DADOS0255;
	
	private List<ItemTb2> _DADOS0355;
		
	public String getDTSOLINF() {
		return DTSOLINF;
	}
	public void setDTSOLINF(String dTSOLINF) {
		DTSOLINF = dTSOLINF;
	}
	public String getTPPESSOA() {
		return TPPESSOA;
	}
	public void setTPPESSOA(String tPPESSOA) {
		TPPESSOA = tPPESSOA;
	}
	public String getNCTR() {
		return NCTR;
	}
	public void setNCTR(String nCTR) {
		NCTR = nCTR;
	}
	public String getCDCLI() {
		return CDCLI;
	}
	public void setCDCLI(String cDCLI) {
		CDCLI = cDCLI;
	}
	public String getLOJA() {
		return LOJA;
	}
	public void setLOJA(String lOJA) {
		LOJA = lOJA;
	}
	public String getVEND1() {
		return VEND1;
	}
	public void setVEND1(String vEND1) {
		VEND1 = vEND1;
	}
	public String getOBS_OS1() {
		return OBS_OS1;
	}
	public void setOBS_OS1(String oBS_OS1) {
		OBS_OS1 = oBS_OS1;
	}
	public String getOBS_OS2() {
		return OBS_OS2;
	}
	public void setOBS_OS2(String oBS_OS2) {
		OBS_OS2 = oBS_OS2;
	}
	public String getOPERADOR() {
		return OPERADOR;
	}
	public void setOPERADOR(String oPERADOR) {
		OPERADOR = oPERADOR;
	}
	public String getCDPGTO() {
		return CDPGTO;
	}
	public void setCDPGTO(String cDPGTO) {
		CDPGTO = cDPGTO;
	}
	public String getOPERACAO() {
		return OPERACAO;
	}
	public void setOPERACAO(String oPERACAO) {
		OPERACAO = oPERACAO;
	}
	public String getMIDIA() {
		return MIDIA;
	}
	public void setMIDIA(String mIDIA) {
		MIDIA = mIDIA;
	}
	public String getTMK() {
		return TMK;
	}
	public void setTMK(String tMK) {
		TMK = tMK;
	}
	public String getTPCLI() {
		return TPCLI;
	}
	public void setTPCLI(String tPCLI) {
		TPCLI = tPCLI;
	}
	public String getTPCTR() {
		return TPCTR;
	}
	public void setTPCTR(String tPCTR) {
		TPCTR = tPCTR;
	}
	public String getPRAZO() {
		return PRAZO;
	}
	public void setPRAZO(String pRAZO) {
		PRAZO = pRAZO;
	}
	public String getVALCTR() {
		return VALCTR;
	}
	public void setVALCTR(String vALCTR) {
		VALCTR = vALCTR;
	}
	public String getUDIAFE() {
		return UDIAFE;
	}
	public void setUDIAFE(String uDIAFE) {
		UDIAFE = uDIAFE;
	}
	public String getASSCLI() {
		return ASSCLI;
	}
	public void setASSCLI(String aSSCLI) {
		ASSCLI = aSSCLI;
	}
	public String getOBSSUA() {
		return OBSSUA;
	}
	public void setOBSSUA(String oBSSUA) {
		OBSSUA = oBSSUA;
	}
	public String getCODENDC() {
		return CODENDC;
	}
	public void setCODENDC(String cODENDC) {
		CODENDC = cODENDC;
	}
	public String getCX() {
		return CX;
	}
	public void setCX(String cX) {
		CX = cX;
	}
	public String getPT() {
		return PT;
	}
	public void setPT(String pT) {
		PT = pT;
	}
	public String getCCGCLI() {
		return CCGCLI;
	}
	public void setCCGCLI(String cCGCLI) {
		CCGCLI = cCGCLI;
	}
	public String getNRSOLIC() {
		return NRSOLIC;
	}
	public void setNRSOLIC(String nRSOLIC) {
		NRSOLIC = nRSOLIC;
	}
	public String getURLGRV() {
		return URLGRV;
	}
	public void setURLGRV(String uRLGRV) {
		URLGRV = uRLGRV;
	}
	public String getFTDIGTAL() {
		return FTDIGTAL;
	}
	public void setFTDIGTAL(String fTDIGTAL) {
		FTDIGTAL = fTDIGTAL;
	}
	public String getCODINS() {
		return CODINS;
	}
	public void setCODINS(String cODINS) {
		CODINS = cODINS;
	}
	public String getOBSLAUDO() {
		return OBSLAUDO;
	}
	public void setOBSLAUDO(String oBSLAUDO) {
		OBSLAUDO = oBSLAUDO;
	}
	public String getMOVTO() {
		return MOVTO;
	}
	public void setMOVTO(String mOVTO) {
		MOVTO = mOVTO;
	}
	public List<FoneTb> get_DADOS0155() {
		return _DADOS0155;
	}
	public void set_DADOS0155(List<FoneTb> _DADOS0155) {
		this._DADOS0155 = _DADOS0155;
	}
	public List<ItemTb1> get_DADOS0255() {
		return _DADOS0255;
	}
	public void set_DADOS0255(List<ItemTb1> _DADOS0255) {
		this._DADOS0255 = _DADOS0255;
	}
	public List<ItemTb2> get_DADOS0355() {
		return _DADOS0355;
	}
	public void set_DADOS0355(List<ItemTb2> _DADOS0355) {
		this._DADOS0355 = _DADOS0355;
	}	

}
