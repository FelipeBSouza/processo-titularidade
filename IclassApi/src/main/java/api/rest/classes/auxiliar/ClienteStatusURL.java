package api.rest.classes.auxiliar;

public class ClienteStatusURL {
	
	public String URL;
	public String numctr;
	public Integer recno;
	public String usuarioid;
	public String cpfcnpj;
	public String celular;
	public String email;
	public String nome;
	public String metodo;
	public String status;

	public ClienteStatusURL(String uRL, String numctr, Integer recno, String usuarioid, String cpfcnpj, String celular,
			String email, String nome, String metodo, String status) {
		super();
		URL = uRL;
		this.numctr = numctr;
		this.recno = recno;
		this.usuarioid = usuarioid;
		this.cpfcnpj = cpfcnpj;
		this.celular = celular;
		this.email = email;
		this.nome = nome;
		this.metodo = metodo;
		this.status = status;
		
	}
	
	public ClienteStatusURL(String uRL, String numctr, Integer recno, String usuarioid, String cpfcnpj,
			String email, String nome, String metodo, String status) {
		super();
		URL = uRL;
		this.numctr = numctr;
		this.recno = recno;
		this.usuarioid = usuarioid;
		this.cpfcnpj = cpfcnpj;
		this.email = email;
		this.nome = nome;
		this.metodo = metodo;
		this.status = status;
		
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMetodo() {
		return metodo;
	}
	
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getUsuarioid() {
		return usuarioid;
	}

	public void setUsuarioid(String usuarioid) {
		this.usuarioid = usuarioid;
	}

	public String getCpfcnpj() {
		return cpfcnpj;
	}

	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getNumctr() {
		return numctr;
	}

	public void setNumctr(String numctr) {
		this.numctr = numctr;
	}

	public Integer getRecno() {
		return recno;
	}

	public void setRecno(Integer recno) {
		this.recno = recno;
	}
	
}
