package api.rest.classes.auxiliar;

public class ClienteStatus {
	
	public String usuarioid;
	public String cpfcnpj;
	public String celular;
	public String email;
	public String nome;
	public String contratoid;
	public String novostatus;
	
	public ClienteStatus() {
	}

	public String getUsuarioid() {
		return usuarioid;
	}

	public void setUsuarioid(String usuarioid) {
		this.usuarioid = usuarioid;
	}

	public String getCpfcnpj() {
		return cpfcnpj;
	}

	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getContratoid() {
		return contratoid;
	}

	public void setContratoid(String contratoid) {
		this.contratoid = contratoid;
	}

	public String getNovostatus() {
		return novostatus;
	}

	public void setNovostatus(String novostatus) {
		this.novostatus = novostatus;
	}
	
}
