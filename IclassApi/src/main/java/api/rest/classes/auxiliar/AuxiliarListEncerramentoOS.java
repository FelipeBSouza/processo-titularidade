package api.rest.classes.auxiliar;

public class AuxiliarListEncerramentoOS {
	
	private String cod_prod;
	private String item_os;
	private String acao_os;
	
	public String getCod_prod() {
		return cod_prod;
	}
	public void setCod_prod(String cod_prod) {
		this.cod_prod = cod_prod;
	}
	public String getItem_os() {
		return item_os;
	}
	public void setItem_os(String item_os) {
		this.item_os = item_os;
	}
	public String getAcao_os() {
		return acao_os;
	}
	public void setAcao_os(String acao_os) {
		this.acao_os = acao_os;
	}

}
