package api.rest.classes.auxiliar;

public class ClienteAuxiliar {	
	
	private String nr_celular;
	private String nome;
	private String nr_fone;
	private String ddd_celular;
	private String cod_cliente;
	private String dt_nascimento;
	private String email;
	private String ddd_fone;
	private String loja;	
	
	public String getNr_celular() {
		return nr_celular;
	}
	public void setNr_celular(String nr_celular) {
		this.nr_celular = nr_celular;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNr_fone() {
		return nr_fone;
	}
	public void setNr_fone(String nr_fone) {
		this.nr_fone = nr_fone;
	}
	public String getDdd_celular() {
		return ddd_celular;
	}
	public void setDdd_celular(String ddd_celular) {
		this.ddd_celular = ddd_celular;
	}
	public String getCod_cliente() {
		return cod_cliente;
	}
	public void setCod_cliente(String cod_cliente) {
		this.cod_cliente = cod_cliente;
	}
	public String getDt_nascimento() {
		return dt_nascimento;
	}
	public void setDt_nascimento(String dt_nascimento) {
		this.dt_nascimento = dt_nascimento;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDdd_fone() {
		return ddd_fone;
	}
	public void setDdd_fone(String ddd_fone) {
		this.ddd_fone = ddd_fone;
	}
	public String getLoja() {
		return loja;
	}
	public void setLoja(String loja) {
		this.loja = loja;
	}
	

}
