package api.rest.encerra.os;

import java.util.List;

public class RestJsonPrincipalEncerraOS {
	
	private List<RestJsonCabecalho> ORDENS;
	
	public RestJsonPrincipalEncerraOS() {
		
	}

	public List<RestJsonCabecalho> getORDENS() {
		return ORDENS;
	}

	public void setORDENS(List<RestJsonCabecalho> oRDENS) {
		ORDENS = oRDENS;
	}
	
}
