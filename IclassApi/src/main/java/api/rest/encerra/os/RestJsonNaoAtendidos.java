package api.rest.encerra.os;

public class RestJsonNaoAtendidos {
	
	private String ITEM;
	private String OS;
	
	public String getITEM() {
		return ITEM;
	}
	public void setITEM(String iTEM) {
		ITEM = iTEM;
	}
	public String getOS() {
		return OS;
	}
	public void setOS(String oS) {
		OS = oS;
	}	

}
