package api.rest.encerra.os.ocorrencia;

public class RestJsonItensOS {	
	
	private String STATUSOS; // numero da ordem de servico
	private String CODPRO; // item da ordem de servico	
	private String CDOCORR;
	private String DESOCO;
		
	public RestJsonItensOS() {
		
	}

	public String getSTATUSOS() {
		return STATUSOS;
	}

	public void setSTATUSOS(String sTATUSOS) {
		STATUSOS = sTATUSOS;
	}

	public String getCODPRO() {
		return CODPRO;
	}

	public void setCODPRO(String cODPRO) {
		CODPRO = cODPRO;
	}

	public String getCDOCORR() {
		return CDOCORR;
	}

	public void setCDOCORR(String cDOCORR) {
		CDOCORR = cDOCORR;
	}

	public String getDESOCO() {
		return DESOCO;
	}

	public void setDESOCO(String dESOCO) {
		DESOCO = dESOCO;
	}		
	
}
