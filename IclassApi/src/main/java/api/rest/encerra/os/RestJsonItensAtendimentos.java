package api.rest.encerra.os;

public class RestJsonItensAtendimentos {
	
	private String DTCHEGADA;
	private String HRCHEGADA;
	private String DTSAIDA;
	private String HRSAIDA;
	private String DTINICIAL;
	private String HRINICIAL;
	private String DTFINAL;
	private String HRFINAL;
	private String OCORR;
	private String LAUDO;
	private String ACUMUL;
	private String STATUS;
	private String TRASLA;
	private String HRSFATS;	
	private String CODOCOR;
	
	public RestJsonItensAtendimentos() {
		
	}	
	
	public String getDTCHEGADA() {
		return DTCHEGADA;
	}
	public void setDTCHEGADA(String dTCHEGADA) {
		DTCHEGADA = dTCHEGADA;
	}
	public String getHRCHEGADA() {
		return HRCHEGADA;
	}
	public void setHRCHEGADA(String hRCHEGADA) {
		HRCHEGADA = hRCHEGADA;
	}
	public String getDTSAIDA() {
		return DTSAIDA;
	}
	public void setDTSAIDA(String dTSAIDA) {
		DTSAIDA = dTSAIDA;
	}
	public String getHRSAIDA() {
		return HRSAIDA;
	}
	public void setHRSAIDA(String hRSAIDA) {
		HRSAIDA = hRSAIDA;
	}
	public String getDTINICIAL() {
		return DTINICIAL;
	}
	public void setDTINICIAL(String dTINICIAL) {
		DTINICIAL = dTINICIAL;
	}
	public String getHRINICIAL() {
		return HRINICIAL;
	}
	public void setHRINICIAL(String hRINICIAL) {
		HRINICIAL = hRINICIAL;
	}
	public String getDTFINAL() {
		return DTFINAL;
	}
	public void setDTFINAL(String dTFINAL) {
		DTFINAL = dTFINAL;
	}
	public String getHRFINAL() {
		return HRFINAL;
	}
	public void setHRFINAL(String hRFINAL) {
		HRFINAL = hRFINAL;
	}
	public String getOCORR() {
		return OCORR;
	}
	public void setOCORR(String oCORR) {
		OCORR = oCORR;
	}
	public String getLAUDO() {
		return LAUDO;
	}
	public void setLAUDO(String lAUDO) {
		LAUDO = lAUDO;
	}
	public String getACUMUL() {
		return ACUMUL;
	}
	public void setACUMUL(String aCUMUL) {
		ACUMUL = aCUMUL;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getTRASLA() {
		return TRASLA;
	}
	public void setTRASLA(String tRASLA) {
		TRASLA = tRASLA;
	}
	public String getHRSFATS() {
		return HRSFATS;
	}
	public void setHRSFATS(String hRSFATS) {
		HRSFATS = hRSFATS;
	}	
	public String getCODOCOR() {
		return CODOCOR;
	}
	public void setCODOCOR(String cODOCOR) {
		CODOCOR = cODOCOR;
	}
	
	

}
