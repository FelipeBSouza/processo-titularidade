package api.rest.encerra.os;

import java.util.List;

public class RestJsonCabecalho {
	
	private String NUMOS;
	private String CODCLI;
	private String LOJA;
	private String ATENDENTE;
	private String CUSUARIO;
	private String PORTA;
	private String CAIXA;	
	
	private List<RestJsonItensAtendimentos> ATENDIMENTOS;
	private List<RestJsonItensMateriais> MATERIAIS;
//	private List<RestJsonItensMateriais> INMATERIAIS;

	public RestJsonCabecalho() {
		
	}	
	
	public String getATENDENTE() {
		return ATENDENTE;
	}

	public void setATENDENTE(String aTENDENTE) {
		ATENDENTE = aTENDENTE;
	}

	public String getNUMOS() {
		return NUMOS;
	}

	public void setNUMOS(String nUMOS) {
		NUMOS = nUMOS;
	}

	public String getCODCLI() {
		return CODCLI;
	}

	public void setCODCLI(String cODCLI) {
		CODCLI = cODCLI;
	}

	public String getLOJA() {
		return LOJA;
	}

	public void setLOJA(String lOJA) {
		LOJA = lOJA;
	}	

	public String getCUSUARIO() {
		return CUSUARIO;
	}

	public void setCUSUARIO(String cUSUARIO) {
		CUSUARIO = cUSUARIO;
	}	

	public String getPORTA() {
		return PORTA;
	}

	public void setPORTA(String pORTA) {
		PORTA = pORTA;
	}

	public String getCAIXA() {
		return CAIXA;
	}

	public void setCAIXA(String cAIXA) {
		CAIXA = cAIXA;
	}

	public List<RestJsonItensMateriais> getMATERIAIS() {
		return MATERIAIS;
	}

	public void setMATERIAIS(List<RestJsonItensMateriais> mATERIAIS) {
		MATERIAIS = mATERIAIS;
	}

	public List<RestJsonItensAtendimentos> getATENDIMENTOS() {
		return ATENDIMENTOS;
	}

	public void setATENDIMENTOS(List<RestJsonItensAtendimentos> aTENDIMENTOS) {
		ATENDIMENTOS = aTENDIMENTOS;
	}

//	public List<RestJsonItensMateriais> getINMATERIAIS() {
//		return INMATERIAIS;
//	}
//
//	public void setINMATERIAIS(List<RestJsonItensMateriais> iNMATERIAIS) {
//		INMATERIAIS = iNMATERIAIS;
//	}

}
