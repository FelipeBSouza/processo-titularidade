package api.rest.encerra.os;

public class RestJsonItensMateriais {
	
	private String CDPRO;
	private String DESCPRO;
	private String QTDE;
	private String VLRUNIT;
	private String VLRTOTAL;
	private String CDSERVICO;
	private String ARMAZEM;
	
	public RestJsonItensMateriais() {
		
	}

	public String getCDPRO() {
		return CDPRO;
	}

	public void setCDPRO(String cDPRO) {
		CDPRO = cDPRO;
	}

	public String getDESCPRO() {
		return DESCPRO;
	}

	public void setDESCPRO(String dESCPRO) {
		DESCPRO = dESCPRO;
	}

	public String getQTDE() {
		return QTDE;
	}

	public void setQTDE(String qTDE) {
		QTDE = qTDE;
	}

	public String getVLRUNIT() {
		return VLRUNIT;
	}

	public void setVLRUNIT(String vLRUNIT) {
		VLRUNIT = vLRUNIT;
	}

	public String getVLRTOTAL() {
		return VLRTOTAL;
	}

	public void setVLRTOTAL(String vLRTOTAL) {
		VLRTOTAL = vLRTOTAL;
	}

	public String getCDSERVICO() {
		return CDSERVICO;
	}

	public void setCDSERVICO(String cDSERVICO) {
		CDSERVICO = cDSERVICO;
	}

	public String getARMAZEM() {
		return ARMAZEM;
	}

	public void setARMAZEM(String aRMAZEM) {
		ARMAZEM = aRMAZEM;
	}
	
	
	
	

}
