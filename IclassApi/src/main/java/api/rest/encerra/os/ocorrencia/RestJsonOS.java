package api.rest.encerra.os.ocorrencia;

import java.util.List;

public class RestJsonOS {
	
	private String CDCLI;
	private String LOJA;
	private String CPAG;
	private String OBS;
	private String LOGIN;
	private String NCTR;
	private String ITEMCTR;
	private String ENDINS;
	private String TPOS;
	private String CX;
	private String PT;
	private String NRSOLIC;
	private String OSORIGIN;
	
	private List<RestJsonItensOS> ITENSOS;
	
	public RestJsonOS() {
		
	}

	public String getCDCLI() {
		return CDCLI;
	}

	public void setCDCLI(String cDCLI) {
		CDCLI = cDCLI;
	}

	public String getLOJA() {
		return LOJA;
	}

	public void setLOJA(String lOJA) {
		LOJA = lOJA;
	}

	public String getCPAG() {
		return CPAG;
	}

	public void setCPAG(String cPAG) {
		CPAG = cPAG;
	}

	public String getOBS() {
		return OBS;
	}

	public void setOBS(String oBS) {
		OBS = oBS;
	}

	public String getLOGIN() {
		return LOGIN;
	}

	public void setLOGIN(String lOGIN) {
		LOGIN = lOGIN;
	}

	public String getNCTR() {
		return NCTR;
	}

	public void setNCTR(String nCTR) {
		NCTR = nCTR;
	}

	public String getITEMCTR() {
		return ITEMCTR;
	}

	public void setITEMCTR(String iTEMCTR) {
		ITEMCTR = iTEMCTR;
	}

	public String getENDINS() {
		return ENDINS;
	}

	public void setENDINS(String eNDINS) {
		ENDINS = eNDINS;
	}

	public String getTPOS() {
		return TPOS;
	}

	public void setTPOS(String tPOS) {
		TPOS = tPOS;
	}

	public String getCX() {
		return CX;
	}

	public void setCX(String cX) {
		CX = cX;
	}

	public String getPT() {
		return PT;
	}

	public void setPT(String pT) {
		PT = pT;
	}

	public String getNRSOLIC() {
		return NRSOLIC;
	}

	public void setNRSOLIC(String nRSOLIC) {
		NRSOLIC = nRSOLIC;
	}

	public List<RestJsonItensOS> getITENSOS() {
		return ITENSOS;
	}

	public void setITENSOS(List<RestJsonItensOS> iTENSOS) {
		ITENSOS = iTENSOS;
	}

	public String getOSORIGIN() {
		return OSORIGIN;
	}

	public void setOSORIGIN(String oSORIGIN) {
		OSORIGIN = oSORIGIN;
	}	
	
}
