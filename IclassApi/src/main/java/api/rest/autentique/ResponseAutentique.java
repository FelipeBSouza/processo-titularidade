package api.rest.autentique;

public class ResponseAutentique {
	
	private String id_contrato;
	private String nr_solicitacao;
	private String status;
	private String cod_status;
	private String link_contrato_assinado;
	private String obs_rejeitado;
	private int atividade;
	
	public int getAtividade() {
		return atividade;
	}
	public void setAtividade(int atividade) {
		this.atividade = atividade;
	}
	public String getId_contrato() {
		return id_contrato;
	}
	public void setId_contrato(String id_contrato) {
		this.id_contrato = id_contrato;
	}
	public String getNr_solicitacao() {
		return nr_solicitacao;
	}
	public void setNr_solicitacao(String nr_solicitacao) {
		this.nr_solicitacao = nr_solicitacao;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCod_status() {
		return cod_status;
	}
	public void setCod_status(String cod_status) {
		this.cod_status = cod_status;
	}
	public String getLink_contrato_assinado() {
		return link_contrato_assinado;
	}
	public void setLink_contrato_assinado(String link_contrato_assinado) {
		this.link_contrato_assinado = link_contrato_assinado;
	}
	public String getObs_rejeitado() {
		return obs_rejeitado;
	}
	public void setObs_rejeitado(String obs_rejeitado) {
		this.obs_rejeitado = obs_rejeitado;
	}	
	
}
