package api.rest.newCustomer;

public class FoneTb {
	
	private String PADDDI;
	private String PADDD;
	private String PATEL;
	private String PATIPO;
	private String PAOP;
	private String CDTELEF;
	private String PTREC;
	
	public String getPADDDI() {
		return PADDDI;
	}
	public void setPADDDI(String pADDDI) {
		PADDDI = pADDDI;
	}
	public String getPADDD() {
		return PADDD;
	}
	public void setPADDD(String pADDD) {
		PADDD = pADDD;
	}
	public String getPATEL() {
		return PATEL;
	}
	public void setPATEL(String pATEL) {
		PATEL = pATEL;
	}
	public String getPATIPO() {
		return PATIPO;
	}
	public void setPATIPO(String pATIPO) {
		PATIPO = pATIPO;
	}
	public String getPAOP() {
		return PAOP;
	}
	public void setPAOP(String pAOP) {
		PAOP = pAOP;
	}
	public String getCDTELEF() {
		return CDTELEF;
	}
	public void setCDTELEF(String cDTELEF) {
		CDTELEF = cDTELEF;
	}
	public String getPTREC() {
		return PTREC;
	}
	public void setPTREC(String pTREC) {
		PTREC = pTREC;
	}
}
