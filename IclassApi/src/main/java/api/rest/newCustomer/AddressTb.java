package api.rest.newCustomer;

public class AddressTb {
	
	private String PEEND;
	private String PEENDNUM;
	private String PECOMP;
	private String PEBAIRRO;
	private String PECEP;
	private String PEMUN;
	private String PEESTADO;
	private String PETIPO;
	private String PEOP;
	private String PETIPO2;
	private String CDEND;
	
	public String getPEEND() {
		return PEEND;
	}
	public void setPEEND(String pEEND) {
		PEEND = pEEND;
	}
	public String getPEENDNUM() {
		return PEENDNUM;
	}
	public void setPEENDNUM(String pEENDNUM) {
		PEENDNUM = pEENDNUM;
	}
	public String getPECOMP() {
		return PECOMP;
	}
	public void setPECOMP(String pECOMP) {
		PECOMP = pECOMP;
	}
	public String getPEBAIRRO() {
		return PEBAIRRO;
	}
	public void setPEBAIRRO(String pEBAIRRO) {
		PEBAIRRO = pEBAIRRO;
	}
	public String getPECEP() {
		return PECEP;
	}
	public void setPECEP(String pECEP) {
		PECEP = pECEP;
	}
	public String getPEMUN() {
		return PEMUN;
	}
	public void setPEMUN(String pEMUN) {
		PEMUN = pEMUN;
	}
	public String getPEESTADO() {
		return PEESTADO;
	}
	public void setPEESTADO(String pEESTADO) {
		PEESTADO = pEESTADO;
	}
	public String getPETIPO() {
		return PETIPO;
	}
	public void setPETIPO(String pETIPO) {
		PETIPO = pETIPO;
	}
	public String getPEOP() {
		return PEOP;
	}
	public void setPEOP(String pEOP) {
		PEOP = pEOP;
	}
	public String getPETIPO2() {
		return PETIPO2;
	}
	public void setPETIPO2(String pETIPO2) {
		PETIPO2 = pETIPO2;
	}
	public String getCDEND() {
		return CDEND;
	}
	public void setCDEND(String cDEND) {
		CDEND = cDEND;
	}

	
}
