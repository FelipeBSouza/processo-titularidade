package api.rest.newCustomer;

public class PartnersTb {
	
	private String PARTNAME;
	private String PARTCGC;
	
	public String getPARTNAME() {
		return PARTNAME;
	}
	public void setPARTNAME(String pARTNAME) {
		PARTNAME = pARTNAME;
	}
	public String getPARTCGC() {
		return PARTCGC;
	}
	public void setPARTCGC(String pARTCGC) {
		PARTCGC = pARTCGC;
	}
}
