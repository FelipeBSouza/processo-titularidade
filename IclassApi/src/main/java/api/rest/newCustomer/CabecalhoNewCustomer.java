package api.rest.newCustomer;

import java.util.List;

public class CabecalhoNewCustomer {
	
	private String CPESSOA;
	private String CCGC;
	private String CNOME;
	private String CEMAIL;
	private String CDTNASC;
	private String CINSCR;
	private String CRG;
	private String CCONTAT;
	private String CCPFCON;
	private String CTIPO;
	private String DDDCLI;
	private String TELCLI;
	private String DDDCELCL;
	private String CELCLI;
	private String CODSU5;
	private String CDLJCLI;
	private String MTHRNAME;
	private String FNTSNAME;
	
	private List<FoneTb> _DADOS7101;
	
	private List<AddressTb> _DADOS7102;
	
	private List<PartnersTb> _DADOS7103;

	public String getCPESSOA() {
		return CPESSOA;
	}

	public void setCPESSOA(String cPESSOA) {
		CPESSOA = cPESSOA;
	}

	public String getCCGC() {
		return CCGC;
	}

	public void setCCGC(String cCGC) {
		CCGC = cCGC;
	}

	public String getCNOME() {
		return CNOME;
	}

	public void setCNOME(String cNOME) {
		CNOME = cNOME;
	}

	public String getCEMAIL() {
		return CEMAIL;
	}

	public void setCEMAIL(String cEMAIL) {
		CEMAIL = cEMAIL;
	}

	public String getCDTNASC() {
		return CDTNASC;
	}

	public void setCDTNASC(String cDTNASC) {
		CDTNASC = cDTNASC;
	}

	public String getCINSCR() {
		return CINSCR;
	}

	public void setCINSCR(String cINSCR) {
		CINSCR = cINSCR;
	}

	public String getCRG() {
		return CRG;
	}

	public void setCRG(String cRG) {
		CRG = cRG;
	}

	public String getCCONTAT() {
		return CCONTAT;
	}

	public void setCCONTAT(String cCONTAT) {
		CCONTAT = cCONTAT;
	}

	public String getCCPFCON() {
		return CCPFCON;
	}

	public void setCCPFCON(String cCPFCON) {
		CCPFCON = cCPFCON;
	}

	public String getCTIPO() {
		return CTIPO;
	}

	public void setCTIPO(String cTIPO) {
		CTIPO = cTIPO;
	}

	public String getDDDCLI() {
		return DDDCLI;
	}

	public void setDDDCLI(String dDDCLI) {
		DDDCLI = dDDCLI;
	}

	public String getTELCLI() {
		return TELCLI;
	}

	public void setTELCLI(String tELCLI) {
		TELCLI = tELCLI;
	}

	public String getDDDCELCL() {
		return DDDCELCL;
	}

	public void setDDDCELCL(String dDDCELCL) {
		DDDCELCL = dDDCELCL;
	}

	public String getCELCLI() {
		return CELCLI;
	}

	public void setCELCLI(String cELCLI) {
		CELCLI = cELCLI;
	}

	public String getCODSU5() {
		return CODSU5;
	}

	public void setCODSU5(String cODSU5) {
		CODSU5 = cODSU5;
	}

	public String getCDLJCLI() {
		return CDLJCLI;
	}

	public void setCDLJCLI(String cDLJCLI) {
		CDLJCLI = cDLJCLI;
	}

	public String getMTHRNAME() {
		return MTHRNAME;
	}

	public void setMTHRNAME(String mTHRNAME) {
		MTHRNAME = mTHRNAME;
	}

	public String getFNTSNAME() {
		return FNTSNAME;
	}

	public void setFNTSNAME(String fNTSNAME) {
		FNTSNAME = fNTSNAME;
	}

	public List<FoneTb> get_DADOS7101() {
		return _DADOS7101;
	}

	public void set_DADOS7101(List<FoneTb> _DADOS7101) {
		this._DADOS7101 = _DADOS7101;
	}

	public List<AddressTb> get_DADOS7102() {
		return _DADOS7102;
	}

	public void set_DADOS7102(List<AddressTb> _DADOS7102) {
		this._DADOS7102 = _DADOS7102;
	}

	public List<PartnersTb> get_DADOS7103() {
		return _DADOS7103;
	}

	public void set_DADOS7103(List<PartnersTb> _DADOS7103) {
		this._DADOS7103 = _DADOS7103;
	}
		
	

}
