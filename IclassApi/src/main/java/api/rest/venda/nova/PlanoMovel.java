package api.rest.venda.nova;

public class PlanoMovel {
	
	private String CODPRODUTOITEM;
	private String QUANTIDADEITEM;
	private String VALORESTIMADO;
	private String VALORCHEIO;
	private String TOTALITEM;
	private String VALORDESCONTO;
	private String QTMESINI;
	private String QTMESCOB;
	private String OBSITEM;
	private String COD_TARIFA;
	private String COD_ITEM;
	private String TPPLANO;
	private String FONEMOVEL;
	
	public String getCODPRODUTOITEM() {
		return CODPRODUTOITEM;
	}
	public void setCODPRODUTOITEM(String cODPRODUTOITEM) {
		CODPRODUTOITEM = cODPRODUTOITEM;
	}
	public String getQUANTIDADEITEM() {
		return QUANTIDADEITEM;
	}
	public void setQUANTIDADEITEM(String qUANTIDADEITEM) {
		QUANTIDADEITEM = qUANTIDADEITEM;
	}
	public String getVALORESTIMADO() {
		return VALORESTIMADO;
	}
	public void setVALORESTIMADO(String vALORESTIMADO) {
		VALORESTIMADO = vALORESTIMADO;
	}
	public String getVALORCHEIO() {
		return VALORCHEIO;
	}
	public void setVALORCHEIO(String vALORCHEIO) {
		VALORCHEIO = vALORCHEIO;
	}
	public String getTOTALITEM() {
		return TOTALITEM;
	}
	public void setTOTALITEM(String tOTALITEM) {
		TOTALITEM = tOTALITEM;
	}
	public String getVALORDESCONTO() {
		return VALORDESCONTO;
	}
	public void setVALORDESCONTO(String vALORDESCONTO) {
		VALORDESCONTO = vALORDESCONTO;
	}
	public String getQTMESINI() {
		return QTMESINI;
	}
	public void setQTMESINI(String qTMESINI) {
		QTMESINI = qTMESINI;
	}
	public String getQTMESCOB() {
		return QTMESCOB;
	}
	public void setQTMESCOB(String qTMESCOB) {
		QTMESCOB = qTMESCOB;
	}
	public String getOBSITEM() {
		return OBSITEM;
	}
	public void setOBSITEM(String oBSITEM) {
		OBSITEM = oBSITEM;
	}
	public String getCOD_TARIFA() {
		return COD_TARIFA;
	}
	public void setCOD_TARIFA(String cOD_TARIFA) {
		COD_TARIFA = cOD_TARIFA;
	}
	public String getCOD_ITEM() {
		return COD_ITEM;
	}
	public void setCOD_ITEM(String cOD_ITEM) {
		COD_ITEM = cOD_ITEM;
	}
	public String getTPPLANO() {
		return TPPLANO;
	}
	public void setTPPLANO(String tPPLANO) {
		TPPLANO = tPPLANO;
	}
	public String getFONEMOVEL() {
		return FONEMOVEL;
	}
	public void setFONEMOVEL(String fONEMOVEL) {
		FONEMOVEL = fONEMOVEL;
	}
	
}
