package api.rest.venda.nova;

public class RespostaIntegracao {
	
	private String STATUS;
	private String MENSAGEM;
	private String OS;
	private String SISTEMA;
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getMENSAGEM() {
		return MENSAGEM;
	}
	public void setMENSAGEM(String mENSAGEM) {
		MENSAGEM = mENSAGEM;
	}
	public String getOS() {
		return OS;
	}
	public void setOS(String oS) {
		OS = oS;
	}
	public String getSISTEMA() {
		return SISTEMA;
	}
	public void setSISTEMA(String sISTEMA) {
		SISTEMA = sISTEMA;
	}	

}
