package api.rest.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import api.rest.autentique.ResponseAutentique;
import api.rest.model.totvs.Ab9010;
import api.rest.repositoy.AB6Repository;
import api.rest.repositoy.AB9Repository;
import api.rest.repositoy.SYPRepository;

public class DaoPortabilidade {
	
	@Autowired
	AB9Repository ab9Repository;
	@Autowired
	AB6Repository ab6Repository;
	@Autowired
	SYPRepository sypRepository;
	
	public DaoPortabilidade(AB9Repository ab9Repository, AB6Repository ab6Repository, SYPRepository sypRepository) {
		this.ab9Repository = ab9Repository;
		this.ab6Repository = ab6Repository;		
		this.sypRepository = sypRepository;	
	}
	
	// busca lista de atendimentos da portabilidade que deram certo e movimenta a solicitacao
	public List<ResponseAutentique> verificaPortabilidadeOK() {
		
		//System.out.println("entrou na funcao verificaPortabilidadeOK");
		
		List<ResponseAutentique> listResponse = new ArrayList<>();
		
		try {
			
			List<Ab9010> listOk = ab9Repository.listaPortabilidadesOk();
			String num_os = "";
			String nr_solicitacao = "";
			String nr_chave_atendimento = "";
			String laudo_atendimento = "";
			
			DaoFluig daoFluig = new DaoFluig();
						
			int i = 0;
			while (i < listOk.size()) {
				
				num_os = listOk.get(i).getAb9Numos().trim();
				
				num_os = num_os.substring(0, num_os.length() - 2);
				
				nr_solicitacao = ab6Repository.buscaNumeroSolicitacao(num_os);
				nr_solicitacao = nr_solicitacao.trim();
				nr_chave_atendimento = listOk.get(i).getAb9Memo1();
				
				if(!nr_solicitacao.equals("") && !nr_solicitacao.equals(null) && !nr_solicitacao.equals("null")) {
					
					//System.out.println("vai chamar funcao q movimenta a solicitacao");
					
					laudo_atendimento = buscaLaudoAtendimento(nr_chave_atendimento);
					
					if(laudo_atendimento.equals("") || laudo_atendimento.equals("null") || laudo_atendimento.equals(null)) {
						laudo_atendimento = "sem laudo de atendimento";
					}
					
					listResponse = daoFluig.movimentaSolicitacaoFluigVendas(Integer.parseInt(nr_solicitacao), 0, laudo_atendimento, "POK", listOk.get(i).getAb9Cdbxa().trim(), num_os);
					
					// se entrar no if abaixo entao deu tudo certo e vai ser add em uma lista pra fazer marcacao para o registro nao aparecer mais pra ser movimentado
					if (listResponse.size() <= 0) {
						marca_atendimento(listOk.get(i).getAb9Numos().trim(), listOk.get(i).getAb9Seq().trim(), listOk.get(i).getRECNO());
					}
					
				}
				
				i++;
			}
			
		}catch(Exception e) {
			//System.out.println("erro na funcao verificaPortabilidade: ");
			e.printStackTrace();
		}
		
		return listResponse;
		
	}
	
	// busca lista de atendimentos da portabilidade que deram errado e movimenta a solicitacao
	public List<ResponseAutentique> verificaPortabilidadERRO() {
		
		//System.out.println("entrou na funcao verificaPortabilidadERRO");
		
		List<ResponseAutentique> listResponse = new ArrayList<>();
		
		try {
			
			List<Ab9010> listErro = ab9Repository.listaPortabilidadesErro();
			String num_os = "";
			String nr_solicitacao = "";
			String nr_chave_atendimento = "";
			String laudo_atendimento = "";
			
			DaoFluig daoFluig = new DaoFluig();
						
			int i = 0;
			while (i < listErro.size()) {
				
				num_os = listErro.get(i).getAb9Numos().trim();
				
				num_os = num_os.substring(0, num_os.length() - 2);
				
				nr_solicitacao = ab6Repository.buscaNumeroSolicitacao(num_os);
				nr_solicitacao = nr_solicitacao.trim();
				nr_chave_atendimento = listErro.get(i).getAb9Memo1();
				
				if(!nr_solicitacao.equals("") && !nr_solicitacao.equals(null) && !nr_solicitacao.equals("null")) {
					
					//System.out.println("vai chamar funcao q movimenta a solicitacao: " + nr_solicitacao);
					
					laudo_atendimento = buscaLaudoAtendimento(nr_chave_atendimento);
					
					if(laudo_atendimento.equals("") || laudo_atendimento.equals("null") || laudo_atendimento.equals(null)) {
						laudo_atendimento = "sem laudo de atendimento";
					}
					
					//System.out.println("cod baixa: " + listErro.get(i).getAb9Cdbxa().trim());
					
					listResponse = daoFluig.movimentaSolicitacaoFluigVendas(Integer.parseInt(nr_solicitacao), 0, laudo_atendimento, "PERRO", listErro.get(i).getAb9Cdbxa().trim(), num_os);
					
					// se entrar no if abaixo entao deu tudo certo e vai ser add em uma lista pra fazer marcacao para o registro nao aparecer mais pra ser movimentado
					if (listResponse.size() <= 0) {
						marca_atendimento(listErro.get(i).getAb9Numos().trim(), listErro.get(i).getAb9Seq().trim(), listErro.get(i).getRECNO());
					}
					
				}
				
				i++;
			}
			
		}catch(Exception e) {
			//System.out.println("erro na funcao verificaPortabilidade: ");
			e.printStackTrace();
		}
		
		return listResponse;
		
	}
	
	// busca laudo do atendimento, e contatena todos os registros em uma string
	public String buscaLaudoAtendimento(String nr_chave_atendimento) {
		
		List<String> list = sypRepository.buscaObsOS(nr_chave_atendimento);
		String retorno = "";
		
		int y = 0;
		while(y < list.size()) {
			retorno = retorno + list.get(y).toString();
			y++;
		}
		
		return retorno;
	}
	
	// faz marcacao para sair da lista pois ja foi feita integracao
	public void marca_atendimento(String num_os, String sequencia, int recno) {
		ab9Repository.marca_atendimento(recno, num_os, sequencia);
	}

}
