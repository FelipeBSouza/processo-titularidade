package api.rest.dao;

import _103._15._3._10._8086.ARRAYOFLISTA6701;
import _103._15._3._10._8086.ARRAYOFLISTA6702;
import _103._15._3._10._8086.ITENSLIST6701;
import _103._15._3._10._8086.ITENSLIST6702;
import _103._15._3._10._8086.LISTA6701;
import _103._15._3._10._8086.LISTA6702;
import api.rest.venda.nova.DadosSVA;
import api.rest.venda.nova.FoneSVA;
import api.rest.venda.nova.PlanoSVA;
import api.rest.venda.nova.RespostaIntegracao;

public class DaoVendaNovaSVA {
	
	public RespostaIntegracao gravaDados(DadosSVA dados) {
		
		String retorno = "";
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
		
		//PLANOS
		ITENSLIST6702 planos = new ITENSLIST6702();	
		ARRAYOFLISTA6702 arrayPlanos = new ARRAYOFLISTA6702(); // contem todos os planos que serao enviados ao totvs	
				
		// FONES
		ITENSLIST6701 fones = new ITENSLIST6701();		
		ARRAYOFLISTA6701 arrayFones = new ARRAYOFLISTA6701(); // contem todos os fones que serao enviados ao totvs	
				
		// item fone
		LISTA6701 fone;
		
		// se a lista de fones vim vazia entao deve ser tratado para nao dar problema, ja que nao pode ser passada vazia
		if(dados.getDADOS().getFONES().size() <= 0) {
			
			fone = new LISTA6701();
			fone.setDADOS6701("");
			arrayFones.getLISTA6701().add(fone);
			
		}else {
			
			for(FoneSVA linhaFone : dados.getDADOS().getFONES()) {
				
				fone = new LISTA6701();
				fone.setDADOS6701(linhaFone.getVERIFICATIPO() + " ;" + 
						linhaFone.getDDI_FONE() + " ;" + 
						linhaFone.getDDD_FONE() + " ;" + 
						linhaFone.getFONE() + " ;" + 
						linhaFone.getTIPO_TELEFONE() + " ;" + 
						linhaFone.getCOMPLEMENTO_FONE() + " ;" + 
						linhaFone.getTITULAR_FONE() + " ;" + 					
						linhaFone.getCPF_CNPJ() + " ;" + 
						linhaFone.getNOVO_ALTERACAO_FONE() + " ;" + 
						linhaFone.getCOD_AGB() + " ;" + 
						linhaFone.getCOD_RECNO_FONE_VENDA() + " ;" +
						linhaFone.getCOD_ITEM() + " ;"
						);
				
				
				arrayFones.getLISTA6701().add(fone);	
			}
		}
		
		
		
		fones.setREGISTROS6701(arrayFones);
		
		//item plano
		LISTA6702 plano;
		
		for(PlanoSVA linhaPlano : dados.getDADOS().getPLANOS()) {
			
			plano = new LISTA6702();
			
			plano.setDADOS6702(linhaPlano.getCODPRODUTOITEM() + " ;" + 
					linhaPlano.getQUANTIDADEITEM() + " ;" + 
					linhaPlano.getVALORESTIMADO() + " ;" + 
					linhaPlano.getVALORCHEIO() + " ;" + 
					linhaPlano.getTOTALITEM() + " ;" + 
					linhaPlano.getVALORDESCONTO() + " ;" + 
					linhaPlano.getQTMESINI() + " ;" + 
					linhaPlano.getQTMESCOB() + " ;" + 
					linhaPlano.getOBSITEM() + " ;" + 
					linhaPlano.getCOD_TARIFA() + " ;" +
					linhaPlano.getCOD_ITEM() + " ;"
					);
			
			arrayPlanos.getLISTA6702().add(plano);
			
		}
		
		planos.setREGISTROS6702(arrayPlanos);
				
		// envia das informacoes para o ws soap 67
		retorno = ligws67GR(dados.getDADOS().getTPPESSOA(),
				dados.getDADOS().getCDCLI(),
				dados.getDADOS().getLOJA(),
				dados.getDADOS().getVEND1(),
				dados.getDADOS().getOBS_OS1(),
				dados.getDADOS().getOBS_OS2(),
				dados.getDADOS().getOPERADOR(),
				dados.getDADOS().getCDPGTO(),
				dados.getDADOS().getOPERACAO(),
				dados.getDADOS().getMIDIA(),
				dados.getDADOS().getTMK(),
				dados.getDADOS().getTPCLI(),
				dados.getDADOS().getTPCTR(),
				dados.getDADOS().getPRAZO(),
				dados.getDADOS().getVALCTR(),
				dados.getDADOS().getUDIAFE(),
				dados.getDADOS().getASSCLI(),
				dados.getDADOS().getOBSSUA(),
				dados.getDADOS().getCODENDC(),
				dados.getDADOS().getCODENDI(),
				dados.getDADOS().getCX(),
				dados.getDADOS().getPT(),
				dados.getDADOS().getCCGCLI(),
				dados.getDADOS().getNRSOLIC(),
				dados.getDADOS().getFDIG(),
				dados.getDADOS().getURLGRV(),		
				dados.getDADOS().getTEMINS(),
				planos, fones);	
		
		if(retorno.equals("")) {
			respostaIntegracao.setMENSAGEM("OK");
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("201");
			
		}else {
			respostaIntegracao.setMENSAGEM(retorno.toString());
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("500");
		}
		
		return respostaIntegracao;
		
	}
	

	public String ligws67GR(String tppessoa, String cdcli, String loja, String vend1, String obsOS1, String obsOS2, String operador, String cdpgto, String operacao, String midia, String tmk, String tpcli, String tpctr, String prazo, String valctr, String udiafe, String asscli, String obssua, String codendc, String codendi, String cx, String pt, String ccgcli, String nrsolic, String fdig, String urlgrv, String temINS, _103._15._3._10._8086.ITENSLIST6702 dados6702, _103._15._3._10._8086.ITENSLIST6701 dados6701) {
        _103._15._3._10._8086.LIGWS067 service = new _103._15._3._10._8086.LIGWS067();
        _103._15._3._10._8086.LIGWS067SOAP port = service.getLIGWS067SOAP();
        return port.ligws67GR(tppessoa, cdcli, loja, vend1, obsOS1, obsOS2, operador, cdpgto, operacao, midia, tmk, tpcli, tpctr, prazo, valctr, udiafe, asscli, obssua, codendc, codendi, cx, pt, ccgcli, nrsolic, fdig, urlgrv, temINS, dados6702, dados6701);
    }
}
