package api.rest.dao;

import _103._15._3._10._8086.ARRAYOFLISTA0168;
import _103._15._3._10._8086.ARRAYOFLISTA0268;
import _103._15._3._10._8086.ARRAYOFLISTA0368;
import _103._15._3._10._8086.ITENSLIST0168;
import _103._15._3._10._8086.ITENSLIST0268;
import _103._15._3._10._8086.ITENSLIST0368;
import _103._15._3._10._8086.LISTA0168;
import _103._15._3._10._8086.LISTA0268;
import _103._15._3._10._8086.LISTA0368;
import api.rest.retencao.sva.DadosRetencaoSVA;
import api.rest.retencao.sva.FoneSVARetencao;
import api.rest.retencao.sva.PlanosContratoRetencaoSVA;
import api.rest.retencao.sva.PlanosRenovadosRetencaoSVA;
import api.rest.venda.nova.RespostaIntegracao;

public class DaoRetencaoSVA {	
	
	public RespostaIntegracao gravaDadosRetencaoTotvs(DadosRetencaoSVA dadosRetencao) {	
		
		String retorno = "";
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
		
		//PLANOS DA TABELA DOS ITENS JA EXISTENTES NO CONTRATO
		ITENSLIST0268 planosTb1 = new ITENSLIST0268();							
		ARRAYOFLISTA0268 arrayPlanosTb1 = new ARRAYOFLISTA0268(); // contem todos os planos que o cliente ja possui
		
		//PLANOS DA TABELA DE ITENS A SEREM RENOVADOS/CONTRATADOS
		ITENSLIST0368 planosTb2 = new ITENSLIST0368();							
		ARRAYOFLISTA0368 arrayPlanosTb2 = new ARRAYOFLISTA0368(); // contem todos os planos que o cliente vai contratar/renovar
		
		//TABELA DE FONES PORTABILIDADE E LINHA NOVA
		ITENSLIST0168 fones = new ITENSLIST0168();							
		ARRAYOFLISTA0168 arrayFones = new ARRAYOFLISTA0168(); // contem todos os fones de portabilidade e/ou linha nova
		
		// item plano tabela itens ja contratados
		LISTA0268 planoTb1;
		// item plano tabela itens a serem renovados/contratados
		LISTA0368 planoTb2;
		// item fone
		LISTA0168 fone;		
		
		//ARRAY DE TELEFONE
		if(dadosRetencao.getDADOS().get_DADOS0155().size() <= 0) {
			
			fone = new LISTA0168();
			fone.setDADOS0168("");
			arrayFones.getLISTA0168().add(fone);
			
		}else {
			
			//array de fones
			for(FoneSVARetencao linhaFone : dadosRetencao.getDADOS().get_DADOS0155()) {
				
				fone = new LISTA0168();
				fone.setDADOS0168(linhaFone.getPADDDI() + " ;" + 
						linhaFone.getPADDD()  + " ;" + 
						linhaFone.getPATEL()  + " ;" + 
						linhaFone.getPATIPO()  + " ;" + 
						linhaFone.getPAPADRAO()  + " ;" + 
						linhaFone.getPAOP()  + " ;" + 
						linhaFone.getCDTELEF()  + " ;" + 
						linhaFone.getPATIPO2()  + " ;" + 
						linhaFone.getPACOMP()  + " ;" + 
						linhaFone.getPAOWNER()  + " ;" + 
						linhaFone.getPACPFOW()  + " ;" +
						linhaFone.getIDITEM()  + " ;" 
						
						);
				
				arrayFones.getLISTA0168().add(fone);	
				
			}
			
		}	
		
		fones.setREGISTROS0168(arrayFones);	
		
		// ARRAY DE PLANOS TB1
		if(dadosRetencao.getDADOS().get_DADOS0255().size() <= 0) {
			
			planoTb1 = new LISTA0268();
			planoTb1.setDADOS0268("");
			arrayPlanosTb1.getLISTA0268().add(planoTb1);
			
		}else{
			
			for(PlanosContratoRetencaoSVA linhaItemTb1 : dadosRetencao.getDADOS().get_DADOS0255()) {
				
				planoTb1 = new LISTA0268();
				planoTb1.setDADOS0268(linhaItemTb1.getP1CODP() + " ;" + 
						linhaItemTb1.getP1ITEM() + " ;" + 
						linhaItemTb1.getP1CODEND() + " ;" + 
						linhaItemTb1.getP1IDENT() + " ;" + 
						linhaItemTb1.getP1CODPN() + " ;" + 
						linhaItemTb1.getP1CODPAA()  + " ;" 
						);
				
				arrayPlanosTb1.getLISTA0268().add(planoTb1);
				
			}			
			
		}
		
		planosTb1.setREGISTROS0268(arrayPlanosTb1);
		
		// ARRAY DE PLANOS TB2
		if(dadosRetencao.getDADOS().get_DADOS0355().size() <= 0) {
			
			planoTb2 = new LISTA0368();
			planoTb2.setDADOS0368("");
			arrayPlanosTb2.getLISTA0368().add(planoTb2);
			
		}else{
			
			for(PlanosRenovadosRetencaoSVA linhaItemTb2 : dadosRetencao.getDADOS().get_DADOS0355()) {
				
				planoTb2 = new LISTA0368();
				planoTb2.setDADOS0368(linhaItemTb2.getP2CODP() + " ;" + 
						linhaItemTb2.getP2ITEM() + " ;" + 
						linhaItemTb2.getP2CODANT() + " ;" + 
						linhaItemTb2.getP2QUANT() + " ;" + 
						linhaItemTb2.getP2VLMES() + " ;" + 
						linhaItemTb2.getP2VALDES() + " ;" + 
						linhaItemTb2.getP2MSG() + " ;" + 
						linhaItemTb2.getP2MESINI() + " ;" + 
						linhaItemTb2.getP2UMESCO() + " ;" + 
						linhaItemTb2.getP2VLUNIT() + " ;" + 
						linhaItemTb2.getP2CODPA() + " ;" + 
						linhaItemTb2.getP2CODTAR() + " ;" + 
						linhaItemTb2.getP2UVLCHE() + " ;" + 						
						linhaItemTb2.getP2IDITEM()  + " ;"
						
						);
				
				arrayPlanosTb2.getLISTA0368().add(planoTb2);
				
			}
			
		}
		
		planosTb2.setREGISTROS0368(arrayPlanosTb2);
			
		retorno = ligws68GR(dadosRetencao.getDADOS().getTPPESSOA(),
				dadosRetencao.getDADOS().getNCTR(),
				dadosRetencao.getDADOS().getCDCLI(),
				dadosRetencao.getDADOS().getLOJA(),
				dadosRetencao.getDADOS().getVEND1(),
				dadosRetencao.getDADOS().getOBS_OS1(),
				dadosRetencao.getDADOS().getOBS_OS2(),
				dadosRetencao.getDADOS().getOPERADOR(),
				dadosRetencao.getDADOS().getCDPGTO(),
				dadosRetencao.getDADOS().getOPERACAO(),
				dadosRetencao.getDADOS().getMIDIA(),
				dadosRetencao.getDADOS().getTMK(),
				dadosRetencao.getDADOS().getTPCLI(),
				dadosRetencao.getDADOS().getTPCTR(),
				dadosRetencao.getDADOS().getPRAZO(),
				dadosRetencao.getDADOS().getVALCTR(),
				dadosRetencao.getDADOS().getUDIAFE(),
				dadosRetencao.getDADOS().getASSCLI(),
				dadosRetencao.getDADOS().getOBSSUA(),
				dadosRetencao.getDADOS().getCODENDC(),
				dadosRetencao.getDADOS().getCX(),
				dadosRetencao.getDADOS().getPT(),
				dadosRetencao.getDADOS().getCCGCLI(),
				dadosRetencao.getDADOS().getNRSOLIC(),
				dadosRetencao.getDADOS().getURLGRV(),
				dadosRetencao.getDADOS().getFTDIGTAL(),
				dadosRetencao.getDADOS().getCODINS(),
				dadosRetencao.getDADOS().getOBSLAUDO(),
				dadosRetencao.getDADOS().getMOVTO(),
				dadosRetencao.getDADOS().getDTSOLINF(),
				dadosRetencao.getDADOS().getCANCRETR(),
				dadosRetencao.getDADOS().getTEM_INS(),
				planosTb1, planosTb2,fones
				);
		
		if(retorno.equals("")) {
			respostaIntegracao.setMENSAGEM("OK");
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("201");
			
		}else {
			respostaIntegracao.setMENSAGEM(retorno.toString());
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("500");
		}
		
		return respostaIntegracao;
		
	}	
	
	private static String ligws68GR(String tppessoa, String nctr, String cdcli, String loja, String vend1, String obsOS1, String obsOS2, String operador, String cdpgto, String operacao, String midia, String tmk, String tpcli, String tpctr, String prazo, String valctr, String udiafe, String asscli, String obssua, String codendc, String cx, String pt, String ccgcli, String nrsolic, String urlgrv, String ftdigtal, String codins, String obslaudo, String movto, String dtsolinf, String cancretr, String temINS, _103._15._3._10._8086.ITENSLIST0268 dados0268, _103._15._3._10._8086.ITENSLIST0368 dados0368, _103._15._3._10._8086.ITENSLIST0168 dados0168) {
        _103._15._3._10._8086.LIGWS068 service = new _103._15._3._10._8086.LIGWS068();
        _103._15._3._10._8086.LIGWS068SOAP port = service.getLIGWS068SOAP();
        return port.ligws68GR(tppessoa, nctr, cdcli, loja, vend1, obsOS1, obsOS2, operador, cdpgto, operacao, midia, tmk, tpcli, tpctr, prazo, valctr, udiafe, asscli, obssua, codendc, cx, pt, ccgcli, nrsolic, urlgrv, ftdigtal, codins, obslaudo, movto, dtsolinf, cancretr, temINS, dados0268, dados0368, dados0168);
    }
}
