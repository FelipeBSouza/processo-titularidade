package api.rest.dao;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import api.rest.classes.auxiliar.IntegracaoAutentiqueDto;
import api.rest.encerra.os.RestJsonPrincipalEncerraOS;
import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.ControleOS;
import api.rest.model.integrador.Funcionario;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.TipoFinalidade;
import api.rest.model.integrador.equipamento.Modelo;
import api.rest.model.integrador.fibra.PortaSplitter;
import api.rest.model.internet.Radacct;
import api.rest.model.telefonia.Numero;
import api.rest.model.telefonia.Planos;
import api.rest.model.telefonia.Sippeers;
import api.rest.response.ResponseDaoLigueApi;

// CLASSE UTILIZADA PRA FAZER INTEGRACAO COM A API QUE CONTROLA O BANCO DE DADOS POSTGRES
// todas as funcoes abaixo estao na api LigueApi
public class DaoLigueApi { // http://172.18.0.2:8080
	
	String ip = "http://172.18.0.2:8080"; // "http://172.18.0.2:8080"
	//String ip = "http://10.3.99.66:8080"; 
	//String ip = "http://10.3.15.64:3001";

	public Modelo consultaModeloDescricao(String ds_modelo) {

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Modelo> response = restTemplate.exchange(ip + "/Iclass/Modelo?ds_modelo=" + ds_modelo.trim(), HttpMethod.GET, null, new ParameterizedTypeReference<Modelo>() {
		});

		Modelo modelo = response.getBody();

		return modelo;

	}

	public TabInternet consultaVelocidadeNet(String cod_ibge, String prod_totvs) {

		// SELECT * FROM integrador.tabinternet WHERE cod_ibge = ?1 AND produto_totvs = ?2 ORDER BY produto_totvs

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<TabInternet> response = restTemplate.exchange(ip + "/Iclass/velNet?codIbge=" + cod_ibge + "&prod_totvs=" + prod_totvs, HttpMethod.GET, null, new ParameterizedTypeReference<TabInternet>() {
		});

		TabInternet tabInternet = response.getBody();

		return tabInternet;

	}

	public Funcionario consultaFuncionario(String cod_funcionario) {

		// SELECT * FROM integrador.funcionario WHERE cd_funcionario = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Funcionario> response = restTemplate.exchange(ip + "/Iclass/Funcionario?cod_funcionario=" + cod_funcionario, HttpMethod.GET, null, new ParameterizedTypeReference<Funcionario>() {
		});

		Funcionario funcionario = response.getBody();

		return funcionario;

	}

	public TipoFinalidade consultaTipoFinalidade(String tp_fidelidade) {

		// SELECT * FROM integrador.funcionario WHERE cd_funcionario = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<TipoFinalidade> response = restTemplate.exchange(ip + "/Iclass/Finalidade?cd_fidelidade=" + tp_fidelidade, HttpMethod.GET, null, new ParameterizedTypeReference<TipoFinalidade>() {
		});

		TipoFinalidade tipoFinalidade = response.getBody();

		return tipoFinalidade;

	}

	public PortaSplitter consultaPortaSplitter(int cod) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<PortaSplitter> response = restTemplate.exchange(ip + "/Iclass/PortaSplitter?cd_porta=" + cod, HttpMethod.GET, null, new ParameterizedTypeReference<PortaSplitter>() {
		});

		PortaSplitter portaSplitter = response.getBody();

		return portaSplitter;

	}

	public Planos consultaPlanosYate(int cod) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Planos> response = restTemplate.exchange(ip + "/Iclass/Planos?id_plano=" + cod, HttpMethod.GET, null, new ParameterizedTypeReference<Planos>() {
		});

		Planos planos = response.getBody();

		return planos;

	}

	public Numero consultaNumeroYate(String num, String area) {

		Numero numero = new Numero();

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Numero> response = restTemplate.exchange(ip + "/Iclass/FoneIclassNumero?fone=" + num + "&area=" + area, HttpMethod.GET, null, new ParameterizedTypeReference<Numero>() {
		});

		numero = response.getBody();

		//System.out.println("entrou no try, numero retorno é: " + numero.getId());

		return numero;

	}

	// recebe classe cliente e transforma essa classe em json para enviar a LigueApi
	// o retorno da chamada da api informa se deu certo ou nao o cadastro do cliente la no postgres
	public ResponseDaoLigueApi salvarCliente(Cliente clienteParam) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1
		Gson gson = new Gson();
		String json = gson.toJson(clienteParam);

		//System.out.println(json);

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ResponseDaoLigueApi> response = restTemplate.exchange(ip + "/cliente/cadastrar?jsonCliente=" + json, HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDaoLigueApi>() {
		});

		ResponseDaoLigueApi responseDaoLigueApi = response.getBody(); // retorno se deu erro ou nao

		return responseDaoLigueApi;

	}

	// recebe classe sippeers e transforma essa classe em json para enviar a LigueApi
	// o retorno da chamada da api informa se deu certo ou nao o cadastro la no postgres
	public ResponseDaoLigueApi salvarSippeers(Sippeers sippeersParam) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1
		Gson gson = new Gson();
		String json = gson.toJson(sippeersParam);

		//System.out.println(json);

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ResponseDaoLigueApi> response = restTemplate.exchange(ip + "/sippeers/cadastrar?jsonSippeers=" + json, HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDaoLigueApi>() {
		});

		ResponseDaoLigueApi responseDaoLigueApi = response.getBody(); // retorno se deu erro ou nao

		return responseDaoLigueApi;

	}

	public ResponseDaoLigueApi alteraNumero(Numero numeroParam) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1
		Gson gson = new Gson();
		String json = gson.toJson(numeroParam);

		//System.out.println(json);

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ResponseDaoLigueApi> response = restTemplate.exchange(ip + "/numero/altera?jsonNumero=" + json, HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDaoLigueApi>() {
		});

		ResponseDaoLigueApi responseDaoLigueApi = response.getBody(); // retorno se deu erro ou nao

		return responseDaoLigueApi;

	}

	public List<Radacct> consultaRadacct(String username) {

		// SELECT * FROM internet.radacct WHERE username = ?1 order by username

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<Radacct>> response = restTemplate.exchange(ip + "/Iclass/RadacctUsername?username=" + username, HttpMethod.GET, null, new ParameterizedTypeReference<List<Radacct>>() {
		});

		List<Radacct> radaccts = response.getBody();

		return radaccts;

	}

	public Sippeers consultaSippersPorId(Long id) {

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Sippeers> response = restTemplate.exchange(ip + "/sippeers/consultaId?id=" + id, HttpMethod.GET, null, new ParameterizedTypeReference<Sippeers>() {
		});

		Sippeers sippeers = response.getBody();

		return sippeers;

	}
	
	public String consultaSequenciaOS(String nr_os) {

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ControleOS> response = restTemplate.exchange(ip + "/Iclass/consultaSeq?nr_os=" + nr_os, HttpMethod.POST, null, new ParameterizedTypeReference<ControleOS>() {
		});

		ControleOS controleOS = response.getBody();

		return controleOS.getNr_os_sequencial();

	}
	
	public String consultaSequencialAtualOS(String nr_os) {

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ControleOS> response = restTemplate.exchange(ip + "/Iclass/consultaSequencialAtualOS?nr_os=" + nr_os, HttpMethod.GET, null, new ParameterizedTypeReference<ControleOS>() {
		});

		ControleOS controleOS = response.getBody();

		return controleOS.getNr_os_sequencial();

	}
	
	public String consultaExisteOSTabela(String nr_os) {
		
		
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ControleOS> response = restTemplate.exchange(ip + "/Iclass/consultaOSTabela?nr_os=" + nr_os, HttpMethod.GET, null, new ParameterizedTypeReference<ControleOS>() {
		});

		ControleOS controleOS = response.getBody();		
		
		//return controleOS.getNr_os_sequencial();
		return controleOS.getNr_os();

	}
	
	public List<IntegracaoAutentiqueDto> consultaContratosASeremAssinados() {

		List<IntegracaoAutentiqueDto> list = new ArrayList<>();

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<IntegracaoAutentiqueDto>> response = restTemplate.exchange(ip + "/autentique/listaContratos", HttpMethod.GET, null, new ParameterizedTypeReference<List<IntegracaoAutentiqueDto>>() {
		});

		list = response.getBody();

		return list;

	}
	
	public ResponseEntity<String> marcaSolicitacao(String nr_solicitacao) {
	
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> response = restTemplate.exchange(ip + "/autentique/marcaSolicitacao?id_solicitacao=" + nr_solicitacao, HttpMethod.PUT, null, new ParameterizedTypeReference<String>() {
		});
		
		//System.out.println("retorno marca solicitacao: " + response);

		return response;

	}
	
	public List<IntegracaoAutentiqueDto> buscaContratosPorSolicitacao(String nr_solicitacao) {
		
		RestTemplate restTemplate = new RestTemplate();
		List<IntegracaoAutentiqueDto> list = new ArrayList<>();

		ResponseEntity<List<IntegracaoAutentiqueDto>> response = restTemplate.exchange(ip + "/autentique/listaContratosSolicitacao?nr_solicitacao=" + nr_solicitacao, HttpMethod.GET, null, new ParameterizedTypeReference<List<IntegracaoAutentiqueDto>>() {
		});

		list = response.getBody();

		return list;

	}
	
	public List<IntegracaoAutentiqueDto> buscaContratosPorSolicitacaoTitularidade(String nr_solicitacao) {
		
		RestTemplate restTemplate = new RestTemplate();
		List<IntegracaoAutentiqueDto> list = new ArrayList<>();

		ResponseEntity<List<IntegracaoAutentiqueDto>> response = restTemplate.exchange(ip + "/autentique/listaContratosSolicitacao?nr_solicitacao=" + nr_solicitacao, HttpMethod.GET, null, new ParameterizedTypeReference<List<IntegracaoAutentiqueDto>>() {
		});

		list = response.getBody();

		return list;

	}
	
	public IntegracaoAutentiqueDto gravaLink(String id_contrato, String link_ctr_assinado, String assinado, String obs_rejeitado, String movement_status) {
		
		RestTemplate restTemplate = new RestTemplate();
		IntegracaoAutentiqueDto integracaoAutentiqueDto = new IntegracaoAutentiqueDto();

		ResponseEntity<IntegracaoAutentiqueDto> response = restTemplate.exchange(ip + "/autentique/gravaLink?id_contrato=" + id_contrato + "&link_assinado=" + link_ctr_assinado + "&assinado=" + assinado + "&obs_rejeitado=" + obs_rejeitado + "&movement_status=" + movement_status, HttpMethod.GET, null, new ParameterizedTypeReference<IntegracaoAutentiqueDto>() {
		});
		
		integracaoAutentiqueDto = response.getBody();
		
		return integracaoAutentiqueDto;

	}
	
	public List<String> listaSolicitacoesASeremMovimentadas() {
		
		RestTemplate restTemplate = new RestTemplate();
		List<String> list;

		ResponseEntity<List<String>>  response = restTemplate.exchange(ip + "/autentique/listaSolicitacoesASeremMovimentadas", HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>() {
		});
		
		list = response.getBody();

		return list;

	}
	
	public ResponseEntity<String> deletaRegistroContrato(String id_contrato) {
		
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> response = restTemplate.exchange(ip + "/autentique/delete?id_contrato=" + id_contrato, HttpMethod.DELETE, null, new ParameterizedTypeReference<String>() {
		});
		
		//System.out.println("retorno deletaRegistroContrato: " + response);

		return response;

	}
	
	public ResponseEntity<String> deletaRegistroContratoPorSolicitacao(String nr_solicitacao) {
		
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> response = restTemplate.exchange(ip + "/autentique/deletePorSolicitacao?nr_solicitacao=" + nr_solicitacao, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
		});
		
		//System.out.println("retorno deletaRegistroContrato: " + response);

		return response;

	}
	
	public List<IntegracaoAutentiqueDto> buscaTodosContratosPorSolicitacao(String nr_solicitacao) {

		RestTemplate restTemplate = new RestTemplate();
		List<IntegracaoAutentiqueDto> list = new ArrayList<>();

		ResponseEntity<List<IntegracaoAutentiqueDto>> response = restTemplate.exchange(ip + "/autentique/listaTodosContratosSolicitacao?nr_solicitacao=" + nr_solicitacao, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<IntegracaoAutentiqueDto>>() {
				});

		list = response.getBody();

		return list;

	}
	
	public String gravaDadosOSPostgres(RestJsonPrincipalEncerraOS ORDENS) {
		
		String retorno = "OK";

		try {
			RestTemplate restTemplate = new RestTemplate();
			URI uri = new URI(ip + "/Iclass/gravaDadosOS");

			ResponseEntity<String> result = restTemplate.postForEntity(uri, ORDENS, String.class);

			retorno = result.getBody();
			
		}catch(Exception erro) {
			//System.out.println("deu erro na funcao iclass gravaDadosOSPostgres: " + erro);
			erro.printStackTrace();
			
			retorno = "ERRO: " + erro;
		}
		
		//System.out.println("retorno da funcao gravaDadosOSPostgres " + retorno);

		return retorno;

	}	
	
	public ResponseEntity<String> atualizaStatusGetCloseOrdens(String nr_solicitacao, String fluxo, String status, String nr_os) {
		
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> response = restTemplate.exchange(ip + "/Iclass/atualizaStatus?nr_os=" + nr_os + "&status=" + status + "&fluxo=" + fluxo + "&nr_solicitacao=" + nr_solicitacao, HttpMethod.PUT, null, new ParameterizedTypeReference<String>() {
		});
		
		//System.out.println("retorno atualizaStatusGetCloseOrdens: " + response);

		return response;

	}
	
	//----------------------------------METODOS TITULARIDADE ----------------------------------------//
	
	public List<IntegracaoAutentiqueDto> consultaContratosASeremAssinadosTitularidade() {

		List<IntegracaoAutentiqueDto> list = new ArrayList<>();

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<IntegracaoAutentiqueDto>> response = restTemplate.exchange(ip + "/autentique/listaContratosTitularidade", HttpMethod.GET, null, new ParameterizedTypeReference<List<IntegracaoAutentiqueDto>>() {
		});

		list = response.getBody();
		
		//System.out.println(list);

		return list;

	}
	
	public List<String> listaSolicitacoesASeremMovimentadasTitularidade() {
		
		RestTemplate restTemplate = new RestTemplate();
		List<String> list;

		ResponseEntity<List<String>>  response = restTemplate.exchange(ip + "/autentique/listaSolicitacoesASeremMovimentadasTitularidade", HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>() {
		});
		
		list = response.getBody();

		return list;

	}
	
	public List<IntegracaoAutentiqueDto> buscaContratosPorIdContratoTitularidade(String id_contrato) {
		
		RestTemplate restTemplate = new RestTemplate();
		List<IntegracaoAutentiqueDto> list = new ArrayList<>();

		ResponseEntity<List<IntegracaoAutentiqueDto>> response = restTemplate.exchange(ip + "/autentique/listaContratosIdContratoTitularidade?id_contrato=" + id_contrato, HttpMethod.GET, null, new ParameterizedTypeReference<List<IntegracaoAutentiqueDto>>() {
		});

		list = response.getBody();

		return list;

	}
	
	public List<IntegracaoAutentiqueDto> buscaContratosPorSolicitacaoTitularidade2(String nr_solicitacao) {
		
		RestTemplate restTemplate = new RestTemplate();
		List<IntegracaoAutentiqueDto> list = new ArrayList<>();

		ResponseEntity<List<IntegracaoAutentiqueDto>> response = restTemplate.exchange(ip + "/autentique/listaContratosSolicitacaoTitularidade2?nr_solicitacao=" + nr_solicitacao, HttpMethod.GET, null, new ParameterizedTypeReference<List<IntegracaoAutentiqueDto>>() {
		});

		list = response.getBody();

		return list;

	}
	
	public ResponseEntity<String> marcaSolicitacaoTitularidade(String id_contrato) {
		
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> response = restTemplate.exchange(ip + "/autentique/marcaSolicitacaoTitularidade?id_contrato=" + id_contrato, HttpMethod.PUT, null, new ParameterizedTypeReference<String>() {
		});
		
		//System.out.println("retorno marca solicitacao: " + response);

		return response;

	}
	
	
	
	
	//-----------------------------------------------------------------------------------------------//
}
