package api.rest.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import api.rest.autentique.ResponseAutentique;
import api.rest.autentique.ResponseReenvioEmail;
import api.rest.classes.auxiliar.IntegracaoAutentiqueDto;
import api.rest.response.ResponseAutentiqueEnvioContrato;

public class DaoAutentique {
	
	DaoLigueApi daoLigueApi = new DaoLigueApi();
	DaoFluig daoFluig = new DaoFluig();
	
	
	// funcao principal para ver quais contratos precisam ser verificados se foram assinados
	// caso tenham sido assinados e gravado o link do contrato assinado para ser usado posteriormente
	public List<ResponseAutentique> listaContratosASeremAssinados() {
		
		int i = 0;
		
		ResponseAutentique responseAutentique;		
		List<IntegracaoAutentiqueDto> list = new ArrayList<>();
		List<ResponseAutentique> listRetornoApi = new ArrayList<>();
		
		try {
			
			list = daoLigueApi.consultaContratosASeremAssinados();
						
			while(i < list.size()) {
				
				responseAutentique = buscaContratosAssinados(list.get(i).getId_contrato().trim(), list.get(i).getNr_solicitacao().trim(), list.get(i).getAtividade(),0);	
				
				if(responseAutentique != null) {
					
					if(responseAutentique.getCod_status()=="200") { // se o contrato foi assinado ou se foi rejeitado
						
						if(!responseAutentique.getObs_rejeitado().equals("OK")){						
							
							listRetornoApi.addAll(gravaLink(responseAutentique.getId_contrato().trim(), responseAutentique.getLink_contrato_assinado(),"N",responseAutentique.getObs_rejeitado(), "awaiting"));
						
						}else {
							
							listRetornoApi.addAll(gravaLink(responseAutentique.getId_contrato().trim(), responseAutentique.getLink_contrato_assinado(),"S","OK", "process"));
						}			
						
					}else { // se deu algum problema entra no else
											
						listRetornoApi.add(responseAutentique);
					}
					
				}
				
				Thread.sleep(1000);								
				i++;
			}
			
		}catch(Exception e) {
			//System.out.println("deu erro listagem de contratos: " + e);
						
			responseAutentique = new ResponseAutentique();
			responseAutentique.setCod_status("500");
			responseAutentique.setStatus("ERROR: " + e);
			responseAutentique.setId_contrato("");
			responseAutentique.setNr_solicitacao("");
			
			listRetornoApi.add(responseAutentique);
			
			e.printStackTrace();
		}
			
		return listRetornoApi;
		
	}	
	
	// FUNCAO PRINCIPAL PARA VER QUAIS CONTRATOS PRECISAM SER VERIFICADOS SE FORAM ASSINADOS
	// CASO TENHAM SIDO ASSINADOS E GRAVADO O LINK DO CONTRATO ASSINADO PARA SER USADO POSTERIORMENTE
	public List<ResponseAutentique> listaContratosASeremAssinadosTitularidade() {
			
			int i = 0;
			
			ResponseAutentique responseAutentique;		
			List<IntegracaoAutentiqueDto> list = new ArrayList<>();
			List<ResponseAutentique> listRetornoApi = new ArrayList<>();
			
			try {
				
				list = daoLigueApi.consultaContratosASeremAssinadosTitularidade();
				
				//System.out.println("TAMANHO DA LISTA DE CONTRATOS: " + list.size());
							
				while(i < list.size()) {
					
					responseAutentique = buscaContratosAssinados(list.get(i).getId_contrato().trim(), list.get(i).getNr_solicitacao().trim(), list.get(i).getAtividade(),0);	
					
					if(responseAutentique != null) {
						
						if(responseAutentique.getCod_status()=="200") { // se o contrato foi assinado ou se foi rejeitado
							
							// CONTRATO REJEITADO
							if(!responseAutentique.getObs_rejeitado().equals("OK")){						
								
								// GRAVA O LINK DO CONTRATO ASSINADO										
								listRetornoApi.addAll(gravaLink(responseAutentique.getId_contrato().trim(), responseAutentique.getLink_contrato_assinado(),"N",responseAutentique.getObs_rejeitado(), "awaiting"));
							
							}else {
								
								// CONTRATO ASSINADO
								listRetornoApi.addAll(gravaLink(responseAutentique.getId_contrato().trim(), responseAutentique.getLink_contrato_assinado(),"S","OK", "process"));
							}			
				
						}else {
							
							// SE DEU ALGUM PROBLEMA ENTRA NO ELSE
							listRetornoApi.add(responseAutentique);
						}
					}else {
						//System.out.println("Valor nulo Skipando para o próximo");
					}
					
					Thread.sleep(1000); //1000
									
					i++;
				}
				
				//System.out.println("tamanho da variavel i: " + i);
				
				// CHAMAR AQUI FUNCAO QUE MOVIMENTA AS SOLICITACOES
				listRetornoApi.addAll(buscaSolicitacoesASeremMovimentadasTitularidade());
				
			}catch(Exception e) {
				//System.out.println("deu erro listagem de contratos: " + e);
							
				responseAutentique = new ResponseAutentique();
				responseAutentique.setCod_status("500");
				responseAutentique.setStatus("ERROR: " + e);
				responseAutentique.setId_contrato("");
				responseAutentique.setNr_solicitacao("");
				
				listRetornoApi.add(responseAutentique);
				
				e.printStackTrace();
			}
				
			return listRetornoApi;
			
		}	
		
	// funcao que grava o link do contrato assinado
	public List<ResponseAutentique> gravaLink(String id_contrato, String link_ctr_assinado, String assinado, String obs_rejeitado, String movement_status) {		
		
		ResponseAutentique responseAutentique;
		List<ResponseAutentique> listErros = new ArrayList<>();
		IntegracaoAutentiqueDto integracaoAutentiqueDto;
		
		try {
			
			//System.out.println("obs teste: " + obs_rejeitado);
			
			if(obs_rejeitado.isEmpty() || obs_rejeitado.equals("null") || obs_rejeitado.equals(null)) {
				obs_rejeitado = "sem observacao do cliente";
			}
			
			integracaoAutentiqueDto = new IntegracaoAutentiqueDto();
			integracaoAutentiqueDto = daoLigueApi.gravaLink(id_contrato, link_ctr_assinado, assinado, obs_rejeitado, movement_status);
						
			responseAutentique = new ResponseAutentique();
			responseAutentique.setCod_status(integracaoAutentiqueDto.getCod_retorno());
			responseAutentique.setStatus(integracaoAutentiqueDto.getDesc_retorno());
			responseAutentique.setId_contrato(integracaoAutentiqueDto.getId_contrato());
			responseAutentique.setNr_solicitacao(integracaoAutentiqueDto.getNr_solicitacao());
			responseAutentique.setLink_contrato_assinado(integracaoAutentiqueDto.getContrato_assinado());
			responseAutentique.setAtividade(integracaoAutentiqueDto.getAtividade());
			listErros.add(responseAutentique);					
			
		}catch(Exception e) {
						
			responseAutentique = new ResponseAutentique();
			responseAutentique.setCod_status("500");
			responseAutentique.setStatus("ERROR: " + e);
			responseAutentique.setId_contrato("");
			responseAutentique.setNr_solicitacao("");
			responseAutentique.setLink_contrato_assinado("");
			responseAutentique.setAtividade(0);
			listErros.add(responseAutentique);
		}
		
		return listErros;
		
	}
	
	public Object buscaContrato(String id_documento) throws IOException {
		
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,
				"{\"query\":\"{document(id: \\\"" + id_documento.toString() + "\\\") {id name files { original signed } signatures {email_events {refused reason}, signed { ...event },rejected { ...event }}}} fragment event on Event {ipv4 ipv6 reason created_at}\"}");
		Request request = new Request.Builder().url("https://api.autentique.com.br/v2/graphql").post(body).addHeader("authorization", "Bearer 5d0fd10abd8f33f2a0d3590dfbbc081c39084171811f722ada6347f26f4ff2dc")
				.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache").addHeader("postman-token", "8bbd2688-6a03-3218-d9ed-d3e7f5b50b37").build();

		Response response = client.newCall(request).execute();
		
		JSONObject jObjPrincipal = new JSONObject(response.body().string());
		
		//System.out.println("dados vindos do autentique: " + jObjPrincipal.toString());
		
		if(!jObjPrincipal.has("data")) {
			
			//System.out.println("Busca Contrato ERRO - Id_documento: " + id_documento);
			//System.out.println(jObjPrincipal);
			return null;
		}		
		
		Object dadosAutentique = jObjPrincipal.get("data");
		
		//System.out.println("Busca Contrato");
		//System.out.println(dadosAutentique);
		
		return dadosAutentique;
	}
	

	// essa funcao faz integracao com autentique e verifica se o contrato passado por parametro ja foi assinado
	public ResponseAutentique buscaContratosAssinados(String id_documento, String nr_solicitacao, int atividade, int execucoes) throws InterruptedException {

		String doc_assinado = "";
		String assinado = ""; // se = null entao o contrato nao foi assinado
		String obs_rejeitado = ""; // observacao caso o contrato tenha sido rejeitado
		String email_invalido = "";
		String rejeitado = "";
		
		int s = 0;
		int cont_ass_null = 0; // controle da quantidade de assinaturas
		
		ResponseAutentique responseAutentique;
		
		try {
			
			Object dadosAutentique = buscaContrato(id_documento);
			
			if(dadosAutentique == null) {
				Thread.sleep(3000);
				
				if(execucoes >= 5) {
					//System.out.println("Quantidade limite de tentativas excedidas, id_documento : " + id_documento);
					return null;
				}
				
				return buscaContratosAssinados(id_documento, nr_solicitacao, atividade, execucoes++);
			}
			
			String dadosDocumento = ((JSONObject) dadosAutentique).get("document").toString();
			
			//System.out.println("Dados documento");
			//System.out.println(dadosDocumento);

			// validacao para ver se o autentique retornou os dados do contrato, para nao dar erro de {"document":null}
			if(!dadosDocumento.equals("null")) {
			//if(jObjPrincipal.toString().length() > 35){
				
				Object ObjDocument = ((JSONObject) dadosAutentique).get("document");
				JSONObject jObjDocument = new JSONObject(ObjDocument.toString());
			
				Object ObjFiles = jObjDocument.get("files");
				JSONObject jObjFiles = new JSONObject(ObjFiles.toString());

				// //System.out.println(jObjFiles.getNames(jObjFiles)[0]);
				// //System.out.println(jObjFiles.getNames(jObjFiles)[1]);
				// //System.out.println(jObjFiles.get("signed"));

				doc_assinado = jObjFiles.get("signed").toString();

				// Object jo8 = jo5.get("signatures");
				// JSONObject jo9 = new JSONObject(jo8.toString());

				JSONArray jsonArraySignatures = jObjDocument.getJSONArray("signatures");

				//System.out.println(jsonArraySignatures.get(0).toString());
				//System.out.println("tamanho array assinaturas: " + jsonArraySignatures.length());

				//System.out.println(jsonArraySignatures.getJSONObject(0).get("signed").toString());
				assinado = jsonArraySignatures.getJSONObject(0).get("signed").toString();
				
				if(jsonArraySignatures.length() > 1) {
					
					rejeitado = jsonArraySignatures.getJSONObject(1).get("rejected").toString();
					//System.out.println("rejeitado: " + rejeitado);	
					
					//System.out.println(" emails: " + jsonArraySignatures.getJSONObject(1).get("email_events").toString());
					
					Object ObjFilesEmail_events = jsonArraySignatures.getJSONObject(1).get("email_events");
					JSONObject jObjFilesEmail_events = new JSONObject(ObjFilesEmail_events.toString());
					
					//System.out.println("email recusado: " + jObjFilesEmail_events.get("refused").toString());
					email_invalido = jObjFilesEmail_events.get("refused").toString();
				}
				
				//System.out.println("rejeitado 2: " + rejeitado);	
				// se o contrato for rejeitado
				if (!rejeitado.equals("null") && !rejeitado.equals(null) && !rejeitado.equals("")) {
					//System.out.println("entrou aqui no contrato rejeitado");
					
					Object ObjFilesRejected = jsonArraySignatures.getJSONObject(1).get("rejected");
					JSONObject jObjFilesRejected = new JSONObject(ObjFilesRejected.toString());
					obs_rejeitado = jObjFilesRejected.get("reason").toString();
					
					responseAutentique = new ResponseAutentique();
					responseAutentique.setCod_status("200");
					responseAutentique.setStatus("CONTRATO REJEITADO");
					responseAutentique.setId_contrato(id_documento);
					responseAutentique.setNr_solicitacao(nr_solicitacao);
					responseAutentique.setLink_contrato_assinado(doc_assinado);
					responseAutentique.setAtividade(atividade);			
					responseAutentique.setObs_rejeitado(obs_rejeitado);
				
				}else if(!email_invalido.equals("null") && !email_invalido.equals(null) && !email_invalido.equals("")){
					responseAutentique = new ResponseAutentique();
					responseAutentique.setCod_status("200");
					responseAutentique.setStatus("CONTRATO REJEITADO");
					responseAutentique.setId_contrato(id_documento);
					responseAutentique.setNr_solicitacao(nr_solicitacao);
					responseAutentique.setLink_contrato_assinado(doc_assinado);
					responseAutentique.setAtividade(atividade);			
					responseAutentique.setObs_rejeitado("email inválido, contrato não enviado ao cliente");
					
				}else {
					
					while (s < jsonArraySignatures.length()) {
						
						assinado = jsonArraySignatures.getJSONObject(s).get("signed").toString();
						
						if (!assinado.equals("null")) { // se tiver alguma assinatura assinada incrementa contador
							cont_ass_null++;
						}
						
						s++;
					}

					if (cont_ass_null <= 0) { // se todas as assinaturas nao estiverem assinadas
						
						//System.out.println("documento nao foi assinado");
						
						responseAutentique = new ResponseAutentique();
						responseAutentique.setCod_status("404");
						responseAutentique.setStatus("CONTRATO NAO ASSINADO");
						responseAutentique.setId_contrato(id_documento);
						responseAutentique.setNr_solicitacao(nr_solicitacao);	
						responseAutentique.setAtividade(atividade);		
						responseAutentique.setObs_rejeitado("Contrato não foi assinado ainda");
						
					} else { // se o contrato foi assinado
						//System.out.println("documento foi assinado, link: " + doc_assinado);
						responseAutentique = new ResponseAutentique();
						responseAutentique.setCod_status("200");
						responseAutentique.setStatus("OK");
						responseAutentique.setId_contrato(id_documento);
						responseAutentique.setNr_solicitacao(nr_solicitacao);
						responseAutentique.setLink_contrato_assinado(doc_assinado);
						responseAutentique.setAtividade(atividade);	
						responseAutentique.setObs_rejeitado("OK");
						
						// CHAMAR AQUI API QUE MOVIMENTA O PROCESSO
					}
					
				}
			}else {
				
				responseAutentique = new ResponseAutentique();
				responseAutentique.setCod_status("500");
				responseAutentique.setStatus("ERROR: não foi retornado os dados do contrato");
				responseAutentique.setId_contrato(id_documento);
				responseAutentique.setNr_solicitacao(nr_solicitacao);
				responseAutentique.setAtividade(atividade);
				
				//System.out.println("===================================================================================");
				//System.out.println("deu erro na busca dos dados do contrato " + nr_solicitacao);
				return null;
			}			
			
			//System.out.println(obs_rejeitado);						
			

		} catch (IOException e) {
			 			
			responseAutentique = new ResponseAutentique();
			responseAutentique.setCod_status("500");
			responseAutentique.setStatus("ERROR: " + e);
			responseAutentique.setId_contrato(id_documento);
			responseAutentique.setNr_solicitacao(nr_solicitacao);
			responseAutentique.setAtividade(atividade);
			
			e.printStackTrace();
		}
		return responseAutentique;
	}
	
	// funcao q busca as solicitacoes q ainda nao foram movimentadas e verifica se todos os contratos foram assinados caso tenha algum sem assinatura nao movimenta o processo
	public List<ResponseAutentique> buscaSolicitacoesASeremMovimentadas() {
		
		List<String> list = new ArrayList<>();
		list = daoLigueApi.listaSolicitacoesASeremMovimentadas();
		List<IntegracaoAutentiqueDto> listSolicitacao = new ArrayList<>();
		List<ResponseAutentique> listResponse = new ArrayList<>();
		
		int x = 0;
		while(x < list.size()) {
			
			listSolicitacao = daoLigueApi.buscaContratosPorSolicitacao(list.get(x).toString());
			listResponse = daoFluig.movimentaSolicitacaoFluigAutentique(listSolicitacao);
			
			// se deu certo a movimentacao entao marca solicitacao para q nao seja buscada novamente
			if(listResponse.size() <= 0) {
				daoLigueApi.marcaSolicitacao(list.get(x).toString());
			}
			
			x++;
		}		
		
		return listResponse;
	}
	
	// deleta o contrato ja enviado ao autentique, recebe o id do contrato por parametro
	public void deletaContratoAutentique(String id_contrato) {
	
		//System.out.println("entrou funcao deletaContratoAutentique: " + id_contrato);
		
		try {

			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, "{\"query\":\"mutation {deleteDocument(id: \\\"" + id_contrato + "\\\")}\"}");
			Request request = new Request.Builder().url("https://api.autentique.com.br/v2/graphql").post(body).addHeader("authorization", "Bearer 5d0fd10abd8f33f2a0d3590dfbbc081c39084171811f722ada6347f26f4ff2dc")
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache").addHeader("postman-token", "ff5a9e8e-2ba3-429b-0faa-c7df632b06a4").build();

			Response response = client.newCall(request).execute();
			
			//System.out.println("retorno: " + response);
			//System.out.println("retorno 2: " + response.body());
			//System.out.println("retorno: " + response.body().toString());
			
			JSONObject jObjPrincipal = new JSONObject(response.body().string());
			Object ObjData = jObjPrincipal.get("data");
			JSONObject jObjData = new JSONObject(ObjData.toString());
			
			Object ObjDocument = jObjData.get("deleteDocument");
			
			//System.out.println("dados: " + jObjData.toString());
			//System.out.println("dados: " + ObjDocument.toString());

		} catch (Exception erro) {
			//System.out.println("erro deletar autentique: " + erro);
		}

	}
	
	public String deletaContratoAutentiqueSolicitacao(String nr_solicitacao) {
		
		String retorno = "OK";
		
		try {
			
			List<IntegracaoAutentiqueDto> list = new ArrayList<>();
			
			DaoLigueApi daoLigueApi = new DaoLigueApi();
			
			list = daoLigueApi.buscaTodosContratosPorSolicitacao(nr_solicitacao);
			
			if(list.size() > 0) {
				
				daoLigueApi.deletaRegistroContratoPorSolicitacao(nr_solicitacao);
				
				int i = 0;
				while(i < list.size()) {
					
					//daoLigueApi.deletaRegistroContratoPorSolicitacao(list.get(i).getNr_solicitacao().trim());
					deletaContratoAutentique(list.get(i).getId_contrato().trim());				
					i++;
				}
				
			}
			
			retorno = "OK";
			
		}catch(Exception e) {
			//System.out.println("deu erro na funcao deletaContratoAutentiqueSolicitacao: " + e);
			retorno = "ERRO";
		}
		
		return retorno;		
		
	}
	
	// funcao q reenvia o contrato do autentique
	public ResponseReenvioEmail reenviarContrato(String email, String id_contrato) {
		
		//System.out.println("entrou na funcao ResponseReenvioEmail");
		
		ResponseReenvioEmail responseReenvioEmail;
		
		try {
			
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
			RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"partes[0]\"\r\n\r\n" + email + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
			Request request = new Request.Builder()
			  .url("https://api.autentique.com.br/documentos/" + id_contrato + "/reenviar.json")
			  .post(body)
			  .addHeader("authorization", "Bearer 5d0fd10abd8f33f2a0d3590dfbbc081c39084171811f722ada6347f26f4ff2dc")
			  .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
			  .addHeader("cache-control", "no-cache")
			  .addHeader("postman-token", "d4c4ecda-15f2-4d4a-0a8e-f5a9f3ccc08a")
			  .build();

			Response response = client.newCall(request).execute();			
			
			//System.out.println("mensagem reenvio email: " + response.message());
			//System.out.println("codigo reenvio email: " + response.code());
			
			responseReenvioEmail = new ResponseReenvioEmail();
			responseReenvioEmail.setCodigo(Integer.toString(response.code()));
			responseReenvioEmail.setMensagem(response.message());
					
		}catch(Exception e) {
			//System.out.println("deu problema no reenvio do email: " + e.getMessage());
			
			responseReenvioEmail = new ResponseReenvioEmail();
			responseReenvioEmail.setCodigo("500");
			responseReenvioEmail.setMensagem(e.getMessage());
			
			e.printStackTrace();
		}
		
		return responseReenvioEmail;		
		
	}

	// essa funcao recebe o arquivo por parametro e envia ao autentique
	public ResponseAutentiqueEnvioContrato envioContratoAutentique(MultipartFile arquivo, String email, String nomeArquivo) {//MultipartFile
		
		ResponseAutentiqueEnvioContrato responseAutentiqueEnvioContrato = new ResponseAutentiqueEnvioContrato();
		String idContrato = "";
		
		try {	
		
			//System.out.println("ENTROU FUNCAO envioContratoAutentique : TRATANTO ARQUIVO : " + nomeArquivo);
			
			Unirest.setTimeouts(0, 0);
			HttpResponse<String> response = Unirest.post("https://api.autentique.com.br/v2/graphql?=")
			  .header("Authorization", "Bearer 5d0fd10abd8f33f2a0d3590dfbbc081c39084171811f722ada6347f26f4ff2dc")
			  .header("Cookie", "__cfduid=da71c4375fcd44ff897c44fcd83eb4c721604598779")
			  .field("operations", "{\"query\":\"mutation CreateDocumentMutation($document: DocumentInput!, $signers: [SignerInput!]!, $file: Upload!) {createDocument(document: $document, signers: $signers, file: $file) {id name refusable sortable created_at signatures { public_id name email created_at action { name } link { short_link } user { id name email }}}}\", \"variables\":{\"document\": {\"name\": \"" + nomeArquivo + "\", \"refusable\":true},\"signers\": [{\"email\": \"" + email + "\",\"action\": \"SIGN\"}],\"file\":null}}")
			  .field("map", "{ \"file\": [\"variables.file\"] }")
			   //.field("file", new File("http://177.125.208.9:8580/webdeskreport/frameset?__id=idownloadFrame&__report=file:///C%3A%5Cfluig%5Crepository%5Cwcmdir%5Cwcm%5Ctenants%5Cwcm%5Ctmp%5Cecm%5Creports%5C502045%5C1000%5CContrato+Combo+Processos+V2+Autentique6100846065310243282.rptdesign&__masterpage=true&__format=pdf&__cID=1&Numero%20Solicitacao=230916"))
			   .field("file", convert(arquivo)) //convert(arquivo)
			  .asString();	
			
			JSONObject jObjPrincipal = new JSONObject(response.getBody().toString());
			
			System.out.println("Response autentique envioContratoAutentique" + response.getBody().toString());
						
			//se deu tudo certo e gerou o contrato entao pega o id
			if(jObjPrincipal.toString().indexOf("data") != -1 && jObjPrincipal.toString().indexOf("must_be_a_valid_email_address") == -1) {
				
				//System.out.println("FUNCAO envioContratoAutentique DEU CERTO ARQUIVO : " + nomeArquivo);
				
				Object ObjData = jObjPrincipal.get("data");
				JSONObject jObjData = new JSONObject(ObjData.toString());
				
				Object ObjDocument = jObjData.get("createDocument");
				JSONObject jObjDocument = new JSONObject(ObjDocument.toString());
				idContrato = jObjDocument.getString("id");
				
				responseAutentiqueEnvioContrato.setCodigo("200");
				responseAutentiqueEnvioContrato.setMensagem("OK");
				responseAutentiqueEnvioContrato.setIdContrato(idContrato);
								
			}else { // se deu problema na geracao do contrato
				
				if(response.getBody().toString().indexOf("errors") != -1) {
					
					if(response.getBody().toString().indexOf("Expected type Email at value") != -1 || response.getBody().toString().indexOf("must_be_a_valid_email_address") != -1) {
						responseAutentiqueEnvioContrato.setCodigo("01");
						responseAutentiqueEnvioContrato.setMensagem("Falha ao Enviar Contrato ao Autentique. Email inválido favor verificar !");
						
						//System.out.println("FUNCAO envioContratoAutentique DEU ERRO EMAIL INVALIDO ARQUIVO : " + nomeArquivo);
						
					}else {
						responseAutentiqueEnvioContrato.setCodigo("02");
						responseAutentiqueEnvioContrato.setMensagem("Falha ao Enviar Contrato ao Autentique. Tente Novamente Mais Tarde");
						
						//System.out.println("FUNCAO envioContratoAutentique DEU ERRO FALHA AO ENVIAR ARQUIVO : " + nomeArquivo);
					}
					
				}else {
					responseAutentiqueEnvioContrato.setCodigo("02");
					responseAutentiqueEnvioContrato.setMensagem("Falha ao Enviar Contrato ao Autentique. Tente Novamente Mais Tarde");
					
					//System.out.println("FUNCAO envioContratoAutentique DEU ERRO FALHA AO ENVIAR ELSE ARQUIVO : " + nomeArquivo);
				}
				
			}
			
			deletaArquivoEnviado(nomeArquivo);
			
		}catch(Exception erro) {
			//System.out.println("deu erro envioContratoAutentique: " + erro);
			//System.out.println("FUNCAO envioContratoAutentique CATCH : " + nomeArquivo);
			erro.printStackTrace();
		}
		
		return responseAutentiqueEnvioContrato;
		
	}
	
	//converte o arquivo multi part recebido por parametro para o tipo file
	public static File convert(MultipartFile file) throws IOException {
		
		//File convFile = new File("/root/api-spring/IclassApi/temp", file.getOriginalFilename());
		
		File convFile = new File(file.getOriginalFilename());
		
		//System.out.println("CAMINHO DO ARQUIVO: " + convFile.getAbsolutePath());
		
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile); 
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}	
	
	// funcao que recebe o nome do arquivo que deseja deletar, o arquivo deve estar dentro da pasta temp
	public void deletaArquivoEnviado(String nomeArquivo) {
		
		//File pasta = new File("/root/api-spring/IclassApi/temp");
		File pasta = new File("/");
		File[] arquivos = pasta.listFiles();
		
		//System.out.println("FUNCAO deletaArquivoEnviado DELETANDO ARQUIVO : " + nomeArquivo);

		for (int i = 0; i < arquivos.length; i++) {
			
			////System.out.println("nomes " + arquivos[i].getName());
			
			if (arquivos[i].getName().matches(nomeArquivo)) {
				arquivos[i].delete();
				//System.out.println("FUNCAO deletaArquivoEnviado ENCONTROU E DELETOU ARQUIVO : " + nomeArquivo);
			}
		}
	}
	
	//-------------------------------------------METODOS TITULARIDADE----------------------------------------------//
	
	// funcao q busca as solicitacoes q ainda nao foram movimentadas e verifica se todos os contratos foram assinados caso tenha algum sem assinatura nao movimenta o processo
		public List<ResponseAutentique> buscaSolicitacoesASeremMovimentadasTitularidade() {
			
			List<String> list = new ArrayList<>();
			list = daoLigueApi.listaSolicitacoesASeremMovimentadasTitularidade();
			List<IntegracaoAutentiqueDto> listSolicitacao = new ArrayList<>();
			List<ResponseAutentique> listResponse = new ArrayList<>();
			
			//System.out.println("Entrou no movimenta solicitações com a lista em :");
			//System.out.println(list);
			
			int x = 0;
			while(x < list.size()) {
				
				listSolicitacao = daoLigueApi.buscaContratosPorIdContratoTitularidade(list.get(x).toString());
				
				//System.out.println("List Solicitacao");
				//System.out.println(listSolicitacao.size());
				
				listResponse = daoFluig.movimentaSolicitacaoFluigAutentiqueTitularidade(listSolicitacao);
				
				//System.out.println("List Response");
				//System.out.println(listResponse.size());
				
				// se deu certo a movimentacao entao marca solicitacao para q nao seja buscada novamente
				if(listResponse.size() <= 0) {
					daoLigueApi.marcaSolicitacaoTitularidade(list.get(x).toString());
				}
				
				x++;
			}		
			
			return listResponse;
		}
	
	//------------------------------------------------------------------------------------------------------------//
		
	// marca contrato q deu certo para que saia da lista de contratos a serem buscados
//	public List<ResponseAutentique> marca_contrato_ja_buscado(List<ResponseAutentique> listRetorno) {
//		
//		ResponseAutentique responseAutentique;	
//		List<ResponseAutentique> listErros = new ArrayList<>();
//		ResponseEntity<String> retornoChamadaApi;
//		try {
//			
//			int i = 0;
//			while (i < listRetorno.size()) {
//				
//				retornoChamadaApi = daoLigueApi.marcacaoAutentique(listRetorno.get(i).getId_contrato().trim(), listRetorno.get(i).getLink_contrato_assinado().trim());
//				
//				if(!retornoChamadaApi.toString().equals("OK")) {
//					responseAutentique = new ResponseAutentique();
//					responseAutentique.setCod_status("500");
//					responseAutentique.setStatus(retornoChamadaApi.toString());
//					responseAutentique.setId_contrato(listRetorno.get(i).getId_contrato().trim());
//					responseAutentique.setNr_solicitacao(listRetorno.get(i).getNr_solicitacao().trim());
//					
//					listErros.add(responseAutentique);
//				
//				}
//				
//				i++;
//			}
//			
//		}catch(Exception e) {
//			//System.out.println("deu erro marcacao contrato assinado: " + e);
//			responseAutentique = new ResponseAutentique();
//			responseAutentique.setCod_status("500");
//			responseAutentique.setStatus("ERROR: " + e);
//			responseAutentique.setId_contrato("");
//			responseAutentique.setNr_solicitacao("");
//			
//			listErros.add(responseAutentique);
//		}
//		
//		return listErros;
//		
//	}	

}
