package api.rest.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import api.rest.classes.auxiliar.PlanosVendaDto;

public class Utils {

	public static String replaceAll(String campo, String caracterEncontrar, String caracterSubstituir) {

		return campo.replaceAll(caracterEncontrar, caracterSubstituir);
	}

	public int dataAtual() {

		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("yyyyMMdd");

		int dt_formatada = Integer.parseInt(formatador.format(data));

		//System.out.println("data formatada: " + dt_formatada);

		return dt_formatada;
	}
	
	// busca o tipo do plano na api de produtos
	public String consultaDadosPlanoApi(String cod_prod) {
		
		String retorno = "";
		
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<List<PlanosVendaDto>> response = restTemplate.exchange("http://172.18.0.2:8080/planos/plano?cidade=CAMPO_MOURAO&uf=PR&bairro=CENTRO&produto=" + cod_prod.trim(), HttpMethod.GET,  null, new ParameterizedTypeReference<List<PlanosVendaDto>>() { 
		});
		
        List<PlanosVendaDto> lista = response.getBody();
        retorno = lista.get(0).getTp_produto();

		//System.out.println("resposta teste: " + lista.get(0).getTp_produto());
		
		return retorno;
		
	}
	
	public String dataToString(Date data, String formato) {
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        return sdf.format(data);
    }

    public Date StringToDate(String data, String formato) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(formato);
        return (Date) formatter.parse(data);
    }
    
    public String primeiraPalavraUpCase(String teste) {
        String fMaiuscula = "";
        String[] split = teste.split(" ");
        for (String string : split) {
            fMaiuscula += StringUtils.capitalize(string);
        }

        return fMaiuscula;
    }
    
    public String removeEspacosCaracteres(String str1) {
        str1 = str1.replaceAll("[|\n]", "");
        str1 = str1.replaceAll("\t", "");
        str1 = str1.replaceAll("-", "");
        str1 = str1.replaceAll(";", "");
        str1 = str1.replaceAll(" ", "");
        return str1;
    }
    
    public String gerar(int nr_digitos) {
        String[] carct = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        String senha = "";
        for (int x = 0; x < nr_digitos; x++) {
            int j = (int) (Math.random() * carct.length);
            senha += carct[j];

        }
        return senha;
    }
    
	public XMLGregorianCalendar converteDataEmXMLGregorianCalendar(String data) { // yyyy-MM-dd hh:mm:ss		
		
		XMLGregorianCalendar xmlGregCalender = null;
		
		try {
//			
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
//			Date date = format.parse(data); // 2014-04-24 11:15:00
//
//			GregorianCalendar cal = new GregorianCalendar();			
//			cal.setTime(date);
//
//			xmlGregCalender =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
//
//			//System.out.println(xmlGregCalender);
			
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
			Date date = format.parse(data); // 2014-04-24 11:15:00

			GregorianCalendar cal = new GregorianCalendar();			
			cal.setTime(date);			
			
			xmlGregCalender = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			//System.out.println("teste data: " + xmlGregCalender);
			
		} catch(Exception e){
		    //System.out.println("deu erro conversao de data: " + e);
		}
		
		return xmlGregCalender;	
		
	}
	
	//RETORNA DATA E HORA ATUAL PARA USO NOS SYSTEM OUT - MODELO DE RETORNO = 06/04/2021 - 17:13:55
	public String buscaDataHora() {
		return new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " - " + new SimpleDateFormat("HH:mm:ss").format(new Date());
	}
	
	/*
	 * METODO DE ENCERRAMENTO DE O.S  - SE PRECISAR
	 * 
	 * 	public void encerrarOsNoIclass() {
		
		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
		
		OrdemServicoWSImplService serviceImpl = new OrdemServicoWSImplService();
		OrdemServicoWS connector = serviceImpl.getOrdemServicoWSImplPort();

		EncerrarOSIn encerrarOSIn = new EncerrarOSIn();
		ComentarioIn comentarioIn = new ComentarioIn();
		Utils utils = new Utils();		

		try {

			GregorianCalendar gregory = new GregorianCalendar();
			gregory.setTime(new Date());
			XMLGregorianCalendar xmlGregorianCalendar;
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);

			//System.out.println(xmlGregorianCalendar.toString());

			encerrarOSIn.setDataEncerramento(xmlGregorianCalendar);

		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			//System.out.println("erro na data: " + e);
		}

		comentarioIn.setComentario("O.S ENCERRADA DE FORMA AUTOMATICA VIA INTEGRACAO");
		comentarioIn.setResponsavel("robson.stirle");
		comentarioIn.setTipo("INFORMACAO");
		comentarioIn.setVisivelEquipe(true);
		comentarioIn.setVisivelCliente(true);

		encerrarOSIn.setCodigoOS("17495302");
		encerrarOSIn.setComentario(comentarioIn);
		encerrarOSIn.setMotivo("070002");		
		
		RespostaOut out = connector.encerrarOS(encerrarOSIn, utils.credenciais_v1(),"pt_br");
		
		//System.out.println("WARNINGS");
		for (WarningOut warning : out.getWarnings()) {			
			
			//System.out.println(warning.getCode());
			//System.out.println(warning.getDescription());			
		}

		//System.out.println("ERRORS");
		for (ErrorOut error : out.getErros()) {
			//System.out.println(error.getCode());
			//System.out.println(error.getDescription());	
		}		

	}
	 */

}
