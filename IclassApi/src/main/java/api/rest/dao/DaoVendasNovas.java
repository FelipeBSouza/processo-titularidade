package api.rest.dao;

import _103._15._3._10._8086.ARRAYOFLISTA6501;
import _103._15._3._10._8086.ARRAYOFLISTA6502;
import _103._15._3._10._8086.ITENSLIST6501;
import _103._15._3._10._8086.ITENSLIST6502;
import _103._15._3._10._8086.LISTA6501;
import _103._15._3._10._8086.LISTA6502;
import api.rest.venda.nova.Dados;
import api.rest.venda.nova.Fone;
import api.rest.venda.nova.Plano;
import api.rest.venda.nova.RespostaIntegracao;

public class DaoVendasNovas {
	
	// recebe um json com os dados da venda e faz o envio desses dados para o totvs
	public RespostaIntegracao gravaDadosTotvs(Dados dados) {
		
		String retorno = "";
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
		
		//PLANOS
		ITENSLIST6502 planos = new ITENSLIST6502();	
		ARRAYOFLISTA6502 arrayPlanos = new ARRAYOFLISTA6502(); // contem todos os planos que serao enviados ao totvs	
				
		// FONES
		ITENSLIST6501 fones = new ITENSLIST6501();		
		ARRAYOFLISTA6501 arrayFones = new ARRAYOFLISTA6501(); // contem todos os fones que serao enviados ao totvs	
				
		// item fone
		LISTA6501 fone;
		
		// se a lista de fones vim vazia entao deve ser tratado para nao dar problema, ja que nao pode ser passada vazia
		if(dados.getDADOS().getFONES().size() <= 0) {
			
			fone = new LISTA6501();
			fone.setDADOS6501("");
			arrayFones.getLISTA6501().add(fone);
			
		}else {
			
			for(Fone linhaFone : dados.getDADOS().getFONES()) {
				
				fone = new LISTA6501();
				fone.setDADOS6501(linhaFone.getVERIFICATIPO() + " ;" + 
						linhaFone.getDDI_FONE() + " ;" + 
						linhaFone.getDDD_FONE() + " ;" + 
						linhaFone.getFONE() + " ;" + 
						linhaFone.getTIPO_TELEFONE() + " ;" + 
						linhaFone.getCOMPLEMENTO_FONE() + " ;" + 
						linhaFone.getTITULAR_FONE() + " ;" + 					
						linhaFone.getCPF_CNPJ() + " ;" + 
						linhaFone.getNOVO_ALTERACAO_FONE() + " ;" + 
						linhaFone.getCOD_AGB() + " ;" + 
						linhaFone.getCOD_RECNO_FONE_VENDA() + " ;"
						);
				
				
				arrayFones.getLISTA6501().add(fone);	
			}
		}
		
		
		
		fones.setREGISTROS6501(arrayFones);
		
		//item plano
		LISTA6502 plano;
		
		for(Plano linhaPlano : dados.getDADOS().getPLANOS()) {
			
			plano = new LISTA6502();
			
			String qtd_mes_cob = linhaPlano.getQTMESCOB();
			
			if(qtd_mes_cob.equals("")) {
				
				if(linhaPlano.getCODPRODUTOITEM().equals("020001") || linhaPlano.getCODPRODUTOITEM().equals("020025") || linhaPlano.getCODPRODUTOITEM().equals("120010")
						|| linhaPlano.getCODPRODUTOITEM().equals("120012") || linhaPlano.getCODPRODUTOITEM().equals("020035") || linhaPlano.getCODPRODUTOITEM().equals("020041")
						|| linhaPlano.getCODPRODUTOITEM().equals("020038")) {
					
					qtd_mes_cob = "1";
					
				}else {
					qtd_mes_cob = "0";
				}				
				
			}
			
			plano.setDADOS6502(linhaPlano.getCODPRODUTOITEM() + " ;" + 
					linhaPlano.getQUANTIDADEITEM() + " ;" + 
					linhaPlano.getVALORESTIMADO() + " ;" + 
					linhaPlano.getVALORCHEIO() + " ;" + 
					linhaPlano.getTOTALITEM() + " ;" + 
					linhaPlano.getVALORDESCONTO() + " ;" + 
					linhaPlano.getQTMESINI() + " ;" + 
					//linhaPlano.getQTMESCOB() + " ;" + 
					qtd_mes_cob + " ;" + 
					linhaPlano.getOBSITEM() + " ;" + 
					linhaPlano.getCOD_TARIFA() + " ;"
					);
			
			arrayPlanos.getLISTA6502().add(plano);
			
		}
		
		planos.setREGISTROS6502(arrayPlanos);
				
		// envia das informacoes para o ws soap 65
		retorno = ligws65GR(dados.getDADOS().getTPPESSOA(),
				dados.getDADOS().getCDCLI(),
				dados.getDADOS().getLOJA(),
				dados.getDADOS().getVEND1(),
				dados.getDADOS().getOBS_OS1(),
				dados.getDADOS().getOBS_OS2(),
				dados.getDADOS().getOPERADOR(),
				dados.getDADOS().getCDPGTO(),
				dados.getDADOS().getOPERACAO(),
				dados.getDADOS().getMIDIA(),
				dados.getDADOS().getTMK(),
				dados.getDADOS().getTPCLI(),
				dados.getDADOS().getTPCTR(),
				dados.getDADOS().getPRAZO(),
				dados.getDADOS().getVALCTR(),
				dados.getDADOS().getUDIAFE(),
				dados.getDADOS().getASSCLI(),
				dados.getDADOS().getOBSSUA(),
				dados.getDADOS().getCODENDC(),
				dados.getDADOS().getCODENDI(),
				dados.getDADOS().getCX(),
				dados.getDADOS().getPT(),
				dados.getDADOS().getCCGCLI(),
				dados.getDADOS().getNRSOLIC(),
				dados.getDADOS().getFDIG(),
				dados.getDADOS().getURLGRV(),			
				planos, fones);	
		
		if(retorno.equals("")) {
			respostaIntegracao.setMENSAGEM("OK");
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("201");
			
		}else {
			respostaIntegracao.setMENSAGEM(retorno.toString());
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("500");
		}
		
		return respostaIntegracao;
	
	}
	
	// aqui chama o SOAP 65 la do totvs, e o metodo de integracao
	private String ligws65GR(String tppessoa, String cdcli, String loja, String vend1, String obsOS1, String obsOS2, String operador, String cdpgto, String operacao, String midia, String tmk, String tpcli, String tpctr, String prazo, String valctr,
			String udiafe, String asscli, String obssua, String codendc, String codendi, String cx, String pt, String ccgcli, String nrsolic, String fdig, String urlgrv, _103._15._3._10._8086.ITENSLIST6502 dados6502,
			_103._15._3._10._8086.ITENSLIST6501 dados6501) {		
		
		_103._15._3._10._8086.LIGWS065 service = new _103._15._3._10._8086.LIGWS065();
		_103._15._3._10._8086.LIGWS065SOAP port = service.getLIGWS065SOAP();
		
		return port.ligws65GR(tppessoa, cdcli, loja, vend1, obsOS1, obsOS2, operador, cdpgto, operacao, midia, tmk, tpcli, tpctr, prazo, valctr, udiafe, asscli, obssua, codendc, codendi, cx, pt, ccgcli, nrsolic, fdig, urlgrv, dados6502, dados6501);

	}

}
