package api.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class DaoEmail {
	
	@Autowired
    private JavaMailSender mailSender;
	
	public DaoEmail(JavaMailSender mailSender) {
		this.mailSender = mailSender;		
	}
	
	public void enviaEmail(String email_destino, String titulo, String conteudo) {

        SimpleMailMessage email = new SimpleMailMessage();

        email.setTo(email_destino);
        email.setSubject(titulo);
        email.setText(conteudo);
       
        mailSender.send(email);
    }

}
