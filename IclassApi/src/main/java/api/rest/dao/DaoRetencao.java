package api.rest.dao;

import _103._15._3._10._8086.ARRAYOFLISTA0166;
import _103._15._3._10._8086.ARRAYOFLISTA0266;
import _103._15._3._10._8086.ARRAYOFLISTA0366;
import _103._15._3._10._8086.ITENSLIST0166;
import _103._15._3._10._8086.ITENSLIST0266;
import _103._15._3._10._8086.ITENSLIST0366;
import _103._15._3._10._8086.LISTA0166;
import _103._15._3._10._8086.LISTA0266;
import _103._15._3._10._8086.LISTA0366;
import api.rest.retencao.DadosRetencao;
import api.rest.retencao.FoneTb;
import api.rest.retencao.ItemTb1;
import api.rest.retencao.ItemTb2;
import api.rest.venda.nova.RespostaIntegracao;

public class DaoRetencao {	
	
	public RespostaIntegracao gravaDadosRtencaoTotvs(DadosRetencao dadosRetencao) {	
		
		String retorno = "";
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
		
		//PLANOS DA TABELA DOS ITENS JA EXISTENTES NO CONTRATO
		ITENSLIST0266 planosTb1 = new ITENSLIST0266();							
		ARRAYOFLISTA0266 arrayPlanosTb1 = new ARRAYOFLISTA0266(); // contem todos os planos que o cliente ja possui
		
		//PLANOS DA TABELA DE ITENS A SEREM RENOVADOS/CONTRATADOS
		ITENSLIST0366 planosTb2 = new ITENSLIST0366();							
		ARRAYOFLISTA0366 arrayPlanosTb2 = new ARRAYOFLISTA0366(); // contem todos os planos que o cliente vai contratar/renovar
		
		//TABELA DE FONES PORTABILIDADE E LINHA NOVA
		ITENSLIST0166 fones = new ITENSLIST0166();							
		ARRAYOFLISTA0166 arrayFones = new ARRAYOFLISTA0166(); // contem todos os fones de portabilidade e/ou linha nova
		
		// item plano tabela itens ja contratados
		LISTA0266 planoTb1;
		// item plano tabela itens a serem renovados/contratados
		LISTA0366 planoTb2;
		// item fone
		LISTA0166 fone;		
		
		//ARRAY DE TELEFONE
		if(dadosRetencao.getDADOS().get_DADOS0155().size() <= 0) {
			
			fone = new LISTA0166();
			fone.setDADOS0166("");
			arrayFones.getLISTA0166().add(fone);
			
		}else {
			
			//array de fones
			for(FoneTb linhaFone : dadosRetencao.getDADOS().get_DADOS0155()) {
				
				fone = new LISTA0166();
				fone.setDADOS0166(linhaFone.getPADDDI() + " ;" + 
						linhaFone.getPADDD()  + " ;" + 
						linhaFone.getPATEL()  + " ;" + 
						linhaFone.getPATIPO()  + " ;" + 
						linhaFone.getPAPADRAO()  + " ;" + 
						linhaFone.getPAOP()  + " ;" + 
						linhaFone.getCDTELEF()  + " ;" + 
						linhaFone.getPATIPO2()  + " ;" + 
						linhaFone.getPACOMP()  + " ;" + 
						linhaFone.getPAOWNER()  + " ;" + 
						linhaFone.getPACPFOW()  + " ;" 
						
						);
				
				arrayFones.getLISTA0166().add(fone);	
				
			}
			
		}	
		
		fones.setREGISTROS0166(arrayFones);	
		
		// ARRAY DE PLANOS TB1
		if(dadosRetencao.getDADOS().get_DADOS0255().size() <= 0) {
			
			planoTb1 = new LISTA0266();
			planoTb1.setDADOS0266("");
			arrayPlanosTb1.getLISTA0266().add(planoTb1);
			
		}else{
			
			for(ItemTb1 linhaItemTb1 : dadosRetencao.getDADOS().get_DADOS0255()) {
				
				planoTb1 = new LISTA0266();
				planoTb1.setDADOS0266(linhaItemTb1.getP1CODP() + " ;" + 
						linhaItemTb1.getP1ITEM() + " ;" + 
						linhaItemTb1.getP1CODEND() + " ;" + 
						linhaItemTb1.getP1IDENT() + " ;" + 
						linhaItemTb1.getP1CODPN() + " ;" + 
						linhaItemTb1.getP1CODPAA()  + " ;" 
						);
				
				arrayPlanosTb1.getLISTA0266().add(planoTb1);
				
			}			
			
		}
		
		planosTb1.setREGISTROS0266(arrayPlanosTb1);
		
		// ARRAY DE PLANOS TB2
		if(dadosRetencao.getDADOS().get_DADOS0355().size() <= 0) {
			
			planoTb2 = new LISTA0366();
			planoTb2.setDADOS0366("");
			arrayPlanosTb2.getLISTA0366().add(planoTb2);
			
		}else{
			
			for(ItemTb2 linhaItemTb2 : dadosRetencao.getDADOS().get_DADOS0355()) {
				
				planoTb2 = new LISTA0366();
				planoTb2.setDADOS0366(linhaItemTb2.getP2CODP() + " ;" + 
						linhaItemTb2.getP2ITEM() + " ;" + 
						linhaItemTb2.getP2CODANT() + " ;" + 
						linhaItemTb2.getP2QUANT() + " ;" + 
						linhaItemTb2.getP2VLMES() + " ;" + 
						linhaItemTb2.getP2VALDES() + " ;" + 
						linhaItemTb2.getP2MSG() + " ;" + 
						linhaItemTb2.getP2MESINI() + " ;" + 
						linhaItemTb2.getP2UMESCO() + " ;" + 
						linhaItemTb2.getP2VLUNIT() + " ;" + 
						linhaItemTb2.getP2CODPA() + " ;" + 
						linhaItemTb2.getP2CODTAR() + " ;" + 
						linhaItemTb2.getP2UVLCHE() + " ;" + 
						linhaItemTb2.getP2OSOBRG()  + " ;"					
						
						);
				
				arrayPlanosTb2.getLISTA0366().add(planoTb2);
				
			}
			
		}
		
		planosTb2.setREGISTROS0366(arrayPlanosTb2);
			
		retorno = ligws66GR(dadosRetencao.getDADOS().getTPPESSOA(),
				dadosRetencao.getDADOS().getNCTR(),
				dadosRetencao.getDADOS().getCDCLI(),
				dadosRetencao.getDADOS().getLOJA(),
				dadosRetencao.getDADOS().getVEND1(),
				dadosRetencao.getDADOS().getOBS_OS1(),
				dadosRetencao.getDADOS().getOBS_OS2(),
				dadosRetencao.getDADOS().getOPERADOR(),
				dadosRetencao.getDADOS().getCDPGTO(),
				dadosRetencao.getDADOS().getOPERACAO(),
				dadosRetencao.getDADOS().getMIDIA(),
				dadosRetencao.getDADOS().getTMK(),
				dadosRetencao.getDADOS().getTPCLI(),
				dadosRetencao.getDADOS().getTPCTR(),
				dadosRetencao.getDADOS().getPRAZO(),
				dadosRetencao.getDADOS().getVALCTR(),
				dadosRetencao.getDADOS().getUDIAFE(),
				dadosRetencao.getDADOS().getASSCLI(),
				dadosRetencao.getDADOS().getOBSSUA(),
				dadosRetencao.getDADOS().getCODENDC(),
				dadosRetencao.getDADOS().getCX(),
				dadosRetencao.getDADOS().getPT(),
				dadosRetencao.getDADOS().getCCGCLI(),
				dadosRetencao.getDADOS().getNRSOLIC(),
				dadosRetencao.getDADOS().getURLGRV(),
				dadosRetencao.getDADOS().getFTDIGTAL(),
				dadosRetencao.getDADOS().getCODINS(),
				dadosRetencao.getDADOS().getOBSLAUDO(),
				dadosRetencao.getDADOS().getMOVTO(),
				dadosRetencao.getDADOS().getDTSOLINF(),
				planosTb1, planosTb2,fones
				);
		
		if(retorno.equals("")) {
			respostaIntegracao.setMENSAGEM("OK");
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("201");
			
		}else {
			respostaIntegracao.setMENSAGEM(retorno.toString());
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("500");
		}
		
		return respostaIntegracao;
		
	}	
	
	private String ligws66GR(String tppessoa, String nctr, String cdcli, String loja, String vend1, String obsOS1, String obsOS2, String operador, String cdpgto, String operacao, String midia, String tmk, String tpcli, String tpctr, String prazo, String valctr, String udiafe, String asscli, String obssua, String codendc, String cx, String pt, String ccgcli, String nrsolic, String urlgrv, String ftdigtal, String codins, String obslaudo, String movto, String dtsolinf, _103._15._3._10._8086.ITENSLIST0266 dados0266, _103._15._3._10._8086.ITENSLIST0366 dados0366, _103._15._3._10._8086.ITENSLIST0166 dados0166) {
        _103._15._3._10._8086.LIGWS066 service = new _103._15._3._10._8086.LIGWS066();
        _103._15._3._10._8086.LIGWS066SOAP port = service.getLIGWS066SOAP();
        return port.ligws66GR(tppessoa, nctr, cdcli, loja, vend1, obsOS1, obsOS2, operador, cdpgto, operacao, midia, tmk, tpcli, tpctr, prazo, valctr, udiafe, asscli, obssua, codendc, cx, pt, ccgcli, nrsolic, urlgrv, ftdigtal, codins, obslaudo, movto, dtsolinf, dados0266, dados0366, dados0166);
    }

}
