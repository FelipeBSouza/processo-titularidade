package api.rest.dao;

import _103._15._3._10._8086.ARRAYOFLISTA7001;
import _103._15._3._10._8086.ARRAYOFLISTA7002;
import _103._15._3._10._8086.ITENSLIST7001;
import _103._15._3._10._8086.ITENSLIST7002;
import _103._15._3._10._8086.LISTA7001;
import _103._15._3._10._8086.LISTA7002;
import api.rest.venda.nova.DadosMovel;
import api.rest.venda.nova.FoneSVA;
import api.rest.venda.nova.PlanoMovel;
import api.rest.venda.nova.RespostaIntegracao;

public class DaoVendaNovaMovel {
	
	public RespostaIntegracao gravaDados(DadosMovel dados) {
		
		String retorno = "";
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
		
		//PLANOS
		ITENSLIST7002 planos = new ITENSLIST7002();	
		ARRAYOFLISTA7002 arrayPlanos = new ARRAYOFLISTA7002(); // contem todos os planos que serao enviados ao totvs	
				
		// FONES
		ITENSLIST7001 fones = new ITENSLIST7001();		
		ARRAYOFLISTA7001 arrayFones = new ARRAYOFLISTA7001(); // contem todos os fones que serao enviados ao totvs	
				
		// item fone
		LISTA7001 fone;
		
		// se a lista de fones vim vazia entao deve ser tratado para nao dar problema, ja que nao pode ser passada vazia
		if(dados.getDADOS().getFONES().size() <= 0) {
			
			fone = new LISTA7001();
			fone.setDADOS7001("");
			arrayFones.getLISTA7001().add(fone);
			
		}else {
			
			for(FoneSVA linhaFone : dados.getDADOS().getFONES()) {
				
				fone = new LISTA7001();
				fone.setDADOS7001(linhaFone.getVERIFICATIPO() + " ;" + 
						linhaFone.getDDI_FONE() + " ;" + 
						linhaFone.getDDD_FONE() + " ;" + 
						linhaFone.getFONE() + " ;" + 
						linhaFone.getTIPO_TELEFONE() + " ;" + 
						linhaFone.getCOMPLEMENTO_FONE() + " ;" + 
						linhaFone.getTITULAR_FONE() + " ;" + 					
						linhaFone.getCPF_CNPJ() + " ;" + 
						linhaFone.getNOVO_ALTERACAO_FONE() + " ;" + 
						linhaFone.getCOD_AGB() + " ;" + 
						linhaFone.getCOD_RECNO_FONE_VENDA() + " ;" +
						linhaFone.getCOD_ITEM() + " ;"
						);
				
				
				arrayFones.getLISTA7001().add(fone);	
			}
		}
		
		
		
		fones.setREGISTROS7001(arrayFones);
		
		//item plano
		LISTA7002 plano;
		
		for(PlanoMovel linhaPlano : dados.getDADOS().getPLANOS()) {
			
			plano = new LISTA7002();
			
			plano.setDADOS7002(linhaPlano.getCODPRODUTOITEM() + " ;" + 
					linhaPlano.getQUANTIDADEITEM() + " ;" + 
					linhaPlano.getVALORESTIMADO() + " ;" + 
					linhaPlano.getVALORCHEIO() + " ;" + 
					linhaPlano.getTOTALITEM() + " ;" + 
					linhaPlano.getVALORDESCONTO() + " ;" + 
					linhaPlano.getQTMESINI() + " ;" + 
					linhaPlano.getQTMESCOB() + " ;" + 
					linhaPlano.getOBSITEM() + " ;" + 
					linhaPlano.getCOD_TARIFA() + " ;" +
					linhaPlano.getCOD_ITEM() + " ;" +
					linhaPlano.getTPPLANO() + " ;" +
					linhaPlano.getFONEMOVEL()+ " ;" 
					);
			
			arrayPlanos.getLISTA7002().add(plano);
			
		}
		
		planos.setREGISTROS7002(arrayPlanos);
				
		// envia das informacoes para o ws soap 70
		retorno = ligws70GR(dados.getDADOS().getTPPESSOA(),
				dados.getDADOS().getCDCLI(),
				dados.getDADOS().getLOJA(),
				dados.getDADOS().getVEND1(),
				dados.getDADOS().getOBS_OS1(),
				dados.getDADOS().getOBS_OS2(),
				dados.getDADOS().getOPERADOR(),
				dados.getDADOS().getCDPGTO(),
				dados.getDADOS().getOPERACAO(),
				dados.getDADOS().getMIDIA(),
				dados.getDADOS().getTMK(),
				dados.getDADOS().getTPCLI(),
				dados.getDADOS().getTPCTR(),
				dados.getDADOS().getPRAZO(),
				dados.getDADOS().getVALCTR(),
				dados.getDADOS().getUDIAFE(),
				dados.getDADOS().getASSCLI(),
				dados.getDADOS().getOBSSUA(),
				dados.getDADOS().getCODENDC(),
				dados.getDADOS().getCODENDI(),
				dados.getDADOS().getCX(),
				dados.getDADOS().getPT(),
				dados.getDADOS().getCCGCLI(),
				dados.getDADOS().getNRSOLIC(),
				dados.getDADOS().getFDIG(),
				dados.getDADOS().getURLGRV(),		
				dados.getDADOS().getTEMINS(),
				planos, fones);	
		
		if(retorno.equals("")) {
			respostaIntegracao.setMENSAGEM("OK");
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("201");
			
		}else {
			respostaIntegracao.setMENSAGEM(retorno.toString());
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("500");
		}
		
		return respostaIntegracao;
		
	}
	

	public String ligws70GR(String tppessoa, String cdcli, String loja, String vend1, String obsOS1, String obsOS2, String operador, String cdpgto, String operacao, String midia, String tmk, String tpcli, String tpctr, String prazo, String valctr, String udiafe, String asscli, String obssua, String codendc, String codendi, String cx, String pt, String ccgcli, String nrsolic, String fdig, String urlgrv, String temINS, _103._15._3._10._8086.ITENSLIST7002 dados7002, _103._15._3._10._8086.ITENSLIST7001 dados7001) {
        _103._15._3._10._8086.LIGWS070 service = new _103._15._3._10._8086.LIGWS070();
        _103._15._3._10._8086.LIGWS070SOAP port = service.getLIGWS070SOAP();
        return port.ligws70GR(tppessoa, cdcli, loja, vend1, obsOS1, obsOS2, operador, cdpgto, operacao, midia, tmk, tpcli, tpctr, prazo, valctr, udiafe, asscli, obssua, codendc, codendi, cx, pt, ccgcli, nrsolic, fdig, urlgrv, temINS, dados7002, dados7001);
    }
}
