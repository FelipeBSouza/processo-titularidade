package api.rest.dao;

import java.util.ArrayList;
import java.util.List;

import com.totvs.technology.ecm.dm.ws.CardFieldDto;
import com.totvs.technology.ecm.dm.ws.CardFieldDtoArray;
import com.totvs.technology.ecm.dm.ws.CardService;
import com.totvs.technology.ecm.dm.ws.ECMCardServiceService;
import com.totvs.technology.ecm.workflow.ws.ECMWorkflowEngineServiceService;
import com.totvs.technology.ecm.workflow.ws.KeyValueDto;
import com.totvs.technology.ecm.workflow.ws.KeyValueDtoArray;
import com.totvs.technology.ecm.workflow.ws.ProcessAttachmentDtoArray;
import com.totvs.technology.ecm.workflow.ws.ProcessTaskAppointmentDtoArray;
import com.totvs.technology.ecm.workflow.ws.WorkflowEngineService;

import api.rest.autentique.ResponseAutentique;
import api.rest.classes.auxiliar.IntegracaoAutentiqueDto;
import api.rest.response.ResponseOS;
import net.java.dev.jaxb.array.StringArray;
import net.java.dev.jaxb.array.StringArrayArray;

public class DaoFluig {

	DaoLigueApi daoLigueApi = new DaoLigueApi();
	/*
	 * ENTRADA: recebe os dados e movimenta a solicitacao passada por parametro
	 * 
	 * idsFluig = usuarios q receberao a tarefa valorCampoForm = valor q definira a decisao automatica para qual atividade o processo deve ir nr_solicitacao = solicitacao do fluig q deseja movimentar nr_atividade = numero da atividade q deseja q seja
	 * enviada a solicitacao
	 * 
	 * RETORNO: retorna erros vindos do WS do Fluig
	 */
	
	public KeyValueDtoArray dadosFormulario(int nr_solicitacao) {
		
		KeyValueDtoArray cardData = new KeyValueDtoArray();
		
		try {
			
			String username = "usuario.teste";
			String password = "CB3oWiwG&u#x";
			int companyId = 1;
//			int nr_solicitacao = 47738;			
									
			ECMWorkflowEngineServiceService service = new ECMWorkflowEngineServiceService();
			WorkflowEngineService port = service.getWorkflowEngineServicePort();								
			
			StringArrayArray dados = port.getInstanceCardData(username, password, companyId, username, nr_solicitacao);
			////System.out.println(dados);
						
			KeyValueDto keyValueDto;
			int i = 0;
			////System.out.println("DADOS DO FORMULARIO");
			while(i < dados.getItem().size()) {				
				
				List<String> linhaDados = dados.getItem().get(i).getItem();
				
				////System.out.println("campo: " + linhaDados.get(0).toString() + " valor: " + linhaDados.get(1).toString());

				if(linhaDados.get(0).toString().equals("datanascimento")) {
					keyValueDto = new KeyValueDto();
					keyValueDto.setKey(linhaDados.get(0).toString()); // nome do campo
					keyValueDto.setValue(arrumaDataNascimento(linhaDados.get(1).toString())); // valor do campo do formulario
					cardData.getItem().add(keyValueDto);	
					
				}else {
					keyValueDto = new KeyValueDto();
					keyValueDto.setKey(linhaDados.get(0).toString()); // nome do campo
					keyValueDto.setValue(linhaDados.get(1).toString()); // valor do campo do formulario
					cardData.getItem().add(keyValueDto);	
				}
			
//				keyValueDto = new KeyValueDto();
//				keyValueDto.setKey(linhaDados.get(0).toString()); // nome do campo
//				keyValueDto.setValue(linhaDados.get(1).toString()); // valor do campo do formulario
//				cardData.getItem().add(keyValueDto);				
				
				i++;
			}
			
			
		}catch(Exception e) {
			//System.out.println("deu erro buscar dados: " + e);
			e.printStackTrace();
		}
		
		return cardData;
	}
	
	public String arrumaDataNascimento(String dataNascimento) {
		
		String dia = "";
		String mes = "";
		String ano = "";
		String dtFinal = "";
		
		////System.out.println("tamanho arrumaDataNascimento: " + dataNascimento.length());
		
		try {
			
			if(dataNascimento.length() == 10) {
				
				if(dataNascimento.indexOf("-") != -1) {
					
					dia = dataNascimento.substring(8,10);
					mes = dataNascimento.substring(5,7);
					ano = dataNascimento.substring(0,4);
					
					dtFinal = dia + "/" + mes + "/" + ano;
					
				}else if(dataNascimento.indexOf("/") != -1) {
					dia = dataNascimento.substring(0,2);
					mes = dataNascimento.substring(3,5);
					ano = dataNascimento.substring(6,10);
					
					dtFinal = dia + "/" + mes + "/" + ano;
					
				}else {
					dtFinal = dataNascimento;
				}				
				
			}else {
				dtFinal = dataNascimento;
			}
			
			
		}catch(Exception e) {
			//System.out.println("deu erro funcao arrumaDataNascimento: " + e);	
			dtFinal = dataNascimento;
		}		
		
		//System.out.println("arrumaDataNascimento: " + dtFinal);
		
		return dtFinal;
		
	}
	
	
	public List<ResponseAutentique> movimentaSolicitacaoFluig(int nr_solicitacao, int nr_atividade, String comments) {

		List<ResponseAutentique> listResponse = new ArrayList<>();

		//System.out.println("chamou funcao chamaIntegracao");

		//String comments = "Movimentado de forma automática pelo sistema";

		// dados de um usuario admin
		String usuario = "robson.stirle";
		String senha = "ePV%SP8x@lQr";

		// consultar ada para pegar codigo do vendedor
		// buscar vendedor pelo codigo de ada em sa3 para pegar o login do fluig

		String userId = "usuario.teste"; // usuario q ira executar a tarefa
		StringArray colleagueIds = new StringArray(); // usuarios q receberao a tarefa
		int company = 1;
		boolean completeTask = true; // indica se vai enviar a solicitacao ou somente salvar
		boolean managerMode = true; // indica se o usuario esta executando a tarefa como gestor
		int threadSequence = 0; // indica se ha atividade paralela no processo, se nao existir o valor é 0, caso exista, este valor pode ser de 1 a infinito dependendo da quantidade de atividade paralelas existentes no processo.
		ProcessAttachmentDtoArray attachments = new ProcessAttachmentDtoArray(); // anexos do processo
		ProcessTaskAppointmentDtoArray appointment = new ProcessTaskAppointmentDtoArray();

		// CAMPOS DO FORMULARIO
		KeyValueDto keyValueDto = new KeyValueDto();
		keyValueDto.setKey("laudo_os"); // laudo da OS
		keyValueDto.setValue(comments); // valor do campo do formulario

		KeyValueDtoArray cardData = new KeyValueDtoArray(); // passa campos dos formularios com seus valores
		
		cardData = dadosFormulario(nr_solicitacao);		
		cardData.getItem().add(keyValueDto);		

		listResponse = saveAndSendTaskClassic(usuario, senha, company, nr_solicitacao, nr_atividade, colleagueIds, comments, userId, completeTask, attachments, cardData, appointment, managerMode, threadSequence);

		return listResponse;

	}
	
	public List<ResponseAutentique> movimentaSolicitacaoFluigVendas(int nr_solicitacao, int nr_atividade, String comments, String opcao, String cod_baixa, String numero_os) {

		List<ResponseAutentique> listResponse = new ArrayList<>();

		//System.out.println("chamou funcao chamaIntegracao opcao " + opcao + " cod baixa: " + cod_baixa);

		//String comments = "Movimentado de forma automática pelo sistema";

		// dados de um usuario admin
		String usuario = "robson.stirle";
		String senha = "ePV%SP8x@lQr";

		// consultar ada para pegar codigo do vendedor
		// buscar vendedor pelo codigo de ada em sa3 para pegar o login do fluig

		String userId = "usuario.teste"; // usuario q ira executar a tarefa
		StringArray colleagueIds = new StringArray(); // usuarios q receberao a tarefa
		int company = 1;
		boolean completeTask = true; // indica se vai enviar a solicitacao ou somente salvar
		boolean managerMode = true; // indica se o usuario esta executando a tarefa como gestor
		int threadSequence = 0; // indica se ha atividade paralela no processo, se nao existir o valor é 0, caso exista, este valor pode ser de 1 a infinito dependendo da quantidade de atividade paralelas existentes no processo.
		ProcessAttachmentDtoArray attachments = new ProcessAttachmentDtoArray(); // anexos do processo
		ProcessTaskAppointmentDtoArray appointment = new ProcessTaskAppointmentDtoArray();

		// CAMPOS DO FORMULARIO
		KeyValueDto keyValueDto = new KeyValueDto();
		keyValueDto.setKey("obs_integracao_iclass"); // laudo da OS
		keyValueDto.setValue(comments); // valor do campo do formulario
		
		KeyValueDto keyValueDto2 = new KeyValueDto();
		keyValueDto2.setKey("integracao_iclass_autentique"); // define pra onde a solicitacao vai
		keyValueDto2.setValue(opcao); // valor do campo do formulario
		
		KeyValueDto keyValueDto3 = new KeyValueDto();
		keyValueDto3.setKey("cod_baixa_integracao"); // define pra onde a solicitacao vai
		keyValueDto3.setValue(cod_baixa); // valor do campo do formulario
		
		KeyValueDto keyValueDto4 = new KeyValueDto();
		keyValueDto4.setKey("os_iclass"); // informa o numero da OS
		keyValueDto4.setValue(numero_os); // valor do campo do formulario

		KeyValueDtoArray cardData = new KeyValueDtoArray(); // passa campos dos formularios com seus valores
		cardData = dadosFormulario(nr_solicitacao);		// busca dados ja existentes na solicitacao, caso nao fizer isso os campos ficam em branco
		cardData.getItem().add(keyValueDto);
		cardData.getItem().add(keyValueDto2);
		cardData.getItem().add(keyValueDto3);
		cardData.getItem().add(keyValueDto4);
		
		listResponse = saveAndSendTaskClassic(usuario, senha, company, nr_solicitacao, nr_atividade, colleagueIds, comments, userId, completeTask, attachments, cardData, appointment, managerMode, threadSequence);

		// atualiza status se deu certo a integracao
//		try {
//			// verifica se a movimentacao nao e da portabilidade
//			if(!cod_baixa.equals("000116") && !cod_baixa.equals("000117")) {
//				// se deu tudo certo na movimentacao da solicitacao
//				if(listResponse.size() <= 0) {
//					DaoLigueApi daoLigueApi = new DaoLigueApi();
//					daoLigueApi.atualizaStatusGetCloseOrdens(String.valueOf(nr_solicitacao), opcao, "FOK", numero_os);
//				}else {
//					DaoLigueApi daoLigueApi = new DaoLigueApi();
//					daoLigueApi.atualizaStatusGetCloseOrdens(String.valueOf(nr_solicitacao), opcao, "FERRO", numero_os);
//				}
//			}
//		}catch(Exception e) {
//			//System.out.println("deu erro na gravacao do status na api que movimenta solicitacao do fluig: " + e);
//		}
				
		return listResponse;

	}

	// faz efetivamente a integracao com fluig
	private List<ResponseAutentique> saveAndSendTaskClassic(String username, String password, int companyId, int processInstanceId, int choosedState, StringArray colleagueIds, String comments, String userId, boolean completeTask,
			ProcessAttachmentDtoArray attachments, KeyValueDtoArray cardData, ProcessTaskAppointmentDtoArray appointment, boolean managerMode, int threadSequence) {

		KeyValueDtoArray retorno = new KeyValueDtoArray();
		List<ResponseAutentique> listResponse = new ArrayList<>();
		ResponseAutentique responseAutentique;

		try {
			ECMWorkflowEngineServiceService service = new ECMWorkflowEngineServiceService();
			WorkflowEngineService port = service.getWorkflowEngineServicePort();
			retorno = port.saveAndSendTaskClassic(username, password, companyId, processInstanceId, choosedState, colleagueIds, comments, userId, completeTask, attachments, cardData, appointment, managerMode, threadSequence);
						
			// retorno do ws do fluig
			int i = 0;
			while (i < retorno.getItem().size()) {
				
				if(retorno.getItem().get(i).getKey().toString().equals("ERROR")) {
					responseAutentique = new ResponseAutentique();
					responseAutentique.setCod_status("500");
					responseAutentique.setStatus(retorno.getItem().get(i).getValue());
					responseAutentique.setId_contrato("");
					responseAutentique.setNr_solicitacao(String.valueOf(processInstanceId));
					responseAutentique.setAtividade(choosedState);
					listResponse.add(responseAutentique);
				}				

				//System.out.println("retorno:  " + retorno.getItem().get(i).getKey() + ":" + retorno.getItem().get(i).getValue());

				i++;
			}

		} catch (Exception e) {

			//System.out.println("deu erro na integracao com fluig: " + e);

			responseAutentique = new ResponseAutentique();
			responseAutentique.setCod_status("500");
			responseAutentique.setStatus("ERROR: " + e);
			responseAutentique.setId_contrato("");
			responseAutentique.setNr_solicitacao(String.valueOf(processInstanceId));
			responseAutentique.setAtividade(choosedState);
			listResponse.add(responseAutentique);

		}

		return listResponse;

	}
	
	public List<ResponseAutentique> movimentaSolicitacaoFluigAutentique(List<IntegracaoAutentiqueDto> listSolicitacao) {

		List<ResponseAutentique> listResponse = new ArrayList<>();
		ResponseAutentique responseAutentique;
		String valorCampoForm = "";
		String obs_rejeitado = "";
		int nr_solicitacao = 0;
		int nr_atividade = 0;
		String id_contrato = "";
		String fluxo_processo = "";
		
		try {
			
			int x = 0;
			while(x < listSolicitacao.size()) {

				valorCampoForm = listSolicitacao.get(x).getContrato_assinado() ;
				//valorCampoForm = "teste" ;
				nr_solicitacao = Integer.parseInt(listSolicitacao.get(x).getNr_solicitacao());
				nr_atividade = listSolicitacao.get(x).getAtividade();
				obs_rejeitado = listSolicitacao.get(x).getObs_rejeitado();
				id_contrato = listSolicitacao.get(x).getId_contrato();
						
				x++;
			}
			
			if(obs_rejeitado.equals("OK")) {
				fluxo_processo = "AOK";
			}else {
				fluxo_processo = "AERRO";
			}

			String comments = "Movimentado de forma automática pelo sistema";

			// dados de um usuario admin
			String usuario = "robson.stirle";
			String senha = "ePV%SP8x@lQr";

			// consultar ada para pegar codigo do vendedor
			// buscar vendedor pelo codigo de ada em sa3 para pegar o login do fluig

			String userId = "usuario.teste"; // usuario q ira executar a tarefa, nesse caso o gestor do processo
			StringArray colleagueIds = new StringArray(); // usuarios q receberao a tarefa
			int company = 1;
			boolean completeTask = true; // indica se vai enviar a solicitacao ou somente salvar
			boolean managerMode = true; // indica se o usuario esta executando a tarefa como gestor
			int threadSequence = 0; // indica se ha atividade paralela no processo, se nao existir o valor é 0, caso exista, este valor pode ser de 1 a infinito dependendo da quantidade de atividade paralelas existentes no processo.
			ProcessAttachmentDtoArray attachments = new ProcessAttachmentDtoArray(); // anexos do processo
			ProcessTaskAppointmentDtoArray appointment = new ProcessTaskAppointmentDtoArray();

			//colleagueIds.getItem().add("");
			
			// CAMPOS DO FORMULARIO
			KeyValueDto keyValueDto = new KeyValueDto();
			keyValueDto.setKey("url_contrato"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto.setValue(valorCampoForm); // valor do campo do formulario
			
			KeyValueDto keyValueDto1 = new KeyValueDto();
			keyValueDto1.setKey("obs_integracao_autentique"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto1.setValue(obs_rejeitado); // valor do campo do formulario
			
			KeyValueDto keyValueDto2 = new KeyValueDto();
			keyValueDto2.setKey("id_contrato"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto2.setValue(id_contrato); // valor do campo do formulario
			
			KeyValueDto keyValueDto3 = new KeyValueDto();
			keyValueDto3.setKey("integracao_iclass_autentique"); // nome do campo do formulario q armazenara fluxo do processo
			keyValueDto3.setValue(fluxo_processo); // valor do campo do formulario


			KeyValueDtoArray cardData = new KeyValueDtoArray(); // passa campos dos formularios com seus valores
			cardData = dadosFormulario(nr_solicitacao);		// busca dados ja existentes na solicitacao, caso nao fizer isso os campos ficam em branco
			cardData.getItem().add(keyValueDto);
			cardData.getItem().add(keyValueDto1);
			cardData.getItem().add(keyValueDto2);
			cardData.getItem().add(keyValueDto3);
			
			System.out.println("Processando solicitação numero : " + nr_solicitacao);
			
			listResponse = saveAndSendTaskClassic(usuario, senha, company, nr_solicitacao, nr_atividade, colleagueIds, comments, userId, completeTask, attachments, cardData, appointment, managerMode, threadSequence);
			
			System.out.println("listResponse solicitação numero : " + listResponse.get(0).getNr_solicitacao());
			System.out.println("listResponse status : " + listResponse.get(0).getStatus());
			
			System.out.println("Processada solicitação numero : " + nr_solicitacao);
			
		}catch(Exception e) {
			
			//System.out.println("deu erro na movimentacao da solicitacao: " + e);
			e.printStackTrace();
			
			responseAutentique = new ResponseAutentique();
			responseAutentique.setCod_status("500");
			responseAutentique.setStatus("ERROR: " + e);
			responseAutentique.setId_contrato("");
			responseAutentique.setNr_solicitacao(String.valueOf(nr_solicitacao));
			responseAutentique.setAtividade(nr_atividade);
			listResponse.add(responseAutentique);
		 
		}
		
		return listResponse;

	}
	
	public void atualizaDadosFormulario() {
		
		int id_documento = 454999;

		try {
			
			ECMCardServiceService service = new ECMCardServiceService();
			CardService port = service.getCardServicePort();
			
			CardFieldDtoArray array = new CardFieldDtoArray();
			CardFieldDto dados;
			
			dados = new CardFieldDto();
			dados.setField("nome_cliente");
			dados.setValue("TESTE FLUIG AUTENTIQUE INTEGRACAO");
			array.getItem().add(dados);
						
			port.updateCardData(1,"usuario.teste","CB3oWiwG&u#x",id_documento, array);
			
		}catch(Exception e) {
			//System.out.println("deu erro atualizaDadosFormulario: " + e);
		}
	}
	
//	// faz efetivamente a integracao com fluig
//	private List<ResponseOS> saveAndSendTaskClassic(String username, String password, int companyId, int processInstanceId, int choosedState, StringArray colleagueIds, String comments, String userId, boolean completeTask,
//			ProcessAttachmentDtoArray attachments, KeyValueDtoArray cardData, ProcessTaskAppointmentDtoArray appointment, boolean managerMode, int threadSequence) {
//
//		KeyValueDtoArray retorno = new KeyValueDtoArray();
//		List<ResponseOS> listResponse = new ArrayList<>();
//		ResponseOS responseOS;
//
//		try {
//			ECMWorkflowEngineServiceService service = new ECMWorkflowEngineServiceService();
//			WorkflowEngineService port = service.getWorkflowEngineServicePort();
//			retorno = port.saveAndSendTaskClassic(username, password, companyId, processInstanceId, choosedState, colleagueIds, comments, userId, completeTask, attachments, cardData, appointment, managerMode, threadSequence);
//
//			// retorno do ws do fluig
//			int i = 0;
//			while (i < retorno.getItem().size()) {
//
//				responseOS = new ResponseOS();
//				responseOS.setId(retorno.getItem().get(i).getKey());
//				responseOS.setResponse(retorno.getItem().get(i).getValue() + ", Solicitacao: " + processInstanceId);
//				listResponse.add(responseOS);
//
////				//System.out.println("retorno:  " + retorno.getItem().get(i).getKey() + ":" + retorno.getItem().get(i).getValue());
//
//				i++;
//			}
//
//		} catch (Exception e) {
//
//			//System.out.println("deu erro na integracao com fluig: " + e);
//
//			responseOS = new ResponseOS();
//			responseOS.setId(e.getCause().toString());
//			responseOS.setResponse(e.getMessage());
//			listResponse.add(responseOS);
//
//		}
//
//		return listResponse;
//
//	}

//	// chama api e verifica se algum contrato dos que estao na lista aguardando assinatura ja foram assinados
//	// caso tenha sido assinado chama api q adiciona a URL do arquivo assinado no processo e movimenta para a proxima atividade
//	public void buscaContratosAssinados() {
//		
//		String doc_assinado = "";
//		String assinado = ""; // se = null entao o contrato nao foi assinado
//		
//		// 0835309b4f6fb74f0c487ca1192a3d98dab91ddf0c2578960 assinado
//		// 2d6a411c54a316bd43ba653cc1d3b0f14d5f8538be0e651ff nao foi assinado
//
//		try {
//			OkHttpClient client = new OkHttpClient();
//
//			MediaType mediaType = MediaType.parse("application/json");
//			RequestBody body = RequestBody.create(mediaType, "{\"query\":\"{document(id: \\\"0835309b4f6fb74f0c487ca1192a3d98dab91ddf0c2578960\\\") {id name files { original signed } signatures {signed { ...event }}}} fragment event on Event {ipv4 ipv6 reason created_at}\"}");
//			Request request = new Request.Builder()
//			  .url("https://api.autentique.com.br/v2/graphql")
//			  .post(body)
//			  .addHeader("authorization", "Bearer 442f0a0ab7ff96b5121c38fbaaf1f398875ca317fe517860ce9208806ef6225e")
//			  .addHeader("content-type", "application/json")
//			  .addHeader("cache-control", "no-cache")
//			  .addHeader("postman-token", "8bbd2688-6a03-3218-d9ed-d3e7f5b50b37")
//			  .build();
//
//			Response response = client.newCall(request).execute();
//			
//			JSONObject jObjPrincipal = new JSONObject(response.body().string());
//			Object ObjData = jObjPrincipal.get("data");
//			JSONObject jObjData =  new JSONObject(ObjData.toString());
//			
//			Object ObjDocument = jObjData.get("document");
//			JSONObject jObjDocument =  new JSONObject(ObjDocument.toString());
//			
//			Object ObjFiles = jObjDocument.get("files");
//			JSONObject jObjFiles =  new JSONObject(ObjFiles.toString());
//			
//			////System.out.println(jObjFiles.getNames(jObjFiles)[0]);
//			////System.out.println(jObjFiles.getNames(jObjFiles)[1]);			
//			////System.out.println(jObjFiles.get("signed"));
//						
//			doc_assinado = jObjFiles.get("signed").toString();		
//			
//			//Object jo8 = jo5.get("signatures");
//			//JSONObject jo9 =  new JSONObject(jo8.toString());
//			
//			JSONArray jsonArraySignatures = jObjDocument.getJSONArray("signatures");
//			
//			//System.out.println(jsonArraySignatures.get(0).toString());
//			
//			//System.out.println(jsonArraySignatures.getJSONObject(0).get("signed").toString());
//			assinado = jsonArraySignatures.getJSONObject(0).get("signed").toString();
//			
//			if (assinado.equals("null")) {
//				//System.out.println("documento nao foi assinado");
//				// CHAMAR AQUI API QUE MOVIMENTA O PROCESSO
//			}else {
//				//System.out.println("documento foi assinado, link: " + doc_assinado);
//			}
//			
//		} catch (IOException e) {			
//			e.printStackTrace();
//		}
//	}
	
	// FUNCAO DE TESTE
	public void chamaIntegracao() {

		List<ResponseOS> listResponse = new ArrayList<>();

		//System.out.println("chamou funcao chamaIntegracao");
		// chamar esse ws e fazer consulta para pegar o userId
		// http://10.0.1.190:8580/api/public/2.0/workflows/findActiveTasks/50432

		String comments = "";
		String userId = "usuario.teste"; // usuario q ira executar a tarefa
		boolean completeTask = true; // indica se vai enviar a solicitacao ou somente salvar
		boolean managerMode = true; // indica se o usuario esta executando a tarefa como gestor
		int threadSequence = 0; // indica se ha atividade paralela no processo, se nao existir o valor é 0, caso exista, este valor pode ser de 1 a infinito dependendo da quantidade de atividade paralelas existentes no processo.
		ProcessAttachmentDtoArray attachments = new ProcessAttachmentDtoArray(); // anexos do processo
		ProcessTaskAppointmentDtoArray appointment = new ProcessTaskAppointmentDtoArray();

		StringArray colleagueIds = new StringArray(); // usuarios q receberao a tarefa
//		colleagueIds.getItem().add("robson.stirle");

		// CAMPOS DO FORMULARIO
		KeyValueDto keyValueDto = new KeyValueDto();
		keyValueDto.setKey("campo1");
		keyValueDto.setValue("sim");

		KeyValueDtoArray cardData = new KeyValueDtoArray();
		cardData.getItem().add(keyValueDto);

		saveAndSendTaskClassic("usuario.teste", "CB3oWiwG&u#x", 1, 50705, 15, colleagueIds, comments, userId, completeTask, attachments, cardData, appointment, managerMode, threadSequence);

		//return listResponse;
		
	}
	
//	public void buscaContratos() {
//
//		RestTemplate restTemplate = new RestTemplate();
//		HttpHeaders headers = new HttpHeaders();
//		headers.set("Authorization", "Bearer 442f0a0ab7ff96b5121c38fbaaf1f398875ca317fe517860ce9208806ef6225e");
//
//		String url = "https://api.autentique.com.br/v2/graphql";
////		String json = "{\"query\":\"{document(id: \"0835309b4f6fb74f0c487ca1192a3d98dab91ddf0c2578960\") {id name files { original signed } signatures {signed { ...event }}}} fragment event on Event {ipv4 ipv6 reason created_at geolocation {country countryISO state stateISO city zipcode latitude longitude}}\"}";
//
//		String json = "{\"query\":\"{document(id: \\\"0835309b4f6fb74f0c487ca1192a3d98dab91ddf0c2578960\\\") {id name files { original signed } signatures {signed { ...event }}}} fragment event on Event {ipv4 ipv6 reason created_at geolocation {country countryISO state stateISO city zipcode latitude longitude}}\"}";
//
//		Gson gson = new Gson();
//		String json2 = gson.toJson(json);
//
//		HttpEntity<String> entity = new HttpEntity<String>(json2, headers);
//
//		ResponseEntity<String> loginResponse = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
//
//		// String retorno = new Gson().fromJson(loginResponse.getBody(), String.class);
//
//		//System.out.println("RESPOSTA: " + loginResponse.getBody());
//		// //System.out.println("dsdasdAD: " + retorno);
//
//		//System.out.println(json2);
//	}
	
	
	//--------------------------------------------METODOS TRANSFERENCIA DE TITULARIDADE------------------------------------//
	
	public List<ResponseAutentique> movimentaSolicitacaoFluigAutentiqueTitularidade(List<IntegracaoAutentiqueDto> listSolicitacao) {

		List<ResponseAutentique> listResponse = new ArrayList<>();
		ResponseAutentique responseAutentique;
		String valorCampoForm = "";
		String obs_rejeitado = "";
		int nr_solicitacao = 0;
		int nr_atividade = 0;
		String id_contrato = "";
		String fluxo_processo = "";
		String valorCampoForm_v = "";
		String obs_rejeitado_v = "";
		String id_contrato_v = "";
		
		try {
			
			int x = 0;
			while(x < listSolicitacao.size()) {
				
				nr_solicitacao = Integer.parseInt(listSolicitacao.get(x).getNr_solicitacao());
				nr_atividade = listSolicitacao.get(x).getAtividade();

				valorCampoForm = listSolicitacao.get(x).getContrato_assinado() ;
				obs_rejeitado = listSolicitacao.get(x).getObs_rejeitado();
				id_contrato = listSolicitacao.get(x).getId_contrato();
				
				List<IntegracaoAutentiqueDto> listSolicT2 = new ArrayList<>();
						
				//System.out.println("Solicitacao");
				//System.out.println(Integer.toString(nr_solicitacao));
				
				listSolicT2 = daoLigueApi.buscaContratosPorSolicitacaoTitularidade2(Integer.toString(nr_solicitacao));
					
				valorCampoForm_v = listSolicT2.get(0).getContrato_assinado() ;
				obs_rejeitado_v = listSolicT2.get(0).getObs_rejeitado();
				id_contrato_v = listSolicT2.get(0).getId_contrato();
				
				x++;
			}
			
			//System.out.println("campo url: " + valorCampoForm);
			//System.out.println("solicitacao: " + nr_solicitacao);
			//System.out.println("numero atividade: " + nr_atividade);
			//System.out.println("obs_rejeitado : " + obs_rejeitado);
			//System.out.println("id_contrato : " + id_contrato);

			//System.out.println("chamou funcao movimentaSolicitacaoFluigAutentique Titularidade");
			
			if(obs_rejeitado.equals("OK") && obs_rejeitado_v.equals("OK")) {
				fluxo_processo = "AOK";
			}else {
				fluxo_processo = "AERRO";
			}

			String comments = "Movimentado de forma automática pelo sistema";

			// dados de um usuario admin
			String usuario = "robson.stirle";
			String senha = "ePV%SP8x@lQr";

			// consultar ada para pegar codigo do vendedor
			// buscar vendedor pelo codigo de ada em sa3 para pegar o login do fluig

			String userId = "usuario.teste"; // usuario q ira executar a tarefa, nesse caso o gestor do processo
			StringArray colleagueIds = new StringArray(); // usuarios q receberao a tarefa
			int company = 1;
			boolean completeTask = true; // indica se vai enviar a solicitacao ou somente salvar
			boolean managerMode = true; // indica se o usuario esta executando a tarefa como gestor
			int threadSequence = 0; // indica se ha atividade paralela no processo, se nao existir o valor é 0, caso exista, este valor pode ser de 1 a infinito dependendo da quantidade de atividade paralelas existentes no processo.
			ProcessAttachmentDtoArray attachments = new ProcessAttachmentDtoArray(); // anexos do processo
			ProcessTaskAppointmentDtoArray appointment = new ProcessTaskAppointmentDtoArray();
			
			// CAMPOS DO FORMULARIO CLIENTE ATUAL
			KeyValueDto keyValueDto = new KeyValueDto();
			keyValueDto.setKey("url_contrato"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto.setValue(valorCampoForm); // valor do campo do formulario
			
			KeyValueDto keyValueDto1 = new KeyValueDto();
			keyValueDto1.setKey("obs_integracao_autentique"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto1.setValue(obs_rejeitado); // valor do campo do formulario
			
			KeyValueDto keyValueDto2 = new KeyValueDto();
			keyValueDto2.setKey("id_contrato"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto2.setValue(id_contrato); // valor do campo do formulario
			
			KeyValueDto keyValueDto3 = new KeyValueDto();
			keyValueDto3.setKey("integracao_iclass_autentique"); // nome do campo do formulario q armazenara fluxo do processo
			keyValueDto3.setValue(fluxo_processo); // valor do campo do formulario

			//----------------------------------------------------------------------------------------------------------------//
			
			//CAMPOS DO FORMULARIO CLIENTE NOVO
			KeyValueDto keyValueDto4 = new KeyValueDto();
			keyValueDto4.setKey("url_contrato_v"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto4.setValue(valorCampoForm_v); // valor do campo do formulario
			
			KeyValueDto keyValueDto5 = new KeyValueDto();
			keyValueDto5.setKey("obs_integracao_autentique_v"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto5.setValue(obs_rejeitado_v); // valor do campo do formulario
			
			KeyValueDto keyValueDto6 = new KeyValueDto();
			keyValueDto6.setKey("id_contrato_v"); // nome do campo do formulario q armazenara o link dos contratos
			keyValueDto6.setValue(id_contrato_v); // valor do campo do formulario
			
			KeyValueDto keyValueDto7 = new KeyValueDto();
			keyValueDto7.setKey("integracao_iclass_autentique_v"); // nome do campo do formulario q armazenara fluxo do processo
			keyValueDto7.setValue(fluxo_processo); // valor do campo do formulario
			
			//---------------------------------------------------------------------------------------------------------------//

			KeyValueDtoArray cardData = new KeyValueDtoArray(); // passa campos dos formularios com seus valores
			cardData = dadosFormulario(nr_solicitacao);		// busca dados ja existentes na solicitacao, caso nao fizer isso os campos ficam em branco
			cardData.getItem().add(keyValueDto);
			cardData.getItem().add(keyValueDto1);
			cardData.getItem().add(keyValueDto2);
			cardData.getItem().add(keyValueDto3);
			cardData.getItem().add(keyValueDto4);
			cardData.getItem().add(keyValueDto5);
			cardData.getItem().add(keyValueDto6);
			cardData.getItem().add(keyValueDto7);

			listResponse = saveAndSendTaskClassic(usuario, senha, company, nr_solicitacao, nr_atividade, colleagueIds, comments, userId, completeTask, attachments, cardData, appointment, managerMode, threadSequence);
			
		}catch(Exception e) {
			
			//System.out.println("deu erro na movimentacao da solicitacao: " + e);
			e.printStackTrace();
			
			responseAutentique = new ResponseAutentique();
			responseAutentique.setCod_status("500");
			responseAutentique.setStatus("ERROR: " + e);
			responseAutentique.setId_contrato("");
			responseAutentique.setNr_solicitacao(String.valueOf(nr_solicitacao));
			responseAutentique.setAtividade(nr_atividade);
			listResponse.add(responseAutentique);
		 
		}
		
		return listResponse;

	}
	
	//---------------------------------------------------------------------------------------------------------------------//

}
