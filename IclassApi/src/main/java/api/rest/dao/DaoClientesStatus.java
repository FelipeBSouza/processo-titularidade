package api.rest.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import api.rest.classes.auxiliar.ClienteStatusURL;
import api.rest.model.totvs.Ada010;
import api.rest.model.totvs.Adb010;
import api.rest.repositoy.ADARepository;
import api.rest.response.ResponseClientesStatus;
import api.rest.response.ResponseClientesStatusAdd;

public class DaoClientesStatus {

	public List<Ada010> contratosClientes;
	public List<Adb010> contratosClientesadb;
	public List<ResponseClientesStatus> clienteStatus;
	public ArrayList<ResponseClientesStatus> retornoClienteStatus;
	public List<ClienteStatusURL> urls;

	@Autowired
	private ADARepository adarepository;

	public DaoClientesStatus(ADARepository adarepository) {
		this.adarepository = adarepository;
	}

	// FUNCAO DE INTEGRACAO COM O METODO DE ADD DAS APIS DO MEDEIROS
	public List<ResponseClientesStatus> integracaoClientesStatus(List<ClienteStatusURL> urls, String AddEdit) {
		
		clienteStatus = new ArrayList<ResponseClientesStatus>();
		String urlCompleta = "";
		
		//System.out.println("//------------------------------------------------------------------------------------------------//"); 

		for (ClienteStatusURL url : urls) {

			RestTemplate restTemplate = new RestTemplate();
			
			//System.out.println("API CLIENTE STATUS PROCESSANDO CLIENTE : " +  url.getNome());
			
			//SE FOR UM NOVO CONTRATO MONTA A URL DE UMA FORMA DIFERENTE DO CASO DE UM EDIT
			if(url.metodo.equals("add")) {
				
				urlCompleta = url.getURL() + url.getMetodo() + 
						"?usuarioid=" + url.getUsuarioid() + 
						"&cpfcnpj=" + url.getCpfcnpj() + 
						"&celular=" + url.getCelular() +
						"&email=" + url.getEmail() + 
						"&nome=" + url.getNome() + 
						"&contratoid=" + url.getNumctr();
			}else {
				
				urlCompleta = url.getURL() + url.getMetodo() + 
						"?usuarioid=" + url.getUsuarioid() + 
						"&contratoid=" + url.getNumctr() + 
						"&novostatus=" + url.getStatus() +
						"&cpfcnpj=" + url.getCpfcnpj() + 
						"&celular=" + url.getCelular() +
						"&email=" + url.getEmail() + 
						"&nome=" + url.getNome();
			}
			
			////System.out.println("URL : " + urlCompleta + " CTR: " + url.getNumctr() + " RECNO : " + url.getRecno() + " TIPO : " + AddEdit);

			//CHAMA API PASSANDO AS URLS PASSADAS POR PARAMETROS
			ResponseEntity<String> response = restTemplate.exchange(urlCompleta, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {});

			//PEGA O RETORNO DA CHAMADA DA API			
			ResponseClientesStatusAdd responseClientesStatusAdd = new Gson().fromJson(response.getBody(), ResponseClientesStatusAdd.class);

			////System.out.println(response);

			// SE DER ERRADO GRAVAR O REGISTRO JOGA NA LISTA DE CLIENTESTATUS O CODIGO DO
			// ERRO O CONTRATO E A MENSAGEM DE ERRO
			if (responseClientesStatusAdd.getStatus() != 0) {
				//System.out.println("ERRO PROCESSAMENTO CLIENTE : " + url.getNome());
				//System.out.println(response);
				clienteStatus.add(new ResponseClientesStatus(AddEdit ,url.getNumctr(), responseClientesStatusAdd.getStatus().toString(), responseClientesStatusAdd.getMsg()));
			} else {
				// DEU CERTO ENTAO MARCA O CAMPO DENTRO DE ADA COM S PARA QUE O MESMO NAO VOLTE A SER ENVIADO EM CASO DE CHAMADA DE EDIT  
				// EM CASO DE UMA CHAMADA DO TIPO ADD MARCA COMO U
				//System.out.println("SUCESSO PROCESSAMENTO CLIENTE : " + url.getNome());
				if(AddEdit.equals("NOVO")) {
										
					adarepository.gravaADA_UAPI(url.getRecno(), "U");
					////System.out.println(chamada);
					
				}else {
					adarepository.gravaADA_UAPI(url.getRecno(), "S");
					
					String chamada = "https://ligue.net:8000/sva-mailer/?cpfcnpj=" + url.getCpfcnpj() + 
							"&email=" + url.getEmail() + 
							"&nome=" + url.getNome() + 
							"&contratoid=" + url.getNumctr();
					
					try {
						//CHAMA API DE EMAIL PASSANDO AS URLS PASSADAS POR PARAMETROS
						restTemplate.exchange(chamada, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}); 
					} catch (Exception e) {
						////System.out.println("NAO VAI ENVIAR EMAIL");
					}
				}				
			}
		}
		
		//System.out.println("//------------------------------------------------------------------------------------------------//"); 

		return clienteStatus;
	}

	// FUNCAO DE INTEGRACAO API MEDEIROS METODO DE EDIT
	public List<ResponseClientesStatus> apiClientesStatus() {
		
		retornoClienteStatus = new ArrayList<ResponseClientesStatus>();
		//--------------------------------------------------------NOVOS CLIENTES------------------------------------------------------------------------//

		//SEMPRE INSTANCIA UMA NOVA LISTA PARA NAO PEGAR OS REGISTROS QUE ESTAVAM PREENCHIDOS ANTERIORMENTE
		urls = new ArrayList<ClienteStatusURL>();
		
		// BUSCA TODOS OS CONTRATOS DOS CLIENTES QUE AINDA NAO FORAM PARA A API(NOVOS)
		contratosClientes = adarepository.buscaTodosContratos();

		for (Ada010 ada : contratosClientes) {
			
			urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/", 
											ada.getAdaNumctr(), 
											ada.getrECNO(), 
											ada.getSa1010().getA1Cod().trim(),
											ada.getSa1010().getA1Cgc().trim().replaceAll("[^0-9]", ""),
											ada.getSa1010().getA1uDddCel().trim() + ada.getSa1010().getA1Cel().trim().replaceAll("[^0-9]", ""),
											ada.getSa1010().getA1Email().trim(),
											ada.getSa1010().getA1Nome().trim(), 
											"add", ""));			  
			
			/*
			 	urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/add?usuarioid=" + ada.getSa1010().getA1Cod().trim() + 
				"&cpfcnpj=" + ada.getSa1010().getA1Cgc().trim().replaceAll("[^0-9]", "") + 
				"&celular=" + ada.getSa1010().getA1uDddCel().trim() + ada.getSa1010().getA1Cel().trim().replaceAll("[^0-9]", "") + 
				"&email=" + ada.getSa1010().getA1Email().trim() + 
				"&nome=" + ada.getSa1010().getA1Nome().trim() + 
				"&contratoid=" + ada.getAdaNumctr().trim(), 
				ada.getAdaNumctr().trim(), 
				ada.getrECNO())); 
			*/
			
			// ALTERAR PARA SO MARCAR NA TRIGGER DE ADB QUANDO FOR DADO INICIO DE VIGENCIA NO ITEM.
	
		}
		
		//ADICIONA A LISTA DE RETORNO QUE CONTEM TODOS OS CLIENTES QUE NAO PUDERAM SER GRAVADOS
		retornoClienteStatus.addAll(integracaoClientesStatus(urls, "NOVO"));
		
		//--------------------------------------------------------CANCELADOS------------------------------------------------------------------------//
		
		//SEMPRE INSTANCIA UMA NOVA LISTA PARA NAO PEGAR OS REGISTROS QUE ESTAVAM PREENCHIDOS ANTERIORMENTE
		urls = new ArrayList<ClienteStatusURL>();
				
		//BUSCA TODOS OS CANCELADOS E ADICIONA NAS URLS (NOVOSTATUS = 2) 
		contratosClientes = adarepository.buscaCanceladosContratos();
				
		for (Ada010 ada : contratosClientes) {
			
			urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/", 
					ada.getAdaNumctr(), 
					ada.getrECNO(), 
					ada.getSa1010().getA1Cod().trim(),
					ada.getSa1010().getA1Cgc().trim().replaceAll("[^0-9]", ""),
					//ada.getSa1010().getA1uDddCel().trim() + ada.getSa1010().getA1Cel().trim().replaceAll("[^0-9]", ""),
					ada.getSa1010().getA1Email().trim(),
					ada.getSa1010().getA1Nome().trim(), 
					"edit", "2"));
			
			/*
			 urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/edit?usuarioid=" + ada.getSa1010().getA1Cod().trim() + 
					"&contratoid=" + ada.getAdaNumctr().trim() + 
					"&novostatus=2",
					ada.getAdaNumctr().trim(), 
					ada.getrECNO()));
			*/
		}
		
		//ADICIONA A LISTA DE RETORNO QUE CONTEM TODOS OS CLIENTES QUE NAO PUDERAM SER GRAVADOS
		retornoClienteStatus.addAll(integracaoClientesStatus(urls, "CANCELADO"));
		
		//--------------------------------------------------------BLOQUEADOS------------------------------------------------------------------------//
		
		//SEMPRE INSTANCIA UMA NOVA LISTA PARA NAO PEGAR OS REGISTROS QUE ESTAVAM PREENCHIDOS ANTERIORMENTE
		urls = new ArrayList<ClienteStatusURL>();
		
		//BUSCA OS BLOQUEADOS E ADICIONA NAS URLS (NOVOSTATUS = 1)
		contratosClientes = adarepository.buscaBloqueadosContratos();
		
		for (Ada010 ada : contratosClientes) {
			
			urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/", 
					ada.getAdaNumctr(), 
					ada.getrECNO(), 
					ada.getSa1010().getA1Cod().trim(),
					ada.getSa1010().getA1Cgc().trim().replaceAll("[^0-9]", ""),
					//ada.getSa1010().getA1uDddCel().trim() + ada.getSa1010().getA1Cel().trim().replaceAll("[^0-9]", ""),
					ada.getSa1010().getA1Email().trim(),
					ada.getSa1010().getA1Nome().trim(), 
					"edit", "1"));
			
			/* 
			 urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/edit?usuarioid=" + ada.getSa1010().getA1Cod().trim() + 
					"&contratoid=" + ada.getAdaNumctr().trim() + 
					"&novostatus=1",
					ada.getAdaNumctr().trim(), 
					ada.getrECNO()));
			 */
		}
		
		//ADICIONA A LISTA DE RETORNO QUE CONTEM TODOS OS CLIENTES QUE NAO PUDERAM SER GRAVADOS
		retornoClienteStatus.addAll(integracaoClientesStatus(urls, "BLOQUEADO"));
		
		//--------------------------------------------------------SUSPENSOS------------------------------------------------------------------------//
		
		//SEMPRE INSTANCIA UMA NOVA LISTA PARA NAO PEGAR OS REGISTROS QUE ESTAVAM PREENCHIDOS ANTERIORMENTE
		urls = new ArrayList<ClienteStatusURL>();
		
		//BUSCA TODOS OS SUSPENSOS E ADICIONA NAS URLS (NOVOSTATUS = 3) 
		contratosClientes = adarepository.buscaSuspensosContratos();
		
		for (Ada010 ada : contratosClientes) {
			
			urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/", 
					ada.getAdaNumctr(), 
					ada.getrECNO(), 
					ada.getSa1010().getA1Cod().trim(),
					ada.getSa1010().getA1Cgc().trim().replaceAll("[^0-9]", ""),
					//ada.getSa1010().getA1uDddCel().trim() + ada.getSa1010().getA1Cel().trim().replaceAll("[^0-9]", ""),
					ada.getSa1010().getA1Email().trim(),
					ada.getSa1010().getA1Nome().trim(), 
					"edit", "3"));
			
			/*
			 	urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/edit?usuarioid=" + ada.getSa1010().getA1Cod().trim() + 
				"&contratoid=" + ada.getAdaNumctr().trim() + 
				"&novostatus=3",
				ada.getAdaNumctr().trim(), 
				ada.getrECNO()));
			 */
		}
		
		//ADICIONA A LISTA DE RETORNO QUE CONTEM TODOS OS CLIENTES QUE NAO PUDERAM SER GRAVADOS
		retornoClienteStatus.addAll(integracaoClientesStatus(urls, "SUSPENSO"));
		
		//--------------------------------------------------------DESBLOQUEADOS---------------------------------------------------------------------//
		
		//SEMPRE INSTANCIA UMA NOVA LISTA PARA NAO PEGAR OS REGISTROS QUE ESTAVAM PREENCHIDOS ANTERIORMENTE
		urls = new ArrayList<ClienteStatusURL>();
		
		//BUSCA TODOS OS DESBLOQUEADOS E ADICIONA NAS URLS (NOVOSTATUS = 4) 
		contratosClientes = adarepository.buscaDesbloqueadosContratos();
		
		for (Ada010 ada : contratosClientes) {
			
			urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/", 
					ada.getAdaNumctr(), 
					ada.getrECNO(), 
					ada.getSa1010().getA1Cod().trim(),
					ada.getSa1010().getA1Cgc().trim().replaceAll("[^0-9]", ""),
					//ada.getSa1010().getA1uDddCel().trim() + ada.getSa1010().getA1Cel().trim().replaceAll("[^0-9]", ""),
					ada.getSa1010().getA1Email().trim(),
					ada.getSa1010().getA1Nome().trim(), 
					"edit", "4"));
			
			/*
			 urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/edit?usuarioid=" + ada.getSa1010().getA1Cod().trim() + 
					"&contratoid=" + ada.getAdaNumctr().trim() + 
					"&novostatus=4",
					ada.getAdaNumctr().trim(), 
					ada.getrECNO()));
			 */
			
		}
		
		//ADICIONA A LISTA DE RETORNO QUE CONTEM TODOS OS CLIENTES QUE NAO PUDERAM SER GRAVADOS
		retornoClienteStatus.addAll(integracaoClientesStatus(urls, "DESBLOQUEADO"));
		
		//--------------------------------------------------------SEM ALTERACAO-------------------------------------------------------------------//
		
		//VERIFICA CAMPOS QUE ESTAO COM ADA_UAPI = 'U' MAS OS CAMPOS DE STATUS NÃO FORAM ALTERADOS E MARCA OS CAMPOS DE UAPI E CAPI COM 'S'
		contratosClientes = adarepository.buscaTodosSemAlterar();
		
		for (Ada010 ada : contratosClientes) {
			
			urls.add(new ClienteStatusURL("https://auth.app.ligue.net/internal/", 
					ada.getAdaNumctr(), 
					ada.getrECNO(), 
					ada.getSa1010().getA1Cod().trim(),
					ada.getSa1010().getA1Cgc().trim().replaceAll("[^0-9]", ""),
					//ada.getSa1010().getA1uDddCel().trim() + ada.getSa1010().getA1Cel().trim().replaceAll("[^0-9]", ""),
					ada.getSa1010().getA1Email().trim(),
					ada.getSa1010().getA1Nome().trim(), 
					"edit", "4"));
		}
		
		//ADICIONA A LISTA DE RETORNO QUE CONTEM TODOS OS CLIENTES QUE NAO PUDERAM SER GRAVADOS
		retornoClienteStatus.addAll(integracaoClientesStatus(urls, "NOVOS"));
		
		//----------------------------------------------------------FIM--------------------------------------------------------------------------//
		
		//RETORNA AS CHAMADAS QUE SERÃO FEITAS NA API DO MEDEIROS
		return retornoClienteStatus;
	}

	
}
