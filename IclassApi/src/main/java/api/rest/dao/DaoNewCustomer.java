package api.rest.dao;

import _103._15._3._10._8086.ARRAYOFLISTA7101;
import _103._15._3._10._8086.ARRAYOFLISTA7102;
import _103._15._3._10._8086.ARRAYOFLISTA7103;
import _103._15._3._10._8086.ITENSLIST7101;
import _103._15._3._10._8086.ITENSLIST7102;
import _103._15._3._10._8086.ITENSLIST7103;
import _103._15._3._10._8086.LISTA7101;
import _103._15._3._10._8086.LISTA7102;
import _103._15._3._10._8086.LISTA7103;
import api.rest.newCustomer.DataNewCustomer;
import api.rest.newCustomer.FoneTb;
import api.rest.newCustomer.AddressTb;
import api.rest.newCustomer.PartnersTb;
import api.rest.venda.nova.RespostaIntegracao;

public class DaoNewCustomer {	
	
	public RespostaIntegracao gravaDadosNewCustomer(DataNewCustomer newCustomer) {	
		
		String retorno = "";
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
		
		ITENSLIST7101 fones = new ITENSLIST7101();							
		ARRAYOFLISTA7101 arrayFones = new ARRAYOFLISTA7101(); 
		
		ITENSLIST7102 addresses = new ITENSLIST7102();							
		ARRAYOFLISTA7102 arrayAddresses = new ARRAYOFLISTA7102();
		
		ITENSLIST7103 partners = new ITENSLIST7103();							
		ARRAYOFLISTA7103 arrayPartners = new ARRAYOFLISTA7103(); 
		
		LISTA7101 fone;	
		LISTA7102 address;
		LISTA7103 partner;
			
		if(newCustomer.getDADOS().get_DADOS7101().size() <= 0) {
			
			fone = new LISTA7101();
			fone.setDADOS7101("");
			arrayFones.getLISTA7101().add(fone);
			
		}else {
			
			//array de fones
			for(FoneTb linhaFone : newCustomer.getDADOS().get_DADOS7101()) {
				
				fone = new LISTA7101();
				fone.setDADOS7101(linhaFone.getPADDDI() + " ;" + 
						linhaFone.getPADDD()  + " ;" + 
						linhaFone.getPATEL()  + " ;" + 
						linhaFone.getPATIPO()  + " ;" +  
						linhaFone.getPAOP()  + " ;" + 
						linhaFone.getCDTELEF()  + " ;" + 
						linhaFone.getPTREC()  + " ;"
						
						);
				
				arrayFones.getLISTA7101().add(fone);	
				
			}
			
		}	
		
		fones.setREGISTROS7101(arrayFones);	

		if(newCustomer.getDADOS().get_DADOS7102().size() <= 0) {
			
			address = new LISTA7102();
			address.setDADOS7102("");
			arrayAddresses.getLISTA7102().add(address);
			
		}else{
			
			for(AddressTb addressTb : newCustomer.getDADOS().get_DADOS7102()) {
				
				address = new LISTA7102();
				address.setDADOS7102(addressTb.getPEEND() + " ;" + 
						addressTb.getPEENDNUM() + " ;" + 
						addressTb.getPECOMP() + " ;" + 
						addressTb.getPEBAIRRO() + " ;" + 
						addressTb.getPECEP() + " ;" + 
						addressTb.getPEMUN() + " ;" + 
						addressTb.getPEESTADO() + " ;" + 
						addressTb.getPETIPO() + " ;" + 
						addressTb.getPEOP() + " ;" + 
						addressTb.getPETIPO2() + " ;" +
						addressTb.getCDEND()  + " ;" 
						);
				
				arrayAddresses.getLISTA7102().add(address);
				
			}			
			
		}
		
		addresses.setREGISTROS7102(arrayAddresses);
		
		if(newCustomer.getDADOS().get_DADOS7103().size() <= 0) {
			
			partner = new LISTA7103();
			partner.setDADOS7103("");
			arrayPartners.getLISTA7103().add(partner);
			
		}else{
			
			for(PartnersTb partnerTb : newCustomer.getDADOS().get_DADOS7103()) {
				
				partner = new LISTA7103();
				partner.setDADOS7103(partnerTb.getPARTNAME() + " ;" +
						partnerTb.getPARTCGC()  + " ;");
				
				arrayPartners.getLISTA7103().add(partner);
				
			}
			
		}
		
		partners.setREGISTROS7103(arrayPartners);
			
		retorno = LIGWS71GR(newCustomer.getDADOS().getCPESSOA(),
				newCustomer.getDADOS().getCCGC(),
				newCustomer.getDADOS().getCNOME(),
				newCustomer.getDADOS().getCEMAIL(),
				newCustomer.getDADOS().getCDTNASC(),
				newCustomer.getDADOS().getCINSCR(),
				newCustomer.getDADOS().getCRG(),
				newCustomer.getDADOS().getCCONTAT(),
				newCustomer.getDADOS().getCCPFCON(),
				newCustomer.getDADOS().getCTIPO(),
				newCustomer.getDADOS().getDDDCLI(),
				newCustomer.getDADOS().getTELCLI(),
				newCustomer.getDADOS().getDDDCELCL(),
				newCustomer.getDADOS().getCELCLI(),
				newCustomer.getDADOS().getCODSU5(),
				newCustomer.getDADOS().getCDLJCLI(),
				newCustomer.getDADOS().getMTHRNAME(),
				newCustomer.getDADOS().getFNTSNAME(),
				fones, addresses, partners);
		
		if(retorno.toString().substring(0, 1).equals("A") 
				|| retorno.toString().substring(0, 1).equals("C") 
				|| retorno.toString().substring(0, 1).equals("I") 
				|| retorno.toString().substring(0, 1).equals("Z")) {
			respostaIntegracao.setMENSAGEM(retorno);
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("201");
			
		}else {
			respostaIntegracao.setMENSAGEM("ERROR " + retorno.toString());
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("500");
		}
		
		return respostaIntegracao;
		
	}	
	
	private String LIGWS71GR(String CPESSOA, String CCGC, String CNOME, String CEMAIL, String CDTNASC, String CINSCR, String CRG, 
			String CCONTAT, String CCPFCON, String CTIPO, String DDDCLI, String TELCLI, String DDDCELCL, String CELCLI, 
			String CODSU5, String CDLJCLI, String MTHRNAME, String FNTSNAME, _103._15._3._10._8086.ITENSLIST7101 dados7101, 
			_103._15._3._10._8086.ITENSLIST7102 dados7102, _103._15._3._10._8086.ITENSLIST7103 dados7103) {
        _103._15._3._10._8086.LIGWS071 service = new _103._15._3._10._8086.LIGWS071();
        _103._15._3._10._8086.LIGWS071SOAP port = service.getLIGWS071SOAP();
        return port.ligws71GR(CPESSOA, CCGC, CNOME, CEMAIL, CDTNASC, CINSCR, CRG, CCONTAT, CCPFCON, CTIPO, DDDCLI, TELCLI, 
        		DDDCELCL, CELCLI, CODSU5, CDLJCLI, MTHRNAME, FNTSNAME, dados7101, dados7102, dados7103);
    }

}
