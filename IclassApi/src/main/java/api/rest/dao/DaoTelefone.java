package api.rest.dao;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import api.rest.model.totvs.Ab6010;
import api.rest.model.totvs.Ada010;
import api.rest.repositoy.AB6Repository;
import api.rest.repositoy.ADARepository;
import api.rest.repositoy.AGBRepository;
import api.rest.telefone.FonesInput;
import api.rest.telefone.TelefoneJsonEnvio;
import api.rest.telefone.TelefoneJsonRecebe;

public class DaoTelefone {

	@Autowired
	AGBRepository agbRepository;
	@Autowired
	ADARepository adaRepository;
	@Autowired
	AB6Repository ab6Repository;

	public DaoTelefone(ADARepository adaRepository, AGBRepository agbRepository, AB6Repository ab6Repository) {
		this.adaRepository = adaRepository;
		this.agbRepository = agbRepository;
		this.ab6Repository = ab6Repository;
	}
	
	public String organizaDados(String fones) {
		
		String retorno = "OK";

		List<FonesInput> listFones = new ArrayList<>();

		try {

			JSONObject obj = null;
			obj = new JSONObject(fones.toString());
			//System.out.println(obj.toString());

			JSONArray jArray = obj.getJSONArray("fones");
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject o = jArray.getJSONObject(i);
				//System.out.println("----------------------");
				//System.out.println(o.toString());

				Gson gson = new Gson();
				FonesInput f = gson.fromJson(o.toString(), FonesInput.class);
				listFones.add(f);
			}

			//System.out.println("===========================");
			//System.out.println(listFones.get(0).getCod_agb());
			
			int k = 0;
			while(k < listFones.size()) {
				
				retorno = alteraCadastraFone(listFones.get(k).getTipo2(), listFones.get(k).getDdd(), listFones.get(k).getTelefone(), listFones.get(k).getDdi(), listFones.get(k).getTipo(), listFones.get(k).getComplemento(), listFones.get(k).getTitular(), listFones.get(k).getNr_solicitacao(),
						listFones.get(k).getCod_agb(), listFones.get(k).getRecno(), listFones.get(k).getNovo_alteracao_fone(), listFones.get(k).getCpf_cnpj());
				
				k++;
			}
			
			// se deu tudo certo e alterou/cadastrou fone, entao adiciona obs na OS para ativar linha nova no lugar da portabilidade
			if(retorno.equals("OK")) {
				if(listFones.size() > 0) {	
					
					String nr_solicitacao = listFones.get(0).getNr_solicitacao();
					String obs = "";
					
					obs = listFones.get(0).getObservacao().trim();
					
					List<Ab6010> list = new ArrayList<>();
					
					list = ab6Repository.buscaOSPorNumeroDaSolicitacaoList(nr_solicitacao);
					
					if(list.size() > 0) {
						
//						obs = "ATIVAR LINHA NOVA, DEU PROBLEMA NA PORTABILIDADE " + list.get(0).getAb6Msg().trim();
						
						obs = obs + list.get(0).getAb6Msg().trim();
						
						ab6Repository.alteraOBS(obs, "000159", "INST - PORTABILIDADE", nr_solicitacao, list.get(0).getRECNO().toString());						
						
					}
					
				}
				
			}			

		} catch (Exception e) {
			//System.out.println("deu problema: " + e);
			retorno = "ERRO: deu erro na integracao fone na funcao organizaDados" + e.getMessage();

		}
		
		return retorno;
	}

	public String alteraCadastraFone(String tipo, String ddd, String telefone, String ddi, String tipo2, String complemento, String titular, String nr_solicitacao, String cod_agb, String recno, String novo_alteracao_fone, String cpf_cliente) {

		String retorno = "OK";
		
		//System.out.println("numero da solicitacao: " + nr_solicitacao);
		
		try {			
			
			List<Ada010> list = new ArrayList<>();
			list = adaRepository.buscaPorSolicitacao(nr_solicitacao);
			
			String cod_cliente = "";
			String cod_sua = "";
			String cod_contrato = "";
			String recno_codigo = "";
			String agb_codigo = "";
			
			//System.out.println("dados: " + novo_alteracao_fone);
						
			// entra nesse if se for alteracao 
			if(!novo_alteracao_fone.equals("N")) {				
				recno_codigo = recno;
				agb_codigo = cod_agb;
			}else { // entra nesse else se for cadastro novo de telefone
				TelefoneJsonRecebe telefoneJsonRecebe = geraNovoRegistroAGB();
				recno_codigo = telefoneJsonRecebe.getRECNO().trim();
				agb_codigo = telefoneJsonRecebe.getSEQUENCIAL().trim();
			}
			
			//System.out.println("dados do if lista: " + list.size() + " - " + agb_codigo + " - " + recno_codigo);

			// valida se esta com dados preenchidos e realiza a alteracao/cadastro novo
			if (list.size() > 0 && !agb_codigo.equals("") && !recno_codigo.equals("")) {
				cod_cliente = list.get(0).getSa1010().getA1Cod().trim();
				cod_sua = list.get(0).getAdaUcallc().trim();
				cod_contrato = list.get(0).getAdaNumctr().trim();

				agbRepository.adicionaNovoTelefone(cod_cliente, tipo, ddd, telefone, ddi, tipo2, complemento, titular, cod_sua, cod_contrato, recno_codigo, agb_codigo, cpf_cliente);

			}else {
				retorno = "Faltou informações no cadastro do telefone";
			}

		} catch (Exception e) {
			//System.out.println("deu erro funcao addFoneCancelaPortabilidade: ");
			e.printStackTrace();
			retorno = "ERRO: " + e.getStackTrace().toString();
		}
		
		return retorno;

	}

	public TelefoneJsonRecebe geraNovoRegistroAGB() {

		TelefoneJsonEnvio telefoneJsonEnvio = new TelefoneJsonEnvio();
		telefoneJsonEnvio.setTABELA("AGB010");

		Gson gson = new Gson();
		String json = gson.toJson(telefoneJsonEnvio);

		//System.out.println(json);

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("tenantId", "01,LG01");

		String url = "http://10.3.15.103:8087/rest/TOTVS_SEQ/getsequenciais";

		HttpEntity<String> entity = new HttpEntity<String>(json, headers);

		ResponseEntity<String> loginResponse = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

		TelefoneJsonRecebe telefoneJsonRecebe = new Gson().fromJson(loginResponse.getBody(), TelefoneJsonRecebe.class);

		//System.out.println("RESPOSTA: " + loginResponse.getBody());

		return telefoneJsonRecebe;
	}

}
