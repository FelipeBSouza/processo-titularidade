package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Sd3010;

@Repository
public interface SD3Repository extends CrudRepository<Sd3010, Integer> {

	@Query(value = "SELECT * FROM SD3010 WHERE D3_UICLAS = 'S' AND D3_ULIBIC = 'N' AND D_E_L_E_T_ = '' AND D3_DOC != 'INVENT'", nativeQuery = true)
	public List<Sd3010> listaProdutosASeremEnviadosIclass();	
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE SD3010 SET D3_ULIBIC = ?1, D3_ULIBFL = ?6 WHERE R_E_C_N_O_ = ?2 AND D3_COD = ?3 AND D_E_L_E_T_ = '' AND D3_LOCAL = ?4 and D3_FILIAL = ?5", nativeQuery = true)
	public void marca_material(String marcacao, int recno, String cod_prod, String cod_armazem, String filial, String controle);	

}