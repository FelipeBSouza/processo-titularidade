package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Ada010;

@Repository
public interface ADARepository extends CrudRepository<Ada010, Integer> {

	@Query(value = "SELECT * FROM ADA010 WHERE ADA_NUMCTR = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public Ada010 buscaContratoPorNumero(String nr_contrato);
	
	@Query(value = "SELECT ADA_UCALLC FROM ADA010 WHERE ADA_NUMCTR = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String busca_codigo_atendimento(String nr_contrato);
	
	@Query(value = "SELECT * FROM ADA010 WHERE ADA_UCLFLU = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Ada010> buscaPorSolicitacao(String nr_solicitacao);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE ADA010 SET D_E_L_E_T_ = '*' WHERE ADA_NUMCTR = ?1", nativeQuery = true)
	public void marca_delete(String nr_contrato);
		
	//---------------------------------------------------------- API MEDEIROS -----------------------------------------------------------------//

	@Query(value = "SELECT * FROM ADA010 WHERE D_E_L_E_T_ = '' AND ADA_NUMCTR <> '' AND ADA_CODCLI <> '' AND ADA_UDTINI <> '' AND ADA_UAPI = 'I' AND ADA_CAPI = '';", nativeQuery = true)
	public List<Ada010> buscaTodosContratos();
	
	@Query(value = "SELECT * FROM ADA010 WHERE D_E_L_E_T_ = '' AND ADA_NUMCTR <> '' AND ADA_CODCLI <> '' AND ADA_UDTINI <> '' AND ADA_UAPI = 'U' AND ADA_MSBLQL = '1';", nativeQuery = true)
	public List<Ada010> buscaCanceladosContratos();
	
	@Query(value = "SELECT * FROM ADA010 WHERE D_E_L_E_T_ = '' AND ADA_NUMCTR <> '' AND ADA_CODCLI <> '' AND ADA_UDTINI <> '' AND ADA_UAPI = 'U' AND ADA_MSBLQL = '' AND ADA_UBLQYA = '' AND ADA_USUSPE = '';", nativeQuery = true)
	public List<Ada010> buscaTodosSemAlterar();
	
	@Query(value = "SELECT * FROM ADA010 WHERE D_E_L_E_T_ = '' AND ADA_NUMCTR <> '' AND ADA_CODCLI <> '' AND ADA_UDTINI <> '' AND ADA_UAPI = 'U' AND ADA_UBLQYA = '1';", nativeQuery = true)
	public List<Ada010> buscaBloqueadosContratos();
	
	@Query(value = "SELECT * FROM ADA010 WHERE D_E_L_E_T_ = '' AND ADA_NUMCTR <> '' AND ADA_CODCLI <> '' AND ADA_UDTINI <> '' AND ADA_UAPI = 'U' AND ADA_UBLQYA = '2';", nativeQuery = true)
	public List<Ada010> buscaDesbloqueadosContratos();
	
	@Query(value = "SELECT * FROM ADA010 WHERE D_E_L_E_T_ = '' AND ADA_NUMCTR <> '' AND ADA_CODCLI <> '' AND ADA_UDTINI <> '' AND ADA_UAPI = 'U' AND ADA_USUSPE = 'S';", nativeQuery = true)
	public List<Ada010> buscaSuspensosContratos();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE ADA010 SET ADA_UAPI = ?2 ,ADA_CAPI = 'S' WHERE R_E_C_N_O_ = ?1", nativeQuery = true)
	public void gravaADA_UAPI(Integer recno, String UAPI);
	
	//------------------------------------------------------ FIM API MEDEIROS -----------------------------------------------------------------//

}
