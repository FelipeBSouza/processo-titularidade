package api.rest.repositoy;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Sz2010;

@Repository
public interface SZ2Repository extends CrudRepository<Sz2010, Integer> {

	@Query(value = "SELECT Z2_ACAO FROM SZ2010 WHERE Z2_NUMOS = ?1 AND Z2_ITEMOS = ?2 AND D_E_L_E_T_ = '';", nativeQuery = true)
	public String buscaAcao(String num_os, String item_os);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE SZ2010 SET D_E_L_E_T_= '*' WHERE Z2_NUMOS = ?1", nativeQuery = true)
	public void marca_delete(String num_os);	
		
}
