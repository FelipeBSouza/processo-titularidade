package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Adb010;

@Repository
public interface ADBRepository extends CrudRepository<Adb010, Integer> {
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE ADB010 SET D_E_L_E_T_ = '*' WHERE ADB_UNUMAT = ?1", nativeQuery = true)
	public void marca_delete(String cod_atendimento);
	
	@Query(value = "SELECT COUNT(ADB_NUMCTR) FROM ADB010 WHERE ADB_NUMCTR = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public int qtd_itens_nao_deletados(String nr_contrato);
	
	@Query(value = "SELECT * FROM ADB010 WHERE ADB_NUMCTR = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Adb010> itensContrato(String nr_contrato);
		
}
