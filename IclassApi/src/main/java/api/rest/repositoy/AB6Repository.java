package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Ab6010;

@Repository
public interface AB6Repository extends CrudRepository<Ab6010, Integer> {

	// SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND D_E_L_E_T_ = ''
	//@Query(value = "SELECT * FROM AB6010 WHERE AB6_NUMOS = '244503'", nativeQuery = true) //175052
	@Query(value = "SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND D_E_L_E_T_ = '' AND AB6_STATUS != 'E' AND AB6_STATUS != 'C'", nativeQuery = true)
	//@Query(value = "SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND D_E_L_E_T_ = '' AND AB6_STATUS != 'E' AND AB6_STATUS != 'C' ORDER BY R_E_C_N_O_ DESC", nativeQuery = true)
	public List<Ab6010> listaOsASeremCriadas();

	@Query(value = "SELECT * FROM AB6010 WHERE AB6_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public Ab6010 buscaOSPorNumeroDaOS(String num_os);
	
	@Query(value = "SELECT * FROM AB6010 WHERE AB6_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Ab6010> buscaOSPorNumeroDaOSList(String num_os);

	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_ULIBIC = ?1, AB6_ULIBFL = ?4 WHERE R_E_C_N_O_ = ?2 AND AB6_NUMOS = ?3 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void marca_os(String marcacao, int recno, String num_os, String controle);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_ULIBIC = ?1, AB6_ULIBFL = ?4 WHERE R_E_C_N_O_ = ?2 AND AB6_NUMOS = ?3", nativeQuery = true)
	public void marca_os_delete(String marcacao, int recno, String num_os, String controle);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_ULIBIC = ?1, AB6_ULIBFL = ?3 WHERE AB6_NUMOS = ?2 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void marca_os_sem_recno(String marcacao, String num_os, String controle);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_UICLAS = 'S', AB6_ULIBIC = 'N', AB6_ULIBFL = '' WHERE AB6_UCLFLU = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void marca_os_pela_solicitacao(String nr_solicitacao);

	@Query(value = "SELECT AB6_UNUMCT FROM AB6010 WHERE AB6_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String buscaNumeroContratoOS(String num_os);
	
	@Query(value = "SELECT * FROM AB6010 WHERE AB6_UCLFLU = ?1 AND D_E_L_E_T_ = '' AND AB6_OSANT = ''", nativeQuery = true)
	public List<Ab6010> buscaPeloNumeroSolicitacao(String nr_solicitacao);

	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_STATUS = 'E' WHERE R_E_C_N_O_ = ?1 AND AB6_NUMOS = ?2 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void encerra_os(int recno, String num_os);
	
	//@Query(value = "SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND D_E_L_E_T_ = '' AND (AB6_STATUS = 'E' OR AB6_STATUS = 'C')", nativeQuery = true)
	@Query(value = "SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND (AB6_STATUS = 'E' OR AB6_STATUS = 'C')", nativeQuery = true)
	public List<Ab6010> listaOsASeremEncerradasIclass();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_RETCOM = 'S', AB6_ATEND = ?1, AB6_UCODAT = ?2  WHERE R_E_C_N_O_ = ?3 AND AB6_NUMOS = ?4 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void mudaFilaComercial(String nome_atendente, String cod_atendente, int recno, String num_os); 
	
	@Transactional
	@Modifying
	//@Query(value = "UPDATE AB6010 SET AB6_ULIBIC = 'S', AB6_ULIBFL = 'S', D_E_L_E_T_= '*' WHERE AB6_NUMOS = ?1", nativeQuery = true)
	@Query(value = "UPDATE AB6010 SET D_E_L_E_T_= '*', AB6_STATUS = 'C' WHERE AB6_NUMOS = ?1", nativeQuery = true)
	public void marca_delete_os(String num_os);	
	
	@Query(value = "SELECT AB6_UCLFLU FROM AB6010 WHERE AB6_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String buscaNumeroSolicitacao(String num_os);	
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_RETCOM = 'S', AB6_ULIBFL = 'S' WHERE R_E_C_N_O_ = ?1 AND AB6_NUMOS = ?2 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void marca_os_reenviar_iclass(int recno, String num_os);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_UICLAS = 'S', AB6_ULIBIC = 'S', AB6_ULIBFL = 'S', AB6_MSG = ?1, AB6_UCODAT = ?2, AB6_ATEND = ?3 WHERE AB6_UCLFLU = ?4 AND D_E_L_E_T_ = '' AND R_E_C_N_O_ = ?5", nativeQuery = true)
	public void alteraOBS(String textoOBS, String cod_atendente, String nome_atendente, String nr_solicitacao, String recno);
	
	@Query(value = "SELECT * FROM AB6010 WHERE AB6_UCLFLU = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Ab6010> buscaOSPorNumeroDaSolicitacaoList(String nr_solicitacao);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_ULIBFL = 'S', AB6_STATUS = 'B' WHERE AB6_UCLFLU = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void coloca_os_em_atendimento(String nr_solicitacao);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_ULIBFL = 'S', AB6_MSG = ?1 WHERE R_E_C_N_O_ = ?2 AND AB6_NUMOS = ?3 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void alteraObservacaoOS(String obs, int recno, String num_os);
		
}
