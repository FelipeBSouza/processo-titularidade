package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Agb010;

@Repository
public interface AGBRepository extends CrudRepository<Agb010, Integer> {

	@Query(value = "SELECT * FROM AGB010 WHERE AGB_UTIPO2 = 'P' AND AGB_UCALLC = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Agb010> buscaSeTemPortabilidade(String nr_atendimento);		
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AGB010 SET AGB_ENTIDA = 'SA1', AGB_CODENT = ?1, AGB_TIPO = ?2, AGB_PADRAO = '1', AGB_DDD = ?3, AGB_TELEFO = ?4, AGB_DDI = ?5, AGB_UTIPO2 = ?6, AGB_COMP = ?7, AGB_UTITUL = ?8, AGB_UCALLC = ?9, AGB_UNUMCT = ?10, AGB_UCGC= ?13 WHERE R_E_C_N_O_ = ?11 AND D_E_L_E_T_ = '' AND AGB_CODIGO = ?12", nativeQuery = true)
	public void adicionaNovoTelefone(String cod_cliente, String tipo, String ddd, String telefone, String ddi, String tipo2, String complemento, String titular, String cod_sua, String cod_contrato, String recno, String cod_agb, String cpf_cliente); 
	
}