package api.rest.repositoy;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Ab7010;

@Repository
public interface AB7Repository extends CrudRepository<Ab7010, Integer> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE AB7010 SET AB7_ULIBIC = ?1, AB7_ULIBFL = ?4 WHERE R_E_C_N_O_ = ?2 AND AB7_NUMOS = ?3 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void marca_item_os(String marcacao, int recno, String num_os, String controle);	
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB7010 SET AB7_ULIBIC = ?1, AB7_ULIBFL = ?4 WHERE R_E_C_N_O_ = ?2 AND AB7_NUMOS = ?3", nativeQuery = true)
	public void marca_item_os_delete(String marcacao, int recno, String num_os, String controle);	
	
	@Query(value = "SELECT * FROM AB7010 WHERE AB67_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public Ab7010 buscaItensOSPorNumeroDaOS(String num_os);	
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB7010 SET AB7_TIPO = '5', AB7_ULIBFL = 'S' WHERE AB7_NUMOS = ?1 AND AB7_ITEM = ?2 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void encerra_item_os(String num_os, String item_os);	
	
	@Transactional
	@Modifying
	//@Query(value = "UPDATE AB7010 SET AB7_ULIBIC = 'S', AB7_ULIBFL = 'S', D_E_L_E_T_= '*' WHERE AB7_NUMOS = ?1", nativeQuery = true)
	@Query(value = "UPDATE AB7010 SET D_E_L_E_T_= '*' WHERE AB7_NUMOS = ?1", nativeQuery = true)
	public void marca_delete_item_os(String num_os);	
	
}
