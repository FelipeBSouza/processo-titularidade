package api.rest.repositoy;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Sua010;

@Repository
public interface SUARepository extends CrudRepository<Sua010, Integer> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE SUA010 SET D_E_L_E_T_ = '*' WHERE UA_NUM = ?1", nativeQuery = true)
	public void marca_delete(String num_os);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE SUA010 SET UA_UCLFLUI = '' WHERE UA_NUM = ?1", nativeQuery = true)
	public void remove_solicitacao(String num_os);
	
	@Query(value = "SELECT UA_NUM FROM SUA010 WHERE UA_UCLFLUI = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String busca_codigo_atendimento(String nr_solicitacao);
		
}
