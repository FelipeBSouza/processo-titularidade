package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Sb2010;

@Repository
public interface SB2Repository extends CrudRepository<Sb2010, Integer> {

	// SELECT * FROM SB2010 WHERE B2_UICLAS = 'S' AND B2_ULIBIC = 'N' AND D_E_L_E_T_ = ''
	//SELECT * FROM SB2010 WHERE B2_LOCAL = '20' AND D_E_L_E_T_ = ''
	@Query(value = "SELECT * FROM SB2010 WHERE B2_UICLAS = 'S' AND B2_ULIBIC = 'N' AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Sb2010> listaProdutosASeremEnviadosIclass();
	
	@Query(value = "SELECT * FROM SB2010 WHERE B2_UICLAS = 'S' AND B2_ULIBIC = 'N' AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Sb2010> listaAtivosASeremEnviadosIclass();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE SB2010 SET B2_ULIBIC = ?1, B2_ULIBFL = ?6 WHERE R_E_C_N_O_ = ?2 AND B2_COD = ?3 AND D_E_L_E_T_ = '' AND B2_LOCAL = ?4 and B2_FILIAL = ?5", nativeQuery = true)
	public void marca_material(String marcacao, int recno, String cod_prod, String cod_armazem, String filial, String controle);	
	
	@Query(value = "SELECT * FROM SB2010 WHERE B2_LOCAL = ?1 AND B2_UFERRAM != 'F' AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Sb2010> listaProdutosPorArmazem(String armazem);

}