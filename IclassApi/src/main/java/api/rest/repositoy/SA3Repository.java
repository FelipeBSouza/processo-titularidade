package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.totvs.Sa1010;

@Repository
public interface SA3Repository extends CrudRepository<Sa1010, Integer> {

	@Query(value = "SELECT * FROM SA3010 WHERE A3_COD = ?1", nativeQuery = true)
	public Sa1010 buscaClientePorCodigo(String cod_cli);
			
}
