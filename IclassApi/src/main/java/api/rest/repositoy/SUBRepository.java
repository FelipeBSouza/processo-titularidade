package api.rest.repositoy;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Sub010;

@Repository
public interface SUBRepository extends CrudRepository<Sub010, Integer> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE SUB010 SET D_E_L_E_T_ = '*' WHERE UB_NUM = ?1", nativeQuery = true)
	public void marca_delete(String num_os);	
		
}
