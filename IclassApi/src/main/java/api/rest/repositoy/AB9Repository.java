package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Ab9010;

@Repository
public interface AB9Repository extends CrudRepository<Ab9010, Integer> {

	@Query(value = "SELECT MAX(AB9_SEQ)+1 AS MAXSEQ FROM AB9010 WHERE AB9_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String buscaProximaSequencia(String num_os);
	
	@Query(value = "SELECT * FROM AB9010 WHERE AB9_NUMOS LIKE %?1% AND D_E_L_E_T_ = '' ORDER BY AB9_SEQ DESC", nativeQuery = true)
	public List<Ab9010> buscaDadosAtendimetoPorNumOS(String num_os);
	
	@Query(value = "SELECT * FROM AB9010 WHERE AB9_UPORT = '' AND D_E_L_E_T_ = '' AND AB9_CDBXA = '000117'", nativeQuery = true)
	public List<Ab9010> listaPortabilidadesOk();
	
	@Query(value = "SELECT * FROM AB9010 WHERE AB9_UPORT = '' AND D_E_L_E_T_ = '' AND AB9_CDBXA = '000116'", nativeQuery = true)
	public List<Ab9010> listaPortabilidadesErro();
		
	@Transactional
	@Modifying
	@Query(value = "UPDATE AB9010 SET AB9_UPORT = 'S' WHERE R_E_C_N_O_ = ?1 AND AB9_NUMOS = ?2 AND AB9_SEQ = ?3 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void marca_atendimento(int recno, String num_os, String sequencia);
		
}
