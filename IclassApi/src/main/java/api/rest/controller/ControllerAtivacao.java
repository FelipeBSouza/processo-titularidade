package api.rest.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dao.DaoAtivarCliente;
import api.rest.dao.Utils;
import api.rest.repositoy.AA3Repository;
import api.rest.repositoy.AB6Repository;
import api.rest.repositoy.ADARepository;
import api.rest.response.ResponseAtivacao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/ATIVACAO")
@Api(value = "API REST ATIVACAO DE CLIENTES")
public class ControllerAtivacao {
	
	@Autowired
	private AA3Repository aa3Repository;
	@Autowired
	private AB6Repository ab6Repository;
	@Autowired
	private ADARepository adarepository; 
	
	Utils util = new Utils();
			
	DaoAtivarCliente daoAtivarCliente;
	
	@ApiOperation(value = "Ativa Cliente")
	@GetMapping(value = "/ativa", produces = "application/json")
	public ResponseEntity<List<ResponseAtivacao>> ativa_cliente(@RequestParam("nr_os") String nr_os, @RequestParam("mac") String mac, @RequestParam("serial") String serial) {

		System.out.println("ControllerAtivacao Função ativa_cliente Inicio: " + util.buscaDataHora());
		
		daoAtivarCliente = new DaoAtivarCliente(aa3Repository, ab6Repository, adarepository);
//		
		List<ResponseAtivacao> listResponse = new ArrayList<>();	
//		
//		// mac = 88B362899E90
//		//serial = ALCLFA5F6BA4
//		// OS = 
//		
//		listResponse = daoAtivarCliente.consultarChassi(nr_os, mac, serial);	
		
		System.out.println("ControllerAtivacao Função ativa_cliente Fim: " + util.buscaDataHora());		
		
		return new ResponseEntity<List<ResponseAtivacao>>(listResponse, HttpStatus.OK);

	}
	
}
