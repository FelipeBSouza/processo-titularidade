package api.rest.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import api.rest.dao.DaoNewCustomer;
import api.rest.dao.DaoRetencao;
import api.rest.dao.DaoRetencaoSVA;
import api.rest.dao.DaoVendaNovaMovel;
import api.rest.dao.DaoVendaNovaSVA;
import api.rest.dao.DaoVendasNovas;
import api.rest.dao.Utils;
import api.rest.newCustomer.DataNewCustomer;
import api.rest.renovacao.DadosRenovacao;
import api.rest.renovacao.DaoRenovacao;
import api.rest.retencao.DadosRetencao;
import api.rest.retencao.sva.DadosRetencaoSVA;
import api.rest.venda.nova.Dados;
import api.rest.venda.nova.DadosMovel;
import api.rest.venda.nova.DadosSVA;
import api.rest.venda.nova.RespostaIntegracao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/totvs")
@Api(value = "API REST WS TOTVS")
public class ControllerTotvs {
	
	Utils util = new Utils();
		
	@ApiOperation(value = "Grava Venda Nova Pelo Ws Soap 65")	
	@PostMapping(value = "/vendaNova", produces = "application/json")
	public ResponseEntity<RespostaIntegracao> gravaVendaNova(@RequestBody String cabecalho) {
		
		System.out.println("ControllerTotvs Função gravaVendaNova Inicio: " + util.buscaDataHora());

		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
									
		try {
			
			JSONObject obj = null;
			obj = new JSONObject(cabecalho.toString());		
			//System.out.println(obj.toString());
					
			Gson gson = new Gson();
			
			Dados dados = gson.fromJson(obj.toString(), Dados.class);
			
			DaoVendasNovas daoVendasNovas = new DaoVendasNovas();
			
			respostaIntegracao = daoVendasNovas.gravaDadosTotvs(dados);
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			 respostaIntegracao = new RespostaIntegracao();
			 respostaIntegracao.setSISTEMA("PROTHEUS");
			 respostaIntegracao.setOS("");
			 respostaIntegracao.setMENSAGEM("Deu erro na integração, contate o suporte");
			 respostaIntegracao.setSTATUS("400");
		}	      
		
		System.out.println("ControllerTotvs Função gravaVendaNova Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<RespostaIntegracao>(respostaIntegracao, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Grava Venda Nova Pelo Ws Soap 67")	
	@PostMapping(value = "/vendaNovaSVA", produces = "application/json")
	public ResponseEntity<RespostaIntegracao> gravaVendaNovaSVA(@RequestBody String cabecalho) {
		
		System.out.println("ControllerTotvs Função gravaVendaNovaSVA Inicio: " + util.buscaDataHora());

		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
									
		try {
			
			JSONObject obj = null;
			obj = new JSONObject(cabecalho.toString());		
					
			Gson gson = new Gson();
			
			DadosSVA dados = gson.fromJson(obj.toString(), DadosSVA.class);
			
			DaoVendaNovaSVA daoVendasNovas = new DaoVendaNovaSVA();
			
			respostaIntegracao = daoVendasNovas.gravaDados(dados);
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			 respostaIntegracao = new RespostaIntegracao();
			 respostaIntegracao.setSISTEMA("PROTHEUS");
			 respostaIntegracao.setOS("");
			 respostaIntegracao.setMENSAGEM("Deu erro na integração, contate o suporte");
			 respostaIntegracao.setSTATUS("400");
		}	      
		
		System.out.println("ControllerTotvs Função gravaVendaNovaSVA Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<RespostaIntegracao>(respostaIntegracao, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Grava Venda Nova Pelo Ws Soap 70")	
	@PostMapping(value = "/vendaNovaMovel", produces = "application/json")
	public ResponseEntity<RespostaIntegracao> gravaVendaNovaMovel(@RequestBody String cabecalho) {
		
		System.out.println("ControllerTotvs Função gravaVendaNovaMovel Inicio: " + util.buscaDataHora());

		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
									
		try {
			
			JSONObject obj = null;
			obj = new JSONObject(cabecalho.toString());		
					
			Gson gson = new Gson();
			
			DadosMovel dados = gson.fromJson(obj.toString(), DadosMovel.class);
			
			DaoVendaNovaMovel daoVendasMovel = new DaoVendaNovaMovel();
			
			respostaIntegracao = daoVendasMovel.gravaDados(dados);
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			 respostaIntegracao = new RespostaIntegracao();
			 respostaIntegracao.setSISTEMA("PROTHEUS");
			 respostaIntegracao.setOS("");
			 respostaIntegracao.setMENSAGEM("Deu erro na integração, contate o suporte");
			 respostaIntegracao.setSTATUS("400");
		}	      
		
		System.out.println("ControllerTotvs Função gravaVendaNovaMovel Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<RespostaIntegracao>(respostaIntegracao, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Renova Planos Pelo Ws Soap 69")	
	@PostMapping(value = "/renovacaoSVA", produces = "application/json")
	public ResponseEntity<RespostaIntegracao> gravaRenovacaoVA(@RequestBody String cabecalho) {
		
		System.out.println("ControllerTotvs Função gravaRenovacaoVA Inicio: " + util.buscaDataHora());

		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
									
		try {
			
			JSONObject obj = null;
			obj = new JSONObject(cabecalho.toString());
			
			System.out.println("CABECALHO QUE CHEGOU");
			System.out.println(cabecalho.toString());
			
			Gson gson = new Gson();
			
			DadosRenovacao dados = gson.fromJson(obj.toString(), DadosRenovacao.class);
			
			DaoRenovacao daoRenovacao = new DaoRenovacao();
			
			System.out.println("MONTOU O GSON: " + dados);
			
			respostaIntegracao = daoRenovacao.gravaDados(dados);
			
		} catch (Exception e) {

			e.printStackTrace();
			
			 respostaIntegracao = new RespostaIntegracao();
			 respostaIntegracao.setSISTEMA("PROTHEUS");
			 respostaIntegracao.setOS("");
			 respostaIntegracao.setMENSAGEM("Deu erro na integração, contate o suporte");
			 respostaIntegracao.setSTATUS("400");
		}	      
		
		System.out.println("ControllerTotvs Função gravaRenovacaoVA Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<RespostaIntegracao>(respostaIntegracao, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Retenção Ws Soap 68")	
	@PostMapping(value = "/retencaoSVA", produces = "application/json")
	public ResponseEntity<RespostaIntegracao> gravaRetencaoSVA(@RequestBody String cabecalho) {

		System.out.println("ControllerTotvs Função gravaRetencaoSVA Inicio: " + util.buscaDataHora());
		
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
									
		try {
			
			JSONObject obj = null;
			obj = new JSONObject(cabecalho.toString());		
					
			Gson gson = new Gson();
			
			DadosRetencaoSVA dados = gson.fromJson(obj.toString(), DadosRetencaoSVA.class);
			
			DaoRetencaoSVA daoRenovacao = new DaoRetencaoSVA();
			
			respostaIntegracao = daoRenovacao.gravaDadosRetencaoTotvs(dados);
			
		} catch (Exception e) {

			e.printStackTrace();
			
			 respostaIntegracao = new RespostaIntegracao();
			 respostaIntegracao.setSISTEMA("PROTHEUS");
			 respostaIntegracao.setOS("");
			 respostaIntegracao.setMENSAGEM("Deu erro na integração, contate o suporte");
			 respostaIntegracao.setSTATUS("400");
		}	      
		
		System.out.println("ControllerTotvs Função gravaRetencaoSVA Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<RespostaIntegracao>(respostaIntegracao, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Grava Dados Retencao Ws Soap 66")	
	@PostMapping(value = "/retencao", produces = "application/json")
	public ResponseEntity<RespostaIntegracao> gravaRetencao(@RequestBody String cabecalho) {
		
		System.out.println("ControllerTotvs Função gravaRetencaoSVA Inicio: " + util.buscaDataHora());

		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
									
		try {
			
			JSONObject obj = null;
			obj = new JSONObject(cabecalho.toString());		
					
			Gson gson = new Gson();
			
			DadosRetencao dadosRetencao = gson.fromJson(obj.toString(), DadosRetencao.class);
			
			DaoRetencao daoRetencao = new DaoRetencao();
			
			respostaIntegracao = daoRetencao.gravaDadosRtencaoTotvs(dadosRetencao);
			
		} catch (Exception e) {

			e.printStackTrace();
			
			 respostaIntegracao = new RespostaIntegracao();
			 respostaIntegracao.setSISTEMA("PROTHEUS");
			 respostaIntegracao.setOS("");
			 respostaIntegracao.setMENSAGEM("Deu erro na integração, contate o suporte");
			 respostaIntegracao.setSTATUS("400");
		}	      
		
		System.out.println("ControllerTotvs Função gravaRetencaoSVA Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<RespostaIntegracao>(respostaIntegracao, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Grava dados cadastro de clientes vendas novas WS071")	
	@PostMapping(value = "/newCustomer", produces = "application/json")
	public ResponseEntity<RespostaIntegracao>newCustomer(@RequestBody String cabecalho) {
		
		System.out.println("ControllerTotvs Função newCustomer Inicio: " + util.buscaDataHora());

		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
									
		try {
			
			JSONObject obj = null;
			obj = new JSONObject(cabecalho.toString());		
					
			Gson gson = new Gson();
			
			DataNewCustomer newCustomer = gson.fromJson(obj.toString(), DataNewCustomer.class);
			
			DaoNewCustomer daoNewCustomer= new DaoNewCustomer();
			
			respostaIntegracao = daoNewCustomer.gravaDadosNewCustomer(newCustomer); 
			
		} catch (Exception e) {

			e.printStackTrace();
			
			 respostaIntegracao = new RespostaIntegracao();
			 respostaIntegracao.setSISTEMA("PROTHEUS");
			 respostaIntegracao.setOS("");
			 respostaIntegracao.setMENSAGEM("ERROR " + e.toString());
			 respostaIntegracao.setSTATUS("400");
		}	      
		
		System.out.println("ControllerTotvs Função newCustomer Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<RespostaIntegracao>(respostaIntegracao, HttpStatus.OK);
		
	}
	
}
