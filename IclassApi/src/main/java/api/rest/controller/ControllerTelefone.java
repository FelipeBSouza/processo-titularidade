package api.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dao.DaoTelefone;
import api.rest.dao.Utils;
import api.rest.repositoy.AB6Repository;
import api.rest.repositoy.ADARepository;
import api.rest.repositoy.AGBRepository;
import api.rest.telefone.ResponsePadrao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/telefone")
@Api(value = "API REST DE TELEFONE")
public class ControllerTelefone {

	@Autowired
	AGBRepository agbRepository;
	@Autowired
	ADARepository adaRepository;
	@Autowired
	AB6Repository ab6Repository;
	
	DaoTelefone daoTelefone;
	
	Utils util = new Utils();
		
	@ApiOperation(value = "Cadastra novo telefone")
	@PostMapping(value = "/addFone", produces = "application/json")
	public ResponseEntity<ResponsePadrao> addFone(@RequestBody String fones) {		   
		
		System.out.println("ControllerTelefone Função addFone Inicio: " + util.buscaDataHora());
		
		String retorno = "OK"; 
		ResponsePadrao telefoneResponse = new ResponsePadrao();	
				
		daoTelefone = new DaoTelefone(adaRepository, agbRepository, ab6Repository);
		retorno = daoTelefone.organizaDados(fones);
				
		telefoneResponse.setStatus(retorno);
		
		System.out.println("ControllerTelefone Função addFone Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<ResponsePadrao>(telefoneResponse, HttpStatus.OK);

	}	

//	@ApiOperation(value = "Cadastra novo telefone")
//	@PostMapping(value = "/somenteTeste", produces = "application/json")
//	public ResponseEntity<String> teste(@RequestBody String fones) {
//		
//		String retorno = "OK";			
//				
//		//System.out.println("entrou na funcao teste : " + fones);	
//		daoTelefone = new DaoTelefone(adaRepository, agbRepository);
//		daoTelefone.organizaDados(fones);
//		
//		
////		List<FonesInput> listFones = new ArrayList<>();
//		
////		try {
////			
////			 JSONObject obj = null;
////			 obj = new JSONObject(fones.toString());
////			 //System.out.println(obj.toString());
////			 
////			 JSONArray jArray = obj.getJSONArray("fones");
////			 for(int i = 0; i < jArray.length(); i++){
////	                JSONObject o = jArray.getJSONObject(i);
////	                //System.out.println("----------------------");
////	                //System.out.println(o.toString());
////	                
////	                Gson gson = new Gson();
////	                FonesInput f = gson.fromJson(o.toString(), FonesInput.class);
////	                listFones.add(f);
////	            }
////			 
////			 //System.out.println("===========================");
////			 //System.out.println(listFones.get(0).getCod_agb());
////			
////		}catch(Exception e) {
////			//System.out.println("deu problema: " + e);
////		}
//								
//		
//		return new ResponseEntity<String>(retorno, HttpStatus.OK);
//
//	}

}
