package api.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.rest.autentique.ResponseAutentique;
import api.rest.dao.DaoPortabilidade;
import api.rest.dao.Utils;
import api.rest.repositoy.AB6Repository;
import api.rest.repositoy.AB9Repository;
import api.rest.repositoy.SYPRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/portabilidadeFluigVendas")
@Api(value = "API REST DE PORTABILIDADE")
public class ControllerPortabilidade {

	@Autowired
	AB9Repository ab9Repository;
	@Autowired
	AB6Repository ab6Repository;
	@Autowired
	SYPRepository sypRepository;
	
	Utils util = new Utils();
	
	@ApiOperation(value = "Portabilidade que deu certo")
	@GetMapping(value = "/verificaPortabilidadeOK", produces = "application/json")
	public ResponseEntity<List<ResponseAutentique>> verificaPortabilidadeOK() {

		System.out.println("ControllerPortabilidade Função verificaPortabilidadeOK Inicio: " + util.buscaDataHora());
		
		List<ResponseAutentique> listResponse = new ArrayList<>();
		
		try {
			
			DaoPortabilidade daoPortabilidade = new DaoPortabilidade(ab9Repository, ab6Repository, sypRepository);
			listResponse = daoPortabilidade.verificaPortabilidadeOK();
		
			
		} catch (Exception e) {
			//System.out.println("erro controler portabilidade funcao verificaPortabilidadeOK: " + e);
		}	    
		
		System.out.println("ControllerPortabilidade Função verificaPortabilidadeOK Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<List<ResponseAutentique>>(listResponse, HttpStatus.OK);

	}
	
	@ApiOperation(value = "Portabilidade que deu errado")
	@GetMapping(value = "/verificaPortabilidadeERRO", produces = "application/json")
	public ResponseEntity<List<ResponseAutentique>> verificaPortabilidadeERRO() {

		System.out.println("ControllerPortabilidade Função verificaPortabilidadeERRO Inicio: " + util.buscaDataHora());
		
		List<ResponseAutentique> listResponse = new ArrayList<>();
		
		try {
			
			DaoPortabilidade daoPortabilidade = new DaoPortabilidade(ab9Repository, ab6Repository, sypRepository);
			listResponse = daoPortabilidade.verificaPortabilidadERRO();
		
			
		} catch (Exception e) {
			//System.out.println("erro controler portabilidade funcao verificaPortabilidadeOK: " + e);
		}	      
		
		System.out.println("ControllerPortabilidade Função verificaPortabilidadeERRO Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<List<ResponseAutentique>>(listResponse, HttpStatus.OK);

	}

}
