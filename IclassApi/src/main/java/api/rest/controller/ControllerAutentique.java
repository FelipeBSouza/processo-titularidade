package api.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import api.rest.autentique.ResponseAutentique;
import api.rest.autentique.ResponseReenvioEmail;
import api.rest.classes.auxiliar.DadosParametroAutentique;
import api.rest.dao.DaoAutentique;
import api.rest.dao.DaoLigueApi;
import api.rest.dao.Utils;
import api.rest.response.ResponseAutentiqueEnvioContrato;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/autentique")
@Api(value = "API REST AUTENTIQUE")
public class ControllerAutentique {
	
	Utils util = new Utils();

	@ApiOperation(value = "Movimenta solicitações que ja tiverem os contratos assinados")
	@GetMapping(value = "/movimentaSolicitacoes", produces = "application/json")
	public ResponseEntity<List<ResponseAutentique>> movimentaSolicitacoes() {

		System.out.println("ControllerAutentique Função movimentaSolicitacoes Inicio: " + util.buscaDataHora());
		
		List<ResponseAutentique> listResponse = new ArrayList<>();
			
		DaoAutentique daoAutentique = new DaoAutentique();
		
		// verifica quais contratos ainda nao foram assinados e realiza busca no autentique para ver se ja foram assinados
		listResponse.addAll(daoAutentique.listaContratosASeremAssinados());
		
		System.out.println("ControllerAutentique Função movimentaSolicitacoes Fim: " + util.buscaDataHora());
										
		return new ResponseEntity<List<ResponseAutentique>>(listResponse, HttpStatus.OK);
	}	
	
	
	@ApiOperation(value = "Deleta contrato do autentique pelo codigo do contrato do autentique")
	@DeleteMapping(value = "/deleteContratoAutentique", produces = "application/json")
	public ResponseEntity<List<ResponseAutentique>> deletaContratoAutentique(@RequestParam("id_contrato") String id_contrato) {

		System.out.println("ControllerAutentique Função deletaContratoAutentique Inicio: " + util.buscaDataHora());
		
		List<ResponseAutentique> listResponse = new ArrayList<>();
		DaoLigueApi daoLigueApi = new DaoLigueApi();
			
		// deleta arquivo no autentique
		DaoAutentique daoAutentique = new DaoAutentique();		
		daoAutentique.deletaContratoAutentique(id_contrato);
		
		// deleta registro tabela postgres
		daoLigueApi.deletaRegistroContrato(id_contrato);
		
		System.out.println("ControllerAutentique Função deletaContratoAutentique Fim: " + util.buscaDataHora());
												
		return new ResponseEntity<List<ResponseAutentique>>(listResponse, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Deleta contrato do autentique pelo numero da solicitação")
	@DeleteMapping(value = "/deleteContratoAutentiqueSolicitacao", produces = "application/json")
	public ResponseEntity<List<ResponseAutentique>> deleteContratoAutentiqueSolicitacao(@RequestParam("nr_solicitacao") String nr_solicitacao) {

		System.out.println("ControllerAutentique Função deleteContratoAutentiqueSolicitacao Inicio: " + util.buscaDataHora());
		
		List<ResponseAutentique> listResponse = new ArrayList<>();
			
		// deleta arquivo no autentique e tbm o registro da tabela do postgres
		DaoAutentique daoAutentique = new DaoAutentique();		
		daoAutentique.deletaContratoAutentiqueSolicitacao(nr_solicitacao);
		
		System.out.println("ControllerAutentique Função deleteContratoAutentiqueSolicitacao Fim: " + util.buscaDataHora());
												
		return new ResponseEntity<List<ResponseAutentique>>(listResponse, HttpStatus.OK);
	}
	
//	@ApiOperation(value = "Movimenta solicitação por ID")
//	@GetMapping(value = "/movimentaSolicitacaoID", produces = "application/json")
//	public ResponseEntity<String> movimentaSolicitacaoPorId(@RequestParam("nr_solicitacao") int nr_solicitacao) {
//
//		//System.out.println("entrou na funcao movimentaSolicitacaoPorId");
//			
//		DaoFluig daoFluig = new DaoFluig();
//		
//		daoFluig.movimentaSolicitacaoFluigVendas(nr_solicitacao, 0, "laudo q vem da OS", "AERRO","","");  		
//										
//		return new ResponseEntity<String>("", HttpStatus.OK);
//	}	
	
	@ApiOperation(value = "Reenvia Email Para o Autentique")
	@GetMapping(value = "/reenviaEmail", produces = "application/json")
	public ResponseEntity<ResponseReenvioEmail> reenviaEmailAutentique(@RequestParam("email") String email, @RequestParam("idContrato") String idContrato) {

		System.out.println("ControllerAutentique Função reenviaEmailAutentique Inicio: " + util.buscaDataHora());
			
		DaoAutentique daoAutentique = new DaoAutentique();	
		ResponseReenvioEmail responseReenvioEmail = new ResponseReenvioEmail();
		
		responseReenvioEmail = daoAutentique.reenviarContrato(email, idContrato);		
		
		System.out.println("ControllerAutentique Função reenviaEmailAutentique Fim: " + util.buscaDataHora());
										
		return new ResponseEntity<ResponseReenvioEmail>(responseReenvioEmail, HttpStatus.OK);
	}	

	@ApiOperation(value = "Envia o Contrato Para o Autentique")
	@PostMapping(value = "/enviaContrato", produces = "application/json")
	public ResponseEntity<ResponseAutentiqueEnvioContrato> enviaContratoAutentique(MultipartFile arquivo, String dados) {//MultipartFile
		
		System.out.println("ControllerAutentique Função enviaContratoAutentique Inicio: " + util.buscaDataHora());
		
		ResponseAutentiqueEnvioContrato responseAutentiqueEnvioContrato = new ResponseAutentiqueEnvioContrato();
		ObjectMapper mapper = new ObjectMapper();
		DadosParametroAutentique dadosParametroAutentique = new DadosParametroAutentique();
		
		DaoAutentique daoAutentique = new DaoAutentique();

		try {
			
			dadosParametroAutentique = mapper.readValue(dados, DadosParametroAutentique.class);

			responseAutentiqueEnvioContrato = daoAutentique.envioContratoAutentique(arquivo, dadosParametroAutentique.getEmail(), dadosParametroAutentique.getNomeArquivo());

		} catch (Exception e) {

			e.printStackTrace();

			responseAutentiqueEnvioContrato.setCodigo("02");
			responseAutentiqueEnvioContrato.setMensagem("Falha nos dados passados por parametro");

		}		
		
		System.out.println("ControllerAutentique Função enviaContratoAutentique Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<ResponseAutentiqueEnvioContrato>(responseAutentiqueEnvioContrato, HttpStatus.OK);
		
	}	
	
	
	//-------------------------------APIS TITULARIDADE-----------------------------------------------//
	
	
	@ApiOperation(value = "Movimenta solicitações que ja tiverem os contratos assinados")
	@GetMapping(value = "/movimentaSolicitacoesTitularidade", produces = "application/json")
	public ResponseEntity<List<ResponseAutentique>> movimentaSolicitacoesTitularidade() {
		
		System.out.println("ControllerAutentique Função movimentaSolicitacoesTitularidade Inicio: " + util.buscaDataHora());

		List<ResponseAutentique> listResponse = new ArrayList<>();
			
		DaoAutentique daoAutentique = new DaoAutentique();
		
		// VERIFICA QUAIS CONTRATOS AINDA NAO FORAM ASSINADOS E REALIZA BUSCA NO AUTENTIQUE PARA VER SE JA FORAM ASSINADOS
		listResponse.addAll(daoAutentique.listaContratosASeremAssinadosTitularidade());
		
		System.out.println("ControllerAutentique Função movimentaSolicitacoesTitularidade Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<List<ResponseAutentique>>(listResponse, HttpStatus.OK);
	}
	
	
	
	//---------------------------------------------------------------------------------------------//
	
	
//	@ApiOperation(value = "TESTE")
//	@GetMapping(value = "/TESTE", produces = "application/json")
//	public ResponseEntity<List<ResponseAutentique>> teste() {
//
//		//System.out.println("entrou na funcao de teste");
//
//		List<ResponseAutentique> listResponse = new ArrayList<>();
//			
//		//DaoAutentique daoAutentique = new DaoAutentique();
//		
//		//daoAutentique.buscaContratosAssinados("21f635e7ede23cd5df1e4788dba73a1185b2ce67508e7bd8e", "249015", 149);
//		
//		// 608bace2fd9761362a6f296c97ddd7ee6bde92f8cb8efc9a5 email incorreto
//		// b337b5998e7ed683d6b86d417b78ebc2ea455d22ea0876a4a assinado
//		// 2d6a411c54a316bd43ba653cc1d3b0f14d5f8538be0e651ff nao assinado
//		// verifica quais contratos ainda nao foram assinados e realiza busca no autentique para ver se ja foram assinados
//		//listResponse.add(daoAutentique.buscaContratosAssinados("2d6a411c54a316bd43ba653cc1d3b0f14d5f8538be0e651ff","161458",6));
//		
//		DaoFluig daoFluig = new DaoFluig();
//		
//		daoFluig.arrumaDataNascimento("1990-08-05");
//				
//										
//		return new ResponseEntity<List<ResponseAutentique>>(listResponse, HttpStatus.OK);
//	}
	
	@GetMapping(value = "/testeAutentique", produces = "application/json")
	public ResponseEntity<String> testApiAutentique() {		
		return new ResponseEntity<String>("Ok", HttpStatus.OK);
	}

}
