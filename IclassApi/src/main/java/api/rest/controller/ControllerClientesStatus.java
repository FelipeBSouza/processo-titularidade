package api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dao.DaoClientesStatus;
import api.rest.repositoy.ADARepository;
import api.rest.response.ResponseClientesStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/IntegracaoApiCliStatus")
@Api(value = "API REST CLIENTES STATUS")
public class ControllerClientesStatus {
	
	@Autowired
	private ADARepository adarepository; 
			
	DaoClientesStatus clientesStatus;
	
	@ApiOperation(value = "Integracao Cliente Status")
	@PostMapping(value = "/clientesStatus", produces = "application/json")
	public ResponseEntity<List<ResponseClientesStatus>> ClientesStatus() {
		
		clientesStatus = new DaoClientesStatus(adarepository);
								
		return new ResponseEntity<List<ResponseClientesStatus>>((List<ResponseClientesStatus>) clientesStatus.apiClientesStatus(), HttpStatus.OK);

	}
	
	@ApiOperation(value = "Teste Api")
	@GetMapping(value = "/testeAPI", produces = "application/json")
	public ResponseEntity<String> TesteApi() {		
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
	
}
