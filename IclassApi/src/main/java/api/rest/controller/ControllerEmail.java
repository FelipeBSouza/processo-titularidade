package api.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dao.DaoEmail;
import api.rest.dao.Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/email")
@Api(value = "API REST DE EMAIL")
public class ControllerEmail {
	
	@Autowired
    JavaMailSender mailSender;	
	
	Utils util = new Utils();
	
	@ApiOperation(value = "Envia Email")
	@GetMapping(value = "/envia", produces = "application/json")
	public ResponseEntity<String> mailSender(@RequestParam (value = "email") String email, @RequestParam (value = "assunto") String assunto, @RequestParam (value = "conteudo") String conteudo) {

		System.out.println("ControllerEmail Função mailSender Inicio: " + util.buscaDataHora());
		
		DaoEmail daoEmail = new DaoEmail(mailSender);		
		
		daoEmail.enviaEmail(email, assunto, conteudo);		
		
		System.out.println("ControllerEmail Função mailSender Fim: " + util.buscaDataHora());
		
		return new ResponseEntity<String>("ok", HttpStatus.OK);

	}

}
