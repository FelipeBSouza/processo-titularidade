package api.rest.alteracao.os;

public class DadosAlteracaoMsgOS {
	
	private String OS;
	private String MSG;
	
	
	public String getOS() {
		return OS;
	}
	public void setOS(String oS) {
		OS = oS;
	}
	public String getMSG() {
		return MSG;
	}
	public void setMSG(String mSG) {
		MSG = mSG;
	}
}
