package api.rest.agendamento;

import javax.xml.datatype.XMLGregorianCalendar;

public class RestJsonDisponibilidades {
	
	private Double custoFim;
	private Double custoInicio;
	private XMLGregorianCalendar dataFim;
	private XMLGregorianCalendar dataInicio;	
	
	public Double getCustoFim() {
		return custoFim;
	}
	public void setCustoFim(Double custoFim) {
		this.custoFim = custoFim;
	}
	public Double getCustoInicio() {
		return custoInicio;
	}
	public void setCustoInicio(Double custoInicio) {
		this.custoInicio = custoInicio;
	}
	public XMLGregorianCalendar getDataFim() {
		return dataFim;
	}
	public void setDataFim(XMLGregorianCalendar dataFim) {
		this.dataFim = dataFim;
	}
	public XMLGregorianCalendar getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(XMLGregorianCalendar dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	
	

}
