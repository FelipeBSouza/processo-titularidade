package api.rest.agendamento;

import java.util.List;

public class RestJsonAgendamentoDisponivel {
	
	private String msg;
	private String cod_erro;
	
	private List<RestJsonCabecalhoDisponibilidades> listDisponibilidades;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCod_erro() {
		return cod_erro;
	}

	public void setCod_erro(String cod_erro) {
		this.cod_erro = cod_erro;
	}
	
	public List<RestJsonCabecalhoDisponibilidades> getListDisponibilidades() {
		return listDisponibilidades;
	}

	public void setListDisponibilidades(List<RestJsonCabecalhoDisponibilidades> listDisponibilidades) {
		this.listDisponibilidades = listDisponibilidades;
	}

}
