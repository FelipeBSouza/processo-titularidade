package api.rest.agendamento;
import javax.xml.datatype.XMLGregorianCalendar;

public class RestJsonAgendamento {
	
	private XMLGregorianCalendar dt_agendamento;
	private XMLGregorianCalendar dt_maxima_agendamento;
	private String equipe;
	private XMLGregorianCalendar inicio_deslocamento;
	private XMLGregorianCalendar fim_deslocamento;
	private XMLGregorianCalendar inicio_atendimento;
	private XMLGregorianCalendar fim_atendimento;	
	
	public XMLGregorianCalendar getDt_agendamento() {
		return dt_agendamento;
	}
	public void setDt_agendamento(XMLGregorianCalendar dt_agendamento) {
		this.dt_agendamento = dt_agendamento;
	}
	
	public XMLGregorianCalendar getDt_maxima_agendamento() {
		return dt_maxima_agendamento;
	}
	public void setDt_maxima_agendamento(XMLGregorianCalendar dt_maxima_agendamento) {
		this.dt_maxima_agendamento = dt_maxima_agendamento;
	}
	public String getEquipe() {
		return equipe;
	}
	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}
	public XMLGregorianCalendar getInicio_deslocamento() {
		return inicio_deslocamento;
	}
	public void setInicio_deslocamento(XMLGregorianCalendar inicio_deslocamento) {
		this.inicio_deslocamento = inicio_deslocamento;
	}
	public XMLGregorianCalendar getFim_deslocamento() {
		return fim_deslocamento;
	}
	public void setFim_deslocamento(XMLGregorianCalendar fim_deslocamento) {
		this.fim_deslocamento = fim_deslocamento;
	}
	public XMLGregorianCalendar getInicio_atendimento() {
		return inicio_atendimento;
	}
	public void setInicio_atendimento(XMLGregorianCalendar inicio_atendimento) {
		this.inicio_atendimento = inicio_atendimento;
	}
	public XMLGregorianCalendar getFim_atendimento() {
		return fim_atendimento;
	}
	public void setFim_atendimento(XMLGregorianCalendar fim_atendimento) {
		this.fim_atendimento = fim_atendimento;
	}	

}
