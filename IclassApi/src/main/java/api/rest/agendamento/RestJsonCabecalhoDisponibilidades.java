package api.rest.agendamento;

import java.util.List;

public class RestJsonCabecalhoDisponibilidades {
	
	private String credenciada;
	private String equipe;
	
	private List<RestJsonDisponibilidades> disponibilidadesEquipe;

	public String getCredenciada() {
		return credenciada;
	}

	public void setCredenciada(String credenciada) {
		this.credenciada = credenciada;
	}

	public String getEquipe() {
		return equipe;
	}

	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}

	public List<RestJsonDisponibilidades> getListDisponibilidade() {
		return disponibilidadesEquipe;
	}

	public void setListDisponibilidade(List<RestJsonDisponibilidades> listDisponibilidade) {
		this.disponibilidadesEquipe = listDisponibilidade;
	}

}
