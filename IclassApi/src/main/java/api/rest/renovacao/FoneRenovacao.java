package api.rest.renovacao;

public class FoneRenovacao {
	
	private String DDI_FONE;
	private String DDD_FONE;
	private String FONE;
	private String TIPO_TELEFONE;
	private String NOVO_ALTERACAO_FONE;
	private String COD_AGB;
	private String VERIFICATIPO;
	private String COMPLEMENTO_FONE;
	private String TITULAR_FONE;
	private String CPF_CNPJ;
	private String ID_ITEM;
	
	public String getDDI_FONE() {
		return DDI_FONE;
	}
	public void setDDI_FONE(String dDI_FONE) {
		DDI_FONE = dDI_FONE;
	}
	public String getDDD_FONE() {
		return DDD_FONE;
	}
	public void setDDD_FONE(String dDD_FONE) {
		DDD_FONE = dDD_FONE;
	}
	public String getFONE() {
		return FONE;
	}
	public void setFONE(String fONE) {
		FONE = fONE;
	}
	public String getTIPO_TELEFONE() {
		return TIPO_TELEFONE;
	}
	public void setTIPO_TELEFONE(String tIPO_TELEFONE) {
		TIPO_TELEFONE = tIPO_TELEFONE;
	}
	public String getNOVO_ALTERACAO_FONE() {
		return NOVO_ALTERACAO_FONE;
	}
	public void setNOVO_ALTERACAO_FONE(String nOVO_ALTERACAO_FONE) {
		NOVO_ALTERACAO_FONE = nOVO_ALTERACAO_FONE;
	}
	public String getCOD_AGB() {
		return COD_AGB;
	}
	public void setCOD_AGB(String cOD_AGB) {
		COD_AGB = cOD_AGB;
	}
	public String getVERIFICATIPO() {
		return VERIFICATIPO;
	}
	public void setVERIFICATIPO(String vERIFICATIPO) {
		VERIFICATIPO = vERIFICATIPO;
	}
	public String getCOMPLEMENTO_FONE() {
		return COMPLEMENTO_FONE;
	}
	public void setCOMPLEMENTO_FONE(String cOMPLEMENTO_FONE) {
		COMPLEMENTO_FONE = cOMPLEMENTO_FONE;
	}
	public String getTITULAR_FONE() {
		return TITULAR_FONE;
	}
	public void setTITULAR_FONE(String tITULAR_FONE) {
		TITULAR_FONE = tITULAR_FONE;
	}
	public String getCPF_CNPJ() {
		return CPF_CNPJ;
	}
	public void setCPF_CNPJ(String cPF_CNPJ) {
		CPF_CNPJ = cPF_CNPJ;
	}
	public String getID_ITEM() {
		return ID_ITEM;
	}
	public void setID_ITEM(String iD_ITEM) {
		ID_ITEM = iD_ITEM;
	}		

}
