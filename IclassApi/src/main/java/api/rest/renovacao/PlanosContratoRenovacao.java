package api.rest.renovacao;

public class PlanosContratoRenovacao {
	
	private String CODPRODUTOITEM;
	private String CODITEM;
	private String CODENDINS;
	private String OPCAOSEL;
	private String CODPRODNOVO;
	private String CODPORANTATU;
	
	
	public String getCODPRODUTOITEM() {
		return CODPRODUTOITEM;
	}
	public void setCODPRODUTOITEM(String cODPRODUTOITEM) {
		CODPRODUTOITEM = cODPRODUTOITEM;
	}
	public String getCODITEM() {
		return CODITEM;
	}
	public void setCODITEM(String cODITEM) {
		CODITEM = cODITEM;
	}
	public String getCODENDINS() {
		return CODENDINS;
	}
	public void setCODENDINS(String cODENDINS) {
		CODENDINS = cODENDINS;
	}
	public String getOPCAOSEL() {
		return OPCAOSEL;
	}
	public void setOPCAOSEL(String oPCAOSEL) {
		OPCAOSEL = oPCAOSEL;
	}
	public String getCODPRODNOVO() {
		return CODPRODNOVO;
	}
	public void setCODPRODNOVO(String cODPRODNOVO) {
		CODPRODNOVO = cODPRODNOVO;
	}
	public String getCODPORANTATU() {
		return CODPORANTATU;
	}
	public void setCODPORANTATU(String cODPORANTATU) {
		CODPORANTATU = cODPORANTATU;
	}	

}
