package api.rest.renovacao;

import _103._15._3._10._8086.ARRAYOFLISTA0169;
import _103._15._3._10._8086.ARRAYOFLISTA0269;
import _103._15._3._10._8086.ARRAYOFLISTA0369;
import _103._15._3._10._8086.ITENSLIST0169;
import _103._15._3._10._8086.ITENSLIST0269;
import _103._15._3._10._8086.ITENSLIST0369;
import _103._15._3._10._8086.LISTA0169;
import _103._15._3._10._8086.LISTA0269;
import _103._15._3._10._8086.LISTA0369;
import api.rest.venda.nova.RespostaIntegracao;

public class DaoRenovacao {
	
	public RespostaIntegracao gravaDados(DadosRenovacao dados) {
		
		String retorno = "";
		RespostaIntegracao respostaIntegracao = new RespostaIntegracao();
		
		System.out.println("ENTROU NO GRAVA DADOS");
		
		//PLANOS DA TABELA DOS ITENS JA EXISTENTES NO CONTRATO
		ITENSLIST0269 planosTb1 = new ITENSLIST0269();							
		ARRAYOFLISTA0269 arrayPlanosTb1 = new ARRAYOFLISTA0269(); // contem todos os planos que o cliente ja possui
		
		//PLANOS DA TABELA DE ITENS A SEREM RENOVADOS/CONTRATADOS
		ITENSLIST0369 planosTb2 = new ITENSLIST0369();							
		ARRAYOFLISTA0369 arrayPlanosTb2 = new ARRAYOFLISTA0369(); // contem todos os planos que o cliente vai contratar/renovar
		
		//TABELA DE FONES PORTABILIDADE E LINHA NOVA
		ITENSLIST0169 fones = new ITENSLIST0169();							
		ARRAYOFLISTA0169 arrayFones = new ARRAYOFLISTA0169(); // contem todos os fones de portabilidade e/ou linha nova
		
		// item plano tabela itens ja contratados
		LISTA0269 planoTb1;
		// item plano tabela itens a serem renovados/contratados
		LISTA0369 planoTb2;
		// item fone
		LISTA0169 fone;		
		
		System.out.println("CRIOU AS LISTAS");
		
		//ARRAY DE TELEFONE
		if(dados.getDADOS().getFONES().size() <= 0) {
			
			fone = new LISTA0169();
			fone.setDADOS0169("");
			arrayFones.getLISTA0169().add(fone);
			
		}else {
			
			//array de fones
			for(FoneRenovacao linhaFone : dados.getDADOS().getFONES()) {
				
				fone = new LISTA0169();
				fone.setDADOS0169(
						linhaFone.getDDI_FONE() + " ;" + 
						linhaFone.getDDD_FONE()  + " ;" + 
						linhaFone.getFONE()  + " ;" + 
						linhaFone.getTIPO_TELEFONE()  + " ;" + 
						"1 ;" + 
						linhaFone.getNOVO_ALTERACAO_FONE()  + " ;" + 
						linhaFone.getCOD_AGB()  + " ;" + 
						linhaFone.getVERIFICATIPO()  + " ;" + 
						linhaFone.getCOMPLEMENTO_FONE()  + " ;" + 
						linhaFone.getTITULAR_FONE()  + " ;" + 
						linhaFone.getCPF_CNPJ()  + " ;" +
						linhaFone.getID_ITEM()  + " ;" 
						
						);
				
				arrayFones.getLISTA0169().add(fone);	
				
			}
			
		}	
		
		fones.setREGISTROS0169(arrayFones);	
		
		System.out.println("ADICIONOU OS TELEFONES");
		
		// ARRAY DE PLANOS TB1
		if(dados.getDADOS().getPLANOSCONTRATO().size() <= 0) {
			
			planoTb1 = new LISTA0269();
			planoTb1.setDADOS0269("");
			arrayPlanosTb1.getLISTA0269().add(planoTb1);
			
		}else{
			
			for(PlanosContratoRenovacao linhaItemTb1 : dados.getDADOS().getPLANOSCONTRATO()) {
				
				planoTb1 = new LISTA0269();
				planoTb1.setDADOS0269(linhaItemTb1.getCODPRODUTOITEM() + " ;" + 
						linhaItemTb1.getCODITEM() + " ;" + 
						linhaItemTb1.getCODENDINS() + " ;" + 
						linhaItemTb1.getOPCAOSEL() + " ;" + 
						linhaItemTb1.getCODPRODNOVO() + " ;" + 
						linhaItemTb1.getCODPORANTATU()  + " ;" 
						);
				
				arrayPlanosTb1.getLISTA0269().add(planoTb1);
				
			}			
			
		}
		
		System.out.println("ADICIONOU OS ITENS JA CONTRATADOS");
		
		planosTb1.setREGISTROS0269(arrayPlanosTb1);
		
		// ARRAY DE PLANOS TB2
		if(dados.getDADOS().getPLANOS().size() <= 0) {
			
			planoTb2 = new LISTA0369();
			planoTb2.setDADOS0369("");
			arrayPlanosTb2.getLISTA0369().add(planoTb2);
			
		}else{
			
			for(PlanosRenovadosRenovacao linhaItemTb2 : dados.getDADOS().getPLANOS()) {
				
				planoTb2 = new LISTA0369();
				planoTb2.setDADOS0369(
						linhaItemTb2.getCODPRODUTOITEM() + " ;" + 
						linhaItemTb2.getCODITEM() + " ;" + 
						linhaItemTb2.getQUANTIDADEITEM() + " ;" + 
						linhaItemTb2.getTOTALITEM() + " ;" + 
						linhaItemTb2.getVALORDESCONTO() + " ;" + 
						linhaItemTb2.getOBSITEM() + " ;" + 
						linhaItemTb2.getQTMESINI() + " ;" + 
						linhaItemTb2.getQTMESCOB() + " ;" + 
						linhaItemTb2.getCODINS() + " ;" + 
						linhaItemTb2.getVALORUNITARIO() + " ;" + 						
						linhaItemTb2.getCODPRODANTIGO() + " ;" + 
						linhaItemTb2.getCOD_TARIFA() + " ;" + 
						linhaItemTb2.getVALORCHEIO() + " ;" + 
						linhaItemTb2.getOSOBRG() + " ;" + 
						linhaItemTb2.getID_ITEM()  + " ;" + 
						linhaItemTb2.getTP_PLANO()  + " ;"					
						
						);
				
				arrayPlanosTb2.getLISTA0369().add(planoTb2);
				
			}
			
		}
		
		planosTb2.setREGISTROS0369(arrayPlanosTb2);
		
		System.out.println("ADICIONOU OS NOVOS ITENS E VAI CHAMAR O WS");
		System.out.println("planosTb1 : " + planosTb1);
		System.out.println("planosTb2 : " + planosTb2);
		System.out.println("fones : " + fones);
		
		if(dados.getDADOS().getOBSSUA().equals("null")) {
			System.out.println("OBS SUA");
		}

		retorno = ligws69GR(dados.getDADOS().getTPPESSOA(),
				dados.getDADOS().getNCTR(),
				dados.getDADOS().getCDCLI(),
				dados.getDADOS().getLOJA(),
				dados.getDADOS().getVEND1(),
				dados.getDADOS().getOBS_OS1(),
				dados.getDADOS().getOBS_OS2(),
				dados.getDADOS().getOPERADOR(),
				dados.getDADOS().getCDPGTO(),
				dados.getDADOS().getOPERACAO(),
				dados.getDADOS().getMIDIA(),
				dados.getDADOS().getTMK(),
				dados.getDADOS().getTPCLI(),
				dados.getDADOS().getTPCTR(),
				dados.getDADOS().getPRAZO(),
				dados.getDADOS().getVALCTR(),
				dados.getDADOS().getUDIAFE(),
				dados.getDADOS().getASSCLI(),
				dados.getDADOS().getOBSSUA(),
				dados.getDADOS().getCODENDC(),
				dados.getDADOS().getCX(),
				dados.getDADOS().getPT(),
				dados.getDADOS().getCCGCLI(),
				dados.getDADOS().getNRSOLIC(),
				dados.getDADOS().getURLGRV(),
				dados.getDADOS().getFTDIGTAL(),						
				dados.getDADOS().getTEM_INS(),
				planosTb1, planosTb2,fones
				);
		
		System.out.println("PASSOU PELA INTEGRACAO COM O RETORNO : " + retorno);
		
		if(retorno.equals("")) {
			respostaIntegracao.setMENSAGEM("OK");
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("201");
			
		}else {
			respostaIntegracao.setMENSAGEM(retorno.toString());
			respostaIntegracao.setOS("");
			respostaIntegracao.setSISTEMA("PROTHEUS");
			respostaIntegracao.setSTATUS("500");
		}
		
		return respostaIntegracao;
		
	}
	
	
	 public String ligws69GR(String tppessoa, String nctr, String cdcli, String loja, String vend1, String obsOS1, String obsOS2, String operador, String cdpgto, String operacao, String midia, String tmk, String tpcli, String tpctr, String prazo, String valctr, String udiafe, String asscli, String obssua, String codendc, String cx, String pt, String ccgcli, String nrsolic, String urlgrv, String ftdigtal, String temINS, _103._15._3._10._8086.ITENSLIST0269 dados0269, _103._15._3._10._8086.ITENSLIST0369 dados0369, _103._15._3._10._8086.ITENSLIST0169 dados0169) {
	        _103._15._3._10._8086.LIGWS069 service = new _103._15._3._10._8086.LIGWS069();
	        _103._15._3._10._8086.LIGWS069SOAP port = service.getLIGWS069SOAP();
	        System.out.println("ENTROU NO ligws69GR");
	        return port.ligws69GR(tppessoa, nctr, cdcli, loja, vend1, obsOS1, obsOS2, operador, cdpgto, operacao, midia, tmk, tpcli, tpctr, prazo, valctr, udiafe, asscli, obssua, codendc, cx, pt, ccgcli, nrsolic, urlgrv, ftdigtal, temINS, dados0269, dados0369, dados0169);
	    }

}
