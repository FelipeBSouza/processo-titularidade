package api.rest.renovacao;

public class PlanosRenovadosRenovacao {
	
	private String CODPRODUTOITEM;
	private String CODITEM;
	private String QUANTIDADEITEM;
	private String TOTALITEM;
	private String VALORDESCONTO;
	private String OBSITEM;
	private String QTMESINI;
	private String QTMESCOB;
	private String CODINS;
	private String VALORUNITARIO;
	private String CODPRODANTIGO;
	private String COD_TARIFA;
	private String VALORCHEIO;
	private String OSOBRG;
	private String ID_ITEM;
	private String TP_PLANO;
	
	public String getTP_PLANO() {
		return TP_PLANO;
	}
	public void setTP_PLANO(String tP_PLANO) {
		TP_PLANO = tP_PLANO;
	}
	public String getCODPRODUTOITEM() {
		return CODPRODUTOITEM;
	}
	public void setCODPRODUTOITEM(String cODPRODUTOITEM) {
		CODPRODUTOITEM = cODPRODUTOITEM;
	}
	public String getCODITEM() {
		return CODITEM;
	}
	public void setCODITEM(String cODITEM) {
		CODITEM = cODITEM;
	}
	public String getQUANTIDADEITEM() {
		return QUANTIDADEITEM;
	}
	public void setQUANTIDADEITEM(String qUANTIDADEITEM) {
		QUANTIDADEITEM = qUANTIDADEITEM;
	}
	public String getTOTALITEM() {
		return TOTALITEM;
	}
	public void setTOTALITEM(String tOTALITEM) {
		TOTALITEM = tOTALITEM;
	}
	public String getVALORDESCONTO() {
		return VALORDESCONTO;
	}
	public void setVALORDESCONTO(String vALORDESCONTO) {
		VALORDESCONTO = vALORDESCONTO;
	}
	public String getOBSITEM() {
		return OBSITEM;
	}
	public void setOBSITEM(String oBSITEM) {
		OBSITEM = oBSITEM;
	}
	public String getQTMESINI() {
		return QTMESINI;
	}
	public void setQTMESINI(String qTMESINI) {
		QTMESINI = qTMESINI;
	}
	public String getQTMESCOB() {
		return QTMESCOB;
	}
	public void setQTMESCOB(String qTMESCOB) {
		QTMESCOB = qTMESCOB;
	}
	public String getCODINS() {
		return CODINS;
	}
	public void setCODINS(String cODINS) {
		CODINS = cODINS;
	}
	public String getVALORUNITARIO() {
		return VALORUNITARIO;
	}
	public void setVALORUNITARIO(String vALORUNITARIO) {
		VALORUNITARIO = vALORUNITARIO;
	}
	public String getCODPRODANTIGO() {
		return CODPRODANTIGO;
	}
	public void setCODPRODANTIGO(String cODPRODANTIGO) {
		CODPRODANTIGO = cODPRODANTIGO;
	}
	public String getCOD_TARIFA() {
		return COD_TARIFA;
	}
	public void setCOD_TARIFA(String cOD_TARIFA) {
		COD_TARIFA = cOD_TARIFA;
	}
	public String getVALORCHEIO() {
		return VALORCHEIO;
	}
	public void setVALORCHEIO(String vALORCHEIO) {
		VALORCHEIO = vALORCHEIO;
	}
	public String getOSOBRG() {
		return OSOBRG;
	}
	public void setOSOBRG(String oSOBRG) {
		OSOBRG = oSOBRG;
	}
	public String getID_ITEM() {
		return ID_ITEM;
	}
	public void setID_ITEM(String iD_ITEM) {
		ID_ITEM = iD_ITEM;
	}	
	
}
