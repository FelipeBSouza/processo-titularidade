package api.rest.model.internet;
// Generated 11/12/2012 10:35:37 by Hibernate Tools 3.2.1.GA


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@Entity
@Table(schema="internet")
public class Radippool  implements java.io.Serializable {

     @Id
     private long id;
     private String poolName;
     private Serializable framedipaddress;
     private String nasipaddress;
     private String poolKey;
     private String calledstationid;
     private String callingstationid;
     @Temporal(javax.persistence.TemporalType.DATE)
     private Date expiryTime;
     private String username;

    public Radippool() {
    }

	
    public Radippool(long id, String poolName, Serializable framedipaddress, String nasipaddress, String poolKey, String callingstationid, Date expiryTime) {
        this.id = id;
        this.poolName = poolName;
        this.framedipaddress = framedipaddress;
        this.nasipaddress = nasipaddress;
        this.poolKey = poolKey;
        this.callingstationid = callingstationid;
        this.expiryTime = expiryTime;
    }
    public Radippool(long id, String poolName, Serializable framedipaddress, String nasipaddress, String poolKey, String calledstationid, String callingstationid, Date expiryTime, String username) {
       this.id = id;
       this.poolName = poolName;
       this.framedipaddress = framedipaddress;
       this.nasipaddress = nasipaddress;
       this.poolKey = poolKey;
       this.calledstationid = calledstationid;
       this.callingstationid = callingstationid;
       this.expiryTime = expiryTime;
       this.username = username;
    }
   
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public String getPoolName() {
        return this.poolName;
    }
    
    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }
    public Serializable getFramedipaddress() {
        return this.framedipaddress;
    }
    
    public void setFramedipaddress(Serializable framedipaddress) {
        this.framedipaddress = framedipaddress;
    }
    public String getNasipaddress() {
        return this.nasipaddress;
    }
    
    public void setNasipaddress(String nasipaddress) {
        this.nasipaddress = nasipaddress;
    }
    public String getPoolKey() {
        return this.poolKey;
    }
    
    public void setPoolKey(String poolKey) {
        this.poolKey = poolKey;
    }
    public String getCalledstationid() {
        return this.calledstationid;
    }
    
    public void setCalledstationid(String calledstationid) {
        this.calledstationid = calledstationid;
    }
    public String getCallingstationid() {
        return this.callingstationid;
    }
    
    public void setCallingstationid(String callingstationid) {
        this.callingstationid = callingstationid;
    }
    public Date getExpiryTime() {
        return this.expiryTime;
    }
    
    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }




}


