package api.rest.model.internet;
// Generated 11/12/2012 10:35:37 by Hibernate Tools 3.2.1.GA

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

import api.rest.model.integrador.Internet;

@XmlRootElement
@Entity
@Table(schema = "internet")
public class Radreply implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String attribute;
    private String op = ":=";
    private String value;
    @JsonIgnore
    @OneToMany(mappedBy = "cd_radreply_velocidade", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Internet> internetVelocidade;
    @JsonIgnore
    @OneToMany(mappedBy = "cd_radreply_ip", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Internet> internetIp;
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_internet")
    private Internet internet;

    public Radreply() {
    }

    public Radreply(String attribute) {
        this.attribute = attribute;
    }

    public Radreply(String username, String attribute, String value) {
        this.username = username;
        this.attribute = attribute;
        this.value = value;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Internet> getInternetIp() {
        return internetIp;
    }

    public void setInternetIp(List<Internet> internetIp) {
        this.internetIp = internetIp;
    }

    public List<Internet> getInternetVelocidade() {
        return internetVelocidade;
    }

    public void setInternetVelocidade(List<Internet> internetVelocidade) {
        this.internetVelocidade = internetVelocidade;
    }

    public Internet getInternet() {
        return internet;
    }

    public void setInternet(Internet internet) {
        this.internet = internet;
    }

    public String getDownload() {
        if (value.equals("1200K/6M")) {
            return "5 Mega";
        } else if (value.equals("2200K/11M")) {
            return "10 Mega";
        } else if (value.equals("3200K/16M")) {
            return "15 Mega";
        } else if (value.equals("3600K/26M")) {
            return "25 Mega";
        } else if (value.equals("4200K/36M")) {
            return "35 Mega";
        } else if (value.equals("5200K/51M")) {
            return "50 Mega";
        } else if (value.equals("5200K/101M")) {
            return "100 Mega";
        }
        return "Download";
    }

    public String getUpload() {
        if (value.equals("1200K/6M")) {
            return "1 Mega";
        } else if (value.equals("2200K/11M")) {
            return "2 Mega";
        } else if (value.equals("3200K/16M")) {
            return "3 Mega";
        } else if (value.equals("3600K/26M")) {
            return "3.5 Mega";
        } else if (value.equals("4200K/36M")) {
            return "4 Mega";
        } else if (value.equals("5200K/51M")) {
            return "5 Mega";
        } else if (value.equals("5200K/101M")) {
            return "5 Mega";
        }
        return "Upload";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Radreply other = (Radreply) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
