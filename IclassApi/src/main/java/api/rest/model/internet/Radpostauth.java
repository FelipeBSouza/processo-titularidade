package api.rest.model.internet;
// Generated 11/12/2012 10:35:37 by Hibernate Tools 3.2.1.GA


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@Entity
@Table(schema="internet")
public class Radpostauth  implements java.io.Serializable {

     @Id
     private long id;
     private String username;
     private String pass;
     private String reply;
     private String calledstationid;
     private String callingstationid;
    @Temporal(javax.persistence.TemporalType.DATE)
     private Date authdate;

    public Radpostauth() {
    }

	
    public Radpostauth(long id, String username, Date authdate) {
        this.id = id;
        this.username = username;
        this.authdate = authdate;
    }
    public Radpostauth(long id, String username, String pass, String reply, String calledstationid, String callingstationid, Date authdate) {
       this.id = id;
       this.username = username;
       this.pass = pass;
       this.reply = reply;
       this.calledstationid = calledstationid;
       this.callingstationid = callingstationid;
       this.authdate = authdate;
    }
   
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPass() {
        return this.pass;
    }
    
    public void setPass(String pass) {
        this.pass = pass;
    }
    public String getReply() {
        return this.reply;
    }
    
    public void setReply(String reply) {
        this.reply = reply;
    }
    public String getCalledstationid() {
        return this.calledstationid;
    }
    
    public void setCalledstationid(String calledstationid) {
        this.calledstationid = calledstationid;
    }
    public String getCallingstationid() {
        return this.callingstationid;
    }
    
    public void setCallingstationid(String callingstationid) {
        this.callingstationid = callingstationid;
    }
    public Date getAuthdate() {
        return this.authdate;
    }
    
    public void setAuthdate(Date authdate) {
        this.authdate = authdate;
    }




}


