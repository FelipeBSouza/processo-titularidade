/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.auditoria;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.Internet;
import api.rest.model.seguranca.Usuario;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "auditoria", name = "auditoria_internet")
public class AuditaInternet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @ManyToOne
    @JoinColumn(name = "cd_usuario")
    private Usuario usuario;
    @JoinColumn(name = "cd_cliente")
    @ManyToOne
    private Cliente cliente;
    @ManyToOne
    @JoinColumn(name = "cd_internet")
    private Internet internet;
    @Column(name = "velocidade_antiga")
    private String velocidadeAntiga;
    @Column(name = "area_antiga")
    private String areaAntiga;
    @Column(name = "area_nova")
    private String areaNova;
    @Column(name = "velocidade_nova")
    private String VelocidadeNova;
    @Column(name = "acao", length = 1)
    private String acao;
    @Column(name = "data_atualizacao")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAtualizacao = new Date();

    public AuditaInternet() {
    }

    public AuditaInternet(Usuario usuario, Cliente cliente, Internet internet) {
        this.usuario = usuario;
        this.cliente = cliente;
        this.internet = internet;
    }

    public String getVelocidadeNova() {
        return VelocidadeNova;
    }

    public void setVelocidadeNova(String VelocidadeNova) {
        this.VelocidadeNova = VelocidadeNova;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Internet getInternet() {
        return internet;
    }

    public void setInternet(Internet internet) {
        this.internet = internet;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getVelocidadeAntiga() {
        return velocidadeAntiga;
    }

    public void setVelocidadeAntiga(String velocidadeAntiga) {
        this.velocidadeAntiga = velocidadeAntiga;
    }

    public String getAcao() {
        return acao;
    }

    public String getAreaAntiga() {
        return areaAntiga;
    }

    public void setAreaAntiga(String areaAntiga) {
        this.areaAntiga = areaAntiga;
    }

    public String getAreaNova() {
        return areaNova;
    }

    public void setAreaNova(String areaNova) {
        this.areaNova = areaNova;
    }
    
    public String getAcaoFormat() {
        switch (acao) {
            case "I":
                return "Inclusão";
            case "A":
                return "Alteração";
            default:
                throw new IllegalArgumentException("Unknown" + acao);
        }
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuditaInternet other = (AuditaInternet) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        return hash;
    }
}
