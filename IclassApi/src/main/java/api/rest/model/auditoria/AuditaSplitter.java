/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.auditoria;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import api.rest.model.integrador.equipamento.EquipamentoYate;
import api.rest.model.integrador.fibra.PortaSplitter;
import api.rest.model.seguranca.Usuario;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "auditoria", name = "auditoria_splitter")
public class AuditaSplitter implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @ManyToOne
    @JoinColumn(name = "cd_usuario")
    private Usuario usuario;
    @Column(name = "acao", length = 1)
    private String acao; //I = INCLUSAO R = REMOÇÃO
    @Column(name = "dataatualização")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAtualizacao = new Date();
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cd_equipamento")
    private EquipamentoYate equipamento;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cd_porta")
    private PortaSplitter portaSplitter;

    public AuditaSplitter() {
    }

    public AuditaSplitter(Usuario usuario, String acao, EquipamentoYate equipamento, PortaSplitter portaSplitter) {
        this.usuario = usuario;
        this.acao = acao;
        this.equipamento = equipamento;
        this.portaSplitter = portaSplitter;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public EquipamentoYate getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(EquipamentoYate equipamento) {
        this.equipamento = equipamento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PortaSplitter getPortaSplitter() {
        return portaSplitter;
    }

    public void setPortaSplitter(PortaSplitter portaSplitter) {
        this.portaSplitter = portaSplitter;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuditaSplitter other = (AuditaSplitter) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        return hash;
    }
}
