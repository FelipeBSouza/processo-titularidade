package api.rest.model.telefonia;
// Generated 11/12/2012 10:01:38 by Hibernate Tools 3.2.1.GA

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@Entity
@Table(schema = "telefonia", name = "sippeers")
@SequenceGenerator(name = "SIP_SEQ", sequenceName = "telefonia.sippeers_id_seq", allocationSize = 1)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class Sippeers implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIP_SEQ")
    private long id;
    private String name;
    private String accountcode;
    private String amaflags;
    private String callgroup;
    private String callerid;
    private String canreinvite = "no";
    private String context = "clienteSIP";
    private String defaultip;
    private String dtmfmode;
    private String fromuser;
    private String fromdomain;
    private String host = "dynamic";
    private String insecure;
    private String language = "pt_BR";
    private String mailbox;
    private String md5secret;
    private String nat = "yes";
    private String permit;
    private String deny;
    private String mask;
    private String pickupgroup;
    private String port = "";
    private String qualify = "yes";
    private String restrictcid;
    private String rtptimeout;
    private String rtpholdtimeout;
    private String secret;
    private String type = "friend";
    private String username;
    private String disallow = "all";
    private String allow = "alaw;g729";
    private String musiconhold;
    private long regseconds = 0;
    private String ipaddr = "";
    private String regexten;
    private String cancallforward = "no";
    private String callingpres = "allowed";
    @Column(name = "\"call-limit\"")
    private String callLimit = "1";
    private String useragent;
    private String lastms;
    private String defaultuser;
    private String regserver;
    private String fullcontact;
    private boolean clir = false;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expires;
    private Short inuse = (short) 0;
//    private Numero numero;

//    public void setId(int id) {
//        this.id = id;
//    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmaflags() {
        return this.amaflags;
    }

    public void setAmaflags(String amaflags) {
        this.amaflags = amaflags;
    }

    public String getCallgroup() {
        return this.callgroup;
    }

    public void setCallgroup(String callgroup) {
        this.callgroup = callgroup;
    }

    public String getCallerid() {
        return this.callerid;
    }

    public void setCallerid(String callerid) {
        this.callerid = callerid;
    }

    public String getCanreinvite() {
        return this.canreinvite;
    }

    public void setCanreinvite(String canreinvite) {
        this.canreinvite = canreinvite;
    }

    public String getContext() {
        return this.context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getDefaultip() {
        return this.defaultip;
    }

    public void setDefaultip(String defaultip) {
        this.defaultip = defaultip;
    }

    public String getDtmfmode() {
        return this.dtmfmode;
    }

    public void setDtmfmode(String dtmfmode) {
        this.dtmfmode = dtmfmode;
    }

    public String getFromuser() {
        return this.fromuser;
    }

    public void setFromuser(String fromuser) {
        this.fromuser = fromuser;
    }

    public String getFromdomain() {
        return this.fromdomain;
    }

    public void setFromdomain(String fromdomain) {
        this.fromdomain = fromdomain;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getInsecure() {
        return this.insecure;
    }

    public void setInsecure(String insecure) {
        this.insecure = insecure;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMailbox() {
        return this.mailbox;
    }

    public void setMailbox(String mailbox) {
        this.mailbox = mailbox;
    }

    public String getMd5secret() {
        return this.md5secret;
    }

    public void setMd5secret(String md5secret) {
        this.md5secret = md5secret;
    }

    public String getNat() {
        return this.nat;
    }

    public void setNat(String nat) {
        this.nat = nat;
    }

    public String getPermit() {
        return this.permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getDeny() {
        return this.deny;
    }

    public void setDeny(String deny) {
        this.deny = deny;
    }

    public String getMask() {
        return this.mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getPickupgroup() {
        return this.pickupgroup;
    }

    public void setPickupgroup(String pickupgroup) {
        this.pickupgroup = pickupgroup;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getQualify() {
        return this.qualify;
    }

    public void setQualify(String qualify) {
        this.qualify = qualify;
    }

    public String getRestrictcid() {
        return this.restrictcid;
    }

    public void setRestrictcid(String restrictcid) {
        this.restrictcid = restrictcid;
    }

    public String getRtptimeout() {
        return this.rtptimeout;
    }

    public void setRtptimeout(String rtptimeout) {
        this.rtptimeout = rtptimeout;
    }

    public String getRtpholdtimeout() {
        return this.rtpholdtimeout;
    }

    public void setRtpholdtimeout(String rtpholdtimeout) {
        this.rtpholdtimeout = rtpholdtimeout;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisallow() {
        return this.disallow;
    }

    public void setDisallow(String disallow) {
        this.disallow = disallow;
    }

    public String getAllow() {
        return this.allow;
    }

    public void setAllow(String allow) {
        this.allow = allow;
    }

    public String getMusiconhold() {
        return this.musiconhold;
    }

    public void setMusiconhold(String musiconhold) {
        this.musiconhold = musiconhold;
    }

    public long getRegseconds() {
        return this.regseconds;
    }

    public void setRegseconds(long regseconds) {
        this.regseconds = regseconds;
    }

    public String getIpaddr() {
        return this.ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getRegexten() {
        return this.regexten;
    }

    public void setRegexten(String regexten) {
        this.regexten = regexten;
    }

    public String getCancallforward() {
        return this.cancallforward;
    }

    public void setCancallforward(String cancallforward) {
        this.cancallforward = cancallforward;
    }

    public String getCallingpres() {
        return this.callingpres;
    }

    public void setCallingpres(String callingpres) {
        this.callingpres = callingpres;
    }

    public String getCallLimit() {
        return this.callLimit;
    }

    public void setCallLimit(String callLimit) {
        this.callLimit = callLimit;
    }

    public String getUseragent() {
        return this.useragent;
    }

    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    public String getLastms() {
        return this.lastms;
    }

    public void setLastms(String lastms) {
        this.lastms = lastms;
    }

    public String getDefaultuser() {
        return this.defaultuser;
    }

    public void setDefaultuser(String defaultuser) {
        this.defaultuser = defaultuser;
    }

    public String getRegserver() {
        return this.regserver;
    }

    public void setRegserver(String regserver) {
        this.regserver = regserver;
    }

    public String getFullcontact() {
        return this.fullcontact;
    }

    public void setFullcontact(String fullcontact) {
        this.fullcontact = fullcontact;
    }

    public boolean isClir() {
        return this.clir;
    }

    public void setClir(boolean clir) {
        this.clir = clir;
    }

    public Date getExpires() {
        return this.expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public Short getInuse() {
        return this.inuse;
    }

    public void setInuse(Short inuse) {
        this.inuse = inuse;
    }

    public String getAccountcode() {
        return accountcode;
    }

    public void setAccountcode(String accountcode) {
        this.accountcode = accountcode;
    }

//    public Numero getNumero() {
//        return numero;
//    }
//
//    public void setNumero(Numero numero) {
//        this.numero = numero;
//    }
    @Override
    public String toString() {
        return this.accountcode;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sippeers other = (Sippeers) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
