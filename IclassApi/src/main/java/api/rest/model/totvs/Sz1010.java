/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement 
@Entity
@Table(name = "SZ1010", schema = "dbo")
public class Sz1010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "Z1_FILIAL")
    private String z1Filial;
    
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "Z1_PREFIXO", referencedColumnName = "E1_PREFIXO", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "Z1_NUM", referencedColumnName = "E1_NUM", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "Z1_PARCELA", referencedColumnName = "E1_PARCELA", nullable = false, insertable = false, updatable = false),})
    @JsonBackReference(value="sz1010")
    private Se1010 se1010;  
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "Z1_TIPO")
    private String z1Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "Z1_COD")
    private String z1Cod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Z1_DESC")
    private String z1Desc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Z1_QUANT")
    private double z1Quant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Z1_VALOR")
    private double z1Valor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Z1_TOTAL")
    private double z1Total;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Z1_CATEG")
    private String z1Categ;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;

    public Sz1010() {
    }

    public Sz1010(Integer rECNO) {
        this.rECNO = rECNO;
    }

 
    public String getZ1Filial() {
        return z1Filial;
    }

    public void setZ1Filial(String z1Filial) {
        this.z1Filial = z1Filial;
    }

    public Se1010 getSe1010() {
        return se1010;
    }

    public void setSe1010(Se1010 se1010) {
        this.se1010 = se1010;
    }

  
    public String getZ1Tipo() {
        return z1Tipo;
    }

    public void setZ1Tipo(String z1Tipo) {
        this.z1Tipo = z1Tipo;
    }

    public String getZ1Cod() {
        return z1Cod;
    }

    public void setZ1Cod(String z1Cod) {
        this.z1Cod = z1Cod;
    }

    public String getZ1Desc() {
        return z1Desc;
    }

    public void setZ1Desc(String z1Desc) {
        this.z1Desc = z1Desc;
    }

    public double getZ1Quant() {
        return z1Quant;
    }

    public void setZ1Quant(double z1Quant) {
        this.z1Quant = z1Quant;
    }

    public double getZ1Valor() {
        return z1Valor;
    }

    public void setZ1Valor(double z1Valor) {
        this.z1Valor = z1Valor;
    }

    public double getZ1Total() {
        return z1Total;
    }

    public void setZ1Total(double z1Total) {
        this.z1Total = z1Total;
    }

    public String getZ1Categ() {
        return z1Categ;
    }

    public void setZ1Categ(String z1Categ) {
        this.z1Categ = z1Categ;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sz1010)) {
            return false;
        }
        Sz1010 other = (Sz1010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Sz1010[ rECNO=" + rECNO + " ]";
    }
    
}
