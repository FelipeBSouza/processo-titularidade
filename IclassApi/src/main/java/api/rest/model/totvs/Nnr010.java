package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Robson
 */
@Entity
@Table(name = "NNR010", schema = "dbo")
public class Nnr010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "NNR_FILIAL")
    private String nnrFilial;
    @Basic(optional = false)
    @Column(name = "NNR_CODIGO")
    private String nnrCodigo;
    @Basic(optional = false)
    @Column(name = "NNR_DESCRI")
    private String nnrDescri;
    @Basic(optional = false)
    @Column(name = "NNR_CODCLI")
    private String nnrCodcli;
    @Basic(optional = false)
    @Column(name = "NNR_LOJCLI")
    private String nnrLojcli;
    @Basic(optional = false)
    @Column(name = "NNR_TIPO")
    private String nnrTipo;
    @Basic(optional = false)
    @Column(name = "NNR_CTRAB")
    private String nnrCtrab;
    @Basic(optional = false)
    @Column(name = "NNR_INTP")
    private String nnrIntp;
    @Basic(optional = false)
    @Column(name = "NNR_MRP")
    private String nnrMrp;
    @Basic(optional = false)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @Basic(optional = false)
    @Column(name = "NNR_ANP45")
    private String nnrAnp45;
    @Basic(optional = false)
    @Column(name = "NNR_ARMALT")
    private String nnrArmalt;
    @Basic(optional = false)
    @Column(name = "NNR_VDADMS")
    private String nnrVdadms;
    @Basic(optional = false)
    @Column(name = "NNR_AMZUNI")
    private String nnrAmzuni;

    public Nnr010() {
    }

    public Nnr010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Nnr010(Integer rECNO, String nnrFilial, String nnrCodigo, String nnrDescri, String nnrCodcli, String nnrLojcli, String nnrTipo, String nnrCtrab, String nnrIntp, String nnrMrp, String dELET, int rECDEL, String nnrAnp45, String nnrArmalt, String nnrVdadms, String nnrAmzuni) {
        this.rECNO = rECNO;
        this.nnrFilial = nnrFilial;
        this.nnrCodigo = nnrCodigo;
        this.nnrDescri = nnrDescri;
        this.nnrCodcli = nnrCodcli;
        this.nnrLojcli = nnrLojcli;
        this.nnrTipo = nnrTipo;
        this.nnrCtrab = nnrCtrab;
        this.nnrIntp = nnrIntp;
        this.nnrMrp = nnrMrp;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
        this.nnrAnp45 = nnrAnp45;
        this.nnrArmalt = nnrArmalt;
        this.nnrVdadms = nnrVdadms;
        this.nnrAmzuni = nnrAmzuni;
    }

    public String getNnrFilial() {
        return nnrFilial;
    }

    public void setNnrFilial(String nnrFilial) {
        this.nnrFilial = nnrFilial;
    }

    public String getNnrCodigo() {
        return nnrCodigo;
    }

    public void setNnrCodigo(String nnrCodigo) {
        this.nnrCodigo = nnrCodigo;
    }

    public String getNnrDescri() {
        return nnrDescri;
    }

    public void setNnrDescri(String nnrDescri) {
        this.nnrDescri = nnrDescri;
    }

    public String getNnrCodcli() {
        return nnrCodcli;
    }

    public void setNnrCodcli(String nnrCodcli) {
        this.nnrCodcli = nnrCodcli;
    }

    public String getNnrLojcli() {
        return nnrLojcli;
    }

    public void setNnrLojcli(String nnrLojcli) {
        this.nnrLojcli = nnrLojcli;
    }

    public String getNnrTipo() {
        return nnrTipo;
    }

    public void setNnrTipo(String nnrTipo) {
        this.nnrTipo = nnrTipo;
    }

    public String getNnrCtrab() {
        return nnrCtrab;
    }

    public void setNnrCtrab(String nnrCtrab) {
        this.nnrCtrab = nnrCtrab;
    }

    public String getNnrIntp() {
        return nnrIntp;
    }

    public void setNnrIntp(String nnrIntp) {
        this.nnrIntp = nnrIntp;
    }

    public String getNnrMrp() {
        return nnrMrp;
    }

    public void setNnrMrp(String nnrMrp) {
        this.nnrMrp = nnrMrp;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getNnrAnp45() {
        return nnrAnp45;
    }

    public void setNnrAnp45(String nnrAnp45) {
        this.nnrAnp45 = nnrAnp45;
    }

    public String getNnrArmalt() {
        return nnrArmalt;
    }

    public void setNnrArmalt(String nnrArmalt) {
        this.nnrArmalt = nnrArmalt;
    }

    public String getNnrVdadms() {
        return nnrVdadms;
    }

    public void setNnrVdadms(String nnrVdadms) {
        this.nnrVdadms = nnrVdadms;
    }

    public String getNnrAmzuni() {
        return nnrAmzuni;
    }

    public void setNnrAmzuni(String nnrAmzuni) {
        this.nnrAmzuni = nnrAmzuni;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nnr010)) {
            return false;
        }
        Nnr010 other = (Nnr010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gerador.Nnr010[ rECNO=" + rECNO + " ]";
    }
    
}
