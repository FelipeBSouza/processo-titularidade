/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "ABK010", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Abk010.findAll", query = "SELECT a FROM Abk010 a"),
    @NamedQuery(name = "Abk010.findByAbkFilial", query = "SELECT a FROM Abk010 a WHERE a.abkFilial = :abkFilial"),
    @NamedQuery(name = "Abk010.findByAbkNrcham", query = "SELECT a FROM Abk010 a WHERE a.abkNrcham = :abkNrcham"),
    @NamedQuery(name = "Abk010.findByAbkSeq", query = "SELECT a FROM Abk010 a WHERE a.abkSeq = :abkSeq"),
    @NamedQuery(name = "Abk010.findByAbkCodcli", query = "SELECT a FROM Abk010 a WHERE a.abkCodcli = :abkCodcli"),
    @NamedQuery(name = "Abk010.findByAbkLoja", query = "SELECT a FROM Abk010 a WHERE a.abkLoja = :abkLoja"),
    @NamedQuery(name = "Abk010.findByAbkCodtec", query = "SELECT a FROM Abk010 a WHERE a.abkCodtec = :abkCodtec"),
    @NamedQuery(name = "Abk010.findByAbkOrigem", query = "SELECT a FROM Abk010 a WHERE a.abkOrigem = :abkOrigem"),
    @NamedQuery(name = "Abk010.findByAbkTempo", query = "SELECT a FROM Abk010 a WHERE a.abkTempo = :abkTempo"),
    @NamedQuery(name = "Abk010.findByAbkCodprb", query = "SELECT a FROM Abk010 a WHERE a.abkCodprb = :abkCodprb"),
    @NamedQuery(name = "Abk010.findByAbkSituac", query = "SELECT a FROM Abk010 a WHERE a.abkSituac = :abkSituac"),
    @NamedQuery(name = "Abk010.findByAbkCodmem", query = "SELECT a FROM Abk010 a WHERE a.abkCodmem = :abkCodmem"),
    @NamedQuery(name = "Abk010.findByDELET", query = "SELECT a FROM Abk010 a WHERE a.dELET = :dELET"),
    @NamedQuery(name = "Abk010.findByRECNO", query = "SELECT a FROM Abk010 a WHERE a.rECNO = :rECNO")})
public class Abk010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "ABK_FILIAL")
    private String abkFilial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ABK_NRCHAM")
    private String abkNrcham;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ABK_SEQ")
    private String abkSeq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "ABK_CODCLI")
    private String abkCodcli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ABK_LOJA")
    private String abkLoja;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ABK_CODTEC")
    private String abkCodtec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ABK_ORIGEM")
    private String abkOrigem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "ABK_TEMPO")
    private String abkTempo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ABK_CODPRB")
    private String abkCodprb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ABK_SITUAC")
    private String abkSituac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ABK_CODMEM")
    private String abkCodmem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;

    public Abk010() {
    }

    public Abk010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Abk010(Integer rECNO, String abkFilial, String abkNrcham, String abkSeq, String abkCodcli, String abkLoja, String abkCodtec, String abkOrigem, String abkTempo, String abkCodprb, String abkSituac, String abkCodmem, String dELET) {
        this.rECNO = rECNO;
        this.abkFilial = abkFilial;
        this.abkNrcham = abkNrcham;
        this.abkSeq = abkSeq;
        this.abkCodcli = abkCodcli;
        this.abkLoja = abkLoja;
        this.abkCodtec = abkCodtec;
        this.abkOrigem = abkOrigem;
        this.abkTempo = abkTempo;
        this.abkCodprb = abkCodprb;
        this.abkSituac = abkSituac;
        this.abkCodmem = abkCodmem;
        this.dELET = dELET;
    }

    public String getAbkFilial() {
        return abkFilial;
    }

    public void setAbkFilial(String abkFilial) {
        this.abkFilial = abkFilial;
    }

    public String getAbkNrcham() {
        return abkNrcham;
    }

    public void setAbkNrcham(String abkNrcham) {
        this.abkNrcham = abkNrcham;
    }

    public String getAbkSeq() {
        return abkSeq;
    }

    public void setAbkSeq(String abkSeq) {
        this.abkSeq = abkSeq;
    }

    public String getAbkCodcli() {
        return abkCodcli;
    }

    public void setAbkCodcli(String abkCodcli) {
        this.abkCodcli = abkCodcli;
    }

    public String getAbkLoja() {
        return abkLoja;
    }

    public void setAbkLoja(String abkLoja) {
        this.abkLoja = abkLoja;
    }

    public String getAbkCodtec() {
        return abkCodtec;
    }

    public void setAbkCodtec(String abkCodtec) {
        this.abkCodtec = abkCodtec;
    }

    public String getAbkOrigem() {
        return abkOrigem;
    }

    public void setAbkOrigem(String abkOrigem) {
        this.abkOrigem = abkOrigem;
    }

    public String getAbkTempo() {
        return abkTempo;
    }

    public void setAbkTempo(String abkTempo) {
        this.abkTempo = abkTempo;
    }

    public String getAbkCodprb() {
        return abkCodprb;
    }

    public void setAbkCodprb(String abkCodprb) {
        this.abkCodprb = abkCodprb;
    }

    public String getAbkSituac() {
        return abkSituac;
    }

    public void setAbkSituac(String abkSituac) {
        this.abkSituac = abkSituac;
    }

    public String getAbkCodmem() {
        return abkCodmem;
    }

    public void setAbkCodmem(String abkCodmem) {
        this.abkCodmem = abkCodmem;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Abk010)) {
            return false;
        }
        Abk010 other = (Abk010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Abk010[ rECNO=" + rECNO + " ]";
    }
    
}
