/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Robson PC
 */
@Entity
@Table(name = "AGB010", schema = "dbo")
public class Agb010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AGB_FILIAL")
    private String agbFilial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AGB_CODIGO")
    private String agbCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AGB_ENTIDA")
    private String agbEntida;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AGB_CODENT")
    private String agbCodent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGB_TIPO")
    private String agbTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGB_PADRAO")
    private String agbPadrao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AGB_DDI")
    private String agbDdi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AGB_DDD")
    private String agbDdd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AGB_TELEFO")
    private String agbTelefo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "AGB_COMP")
    private String agbComp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AGB_UCALLC")
    private String agbUcallc;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AGB_UNUMCT", referencedColumnName = "ADA_NUMCTR", nullable = false, insertable = false, updatable = false)
    private Ada010 ada010;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGB_UTIPO2")
    private String agbUtipo2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "AGB_UTITUL")
    private String agbUtitul;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 21)
    @Column(name = "AGB_UCGC")
    private String agbUcgc;

    public Agb010() {
    }

    public Agb010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAgbFilial() {
        return agbFilial;
    }

    public void setAgbFilial(String agbFilial) {
        this.agbFilial = agbFilial;
    }

    public String getAgbCodigo() {
        return agbCodigo;
    }

    public void setAgbCodigo(String agbCodigo) {
        this.agbCodigo = agbCodigo;
    }

    public String getAgbEntida() {
        return agbEntida;
    }

    public void setAgbEntida(String agbEntida) {
        this.agbEntida = agbEntida;
    }

    public String getAgbCodent() {
        return agbCodent;
    }

    public void setAgbCodent(String agbCodent) {
        this.agbCodent = agbCodent;
    }

    public String getAgbTipo() {
        return agbTipo;
    }

    public void setAgbTipo(String agbTipo) {
        this.agbTipo = agbTipo;
    }

    public String getAgbPadrao() {
        return agbPadrao;
    }

    public void setAgbPadrao(String agbPadrao) {
        this.agbPadrao = agbPadrao;
    }

    public String getAgbDdi() {
        return agbDdi;
    }

    public void setAgbDdi(String agbDdi) {
        this.agbDdi = agbDdi;
    }

    public String getAgbDdd() {
        return agbDdd;
    }

    public void setAgbDdd(String agbDdd) {
        this.agbDdd = agbDdd;
    }

    public String getAgbTelefo() {
        return agbTelefo;
    }

    public void setAgbTelefo(String agbTelefo) {
        this.agbTelefo = agbTelefo;
    }

    public String getAgbComp() {
        return agbComp;
    }

    public void setAgbComp(String agbComp) {
        this.agbComp = agbComp;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAgbUcallc() {
        return agbUcallc;
    }

    public void setAgbUcallc(String agbUcallc) {
        this.agbUcallc = agbUcallc;
    }

    public Ada010 getAda010() {
        return ada010;
    }

    public void setAda010(Ada010 ada010) {
        this.ada010 = ada010;
    }

    public String getAgbUtipo2() {
        return agbUtipo2;
    }

    public void setAgbUtipo2(String agbUtipo2) {
        this.agbUtipo2 = agbUtipo2;
    }

    public String getAgbUtitul() {
        return agbUtitul;
    }

    public void setAgbUtitul(String agbUtitul) {
        this.agbUtitul = agbUtitul;
    }

    public String getAgbUcgc() {
        return agbUcgc;
    }

    public void setAgbUcgc(String agbUcgc) {
        this.agbUcgc = agbUcgc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agb010)) {
            return false;
        }
        Agb010 other = (Agb010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.liguetelecom.model.totvs.Agb010[ rECNO=" + rECNO + " ]";
    }
}
