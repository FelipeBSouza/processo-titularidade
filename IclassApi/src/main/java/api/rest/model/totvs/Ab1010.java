/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(name = "AB1010", schema = "dbo")
public class Ab1010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AB1_FILIAL")
    private String ab1Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB1_NRCHAM")
    private String ab1Nrcham;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB1_EMISSA")
    private String ab1Emissa;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "AB1_CODCLI", referencedColumnName = "A1_COD", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "AB1_LOJA", referencedColumnName = "A1_LOJA", nullable = false, insertable = false, updatable = false),})
    private Sa1010 sa1010;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "AB1_HORA")
    private String ab1Hora;
    @Basic(optional = false)
    @NotNull  
    @Size(min = 1, max = 5)
    @Column(name = "AB1_HORAF")
    private String ab1Horaf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "AB1_CONTAT")
    private String ab1Contat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "AB1_TEL")
    private String ab1Tel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AB1_ATEND")
    private String ab1Atend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AB1_STATUS")
    private String ab1Status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AB1_OK")
    private String ab1Ok;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AB1_REGIAO")
    private String ab1Regiao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB1_NUMTMK")
    private String ab1Numtmk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB1_UATEND")
    private String ab1Uatend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @OneToMany(mappedBy = "ab1", fetch = FetchType.LAZY)
    private List<Ab2010> ab2010s = new ArrayList<Ab2010>();

    public Ab1010() {
    }

    public Ab1010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAb1Filial() {
        return ab1Filial;
    }

    public void setAb1Filial(String ab1Filial) {
        this.ab1Filial = ab1Filial;
    }

    public String getAb1Nrcham() {
        return ab1Nrcham;
    }

    public void setAb1Nrcham(String ab1Nrcham) {
        this.ab1Nrcham = ab1Nrcham;
    }

    public String getAb1Emissa() {
        return ab1Emissa;
    }

    public void setAb1Emissa(String ab1Emissa) {
        this.ab1Emissa = ab1Emissa;
    }

    public List<Ab2010> getAb2010s() {
        return ab2010s;
    }

    public void setAb2010s(List<Ab2010> ab2010s) {
        this.ab2010s = ab2010s;
    }

    public Sa1010 getSa1010() {
        return sa1010;
    }

    public void setSa1010(Sa1010 sa1010) {
        this.sa1010 = sa1010;
    }

    public String getAb1Hora() {
        return ab1Hora;
    }

    public void setAb1Hora(String ab1Hora) {
        this.ab1Hora = ab1Hora;
    }

    public String getAb1Horaf() {
        return ab1Horaf;
    }

    public void setAb1Horaf(String ab1Horaf) {
        this.ab1Horaf = ab1Horaf;
    }

    public String getAb1Contat() {
        return ab1Contat;
    }

    public void setAb1Contat(String ab1Contat) {
        this.ab1Contat = ab1Contat;
    }

    public String getAb1Tel() {
        return ab1Tel;
    }

    public void setAb1Tel(String ab1Tel) {
        this.ab1Tel = ab1Tel;
    }

    public String getAb1Atend() {
        return ab1Atend;
    }

    public void setAb1Atend(String ab1Atend) {
        this.ab1Atend = ab1Atend;
    }

    public String getAb1Status() {
        return ab1Status;
    }

    public void setAb1Status(String ab1Status) {
        this.ab1Status = ab1Status;
    }

    public String getAb1Ok() {
        return ab1Ok;
    }

    public void setAb1Ok(String ab1Ok) {
        this.ab1Ok = ab1Ok;
    }

    public String getAb1Regiao() {
        return ab1Regiao;
    }

    public void setAb1Regiao(String ab1Regiao) {
        this.ab1Regiao = ab1Regiao;
    }

    public String getAb1Numtmk() {
        return ab1Numtmk;
    }

    public void setAb1Numtmk(String ab1Numtmk) {
        this.ab1Numtmk = ab1Numtmk;
    }

    public String getAb1Uatend() {
        return ab1Uatend;
    }

    public void setAb1Uatend(String ab1Uatend) {
        this.ab1Uatend = ab1Uatend;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ab1010)) {
            return false;
        }
        Ab1010 other = (Ab1010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Ab1010[ rECNO=" + rECNO + " ]";
    }
}
