/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ROBSON
 */

@Entity(name = "Ab7010")
@Table(name = "AB7010", schema = "dbo")
public class Ab7010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AB7_NUMOS", referencedColumnName = "AB6_NUMOS")
    private Ab6010 ab6;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AB7_ITEM")
    private String ab7Item;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AB7_TIPO")
    private String ab7Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AB7_CODPRO")
    private String ab7Codpro;
   
    
    @OneToOne(targetEntity = Aa3010.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "AB7_NUMSER", referencedColumnName = "AA3_NUMSER")
    private Aa3010 aa3;
   
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB7_CODPRB")
    private String ab7Codprb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AB7_NRCHAM")
    private String ab7Nrcham;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB7_NUMORC")
    private String ab7Numorc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB7_MEMO1")
    private String ab7Memo1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB7_MEMO3")
    private String ab7Memo3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AB7_CODFAB")
    private String ab7Codfab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AB7_LOJAFA")
    private String ab7Lojafa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AB7_CODCLI")
    private String ab7Codcli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AB7_LOJA")
    private String ab7Loja;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AB7_EMISSA")
    private String ab7Emissa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AB7_NUMHDE")
    private String ab7Numhde;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AB7_FILIAL")
    private String ab7Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB7_CODCON")
    private String ab7Codcon;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AB7_TMKLST")
    private String ab7Tmklst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "AB7_USERGI")
    private String ab7Usergi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "AB7_USERGA")
    private String ab7Userga;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @OneToMany(mappedBy = "ab7", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Sz2010> sz2010s;
    
    @Basic(optional = false)
    @Size(min = 0, max = 1)
    @Column(name = "AB7_ULIBIC")
    private String ab7Ulibic;
    
    @Basic(optional = false)
    @Size(min = 0, max = 1)
    @Column(name = "AB7_UICLAS")
    private String ab7Uiclass;

    public Ab7010() {
    	
    }       

    public String getAb7Ulibic() {
		return ab7Ulibic;
	}

	public void setAb7Ulibic(String ab7Ulibic) {
		this.ab7Ulibic = ab7Ulibic;
	}

	public String getAb7Uiclass() {
		return ab7Uiclass;
	}

	public void setAb7Uiclass(String ab7Uiclass) {
		this.ab7Uiclass = ab7Uiclass;
	}

	public Ab7010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAb7Item() {
        return ab7Item;
    }

    public void setAb7Item(String ab7Item) {
        this.ab7Item = ab7Item;
    }

    public String getAb7Tipo() {
        return ab7Tipo;
    }

    public void setAb7Tipo(String ab7Tipo) {
        this.ab7Tipo = ab7Tipo;
    }

    public String getAb7Codpro() {
        return ab7Codpro;
    }

    public void setAb7Codpro(String ab7Codpro) {
        this.ab7Codpro = ab7Codpro;
    }

    public Aa3010 getAa3() {
        return aa3;
    }

    public void setAa3(Aa3010 aa3) {
        this.aa3 = aa3;
    }

    public String getAb7Codprb() {
        return ab7Codprb;
    }

    public void setAb7Codprb(String ab7Codprb) {
        this.ab7Codprb = ab7Codprb;
    }

    public String getAb7Nrcham() {
        return ab7Nrcham;
    }

    public void setAb7Nrcham(String ab7Nrcham) {
        this.ab7Nrcham = ab7Nrcham;
    }

    public String getAb7Numorc() {
        return ab7Numorc;
    }

    public void setAb7Numorc(String ab7Numorc) {
        this.ab7Numorc = ab7Numorc;
    }

    public String getAb7Memo1() {
        return ab7Memo1;
    }

    public void setAb7Memo1(String ab7Memo1) {
        this.ab7Memo1 = ab7Memo1;
    }

    public String getAb7Memo3() {
        return ab7Memo3;
    }

    public void setAb7Memo3(String ab7Memo3) {
        this.ab7Memo3 = ab7Memo3;
    }

    public String getAb7Codfab() {
        return ab7Codfab;
    }

    public void setAb7Codfab(String ab7Codfab) {
        this.ab7Codfab = ab7Codfab;
    }

    public String getAb7Lojafa() {
        return ab7Lojafa;
    }

    public void setAb7Lojafa(String ab7Lojafa) {
        this.ab7Lojafa = ab7Lojafa;
    }

    public String getAb7Codcli() {
        return ab7Codcli;
    }

    public void setAb7Codcli(String ab7Codcli) {
        this.ab7Codcli = ab7Codcli;
    }

    public String getAb7Loja() {
        return ab7Loja;
    }

    public void setAb7Loja(String ab7Loja) {
        this.ab7Loja = ab7Loja;
    }

    public String getAb7Emissa() {
        return ab7Emissa;
    }

    public void setAb7Emissa(String ab7Emissa) {
        this.ab7Emissa = ab7Emissa;
    }

    public String getAb7Numhde() {
        return ab7Numhde;
    }

    public void setAb7Numhde(String ab7Numhde) {
        this.ab7Numhde = ab7Numhde;
    }

    public String getAb7Filial() {
        return ab7Filial;
    }

    public void setAb7Filial(String ab7Filial) {
        this.ab7Filial = ab7Filial;
    }

    public String getAb7Codcon() {
        return ab7Codcon;
    }

    public void setAb7Codcon(String ab7Codcon) {
        this.ab7Codcon = ab7Codcon;
    }

    public String getAb7Tmklst() {
        return ab7Tmklst;
    }

    public void setAb7Tmklst(String ab7Tmklst) {
        this.ab7Tmklst = ab7Tmklst;
    }

    public String getAb7Usergi() {
        return ab7Usergi;
    }

    public void setAb7Usergi(String ab7Usergi) {
        this.ab7Usergi = ab7Usergi;
    }

    public String getAb7Userga() {
        return ab7Userga;
    }

    public void setAb7Userga(String ab7Userga) {
        this.ab7Userga = ab7Userga;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public Ab6010 getAb6() {
        return ab6;
    }

    public void setAb6(Ab6010 ab6) {
        this.ab6 = ab6;
    }

    public String getdELET() {
        return dELET;
    }

    public void setdELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getrECNO() {
        return rECNO;
    }

    public void setrECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getrECDEL() {
        return rECDEL;
    }

    public void setrECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public List<Sz2010> getSz2010s() {
        return sz2010s;
    }

    public void setSz2010s(List<Sz2010> sz2010s) {
        this.sz2010s = sz2010s;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ab7010)) {
            return false;
        }
        Ab7010 other = (Ab7010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Ab7010[ rECNO=" + rECNO + " ]";
    }
}
