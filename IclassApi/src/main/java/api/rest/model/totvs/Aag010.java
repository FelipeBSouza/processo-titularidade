/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON OCORRENCIA
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(name = "AAG010", schema = "dbo")
public class Aag010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AAG_FILIAL")
    private String aagFilial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AAG_CODPRB")
    private String aagCodprb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "AAG_DESCRI")
    private String aagDescri;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AAG_TIPPRB")
    private String aagTipprb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AAG_PRIORI")
    private String aagPriori;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AAG_COR")
    private String aagCor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AAG_GERFNC")
    private String aagGerfnc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @OneToMany(mappedBy = "aag", fetch = FetchType.LAZY)
    private List<Ab2010> ab2010s;

    public Aag010() {
    }

    public Aag010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Aag010(Integer rECNO, String aagFilial, String aagCodprb, String aagDescri, String aagTipprb, String aagPriori, String aagCor, String aagGerfnc, String dELET, int rECDEL) {
        this.rECNO = rECNO;
        this.aagFilial = aagFilial;
        this.aagCodprb = aagCodprb;
        this.aagDescri = aagDescri;
        this.aagTipprb = aagTipprb;
        this.aagPriori = aagPriori;
        this.aagCor = aagCor;
        this.aagGerfnc = aagGerfnc;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
    }

    public String getAagFilial() {
        return aagFilial;
    }

    public void setAagFilial(String aagFilial) {
        this.aagFilial = aagFilial;
    }

    public String getAagCodprb() {
        return aagCodprb;
    }

    public void setAagCodprb(String aagCodprb) {
        this.aagCodprb = aagCodprb;
    }

    public String getAagDescri() {
        return aagDescri;
    }

    public void setAagDescri(String aagDescri) {
        this.aagDescri = aagDescri;
    }

    public String getAagTipprb() {
        return aagTipprb;
    }

    public void setAagTipprb(String aagTipprb) {
        this.aagTipprb = aagTipprb;
    }

    public String getAagPriori() {
        return aagPriori;
    }

    public void setAagPriori(String aagPriori) {
        this.aagPriori = aagPriori;
    }

    public String getAagCor() {
        return aagCor;
    }

    public void setAagCor(String aagCor) {
        this.aagCor = aagCor;
    }

    public String getAagGerfnc() {
        return aagGerfnc;
    }

    public void setAagGerfnc(String aagGerfnc) {
        this.aagGerfnc = aagGerfnc;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aag010)) {
            return false;
        }
        Aag010 other = (Aag010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Aag010[ rECNO=" + rECNO + " ]";
    }
}
