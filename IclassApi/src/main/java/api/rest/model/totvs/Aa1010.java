/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON ATENDENTES
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(name = "AA1010", schema = "dbo")
public class Aa1010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AA1_FILIAL")
    private String aa1Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA1_CODTEC")
    private String aa1Codtec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA1_NOMTEC")
    private String aa1Nomtec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "AA1_FUNCAO")
    private String aa1Funcao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA1_CDFUNC")
    private String aa1Cdfunc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AA1_CC")
    private String aa1Cc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA1_CENTRA")
    private String aa1Centra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AA1_PAGER")
    private String aa1Pager;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA1_EMAIL")
    private String aa1Email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA1_FONE")
    private String aa1Fone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AA1_REGIAO")
    private String aa1Regiao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AA1_TURNO")
    private String aa1Turno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA1_VALOR")
    private double aa1Valor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA1_RATE")
    private double aa1Rate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_EMINFI")
    private String aa1Eminfi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA1_CUSTO")
    private double aa1Custo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_FUNPRO")
    private String aa1Funpro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AA1_LOCAL")
    private String aa1Local;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA1_LOCLZB")
    private String aa1Loclzb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_LIBOSV")
    private String aa1Libosv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA1_LOCLZR")
    private String aa1Loclzr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA1_LOCLZF")
    private String aa1Loclzf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA1_DULTES")
    private String aa1Dultes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA1_CULTES")
    private String aa1Cultes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA1_DATUES")
    private String aa1Datues;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA1_CATUES")
    private String aa1Catues;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_REQPEC")
    private String aa1Reqpec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA1_DATADM")
    private String aa1Datadm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA1_DATDEM")
    private String aa1Datdem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA1_CODVEN")
    private String aa1Codven;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AA1_NOMUSU")
    private String aa1Nomusu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_TIPO")
    private String aa1Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA1_CODUSR")
    private String aa1Codusr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_CONTRB")
    private String aa1Contrb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_TIPVEN")
    private String aa1Tipven;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_ALOCA")
    private String aa1Aloca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AA1_CODFOR")
    private String aa1Codfor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AA1_LOJFOR")
    private String aa1Lojfor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AA1_ACESSO")
    private String aa1Acesso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_VISTOR")
    private String aa1Vistor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_VISVLR")
    private String aa1Visvlr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_VISPRO")
    private String aa1Vispro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_IMPPRO")
    private String aa1Imppro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_CATEGO")
    private String aa1Catego;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_ALTVIS")
    private String aa1Altvis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_FTVIST")
    private String aa1Ftvist;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_CRMSIM")
    private String aa1Crmsim;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AA1_FUNFIL")
    private String aa1Funfil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AA1_MPONTO")
    private String aa1Mponto;
    @Column(name = "AA1_ULOGIN")
    private String aa1Ulogin;
    
    @Column(name = "AA1_UCREDE")
    private String aa1Ucrede;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;

    public Aa1010() {
    }

    public Aa1010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Aa1010(Integer rECNO, String aa1Filial, String aa1Codtec, String aa1Nomtec, String aa1Funcao, String aa1Cdfunc, String aa1Cc, String aa1Centra, String aa1Pager, String aa1Email, String aa1Fone, String aa1Regiao, String aa1Turno, double aa1Valor, double aa1Rate, String aa1Eminfi, double aa1Custo, String aa1Funpro, String aa1Local, String aa1Loclzb, String aa1Libosv, String aa1Loclzr, String aa1Loclzf, String aa1Dultes, String aa1Cultes, String aa1Datues, String aa1Catues, String aa1Reqpec, String aa1Datadm, String aa1Datdem, String aa1Codven, String aa1Nomusu, String aa1Tipo, String aa1Codusr, String aa1Contrb, String aa1Tipven, String aa1Aloca, String aa1Codfor, String aa1Lojfor, String aa1Acesso, String aa1Vistor, String aa1Visvlr, String aa1Vispro, String aa1Imppro, String aa1Catego, String aa1Altvis, String aa1Ftvist, String aa1Crmsim, String aa1Funfil, String aa1Mponto, String dELET, int rECDEL) {
        this.rECNO = rECNO;
        this.aa1Filial = aa1Filial;
        this.aa1Codtec = aa1Codtec;
        this.aa1Nomtec = aa1Nomtec;
        this.aa1Funcao = aa1Funcao;
        this.aa1Cdfunc = aa1Cdfunc;
        this.aa1Cc = aa1Cc;
        this.aa1Centra = aa1Centra;
        this.aa1Pager = aa1Pager;
        this.aa1Email = aa1Email;
        this.aa1Fone = aa1Fone;
        this.aa1Regiao = aa1Regiao;
        this.aa1Turno = aa1Turno;
        this.aa1Valor = aa1Valor;
        this.aa1Rate = aa1Rate;
        this.aa1Eminfi = aa1Eminfi;
        this.aa1Custo = aa1Custo;
        this.aa1Funpro = aa1Funpro;
        this.aa1Local = aa1Local;
        this.aa1Loclzb = aa1Loclzb;
        this.aa1Libosv = aa1Libosv;
        this.aa1Loclzr = aa1Loclzr;
        this.aa1Loclzf = aa1Loclzf;
        this.aa1Dultes = aa1Dultes;
        this.aa1Cultes = aa1Cultes;
        this.aa1Datues = aa1Datues;
        this.aa1Catues = aa1Catues;
        this.aa1Reqpec = aa1Reqpec;
        this.aa1Datadm = aa1Datadm;
        this.aa1Datdem = aa1Datdem;
        this.aa1Codven = aa1Codven;
        this.aa1Nomusu = aa1Nomusu;
        this.aa1Tipo = aa1Tipo;
        this.aa1Codusr = aa1Codusr;
        this.aa1Contrb = aa1Contrb;
        this.aa1Tipven = aa1Tipven;
        this.aa1Aloca = aa1Aloca;
        this.aa1Codfor = aa1Codfor;
        this.aa1Lojfor = aa1Lojfor;
        this.aa1Acesso = aa1Acesso;
        this.aa1Vistor = aa1Vistor;
        this.aa1Visvlr = aa1Visvlr;
        this.aa1Vispro = aa1Vispro;
        this.aa1Imppro = aa1Imppro;
        this.aa1Catego = aa1Catego;
        this.aa1Altvis = aa1Altvis;
        this.aa1Ftvist = aa1Ftvist;
        this.aa1Crmsim = aa1Crmsim;
        this.aa1Funfil = aa1Funfil;
        this.aa1Mponto = aa1Mponto;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
    }

    public String getAa1Filial() {
        return aa1Filial;
    }

    public void setAa1Filial(String aa1Filial) {
        this.aa1Filial = aa1Filial;
    }

    public String getAa1Codtec() {
        return aa1Codtec;
    }

    public void setAa1Codtec(String aa1Codtec) {
        this.aa1Codtec = aa1Codtec;
    }

    public String getAa1Nomtec() {
        return aa1Nomtec;
    }

    public void setAa1Nomtec(String aa1Nomtec) {
        this.aa1Nomtec = aa1Nomtec;
    }

    public String getAa1Funcao() {
        return aa1Funcao;
    }

    public void setAa1Funcao(String aa1Funcao) {
        this.aa1Funcao = aa1Funcao;
    }

    public String getAa1Cdfunc() {
        return aa1Cdfunc;
    }

    public void setAa1Cdfunc(String aa1Cdfunc) {
        this.aa1Cdfunc = aa1Cdfunc;
    }

    public String getAa1Cc() {
        return aa1Cc;
    }

    public void setAa1Cc(String aa1Cc) {
        this.aa1Cc = aa1Cc;
    }

    public String getAa1Centra() {
        return aa1Centra;
    }

    public void setAa1Centra(String aa1Centra) {
        this.aa1Centra = aa1Centra;
    }

    public String getAa1Pager() {
        return aa1Pager;
    }

    public void setAa1Pager(String aa1Pager) {
        this.aa1Pager = aa1Pager;
    }

    public String getAa1Email() {
        return aa1Email;
    }

    public void setAa1Email(String aa1Email) {
        this.aa1Email = aa1Email;
    }

    public String getAa1Fone() {
        return aa1Fone;
    }

    public void setAa1Fone(String aa1Fone) {
        this.aa1Fone = aa1Fone;
    }

    public String getAa1Regiao() {
        return aa1Regiao;
    }

    public void setAa1Regiao(String aa1Regiao) {
        this.aa1Regiao = aa1Regiao;
    }

    public String getAa1Turno() {
        return aa1Turno;
    }

    public void setAa1Turno(String aa1Turno) {
        this.aa1Turno = aa1Turno;
    }

    public double getAa1Valor() {
        return aa1Valor;
    }

    public void setAa1Valor(double aa1Valor) {
        this.aa1Valor = aa1Valor;
    }

    public double getAa1Rate() {
        return aa1Rate;
    }

    public void setAa1Rate(double aa1Rate) {
        this.aa1Rate = aa1Rate;
    }

    public String getAa1Eminfi() {
        return aa1Eminfi;
    }

    public void setAa1Eminfi(String aa1Eminfi) {
        this.aa1Eminfi = aa1Eminfi;
    }

    public double getAa1Custo() {
        return aa1Custo;
    }

    public void setAa1Custo(double aa1Custo) {
        this.aa1Custo = aa1Custo;
    }

    public String getAa1Funpro() {
        return aa1Funpro;
    }

    public void setAa1Funpro(String aa1Funpro) {
        this.aa1Funpro = aa1Funpro;
    }

    public String getAa1Local() {
        return aa1Local;
    }

    public void setAa1Local(String aa1Local) {
        this.aa1Local = aa1Local;
    }

    public String getAa1Loclzb() {
        return aa1Loclzb;
    }

    public void setAa1Loclzb(String aa1Loclzb) {
        this.aa1Loclzb = aa1Loclzb;
    }

    public String getAa1Libosv() {
        return aa1Libosv;
    }

    public void setAa1Libosv(String aa1Libosv) {
        this.aa1Libosv = aa1Libosv;
    }

    public String getAa1Loclzr() {
        return aa1Loclzr;
    }

    public void setAa1Loclzr(String aa1Loclzr) {
        this.aa1Loclzr = aa1Loclzr;
    }

    public String getAa1Loclzf() {
        return aa1Loclzf;
    }

    public void setAa1Loclzf(String aa1Loclzf) {
        this.aa1Loclzf = aa1Loclzf;
    }

    public String getAa1Dultes() {
        return aa1Dultes;
    }

    public void setAa1Dultes(String aa1Dultes) {
        this.aa1Dultes = aa1Dultes;
    }

    public String getAa1Cultes() {
        return aa1Cultes;
    }

    public void setAa1Cultes(String aa1Cultes) {
        this.aa1Cultes = aa1Cultes;
    }

    public String getAa1Datues() {
        return aa1Datues;
    }

    public void setAa1Datues(String aa1Datues) {
        this.aa1Datues = aa1Datues;
    }

    public String getAa1Catues() {
        return aa1Catues;
    }

    public void setAa1Catues(String aa1Catues) {
        this.aa1Catues = aa1Catues;
    }

    public String getAa1Reqpec() {
        return aa1Reqpec;
    }

    public void setAa1Reqpec(String aa1Reqpec) {
        this.aa1Reqpec = aa1Reqpec;
    }

    public String getAa1Datadm() {
        return aa1Datadm;
    }

    public void setAa1Datadm(String aa1Datadm) {
        this.aa1Datadm = aa1Datadm;
    }

    public String getAa1Datdem() {
        return aa1Datdem;
    }

    public void setAa1Datdem(String aa1Datdem) {
        this.aa1Datdem = aa1Datdem;
    }

    public String getAa1Codven() {
        return aa1Codven;
    }

    public void setAa1Codven(String aa1Codven) {
        this.aa1Codven = aa1Codven;
    }

    public String getAa1Nomusu() {
        return aa1Nomusu;
    }

    public void setAa1Nomusu(String aa1Nomusu) {
        this.aa1Nomusu = aa1Nomusu;
    }

    public String getAa1Tipo() {
        return aa1Tipo;
    }

    public void setAa1Tipo(String aa1Tipo) {
        this.aa1Tipo = aa1Tipo;
    }

    public String getAa1Codusr() {
        return aa1Codusr;
    }

    public void setAa1Codusr(String aa1Codusr) {
        this.aa1Codusr = aa1Codusr;
    }

    public String getAa1Contrb() {
        return aa1Contrb;
    }

    public void setAa1Contrb(String aa1Contrb) {
        this.aa1Contrb = aa1Contrb;
    }

    public String getAa1Tipven() {
        return aa1Tipven;
    }

    public void setAa1Tipven(String aa1Tipven) {
        this.aa1Tipven = aa1Tipven;
    }

    public String getAa1Aloca() {
        return aa1Aloca;
    }

    public void setAa1Aloca(String aa1Aloca) {
        this.aa1Aloca = aa1Aloca;
    }

    public String getAa1Codfor() {
        return aa1Codfor;
    }

    public void setAa1Codfor(String aa1Codfor) {
        this.aa1Codfor = aa1Codfor;
    }

    public String getAa1Lojfor() {
        return aa1Lojfor;
    }

    public void setAa1Lojfor(String aa1Lojfor) {
        this.aa1Lojfor = aa1Lojfor;
    }

    public String getAa1Acesso() {
        return aa1Acesso;
    }

    public void setAa1Acesso(String aa1Acesso) {
        this.aa1Acesso = aa1Acesso;
    }

    public String getAa1Vistor() {
        return aa1Vistor;
    }

    public void setAa1Vistor(String aa1Vistor) {
        this.aa1Vistor = aa1Vistor;
    }

    public String getAa1Visvlr() {
        return aa1Visvlr;
    }

    public void setAa1Visvlr(String aa1Visvlr) {
        this.aa1Visvlr = aa1Visvlr;
    }

    public String getAa1Vispro() {
        return aa1Vispro;
    }

    public void setAa1Vispro(String aa1Vispro) {
        this.aa1Vispro = aa1Vispro;
    }

    public String getAa1Imppro() {
        return aa1Imppro;
    }

    public void setAa1Imppro(String aa1Imppro) {
        this.aa1Imppro = aa1Imppro;
    }

    public String getAa1Catego() {
        return aa1Catego;
    }

    public void setAa1Catego(String aa1Catego) {
        this.aa1Catego = aa1Catego;
    }

    public String getAa1Altvis() {
        return aa1Altvis;
    }

    public void setAa1Altvis(String aa1Altvis) {
        this.aa1Altvis = aa1Altvis;
    }

    public String getAa1Ftvist() {
        return aa1Ftvist;
    }

    public void setAa1Ftvist(String aa1Ftvist) {
        this.aa1Ftvist = aa1Ftvist;
    }

    public String getAa1Crmsim() {
        return aa1Crmsim;
    }

    public void setAa1Crmsim(String aa1Crmsim) {
        this.aa1Crmsim = aa1Crmsim;
    }

    public String getAa1Funfil() {
        return aa1Funfil;
    }

    public void setAa1Funfil(String aa1Funfil) {
        this.aa1Funfil = aa1Funfil;
    }

    public String getAa1Mponto() {
        return aa1Mponto;
    }

    public void setAa1Mponto(String aa1Mponto) {
        this.aa1Mponto = aa1Mponto;
    }    
    
    public String getAa1Ucrede() {
		return aa1Ucrede;
	}

	public void setAa1Ucrede(String aa1Ucrede) {
		this.aa1Ucrede = aa1Ucrede;
	}

	public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getAa1Ulogin() {
        return aa1Ulogin;
    }

    public void setAa1Ulogin(String aa1Ulogin) {
        this.aa1Ulogin = aa1Ulogin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aa1010)) {
            return false;
        }
        Aa1010 other = (Aa1010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Aa1010[ rECNO=" + rECNO + " ]";
    }
}
