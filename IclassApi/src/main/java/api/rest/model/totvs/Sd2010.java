/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author ROBSON 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(name = "SD2010", schema = "dbo")
public class Sd2010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "D2_FILIAL")
    private String d2Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_ITEM")
    private String d2Item;
    @ManyToOne
    @JoinColumn(name = "D2_COD", referencedColumnName = "B1_COD", nullable = false, insertable = false, updatable = false)
    @JsonBackReference(value="Sb1010")
    private Sb1010 sb1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_UM")
    private String d2Um;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_SEGUM")
    private String d2Segum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_QUANT")
    private double d2Quant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_PRCVEN")
    private double d2Prcven;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_TOTAL")
    private double d2Total;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIPI")
    private double d2Valipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALICM")
    private double d2Valicm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_TES")
    private String d2Tes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "D2_CF")
    private String d2Cf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_DESC")
    private double d2Desc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_IPI")
    private double d2Ipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_PICM")
    private double d2Picm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALCSL")
    private double d2Valcsl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_CONTA")
    private String d2Conta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_PESO")
    private double d2Peso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "D2_OP")
    private String d2Op;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_PEDIDO")
    private String d2Pedido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_ITEMPV")
    private String d2Itempv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_CLIENTE")
    private String d2Cliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_LOJA")
    private String d2Loja;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_LOCAL")
    private String d2Local;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "D2_DOC", referencedColumnName = "F2_DOC", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "D2_SERIE", referencedColumnName = "F2_SERIE", nullable = false, insertable = false, updatable = false),})
    @JsonBackReference(value="sf2010")
    private Sf2010 sf2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "D2_GRUPO")
    private String d2Grupo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_TP")
    private String d2Tp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "D2_EMISSAO")
    private String d2Emissao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSTO1")
    private double d2Custo1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSTO2")
    private double d2Custo2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSTO3")
    private double d2Custo3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSTO4")
    private double d2Custo4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSTO5")
    private double d2Custo5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_PRUNIT")
    private double d2Prunit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_QTSEGUM")
    private double d2Qtsegum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_NUMSEQ")
    private String d2Numseq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_EST")
    private String d2Est;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_DESCON")
    private double d2Descon;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_TIPO")
    private String d2Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_NFORI")
    private String d2Nfori;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_SERIORI")
    private String d2Seriori;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_QTDEDEV")
    private double d2Qtdedev;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALDEV")
    private double d2Valdev;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_ORIGLAN")
    private String d2Origlan;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BRICMS")
    private double d2Bricms;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEORI")
    private double d2Baseori;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEICM")
    private double d2Baseicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALACRS")
    private double d2Valacrs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_IDENTB6")
    private String d2Identb6;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_CODISS")
    private String d2Codiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_GRADE")
    private String d2Grade;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "D2_SEQCALC")
    private String d2Seqcalc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ICMSRET")
    private double d2Icmsret;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_COMIS1")
    private double d2Comis1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_COMIS2")
    private double d2Comis2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_COMIS3")
    private double d2Comis3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_COMIS4")
    private double d2Comis4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_COMIS5")
    private double d2Comis5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "D2_LOTECTL")
    private String d2Lotectl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_NUMLOTE")
    private String d2Numlote;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "D2_DTVALID")
    private String d2Dtvalid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_DESCZFR")
    private double d2Desczfr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "D2_PDV")
    private String d2Pdv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_NUMSERI")
    private String d2Numseri;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "D2_DTLCTCT")
    private String d2Dtlctct;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSFF1")
    private double d2Cusff1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSFF2")
    private double d2Cusff2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSFF3")
    private double d2Cusff3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSFF4")
    private double d2Cusff4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSFF5")
    private double d2Cusff5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_CLASFIS")
    private String d2Clasfis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASIMP1")
    private double d2Basimp1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_REMITO")
    private String d2Remito;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_SERIREM")
    private String d2Serirem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASIMP2")
    private double d2Basimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASIMP3")
    private double d2Basimp3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_ITEMREM")
    private String d2Itemrem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASIMP4")
    private double d2Basimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASIMP5")
    private double d2Basimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASIMP6")
    private double d2Basimp6;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIMP1")
    private double d2Valimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIMP2")
    private double d2Valimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIMP3")
    private double d2Valimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIMP4")
    private double d2Valimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIMP5")
    private double d2Valimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIMP6")
    private double d2Valimp6;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "D2_ITEMORI")
    private String d2Itemori;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_CODFAB")
    private String d2Codfab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_LOJAFA")
    private String d2Lojafa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQIMP1")
    private double d2Alqimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQIMP2")
    private double d2Alqimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQIMP3")
    private double d2Alqimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQIMP4")
    private double d2Alqimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQIMP5")
    private double d2Alqimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQIMP6")
    private double d2Alqimp6;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "D2_ESPECIE")
    private String d2Especie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_CCUSTO")
    private String d2Ccusto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_ITEMCC")
    private String d2Itemcc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "D2_LOCALIZ")
    private String d2Localiz;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "D2_ENVCNAB")
    private String d2Envcnab;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQINS")
    private double d2Aliqins;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ABSCINS")
    private double d2Abscins;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VCAT156")
    private double d2Vcat156;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VLIMPOR")
    private double d2Vlimpor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_PREEMB")
    private String d2Preemb;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQISS")
    private double d2Aliqiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEIPI")
    private double d2Baseipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEISS")
    private double d2Baseiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALISS")
    private double d2Valiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_SEGURO")
    private double d2Seguro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALFRE")
    private double d2Valfre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_TPDCENV")
    private String d2Tpdcenv;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_DESPESA")
    private double d2Despesa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_OK")
    private String d2Ok;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_CLVL")
    private String d2Clvl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEINS")
    private double d2Baseins;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ICMFRET")
    private double d2Icmfret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_SERVIC")
    private String d2Servic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_STSERV")
    private String d2Stserv;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALINS")
    private double d2Valins;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "D2_PROJPMS")
    private String d2Projpms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "D2_EDTPMS")
    private String d2Edtpms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "D2_TASKPMS")
    private String d2Taskpms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_LICITA")
    private String d2Licita;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VARPRUN")
    private double d2Varprun;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_FORMUL")
    private String d2Formul;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_TIPODOC")
    private String d2Tipodoc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VAC")
    private double d2Vac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_TIPOREM")
    private String d2Tiporem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_QTDEFAT")
    private double d2Qtdefat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_QTDAFAT")
    private double d2Qtdafat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_SEQUEN")
    private String d2Sequen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_POTENCI")
    private double d2Potenci;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_TPESTR")
    private String d2Tpestr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALBRUT")
    private double d2Valbrut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_REGWMS")
    private String d2Regwms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "D2_DTDIGIT")
    private String d2Dtdigit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_NUMCP")
    private String d2Numcp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSRP1")
    private double d2Cusrp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSRP2")
    private double d2Cusrp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSRP3")
    private double d2Cusrp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSRP4")
    private double d2Cusrp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CUSRP5")
    private double d2Cusrp5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "D2_SITTRIB")
    private String d2Sittrib;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "D2_NFCUP")
    private String d2Nfcup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEPS3")
    private double d2Baseps3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQPS3")
    private double d2Aliqps3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALPS3")
    private double d2Valps3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_CFPS")
    private String d2Cfps;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_DESCICM")
    private double d2Descicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASECF3")
    private double d2Basecf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQCF3")
    private double d2Aliqcf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALCF3")
    private double d2Valcf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEIRR")
    private double d2Baseirr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_CODROM")
    private String d2Codrom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ESTCRED")
    private double d2Estcred;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_UFERMS")
    private double d2Uferms;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_PRFDSUL")
    private double d2Prfdsul;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALFDS")
    private double d2Valfds;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_TRT")
    private String d2Trt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_PRUNDA")
    private double d2Prunda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALTST")
    private double d2Valtst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQTST")
    private double d2Aliqtst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASETST")
    private double d2Basetst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQIRRF")
    private double d2Alqirrf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_ORDSEP")
    private String d2Ordsep;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_DESCZFP")
    private double d2Desczfp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_DESCZFC")
    private double d2Desczfc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASECOF")
    private double d2Basecof;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASECSL")
    private double d2Basecsl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEPIS")
    private double d2Basepis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALCOF")
    private double d2Valcof;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALIRRF")
    private double d2Valirrf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ABATMAT")
    private double d2Abatmat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQINA")
    private double d2Aliqina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEINA")
    private double d2Baseina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALINA")
    private double d2Valina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ABATISS")
    private double d2Abatiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "D2_DTFIMNT")
    private String d2Dtfimnt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "D2_GRUPONC")
    private String d2Gruponc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_CNATREC")
    private String d2Cnatrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "D2_TNATREC")
    private String d2Tnatrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_ESTOQUE")
    private String d2Estoque;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "D2_FCICOD")
    private String d2Fcicod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_IDSF7")
    private String d2Idsf7;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_IDSF4")
    private String d2Idsf4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_IDSBZ")
    private String d2Idsbz;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_IDSB5")
    private String d2Idsb5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_IDSB1")
    private String d2Idsb1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALFAC")
    private double d2Valfac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALFET")
    private double d2Valfet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEFET")
    private double d2Basefet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_CRPRESC")
    private double d2Crpresc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "D2_OKISS")
    private String d2Okiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_09CAT17")
    private double d209cat17;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_16CAT17")
    private double d216cat17;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ABATINS")
    private double d2Abatins;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQFAB")
    private double d2Aliqfab;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQFAC")
    private double d2Aliqfac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQFET")
    private double d2Aliqfet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEFAB")
    private double d2Basefab;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEFAC")
    private double d2Basefac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALFAB")
    private double d2Valfab;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQSOL")
    private double d2Aliqsol;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_CODLAN")
    private String d2Codlan;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BSSENAR")
    private double d2Bssenar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VLSENAR")
    private double d2Vlsenar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALSENAR")
    private double d2Alsenar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D2_RATEIO")
    private String d2Rateio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "D2_CBASEAF")
    private String d2Cbaseaf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BASEFUN")
    private double d2Basefun;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALIQFUN")
    private double d2Aliqfun;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALFUN")
    private double d2Valfun;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "D2_CODLPRE")
    private String d2Codlpre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_ITLPRE")
    private String d2Itlpre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VALPIS")
    private double d2Valpis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQCOF")
    private double d2Alqcof;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQCSL")
    private double d2Alqcsl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_ALQPIS")
    private double d2Alqpis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_MARGEM")
    private double d2Margem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VLINCMG")
    private double d2Vlincmg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_PRINCMG")
    private double d2Princmg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "D2_PRODFIN")
    private String d2Prodfin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "D2_REVISAO")
    private String d2Revisao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_BSREIN")
    private double d2Bsrein;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_VREINT")
    private double d2Vreint;
    @Basic(optional = false)
    @NotNull
    @Column(name = "D2_TOTIMP")
    private double d2Totimp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "D2_IDCFC")
    private String d2Idcfc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;

    public Sd2010() {
    }

    public Sd2010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getD2Filial() {
        return d2Filial;
    }

    public void setD2Filial(String d2Filial) {
        this.d2Filial = d2Filial;
    }

    public String getD2Item() {
        return d2Item;
    }

    public void setD2Item(String d2Item) {
        this.d2Item = d2Item;
    }

    public Sb1010 getSb1() {
        return sb1;
    }

    public void setSb1(Sb1010 sb1) {
        this.sb1 = sb1;
    }

    public String getD2Um() {
        return d2Um;
    }

    public void setD2Um(String d2Um) {
        this.d2Um = d2Um;
    }

    public String getD2Segum() {
        return d2Segum;
    }

    public void setD2Segum(String d2Segum) {
        this.d2Segum = d2Segum;
    }

    public double getD2Quant() {
        return d2Quant;
    }

    public void setD2Quant(double d2Quant) {
        this.d2Quant = d2Quant;
    }

    public double getD2Prcven() {
        return d2Prcven;
    }

    public void setD2Prcven(double d2Prcven) {
        this.d2Prcven = d2Prcven;
    }

    public double getD2Total() {
        return d2Total;
    }

    public void setD2Total(double d2Total) {
        this.d2Total = d2Total;
    }

    public double getD2Valipi() {
        return d2Valipi;
    }

    public void setD2Valipi(double d2Valipi) {
        this.d2Valipi = d2Valipi;
    }

    public double getD2Valicm() {
        return d2Valicm;
    }

    public void setD2Valicm(double d2Valicm) {
        this.d2Valicm = d2Valicm;
    }

    public String getD2Tes() {
        return d2Tes;
    }

    public void setD2Tes(String d2Tes) {
        this.d2Tes = d2Tes;
    }

    public String getD2Cf() {
        return d2Cf;
    }

    public void setD2Cf(String d2Cf) {
        this.d2Cf = d2Cf;
    }

    public double getD2Desc() {
        return d2Desc;
    }

    public void setD2Desc(double d2Desc) {
        this.d2Desc = d2Desc;
    }

    public double getD2Ipi() {
        return d2Ipi;
    }

    public void setD2Ipi(double d2Ipi) {
        this.d2Ipi = d2Ipi;
    }

    public double getD2Picm() {
        return d2Picm;
    }

    public void setD2Picm(double d2Picm) {
        this.d2Picm = d2Picm;
    }

    public double getD2Valcsl() {
        return d2Valcsl;
    }

    public void setD2Valcsl(double d2Valcsl) {
        this.d2Valcsl = d2Valcsl;
    }

    public String getD2Conta() {
        return d2Conta;
    }

    public void setD2Conta(String d2Conta) {
        this.d2Conta = d2Conta;
    }

    public double getD2Peso() {
        return d2Peso;
    }

    public void setD2Peso(double d2Peso) {
        this.d2Peso = d2Peso;
    }

    public String getD2Op() {
        return d2Op;
    }

    public void setD2Op(String d2Op) {
        this.d2Op = d2Op;
    }

    public String getD2Pedido() {
        return d2Pedido;
    }

    public void setD2Pedido(String d2Pedido) {
        this.d2Pedido = d2Pedido;
    }

    public String getD2Itempv() {
        return d2Itempv;
    }

    public void setD2Itempv(String d2Itempv) {
        this.d2Itempv = d2Itempv;
    }

    public String getD2Cliente() {
        return d2Cliente;
    }

    public void setD2Cliente(String d2Cliente) {
        this.d2Cliente = d2Cliente;
    }

    public String getD2Loja() {
        return d2Loja;
    }

    public void setD2Loja(String d2Loja) {
        this.d2Loja = d2Loja;
    }

    public String getD2Local() {
        return d2Local;
    }

    public void setD2Local(String d2Local) {
        this.d2Local = d2Local;
    }

    public Sf2010 getSf2() {
        return sf2;
    }

    public void setSf2(Sf2010 sf2) {
        this.sf2 = sf2;
    }

    public String getD2Grupo() {
        return d2Grupo;
    }

    public void setD2Grupo(String d2Grupo) {
        this.d2Grupo = d2Grupo;
    }

    public String getD2Tp() {
        return d2Tp;
    }

    public void setD2Tp(String d2Tp) {
        this.d2Tp = d2Tp;
    }

    public String getD2Emissao() {
        return d2Emissao;
    }

    public void setD2Emissao(String d2Emissao) {
        this.d2Emissao = d2Emissao;
    }

    public double getD2Custo1() {
        return d2Custo1;
    }

    public void setD2Custo1(double d2Custo1) {
        this.d2Custo1 = d2Custo1;
    }

    public double getD2Custo2() {
        return d2Custo2;
    }

    public void setD2Custo2(double d2Custo2) {
        this.d2Custo2 = d2Custo2;
    }

    public double getD2Custo3() {
        return d2Custo3;
    }

    public void setD2Custo3(double d2Custo3) {
        this.d2Custo3 = d2Custo3;
    }

    public double getD2Custo4() {
        return d2Custo4;
    }

    public void setD2Custo4(double d2Custo4) {
        this.d2Custo4 = d2Custo4;
    }

    public double getD2Custo5() {
        return d2Custo5;
    }

    public void setD2Custo5(double d2Custo5) {
        this.d2Custo5 = d2Custo5;
    }

    public double getD2Prunit() {
        return d2Prunit;
    }

    public void setD2Prunit(double d2Prunit) {
        this.d2Prunit = d2Prunit;
    }

    public double getD2Qtsegum() {
        return d2Qtsegum;
    }

    public void setD2Qtsegum(double d2Qtsegum) {
        this.d2Qtsegum = d2Qtsegum;
    }

    public String getD2Numseq() {
        return d2Numseq;
    }

    public void setD2Numseq(String d2Numseq) {
        this.d2Numseq = d2Numseq;
    }

    public String getD2Est() {
        return d2Est;
    }

    public void setD2Est(String d2Est) {
        this.d2Est = d2Est;
    }

    public double getD2Descon() {
        return d2Descon;
    }

    public void setD2Descon(double d2Descon) {
        this.d2Descon = d2Descon;
    }

    public String getD2Tipo() {
        return d2Tipo;
    }

    public void setD2Tipo(String d2Tipo) {
        this.d2Tipo = d2Tipo;
    }

    public String getD2Nfori() {
        return d2Nfori;
    }

    public void setD2Nfori(String d2Nfori) {
        this.d2Nfori = d2Nfori;
    }

    public String getD2Seriori() {
        return d2Seriori;
    }

    public void setD2Seriori(String d2Seriori) {
        this.d2Seriori = d2Seriori;
    }

    public double getD2Qtdedev() {
        return d2Qtdedev;
    }

    public void setD2Qtdedev(double d2Qtdedev) {
        this.d2Qtdedev = d2Qtdedev;
    }

    public double getD2Valdev() {
        return d2Valdev;
    }

    public void setD2Valdev(double d2Valdev) {
        this.d2Valdev = d2Valdev;
    }

    public String getD2Origlan() {
        return d2Origlan;
    }

    public void setD2Origlan(String d2Origlan) {
        this.d2Origlan = d2Origlan;
    }

    public double getD2Bricms() {
        return d2Bricms;
    }

    public void setD2Bricms(double d2Bricms) {
        this.d2Bricms = d2Bricms;
    }

    public double getD2Baseori() {
        return d2Baseori;
    }

    public void setD2Baseori(double d2Baseori) {
        this.d2Baseori = d2Baseori;
    }

    public double getD2Baseicm() {
        return d2Baseicm;
    }

    public void setD2Baseicm(double d2Baseicm) {
        this.d2Baseicm = d2Baseicm;
    }

    public double getD2Valacrs() {
        return d2Valacrs;
    }

    public void setD2Valacrs(double d2Valacrs) {
        this.d2Valacrs = d2Valacrs;
    }

    public String getD2Identb6() {
        return d2Identb6;
    }

    public void setD2Identb6(String d2Identb6) {
        this.d2Identb6 = d2Identb6;
    }

    public String getD2Codiss() {
        return d2Codiss;
    }

    public void setD2Codiss(String d2Codiss) {
        this.d2Codiss = d2Codiss;
    }

    public String getD2Grade() {
        return d2Grade;
    }

    public void setD2Grade(String d2Grade) {
        this.d2Grade = d2Grade;
    }

    public String getD2Seqcalc() {
        return d2Seqcalc;
    }

    public void setD2Seqcalc(String d2Seqcalc) {
        this.d2Seqcalc = d2Seqcalc;
    }

    public double getD2Icmsret() {
        return d2Icmsret;
    }

    public void setD2Icmsret(double d2Icmsret) {
        this.d2Icmsret = d2Icmsret;
    }

    public double getD2Comis1() {
        return d2Comis1;
    }

    public void setD2Comis1(double d2Comis1) {
        this.d2Comis1 = d2Comis1;
    }

    public double getD2Comis2() {
        return d2Comis2;
    }

    public void setD2Comis2(double d2Comis2) {
        this.d2Comis2 = d2Comis2;
    }

    public double getD2Comis3() {
        return d2Comis3;
    }

    public void setD2Comis3(double d2Comis3) {
        this.d2Comis3 = d2Comis3;
    }

    public double getD2Comis4() {
        return d2Comis4;
    }

    public void setD2Comis4(double d2Comis4) {
        this.d2Comis4 = d2Comis4;
    }

    public double getD2Comis5() {
        return d2Comis5;
    }

    public void setD2Comis5(double d2Comis5) {
        this.d2Comis5 = d2Comis5;
    }

    public String getD2Lotectl() {
        return d2Lotectl;
    }

    public void setD2Lotectl(String d2Lotectl) {
        this.d2Lotectl = d2Lotectl;
    }

    public String getD2Numlote() {
        return d2Numlote;
    }

    public void setD2Numlote(String d2Numlote) {
        this.d2Numlote = d2Numlote;
    }

    public String getD2Dtvalid() {
        return d2Dtvalid;
    }

    public void setD2Dtvalid(String d2Dtvalid) {
        this.d2Dtvalid = d2Dtvalid;
    }

    public double getD2Desczfr() {
        return d2Desczfr;
    }

    public void setD2Desczfr(double d2Desczfr) {
        this.d2Desczfr = d2Desczfr;
    }

    public String getD2Pdv() {
        return d2Pdv;
    }

    public void setD2Pdv(String d2Pdv) {
        this.d2Pdv = d2Pdv;
    }

    public String getD2Numseri() {
        return d2Numseri;
    }

    public void setD2Numseri(String d2Numseri) {
        this.d2Numseri = d2Numseri;
    }

    public String getD2Dtlctct() {
        return d2Dtlctct;
    }

    public void setD2Dtlctct(String d2Dtlctct) {
        this.d2Dtlctct = d2Dtlctct;
    }

    public double getD2Cusff1() {
        return d2Cusff1;
    }

    public void setD2Cusff1(double d2Cusff1) {
        this.d2Cusff1 = d2Cusff1;
    }

    public double getD2Cusff2() {
        return d2Cusff2;
    }

    public void setD2Cusff2(double d2Cusff2) {
        this.d2Cusff2 = d2Cusff2;
    }

    public double getD2Cusff3() {
        return d2Cusff3;
    }

    public void setD2Cusff3(double d2Cusff3) {
        this.d2Cusff3 = d2Cusff3;
    }

    public double getD2Cusff4() {
        return d2Cusff4;
    }

    public void setD2Cusff4(double d2Cusff4) {
        this.d2Cusff4 = d2Cusff4;
    }

    public double getD2Cusff5() {
        return d2Cusff5;
    }

    public void setD2Cusff5(double d2Cusff5) {
        this.d2Cusff5 = d2Cusff5;
    }

    public String getD2Clasfis() {
        return d2Clasfis;
    }

    public void setD2Clasfis(String d2Clasfis) {
        this.d2Clasfis = d2Clasfis;
    }

    public double getD2Basimp1() {
        return d2Basimp1;
    }

    public void setD2Basimp1(double d2Basimp1) {
        this.d2Basimp1 = d2Basimp1;
    }

    public String getD2Remito() {
        return d2Remito;
    }

    public void setD2Remito(String d2Remito) {
        this.d2Remito = d2Remito;
    }

    public String getD2Serirem() {
        return d2Serirem;
    }

    public void setD2Serirem(String d2Serirem) {
        this.d2Serirem = d2Serirem;
    }

    public double getD2Basimp2() {
        return d2Basimp2;
    }

    public void setD2Basimp2(double d2Basimp2) {
        this.d2Basimp2 = d2Basimp2;
    }

    public double getD2Basimp3() {
        return d2Basimp3;
    }

    public void setD2Basimp3(double d2Basimp3) {
        this.d2Basimp3 = d2Basimp3;
    }

    public String getD2Itemrem() {
        return d2Itemrem;
    }

    public void setD2Itemrem(String d2Itemrem) {
        this.d2Itemrem = d2Itemrem;
    }

    public double getD2Basimp4() {
        return d2Basimp4;
    }

    public void setD2Basimp4(double d2Basimp4) {
        this.d2Basimp4 = d2Basimp4;
    }

    public double getD2Basimp5() {
        return d2Basimp5;
    }

    public void setD2Basimp5(double d2Basimp5) {
        this.d2Basimp5 = d2Basimp5;
    }

    public double getD2Basimp6() {
        return d2Basimp6;
    }

    public void setD2Basimp6(double d2Basimp6) {
        this.d2Basimp6 = d2Basimp6;
    }

    public double getD2Valimp1() {
        return d2Valimp1;
    }

    public void setD2Valimp1(double d2Valimp1) {
        this.d2Valimp1 = d2Valimp1;
    }

    public double getD2Valimp2() {
        return d2Valimp2;
    }

    public void setD2Valimp2(double d2Valimp2) {
        this.d2Valimp2 = d2Valimp2;
    }

    public double getD2Valimp3() {
        return d2Valimp3;
    }

    public void setD2Valimp3(double d2Valimp3) {
        this.d2Valimp3 = d2Valimp3;
    }

    public double getD2Valimp4() {
        return d2Valimp4;
    }

    public void setD2Valimp4(double d2Valimp4) {
        this.d2Valimp4 = d2Valimp4;
    }

    public double getD2Valimp5() {
        return d2Valimp5;
    }

    public void setD2Valimp5(double d2Valimp5) {
        this.d2Valimp5 = d2Valimp5;
    }

    public double getD2Valimp6() {
        return d2Valimp6;
    }

    public void setD2Valimp6(double d2Valimp6) {
        this.d2Valimp6 = d2Valimp6;
    }

    public String getD2Itemori() {
        return d2Itemori;
    }

    public void setD2Itemori(String d2Itemori) {
        this.d2Itemori = d2Itemori;
    }

    public String getD2Codfab() {
        return d2Codfab;
    }

    public void setD2Codfab(String d2Codfab) {
        this.d2Codfab = d2Codfab;
    }

    public String getD2Lojafa() {
        return d2Lojafa;
    }

    public void setD2Lojafa(String d2Lojafa) {
        this.d2Lojafa = d2Lojafa;
    }

    public double getD2Alqimp1() {
        return d2Alqimp1;
    }

    public void setD2Alqimp1(double d2Alqimp1) {
        this.d2Alqimp1 = d2Alqimp1;
    }

    public double getD2Alqimp2() {
        return d2Alqimp2;
    }

    public void setD2Alqimp2(double d2Alqimp2) {
        this.d2Alqimp2 = d2Alqimp2;
    }

    public double getD2Alqimp3() {
        return d2Alqimp3;
    }

    public void setD2Alqimp3(double d2Alqimp3) {
        this.d2Alqimp3 = d2Alqimp3;
    }

    public double getD2Alqimp4() {
        return d2Alqimp4;
    }

    public void setD2Alqimp4(double d2Alqimp4) {
        this.d2Alqimp4 = d2Alqimp4;
    }

    public double getD2Alqimp5() {
        return d2Alqimp5;
    }

    public void setD2Alqimp5(double d2Alqimp5) {
        this.d2Alqimp5 = d2Alqimp5;
    }

    public double getD2Alqimp6() {
        return d2Alqimp6;
    }

    public void setD2Alqimp6(double d2Alqimp6) {
        this.d2Alqimp6 = d2Alqimp6;
    }

    public String getD2Especie() {
        return d2Especie;
    }

    public void setD2Especie(String d2Especie) {
        this.d2Especie = d2Especie;
    }

    public String getD2Ccusto() {
        return d2Ccusto;
    }

    public void setD2Ccusto(String d2Ccusto) {
        this.d2Ccusto = d2Ccusto;
    }

    public String getD2Itemcc() {
        return d2Itemcc;
    }

    public void setD2Itemcc(String d2Itemcc) {
        this.d2Itemcc = d2Itemcc;
    }

    public String getD2Localiz() {
        return d2Localiz;
    }

    public void setD2Localiz(String d2Localiz) {
        this.d2Localiz = d2Localiz;
    }

    public String getD2Envcnab() {
        return d2Envcnab;
    }

    public void setD2Envcnab(String d2Envcnab) {
        this.d2Envcnab = d2Envcnab;
    }

    public double getD2Aliqins() {
        return d2Aliqins;
    }

    public void setD2Aliqins(double d2Aliqins) {
        this.d2Aliqins = d2Aliqins;
    }

    public double getD2Abscins() {
        return d2Abscins;
    }

    public void setD2Abscins(double d2Abscins) {
        this.d2Abscins = d2Abscins;
    }

    public double getD2Vcat156() {
        return d2Vcat156;
    }

    public void setD2Vcat156(double d2Vcat156) {
        this.d2Vcat156 = d2Vcat156;
    }

    public double getD2Vlimpor() {
        return d2Vlimpor;
    }

    public void setD2Vlimpor(double d2Vlimpor) {
        this.d2Vlimpor = d2Vlimpor;
    }

    public String getD2Preemb() {
        return d2Preemb;
    }

    public void setD2Preemb(String d2Preemb) {
        this.d2Preemb = d2Preemb;
    }

    public double getD2Aliqiss() {
        return d2Aliqiss;
    }

    public void setD2Aliqiss(double d2Aliqiss) {
        this.d2Aliqiss = d2Aliqiss;
    }

    public double getD2Baseipi() {
        return d2Baseipi;
    }

    public void setD2Baseipi(double d2Baseipi) {
        this.d2Baseipi = d2Baseipi;
    }

    public double getD2Baseiss() {
        return d2Baseiss;
    }

    public void setD2Baseiss(double d2Baseiss) {
        this.d2Baseiss = d2Baseiss;
    }

    public double getD2Valiss() {
        return d2Valiss;
    }

    public void setD2Valiss(double d2Valiss) {
        this.d2Valiss = d2Valiss;
    }

    public double getD2Seguro() {
        return d2Seguro;
    }

    public void setD2Seguro(double d2Seguro) {
        this.d2Seguro = d2Seguro;
    }

    public double getD2Valfre() {
        return d2Valfre;
    }

    public void setD2Valfre(double d2Valfre) {
        this.d2Valfre = d2Valfre;
    }

    public String getD2Tpdcenv() {
        return d2Tpdcenv;
    }

    public void setD2Tpdcenv(String d2Tpdcenv) {
        this.d2Tpdcenv = d2Tpdcenv;
    }

    public double getD2Despesa() {
        return d2Despesa;
    }

    public void setD2Despesa(double d2Despesa) {
        this.d2Despesa = d2Despesa;
    }

    public String getD2Ok() {
        return d2Ok;
    }

    public void setD2Ok(String d2Ok) {
        this.d2Ok = d2Ok;
    }

    public String getD2Clvl() {
        return d2Clvl;
    }

    public void setD2Clvl(String d2Clvl) {
        this.d2Clvl = d2Clvl;
    }

    public double getD2Baseins() {
        return d2Baseins;
    }

    public void setD2Baseins(double d2Baseins) {
        this.d2Baseins = d2Baseins;
    }

    public double getD2Icmfret() {
        return d2Icmfret;
    }

    public void setD2Icmfret(double d2Icmfret) {
        this.d2Icmfret = d2Icmfret;
    }

    public String getD2Servic() {
        return d2Servic;
    }

    public void setD2Servic(String d2Servic) {
        this.d2Servic = d2Servic;
    }

    public String getD2Stserv() {
        return d2Stserv;
    }

    public void setD2Stserv(String d2Stserv) {
        this.d2Stserv = d2Stserv;
    }

    public double getD2Valins() {
        return d2Valins;
    }

    public void setD2Valins(double d2Valins) {
        this.d2Valins = d2Valins;
    }

    public String getD2Projpms() {
        return d2Projpms;
    }

    public void setD2Projpms(String d2Projpms) {
        this.d2Projpms = d2Projpms;
    }

    public String getD2Edtpms() {
        return d2Edtpms;
    }

    public void setD2Edtpms(String d2Edtpms) {
        this.d2Edtpms = d2Edtpms;
    }

    public String getD2Taskpms() {
        return d2Taskpms;
    }

    public void setD2Taskpms(String d2Taskpms) {
        this.d2Taskpms = d2Taskpms;
    }

    public String getD2Licita() {
        return d2Licita;
    }

    public void setD2Licita(String d2Licita) {
        this.d2Licita = d2Licita;
    }

    public double getD2Varprun() {
        return d2Varprun;
    }

    public void setD2Varprun(double d2Varprun) {
        this.d2Varprun = d2Varprun;
    }

    public String getD2Formul() {
        return d2Formul;
    }

    public void setD2Formul(String d2Formul) {
        this.d2Formul = d2Formul;
    }

    public String getD2Tipodoc() {
        return d2Tipodoc;
    }

    public void setD2Tipodoc(String d2Tipodoc) {
        this.d2Tipodoc = d2Tipodoc;
    }

    public double getD2Vac() {
        return d2Vac;
    }

    public void setD2Vac(double d2Vac) {
        this.d2Vac = d2Vac;
    }

    public String getD2Tiporem() {
        return d2Tiporem;
    }

    public void setD2Tiporem(String d2Tiporem) {
        this.d2Tiporem = d2Tiporem;
    }

    public double getD2Qtdefat() {
        return d2Qtdefat;
    }

    public void setD2Qtdefat(double d2Qtdefat) {
        this.d2Qtdefat = d2Qtdefat;
    }

    public double getD2Qtdafat() {
        return d2Qtdafat;
    }

    public void setD2Qtdafat(double d2Qtdafat) {
        this.d2Qtdafat = d2Qtdafat;
    }

    public String getD2Sequen() {
        return d2Sequen;
    }

    public void setD2Sequen(String d2Sequen) {
        this.d2Sequen = d2Sequen;
    }

    public double getD2Potenci() {
        return d2Potenci;
    }

    public void setD2Potenci(double d2Potenci) {
        this.d2Potenci = d2Potenci;
    }

    public String getD2Tpestr() {
        return d2Tpestr;
    }

    public void setD2Tpestr(String d2Tpestr) {
        this.d2Tpestr = d2Tpestr;
    }

    public double getD2Valbrut() {
        return d2Valbrut;
    }

    public void setD2Valbrut(double d2Valbrut) {
        this.d2Valbrut = d2Valbrut;
    }

    public String getD2Regwms() {
        return d2Regwms;
    }

    public void setD2Regwms(String d2Regwms) {
        this.d2Regwms = d2Regwms;
    }

    public String getD2Dtdigit() {
        return d2Dtdigit;
    }

    public void setD2Dtdigit(String d2Dtdigit) {
        this.d2Dtdigit = d2Dtdigit;
    }

    public String getD2Numcp() {
        return d2Numcp;
    }

    public void setD2Numcp(String d2Numcp) {
        this.d2Numcp = d2Numcp;
    }

    public double getD2Cusrp1() {
        return d2Cusrp1;
    }

    public void setD2Cusrp1(double d2Cusrp1) {
        this.d2Cusrp1 = d2Cusrp1;
    }

    public double getD2Cusrp2() {
        return d2Cusrp2;
    }

    public void setD2Cusrp2(double d2Cusrp2) {
        this.d2Cusrp2 = d2Cusrp2;
    }

    public double getD2Cusrp3() {
        return d2Cusrp3;
    }

    public void setD2Cusrp3(double d2Cusrp3) {
        this.d2Cusrp3 = d2Cusrp3;
    }

    public double getD2Cusrp4() {
        return d2Cusrp4;
    }

    public void setD2Cusrp4(double d2Cusrp4) {
        this.d2Cusrp4 = d2Cusrp4;
    }

    public double getD2Cusrp5() {
        return d2Cusrp5;
    }

    public void setD2Cusrp5(double d2Cusrp5) {
        this.d2Cusrp5 = d2Cusrp5;
    }

    public String getD2Sittrib() {
        return d2Sittrib;
    }

    public void setD2Sittrib(String d2Sittrib) {
        this.d2Sittrib = d2Sittrib;
    }

    public String getD2Nfcup() {
        return d2Nfcup;
    }

    public void setD2Nfcup(String d2Nfcup) {
        this.d2Nfcup = d2Nfcup;
    }

    public double getD2Baseps3() {
        return d2Baseps3;
    }

    public void setD2Baseps3(double d2Baseps3) {
        this.d2Baseps3 = d2Baseps3;
    }

    public double getD2Aliqps3() {
        return d2Aliqps3;
    }

    public void setD2Aliqps3(double d2Aliqps3) {
        this.d2Aliqps3 = d2Aliqps3;
    }

    public double getD2Valps3() {
        return d2Valps3;
    }

    public void setD2Valps3(double d2Valps3) {
        this.d2Valps3 = d2Valps3;
    }

    public String getD2Cfps() {
        return d2Cfps;
    }

    public void setD2Cfps(String d2Cfps) {
        this.d2Cfps = d2Cfps;
    }

    public double getD2Descicm() {
        return d2Descicm;
    }

    public void setD2Descicm(double d2Descicm) {
        this.d2Descicm = d2Descicm;
    }

    public double getD2Basecf3() {
        return d2Basecf3;
    }

    public void setD2Basecf3(double d2Basecf3) {
        this.d2Basecf3 = d2Basecf3;
    }

    public double getD2Aliqcf3() {
        return d2Aliqcf3;
    }

    public void setD2Aliqcf3(double d2Aliqcf3) {
        this.d2Aliqcf3 = d2Aliqcf3;
    }

    public double getD2Valcf3() {
        return d2Valcf3;
    }

    public void setD2Valcf3(double d2Valcf3) {
        this.d2Valcf3 = d2Valcf3;
    }

    public double getD2Baseirr() {
        return d2Baseirr;
    }

    public void setD2Baseirr(double d2Baseirr) {
        this.d2Baseirr = d2Baseirr;
    }

    public String getD2Codrom() {
        return d2Codrom;
    }

    public void setD2Codrom(String d2Codrom) {
        this.d2Codrom = d2Codrom;
    }

    public double getD2Estcred() {
        return d2Estcred;
    }

    public void setD2Estcred(double d2Estcred) {
        this.d2Estcred = d2Estcred;
    }

    public double getD2Uferms() {
        return d2Uferms;
    }

    public void setD2Uferms(double d2Uferms) {
        this.d2Uferms = d2Uferms;
    }

    public double getD2Prfdsul() {
        return d2Prfdsul;
    }

    public void setD2Prfdsul(double d2Prfdsul) {
        this.d2Prfdsul = d2Prfdsul;
    }

    public double getD2Valfds() {
        return d2Valfds;
    }

    public void setD2Valfds(double d2Valfds) {
        this.d2Valfds = d2Valfds;
    }

    public String getD2Trt() {
        return d2Trt;
    }

    public void setD2Trt(String d2Trt) {
        this.d2Trt = d2Trt;
    }

    public double getD2Prunda() {
        return d2Prunda;
    }

    public void setD2Prunda(double d2Prunda) {
        this.d2Prunda = d2Prunda;
    }

    public double getD2Valtst() {
        return d2Valtst;
    }

    public void setD2Valtst(double d2Valtst) {
        this.d2Valtst = d2Valtst;
    }

    public double getD2Aliqtst() {
        return d2Aliqtst;
    }

    public void setD2Aliqtst(double d2Aliqtst) {
        this.d2Aliqtst = d2Aliqtst;
    }

    public double getD2Basetst() {
        return d2Basetst;
    }

    public void setD2Basetst(double d2Basetst) {
        this.d2Basetst = d2Basetst;
    }

    public double getD2Alqirrf() {
        return d2Alqirrf;
    }

    public void setD2Alqirrf(double d2Alqirrf) {
        this.d2Alqirrf = d2Alqirrf;
    }

    public String getD2Ordsep() {
        return d2Ordsep;
    }

    public void setD2Ordsep(String d2Ordsep) {
        this.d2Ordsep = d2Ordsep;
    }

    public double getD2Desczfp() {
        return d2Desczfp;
    }

    public void setD2Desczfp(double d2Desczfp) {
        this.d2Desczfp = d2Desczfp;
    }

    public double getD2Desczfc() {
        return d2Desczfc;
    }

    public void setD2Desczfc(double d2Desczfc) {
        this.d2Desczfc = d2Desczfc;
    }

    public double getD2Basecof() {
        return d2Basecof;
    }

    public void setD2Basecof(double d2Basecof) {
        this.d2Basecof = d2Basecof;
    }

    public double getD2Basecsl() {
        return d2Basecsl;
    }

    public void setD2Basecsl(double d2Basecsl) {
        this.d2Basecsl = d2Basecsl;
    }

    public double getD2Basepis() {
        return d2Basepis;
    }

    public void setD2Basepis(double d2Basepis) {
        this.d2Basepis = d2Basepis;
    }

    public double getD2Valcof() {
        return d2Valcof;
    }

    public void setD2Valcof(double d2Valcof) {
        this.d2Valcof = d2Valcof;
    }

    public double getD2Valirrf() {
        return d2Valirrf;
    }

    public void setD2Valirrf(double d2Valirrf) {
        this.d2Valirrf = d2Valirrf;
    }

    public double getD2Abatmat() {
        return d2Abatmat;
    }

    public void setD2Abatmat(double d2Abatmat) {
        this.d2Abatmat = d2Abatmat;
    }

    public double getD2Aliqina() {
        return d2Aliqina;
    }

    public void setD2Aliqina(double d2Aliqina) {
        this.d2Aliqina = d2Aliqina;
    }

    public double getD2Baseina() {
        return d2Baseina;
    }

    public void setD2Baseina(double d2Baseina) {
        this.d2Baseina = d2Baseina;
    }

    public double getD2Valina() {
        return d2Valina;
    }

    public void setD2Valina(double d2Valina) {
        this.d2Valina = d2Valina;
    }

    public double getD2Abatiss() {
        return d2Abatiss;
    }

    public void setD2Abatiss(double d2Abatiss) {
        this.d2Abatiss = d2Abatiss;
    }

    public String getD2Dtfimnt() {
        return d2Dtfimnt;
    }

    public void setD2Dtfimnt(String d2Dtfimnt) {
        this.d2Dtfimnt = d2Dtfimnt;
    }

    public String getD2Gruponc() {
        return d2Gruponc;
    }

    public void setD2Gruponc(String d2Gruponc) {
        this.d2Gruponc = d2Gruponc;
    }

    public String getD2Cnatrec() {
        return d2Cnatrec;
    }

    public void setD2Cnatrec(String d2Cnatrec) {
        this.d2Cnatrec = d2Cnatrec;
    }

    public String getD2Tnatrec() {
        return d2Tnatrec;
    }

    public void setD2Tnatrec(String d2Tnatrec) {
        this.d2Tnatrec = d2Tnatrec;
    }

    public String getD2Estoque() {
        return d2Estoque;
    }

    public void setD2Estoque(String d2Estoque) {
        this.d2Estoque = d2Estoque;
    }

    public String getD2Fcicod() {
        return d2Fcicod;
    }

    public void setD2Fcicod(String d2Fcicod) {
        this.d2Fcicod = d2Fcicod;
    }

    public String getD2Idsf7() {
        return d2Idsf7;
    }

    public void setD2Idsf7(String d2Idsf7) {
        this.d2Idsf7 = d2Idsf7;
    }

    public String getD2Idsf4() {
        return d2Idsf4;
    }

    public void setD2Idsf4(String d2Idsf4) {
        this.d2Idsf4 = d2Idsf4;
    }

    public String getD2Idsbz() {
        return d2Idsbz;
    }

    public void setD2Idsbz(String d2Idsbz) {
        this.d2Idsbz = d2Idsbz;
    }

    public String getD2Idsb5() {
        return d2Idsb5;
    }

    public void setD2Idsb5(String d2Idsb5) {
        this.d2Idsb5 = d2Idsb5;
    }

    public String getD2Idsb1() {
        return d2Idsb1;
    }

    public void setD2Idsb1(String d2Idsb1) {
        this.d2Idsb1 = d2Idsb1;
    }

    public double getD2Valfac() {
        return d2Valfac;
    }

    public void setD2Valfac(double d2Valfac) {
        this.d2Valfac = d2Valfac;
    }

    public double getD2Valfet() {
        return d2Valfet;
    }

    public void setD2Valfet(double d2Valfet) {
        this.d2Valfet = d2Valfet;
    }

    public double getD2Basefet() {
        return d2Basefet;
    }

    public void setD2Basefet(double d2Basefet) {
        this.d2Basefet = d2Basefet;
    }

    public double getD2Crpresc() {
        return d2Crpresc;
    }

    public void setD2Crpresc(double d2Crpresc) {
        this.d2Crpresc = d2Crpresc;
    }

    public String getD2Okiss() {
        return d2Okiss;
    }

    public void setD2Okiss(String d2Okiss) {
        this.d2Okiss = d2Okiss;
    }

    public double getD209cat17() {
        return d209cat17;
    }

    public void setD209cat17(double d209cat17) {
        this.d209cat17 = d209cat17;
    }

    public double getD216cat17() {
        return d216cat17;
    }

    public void setD216cat17(double d216cat17) {
        this.d216cat17 = d216cat17;
    }

    public double getD2Abatins() {
        return d2Abatins;
    }

    public void setD2Abatins(double d2Abatins) {
        this.d2Abatins = d2Abatins;
    }

    public double getD2Aliqfab() {
        return d2Aliqfab;
    }

    public void setD2Aliqfab(double d2Aliqfab) {
        this.d2Aliqfab = d2Aliqfab;
    }

    public double getD2Aliqfac() {
        return d2Aliqfac;
    }

    public void setD2Aliqfac(double d2Aliqfac) {
        this.d2Aliqfac = d2Aliqfac;
    }

    public double getD2Aliqfet() {
        return d2Aliqfet;
    }

    public void setD2Aliqfet(double d2Aliqfet) {
        this.d2Aliqfet = d2Aliqfet;
    }

    public double getD2Basefab() {
        return d2Basefab;
    }

    public void setD2Basefab(double d2Basefab) {
        this.d2Basefab = d2Basefab;
    }

    public double getD2Basefac() {
        return d2Basefac;
    }

    public void setD2Basefac(double d2Basefac) {
        this.d2Basefac = d2Basefac;
    }

    public double getD2Valfab() {
        return d2Valfab;
    }

    public void setD2Valfab(double d2Valfab) {
        this.d2Valfab = d2Valfab;
    }

    public double getD2Aliqsol() {
        return d2Aliqsol;
    }

    public void setD2Aliqsol(double d2Aliqsol) {
        this.d2Aliqsol = d2Aliqsol;
    }

    public String getD2Codlan() {
        return d2Codlan;
    }

    public void setD2Codlan(String d2Codlan) {
        this.d2Codlan = d2Codlan;
    }

    public double getD2Bssenar() {
        return d2Bssenar;
    }

    public void setD2Bssenar(double d2Bssenar) {
        this.d2Bssenar = d2Bssenar;
    }

    public double getD2Vlsenar() {
        return d2Vlsenar;
    }

    public void setD2Vlsenar(double d2Vlsenar) {
        this.d2Vlsenar = d2Vlsenar;
    }

    public double getD2Alsenar() {
        return d2Alsenar;
    }

    public void setD2Alsenar(double d2Alsenar) {
        this.d2Alsenar = d2Alsenar;
    }

    public String getD2Rateio() {
        return d2Rateio;
    }

    public void setD2Rateio(String d2Rateio) {
        this.d2Rateio = d2Rateio;
    }

    public String getD2Cbaseaf() {
        return d2Cbaseaf;
    }

    public void setD2Cbaseaf(String d2Cbaseaf) {
        this.d2Cbaseaf = d2Cbaseaf;
    }

    public double getD2Basefun() {
        return d2Basefun;
    }

    public void setD2Basefun(double d2Basefun) {
        this.d2Basefun = d2Basefun;
    }

    public double getD2Aliqfun() {
        return d2Aliqfun;
    }

    public void setD2Aliqfun(double d2Aliqfun) {
        this.d2Aliqfun = d2Aliqfun;
    }

    public double getD2Valfun() {
        return d2Valfun;
    }

    public void setD2Valfun(double d2Valfun) {
        this.d2Valfun = d2Valfun;
    }

    public String getD2Codlpre() {
        return d2Codlpre;
    }

    public void setD2Codlpre(String d2Codlpre) {
        this.d2Codlpre = d2Codlpre;
    }

    public String getD2Itlpre() {
        return d2Itlpre;
    }

    public void setD2Itlpre(String d2Itlpre) {
        this.d2Itlpre = d2Itlpre;
    }

    public double getD2Valpis() {
        return d2Valpis;
    }

    public void setD2Valpis(double d2Valpis) {
        this.d2Valpis = d2Valpis;
    }

    public double getD2Alqcof() {
        return d2Alqcof;
    }

    public void setD2Alqcof(double d2Alqcof) {
        this.d2Alqcof = d2Alqcof;
    }

    public double getD2Alqcsl() {
        return d2Alqcsl;
    }

    public void setD2Alqcsl(double d2Alqcsl) {
        this.d2Alqcsl = d2Alqcsl;
    }

    public double getD2Alqpis() {
        return d2Alqpis;
    }

    public void setD2Alqpis(double d2Alqpis) {
        this.d2Alqpis = d2Alqpis;
    }

    public double getD2Margem() {
        return d2Margem;
    }

    public void setD2Margem(double d2Margem) {
        this.d2Margem = d2Margem;
    }

    public double getD2Vlincmg() {
        return d2Vlincmg;
    }

    public void setD2Vlincmg(double d2Vlincmg) {
        this.d2Vlincmg = d2Vlincmg;
    }

    public double getD2Princmg() {
        return d2Princmg;
    }

    public void setD2Princmg(double d2Princmg) {
        this.d2Princmg = d2Princmg;
    }

    public String getD2Prodfin() {
        return d2Prodfin;
    }

    public void setD2Prodfin(String d2Prodfin) {
        this.d2Prodfin = d2Prodfin;
    }

    public String getD2Revisao() {
        return d2Revisao;
    }

    public void setD2Revisao(String d2Revisao) {
        this.d2Revisao = d2Revisao;
    }

    public double getD2Bsrein() {
        return d2Bsrein;
    }

    public void setD2Bsrein(double d2Bsrein) {
        this.d2Bsrein = d2Bsrein;
    }

    public double getD2Vreint() {
        return d2Vreint;
    }

    public void setD2Vreint(double d2Vreint) {
        this.d2Vreint = d2Vreint;
    }

    public double getD2Totimp() {
        return d2Totimp;
    }

    public void setD2Totimp(double d2Totimp) {
        this.d2Totimp = d2Totimp;
    }

    public String getD2Idcfc() {
        return d2Idcfc;
    }

    public void setD2Idcfc(String d2Idcfc) {
        this.d2Idcfc = d2Idcfc;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sd2010)) {
            return false;
        }
        Sd2010 other = (Sd2010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Sd2010[ rECNO=" + rECNO + " ]";
    }
}
