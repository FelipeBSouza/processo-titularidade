/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Robson
 */
@Entity
@Table(name = "SUA010")
public class Sua010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "UA_FILIAL")
    private String uaFilial;
    @Basic(optional = false)
    @Column(name = "UA_NUM")
    private String uaNum;
    @Basic(optional = false)
    @Column(name = "UA_CLIENTE")
    private String uaCliente;
    @Basic(optional = false)
    @Column(name = "UA_LOJA")
    private String uaLoja;
    @Basic(optional = false)
    @Column(name = "UA_CODCONT")
    private String uaCodcont;
    @Basic(optional = false)
    @Column(name = "UA_DESCNT")
    private String uaDescnt;
    @Basic(optional = false)
    @Column(name = "UA_OPERADO")
    private String uaOperado;
    @Basic(optional = false)
    @Column(name = "UA_CONDPG")
    private String uaCondpg;
    @Basic(optional = false)
    @Column(name = "UA_TABELA")
    private String uaTabela;
    @Basic(optional = false)
    @Column(name = "UA_OPER")
    private String uaOper;
    @Basic(optional = false)
    @Column(name = "UA_MIDIA")
    private String uaMidia;
    @Basic(optional = false)
    @Column(name = "UA_VEND")
    private String uaVend;
    @Basic(optional = false)
    @Column(name = "UA_TMK")
    private String uaTmk;
    @Basic(optional = false)
    @Column(name = "UA_EMISSAO")
    private String uaEmissao;
    @Basic(optional = false)
    @Column(name = "UA_CODCAMP")
    private String uaCodcamp;
    @Basic(optional = false)
    @Column(name = "UA_CODLIG")
    private String uaCodlig;
    @Basic(optional = false)
    @Column(name = "UA_PROXLIG")
    private String uaProxlig;
    @Basic(optional = false)
    @Column(name = "UA_HRPEND")
    private String uaHrpend;
    @Basic(optional = false)
    @Column(name = "UA_CODOBS")
    private String uaCodobs;
    @Basic(optional = false)
    @Column(name = "UA_VALICM")
    private double uaValicm;
    @Basic(optional = false)
    @Column(name = "UA_VALISS")
    private double uaValiss;
    @Basic(optional = false)
    @Column(name = "UA_VALIPI")
    private double uaValipi;
    @Basic(optional = false)
    @Column(name = "UA_FORMPG")
    private String uaFormpg;
    @Basic(optional = false)
    @Column(name = "UA_FRETE")
    private double uaFrete;
    @Basic(optional = false)
    @Column(name = "UA_DESPESA")
    private double uaDespesa;
    @Basic(optional = false)
    @Column(name = "UA_COMIS")
    private double uaComis;
    @Basic(optional = false)
    @Column(name = "UA_INICIO")
    private String uaInicio;
    @Basic(optional = false)
    @Column(name = "UA_FIM")
    private String uaFim;
    @Basic(optional = false)
    @Column(name = "UA_CREDITO")
    private double uaCredito;
    @Basic(optional = false)
    @Column(name = "UA_STATUS")
    private String uaStatus;
    @Basic(optional = false)
    @Column(name = "UA_CANC")
    private String uaCanc;
    @Basic(optional = false)
    @Column(name = "UA_CODCANC")
    private String uaCodcanc;
    @Basic(optional = false)
    @Column(name = "UA_ENDCOB")
    private String uaEndcob;
    @Basic(optional = false)
    @Column(name = "UA_BAIRROC")
    private String uaBairroc;
    @Basic(optional = false)
    @Column(name = "UA_ENDENT")
    private String uaEndent;
    @Basic(optional = false)
    @Column(name = "UA_BAIRROE")
    private String uaBairroe;
    @Basic(optional = false)
    @Column(name = "UA_CEPC")
    private String uaCepc;
    @Basic(optional = false)
    @Column(name = "UA_CEPE")
    private String uaCepe;
    @Basic(optional = false)
    @Column(name = "UA_ESTC")
    private String uaEstc;
    @Basic(optional = false)
    @Column(name = "UA_ESTE")
    private String uaEste;
    @Basic(optional = false)
    @Column(name = "UA_MUNC")
    private String uaMunc;
    @Basic(optional = false)
    @Column(name = "UA_MUNE")
    private String uaMune;
    @Basic(optional = false)
    @Column(name = "UA_TRANSP")
    private String uaTransp;
    @Basic(optional = false)
    @Column(name = "UA_VENDTEF")
    private String uaVendtef;
    @Basic(optional = false)
    @Column(name = "UA_DATATEF")
    private String uaDatatef;
    @Basic(optional = false)
    @Column(name = "UA_HORATEF")
    private String uaHoratef;
    @Basic(optional = false)
    @Column(name = "UA_DOCTEF")
    private String uaDoctef;
    @Basic(optional = false)
    @Column(name = "UA_AUTORIZ")
    private String uaAutoriz;
    @Basic(optional = false)
    @Column(name = "UA_DOCCANC")
    private String uaDoccanc;
    @Basic(optional = false)
    @Column(name = "UA_DATCANC")
    private String uaDatcanc;
    @Basic(optional = false)
    @Column(name = "UA_HORCANC")
    private String uaHorcanc;
    @Basic(optional = false)
    @Column(name = "UA_INSTITU")
    private String uaInstitu;
    @Basic(optional = false)
    @Column(name = "UA_NSUTEF")
    private String uaNsutef;
    @Basic(optional = false)
    @Column(name = "UA_TIPCART")
    private String uaTipcart;
    @Basic(optional = false)
    @Column(name = "UA_CARTAO")
    private double uaCartao;
    @Basic(optional = false)
    @Column(name = "UA_NUMSL1")
    private String uaNumsl1;
    @Basic(optional = false)
    @Column(name = "UA_NUMSC5")
    private String uaNumsc5;
    @Basic(optional = false)
    @Column(name = "UA_ACRECND")
    private double uaAcrecnd;
    @Basic(optional = false)
    @Column(name = "UA_DIASDAT")
    private double uaDiasdat;
    @Basic(optional = false)
    @Column(name = "UA_PROSPEC")
    private String uaProspec;
    @Basic(optional = false)
    @Column(name = "UA_HORADAT")
    private double uaHoradat;
    @Basic(optional = false)
    @Column(name = "UA_EMISNF")
    private String uaEmisnf;
    @Basic(optional = false)
    @Column(name = "UA_VALBRUT")
    private double uaValbrut;
    @Basic(optional = false)
    @Column(name = "UA_DTLIM")
    private String uaDtlim;
    @Basic(optional = false)
    @Column(name = "UA_VALMERC")
    private double uaValmerc;
    @Basic(optional = false)
    @Column(name = "UA_VLRLIQ")
    private double uaVlrliq;
    @Basic(optional = false)
    @Column(name = "UA_TXDESC")
    private double uaTxdesc;
    @Basic(optional = false)
    @Column(name = "UA_JUROS")
    private double uaJuros;
    @Basic(optional = false)
    @Column(name = "UA_ENTRADA")
    private double uaEntrada;
    @Basic(optional = false)
    @Column(name = "UA_DESCONT")
    private double uaDescont;
    @Basic(optional = false)
    @Column(name = "UA_PARCELA")
    private double uaParcela;
    @Basic(optional = false)
    @Column(name = "UA_FINANC")
    private double uaFinanc;
    @Basic(optional = false)
    @Column(name = "UA_ACRESCI")
    private double uaAcresci;
    @Basic(optional = false)
    @Column(name = "UA_SERIE")
    private String uaSerie;
    @Basic(optional = false)
    @Column(name = "UA_DOC")
    private String uaDoc;
    @Basic(optional = false)
    @Column(name = "UA_MOEDA")
    private double uaMoeda;
    @Basic(optional = false)
    @Column(name = "UA_TPCARGA")
    private String uaTpcarga;
    @Basic(optional = false)
    @Column(name = "UA_PDESCAB")
    private double uaPdescab;
    @Basic(optional = false)
    @Column(name = "UA_TPFRETE")
    private String uaTpfrete;
    @Basic(optional = false)
    @Column(name = "UA_DOCGER")
    private String uaDocger;
    @Basic(optional = false)
    @Column(name = "UA_DESC1")
    private double uaDesc1;
    @Basic(optional = false)
    @Column(name = "UA_DESC2")
    private double uaDesc2;
    @Basic(optional = false)
    @Column(name = "UA_DESC3")
    private double uaDesc3;
    @Basic(optional = false)
    @Column(name = "UA_DESC4")
    private double uaDesc4;
    @Basic(optional = false)
    @Column(name = "UA_CONTRA")
    private String uaContra;
    @Basic(optional = false)
    @Column(name = "UA_CGCCART")
    private String uaCgccart;
    @Basic(optional = false)
    @Column(name = "UA_FORCADA")
    private String uaForcada;
    @Basic(optional = false)
    @Column(name = "UA_TIPLIB")
    private String uaTiplib;
    @Basic(optional = false)
    @Column(name = "UA_TIPOCLI")
    private String uaTipocli;
    @Basic(optional = false)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @Basic(optional = false)
    @Column(name = "UA_UTIPO")
    private String uaUtipo;
    @Basic(optional = false)
    @Column(name = "UA_UATEND")
    private String uaUatend;
    @Basic(optional = false)
    @Column(name = "UA_UCCANT")
    private String uaUccant;
    @Basic(optional = false)
    @Column(name = "UA_UPRAZ")
    private double uaUpraz;
    @Basic(optional = false)
    @Column(name = "UA_UVCTR")
    private double uaUvctr;
    @Basic(optional = false)
    @Column(name = "UA_UCEP")
    private String uaUcep;
    @Basic(optional = false)
    @Column(name = "UA_UEND")
    private String uaUend;
    @Basic(optional = false)
    @Column(name = "UA_UNUM")
    private String uaUnum;
    @Basic(optional = false)
    @Column(name = "UA_UBAIRRO")
    private String uaUbairro;
    @Basic(optional = false)
    @Column(name = "UA_UMUN")
    private String uaUmun;
    @Basic(optional = false)
    @Column(name = "UA_UCOMPLE")
    private String uaUcomple;
    @Basic(optional = false)
    @Column(name = "UA_UEST")
    private String uaUest;
    @Basic(optional = false)
    @Column(name = "UA_UCOD_MU")
    private String uaUcodMu;
    @Basic(optional = false)
    @Column(name = "UA_UIDAGA")
    private String uaUidaga;
    @Basic(optional = false)
    @Column(name = "UA_UDIAFE")
    private double uaUdiafe;
    @Basic(optional = false)
    @Column(name = "UA_UNUMCTR")
    private String uaUnumctr;
    @Basic(optional = false)
    @Column(name = "UA_UCAIXA")
    private String uaUcaixa;
    @Basic(optional = false)
    @Column(name = "UA_UCLIASS")
    private String uaUcliass;
    @Basic(optional = false)
    @Column(name = "UA_SDOC")
    private String uaSdoc;
    @Basic(optional = false)
    @Column(name = "UA_UFADIG")
    private String uaUfadig;
    @Basic(optional = false)
    @Column(name = "UA_UCLFLUI")
    private String uaUclflui;
    @Basic(optional = false)
    @Column(name = "UA_URLGRV")
    private String uaUrlgrv;

    public Sua010() {
    }

    public Sua010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Sua010(Integer rECNO, String uaFilial, String uaNum, String uaCliente, String uaLoja, String uaCodcont, String uaDescnt, String uaOperado, String uaCondpg, String uaTabela, String uaOper, String uaMidia, String uaVend, String uaTmk, String uaEmissao, String uaCodcamp, String uaCodlig, String uaProxlig, String uaHrpend, String uaCodobs, double uaValicm, double uaValiss, double uaValipi, String uaFormpg, double uaFrete, double uaDespesa, double uaComis, String uaInicio, String uaFim, double uaCredito, String uaStatus, String uaCanc, String uaCodcanc, String uaEndcob, String uaBairroc, String uaEndent, String uaBairroe, String uaCepc, String uaCepe, String uaEstc, String uaEste, String uaMunc, String uaMune, String uaTransp, String uaVendtef, String uaDatatef, String uaHoratef, String uaDoctef, String uaAutoriz, String uaDoccanc, String uaDatcanc, String uaHorcanc, String uaInstitu, String uaNsutef, String uaTipcart, double uaCartao, String uaNumsl1, String uaNumsc5, double uaAcrecnd, double uaDiasdat, String uaProspec, double uaHoradat, String uaEmisnf, double uaValbrut, String uaDtlim, double uaValmerc, double uaVlrliq, double uaTxdesc, double uaJuros, double uaEntrada, double uaDescont, double uaParcela, double uaFinanc, double uaAcresci, String uaSerie, String uaDoc, double uaMoeda, String uaTpcarga, double uaPdescab, String uaTpfrete, String uaDocger, double uaDesc1, double uaDesc2, double uaDesc3, double uaDesc4, String uaContra, String uaCgccart, String uaForcada, String uaTiplib, String uaTipocli, String dELET, int rECDEL, String uaUtipo, String uaUatend, String uaUccant, double uaUpraz, double uaUvctr, String uaUcep, String uaUend, String uaUnum, String uaUbairro, String uaUmun, String uaUcomple, String uaUest, String uaUcodMu, String uaUidaga, double uaUdiafe, String uaUnumctr, String uaUcaixa, String uaUcliass, String uaSdoc, String uaUfadig, String uaUclflui, String uaUrlgrv) {
        this.rECNO = rECNO;
        this.uaFilial = uaFilial;
        this.uaNum = uaNum;
        this.uaCliente = uaCliente;
        this.uaLoja = uaLoja;
        this.uaCodcont = uaCodcont;
        this.uaDescnt = uaDescnt;
        this.uaOperado = uaOperado;
        this.uaCondpg = uaCondpg;
        this.uaTabela = uaTabela;
        this.uaOper = uaOper;
        this.uaMidia = uaMidia;
        this.uaVend = uaVend;
        this.uaTmk = uaTmk;
        this.uaEmissao = uaEmissao;
        this.uaCodcamp = uaCodcamp;
        this.uaCodlig = uaCodlig;
        this.uaProxlig = uaProxlig;
        this.uaHrpend = uaHrpend;
        this.uaCodobs = uaCodobs;
        this.uaValicm = uaValicm;
        this.uaValiss = uaValiss;
        this.uaValipi = uaValipi;
        this.uaFormpg = uaFormpg;
        this.uaFrete = uaFrete;
        this.uaDespesa = uaDespesa;
        this.uaComis = uaComis;
        this.uaInicio = uaInicio;
        this.uaFim = uaFim;
        this.uaCredito = uaCredito;
        this.uaStatus = uaStatus;
        this.uaCanc = uaCanc;
        this.uaCodcanc = uaCodcanc;
        this.uaEndcob = uaEndcob;
        this.uaBairroc = uaBairroc;
        this.uaEndent = uaEndent;
        this.uaBairroe = uaBairroe;
        this.uaCepc = uaCepc;
        this.uaCepe = uaCepe;
        this.uaEstc = uaEstc;
        this.uaEste = uaEste;
        this.uaMunc = uaMunc;
        this.uaMune = uaMune;
        this.uaTransp = uaTransp;
        this.uaVendtef = uaVendtef;
        this.uaDatatef = uaDatatef;
        this.uaHoratef = uaHoratef;
        this.uaDoctef = uaDoctef;
        this.uaAutoriz = uaAutoriz;
        this.uaDoccanc = uaDoccanc;
        this.uaDatcanc = uaDatcanc;
        this.uaHorcanc = uaHorcanc;
        this.uaInstitu = uaInstitu;
        this.uaNsutef = uaNsutef;
        this.uaTipcart = uaTipcart;
        this.uaCartao = uaCartao;
        this.uaNumsl1 = uaNumsl1;
        this.uaNumsc5 = uaNumsc5;
        this.uaAcrecnd = uaAcrecnd;
        this.uaDiasdat = uaDiasdat;
        this.uaProspec = uaProspec;
        this.uaHoradat = uaHoradat;
        this.uaEmisnf = uaEmisnf;
        this.uaValbrut = uaValbrut;
        this.uaDtlim = uaDtlim;
        this.uaValmerc = uaValmerc;
        this.uaVlrliq = uaVlrliq;
        this.uaTxdesc = uaTxdesc;
        this.uaJuros = uaJuros;
        this.uaEntrada = uaEntrada;
        this.uaDescont = uaDescont;
        this.uaParcela = uaParcela;
        this.uaFinanc = uaFinanc;
        this.uaAcresci = uaAcresci;
        this.uaSerie = uaSerie;
        this.uaDoc = uaDoc;
        this.uaMoeda = uaMoeda;
        this.uaTpcarga = uaTpcarga;
        this.uaPdescab = uaPdescab;
        this.uaTpfrete = uaTpfrete;
        this.uaDocger = uaDocger;
        this.uaDesc1 = uaDesc1;
        this.uaDesc2 = uaDesc2;
        this.uaDesc3 = uaDesc3;
        this.uaDesc4 = uaDesc4;
        this.uaContra = uaContra;
        this.uaCgccart = uaCgccart;
        this.uaForcada = uaForcada;
        this.uaTiplib = uaTiplib;
        this.uaTipocli = uaTipocli;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
        this.uaUtipo = uaUtipo;
        this.uaUatend = uaUatend;
        this.uaUccant = uaUccant;
        this.uaUpraz = uaUpraz;
        this.uaUvctr = uaUvctr;
        this.uaUcep = uaUcep;
        this.uaUend = uaUend;
        this.uaUnum = uaUnum;
        this.uaUbairro = uaUbairro;
        this.uaUmun = uaUmun;
        this.uaUcomple = uaUcomple;
        this.uaUest = uaUest;
        this.uaUcodMu = uaUcodMu;
        this.uaUidaga = uaUidaga;
        this.uaUdiafe = uaUdiafe;
        this.uaUnumctr = uaUnumctr;
        this.uaUcaixa = uaUcaixa;
        this.uaUcliass = uaUcliass;
        this.uaSdoc = uaSdoc;
        this.uaUfadig = uaUfadig;
        this.uaUclflui = uaUclflui;
        this.uaUrlgrv = uaUrlgrv;
    }

    public String getUaFilial() {
        return uaFilial;
    }

    public void setUaFilial(String uaFilial) {
        this.uaFilial = uaFilial;
    }

    public String getUaNum() {
        return uaNum;
    }

    public void setUaNum(String uaNum) {
        this.uaNum = uaNum;
    }

    public String getUaCliente() {
        return uaCliente;
    }

    public void setUaCliente(String uaCliente) {
        this.uaCliente = uaCliente;
    }

    public String getUaLoja() {
        return uaLoja;
    }

    public void setUaLoja(String uaLoja) {
        this.uaLoja = uaLoja;
    }

    public String getUaCodcont() {
        return uaCodcont;
    }

    public void setUaCodcont(String uaCodcont) {
        this.uaCodcont = uaCodcont;
    }

    public String getUaDescnt() {
        return uaDescnt;
    }

    public void setUaDescnt(String uaDescnt) {
        this.uaDescnt = uaDescnt;
    }

    public String getUaOperado() {
        return uaOperado;
    }

    public void setUaOperado(String uaOperado) {
        this.uaOperado = uaOperado;
    }

    public String getUaCondpg() {
        return uaCondpg;
    }

    public void setUaCondpg(String uaCondpg) {
        this.uaCondpg = uaCondpg;
    }

    public String getUaTabela() {
        return uaTabela;
    }

    public void setUaTabela(String uaTabela) {
        this.uaTabela = uaTabela;
    }

    public String getUaOper() {
        return uaOper;
    }

    public void setUaOper(String uaOper) {
        this.uaOper = uaOper;
    }

    public String getUaMidia() {
        return uaMidia;
    }

    public void setUaMidia(String uaMidia) {
        this.uaMidia = uaMidia;
    }

    public String getUaVend() {
        return uaVend;
    }

    public void setUaVend(String uaVend) {
        this.uaVend = uaVend;
    }

    public String getUaTmk() {
        return uaTmk;
    }

    public void setUaTmk(String uaTmk) {
        this.uaTmk = uaTmk;
    }

    public String getUaEmissao() {
        return uaEmissao;
    }

    public void setUaEmissao(String uaEmissao) {
        this.uaEmissao = uaEmissao;
    }

    public String getUaCodcamp() {
        return uaCodcamp;
    }

    public void setUaCodcamp(String uaCodcamp) {
        this.uaCodcamp = uaCodcamp;
    }

    public String getUaCodlig() {
        return uaCodlig;
    }

    public void setUaCodlig(String uaCodlig) {
        this.uaCodlig = uaCodlig;
    }

    public String getUaProxlig() {
        return uaProxlig;
    }

    public void setUaProxlig(String uaProxlig) {
        this.uaProxlig = uaProxlig;
    }

    public String getUaHrpend() {
        return uaHrpend;
    }

    public void setUaHrpend(String uaHrpend) {
        this.uaHrpend = uaHrpend;
    }

    public String getUaCodobs() {
        return uaCodobs;
    }

    public void setUaCodobs(String uaCodobs) {
        this.uaCodobs = uaCodobs;
    }

    public double getUaValicm() {
        return uaValicm;
    }

    public void setUaValicm(double uaValicm) {
        this.uaValicm = uaValicm;
    }

    public double getUaValiss() {
        return uaValiss;
    }

    public void setUaValiss(double uaValiss) {
        this.uaValiss = uaValiss;
    }

    public double getUaValipi() {
        return uaValipi;
    }

    public void setUaValipi(double uaValipi) {
        this.uaValipi = uaValipi;
    }

    public String getUaFormpg() {
        return uaFormpg;
    }

    public void setUaFormpg(String uaFormpg) {
        this.uaFormpg = uaFormpg;
    }

    public double getUaFrete() {
        return uaFrete;
    }

    public void setUaFrete(double uaFrete) {
        this.uaFrete = uaFrete;
    }

    public double getUaDespesa() {
        return uaDespesa;
    }

    public void setUaDespesa(double uaDespesa) {
        this.uaDespesa = uaDespesa;
    }

    public double getUaComis() {
        return uaComis;
    }

    public void setUaComis(double uaComis) {
        this.uaComis = uaComis;
    }

    public String getUaInicio() {
        return uaInicio;
    }

    public void setUaInicio(String uaInicio) {
        this.uaInicio = uaInicio;
    }

    public String getUaFim() {
        return uaFim;
    }

    public void setUaFim(String uaFim) {
        this.uaFim = uaFim;
    }

    public double getUaCredito() {
        return uaCredito;
    }

    public void setUaCredito(double uaCredito) {
        this.uaCredito = uaCredito;
    }

    public String getUaStatus() {
        return uaStatus;
    }

    public void setUaStatus(String uaStatus) {
        this.uaStatus = uaStatus;
    }

    public String getUaCanc() {
        return uaCanc;
    }

    public void setUaCanc(String uaCanc) {
        this.uaCanc = uaCanc;
    }

    public String getUaCodcanc() {
        return uaCodcanc;
    }

    public void setUaCodcanc(String uaCodcanc) {
        this.uaCodcanc = uaCodcanc;
    }

    public String getUaEndcob() {
        return uaEndcob;
    }

    public void setUaEndcob(String uaEndcob) {
        this.uaEndcob = uaEndcob;
    }

    public String getUaBairroc() {
        return uaBairroc;
    }

    public void setUaBairroc(String uaBairroc) {
        this.uaBairroc = uaBairroc;
    }

    public String getUaEndent() {
        return uaEndent;
    }

    public void setUaEndent(String uaEndent) {
        this.uaEndent = uaEndent;
    }

    public String getUaBairroe() {
        return uaBairroe;
    }

    public void setUaBairroe(String uaBairroe) {
        this.uaBairroe = uaBairroe;
    }

    public String getUaCepc() {
        return uaCepc;
    }

    public void setUaCepc(String uaCepc) {
        this.uaCepc = uaCepc;
    }

    public String getUaCepe() {
        return uaCepe;
    }

    public void setUaCepe(String uaCepe) {
        this.uaCepe = uaCepe;
    }

    public String getUaEstc() {
        return uaEstc;
    }

    public void setUaEstc(String uaEstc) {
        this.uaEstc = uaEstc;
    }

    public String getUaEste() {
        return uaEste;
    }

    public void setUaEste(String uaEste) {
        this.uaEste = uaEste;
    }

    public String getUaMunc() {
        return uaMunc;
    }

    public void setUaMunc(String uaMunc) {
        this.uaMunc = uaMunc;
    }

    public String getUaMune() {
        return uaMune;
    }

    public void setUaMune(String uaMune) {
        this.uaMune = uaMune;
    }

    public String getUaTransp() {
        return uaTransp;
    }

    public void setUaTransp(String uaTransp) {
        this.uaTransp = uaTransp;
    }

    public String getUaVendtef() {
        return uaVendtef;
    }

    public void setUaVendtef(String uaVendtef) {
        this.uaVendtef = uaVendtef;
    }

    public String getUaDatatef() {
        return uaDatatef;
    }

    public void setUaDatatef(String uaDatatef) {
        this.uaDatatef = uaDatatef;
    }

    public String getUaHoratef() {
        return uaHoratef;
    }

    public void setUaHoratef(String uaHoratef) {
        this.uaHoratef = uaHoratef;
    }

    public String getUaDoctef() {
        return uaDoctef;
    }

    public void setUaDoctef(String uaDoctef) {
        this.uaDoctef = uaDoctef;
    }

    public String getUaAutoriz() {
        return uaAutoriz;
    }

    public void setUaAutoriz(String uaAutoriz) {
        this.uaAutoriz = uaAutoriz;
    }

    public String getUaDoccanc() {
        return uaDoccanc;
    }

    public void setUaDoccanc(String uaDoccanc) {
        this.uaDoccanc = uaDoccanc;
    }

    public String getUaDatcanc() {
        return uaDatcanc;
    }

    public void setUaDatcanc(String uaDatcanc) {
        this.uaDatcanc = uaDatcanc;
    }

    public String getUaHorcanc() {
        return uaHorcanc;
    }

    public void setUaHorcanc(String uaHorcanc) {
        this.uaHorcanc = uaHorcanc;
    }

    public String getUaInstitu() {
        return uaInstitu;
    }

    public void setUaInstitu(String uaInstitu) {
        this.uaInstitu = uaInstitu;
    }

    public String getUaNsutef() {
        return uaNsutef;
    }

    public void setUaNsutef(String uaNsutef) {
        this.uaNsutef = uaNsutef;
    }

    public String getUaTipcart() {
        return uaTipcart;
    }

    public void setUaTipcart(String uaTipcart) {
        this.uaTipcart = uaTipcart;
    }

    public double getUaCartao() {
        return uaCartao;
    }

    public void setUaCartao(double uaCartao) {
        this.uaCartao = uaCartao;
    }

    public String getUaNumsl1() {
        return uaNumsl1;
    }

    public void setUaNumsl1(String uaNumsl1) {
        this.uaNumsl1 = uaNumsl1;
    }

    public String getUaNumsc5() {
        return uaNumsc5;
    }

    public void setUaNumsc5(String uaNumsc5) {
        this.uaNumsc5 = uaNumsc5;
    }

    public double getUaAcrecnd() {
        return uaAcrecnd;
    }

    public void setUaAcrecnd(double uaAcrecnd) {
        this.uaAcrecnd = uaAcrecnd;
    }

    public double getUaDiasdat() {
        return uaDiasdat;
    }

    public void setUaDiasdat(double uaDiasdat) {
        this.uaDiasdat = uaDiasdat;
    }

    public String getUaProspec() {
        return uaProspec;
    }

    public void setUaProspec(String uaProspec) {
        this.uaProspec = uaProspec;
    }

    public double getUaHoradat() {
        return uaHoradat;
    }

    public void setUaHoradat(double uaHoradat) {
        this.uaHoradat = uaHoradat;
    }

    public String getUaEmisnf() {
        return uaEmisnf;
    }

    public void setUaEmisnf(String uaEmisnf) {
        this.uaEmisnf = uaEmisnf;
    }

    public double getUaValbrut() {
        return uaValbrut;
    }

    public void setUaValbrut(double uaValbrut) {
        this.uaValbrut = uaValbrut;
    }

    public String getUaDtlim() {
        return uaDtlim;
    }

    public void setUaDtlim(String uaDtlim) {
        this.uaDtlim = uaDtlim;
    }

    public double getUaValmerc() {
        return uaValmerc;
    }

    public void setUaValmerc(double uaValmerc) {
        this.uaValmerc = uaValmerc;
    }

    public double getUaVlrliq() {
        return uaVlrliq;
    }

    public void setUaVlrliq(double uaVlrliq) {
        this.uaVlrliq = uaVlrliq;
    }

    public double getUaTxdesc() {
        return uaTxdesc;
    }

    public void setUaTxdesc(double uaTxdesc) {
        this.uaTxdesc = uaTxdesc;
    }

    public double getUaJuros() {
        return uaJuros;
    }

    public void setUaJuros(double uaJuros) {
        this.uaJuros = uaJuros;
    }

    public double getUaEntrada() {
        return uaEntrada;
    }

    public void setUaEntrada(double uaEntrada) {
        this.uaEntrada = uaEntrada;
    }

    public double getUaDescont() {
        return uaDescont;
    }

    public void setUaDescont(double uaDescont) {
        this.uaDescont = uaDescont;
    }

    public double getUaParcela() {
        return uaParcela;
    }

    public void setUaParcela(double uaParcela) {
        this.uaParcela = uaParcela;
    }

    public double getUaFinanc() {
        return uaFinanc;
    }

    public void setUaFinanc(double uaFinanc) {
        this.uaFinanc = uaFinanc;
    }

    public double getUaAcresci() {
        return uaAcresci;
    }

    public void setUaAcresci(double uaAcresci) {
        this.uaAcresci = uaAcresci;
    }

    public String getUaSerie() {
        return uaSerie;
    }

    public void setUaSerie(String uaSerie) {
        this.uaSerie = uaSerie;
    }

    public String getUaDoc() {
        return uaDoc;
    }

    public void setUaDoc(String uaDoc) {
        this.uaDoc = uaDoc;
    }

    public double getUaMoeda() {
        return uaMoeda;
    }

    public void setUaMoeda(double uaMoeda) {
        this.uaMoeda = uaMoeda;
    }

    public String getUaTpcarga() {
        return uaTpcarga;
    }

    public void setUaTpcarga(String uaTpcarga) {
        this.uaTpcarga = uaTpcarga;
    }

    public double getUaPdescab() {
        return uaPdescab;
    }

    public void setUaPdescab(double uaPdescab) {
        this.uaPdescab = uaPdescab;
    }

    public String getUaTpfrete() {
        return uaTpfrete;
    }

    public void setUaTpfrete(String uaTpfrete) {
        this.uaTpfrete = uaTpfrete;
    }

    public String getUaDocger() {
        return uaDocger;
    }

    public void setUaDocger(String uaDocger) {
        this.uaDocger = uaDocger;
    }

    public double getUaDesc1() {
        return uaDesc1;
    }

    public void setUaDesc1(double uaDesc1) {
        this.uaDesc1 = uaDesc1;
    }

    public double getUaDesc2() {
        return uaDesc2;
    }

    public void setUaDesc2(double uaDesc2) {
        this.uaDesc2 = uaDesc2;
    }

    public double getUaDesc3() {
        return uaDesc3;
    }

    public void setUaDesc3(double uaDesc3) {
        this.uaDesc3 = uaDesc3;
    }

    public double getUaDesc4() {
        return uaDesc4;
    }

    public void setUaDesc4(double uaDesc4) {
        this.uaDesc4 = uaDesc4;
    }

    public String getUaContra() {
        return uaContra;
    }

    public void setUaContra(String uaContra) {
        this.uaContra = uaContra;
    }

    public String getUaCgccart() {
        return uaCgccart;
    }

    public void setUaCgccart(String uaCgccart) {
        this.uaCgccart = uaCgccart;
    }

    public String getUaForcada() {
        return uaForcada;
    }

    public void setUaForcada(String uaForcada) {
        this.uaForcada = uaForcada;
    }

    public String getUaTiplib() {
        return uaTiplib;
    }

    public void setUaTiplib(String uaTiplib) {
        this.uaTiplib = uaTiplib;
    }

    public String getUaTipocli() {
        return uaTipocli;
    }

    public void setUaTipocli(String uaTipocli) {
        this.uaTipocli = uaTipocli;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getUaUtipo() {
        return uaUtipo;
    }

    public void setUaUtipo(String uaUtipo) {
        this.uaUtipo = uaUtipo;
    }

    public String getUaUatend() {
        return uaUatend;
    }

    public void setUaUatend(String uaUatend) {
        this.uaUatend = uaUatend;
    }

    public String getUaUccant() {
        return uaUccant;
    }

    public void setUaUccant(String uaUccant) {
        this.uaUccant = uaUccant;
    }

    public double getUaUpraz() {
        return uaUpraz;
    }

    public void setUaUpraz(double uaUpraz) {
        this.uaUpraz = uaUpraz;
    }

    public double getUaUvctr() {
        return uaUvctr;
    }

    public void setUaUvctr(double uaUvctr) {
        this.uaUvctr = uaUvctr;
    }

    public String getUaUcep() {
        return uaUcep;
    }

    public void setUaUcep(String uaUcep) {
        this.uaUcep = uaUcep;
    }

    public String getUaUend() {
        return uaUend;
    }

    public void setUaUend(String uaUend) {
        this.uaUend = uaUend;
    }

    public String getUaUnum() {
        return uaUnum;
    }

    public void setUaUnum(String uaUnum) {
        this.uaUnum = uaUnum;
    }

    public String getUaUbairro() {
        return uaUbairro;
    }

    public void setUaUbairro(String uaUbairro) {
        this.uaUbairro = uaUbairro;
    }

    public String getUaUmun() {
        return uaUmun;
    }

    public void setUaUmun(String uaUmun) {
        this.uaUmun = uaUmun;
    }

    public String getUaUcomple() {
        return uaUcomple;
    }

    public void setUaUcomple(String uaUcomple) {
        this.uaUcomple = uaUcomple;
    }

    public String getUaUest() {
        return uaUest;
    }

    public void setUaUest(String uaUest) {
        this.uaUest = uaUest;
    }

    public String getUaUcodMu() {
        return uaUcodMu;
    }

    public void setUaUcodMu(String uaUcodMu) {
        this.uaUcodMu = uaUcodMu;
    }

    public String getUaUidaga() {
        return uaUidaga;
    }

    public void setUaUidaga(String uaUidaga) {
        this.uaUidaga = uaUidaga;
    }

    public double getUaUdiafe() {
        return uaUdiafe;
    }

    public void setUaUdiafe(double uaUdiafe) {
        this.uaUdiafe = uaUdiafe;
    }

    public String getUaUnumctr() {
        return uaUnumctr;
    }

    public void setUaUnumctr(String uaUnumctr) {
        this.uaUnumctr = uaUnumctr;
    }

    public String getUaUcaixa() {
        return uaUcaixa;
    }

    public void setUaUcaixa(String uaUcaixa) {
        this.uaUcaixa = uaUcaixa;
    }

    public String getUaUcliass() {
        return uaUcliass;
    }

    public void setUaUcliass(String uaUcliass) {
        this.uaUcliass = uaUcliass;
    }

    public String getUaSdoc() {
        return uaSdoc;
    }

    public void setUaSdoc(String uaSdoc) {
        this.uaSdoc = uaSdoc;
    }

    public String getUaUfadig() {
        return uaUfadig;
    }

    public void setUaUfadig(String uaUfadig) {
        this.uaUfadig = uaUfadig;
    }

    public String getUaUclflui() {
        return uaUclflui;
    }

    public void setUaUclflui(String uaUclflui) {
        this.uaUclflui = uaUclflui;
    }

    public String getUaUrlgrv() {
        return uaUrlgrv;
    }

    public void setUaUrlgrv(String uaUrlgrv) {
        this.uaUrlgrv = uaUrlgrv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sua010)) {
            return false;
        }
        Sua010 other = (Sua010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gerador.Sua010[ rECNO=" + rECNO + " ]";
    }
    
}
