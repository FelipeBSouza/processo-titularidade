/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(name = "SE1010")
public class Se1010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_FILIAL")
    private String e1Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PREFIXO")
    private String e1Prefixo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_NUM")
    private String e1Num;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PARCELA")
    private String e1Parcela;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_TIPO")
    private String e1Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_NATUREZ")
    private String e1Naturez;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "E1_PORTADO", referencedColumnName = "EE_CODIGO"),
        @JoinColumn(name = "E1_AGEDEP", referencedColumnName = "EE_AGENCIA"),
        @JoinColumn(name = "E1_CONTA", referencedColumnName = "EE_CONTA")})

    private See010 see010;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "E1_CLIENTE", referencedColumnName = "A1_COD", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "E1_LOJA", referencedColumnName = "A1_LOJA", nullable = false, insertable = false, updatable = false),})
    private Sa1010 sa1010;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "E1_NOMCLI")
    private String e1Nomcli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_EMISSAO")
    private String e1Emissao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_VENCTO")
    private String e1Vencto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_VENCREA")
    private String e1Vencrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALOR")
    private double e1Valor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASEIRF")
    private double e1Baseirf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_IRRF")
    private double e1Irrf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_ISS")
    private double e1Iss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "E1_NUMBCO")
    private String e1Numbco;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_INDICE")
    private String e1Indice;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_BAIXA")
    private String e1Baixa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_NUMBOR")
    private String e1Numbor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DATABOR")
    private String e1Databor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_EMIS1")
    private String e1Emis1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "E1_HIST")
    private String e1Hist;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_LA")
    private String e1La;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_LOTE")
    private String e1Lote;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "E1_MOTIVO")
    private String e1Motivo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_MOVIMEN")
    private String e1Movimen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "E1_OP")
    private String e1Op;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_SITUACA")
    private String e1Situaca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "E1_CONTRAT")
    private String e1Contrat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SALDO")
    private double e1Saldo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_SUPERVI")
    private String e1Supervi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_VEND1")
    private String e1Vend1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_VEND2")
    private String e1Vend2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_VEND3")
    private String e1Vend3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_VEND4")
    private String e1Vend4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_VEND5")
    private String e1Vend5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_COMIS1")
    private double e1Comis1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_COMIS2")
    private double e1Comis2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_COMIS3")
    private double e1Comis3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_COMIS4")
    private double e1Comis4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_DESCONT")
    private double e1Descont;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_COMIS5")
    private double e1Comis5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_MULTA")
    private double e1Multa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_JUROS")
    private double e1Juros;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_CORREC")
    private double e1Correc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALLIQ")
    private double e1Valliq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_VENCORI")
    private String e1Vencori;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALJUR")
    private double e1Valjur;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_PORCJUR")
    private double e1Porcjur;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_MOEDA")
    private double e1Moeda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASCOM1")
    private double e1Bascom1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASCOM2")
    private double e1Bascom2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASCOM3")
    private double e1Bascom3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASCOM4")
    private double e1Bascom4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASCOM5")
    private double e1Bascom5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_FATPREF")
    private String e1Fatpref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_FATURA")
    private String e1Fatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_OK")
    private String e1Ok;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_PROJETO")
    private String e1Projeto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "E1_CLASCON")
    private String e1Clascon;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALCOM1")
    private double e1Valcom1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALCOM2")
    private double e1Valcom2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALCOM3")
    private double e1Valcom3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALCOM4")
    private double e1Valcom4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VALCOM5")
    private double e1Valcom5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_OCORREN")
    private String e1Ocorren;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_INSTR1")
    private String e1Instr1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_INSTR2")
    private String e1Instr2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_PEDIDO")
    private String e1Pedido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DTVARIA")
    private String e1Dtvaria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VARURV")
    private double e1Varurv;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VLCRUZ")
    private double e1Vlcruz;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DTFATUR")
    private String e1Dtfatur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_NUMNOTA")
    private String e1Numnota;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_SERIE")
    private String e1Serie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_STATUS")
    private String e1Status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_ORIGEM")
    private String e1Origem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_IDENTEE")
    private String e1Identee;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 19)
    @Column(name = "E1_NUMCART")
    private String e1Numcart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_FLUXO")
    private String e1Fluxo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_DESCFIN")
    private double e1Descfin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_DIADESC")
    private double e1Diadesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_TIPODES")
    private String e1Tipodes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "E1_CARTAO")
    private String e1Cartao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_CARTVAL")
    private String e1Cartval;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_CARTAUT")
    private String e1Cartaut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_ADM")
    private String e1Adm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VLRREAL")
    private double e1Vlrreal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "E1_TRANSF")
    private String e1Transf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_BCOCHQ")
    private String e1Bcochq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "E1_AGECHQ")
    private String e1Agechq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_CTACHQ")
    private String e1Ctachq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_NUMLIQ")
    private String e1Numliq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_ORDPAGO")
    private String e1Ordpago;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_RECIBO")
    private String e1Recibo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_INSS")
    private double e1Inss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_FILORIG")
    private String e1Filorig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DTACRED")
    private String e1Dtacred;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_TIPOFAT")
    private String e1Tipofat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_TIPOLIQ")
    private String e1Tipoliq;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_CSLL")
    private double e1Csll;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_COFINS")
    private double e1Cofins;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_PIS")
    private double e1Pis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_FLAGFAT")
    private String e1Flagfat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_MESBASE")
    private String e1Mesbase;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_ANOBASE")
    private String e1Anobase;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "E1_PLNUCOB")
    private String e1Plnucob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_CODINT")
    private String e1Codint;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_CODEMP")
    private String e1Codemp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_MATRIC")
    private String e1Matric;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_TXMOEDA")
    private double e1Txmoeda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_ACRESC")
    private double e1Acresc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SDACRES")
    private double e1Sdacres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_DECRESC")
    private double e1Decresc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SDDECRE")
    private double e1Sddecre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_MULTNAT")
    private String e1Multnat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_MSFIL")
    private String e1Msfil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_MSEMP")
    private String e1Msemp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_PROJPMS")
    private String e1Projpms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_DESDOBR")
    private String e1Desdobr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "E1_NRDOC")
    private String e1Nrdoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_MODSPB")
    private String e1Modspb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "E1_EMITCHQ")
    private String e1Emitchq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_IDCNAB")
    private String e1Idcnab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "E1_PLCOEMP")
    private String e1Plcoemp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_PLTPCOE")
    private String e1Pltpcoe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_CODCOR")
    private String e1Codcor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PARCCSS")
    private String e1Parccss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_CODORCA")
    private String e1Codorca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "E1_CODIMOV")
    private String e1Codimov;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_FILDEB")
    private String e1Fildeb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_NUMSOL")
    private String e1Numsol;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "E1_NUMRA")
    private String e1Numra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_INSCRIC")
    private String e1Inscric;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_SERREC")
    private String e1Serrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DATAEDI")
    private String e1Dataedi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 44)
    @Column(name = "E1_CODBAR")
    private String e1Codbar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 48)
    @Column(name = "E1_CODDIG")
    private String e1Coddig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_CHQDEV")
    private String e1Chqdev;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_LIDESCF")
    private String e1Lidescf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VLBOLSA")
    private double e1Vlbolsa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VLFIES")
    private double e1Vlfies;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_NUMCRD")
    private String e1Numcrd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "E1_DEBITO")
    private String e1Debito;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_CCD")
    private String e1Ccd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_ITEMD")
    private String e1Itemd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_CLVLDB")
    private String e1Clvldb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "E1_CREDIT")
    private String e1Credit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_CCC")
    private String e1Ccc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_ITEMC")
    private String e1Itemc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_CLVLCR")
    private String e1Clvlcr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_DESCON1")
    private double e1Descon1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_DESCON2")
    private double e1Descon2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DTDESC3")
    private String e1Dtdesc3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DTDESC1")
    private String e1Dtdesc1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_DTDESC2")
    private String e1Dtdesc2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VLMULTA")
    private double e1Vlmulta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_DESCON3")
    private double e1Descon3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_MOTNEG")
    private String e1Motneg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SABTPIS")
    private double e1Sabtpis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SABTCOF")
    private double e1Sabtcof;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SABTCSL")
    private double e1Sabtcsl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_FORNISS")
    private String e1Forniss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_PARTOT")
    private String e1Partot;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_SITFAT")
    private String e1Sitfat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASEPIS")
    private double e1Basepis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASECOF")
    private double e1Basecof;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASECSL")
    private double e1Basecsl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VRETISS")
    private double e1Vretiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PARCIRF")
    private String e1Parcirf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_SCORGP")
    private String e1Scorgp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_FRETISS")
    private String e1Fretiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_TXMDCOR")
    private double e1Txmdcor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SATBIRF")
    private double e1Satbirf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_TIPREG")
    private String e1Tipreg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "E1_CONEMP")
    private String e1Conemp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_VERCON")
    private String e1Vercon;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_SUBCON")
    private String e1Subcon;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_VERSUB")
    private String e1Versub;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_PLLOTE")
    private String e1Pllote;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_PLOPELT")
    private String e1Plopelt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_CODRDA")
    private String e1Codrda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_FORMREC")
    private String e1Formrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_BCOCLI")
    private String e1Bcocli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "E1_AGECLI")
    private String e1Agecli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_CTACLI")
    private String e1Ctacli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PARCFET")
    private String e1Parcfet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_FETHAB")
    private double e1Fethab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_MDCRON")
    private String e1Mdcron;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "E1_MDCONTR")
    private String e1Mdcontr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_MEDNUME")
    private String e1Mednume;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_MDPLANI")
    private String e1Mdplani;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_MDPARCE")
    private String e1Mdparce;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_MDREVIS")
    private String e1Mdrevis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_NUMMOV")
    private String e1Nummov;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PREFORI")
    private String e1Prefori;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_IDMOV")
    private String e1Idmov;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_NUMPRO")
    private String e1Numpro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_INDPRO")
    private String e1Indpro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_MDMULT")
    private double e1Mdmult;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_MDBONI")
    private double e1Mdboni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_MDDESC")
    private double e1Mddesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_RETCNTR")
    private double e1Retcntr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_NODIA")
    private String e1Nodia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_MULTDIA")
    private double e1Multdia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "E1_JURFAT")
    private String e1Jurfat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_RELATO")
    private String e1Relato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASEINS")
    private double e1Baseins;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "E1_TITPAI")
    private String e1Titpai;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_NFELETR")
    private String e1Nfeletr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "E1_DOCTEF")
    private String e1Doctef;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_PRINSS")
    private double e1Prinss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PARCFAB")
    private String e1Parcfab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_PARCFAC")
    private String e1Parcfac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_PARTPDP")
    private String e1Partpdp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_TPDESC")
    private String e1Tpdesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_TPDP")
    private double e1Tpdp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_VLMINIS")
    private String e1Vlminis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_FACS")
    private double e1Facs;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_FABOV")
    private double e1Fabov;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "E1_NUMCON")
    private String e1Numcon;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_SABTIRF")
    private double e1Sabtirf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_IDLAN")
    private double e1Idlan;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_IDBOLET")
    private double e1Idbolet;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_SERVICO")
    private String e1Servico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VLBOLP")
    private double e1Vlbolp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_IDAPLIC")
    private double e1Idaplic;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_PROCEL")
    private double e1Procel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_NUMINSC")
    private double e1Numinsc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_DIACTB")
    private String e1Diactb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "E1_LTCXA")
    private String e1Ltcxa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_NOPER")
    private double e1Noper;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "E1_TURMA")
    private String e1Turma;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_NSUTEF")
    private String e1Nsutef;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_VRETIRF")
    private double e1Vretirf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_APLVLMN")
    private String e1Aplvlmn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_BASEISS")
    private double e1Baseiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "E1_SEQBX")
    private String e1Seqbx;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "E1_RATFIN")
    private String e1Ratfin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_CODIRRF")
    private String e1Codirrf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_PRISS")
    private double e1Priss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "E1_CODISS")
    private String e1Codiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "E1_USERLGI")
    private String e1Userlgi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "E1_USERLGA")
    private String e1Userlga;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_USERNF1")
    private String e1Usernf1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_UFILNF1")
    private String e1Ufilnf1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_UNUMNF1")
    private String e1Unumnf1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_UVALNF1")
    private double e1Uvalnf1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_UFILNF2")
    private String e1Ufilnf2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_USERNF2")
    private String e1Usernf2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_UNUMNF2")
    private String e1Unumnf2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_UVALNF2")
    private double e1Uvalnf2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_UFILNF3")
    private String e1Ufilnf3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_USERNF3")
    private String e1Usernf3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_UNUMNF3")
    private String e1Unumnf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_UVALNF3")
    private double e1Uvalnf3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_UFILNF4")
    private String e1Ufilnf4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_USERNF4")
    private String e1Usernf4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_UNUMNF4")
    private String e1Unumnf4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_UVALNF4")
    private double e1Uvalnf4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "E1_UFILNF5")
    private String e1Ufilnf5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "E1_USERNF5")
    private String e1Usernf5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "E1_UNUMNF5")
    private String e1Unumnf5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_UVALNF5")
    private double e1Uvalnf5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 44)
    @Column(name = "E1_UCODBAR")
    private String e1Ucodbar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "E1_ULINDIG")
    private String e1Ulindig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "R_E_C_D_E_L_")
//    private int rECDEL;
    @Basic(optional = false)
    @NotNull
    @Column(name = "E1_USITUA1")
    private double e1Usitua1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "E1_UDTBLS")
    private String e1Udtbls;
    @Column(name = "E1_ULIBFAT")
    private String e1Ulibfat;
    @OneToMany(mappedBy = "se1010")
    private List<Sz1010> sz1010s;

    public Se1010() {
    }

    public Se1010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getE1Filial() {
        return e1Filial;
    }

    public void setE1Filial(String e1Filial) {
        this.e1Filial = e1Filial;
    }

    public String getE1Prefixo() {
        return e1Prefixo;
    }

    public void setE1Prefixo(String e1Prefixo) {
        this.e1Prefixo = e1Prefixo;
    }

    public String getE1Num() {
        return e1Num;
    }

    public void setE1Num(String e1Num) {
        this.e1Num = e1Num;
    }

    public String getE1Parcela() {
        return e1Parcela;
    }

    public void setE1Parcela(String e1Parcela) {
        this.e1Parcela = e1Parcela;
    }

    public String getE1Tipo() {
        return e1Tipo;
    }

    public void setE1Tipo(String e1Tipo) {
        this.e1Tipo = e1Tipo;
    }

    public String getE1Naturez() {
        return e1Naturez;
    }

    public void setE1Naturez(String e1Naturez) {
        this.e1Naturez = e1Naturez;
    }

    public Sa1010 getSa1010() {
        return sa1010;
    }

    public void setSa1010(Sa1010 sa1010) {
        this.sa1010 = sa1010;
    }

    public String getE1Nomcli() {
        return e1Nomcli;
    }

    public void setE1Nomcli(String e1Nomcli) {
        this.e1Nomcli = e1Nomcli;
    }

    public String getE1Emissao() {
        return e1Emissao;
    }

    public void setE1Emissao(String e1Emissao) {
        this.e1Emissao = e1Emissao;
    }

    public Date getE1EmissaoDt() throws ParseException {
        if (!e1Emissao.isEmpty()) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = dateFormat.parse(e1Emissao.substring(6, 8) + "/" + e1Emissao.substring(4, 6) + "/" + e1Emissao.substring(0, 4));
            return date;
        }
        return null;
    }

    public String getE1Vencto() {
        return e1Vencto;
    }

    public Date getE1VenctoDt() throws ParseException {
        if (!e1Vencto.isEmpty()) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = dateFormat.parse(e1Vencto.substring(6, 8) + "/" + e1Vencto.substring(4, 6) + "/" + e1Vencto.substring(0, 4));
            return date;
        }
        return null;
    }

    public void setE1Vencto(String e1Vencto) {
        this.e1Vencto = e1Vencto;
    }

    public String getE1Vencrea() {
        return e1Vencrea;
    }

    public void setE1Vencrea(String e1Vencrea) {
        this.e1Vencrea = e1Vencrea;
    }

    public double getE1Valor() {
        return e1Valor;
    }

    public void setE1Valor(double e1Valor) {
        this.e1Valor = e1Valor;
    }

    public double getE1Baseirf() {
        return e1Baseirf;
    }

    public void setE1Baseirf(double e1Baseirf) {
        this.e1Baseirf = e1Baseirf;
    }

    public double getE1Irrf() {
        return e1Irrf;
    }

    public void setE1Irrf(double e1Irrf) {
        this.e1Irrf = e1Irrf;
    }

    public double getE1Iss() {
        return e1Iss;
    }

    public void setE1Iss(double e1Iss) {
        this.e1Iss = e1Iss;
    }

    public String getE1Numbco() {
        return e1Numbco;
    }

    public void setE1Numbco(String e1Numbco) {
        this.e1Numbco = e1Numbco;
    }

    public String getE1Indice() {
        return e1Indice;
    }

    public void setE1Indice(String e1Indice) {
        this.e1Indice = e1Indice;
    }

    public String getE1Baixa() {
        return e1Baixa;
    }

    public void setE1Baixa(String e1Baixa) {
        this.e1Baixa = e1Baixa;
    }

    public String getE1Numbor() {
        return e1Numbor;
    }

    public void setE1Numbor(String e1Numbor) {
        this.e1Numbor = e1Numbor;
    }

    public String getE1Databor() {
        return e1Databor;
    }

    public void setE1Databor(String e1Databor) {
        this.e1Databor = e1Databor;
    }

    public String getE1Emis1() {
        return e1Emis1;
    }

    public void setE1Emis1(String e1Emis1) {
        this.e1Emis1 = e1Emis1;
    }

    public String getE1Hist() {
        return e1Hist;
    }

    public void setE1Hist(String e1Hist) {
        this.e1Hist = e1Hist;
    }

    public String getE1La() {
        return e1La;
    }

    public void setE1La(String e1La) {
        this.e1La = e1La;
    }

    public String getE1Lote() {
        return e1Lote;
    }

    public void setE1Lote(String e1Lote) {
        this.e1Lote = e1Lote;
    }

    public String getE1Motivo() {
        return e1Motivo;
    }

    public void setE1Motivo(String e1Motivo) {
        this.e1Motivo = e1Motivo;
    }

    public String getE1Movimen() {
        return e1Movimen;
    }

    public void setE1Movimen(String e1Movimen) {
        this.e1Movimen = e1Movimen;
    }

    public String getE1Op() {
        return e1Op;
    }

    public void setE1Op(String e1Op) {
        this.e1Op = e1Op;
    }

    public String getE1Situaca() {
        return e1Situaca;
    }

    public void setE1Situaca(String e1Situaca) {
        this.e1Situaca = e1Situaca;
    }

    public String getE1Contrat() {
        return e1Contrat;
    }

    public void setE1Contrat(String e1Contrat) {
        this.e1Contrat = e1Contrat;
    }

    public double getE1Saldo() {
        return e1Saldo;
    }

    public void setE1Saldo(double e1Saldo) {
        this.e1Saldo = e1Saldo;
    }

    public String getE1Supervi() {
        return e1Supervi;
    }

    public void setE1Supervi(String e1Supervi) {
        this.e1Supervi = e1Supervi;
    }

    public String getE1Vend1() {
        return e1Vend1;
    }

    public void setE1Vend1(String e1Vend1) {
        this.e1Vend1 = e1Vend1;
    }

    public String getE1Vend2() {
        return e1Vend2;
    }

    public void setE1Vend2(String e1Vend2) {
        this.e1Vend2 = e1Vend2;
    }

    public String getE1Vend3() {
        return e1Vend3;
    }

    public void setE1Vend3(String e1Vend3) {
        this.e1Vend3 = e1Vend3;
    }

    public String getE1Vend4() {
        return e1Vend4;
    }

    public void setE1Vend4(String e1Vend4) {
        this.e1Vend4 = e1Vend4;
    }

    public String getE1Vend5() {
        return e1Vend5;
    }

    public void setE1Vend5(String e1Vend5) {
        this.e1Vend5 = e1Vend5;
    }

    public double getE1Comis1() {
        return e1Comis1;
    }

    public void setE1Comis1(double e1Comis1) {
        this.e1Comis1 = e1Comis1;
    }

    public double getE1Comis2() {
        return e1Comis2;
    }

    public void setE1Comis2(double e1Comis2) {
        this.e1Comis2 = e1Comis2;
    }

    public double getE1Comis3() {
        return e1Comis3;
    }

    public void setE1Comis3(double e1Comis3) {
        this.e1Comis3 = e1Comis3;
    }

    public double getE1Comis4() {
        return e1Comis4;
    }

    public void setE1Comis4(double e1Comis4) {
        this.e1Comis4 = e1Comis4;
    }

    public double getE1Descont() {
        return e1Descont;
    }

    public void setE1Descont(double e1Descont) {
        this.e1Descont = e1Descont;
    }

    public double getE1Comis5() {
        return e1Comis5;
    }

    public void setE1Comis5(double e1Comis5) {
        this.e1Comis5 = e1Comis5;
    }

    public double getE1Multa() {
        return e1Multa;
    }

    public void setE1Multa(double e1Multa) {
        this.e1Multa = e1Multa;
    }

    public double getE1Juros() {
        return e1Juros;
    }

    public void setE1Juros(double e1Juros) {
        this.e1Juros = e1Juros;
    }

    public double getE1Correc() {
        return e1Correc;
    }

    public void setE1Correc(double e1Correc) {
        this.e1Correc = e1Correc;
    }

    public double getE1Valliq() {
        return e1Valliq;
    }

    public void setE1Valliq(double e1Valliq) {
        this.e1Valliq = e1Valliq;
    }

    public String getE1Vencori() {
        return e1Vencori;
    }

    public void setE1Vencori(String e1Vencori) {
        this.e1Vencori = e1Vencori;
    }

    public double getE1Valjur() {
        return e1Valjur;
    }

    public void setE1Valjur(double e1Valjur) {
        this.e1Valjur = e1Valjur;
    }

    public double getE1Porcjur() {
        return e1Porcjur;
    }

    public void setE1Porcjur(double e1Porcjur) {
        this.e1Porcjur = e1Porcjur;
    }

    public double getE1Moeda() {
        return e1Moeda;
    }

    public void setE1Moeda(double e1Moeda) {
        this.e1Moeda = e1Moeda;
    }

    public double getE1Bascom1() {
        return e1Bascom1;
    }

    public void setE1Bascom1(double e1Bascom1) {
        this.e1Bascom1 = e1Bascom1;
    }

    public double getE1Bascom2() {
        return e1Bascom2;
    }

    public void setE1Bascom2(double e1Bascom2) {
        this.e1Bascom2 = e1Bascom2;
    }

    public double getE1Bascom3() {
        return e1Bascom3;
    }

    public void setE1Bascom3(double e1Bascom3) {
        this.e1Bascom3 = e1Bascom3;
    }

    public double getE1Bascom4() {
        return e1Bascom4;
    }

    public void setE1Bascom4(double e1Bascom4) {
        this.e1Bascom4 = e1Bascom4;
    }

    public double getE1Bascom5() {
        return e1Bascom5;
    }

    public void setE1Bascom5(double e1Bascom5) {
        this.e1Bascom5 = e1Bascom5;
    }

    public String getE1Fatpref() {
        return e1Fatpref;
    }

    public void setE1Fatpref(String e1Fatpref) {
        this.e1Fatpref = e1Fatpref;
    }

    public String getE1Fatura() {
        return e1Fatura;
    }

    public void setE1Fatura(String e1Fatura) {
        this.e1Fatura = e1Fatura;
    }

    public String getE1Ok() {
        return e1Ok;
    }

    public void setE1Ok(String e1Ok) {
        this.e1Ok = e1Ok;
    }

    public String getE1Projeto() {
        return e1Projeto;
    }

    public void setE1Projeto(String e1Projeto) {
        this.e1Projeto = e1Projeto;
    }

    public String getE1Clascon() {
        return e1Clascon;
    }

    public void setE1Clascon(String e1Clascon) {
        this.e1Clascon = e1Clascon;
    }

    public double getE1Valcom1() {
        return e1Valcom1;
    }

    public void setE1Valcom1(double e1Valcom1) {
        this.e1Valcom1 = e1Valcom1;
    }

    public double getE1Valcom2() {
        return e1Valcom2;
    }

    public void setE1Valcom2(double e1Valcom2) {
        this.e1Valcom2 = e1Valcom2;
    }

    public double getE1Valcom3() {
        return e1Valcom3;
    }

    public void setE1Valcom3(double e1Valcom3) {
        this.e1Valcom3 = e1Valcom3;
    }

    public double getE1Valcom4() {
        return e1Valcom4;
    }

    public void setE1Valcom4(double e1Valcom4) {
        this.e1Valcom4 = e1Valcom4;
    }

    public double getE1Valcom5() {
        return e1Valcom5;
    }

    public void setE1Valcom5(double e1Valcom5) {
        this.e1Valcom5 = e1Valcom5;
    }

    public String getE1Ocorren() {
        return e1Ocorren;
    }

    public void setE1Ocorren(String e1Ocorren) {
        this.e1Ocorren = e1Ocorren;
    }

    public String getE1Instr1() {
        return e1Instr1;
    }

    public void setE1Instr1(String e1Instr1) {
        this.e1Instr1 = e1Instr1;
    }

    public String getE1Instr2() {
        return e1Instr2;
    }

    public void setE1Instr2(String e1Instr2) {
        this.e1Instr2 = e1Instr2;
    }

    public String getE1Pedido() {
        return e1Pedido;
    }

    public void setE1Pedido(String e1Pedido) {
        this.e1Pedido = e1Pedido;
    }

    public String getE1Dtvaria() {
        return e1Dtvaria;
    }

    public void setE1Dtvaria(String e1Dtvaria) {
        this.e1Dtvaria = e1Dtvaria;
    }

    public double getE1Varurv() {
        return e1Varurv;
    }

    public void setE1Varurv(double e1Varurv) {
        this.e1Varurv = e1Varurv;
    }

    public double getE1Vlcruz() {
        return e1Vlcruz;
    }

    public void setE1Vlcruz(double e1Vlcruz) {
        this.e1Vlcruz = e1Vlcruz;
    }

    public String getE1Dtfatur() {
        return e1Dtfatur;
    }

    public void setE1Dtfatur(String e1Dtfatur) {
        this.e1Dtfatur = e1Dtfatur;
    }

    public String getE1Numnota() {
        return e1Numnota;
    }

    public void setE1Numnota(String e1Numnota) {
        this.e1Numnota = e1Numnota;
    }

    public String getE1Serie() {
        return e1Serie;
    }

    public void setE1Serie(String e1Serie) {
        this.e1Serie = e1Serie;
    }

    public String getE1Status() {
        return e1Status;
    }

    public void setE1Status(String e1Status) {
        this.e1Status = e1Status;
    }

    public String getE1Origem() {
        return e1Origem;
    }

    public void setE1Origem(String e1Origem) {
        this.e1Origem = e1Origem;
    }

    public String getE1Identee() {
        return e1Identee;
    }

    public void setE1Identee(String e1Identee) {
        this.e1Identee = e1Identee;
    }

    public String getE1Numcart() {
        return e1Numcart;
    }

    public void setE1Numcart(String e1Numcart) {
        this.e1Numcart = e1Numcart;
    }

    public String getE1Fluxo() {
        return e1Fluxo;
    }

    public void setE1Fluxo(String e1Fluxo) {
        this.e1Fluxo = e1Fluxo;
    }

    public double getE1Descfin() {
        return e1Descfin;
    }

    public void setE1Descfin(double e1Descfin) {
        this.e1Descfin = e1Descfin;
    }

    public double getE1Diadesc() {
        return e1Diadesc;
    }

    public void setE1Diadesc(double e1Diadesc) {
        this.e1Diadesc = e1Diadesc;
    }

    public String getE1Tipodes() {
        return e1Tipodes;
    }

    public void setE1Tipodes(String e1Tipodes) {
        this.e1Tipodes = e1Tipodes;
    }

    public String getE1Cartao() {
        return e1Cartao;
    }

    public void setE1Cartao(String e1Cartao) {
        this.e1Cartao = e1Cartao;
    }

    public String getE1Cartval() {
        return e1Cartval;
    }

    public void setE1Cartval(String e1Cartval) {
        this.e1Cartval = e1Cartval;
    }

    public String getE1Cartaut() {
        return e1Cartaut;
    }

    public void setE1Cartaut(String e1Cartaut) {
        this.e1Cartaut = e1Cartaut;
    }

    public String getE1Adm() {
        return e1Adm;
    }

    public void setE1Adm(String e1Adm) {
        this.e1Adm = e1Adm;
    }

    public double getE1Vlrreal() {
        return e1Vlrreal;
    }

    public void setE1Vlrreal(double e1Vlrreal) {
        this.e1Vlrreal = e1Vlrreal;
    }

    public String getE1Transf() {
        return e1Transf;
    }

    public void setE1Transf(String e1Transf) {
        this.e1Transf = e1Transf;
    }

    public String getE1Bcochq() {
        return e1Bcochq;
    }

    public void setE1Bcochq(String e1Bcochq) {
        this.e1Bcochq = e1Bcochq;
    }

    public String getE1Agechq() {
        return e1Agechq;
    }

    public void setE1Agechq(String e1Agechq) {
        this.e1Agechq = e1Agechq;
    }

    public String getE1Ctachq() {
        return e1Ctachq;
    }

    public void setE1Ctachq(String e1Ctachq) {
        this.e1Ctachq = e1Ctachq;
    }

    public String getE1Numliq() {
        return e1Numliq;
    }

    public void setE1Numliq(String e1Numliq) {
        this.e1Numliq = e1Numliq;
    }

    public String getE1Ordpago() {
        return e1Ordpago;
    }

    public void setE1Ordpago(String e1Ordpago) {
        this.e1Ordpago = e1Ordpago;
    }

    public String getE1Recibo() {
        return e1Recibo;
    }

    public void setE1Recibo(String e1Recibo) {
        this.e1Recibo = e1Recibo;
    }

    public double getE1Inss() {
        return e1Inss;
    }

    public void setE1Inss(double e1Inss) {
        this.e1Inss = e1Inss;
    }

    public String getE1Filorig() {
        return e1Filorig;
    }

    public void setE1Filorig(String e1Filorig) {
        this.e1Filorig = e1Filorig;
    }

    public String getE1Dtacred() {
        return e1Dtacred;
    }

    public void setE1Dtacred(String e1Dtacred) {
        this.e1Dtacred = e1Dtacred;
    }

    public String getE1Tipofat() {
        return e1Tipofat;
    }

    public void setE1Tipofat(String e1Tipofat) {
        this.e1Tipofat = e1Tipofat;
    }

    public String getE1Tipoliq() {
        return e1Tipoliq;
    }

    public void setE1Tipoliq(String e1Tipoliq) {
        this.e1Tipoliq = e1Tipoliq;
    }

    public double getE1Csll() {
        return e1Csll;
    }

    public void setE1Csll(double e1Csll) {
        this.e1Csll = e1Csll;
    }

    public double getE1Cofins() {
        return e1Cofins;
    }

    public void setE1Cofins(double e1Cofins) {
        this.e1Cofins = e1Cofins;
    }

    public double getE1Pis() {
        return e1Pis;
    }

    public void setE1Pis(double e1Pis) {
        this.e1Pis = e1Pis;
    }

    public String getE1Flagfat() {
        return e1Flagfat;
    }

    public void setE1Flagfat(String e1Flagfat) {
        this.e1Flagfat = e1Flagfat;
    }

    public String getE1Mesbase() {
        return e1Mesbase;
    }

    public void setE1Mesbase(String e1Mesbase) {
        this.e1Mesbase = e1Mesbase;
    }

    public String getE1Anobase() {
        return e1Anobase;
    }

    public void setE1Anobase(String e1Anobase) {
        this.e1Anobase = e1Anobase;
    }

    public String getE1Plnucob() {
        return e1Plnucob;
    }

    public void setE1Plnucob(String e1Plnucob) {
        this.e1Plnucob = e1Plnucob;
    }

    public String getE1Codint() {
        return e1Codint;
    }

    public void setE1Codint(String e1Codint) {
        this.e1Codint = e1Codint;
    }

    public String getE1Codemp() {
        return e1Codemp;
    }

    public void setE1Codemp(String e1Codemp) {
        this.e1Codemp = e1Codemp;
    }

    public String getE1Matric() {
        return e1Matric;
    }

    public void setE1Matric(String e1Matric) {
        this.e1Matric = e1Matric;
    }

    public double getE1Txmoeda() {
        return e1Txmoeda;
    }

    public void setE1Txmoeda(double e1Txmoeda) {
        this.e1Txmoeda = e1Txmoeda;
    }

    public double getE1Acresc() {
        return e1Acresc;
    }

    public void setE1Acresc(double e1Acresc) {
        this.e1Acresc = e1Acresc;
    }

    public double getE1Sdacres() {
        return e1Sdacres;
    }

    public void setE1Sdacres(double e1Sdacres) {
        this.e1Sdacres = e1Sdacres;
    }

    public double getE1Decresc() {
        return e1Decresc;
    }

    public void setE1Decresc(double e1Decresc) {
        this.e1Decresc = e1Decresc;
    }

    public double getE1Sddecre() {
        return e1Sddecre;
    }

    public void setE1Sddecre(double e1Sddecre) {
        this.e1Sddecre = e1Sddecre;
    }

    public String getE1Multnat() {
        return e1Multnat;
    }

    public void setE1Multnat(String e1Multnat) {
        this.e1Multnat = e1Multnat;
    }

    public String getE1Msfil() {
        return e1Msfil;
    }

    public void setE1Msfil(String e1Msfil) {
        this.e1Msfil = e1Msfil;
    }

    public String getE1Msemp() {
        return e1Msemp;
    }

    public void setE1Msemp(String e1Msemp) {
        this.e1Msemp = e1Msemp;
    }

    public String getE1Projpms() {
        return e1Projpms;
    }

    public void setE1Projpms(String e1Projpms) {
        this.e1Projpms = e1Projpms;
    }

    public String getE1Desdobr() {
        return e1Desdobr;
    }

    public void setE1Desdobr(String e1Desdobr) {
        this.e1Desdobr = e1Desdobr;
    }

    public String getE1Nrdoc() {
        return e1Nrdoc;
    }

    public void setE1Nrdoc(String e1Nrdoc) {
        this.e1Nrdoc = e1Nrdoc;
    }

    public String getE1Modspb() {
        return e1Modspb;
    }

    public void setE1Modspb(String e1Modspb) {
        this.e1Modspb = e1Modspb;
    }

    public String getE1Emitchq() {
        return e1Emitchq;
    }

    public void setE1Emitchq(String e1Emitchq) {
        this.e1Emitchq = e1Emitchq;
    }

    public String getE1Idcnab() {
        return e1Idcnab;
    }

    public void setE1Idcnab(String e1Idcnab) {
        this.e1Idcnab = e1Idcnab;
    }

    public String getE1Plcoemp() {
        return e1Plcoemp;
    }

    public void setE1Plcoemp(String e1Plcoemp) {
        this.e1Plcoemp = e1Plcoemp;
    }

    public String getE1Pltpcoe() {
        return e1Pltpcoe;
    }

    public void setE1Pltpcoe(String e1Pltpcoe) {
        this.e1Pltpcoe = e1Pltpcoe;
    }

    public String getE1Codcor() {
        return e1Codcor;
    }

    public void setE1Codcor(String e1Codcor) {
        this.e1Codcor = e1Codcor;
    }

    public String getE1Parccss() {
        return e1Parccss;
    }

    public void setE1Parccss(String e1Parccss) {
        this.e1Parccss = e1Parccss;
    }

    public String getE1Codorca() {
        return e1Codorca;
    }

    public void setE1Codorca(String e1Codorca) {
        this.e1Codorca = e1Codorca;
    }

    public String getE1Codimov() {
        return e1Codimov;
    }

    public void setE1Codimov(String e1Codimov) {
        this.e1Codimov = e1Codimov;
    }

    public String getE1Fildeb() {
        return e1Fildeb;
    }

    public void setE1Fildeb(String e1Fildeb) {
        this.e1Fildeb = e1Fildeb;
    }

    public String getE1Numsol() {
        return e1Numsol;
    }

    public void setE1Numsol(String e1Numsol) {
        this.e1Numsol = e1Numsol;
    }

    public String getE1Numra() {
        return e1Numra;
    }

    public void setE1Numra(String e1Numra) {
        this.e1Numra = e1Numra;
    }

    public String getE1Inscric() {
        return e1Inscric;
    }

    public void setE1Inscric(String e1Inscric) {
        this.e1Inscric = e1Inscric;
    }

    public String getE1Serrec() {
        return e1Serrec;
    }

    public void setE1Serrec(String e1Serrec) {
        this.e1Serrec = e1Serrec;
    }

    public String getE1Dataedi() {
        return e1Dataedi;
    }

    public void setE1Dataedi(String e1Dataedi) {
        this.e1Dataedi = e1Dataedi;
    }

    public String getE1Codbar() {
        return e1Codbar;
    }

    public void setE1Codbar(String e1Codbar) {
        this.e1Codbar = e1Codbar;
    }

    public String getE1Coddig() {
        return e1Coddig;
    }

    public void setE1Coddig(String e1Coddig) {
        this.e1Coddig = e1Coddig;
    }

    public String getE1Chqdev() {
        return e1Chqdev;
    }

    public void setE1Chqdev(String e1Chqdev) {
        this.e1Chqdev = e1Chqdev;
    }

    public String getE1Lidescf() {
        return e1Lidescf;
    }

    public void setE1Lidescf(String e1Lidescf) {
        this.e1Lidescf = e1Lidescf;
    }

    public double getE1Vlbolsa() {
        return e1Vlbolsa;
    }

    public void setE1Vlbolsa(double e1Vlbolsa) {
        this.e1Vlbolsa = e1Vlbolsa;
    }

    public double getE1Vlfies() {
        return e1Vlfies;
    }

    public void setE1Vlfies(double e1Vlfies) {
        this.e1Vlfies = e1Vlfies;
    }

    public String getE1Numcrd() {
        return e1Numcrd;
    }

    public void setE1Numcrd(String e1Numcrd) {
        this.e1Numcrd = e1Numcrd;
    }

    public String getE1Debito() {
        return e1Debito;
    }

    public void setE1Debito(String e1Debito) {
        this.e1Debito = e1Debito;
    }

    public String getE1Ccd() {
        return e1Ccd;
    }

    public void setE1Ccd(String e1Ccd) {
        this.e1Ccd = e1Ccd;
    }

    public String getE1Itemd() {
        return e1Itemd;
    }

    public void setE1Itemd(String e1Itemd) {
        this.e1Itemd = e1Itemd;
    }

    public String getE1Clvldb() {
        return e1Clvldb;
    }

    public void setE1Clvldb(String e1Clvldb) {
        this.e1Clvldb = e1Clvldb;
    }

    public String getE1Credit() {
        return e1Credit;
    }

    public void setE1Credit(String e1Credit) {
        this.e1Credit = e1Credit;
    }

    public String getE1Ccc() {
        return e1Ccc;
    }

    public void setE1Ccc(String e1Ccc) {
        this.e1Ccc = e1Ccc;
    }

    public String getE1Itemc() {
        return e1Itemc;
    }

    public void setE1Itemc(String e1Itemc) {
        this.e1Itemc = e1Itemc;
    }

    public String getE1Clvlcr() {
        return e1Clvlcr;
    }

    public void setE1Clvlcr(String e1Clvlcr) {
        this.e1Clvlcr = e1Clvlcr;
    }

    public double getE1Descon1() {
        return e1Descon1;
    }

    public void setE1Descon1(double e1Descon1) {
        this.e1Descon1 = e1Descon1;
    }

    public double getE1Descon2() {
        return e1Descon2;
    }

    public void setE1Descon2(double e1Descon2) {
        this.e1Descon2 = e1Descon2;
    }

    public String getE1Dtdesc3() {
        return e1Dtdesc3;
    }

    public void setE1Dtdesc3(String e1Dtdesc3) {
        this.e1Dtdesc3 = e1Dtdesc3;
    }

    public String getE1Dtdesc1() {
        return e1Dtdesc1;
    }

    public void setE1Dtdesc1(String e1Dtdesc1) {
        this.e1Dtdesc1 = e1Dtdesc1;
    }

    public String getE1Dtdesc2() {
        return e1Dtdesc2;
    }

    public void setE1Dtdesc2(String e1Dtdesc2) {
        this.e1Dtdesc2 = e1Dtdesc2;
    }

    public double getE1Vlmulta() {
        return e1Vlmulta;
    }

    public void setE1Vlmulta(double e1Vlmulta) {
        this.e1Vlmulta = e1Vlmulta;
    }

    public double getE1Descon3() {
        return e1Descon3;
    }

    public void setE1Descon3(double e1Descon3) {
        this.e1Descon3 = e1Descon3;
    }

    public String getE1Motneg() {
        return e1Motneg;
    }

    public void setE1Motneg(String e1Motneg) {
        this.e1Motneg = e1Motneg;
    }

    public double getE1Sabtpis() {
        return e1Sabtpis;
    }

    public void setE1Sabtpis(double e1Sabtpis) {
        this.e1Sabtpis = e1Sabtpis;
    }

    public double getE1Sabtcof() {
        return e1Sabtcof;
    }

    public void setE1Sabtcof(double e1Sabtcof) {
        this.e1Sabtcof = e1Sabtcof;
    }

    public double getE1Sabtcsl() {
        return e1Sabtcsl;
    }

    public void setE1Sabtcsl(double e1Sabtcsl) {
        this.e1Sabtcsl = e1Sabtcsl;
    }

    public String getE1Forniss() {
        return e1Forniss;
    }

    public void setE1Forniss(String e1Forniss) {
        this.e1Forniss = e1Forniss;
    }

    public String getE1Partot() {
        return e1Partot;
    }

    public void setE1Partot(String e1Partot) {
        this.e1Partot = e1Partot;
    }

    public String getE1Sitfat() {
        return e1Sitfat;
    }

    public void setE1Sitfat(String e1Sitfat) {
        this.e1Sitfat = e1Sitfat;
    }

    public double getE1Basepis() {
        return e1Basepis;
    }

    public void setE1Basepis(double e1Basepis) {
        this.e1Basepis = e1Basepis;
    }

    public double getE1Basecof() {
        return e1Basecof;
    }

    public void setE1Basecof(double e1Basecof) {
        this.e1Basecof = e1Basecof;
    }

    public double getE1Basecsl() {
        return e1Basecsl;
    }

    public void setE1Basecsl(double e1Basecsl) {
        this.e1Basecsl = e1Basecsl;
    }

    public double getE1Vretiss() {
        return e1Vretiss;
    }

    public void setE1Vretiss(double e1Vretiss) {
        this.e1Vretiss = e1Vretiss;
    }

    public String getE1Parcirf() {
        return e1Parcirf;
    }

    public void setE1Parcirf(String e1Parcirf) {
        this.e1Parcirf = e1Parcirf;
    }

    public String getE1Scorgp() {
        return e1Scorgp;
    }

    public void setE1Scorgp(String e1Scorgp) {
        this.e1Scorgp = e1Scorgp;
    }

    public String getE1Fretiss() {
        return e1Fretiss;
    }

    public void setE1Fretiss(String e1Fretiss) {
        this.e1Fretiss = e1Fretiss;
    }

    public double getE1Txmdcor() {
        return e1Txmdcor;
    }

    public void setE1Txmdcor(double e1Txmdcor) {
        this.e1Txmdcor = e1Txmdcor;
    }

    public double getE1Satbirf() {
        return e1Satbirf;
    }

    public void setE1Satbirf(double e1Satbirf) {
        this.e1Satbirf = e1Satbirf;
    }

    public String getE1Tipreg() {
        return e1Tipreg;
    }

    public void setE1Tipreg(String e1Tipreg) {
        this.e1Tipreg = e1Tipreg;
    }

    public String getE1Conemp() {
        return e1Conemp;
    }

    public void setE1Conemp(String e1Conemp) {
        this.e1Conemp = e1Conemp;
    }

    public String getE1Vercon() {
        return e1Vercon;
    }

    public void setE1Vercon(String e1Vercon) {
        this.e1Vercon = e1Vercon;
    }

    public String getE1Subcon() {
        return e1Subcon;
    }

    public void setE1Subcon(String e1Subcon) {
        this.e1Subcon = e1Subcon;
    }

    public String getE1Versub() {
        return e1Versub;
    }

    public void setE1Versub(String e1Versub) {
        this.e1Versub = e1Versub;
    }

    public String getE1Pllote() {
        return e1Pllote;
    }

    public void setE1Pllote(String e1Pllote) {
        this.e1Pllote = e1Pllote;
    }

    public String getE1Plopelt() {
        return e1Plopelt;
    }

    public void setE1Plopelt(String e1Plopelt) {
        this.e1Plopelt = e1Plopelt;
    }

    public String getE1Codrda() {
        return e1Codrda;
    }

    public void setE1Codrda(String e1Codrda) {
        this.e1Codrda = e1Codrda;
    }

    public String getE1Formrec() {
        return e1Formrec;
    }

    public void setE1Formrec(String e1Formrec) {
        this.e1Formrec = e1Formrec;
    }

    public String getE1Bcocli() {
        return e1Bcocli;
    }

    public void setE1Bcocli(String e1Bcocli) {
        this.e1Bcocli = e1Bcocli;
    }

    public String getE1Agecli() {
        return e1Agecli;
    }

    public void setE1Agecli(String e1Agecli) {
        this.e1Agecli = e1Agecli;
    }

    public String getE1Ctacli() {
        return e1Ctacli;
    }

    public void setE1Ctacli(String e1Ctacli) {
        this.e1Ctacli = e1Ctacli;
    }

    public String getE1Parcfet() {
        return e1Parcfet;
    }

    public void setE1Parcfet(String e1Parcfet) {
        this.e1Parcfet = e1Parcfet;
    }

    public double getE1Fethab() {
        return e1Fethab;
    }

    public void setE1Fethab(double e1Fethab) {
        this.e1Fethab = e1Fethab;
    }

    public String getE1Mdcron() {
        return e1Mdcron;
    }

    public void setE1Mdcron(String e1Mdcron) {
        this.e1Mdcron = e1Mdcron;
    }

    public String getE1Mdcontr() {
        return e1Mdcontr;
    }

    public void setE1Mdcontr(String e1Mdcontr) {
        this.e1Mdcontr = e1Mdcontr;
    }

    public String getE1Mednume() {
        return e1Mednume;
    }

    public void setE1Mednume(String e1Mednume) {
        this.e1Mednume = e1Mednume;
    }

    public String getE1Mdplani() {
        return e1Mdplani;
    }

    public void setE1Mdplani(String e1Mdplani) {
        this.e1Mdplani = e1Mdplani;
    }

    public String getE1Mdparce() {
        return e1Mdparce;
    }

    public void setE1Mdparce(String e1Mdparce) {
        this.e1Mdparce = e1Mdparce;
    }

    public String getE1Mdrevis() {
        return e1Mdrevis;
    }

    public void setE1Mdrevis(String e1Mdrevis) {
        this.e1Mdrevis = e1Mdrevis;
    }

    public String getE1Nummov() {
        return e1Nummov;
    }

    public void setE1Nummov(String e1Nummov) {
        this.e1Nummov = e1Nummov;
    }

    public String getE1Prefori() {
        return e1Prefori;
    }

    public void setE1Prefori(String e1Prefori) {
        this.e1Prefori = e1Prefori;
    }

    public String getE1Idmov() {
        return e1Idmov;
    }

    public void setE1Idmov(String e1Idmov) {
        this.e1Idmov = e1Idmov;
    }

    public String getE1Numpro() {
        return e1Numpro;
    }

    public void setE1Numpro(String e1Numpro) {
        this.e1Numpro = e1Numpro;
    }

    public String getE1Indpro() {
        return e1Indpro;
    }

    public void setE1Indpro(String e1Indpro) {
        this.e1Indpro = e1Indpro;
    }

    public double getE1Mdmult() {
        return e1Mdmult;
    }

    public void setE1Mdmult(double e1Mdmult) {
        this.e1Mdmult = e1Mdmult;
    }

    public double getE1Mdboni() {
        return e1Mdboni;
    }

    public void setE1Mdboni(double e1Mdboni) {
        this.e1Mdboni = e1Mdboni;
    }

    public double getE1Mddesc() {
        return e1Mddesc;
    }

    public void setE1Mddesc(double e1Mddesc) {
        this.e1Mddesc = e1Mddesc;
    }

    public double getE1Retcntr() {
        return e1Retcntr;
    }

    public void setE1Retcntr(double e1Retcntr) {
        this.e1Retcntr = e1Retcntr;
    }

    public String getE1Nodia() {
        return e1Nodia;
    }

    public void setE1Nodia(String e1Nodia) {
        this.e1Nodia = e1Nodia;
    }

    public double getE1Multdia() {
        return e1Multdia;
    }

    public void setE1Multdia(double e1Multdia) {
        this.e1Multdia = e1Multdia;
    }

    public String getE1Jurfat() {
        return e1Jurfat;
    }

    public void setE1Jurfat(String e1Jurfat) {
        this.e1Jurfat = e1Jurfat;
    }

    public String getE1Relato() {
        return e1Relato;
    }

    public void setE1Relato(String e1Relato) {
        this.e1Relato = e1Relato;
    }

    public double getE1Baseins() {
        return e1Baseins;
    }

    public void setE1Baseins(double e1Baseins) {
        this.e1Baseins = e1Baseins;
    }

    public String getE1Titpai() {
        return e1Titpai;
    }

    public void setE1Titpai(String e1Titpai) {
        this.e1Titpai = e1Titpai;
    }

    public String getE1Nfeletr() {
        return e1Nfeletr;
    }

    public void setE1Nfeletr(String e1Nfeletr) {
        this.e1Nfeletr = e1Nfeletr;
    }

    public String getE1Doctef() {
        return e1Doctef;
    }

    public void setE1Doctef(String e1Doctef) {
        this.e1Doctef = e1Doctef;
    }

    public double getE1Prinss() {
        return e1Prinss;
    }

    public void setE1Prinss(double e1Prinss) {
        this.e1Prinss = e1Prinss;
    }

    public String getE1Parcfab() {
        return e1Parcfab;
    }

    public void setE1Parcfab(String e1Parcfab) {
        this.e1Parcfab = e1Parcfab;
    }

    public String getE1Parcfac() {
        return e1Parcfac;
    }

    public void setE1Parcfac(String e1Parcfac) {
        this.e1Parcfac = e1Parcfac;
    }

    public String getE1Partpdp() {
        return e1Partpdp;
    }

    public void setE1Partpdp(String e1Partpdp) {
        this.e1Partpdp = e1Partpdp;
    }

    public String getE1Tpdesc() {
        return e1Tpdesc;
    }

    public void setE1Tpdesc(String e1Tpdesc) {
        this.e1Tpdesc = e1Tpdesc;
    }

    public double getE1Tpdp() {
        return e1Tpdp;
    }

    public void setE1Tpdp(double e1Tpdp) {
        this.e1Tpdp = e1Tpdp;
    }

    public String getE1Vlminis() {
        return e1Vlminis;
    }

    public void setE1Vlminis(String e1Vlminis) {
        this.e1Vlminis = e1Vlminis;
    }

    public double getE1Facs() {
        return e1Facs;
    }

    public void setE1Facs(double e1Facs) {
        this.e1Facs = e1Facs;
    }

    public double getE1Fabov() {
        return e1Fabov;
    }

    public void setE1Fabov(double e1Fabov) {
        this.e1Fabov = e1Fabov;
    }

    public String getE1Numcon() {
        return e1Numcon;
    }

    public void setE1Numcon(String e1Numcon) {
        this.e1Numcon = e1Numcon;
    }

    public double getE1Sabtirf() {
        return e1Sabtirf;
    }

    public void setE1Sabtirf(double e1Sabtirf) {
        this.e1Sabtirf = e1Sabtirf;
    }

    public double getE1Idlan() {
        return e1Idlan;
    }

    public void setE1Idlan(double e1Idlan) {
        this.e1Idlan = e1Idlan;
    }

    public double getE1Idbolet() {
        return e1Idbolet;
    }

    public void setE1Idbolet(double e1Idbolet) {
        this.e1Idbolet = e1Idbolet;
    }

    public String getE1Servico() {
        return e1Servico;
    }

    public void setE1Servico(String e1Servico) {
        this.e1Servico = e1Servico;
    }

    public double getE1Vlbolp() {
        return e1Vlbolp;
    }

    public void setE1Vlbolp(double e1Vlbolp) {
        this.e1Vlbolp = e1Vlbolp;
    }

    public double getE1Idaplic() {
        return e1Idaplic;
    }

    public void setE1Idaplic(double e1Idaplic) {
        this.e1Idaplic = e1Idaplic;
    }

    public double getE1Procel() {
        return e1Procel;
    }

    public void setE1Procel(double e1Procel) {
        this.e1Procel = e1Procel;
    }

    public double getE1Numinsc() {
        return e1Numinsc;
    }

    public void setE1Numinsc(double e1Numinsc) {
        this.e1Numinsc = e1Numinsc;
    }

    public String getE1Diactb() {
        return e1Diactb;
    }

    public void setE1Diactb(String e1Diactb) {
        this.e1Diactb = e1Diactb;
    }

    public String getE1Ltcxa() {
        return e1Ltcxa;
    }

    public void setE1Ltcxa(String e1Ltcxa) {
        this.e1Ltcxa = e1Ltcxa;
    }

    public double getE1Noper() {
        return e1Noper;
    }

    public void setE1Noper(double e1Noper) {
        this.e1Noper = e1Noper;
    }

    public String getE1Turma() {
        return e1Turma;
    }

    public void setE1Turma(String e1Turma) {
        this.e1Turma = e1Turma;
    }

    public String getE1Nsutef() {
        return e1Nsutef;
    }

    public void setE1Nsutef(String e1Nsutef) {
        this.e1Nsutef = e1Nsutef;
    }

    public double getE1Vretirf() {
        return e1Vretirf;
    }

    public void setE1Vretirf(double e1Vretirf) {
        this.e1Vretirf = e1Vretirf;
    }

    public String getE1Aplvlmn() {
        return e1Aplvlmn;
    }

    public void setE1Aplvlmn(String e1Aplvlmn) {
        this.e1Aplvlmn = e1Aplvlmn;
    }

    public double getE1Baseiss() {
        return e1Baseiss;
    }

    public void setE1Baseiss(double e1Baseiss) {
        this.e1Baseiss = e1Baseiss;
    }

    public String getE1Seqbx() {
        return e1Seqbx;
    }

    public void setE1Seqbx(String e1Seqbx) {
        this.e1Seqbx = e1Seqbx;
    }

    public String getE1Ratfin() {
        return e1Ratfin;
    }

    public void setE1Ratfin(String e1Ratfin) {
        this.e1Ratfin = e1Ratfin;
    }

    public String getE1Codirrf() {
        return e1Codirrf;
    }

    public void setE1Codirrf(String e1Codirrf) {
        this.e1Codirrf = e1Codirrf;
    }

    public double getE1Priss() {
        return e1Priss;
    }

    public void setE1Priss(double e1Priss) {
        this.e1Priss = e1Priss;
    }

    public String getE1Codiss() {
        return e1Codiss;
    }

    public void setE1Codiss(String e1Codiss) {
        this.e1Codiss = e1Codiss;
    }

    public String getE1Userlgi() {
        return e1Userlgi;
    }

    public void setE1Userlgi(String e1Userlgi) {
        this.e1Userlgi = e1Userlgi;
    }

    public String getE1Userlga() {
        return e1Userlga;
    }

    public void setE1Userlga(String e1Userlga) {
        this.e1Userlga = e1Userlga;
    }

    public String getE1Usernf1() {
        return e1Usernf1;
    }

    public void setE1Usernf1(String e1Usernf1) {
        this.e1Usernf1 = e1Usernf1;
    }

    public String getE1Ufilnf1() {
        return e1Ufilnf1;
    }

    public void setE1Ufilnf1(String e1Ufilnf1) {
        this.e1Ufilnf1 = e1Ufilnf1;
    }

    public String getE1Unumnf1() {
        return e1Unumnf1;
    }

    public void setE1Unumnf1(String e1Unumnf1) {
        this.e1Unumnf1 = e1Unumnf1;
    }

    public double getE1Uvalnf1() {
        return e1Uvalnf1;
    }

    public void setE1Uvalnf1(double e1Uvalnf1) {
        this.e1Uvalnf1 = e1Uvalnf1;
    }

    public String getE1Ufilnf2() {
        return e1Ufilnf2;
    }

    public void setE1Ufilnf2(String e1Ufilnf2) {
        this.e1Ufilnf2 = e1Ufilnf2;
    }

    public String getE1Usernf2() {
        return e1Usernf2;
    }

    public void setE1Usernf2(String e1Usernf2) {
        this.e1Usernf2 = e1Usernf2;
    }

    public String getE1Unumnf2() {
        return e1Unumnf2;
    }

    public void setE1Unumnf2(String e1Unumnf2) {
        this.e1Unumnf2 = e1Unumnf2;
    }

    public double getE1Uvalnf2() {
        return e1Uvalnf2;
    }

    public void setE1Uvalnf2(double e1Uvalnf2) {
        this.e1Uvalnf2 = e1Uvalnf2;
    }

    public String getE1Ufilnf3() {
        return e1Ufilnf3;
    }

    public void setE1Ufilnf3(String e1Ufilnf3) {
        this.e1Ufilnf3 = e1Ufilnf3;
    }

    public String getE1Usernf3() {
        return e1Usernf3;
    }

    public void setE1Usernf3(String e1Usernf3) {
        this.e1Usernf3 = e1Usernf3;
    }

    public String getE1Unumnf3() {
        return e1Unumnf3;
    }

    public void setE1Unumnf3(String e1Unumnf3) {
        this.e1Unumnf3 = e1Unumnf3;
    }

    public double getE1Uvalnf3() {
        return e1Uvalnf3;
    }

    public void setE1Uvalnf3(double e1Uvalnf3) {
        this.e1Uvalnf3 = e1Uvalnf3;
    }

    public String getE1Ufilnf4() {
        return e1Ufilnf4;
    }

    public void setE1Ufilnf4(String e1Ufilnf4) {
        this.e1Ufilnf4 = e1Ufilnf4;
    }

    public String getE1Usernf4() {
        return e1Usernf4;
    }

    public void setE1Usernf4(String e1Usernf4) {
        this.e1Usernf4 = e1Usernf4;
    }

    public String getE1Unumnf4() {
        return e1Unumnf4;
    }

    public void setE1Unumnf4(String e1Unumnf4) {
        this.e1Unumnf4 = e1Unumnf4;
    }

    public double getE1Uvalnf4() {
        return e1Uvalnf4;
    }

    public void setE1Uvalnf4(double e1Uvalnf4) {
        this.e1Uvalnf4 = e1Uvalnf4;
    }

    public String getE1Ufilnf5() {
        return e1Ufilnf5;
    }

    public void setE1Ufilnf5(String e1Ufilnf5) {
        this.e1Ufilnf5 = e1Ufilnf5;
    }

    public String getE1Usernf5() {
        return e1Usernf5;
    }

    public void setE1Usernf5(String e1Usernf5) {
        this.e1Usernf5 = e1Usernf5;
    }

    public String getE1Unumnf5() {
        return e1Unumnf5;
    }

    public void setE1Unumnf5(String e1Unumnf5) {
        this.e1Unumnf5 = e1Unumnf5;
    }

    public double getE1Uvalnf5() {
        return e1Uvalnf5;
    }

    public void setE1Uvalnf5(double e1Uvalnf5) {
        this.e1Uvalnf5 = e1Uvalnf5;
    }

    public String getE1Ucodbar() {
        return e1Ucodbar;
    }

    public void setE1Ucodbar(String e1Ucodbar) {
        this.e1Ucodbar = e1Ucodbar;
    }

    public String getE1Ulindig() {
        return e1Ulindig;
    }

    public void setE1Ulindig(String e1Ulindig) {
        this.e1Ulindig = e1Ulindig;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

//    public int getRECDEL() {
//        return rECDEL;
//    }
//
//    public void setRECDEL(int rECDEL) {
//        this.rECDEL = rECDEL;
//    }
    public double getE1Usitua1() {
        return e1Usitua1;
    }

    public void setE1Usitua1(double e1Usitua1) {
        this.e1Usitua1 = e1Usitua1;
    }

    public String getE1Udtbls() {
        return e1Udtbls;
    }

    public void setE1Udtbls(String e1Udtbls) {
        this.e1Udtbls = e1Udtbls;
    }

    public List<Sz1010> getSz1010s() {
        return sz1010s;
    }

    public void setSz1010s(List<Sz1010> sz1010s) {
        this.sz1010s = sz1010s;
    }

    public See010 getSee010() {
        return see010;
    }

    public void setSee010(See010 see010) {
        this.see010 = see010;
    }

    public String getE1Ulibfat() {
        return e1Ulibfat;
    }

    public void setE1Ulibfat(String e1Ulibfat) {
        this.e1Ulibfat = e1Ulibfat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Se1010)) {
            return false;
        }
        Se1010 other = (Se1010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Se1010[ rECNO=" + rECNO + " ]";
    }
}
