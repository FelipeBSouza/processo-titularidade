/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Robson
 */
@Entity
@Table(name = "SUB010")
public class Sub010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "UB_FILIAL")
    private String ubFilial;
    @Basic(optional = false)
    @Column(name = "UB_ITEM")
    private String ubItem;
    @Basic(optional = false)
    @Column(name = "UB_NUM")
    private String ubNum;
    @Basic(optional = false)
    @Column(name = "UB_PRODUTO")
    private String ubProduto;
    @Basic(optional = false)
    @Column(name = "UB_SITPROD")
    private String ubSitprod;
    @Basic(optional = false)
    @Column(name = "UB_QUANT")
    private double ubQuant;
    @Basic(optional = false)
    @Column(name = "UB_VRUNIT")
    private double ubVrunit;
    @Basic(optional = false)
    @Column(name = "UB_VLRITEM")
    private double ubVlritem;
    @Basic(optional = false)
    @Column(name = "UB_DESC")
    private double ubDesc;
    @Basic(optional = false)
    @Column(name = "UB_VALDESC")
    private double ubValdesc;
    @Basic(optional = false)
    @Column(name = "UB_ACRE")
    private double ubAcre;
    @Basic(optional = false)
    @Column(name = "UB_VALACRE")
    private double ubValacre;
    @Basic(optional = false)
    @Column(name = "UB_TES")
    private String ubTes;
    @Basic(optional = false)
    @Column(name = "UB_CF")
    private String ubCf;
    @Basic(optional = false)
    @Column(name = "UB_PRCTAB")
    private double ubPrctab;
    @Basic(optional = false)
    @Column(name = "UB_BASEICM")
    private double ubBaseicm;
    @Basic(optional = false)
    @Column(name = "UB_LOCAL")
    private String ubLocal;
    @Basic(optional = false)
    @Column(name = "UB_UM")
    private String ubUm;
    @Basic(optional = false)
    @Column(name = "UB_EMISSAO")
    private String ubEmissao;
    @Basic(optional = false)
    @Column(name = "UB_DTENTRE")
    private String ubDtentre;
    @Basic(optional = false)
    @Column(name = "UB_LOTE")
    private String ubLote;
    @Basic(optional = false)
    @Column(name = "UB_SUBLOTE")
    private String ubSublote;
    @Basic(optional = false)
    @Column(name = "UB_DTVALID")
    private String ubDtvalid;
    @Basic(optional = false)
    @Column(name = "UB_NUMPV")
    private String ubNumpv;
    @Basic(optional = false)
    @Column(name = "UB_ITEMPV")
    private String ubItempv;
    @Basic(optional = false)
    @Column(name = "UB_TABELA")
    private String ubTabela;
    @Basic(optional = false)
    @Column(name = "UB_OPC")
    private String ubOpc;
    @Basic(optional = false)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @Basic(optional = false)
    @Column(name = "UB_VLIMPOR")
    private double ubVlimpor;
    @Basic(optional = false)
    @Column(name = "UB_FCICOD")
    private String ubFcicod;
    @Basic(optional = false)
    @Column(name = "UB_UCTR")
    private String ubUctr;
    @Basic(optional = false)
    @Column(name = "UB_UMSG")
    private String ubUmsg;
    @Basic(optional = false)
    @Column(name = "UB_UITTROC")
    private String ubUittroc;
    @Basic(optional = false)
    @Column(name = "UB_UMESINI")
    private double ubUmesini;
    @Basic(optional = false)
    @Column(name = "UB_UMESCOB")
    private double ubUmescob;
    @Basic(optional = false)
    @Column(name = "UB_UIDAGA")
    private String ubUidaga;
    @Basic(optional = false)
    @Column(name = "UB_UVLCHE")
    private double ubUvlche;
    @Basic(optional = false)
    @Column(name = "UB_OPER")
    private String ubOper;
    @Basic(optional = false)
    @Column(name = "UB_ITEMPC")
    private String ubItempc;
    @Basic(optional = false)
    @Column(name = "UB_NUMPCOM")
    private String ubNumpcom;
    @Basic(optional = false)
    @Column(name = "UB_PICMRET")
    private double ubPicmret;
    @Basic(optional = false)
    @Column(name = "UB_UTARIFA")
    private String ubUtarifa;
    @Basic(optional = false)
    @Column(name = "UB_UIDAGA2")
    private String ubUidaga2;

    public Sub010() {
    }

    public Sub010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Sub010(Integer rECNO, String ubFilial, String ubItem, String ubNum, String ubProduto, String ubSitprod, double ubQuant, double ubVrunit, double ubVlritem, double ubDesc, double ubValdesc, double ubAcre, double ubValacre, String ubTes, String ubCf, double ubPrctab, double ubBaseicm, String ubLocal, String ubUm, String ubEmissao, String ubDtentre, String ubLote, String ubSublote, String ubDtvalid, String ubNumpv, String ubItempv, String ubTabela, String ubOpc, String dELET, int rECDEL, double ubVlimpor, String ubFcicod, String ubUctr, String ubUmsg, String ubUittroc, double ubUmesini, double ubUmescob, String ubUidaga, double ubUvlche, String ubOper, String ubItempc, String ubNumpcom, double ubPicmret, String ubUtarifa, String ubUidaga2) {
        this.rECNO = rECNO;
        this.ubFilial = ubFilial;
        this.ubItem = ubItem;
        this.ubNum = ubNum;
        this.ubProduto = ubProduto;
        this.ubSitprod = ubSitprod;
        this.ubQuant = ubQuant;
        this.ubVrunit = ubVrunit;
        this.ubVlritem = ubVlritem;
        this.ubDesc = ubDesc;
        this.ubValdesc = ubValdesc;
        this.ubAcre = ubAcre;
        this.ubValacre = ubValacre;
        this.ubTes = ubTes;
        this.ubCf = ubCf;
        this.ubPrctab = ubPrctab;
        this.ubBaseicm = ubBaseicm;
        this.ubLocal = ubLocal;
        this.ubUm = ubUm;
        this.ubEmissao = ubEmissao;
        this.ubDtentre = ubDtentre;
        this.ubLote = ubLote;
        this.ubSublote = ubSublote;
        this.ubDtvalid = ubDtvalid;
        this.ubNumpv = ubNumpv;
        this.ubItempv = ubItempv;
        this.ubTabela = ubTabela;
        this.ubOpc = ubOpc;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
        this.ubVlimpor = ubVlimpor;
        this.ubFcicod = ubFcicod;
        this.ubUctr = ubUctr;
        this.ubUmsg = ubUmsg;
        this.ubUittroc = ubUittroc;
        this.ubUmesini = ubUmesini;
        this.ubUmescob = ubUmescob;
        this.ubUidaga = ubUidaga;
        this.ubUvlche = ubUvlche;
        this.ubOper = ubOper;
        this.ubItempc = ubItempc;
        this.ubNumpcom = ubNumpcom;
        this.ubPicmret = ubPicmret;
        this.ubUtarifa = ubUtarifa;
        this.ubUidaga2 = ubUidaga2;
    }

    public String getUbFilial() {
        return ubFilial;
    }

    public void setUbFilial(String ubFilial) {
        this.ubFilial = ubFilial;
    }

    public String getUbItem() {
        return ubItem;
    }

    public void setUbItem(String ubItem) {
        this.ubItem = ubItem;
    }

    public String getUbNum() {
        return ubNum;
    }

    public void setUbNum(String ubNum) {
        this.ubNum = ubNum;
    }

    public String getUbProduto() {
        return ubProduto;
    }

    public void setUbProduto(String ubProduto) {
        this.ubProduto = ubProduto;
    }

    public String getUbSitprod() {
        return ubSitprod;
    }

    public void setUbSitprod(String ubSitprod) {
        this.ubSitprod = ubSitprod;
    }

    public double getUbQuant() {
        return ubQuant;
    }

    public void setUbQuant(double ubQuant) {
        this.ubQuant = ubQuant;
    }

    public double getUbVrunit() {
        return ubVrunit;
    }

    public void setUbVrunit(double ubVrunit) {
        this.ubVrunit = ubVrunit;
    }

    public double getUbVlritem() {
        return ubVlritem;
    }

    public void setUbVlritem(double ubVlritem) {
        this.ubVlritem = ubVlritem;
    }

    public double getUbDesc() {
        return ubDesc;
    }

    public void setUbDesc(double ubDesc) {
        this.ubDesc = ubDesc;
    }

    public double getUbValdesc() {
        return ubValdesc;
    }

    public void setUbValdesc(double ubValdesc) {
        this.ubValdesc = ubValdesc;
    }

    public double getUbAcre() {
        return ubAcre;
    }

    public void setUbAcre(double ubAcre) {
        this.ubAcre = ubAcre;
    }

    public double getUbValacre() {
        return ubValacre;
    }

    public void setUbValacre(double ubValacre) {
        this.ubValacre = ubValacre;
    }

    public String getUbTes() {
        return ubTes;
    }

    public void setUbTes(String ubTes) {
        this.ubTes = ubTes;
    }

    public String getUbCf() {
        return ubCf;
    }

    public void setUbCf(String ubCf) {
        this.ubCf = ubCf;
    }

    public double getUbPrctab() {
        return ubPrctab;
    }

    public void setUbPrctab(double ubPrctab) {
        this.ubPrctab = ubPrctab;
    }

    public double getUbBaseicm() {
        return ubBaseicm;
    }

    public void setUbBaseicm(double ubBaseicm) {
        this.ubBaseicm = ubBaseicm;
    }

    public String getUbLocal() {
        return ubLocal;
    }

    public void setUbLocal(String ubLocal) {
        this.ubLocal = ubLocal;
    }

    public String getUbUm() {
        return ubUm;
    }

    public void setUbUm(String ubUm) {
        this.ubUm = ubUm;
    }

    public String getUbEmissao() {
        return ubEmissao;
    }

    public void setUbEmissao(String ubEmissao) {
        this.ubEmissao = ubEmissao;
    }

    public String getUbDtentre() {
        return ubDtentre;
    }

    public void setUbDtentre(String ubDtentre) {
        this.ubDtentre = ubDtentre;
    }

    public String getUbLote() {
        return ubLote;
    }

    public void setUbLote(String ubLote) {
        this.ubLote = ubLote;
    }

    public String getUbSublote() {
        return ubSublote;
    }

    public void setUbSublote(String ubSublote) {
        this.ubSublote = ubSublote;
    }

    public String getUbDtvalid() {
        return ubDtvalid;
    }

    public void setUbDtvalid(String ubDtvalid) {
        this.ubDtvalid = ubDtvalid;
    }

    public String getUbNumpv() {
        return ubNumpv;
    }

    public void setUbNumpv(String ubNumpv) {
        this.ubNumpv = ubNumpv;
    }

    public String getUbItempv() {
        return ubItempv;
    }

    public void setUbItempv(String ubItempv) {
        this.ubItempv = ubItempv;
    }

    public String getUbTabela() {
        return ubTabela;
    }

    public void setUbTabela(String ubTabela) {
        this.ubTabela = ubTabela;
    }

    public String getUbOpc() {
        return ubOpc;
    }

    public void setUbOpc(String ubOpc) {
        this.ubOpc = ubOpc;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public double getUbVlimpor() {
        return ubVlimpor;
    }

    public void setUbVlimpor(double ubVlimpor) {
        this.ubVlimpor = ubVlimpor;
    }

    public String getUbFcicod() {
        return ubFcicod;
    }

    public void setUbFcicod(String ubFcicod) {
        this.ubFcicod = ubFcicod;
    }

    public String getUbUctr() {
        return ubUctr;
    }

    public void setUbUctr(String ubUctr) {
        this.ubUctr = ubUctr;
    }

    public String getUbUmsg() {
        return ubUmsg;
    }

    public void setUbUmsg(String ubUmsg) {
        this.ubUmsg = ubUmsg;
    }

    public String getUbUittroc() {
        return ubUittroc;
    }

    public void setUbUittroc(String ubUittroc) {
        this.ubUittroc = ubUittroc;
    }

    public double getUbUmesini() {
        return ubUmesini;
    }

    public void setUbUmesini(double ubUmesini) {
        this.ubUmesini = ubUmesini;
    }

    public double getUbUmescob() {
        return ubUmescob;
    }

    public void setUbUmescob(double ubUmescob) {
        this.ubUmescob = ubUmescob;
    }

    public String getUbUidaga() {
        return ubUidaga;
    }

    public void setUbUidaga(String ubUidaga) {
        this.ubUidaga = ubUidaga;
    }

    public double getUbUvlche() {
        return ubUvlche;
    }

    public void setUbUvlche(double ubUvlche) {
        this.ubUvlche = ubUvlche;
    }

    public String getUbOper() {
        return ubOper;
    }

    public void setUbOper(String ubOper) {
        this.ubOper = ubOper;
    }

    public String getUbItempc() {
        return ubItempc;
    }

    public void setUbItempc(String ubItempc) {
        this.ubItempc = ubItempc;
    }

    public String getUbNumpcom() {
        return ubNumpcom;
    }

    public void setUbNumpcom(String ubNumpcom) {
        this.ubNumpcom = ubNumpcom;
    }

    public double getUbPicmret() {
        return ubPicmret;
    }

    public void setUbPicmret(double ubPicmret) {
        this.ubPicmret = ubPicmret;
    }

    public String getUbUtarifa() {
        return ubUtarifa;
    }

    public void setUbUtarifa(String ubUtarifa) {
        this.ubUtarifa = ubUtarifa;
    }

    public String getUbUidaga2() {
        return ubUidaga2;
    }

    public void setUbUidaga2(String ubUidaga2) {
        this.ubUidaga2 = ubUidaga2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sub010)) {
            return false;
        }
        Sub010 other = (Sub010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gerador.Sub010[ rECNO=" + rECNO + " ]";
    }
    
}
