/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/** 
 *
 * @author ROBSON
 * TABELA DE FAQ
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(name = "AAI010", schema = "dbo")
public class Aai010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AAI_FILIAL")
    private String aaiFilial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AAI_CODFAQ")
    private String aaiCodfaq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AAI_GRUPO")
    private String aaiGrupo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AAI_CODPRO")
    private String aaiCodpro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AAI_ITEM")
    private String aaiItem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AAI_CODPRB")
    private String aaiCodprb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "AAI_SOLUC")
    private String aaiSoluc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AAI_MEMO")
    private String aaiMemo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;

    public Aai010() {
    }

    public Aai010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Aai010(Integer rECNO, String aaiFilial, String aaiCodfaq, String aaiGrupo, String aaiCodpro, String aaiItem, String aaiCodprb, String aaiSoluc, String aaiMemo, String dELET) {
        this.rECNO = rECNO;
        this.aaiFilial = aaiFilial;
        this.aaiCodfaq = aaiCodfaq;
        this.aaiGrupo = aaiGrupo;
        this.aaiCodpro = aaiCodpro;
        this.aaiItem = aaiItem;
        this.aaiCodprb = aaiCodprb;
        this.aaiSoluc = aaiSoluc;
        this.aaiMemo = aaiMemo;
        this.dELET = dELET;
    }

    public String getAaiFilial() {
        return aaiFilial;
    }

    public void setAaiFilial(String aaiFilial) {
        this.aaiFilial = aaiFilial;
    }

    public String getAaiCodfaq() {
        return aaiCodfaq;
    }

    public void setAaiCodfaq(String aaiCodfaq) {
        this.aaiCodfaq = aaiCodfaq;
    }

    public String getAaiGrupo() {
        return aaiGrupo;
    }

    public void setAaiGrupo(String aaiGrupo) {
        this.aaiGrupo = aaiGrupo;
    }

    public String getAaiCodpro() {
        return aaiCodpro;
    }

    public void setAaiCodpro(String aaiCodpro) {
        this.aaiCodpro = aaiCodpro;
    }

    public String getAaiItem() {
        return aaiItem;
    }

    public void setAaiItem(String aaiItem) {
        this.aaiItem = aaiItem;
    }

    public String getAaiCodprb() {
        return aaiCodprb;
    }

    public void setAaiCodprb(String aaiCodprb) {
        this.aaiCodprb = aaiCodprb;
    }

    public String getAaiSoluc() {
        return aaiSoluc;
    }

    public void setAaiSoluc(String aaiSoluc) {
        this.aaiSoluc = aaiSoluc;
    }

    public String getAaiMemo() {
        return aaiMemo;
    }

    public void setAaiMemo(String aaiMemo) {
        this.aaiMemo = aaiMemo;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aai010)) {
            return false;
        }
        Aai010 other = (Aai010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Aai010[ rECNO=" + rECNO + " ]";
    }
    
}
