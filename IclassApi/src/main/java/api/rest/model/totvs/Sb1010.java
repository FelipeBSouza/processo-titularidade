/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(name = "SB1010", schema = "dbo")
@Where(clause = "D_E_L_E_T_= ''")
public class Sb1010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "B1_FILIAL")
    private String b1Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_COD")
    private String b1Cod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "B1_DESC")
    private String b1Desc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_TIPO")
    private String b1Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 27)
    @Column(name = "B1_CODITE")
    private String b1Codite;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_UM")
    private String b1Um;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_LOCPAD")
    private String b1Locpad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "B1_GRUPO")
    private String b1Grupo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PICM")
    private double b1Picm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_IPI")
    private double b1Ipi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "B1_POSIPI")
    private String b1Posipi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_ESPECIE")
    private String b1Especie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_EX_NCM")
    private String b1ExNcm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_EX_NBM")
    private String b1ExNbm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_ALIQISS")
    private double b1Aliqiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_CODISS")
    private String b1Codiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_TE")
    private String b1Te;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_TS")
    private String b1Ts;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PICMRET")
    private double b1Picmret;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PICMENT")
    private double b1Picment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_IMPZFRC")
    private String b1Impzfrc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_BITMAP")
    private String b1Bitmap;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_SEGUM")
    private String b1Segum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_CONV")
    private double b1Conv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TIPCONV")
    private String b1Tipconv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_ALTER")
    private String b1Alter;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_QE")
    private double b1Qe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PRV1")
    private double b1Prv1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_EMIN")
    private double b1Emin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_CUSTD")
    private double b1Custd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_UCALSTD")
    private String b1Ucalstd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_UPRC")
    private double b1Uprc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_MCUSTD")
    private String b1Mcustd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_UCOM")
    private String b1Ucom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PESO")
    private double b1Peso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_ESTSEG")
    private double b1Estseg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_ESTFOR")
    private String b1Estfor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_FORPRZ")
    private String b1Forprz;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PE")
    private double b1Pe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TIPE")
    private String b1Tipe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_LE")
    private double b1Le;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_LM")
    private double b1Lm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_CONTA")
    private String b1Conta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_TOLER")
    private double b1Toler;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_CC")
    private String b1Cc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_ITEMCC")
    private String b1Itemcc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FAMILIA")
    private String b1Familia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_PROC")
    private String b1Proc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_QB")
    private double b1Qb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_LOJPROC")
    private String b1Lojproc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_APROPRI")
    private String b1Apropri;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TIPODEC")
    private String b1Tipodec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_ORIGEM")
    private String b1Origem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_CLASFIS")
    private String b1Clasfis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FANTASM")
    private String b1Fantasm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_RASTRO")
    private String b1Rastro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_UREV")
    private String b1Urev;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_DATREF")
    private String b1Datref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FORAEST")
    private String b1Foraest;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_COMIS")
    private double b1Comis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_MONO")
    private String b1Mono;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PERINV")
    private double b1Perinv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_DTREFP1")
    private String b1Dtrefp1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_GRTRIB")
    private String b1Grtrib;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_MRP")
    private String b1Mrp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_NOTAMIN")
    private double b1Notamin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PRVALID")
    private double b1Prvalid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_NUMCOP")
    private double b1Numcop;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_CONINI")
    private String b1Conini;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CONTSOC")
    private String b1Contsoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_IRRF")
    private String b1Irrf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_CODBAR")
    private String b1Codbar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_GRADE")
    private String b1Grade;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_FORMLOT")
    private String b1Formlot;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_FPCOD")
    private String b1Fpcod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_LOCALIZ")
    private String b1Localiz;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_OPERPAD")
    private String b1Operpad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CONTRAT")
    private String b1Contrat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_DESC_P")
    private String b1DescP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_DESC_GI")
    private String b1DescGi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_DESC_I")
    private String b1DescI;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_VLREFUS")
    private double b1Vlrefus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_IMPORT")
    private String b1Import;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "B1_OPC")
    private String b1Opc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_ANUENTE")
    private String b1Anuente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_CODOBS")
    private String b1Codobs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_SITPROD")
    private String b1Sitprod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_FABRIC")
    private String b1Fabric;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_MODELO")
    private String b1Modelo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_SETOR")
    private String b1Setor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_BALANCA")
    private String b1Balanca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_TECLA")
    private String b1Tecla;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_PRODPAI")
    private String b1Prodpai;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TIPOCQ")
    private String b1Tipocq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_SOLICIT")
    private String b1Solicit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_AGREGCU")
    private String b1Agregcu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_GRUPCOM")
    private String b1Grupcom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_QUADPRO")
    private String b1Quadpro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_DESPIMP")
    private String b1Despimp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "B1_BASE3")
    private String b1Base3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "B1_DESBSE3")
    private String b1Desbse3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_NUMCQPR")
    private double b1Numcqpr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_CONTCQP")
    private double b1Contcqp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_REVATU")
    private String b1Revatu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_INSS")
    private String b1Inss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "B1_CODEMB")
    private String b1Codemb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "B1_ESPECIF")
    private String b1Especif;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_MAT_PRI")
    private String b1MatPri;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "B1_NALNCCA")
    private String b1Nalncca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_REDINSS")
    private double b1Redinss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_ALADI")
    private String b1Aladi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_NALSH")
    private String b1Nalsh;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_REDIRRF")
    private double b1Redirrf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_TAB_IPI")
    private String b1TabIpi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_GRUDES")
    private String b1Grudes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_REDPIS")
    private double b1Redpis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_REDCOF")
    private double b1Redcof;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_DATASUB")
    private String b1Datasub;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PCSLL")
    private double b1Pcsll;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PCOFINS")
    private double b1Pcofins;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PPIS")
    private double b1Ppis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_MTBF")
    private double b1Mtbf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_MTTR")
    private double b1Mttr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FLAGSUG")
    private String b1Flagsug;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CLASSVE")
    private String b1Classve;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_MIDIA")
    private String b1Midia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_QTMIDIA")
    private double b1Qtmidia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_VLR_IPI")
    private double b1VlrIpi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_ENVOBR")
    private String b1Envobr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_QTDSER")
    private double b1Qtdser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_SERIE")
    private String b1Serie;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_FAIXAS")
    private double b1Faixas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_NROPAG")
    private double b1Nropag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "B1_ISBN")
    private String b1Isbn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "B1_TITORIG")
    private String b1Titorig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_LINGUA")
    private String b1Lingua;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_EDICAO")
    private String b1Edicao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "B1_OBSISBN")
    private String b1Obsisbn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_CLVL")
    private String b1Clvl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_ATIVO")
    private String b1Ativo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_EMAX")
    private double b1Emax;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PESBRU")
    private double b1Pesbru;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_TIPCAR")
    private String b1Tipcar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_FRACPER")
    private double b1Fracper;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_INT_ICM")
    private double b1IntIcm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_VLR_ICM")
    private double b1VlrIcm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_VLRSELO")
    private double b1Vlrselo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_CODNOR")
    private String b1Codnor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_CORPRI")
    private String b1Corpri;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_CORSEC")
    private String b1Corsec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_NICONE")
    private String b1Nicone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_ATRIB1")
    private String b1Atrib1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_ATRIB2")
    private String b1Atrib2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_ATRIB3")
    private String b1Atrib3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_REGSEQ")
    private String b1Regseq;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CPOTENC")
    private String b1Cpotenc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_POTENCI")
    private double b1Potenci;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_QTDACUM")
    private double b1Qtdacum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_QTDINIC")
    private double b1Qtdinic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_REQUIS")
    private String b1Requis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_SELO")
    private String b1Selo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_LOTVEN")
    private double b1Lotven;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "B1_OK")
    private String b1Ok;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_USAFEFO")
    private String b1Usafefo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_IAT")
    private String b1Iat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_IPPT")
    private String b1Ippt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_CNATREC")
    private String b1Cnatrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "B1_TNATREC")
    private String b1Tnatrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_REFBAS")
    private String b1Refbas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_COEFDCR")
    private double b1Coefdcr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_UMOEC")
    private double b1Umoec;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_UVLRC")
    private double b1Uvlrc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_GCCUSTO")
    private String b1Gccusto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_CCCUSTO")
    private String b1Cccusto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_VLR_PIS")
    private double b1VlrPis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_PIS")
    private String b1Pis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_PARCEI")
    private String b1Parcei;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_CLASSE")
    private String b1Classe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FUSTF")
    private String b1Fustf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_ESCRIPI")
    private String b1Escripi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_MSBLQL")
    private String b1Msblql;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_CODQAD")
    private String b1Codqad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PMACNUT")
    private double b1Pmacnut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PMICNUT")
    private double b1Pmicnut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_CODPROC")
    private String b1Codproc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_VALEPRE")
    private String b1Valepre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_TIPOBN")
    private String b1Tipobn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CRICMS")
    private String b1Cricms;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_QBP")
    private double b1Qbp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_PRODSBP")
    private String b1Prodsbp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_LOTESBP")
    private double b1Lotesbp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_TALLA")
    private String b1Talla;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_GDODIF")
    private String b1Gdodif;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_MARKUP")
    private double b1Markup;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_DTCORTE")
    private String b1Dtcorte;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_DCR")
    private String b1Dcr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_DCRII")
    private double b1Dcrii;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "B1_DIFCNAE")
    private String b1Difcnae;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TPPROD")
    private String b1Tpprod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_GRPNATR")
    private String b1Grpnatr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_DTFIMNT")
    private String b1Dtfimnt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_DCI")
    private String b1Dci;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "B1_DCRE")
    private String b1Dcre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_FECP")
    private double b1Fecp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TPREG")
    private String b1Tpreg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_VEREAN")
    private String b1Verean;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_VIGENC")
    private String b1Vigenc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_AFABOV")
    private double b1Afabov;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_VLCIF")
    private double b1Vlcif;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "B1_CHASSI")
    private String b1Chassi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_PRN944I")
    private String b1Prn944i;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_CODLAN")
    private String b1Codlan;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CARGAE")
    private String b1Cargae;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PRINCMG")
    private double b1Princmg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CSLL")
    private String b1Csll;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_COFINS")
    private String b1Cofins;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FRETISS")
    private String b1Fretiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PRFDSUL")
    private double b1Prfdsul;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_VLR_COF")
    private double b1VlrCof;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "B1_CNAE")
    private String b1Cnae;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_RETOPER")
    private String b1Retoper;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_IVAAJU")
    private String b1Ivaaju;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "B1_REGRISS")
    private String b1Regriss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_CODANT")
    private String b1Codant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_CRDEST")
    private double b1Crdest;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "B1_SELOEN")
    private String b1Seloen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_RICM65")
    private String b1Ricm65;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_PRODREC")
    private String b1Prodrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "B1_DESBSE2")
    private String b1Desbse2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "B1_COLOR")
    private String b1Color;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "B1_TIPVEC")
    private String b1Tipvec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FETHAB")
    private String b1Fethab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_ESTRORI")
    private String b1Estrori;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CALCFET")
    private String b1Calcfet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PAUTFET")
    private double b1Pautfet;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "B1_BASE")
    private String b1Base;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_ALFUMAC")
    private double b1Alfumac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "B1_BASE2")
    private String b1Base2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_GARANT")
    private String b1Garant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PERGART")
    private double b1Pergart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "B1_ADMIN")
    private String b1Admin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_TRIBMUN")
    private String b1Tribmun;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_PR43080")
    private double b1Pr43080;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_RPRODEP")
    private String b1Rprodep;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "B1_IDHIST")
    private String b1Idhist;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "B1_MSEXP")
    private String b1Msexp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "B1_PAFMD5")
    private String b1Pafmd5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_SITTRIB")
    private String b1Sittrib;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_AFACS")
    private double b1Afacs;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_AFETHAB")
    private double b1Afethab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_AJUDIF")
    private String b1Ajudif;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_ALFECOP")
    private double b1Alfecop;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_ALFECRN")
    private double b1Alfecrn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_ALFECST")
    private double b1Alfecst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CFEM")
    private String b1Cfem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_CFEMA")
    private double b1Cfema;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CFEMS")
    private String b1Cfems;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_CRDPRES")
    private double b1Crdpres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_CRICMST")
    private String b1Cricmst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_RSATIVO")
    private String b1Rsativo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TFETHAB")
    private String b1Tfethab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_TPDP")
    private String b1Tpdp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_PRDORI")
    private String b1Prdori;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_FECOP")
    private String b1Fecop;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_FECPBA")
    private double b1Fecpba;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_MEPLES")
    private String b1Meples;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_REGESIM")
    private String b1Regesim;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "B1_UEMP01")
    private String b1Uemp01;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_USER01")
    private String b1User01;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_UPC01")
    private double b1Upc01;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_UPROD02")
    private String b1Uprod02;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "B1_UEMP02")
    private String b1Uemp02;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "B1_USER02")
    private String b1User02;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_UPC02")
    private double b1Upc02;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_UCODANT")
    private String b1Ucodant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "B1_MSFIL")
    private String b1Msfil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "B1_USERLGI")
    private String b1Userlgi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "B1_USERLGA")
    private String b1Userlga;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "B1_UPRDEXT")
    private String b1Uprdext;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_UCOM1")
    private double b1Ucom1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B1_UCOM2")
    private double b1Ucom2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "B1_UITCONT")
    private String b1Uitcont;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @OneToMany(mappedBy = "sb1", fetch = FetchType.LAZY)
    @JsonManagedReference(value = "Sb1010")
    private List<Sd2010> sd2010s;
//C    @OneToMany(mappedBy = "sb1", fetch = FetchType.LAZY)
//C    @JsonManagedReference(value = "Aa3010")
//C    private List<Aa3010> aa3010s;
    @OneToMany(mappedBy = "sb1", fetch = FetchType.LAZY)
    @JsonManagedReference(value = "Sb1Adb")
    private List<Adb010> adb010s;
    @OneToMany(mappedBy = "sb1", fetch = FetchType.LAZY)
    private List<Sz2010> sz2010s;
    
    public Sb1010() {
    }

    public Sb1010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getB1Filial() {
        return b1Filial;
    }

    public void setB1Filial(String b1Filial) {
        this.b1Filial = b1Filial;
    }

    public String getB1Cod() {
        return b1Cod;
    }

    public void setB1Cod(String b1Cod) {
        this.b1Cod = b1Cod;
    }

    public String getB1Desc() {
        return b1Desc;
    }

    public void setB1Desc(String b1Desc) {
        this.b1Desc = b1Desc;
    }

    public String getB1Tipo() {
        return b1Tipo;
    }

    public void setB1Tipo(String b1Tipo) {
        this.b1Tipo = b1Tipo;
    }

    public String getB1Codite() {
        return b1Codite;
    }       

	public void setB1Codite(String b1Codite) {
        this.b1Codite = b1Codite;
    }

    public String getB1Um() {
        return b1Um;
    }

    public void setB1Um(String b1Um) {
        this.b1Um = b1Um;
    }

    public String getB1Locpad() {
        return b1Locpad;
    }

    public void setB1Locpad(String b1Locpad) {
        this.b1Locpad = b1Locpad;
    }

    public String getB1Grupo() {
        return b1Grupo;
    }

    public void setB1Grupo(String b1Grupo) {
        this.b1Grupo = b1Grupo;
    }

    public double getB1Picm() {
        return b1Picm;
    }

    public void setB1Picm(double b1Picm) {
        this.b1Picm = b1Picm;
    }

    public double getB1Ipi() {
        return b1Ipi;
    }

    public void setB1Ipi(double b1Ipi) {
        this.b1Ipi = b1Ipi;
    }

    public String getB1Posipi() {
        return b1Posipi;
    }

    public void setB1Posipi(String b1Posipi) {
        this.b1Posipi = b1Posipi;
    }

    public String getB1Especie() {
        return b1Especie;
    }

    public void setB1Especie(String b1Especie) {
        this.b1Especie = b1Especie;
    }

    public String getB1ExNcm() {
        return b1ExNcm;
    }

    public void setB1ExNcm(String b1ExNcm) {
        this.b1ExNcm = b1ExNcm;
    }

    public String getB1ExNbm() {
        return b1ExNbm;
    }

    public void setB1ExNbm(String b1ExNbm) {
        this.b1ExNbm = b1ExNbm;
    }

    public double getB1Aliqiss() {
        return b1Aliqiss;
    }

    public void setB1Aliqiss(double b1Aliqiss) {
        this.b1Aliqiss = b1Aliqiss;
    }

    public String getB1Codiss() {
        return b1Codiss;
    }

    public void setB1Codiss(String b1Codiss) {
        this.b1Codiss = b1Codiss;
    }

    public String getB1Te() {
        return b1Te;
    }

    public void setB1Te(String b1Te) {
        this.b1Te = b1Te;
    }

    public String getB1Ts() {
        return b1Ts;
    }

    public void setB1Ts(String b1Ts) {
        this.b1Ts = b1Ts;
    }

    public double getB1Picmret() {
        return b1Picmret;
    }

    public void setB1Picmret(double b1Picmret) {
        this.b1Picmret = b1Picmret;
    }

    public double getB1Picment() {
        return b1Picment;
    }

    public void setB1Picment(double b1Picment) {
        this.b1Picment = b1Picment;
    }

    public String getB1Impzfrc() {
        return b1Impzfrc;
    }

    public void setB1Impzfrc(String b1Impzfrc) {
        this.b1Impzfrc = b1Impzfrc;
    }

    public String getB1Bitmap() {
        return b1Bitmap;
    }

    public void setB1Bitmap(String b1Bitmap) {
        this.b1Bitmap = b1Bitmap;
    }

    public String getB1Segum() {
        return b1Segum;
    }

    public void setB1Segum(String b1Segum) {
        this.b1Segum = b1Segum;
    }

    public double getB1Conv() {
        return b1Conv;
    }

    public void setB1Conv(double b1Conv) {
        this.b1Conv = b1Conv;
    }

    public String getB1Tipconv() {
        return b1Tipconv;
    }

    public void setB1Tipconv(String b1Tipconv) {
        this.b1Tipconv = b1Tipconv;
    }

    public String getB1Alter() {
        return b1Alter;
    }

    public void setB1Alter(String b1Alter) {
        this.b1Alter = b1Alter;
    }

    public double getB1Qe() {
        return b1Qe;
    }

    public void setB1Qe(double b1Qe) {
        this.b1Qe = b1Qe;
    }

    public double getB1Prv1() {
        return b1Prv1;
    }

    public void setB1Prv1(double b1Prv1) {
        this.b1Prv1 = b1Prv1;
    }

    public double getB1Emin() {
        return b1Emin;
    }

    public void setB1Emin(double b1Emin) {
        this.b1Emin = b1Emin;
    }

    public double getB1Custd() {
        return b1Custd;
    }

    public void setB1Custd(double b1Custd) {
        this.b1Custd = b1Custd;
    }

    public String getB1Ucalstd() {
        return b1Ucalstd;
    }

    public void setB1Ucalstd(String b1Ucalstd) {
        this.b1Ucalstd = b1Ucalstd;
    }

    public double getB1Uprc() {
        return b1Uprc;
    }

    public void setB1Uprc(double b1Uprc) {
        this.b1Uprc = b1Uprc;
    }

    public String getB1Mcustd() {
        return b1Mcustd;
    }

    public void setB1Mcustd(String b1Mcustd) {
        this.b1Mcustd = b1Mcustd;
    }

    public String getB1Ucom() {
        return b1Ucom;
    }

    public void setB1Ucom(String b1Ucom) {
        this.b1Ucom = b1Ucom;
    }

    public double getB1Peso() {
        return b1Peso;
    }

    public void setB1Peso(double b1Peso) {
        this.b1Peso = b1Peso;
    }

    public double getB1Estseg() {
        return b1Estseg;
    }

    public void setB1Estseg(double b1Estseg) {
        this.b1Estseg = b1Estseg;
    }

    public String getB1Estfor() {
        return b1Estfor;
    }

    public void setB1Estfor(String b1Estfor) {
        this.b1Estfor = b1Estfor;
    }

    public String getB1Forprz() {
        return b1Forprz;
    }

    public void setB1Forprz(String b1Forprz) {
        this.b1Forprz = b1Forprz;
    }

    public double getB1Pe() {
        return b1Pe;
    }

    public void setB1Pe(double b1Pe) {
        this.b1Pe = b1Pe;
    }

    public String getB1Tipe() {
        return b1Tipe;
    }

    public void setB1Tipe(String b1Tipe) {
        this.b1Tipe = b1Tipe;
    }

    public double getB1Le() {
        return b1Le;
    }

    public void setB1Le(double b1Le) {
        this.b1Le = b1Le;
    }

    public double getB1Lm() {
        return b1Lm;
    }

    public void setB1Lm(double b1Lm) {
        this.b1Lm = b1Lm;
    }

    public String getB1Conta() {
        return b1Conta;
    }

    public void setB1Conta(String b1Conta) {
        this.b1Conta = b1Conta;
    }

    public double getB1Toler() {
        return b1Toler;
    }

    public void setB1Toler(double b1Toler) {
        this.b1Toler = b1Toler;
    }

    public String getB1Cc() {
        return b1Cc;
    }

    public void setB1Cc(String b1Cc) {
        this.b1Cc = b1Cc;
    }

    public String getB1Itemcc() {
        return b1Itemcc;
    }

    public void setB1Itemcc(String b1Itemcc) {
        this.b1Itemcc = b1Itemcc;
    }

    public String getB1Familia() {
        return b1Familia;
    }

    public void setB1Familia(String b1Familia) {
        this.b1Familia = b1Familia;
    }

    public String getB1Proc() {
        return b1Proc;
    }

    public void setB1Proc(String b1Proc) {
        this.b1Proc = b1Proc;
    }

    public double getB1Qb() {
        return b1Qb;
    }

    public void setB1Qb(double b1Qb) {
        this.b1Qb = b1Qb;
    }

    public String getB1Lojproc() {
        return b1Lojproc;
    }

    public void setB1Lojproc(String b1Lojproc) {
        this.b1Lojproc = b1Lojproc;
    }

    public String getB1Apropri() {
        return b1Apropri;
    }

    public void setB1Apropri(String b1Apropri) {
        this.b1Apropri = b1Apropri;
    }

    public String getB1Tipodec() {
        return b1Tipodec;
    }

    public void setB1Tipodec(String b1Tipodec) {
        this.b1Tipodec = b1Tipodec;
    }

    public String getB1Origem() {
        return b1Origem;
    }

    public void setB1Origem(String b1Origem) {
        this.b1Origem = b1Origem;
    }

    public String getB1Clasfis() {
        return b1Clasfis;
    }

    public void setB1Clasfis(String b1Clasfis) {
        this.b1Clasfis = b1Clasfis;
    }

    public String getB1Fantasm() {
        return b1Fantasm;
    }

    public void setB1Fantasm(String b1Fantasm) {
        this.b1Fantasm = b1Fantasm;
    }

    public String getB1Rastro() {
        return b1Rastro;
    }

    public void setB1Rastro(String b1Rastro) {
        this.b1Rastro = b1Rastro;
    }

    public String getB1Urev() {
        return b1Urev;
    }

    public void setB1Urev(String b1Urev) {
        this.b1Urev = b1Urev;
    }

    public String getB1Datref() {
        return b1Datref;
    }

    public void setB1Datref(String b1Datref) {
        this.b1Datref = b1Datref;
    }

    public String getB1Foraest() {
        return b1Foraest;
    }

    public void setB1Foraest(String b1Foraest) {
        this.b1Foraest = b1Foraest;
    }

    public double getB1Comis() {
        return b1Comis;
    }

    public void setB1Comis(double b1Comis) {
        this.b1Comis = b1Comis;
    }

    public String getB1Mono() {
        return b1Mono;
    }

    public void setB1Mono(String b1Mono) {
        this.b1Mono = b1Mono;
    }

    public double getB1Perinv() {
        return b1Perinv;
    }

    public void setB1Perinv(double b1Perinv) {
        this.b1Perinv = b1Perinv;
    }

    public String getB1Dtrefp1() {
        return b1Dtrefp1;
    }

    public void setB1Dtrefp1(String b1Dtrefp1) {
        this.b1Dtrefp1 = b1Dtrefp1;
    }

    public String getB1Grtrib() {
        return b1Grtrib;
    }

    public void setB1Grtrib(String b1Grtrib) {
        this.b1Grtrib = b1Grtrib;
    }

    public String getB1Mrp() {
        return b1Mrp;
    }

    public void setB1Mrp(String b1Mrp) {
        this.b1Mrp = b1Mrp;
    }

    public double getB1Notamin() {
        return b1Notamin;
    }

    public void setB1Notamin(double b1Notamin) {
        this.b1Notamin = b1Notamin;
    }

    public double getB1Prvalid() {
        return b1Prvalid;
    }

    public void setB1Prvalid(double b1Prvalid) {
        this.b1Prvalid = b1Prvalid;
    }

    public double getB1Numcop() {
        return b1Numcop;
    }

    public void setB1Numcop(double b1Numcop) {
        this.b1Numcop = b1Numcop;
    }

    public String getB1Conini() {
        return b1Conini;
    }

    public void setB1Conini(String b1Conini) {
        this.b1Conini = b1Conini;
    }

    public String getB1Contsoc() {
        return b1Contsoc;
    }

    public void setB1Contsoc(String b1Contsoc) {
        this.b1Contsoc = b1Contsoc;
    }

    public String getB1Irrf() {
        return b1Irrf;
    }

    public void setB1Irrf(String b1Irrf) {
        this.b1Irrf = b1Irrf;
    }

    public String getB1Codbar() {
        return b1Codbar;
    }

    public void setB1Codbar(String b1Codbar) {
        this.b1Codbar = b1Codbar;
    }

    public String getB1Grade() {
        return b1Grade;
    }

    public void setB1Grade(String b1Grade) {
        this.b1Grade = b1Grade;
    }

    public String getB1Formlot() {
        return b1Formlot;
    }

    public void setB1Formlot(String b1Formlot) {
        this.b1Formlot = b1Formlot;
    }

    public String getB1Fpcod() {
        return b1Fpcod;
    }

    public void setB1Fpcod(String b1Fpcod) {
        this.b1Fpcod = b1Fpcod;
    }

    public String getB1Localiz() {
        return b1Localiz;
    }

    public void setB1Localiz(String b1Localiz) {
        this.b1Localiz = b1Localiz;
    }

    public String getB1Operpad() {
        return b1Operpad;
    }

    public void setB1Operpad(String b1Operpad) {
        this.b1Operpad = b1Operpad;
    }

    public String getB1Contrat() {
        return b1Contrat;
    }

    public void setB1Contrat(String b1Contrat) {
        this.b1Contrat = b1Contrat;
    }

    public String getB1DescP() {
        return b1DescP;
    }

    public void setB1DescP(String b1DescP) {
        this.b1DescP = b1DescP;
    }

    public String getB1DescGi() {
        return b1DescGi;
    }

    public void setB1DescGi(String b1DescGi) {
        this.b1DescGi = b1DescGi;
    }

    public String getB1DescI() {
        return b1DescI;
    }

    public void setB1DescI(String b1DescI) {
        this.b1DescI = b1DescI;
    }

    public double getB1Vlrefus() {
        return b1Vlrefus;
    }

    public void setB1Vlrefus(double b1Vlrefus) {
        this.b1Vlrefus = b1Vlrefus;
    }

    public String getB1Import() {
        return b1Import;
    }

    public void setB1Import(String b1Import) {
        this.b1Import = b1Import;
    }

    public String getB1Opc() {
        return b1Opc;
    }

    public void setB1Opc(String b1Opc) {
        this.b1Opc = b1Opc;
    }

    public String getB1Anuente() {
        return b1Anuente;
    }

    public void setB1Anuente(String b1Anuente) {
        this.b1Anuente = b1Anuente;
    }

    public String getB1Codobs() {
        return b1Codobs;
    }

    public void setB1Codobs(String b1Codobs) {
        this.b1Codobs = b1Codobs;
    }

    public String getB1Sitprod() {
        return b1Sitprod;
    }

    public void setB1Sitprod(String b1Sitprod) {
        this.b1Sitprod = b1Sitprod;
    }

    public String getB1Fabric() {
        return b1Fabric;
    }

    public void setB1Fabric(String b1Fabric) {
        this.b1Fabric = b1Fabric;
    }

    public String getB1Modelo() {
        return b1Modelo;
    }

    public void setB1Modelo(String b1Modelo) {
        this.b1Modelo = b1Modelo;
    }

    public String getB1Setor() {
        return b1Setor;
    }

    public void setB1Setor(String b1Setor) {
        this.b1Setor = b1Setor;
    }

    public String getB1Balanca() {
        return b1Balanca;
    }

    public void setB1Balanca(String b1Balanca) {
        this.b1Balanca = b1Balanca;
    }

    public String getB1Tecla() {
        return b1Tecla;
    }

    public void setB1Tecla(String b1Tecla) {
        this.b1Tecla = b1Tecla;
    }

    public String getB1Prodpai() {
        return b1Prodpai;
    }

    public void setB1Prodpai(String b1Prodpai) {
        this.b1Prodpai = b1Prodpai;
    }

    public String getB1Tipocq() {
        return b1Tipocq;
    }

    public void setB1Tipocq(String b1Tipocq) {
        this.b1Tipocq = b1Tipocq;
    }

    public String getB1Solicit() {
        return b1Solicit;
    }

    public void setB1Solicit(String b1Solicit) {
        this.b1Solicit = b1Solicit;
    }

    public String getB1Agregcu() {
        return b1Agregcu;
    }

    public void setB1Agregcu(String b1Agregcu) {
        this.b1Agregcu = b1Agregcu;
    }

    public String getB1Grupcom() {
        return b1Grupcom;
    }

    public void setB1Grupcom(String b1Grupcom) {
        this.b1Grupcom = b1Grupcom;
    }

    public String getB1Quadpro() {
        return b1Quadpro;
    }

    public void setB1Quadpro(String b1Quadpro) {
        this.b1Quadpro = b1Quadpro;
    }

    public String getB1Despimp() {
        return b1Despimp;
    }

    public void setB1Despimp(String b1Despimp) {
        this.b1Despimp = b1Despimp;
    }

    public String getB1Base3() {
        return b1Base3;
    }

    public void setB1Base3(String b1Base3) {
        this.b1Base3 = b1Base3;
    }

    public String getB1Desbse3() {
        return b1Desbse3;
    }

    public void setB1Desbse3(String b1Desbse3) {
        this.b1Desbse3 = b1Desbse3;
    }

    public double getB1Numcqpr() {
        return b1Numcqpr;
    }

    public void setB1Numcqpr(double b1Numcqpr) {
        this.b1Numcqpr = b1Numcqpr;
    }

    public double getB1Contcqp() {
        return b1Contcqp;
    }

    public void setB1Contcqp(double b1Contcqp) {
        this.b1Contcqp = b1Contcqp;
    }

    public String getB1Revatu() {
        return b1Revatu;
    }

    public void setB1Revatu(String b1Revatu) {
        this.b1Revatu = b1Revatu;
    }

    public String getB1Inss() {
        return b1Inss;
    }

    public void setB1Inss(String b1Inss) {
        this.b1Inss = b1Inss;
    }

    public String getB1Codemb() {
        return b1Codemb;
    }

    public void setB1Codemb(String b1Codemb) {
        this.b1Codemb = b1Codemb;
    }

    public String getB1Especif() {
        return b1Especif;
    }

    public void setB1Especif(String b1Especif) {
        this.b1Especif = b1Especif;
    }

    public String getB1MatPri() {
        return b1MatPri;
    }

    public void setB1MatPri(String b1MatPri) {
        this.b1MatPri = b1MatPri;
    }

    public String getB1Nalncca() {
        return b1Nalncca;
    }

    public void setB1Nalncca(String b1Nalncca) {
        this.b1Nalncca = b1Nalncca;
    }

    public double getB1Redinss() {
        return b1Redinss;
    }

    public void setB1Redinss(double b1Redinss) {
        this.b1Redinss = b1Redinss;
    }

    public String getB1Aladi() {
        return b1Aladi;
    }

    public void setB1Aladi(String b1Aladi) {
        this.b1Aladi = b1Aladi;
    }

    public String getB1Nalsh() {
        return b1Nalsh;
    }

    public void setB1Nalsh(String b1Nalsh) {
        this.b1Nalsh = b1Nalsh;
    }

    public double getB1Redirrf() {
        return b1Redirrf;
    }

    public void setB1Redirrf(double b1Redirrf) {
        this.b1Redirrf = b1Redirrf;
    }

    public String getB1TabIpi() {
        return b1TabIpi;
    }

    public void setB1TabIpi(String b1TabIpi) {
        this.b1TabIpi = b1TabIpi;
    }

    public String getB1Grudes() {
        return b1Grudes;
    }

    public void setB1Grudes(String b1Grudes) {
        this.b1Grudes = b1Grudes;
    }

    public double getB1Redpis() {
        return b1Redpis;
    }

    public void setB1Redpis(double b1Redpis) {
        this.b1Redpis = b1Redpis;
    }

    public double getB1Redcof() {
        return b1Redcof;
    }

    public void setB1Redcof(double b1Redcof) {
        this.b1Redcof = b1Redcof;
    }

    public String getB1Datasub() {
        return b1Datasub;
    }

    public void setB1Datasub(String b1Datasub) {
        this.b1Datasub = b1Datasub;
    }

    public double getB1Pcsll() {
        return b1Pcsll;
    }

    public void setB1Pcsll(double b1Pcsll) {
        this.b1Pcsll = b1Pcsll;
    }

    public double getB1Pcofins() {
        return b1Pcofins;
    }

    public void setB1Pcofins(double b1Pcofins) {
        this.b1Pcofins = b1Pcofins;
    }

    public double getB1Ppis() {
        return b1Ppis;
    }

    public void setB1Ppis(double b1Ppis) {
        this.b1Ppis = b1Ppis;
    }

    public double getB1Mtbf() {
        return b1Mtbf;
    }

    public void setB1Mtbf(double b1Mtbf) {
        this.b1Mtbf = b1Mtbf;
    }

    public double getB1Mttr() {
        return b1Mttr;
    }

    public void setB1Mttr(double b1Mttr) {
        this.b1Mttr = b1Mttr;
    }

    public String getB1Flagsug() {
        return b1Flagsug;
    }

    public void setB1Flagsug(String b1Flagsug) {
        this.b1Flagsug = b1Flagsug;
    }

    public String getB1Classve() {
        return b1Classve;
    }

    public void setB1Classve(String b1Classve) {
        this.b1Classve = b1Classve;
    }

    public String getB1Midia() {
        return b1Midia;
    }

    public void setB1Midia(String b1Midia) {
        this.b1Midia = b1Midia;
    }

    public double getB1Qtmidia() {
        return b1Qtmidia;
    }

    public void setB1Qtmidia(double b1Qtmidia) {
        this.b1Qtmidia = b1Qtmidia;
    }

    public double getB1VlrIpi() {
        return b1VlrIpi;
    }

    public void setB1VlrIpi(double b1VlrIpi) {
        this.b1VlrIpi = b1VlrIpi;
    }

    public String getB1Envobr() {
        return b1Envobr;
    }

    public void setB1Envobr(String b1Envobr) {
        this.b1Envobr = b1Envobr;
    }

    public double getB1Qtdser() {
        return b1Qtdser;
    }

    public void setB1Qtdser(double b1Qtdser) {
        this.b1Qtdser = b1Qtdser;
    }

    public String getB1Serie() {
        return b1Serie;
    }

    public void setB1Serie(String b1Serie) {
        this.b1Serie = b1Serie;
    }

    public double getB1Faixas() {
        return b1Faixas;
    }

    public void setB1Faixas(double b1Faixas) {
        this.b1Faixas = b1Faixas;
    }

    public double getB1Nropag() {
        return b1Nropag;
    }

    public void setB1Nropag(double b1Nropag) {
        this.b1Nropag = b1Nropag;
    }

    public String getB1Isbn() {
        return b1Isbn;
    }

    public void setB1Isbn(String b1Isbn) {
        this.b1Isbn = b1Isbn;
    }

    public String getB1Titorig() {
        return b1Titorig;
    }

    public void setB1Titorig(String b1Titorig) {
        this.b1Titorig = b1Titorig;
    }

    public String getB1Lingua() {
        return b1Lingua;
    }

    public void setB1Lingua(String b1Lingua) {
        this.b1Lingua = b1Lingua;
    }

    public String getB1Edicao() {
        return b1Edicao;
    }

    public void setB1Edicao(String b1Edicao) {
        this.b1Edicao = b1Edicao;
    }

    public String getB1Obsisbn() {
        return b1Obsisbn;
    }

    public void setB1Obsisbn(String b1Obsisbn) {
        this.b1Obsisbn = b1Obsisbn;
    }

    public String getB1Clvl() {
        return b1Clvl;
    }

    public void setB1Clvl(String b1Clvl) {
        this.b1Clvl = b1Clvl;
    }

    public String getB1Ativo() {
        return b1Ativo;
    }

    public void setB1Ativo(String b1Ativo) {
        this.b1Ativo = b1Ativo;
    }

    public double getB1Emax() {
        return b1Emax;
    }

    public void setB1Emax(double b1Emax) {
        this.b1Emax = b1Emax;
    }

    public double getB1Pesbru() {
        return b1Pesbru;
    }

    public void setB1Pesbru(double b1Pesbru) {
        this.b1Pesbru = b1Pesbru;
    }

    public String getB1Tipcar() {
        return b1Tipcar;
    }

    public void setB1Tipcar(String b1Tipcar) {
        this.b1Tipcar = b1Tipcar;
    }

    public double getB1Fracper() {
        return b1Fracper;
    }

    public void setB1Fracper(double b1Fracper) {
        this.b1Fracper = b1Fracper;
    }

    public double getB1IntIcm() {
        return b1IntIcm;
    }

    public void setB1IntIcm(double b1IntIcm) {
        this.b1IntIcm = b1IntIcm;
    }

    public double getB1VlrIcm() {
        return b1VlrIcm;
    }

    public void setB1VlrIcm(double b1VlrIcm) {
        this.b1VlrIcm = b1VlrIcm;
    }

    public double getB1Vlrselo() {
        return b1Vlrselo;
    }

    public void setB1Vlrselo(double b1Vlrselo) {
        this.b1Vlrselo = b1Vlrselo;
    }

    public String getB1Codnor() {
        return b1Codnor;
    }

    public void setB1Codnor(String b1Codnor) {
        this.b1Codnor = b1Codnor;
    }

    public String getB1Corpri() {
        return b1Corpri;
    }

    public void setB1Corpri(String b1Corpri) {
        this.b1Corpri = b1Corpri;
    }

    public String getB1Corsec() {
        return b1Corsec;
    }

    public void setB1Corsec(String b1Corsec) {
        this.b1Corsec = b1Corsec;
    }

    public String getB1Nicone() {
        return b1Nicone;
    }

    public void setB1Nicone(String b1Nicone) {
        this.b1Nicone = b1Nicone;
    }

    public String getB1Atrib1() {
        return b1Atrib1;
    }

    public void setB1Atrib1(String b1Atrib1) {
        this.b1Atrib1 = b1Atrib1;
    }

    public String getB1Atrib2() {
        return b1Atrib2;
    }

    public void setB1Atrib2(String b1Atrib2) {
        this.b1Atrib2 = b1Atrib2;
    }

    public String getB1Atrib3() {
        return b1Atrib3;
    }

    public void setB1Atrib3(String b1Atrib3) {
        this.b1Atrib3 = b1Atrib3;
    }

    public String getB1Regseq() {
        return b1Regseq;
    }

    public void setB1Regseq(String b1Regseq) {
        this.b1Regseq = b1Regseq;
    }

    public String getB1Cpotenc() {
        return b1Cpotenc;
    }

    public void setB1Cpotenc(String b1Cpotenc) {
        this.b1Cpotenc = b1Cpotenc;
    }

    public double getB1Potenci() {
        return b1Potenci;
    }

    public void setB1Potenci(double b1Potenci) {
        this.b1Potenci = b1Potenci;
    }

    public double getB1Qtdacum() {
        return b1Qtdacum;
    }

    public void setB1Qtdacum(double b1Qtdacum) {
        this.b1Qtdacum = b1Qtdacum;
    }

    public double getB1Qtdinic() {
        return b1Qtdinic;
    }

    public void setB1Qtdinic(double b1Qtdinic) {
        this.b1Qtdinic = b1Qtdinic;
    }

    public String getB1Requis() {
        return b1Requis;
    }

    public void setB1Requis(String b1Requis) {
        this.b1Requis = b1Requis;
    }

    public String getB1Selo() {
        return b1Selo;
    }

    public void setB1Selo(String b1Selo) {
        this.b1Selo = b1Selo;
    }

    public double getB1Lotven() {
        return b1Lotven;
    }

    public void setB1Lotven(double b1Lotven) {
        this.b1Lotven = b1Lotven;
    }

    public String getB1Ok() {
        return b1Ok;
    }

    public void setB1Ok(String b1Ok) {
        this.b1Ok = b1Ok;
    }

    public String getB1Usafefo() {
        return b1Usafefo;
    }

    public void setB1Usafefo(String b1Usafefo) {
        this.b1Usafefo = b1Usafefo;
    }

    public String getB1Iat() {
        return b1Iat;
    }

    public void setB1Iat(String b1Iat) {
        this.b1Iat = b1Iat;
    }

    public String getB1Ippt() {
        return b1Ippt;
    }

    public void setB1Ippt(String b1Ippt) {
        this.b1Ippt = b1Ippt;
    }

    public String getB1Cnatrec() {
        return b1Cnatrec;
    }

    public void setB1Cnatrec(String b1Cnatrec) {
        this.b1Cnatrec = b1Cnatrec;
    }

    public String getB1Tnatrec() {
        return b1Tnatrec;
    }

    public void setB1Tnatrec(String b1Tnatrec) {
        this.b1Tnatrec = b1Tnatrec;
    }

    public String getB1Refbas() {
        return b1Refbas;
    }

    public void setB1Refbas(String b1Refbas) {
        this.b1Refbas = b1Refbas;
    }

    public double getB1Coefdcr() {
        return b1Coefdcr;
    }

    public void setB1Coefdcr(double b1Coefdcr) {
        this.b1Coefdcr = b1Coefdcr;
    }

    public double getB1Umoec() {
        return b1Umoec;
    }

    public void setB1Umoec(double b1Umoec) {
        this.b1Umoec = b1Umoec;
    }

    public double getB1Uvlrc() {
        return b1Uvlrc;
    }

    public void setB1Uvlrc(double b1Uvlrc) {
        this.b1Uvlrc = b1Uvlrc;
    }

    public String getB1Gccusto() {
        return b1Gccusto;
    }

    public void setB1Gccusto(String b1Gccusto) {
        this.b1Gccusto = b1Gccusto;
    }

    public String getB1Cccusto() {
        return b1Cccusto;
    }

    public void setB1Cccusto(String b1Cccusto) {
        this.b1Cccusto = b1Cccusto;
    }

    public double getB1VlrPis() {
        return b1VlrPis;
    }

    public void setB1VlrPis(double b1VlrPis) {
        this.b1VlrPis = b1VlrPis;
    }

    public String getB1Pis() {
        return b1Pis;
    }

    public void setB1Pis(String b1Pis) {
        this.b1Pis = b1Pis;
    }

    public String getB1Parcei() {
        return b1Parcei;
    }

    public void setB1Parcei(String b1Parcei) {
        this.b1Parcei = b1Parcei;
    }

    public String getB1Classe() {
        return b1Classe;
    }

    public void setB1Classe(String b1Classe) {
        this.b1Classe = b1Classe;
    }

    public String getB1Fustf() {
        return b1Fustf;
    }

    public void setB1Fustf(String b1Fustf) {
        this.b1Fustf = b1Fustf;
    }

    public String getB1Escripi() {
        return b1Escripi;
    }

    public void setB1Escripi(String b1Escripi) {
        this.b1Escripi = b1Escripi;
    }

    public String getB1Msblql() {
        return b1Msblql;
    }

    public void setB1Msblql(String b1Msblql) {
        this.b1Msblql = b1Msblql;
    }

    public String getB1Codqad() {
        return b1Codqad;
    }

    public void setB1Codqad(String b1Codqad) {
        this.b1Codqad = b1Codqad;
    }

    public double getB1Pmacnut() {
        return b1Pmacnut;
    }

    public void setB1Pmacnut(double b1Pmacnut) {
        this.b1Pmacnut = b1Pmacnut;
    }

    public double getB1Pmicnut() {
        return b1Pmicnut;
    }

    public void setB1Pmicnut(double b1Pmicnut) {
        this.b1Pmicnut = b1Pmicnut;
    }

    public String getB1Codproc() {
        return b1Codproc;
    }

    public void setB1Codproc(String b1Codproc) {
        this.b1Codproc = b1Codproc;
    }

    public String getB1Valepre() {
        return b1Valepre;
    }

    public void setB1Valepre(String b1Valepre) {
        this.b1Valepre = b1Valepre;
    }

    public String getB1Tipobn() {
        return b1Tipobn;
    }

    public void setB1Tipobn(String b1Tipobn) {
        this.b1Tipobn = b1Tipobn;
    }

    public String getB1Cricms() {
        return b1Cricms;
    }

    public void setB1Cricms(String b1Cricms) {
        this.b1Cricms = b1Cricms;
    }

    public double getB1Qbp() {
        return b1Qbp;
    }

    public void setB1Qbp(double b1Qbp) {
        this.b1Qbp = b1Qbp;
    }

    public String getB1Prodsbp() {
        return b1Prodsbp;
    }

    public void setB1Prodsbp(String b1Prodsbp) {
        this.b1Prodsbp = b1Prodsbp;
    }

    public double getB1Lotesbp() {
        return b1Lotesbp;
    }

    public void setB1Lotesbp(double b1Lotesbp) {
        this.b1Lotesbp = b1Lotesbp;
    }

    public String getB1Talla() {
        return b1Talla;
    }

    public void setB1Talla(String b1Talla) {
        this.b1Talla = b1Talla;
    }

    public String getB1Gdodif() {
        return b1Gdodif;
    }

    public void setB1Gdodif(String b1Gdodif) {
        this.b1Gdodif = b1Gdodif;
    }

    public double getB1Markup() {
        return b1Markup;
    }

    public void setB1Markup(double b1Markup) {
        this.b1Markup = b1Markup;
    }

    public String getB1Dtcorte() {
        return b1Dtcorte;
    }

    public void setB1Dtcorte(String b1Dtcorte) {
        this.b1Dtcorte = b1Dtcorte;
    }

    public String getB1Dcr() {
        return b1Dcr;
    }

    public void setB1Dcr(String b1Dcr) {
        this.b1Dcr = b1Dcr;
    }

    public double getB1Dcrii() {
        return b1Dcrii;
    }

    public void setB1Dcrii(double b1Dcrii) {
        this.b1Dcrii = b1Dcrii;
    }

    public String getB1Difcnae() {
        return b1Difcnae;
    }

    public void setB1Difcnae(String b1Difcnae) {
        this.b1Difcnae = b1Difcnae;
    }

    public String getB1Tpprod() {
        return b1Tpprod;
    }

    public void setB1Tpprod(String b1Tpprod) {
        this.b1Tpprod = b1Tpprod;
    }

    public String getB1Grpnatr() {
        return b1Grpnatr;
    }

    public void setB1Grpnatr(String b1Grpnatr) {
        this.b1Grpnatr = b1Grpnatr;
    }

    public String getB1Dtfimnt() {
        return b1Dtfimnt;
    }

    public void setB1Dtfimnt(String b1Dtfimnt) {
        this.b1Dtfimnt = b1Dtfimnt;
    }

    public String getB1Dci() {
        return b1Dci;
    }

    public void setB1Dci(String b1Dci) {
        this.b1Dci = b1Dci;
    }

    public String getB1Dcre() {
        return b1Dcre;
    }

    public void setB1Dcre(String b1Dcre) {
        this.b1Dcre = b1Dcre;
    }

    public double getB1Fecp() {
        return b1Fecp;
    }

    public void setB1Fecp(double b1Fecp) {
        this.b1Fecp = b1Fecp;
    }

    public String getB1Tpreg() {
        return b1Tpreg;
    }

    public void setB1Tpreg(String b1Tpreg) {
        this.b1Tpreg = b1Tpreg;
    }

    public String getB1Verean() {
        return b1Verean;
    }

    public void setB1Verean(String b1Verean) {
        this.b1Verean = b1Verean;
    }

    public String getB1Vigenc() {
        return b1Vigenc;
    }

    public void setB1Vigenc(String b1Vigenc) {
        this.b1Vigenc = b1Vigenc;
    }

    public double getB1Afabov() {
        return b1Afabov;
    }

    public void setB1Afabov(double b1Afabov) {
        this.b1Afabov = b1Afabov;
    }

    public double getB1Vlcif() {
        return b1Vlcif;
    }

    public void setB1Vlcif(double b1Vlcif) {
        this.b1Vlcif = b1Vlcif;
    }

    public String getB1Chassi() {
        return b1Chassi;
    }

    public void setB1Chassi(String b1Chassi) {
        this.b1Chassi = b1Chassi;
    }

    public String getB1Prn944i() {
        return b1Prn944i;
    }

    public void setB1Prn944i(String b1Prn944i) {
        this.b1Prn944i = b1Prn944i;
    }

    public String getB1Codlan() {
        return b1Codlan;
    }

    public void setB1Codlan(String b1Codlan) {
        this.b1Codlan = b1Codlan;
    }

    public String getB1Cargae() {
        return b1Cargae;
    }

    public void setB1Cargae(String b1Cargae) {
        this.b1Cargae = b1Cargae;
    }

    public double getB1Princmg() {
        return b1Princmg;
    }

    public void setB1Princmg(double b1Princmg) {
        this.b1Princmg = b1Princmg;
    }

    public String getB1Csll() {
        return b1Csll;
    }

    public void setB1Csll(String b1Csll) {
        this.b1Csll = b1Csll;
    }

    public String getB1Cofins() {
        return b1Cofins;
    }

    public void setB1Cofins(String b1Cofins) {
        this.b1Cofins = b1Cofins;
    }

    public String getB1Fretiss() {
        return b1Fretiss;
    }

    public void setB1Fretiss(String b1Fretiss) {
        this.b1Fretiss = b1Fretiss;
    }

    public double getB1Prfdsul() {
        return b1Prfdsul;
    }

    public void setB1Prfdsul(double b1Prfdsul) {
        this.b1Prfdsul = b1Prfdsul;
    }

    public double getB1VlrCof() {
        return b1VlrCof;
    }

    public void setB1VlrCof(double b1VlrCof) {
        this.b1VlrCof = b1VlrCof;
    }

    public String getB1Cnae() {
        return b1Cnae;
    }

    public void setB1Cnae(String b1Cnae) {
        this.b1Cnae = b1Cnae;
    }

    public String getB1Retoper() {
        return b1Retoper;
    }

    public void setB1Retoper(String b1Retoper) {
        this.b1Retoper = b1Retoper;
    }

    public String getB1Ivaaju() {
        return b1Ivaaju;
    }

    public void setB1Ivaaju(String b1Ivaaju) {
        this.b1Ivaaju = b1Ivaaju;
    }

    public String getB1Regriss() {
        return b1Regriss;
    }

    public void setB1Regriss(String b1Regriss) {
        this.b1Regriss = b1Regriss;
    }

    public String getB1Codant() {
        return b1Codant;
    }

    public void setB1Codant(String b1Codant) {
        this.b1Codant = b1Codant;
    }

    public double getB1Crdest() {
        return b1Crdest;
    }

    public void setB1Crdest(double b1Crdest) {
        this.b1Crdest = b1Crdest;
    }

    public String getB1Seloen() {
        return b1Seloen;
    }

    public void setB1Seloen(String b1Seloen) {
        this.b1Seloen = b1Seloen;
    }

    public String getB1Ricm65() {
        return b1Ricm65;
    }

    public void setB1Ricm65(String b1Ricm65) {
        this.b1Ricm65 = b1Ricm65;
    }

    public String getB1Prodrec() {
        return b1Prodrec;
    }

    public void setB1Prodrec(String b1Prodrec) {
        this.b1Prodrec = b1Prodrec;
    }

    public String getB1Desbse2() {
        return b1Desbse2;
    }

    public void setB1Desbse2(String b1Desbse2) {
        this.b1Desbse2 = b1Desbse2;
    }

    public String getB1Color() {
        return b1Color;
    }

    public void setB1Color(String b1Color) {
        this.b1Color = b1Color;
    }

    public String getB1Tipvec() {
        return b1Tipvec;
    }

    public void setB1Tipvec(String b1Tipvec) {
        this.b1Tipvec = b1Tipvec;
    }

    public String getB1Fethab() {
        return b1Fethab;
    }

    public void setB1Fethab(String b1Fethab) {
        this.b1Fethab = b1Fethab;
    }

    public String getB1Estrori() {
        return b1Estrori;
    }

    public void setB1Estrori(String b1Estrori) {
        this.b1Estrori = b1Estrori;
    }

    public String getB1Calcfet() {
        return b1Calcfet;
    }

    public void setB1Calcfet(String b1Calcfet) {
        this.b1Calcfet = b1Calcfet;
    }

    public double getB1Pautfet() {
        return b1Pautfet;
    }

    public void setB1Pautfet(double b1Pautfet) {
        this.b1Pautfet = b1Pautfet;
    }

    public String getB1Base() {
        return b1Base;
    }

    public void setB1Base(String b1Base) {
        this.b1Base = b1Base;
    }

    public double getB1Alfumac() {
        return b1Alfumac;
    }

    public void setB1Alfumac(double b1Alfumac) {
        this.b1Alfumac = b1Alfumac;
    }

    public String getB1Base2() {
        return b1Base2;
    }

    public void setB1Base2(String b1Base2) {
        this.b1Base2 = b1Base2;
    }

    public String getB1Garant() {
        return b1Garant;
    }

    public void setB1Garant(String b1Garant) {
        this.b1Garant = b1Garant;
    }

    public double getB1Pergart() {
        return b1Pergart;
    }

    public void setB1Pergart(double b1Pergart) {
        this.b1Pergart = b1Pergart;
    }

    public String getB1Admin() {
        return b1Admin;
    }

    public void setB1Admin(String b1Admin) {
        this.b1Admin = b1Admin;
    }

    public String getB1Tribmun() {
        return b1Tribmun;
    }

    public void setB1Tribmun(String b1Tribmun) {
        this.b1Tribmun = b1Tribmun;
    }

    public double getB1Pr43080() {
        return b1Pr43080;
    }

    public void setB1Pr43080(double b1Pr43080) {
        this.b1Pr43080 = b1Pr43080;
    }

    public String getB1Rprodep() {
        return b1Rprodep;
    }

    public void setB1Rprodep(String b1Rprodep) {
        this.b1Rprodep = b1Rprodep;
    }

    public String getB1Idhist() {
        return b1Idhist;
    }

    public void setB1Idhist(String b1Idhist) {
        this.b1Idhist = b1Idhist;
    }

    public String getB1Msexp() {
        return b1Msexp;
    }

    public void setB1Msexp(String b1Msexp) {
        this.b1Msexp = b1Msexp;
    }

    public String getB1Pafmd5() {
        return b1Pafmd5;
    }

    public void setB1Pafmd5(String b1Pafmd5) {
        this.b1Pafmd5 = b1Pafmd5;
    }

    public String getB1Sittrib() {
        return b1Sittrib;
    }

    public void setB1Sittrib(String b1Sittrib) {
        this.b1Sittrib = b1Sittrib;
    }

    public double getB1Afacs() {
        return b1Afacs;
    }

    public void setB1Afacs(double b1Afacs) {
        this.b1Afacs = b1Afacs;
    }

    public double getB1Afethab() {
        return b1Afethab;
    }

    public void setB1Afethab(double b1Afethab) {
        this.b1Afethab = b1Afethab;
    }

    public String getB1Ajudif() {
        return b1Ajudif;
    }

    public void setB1Ajudif(String b1Ajudif) {
        this.b1Ajudif = b1Ajudif;
    }

    public double getB1Alfecop() {
        return b1Alfecop;
    }

    public void setB1Alfecop(double b1Alfecop) {
        this.b1Alfecop = b1Alfecop;
    }

    public double getB1Alfecrn() {
        return b1Alfecrn;
    }

    public void setB1Alfecrn(double b1Alfecrn) {
        this.b1Alfecrn = b1Alfecrn;
    }

    public double getB1Alfecst() {
        return b1Alfecst;
    }

    public void setB1Alfecst(double b1Alfecst) {
        this.b1Alfecst = b1Alfecst;
    }

    public String getB1Cfem() {
        return b1Cfem;
    }

    public void setB1Cfem(String b1Cfem) {
        this.b1Cfem = b1Cfem;
    }

    public double getB1Cfema() {
        return b1Cfema;
    }

    public void setB1Cfema(double b1Cfema) {
        this.b1Cfema = b1Cfema;
    }

    public String getB1Cfems() {
        return b1Cfems;
    }

    public void setB1Cfems(String b1Cfems) {
        this.b1Cfems = b1Cfems;
    }

    public double getB1Crdpres() {
        return b1Crdpres;
    }

    public void setB1Crdpres(double b1Crdpres) {
        this.b1Crdpres = b1Crdpres;
    }

    public String getB1Cricmst() {
        return b1Cricmst;
    }

    public void setB1Cricmst(String b1Cricmst) {
        this.b1Cricmst = b1Cricmst;
    }

    public String getB1Rsativo() {
        return b1Rsativo;
    }

    public void setB1Rsativo(String b1Rsativo) {
        this.b1Rsativo = b1Rsativo;
    }

    public String getB1Tfethab() {
        return b1Tfethab;
    }

    public void setB1Tfethab(String b1Tfethab) {
        this.b1Tfethab = b1Tfethab;
    }

    public String getB1Tpdp() {
        return b1Tpdp;
    }

    public void setB1Tpdp(String b1Tpdp) {
        this.b1Tpdp = b1Tpdp;
    }

    public String getB1Prdori() {
        return b1Prdori;
    }

    public void setB1Prdori(String b1Prdori) {
        this.b1Prdori = b1Prdori;
    }

    public String getB1Fecop() {
        return b1Fecop;
    }

    public void setB1Fecop(String b1Fecop) {
        this.b1Fecop = b1Fecop;
    }

    public double getB1Fecpba() {
        return b1Fecpba;
    }

    public void setB1Fecpba(double b1Fecpba) {
        this.b1Fecpba = b1Fecpba;
    }

    public String getB1Meples() {
        return b1Meples;
    }

    public void setB1Meples(String b1Meples) {
        this.b1Meples = b1Meples;
    }

    public String getB1Regesim() {
        return b1Regesim;
    }

    public void setB1Regesim(String b1Regesim) {
        this.b1Regesim = b1Regesim;
    }

    public String getB1Uemp01() {
        return b1Uemp01;
    }

    public void setB1Uemp01(String b1Uemp01) {
        this.b1Uemp01 = b1Uemp01;
    }

    public String getB1User01() {
        return b1User01;
    }

    public void setB1User01(String b1User01) {
        this.b1User01 = b1User01;
    }

    public double getB1Upc01() {
        return b1Upc01;
    }

    public void setB1Upc01(double b1Upc01) {
        this.b1Upc01 = b1Upc01;
    }

    public String getB1Uprod02() {
        return b1Uprod02;
    }

    public void setB1Uprod02(String b1Uprod02) {
        this.b1Uprod02 = b1Uprod02;
    }

    public String getB1Uemp02() {
        return b1Uemp02;
    }

    public void setB1Uemp02(String b1Uemp02) {
        this.b1Uemp02 = b1Uemp02;
    }

    public String getB1User02() {
        return b1User02;
    }

    public void setB1User02(String b1User02) {
        this.b1User02 = b1User02;
    }

    public double getB1Upc02() {
        return b1Upc02;
    }

    public void setB1Upc02(double b1Upc02) {
        this.b1Upc02 = b1Upc02;
    }

    public String getB1Ucodant() {
        return b1Ucodant;
    }

    public void setB1Ucodant(String b1Ucodant) {
        this.b1Ucodant = b1Ucodant;
    }

    public String getB1Msfil() {
        return b1Msfil;
    }

    public void setB1Msfil(String b1Msfil) {
        this.b1Msfil = b1Msfil;
    }

    public String getB1Userlgi() {
        return b1Userlgi;
    }

    public void setB1Userlgi(String b1Userlgi) {
        this.b1Userlgi = b1Userlgi;
    }

    public String getB1Userlga() {
        return b1Userlga;
    }

    public void setB1Userlga(String b1Userlga) {
        this.b1Userlga = b1Userlga;
    }

    public String getB1Uprdext() {
        return b1Uprdext;
    }

    public void setB1Uprdext(String b1Uprdext) {
        this.b1Uprdext = b1Uprdext;
    }

    public double getB1Ucom1() {
        return b1Ucom1;
    }

    public void setB1Ucom1(double b1Ucom1) {
        this.b1Ucom1 = b1Ucom1;
    }

    public double getB1Ucom2() {
        return b1Ucom2;
    }

    public void setB1Ucom2(double b1Ucom2) {
        this.b1Ucom2 = b1Ucom2;
    }

    public String getB1Uitcont() {
        return b1Uitcont;
    }

    public void setB1Uitcont(String b1Uitcont) {
        this.b1Uitcont = b1Uitcont;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sb1010)) {
            return false;
        }
        Sb1010 other = (Sb1010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Sb1010[ rECNO=" + rECNO + " ]";
    }
}
