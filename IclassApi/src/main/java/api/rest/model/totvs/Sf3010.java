/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(name = "SF3010", schema = "dbo")
public class Sf3010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false) 
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_REPROC")
    private String f3Reproc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F3_ENTRADA")
    private String f3Entrada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F3_NFISCAL")
    private String f3Nfiscal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F3_SERIE")
    private String f3Serie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F3_CLIEFOR")
    private String f3Cliefor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F3_LOJA")
    private String f3Loja;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "F3_CFO")
    private String f3Cfo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F3_CODISS")
    private String f3Codiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F3_ESTADO")
    private String f3Estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F3_EMISSAO")
    private String f3Emissao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "F3_CONTA")
    private String f3Conta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALIQICM")
    private double f3Aliqicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALCONT")
    private double f3Valcont;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASEICM")
    private double f3Baseicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALICM")
    private double f3Valicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ISENICM")
    private double f3Isenicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_OUTRICM")
    private double f3Outricm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASEIPI")
    private double f3Baseipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALIPI")
    private double f3Valipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ISENIPI")
    private double f3Isenipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_OUTRIPI")
    private double f3Outripi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "F3_OBSERV")
    private String f3Observ;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALOBSE")
    private double f3Valobse;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ICMSRET")
    private double f3Icmsret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_TIPO")
    private String f3Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F3_LANCAM")
    private String f3Lancam;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F3_DOCOR")
    private String f3Docor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ICMSCOM")
    private double f3Icmscom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_IPIOBS")
    private double f3Ipiobs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_NRLIVRO")
    private String f3Nrlivro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ICMAUTO")
    private double f3Icmauto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASERET")
    private double f3Baseret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_FORMUL")
    private String f3Formul;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "F3_ESPECIE")
    private String f3Especie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F3_FORMULA")
    private String f3Formula;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_DESPESA")
    private double f3Despesa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F3_PDV")
    private String f3Pdv;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASIMP1")
    private double f3Basimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASIMP2")
    private double f3Basimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASIMP3")
    private double f3Basimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASIMP4")
    private double f3Basimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASIMP5")
    private double f3Basimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASIMP6")
    private double f3Basimp6;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALQIMP1")
    private double f3Alqimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALQIMP2")
    private double f3Alqimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALQIMP3")
    private double f3Alqimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALQIMP4")
    private double f3Alqimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALQIMP5")
    private double f3Alqimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALQIMP6")
    private double f3Alqimp6;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALIMP1")
    private double f3Valimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALIMP2")
    private double f3Valimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALIMP3")
    private double f3Valimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALIMP4")
    private double f3Valimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALIMP5")
    private double f3Valimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALIMP6")
    private double f3Valimp6;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_RETIMP1")
    private double f3Retimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_RETIMP2")
    private double f3Retimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_RETIMP3")
    private double f3Retimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_RETIMP4")
    private double f3Retimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_RETIMP5")
    private double f3Retimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_RETIMP6")
    private double f3Retimp6;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F3_DTLANC")
    private String f3Dtlanc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "F3_FILIAL")
    private String f3Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F3_DTCANC")
    private String f3Dtcanc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "F3_DOCPRE")
    private String f3Docpre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_ISSST")
    private String f3Issst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRDPCTR")
    private double f3Crdpctr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_CREDST")
    private String f3Credst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRDPRES")
    private double f3Crdpres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRPRELE")
    private double f3Crprele;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "F3_MDCAT79")
    private String f3Mdcat79;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 74)
    @Column(name = "F3_NFEMP")
    private String f3Nfemp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ICMSDIF")
    private double f3Icmsdif;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_TRFICM")
    private double f3Trficm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_OBSICM")
    private double f3Obsicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_OBSSOL")
    private double f3Obssol;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_SOLTRIB")
    private double f3Soltrib;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F3_CFOEXT")
    private String f3Cfoext;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_SIMPLES")
    private double f3Simples;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRDZFM")
    private double f3Crdzfm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_RECISS")
    private String f3Reciss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F3_IDENTFT")
    private String f3Identft;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRPRST")
    private double f3Crprst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CPPRODE")
    private double f3Cpprode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_TPPRODE")
    private String f3Tpprode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VFESTMT")
    private double f3Vfestmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VFECPMT")
    private double f3Vfecpmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CROUTGO")
    private double f3Croutgo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALANTI")
    private double f3Valanti;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALTPDP")
    private double f3Valtpdp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F3_CSTISS")
    private String f3Cstiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_ECF")
    private String f3Ecf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F3_CFPS")
    private String f3Cfps;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRDTRAN")
    private double f3Crdtran;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_DESCZFR")
    private double f3Desczfr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASEPS3")
    private double f3Baseps3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALIQPS3")
    private double f3Aliqps3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALPS3")
    private double f3Valps3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASECF3")
    private double f3Basecf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ALIQCF3")
    private double f3Aliqcf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALCF3")
    private double f3Valcf3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F3_EMINFE")
    private String f3Eminfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F3_NUMINI")
    private String f3Numini;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F3_NUMFIM")
    private String f3Numfim;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRDEST")
    private double f3Crdest;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "F3_NUMRPS")
    private String f3Numrps;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRPRERO")
    private double f3Crprero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ESTCRED")
    private double f3Estcred;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F3_CNAE")
    private String f3Cnae;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CREDNFE")
    private double f3Crednfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_CREDACU")
    private String f3Credacu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ISSSUB")
    private double f3Isssub;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VFECPST")
    private double f3Vfecpst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F3_HORNFE")
    private String f3Hornfe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALFET")
    private double f3Valfet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALFAB")
    private double f3Valfab;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALFAC")
    private double f3Valfac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CPRESPR")
    private double f3Cprespr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALFECP")
    private double f3Valfecp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRPRSIM")
    private double f3Crprsim;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_ANTICMS")
    private String f3Anticms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "F3_CODNFE")
    private String f3Codnfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F3_CODRSEF")
    private String f3Codrsef;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 44)
    @Column(name = "F3_CHVNFE")
    private String f3Chvnfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F3_NFELETR")
    private String f3Nfeletr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "F3_DESCRET")
    private String f3Descret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F3_CODRET")
    private String f3Codret;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BSREIN")
    private double f3Bsrein;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VREINT")
    private double f3Vreint;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VFECPMG")
    private double f3Vfecpmg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VFECPRN")
    private double f3Vfecprn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VFESTRN")
    private double f3Vfestrn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CREDPRE")
    private double f3Credpre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VFESTMG")
    private double f3Vfestmg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F3_CLASCO")
    private String f3Clasco;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F3_CLIENT")
    private String f3Client;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F3_LOJENT")
    private String f3Lojent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALTST")
    private double f3Valtst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_BASETST")
    private double f3Basetst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_ISSMAT")
    private double f3Issmat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALFDS")
    private double f3Valfds;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRPREPR")
    private double f3Crprepr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F3_CAT102")
    private String f3Cat102;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRPREPE")
    private double f3Crprepe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CRPRESP")
    private double f3Crpresp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_CROUTSP")
    private double f3Croutsp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F3_OK")
    private String f3Ok;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VALFUM")
    private double f3Valfum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VLSENAR")
    private double f3Vlsenar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VLINCMG")
    private double f3Vlincmg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_DS43080")
    private double f3Ds43080;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F3_VL43080")
    private double f3Vl43080;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;

    public Sf3010() {
    }

    public Sf3010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Sf3010(Integer rECNO, String f3Reproc, String f3Entrada, String f3Nfiscal, String f3Serie, String f3Cliefor, String f3Loja, String f3Cfo, String f3Codiss, String f3Estado, String f3Emissao, String f3Conta, double f3Aliqicm, double f3Valcont, double f3Baseicm, double f3Valicm, double f3Isenicm, double f3Outricm, double f3Baseipi, double f3Valipi, double f3Isenipi, double f3Outripi, String f3Observ, double f3Valobse, double f3Icmsret, String f3Tipo, String f3Lancam, String f3Docor, double f3Icmscom, double f3Ipiobs, String f3Nrlivro, double f3Icmauto, double f3Baseret, String f3Formul, String f3Especie, String f3Formula, double f3Despesa, String f3Pdv, double f3Basimp1, double f3Basimp2, double f3Basimp3, double f3Basimp4, double f3Basimp5, double f3Basimp6, double f3Alqimp1, double f3Alqimp2, double f3Alqimp3, double f3Alqimp4, double f3Alqimp5, double f3Alqimp6, double f3Valimp1, double f3Valimp2, double f3Valimp3, double f3Valimp4, double f3Valimp5, double f3Valimp6, double f3Retimp1, double f3Retimp2, double f3Retimp3, double f3Retimp4, double f3Retimp5, double f3Retimp6, String f3Dtlanc, String f3Filial, String f3Dtcanc, String f3Docpre, String f3Issst, double f3Crdpctr, String f3Credst, double f3Crdpres, double f3Crprele, String f3Mdcat79, String f3Nfemp, double f3Icmsdif, double f3Trficm, double f3Obsicm, double f3Obssol, double f3Soltrib, String f3Cfoext, double f3Simples, double f3Crdzfm, String f3Reciss, String f3Identft, double f3Crprst, double f3Cpprode, String f3Tpprode, double f3Vfestmt, double f3Vfecpmt, double f3Croutgo, double f3Valanti, double f3Valtpdp, String f3Cstiss, String f3Ecf, String f3Cfps, double f3Crdtran, double f3Desczfr, double f3Baseps3, double f3Aliqps3, double f3Valps3, double f3Basecf3, double f3Aliqcf3, double f3Valcf3, String f3Eminfe, String f3Numini, String f3Numfim, double f3Crdest, String f3Numrps, double f3Crprero, double f3Estcred, String f3Cnae, double f3Crednfe, String f3Credacu, double f3Isssub, double f3Vfecpst, String f3Hornfe, double f3Valfet, double f3Valfab, double f3Valfac, double f3Cprespr, double f3Valfecp, double f3Crprsim, String f3Anticms, String f3Codnfe, String f3Codrsef, String f3Chvnfe, String f3Nfeletr, String f3Descret, String f3Codret, double f3Bsrein, double f3Vreint, double f3Vfecpmg, double f3Vfecprn, double f3Vfestrn, double f3Credpre, double f3Vfestmg, String f3Clasco, String f3Client, String f3Lojent, double f3Valtst, double f3Basetst, double f3Issmat, double f3Valfds, double f3Crprepr, String f3Cat102, double f3Crprepe, double f3Crpresp, double f3Croutsp, String f3Ok, double f3Valfum, double f3Vlsenar, double f3Vlincmg, double f3Ds43080, double f3Vl43080, String dELET) {
        this.rECNO = rECNO;
        this.f3Reproc = f3Reproc;
        this.f3Entrada = f3Entrada;
        this.f3Nfiscal = f3Nfiscal;
        this.f3Serie = f3Serie;
        this.f3Cliefor = f3Cliefor;
        this.f3Loja = f3Loja;
        this.f3Cfo = f3Cfo;
        this.f3Codiss = f3Codiss;
        this.f3Estado = f3Estado;
        this.f3Emissao = f3Emissao;
        this.f3Conta = f3Conta;
        this.f3Aliqicm = f3Aliqicm;
        this.f3Valcont = f3Valcont;
        this.f3Baseicm = f3Baseicm;
        this.f3Valicm = f3Valicm;
        this.f3Isenicm = f3Isenicm;
        this.f3Outricm = f3Outricm;
        this.f3Baseipi = f3Baseipi;
        this.f3Valipi = f3Valipi;
        this.f3Isenipi = f3Isenipi;
        this.f3Outripi = f3Outripi;
        this.f3Observ = f3Observ;
        this.f3Valobse = f3Valobse;
        this.f3Icmsret = f3Icmsret;
        this.f3Tipo = f3Tipo;
        this.f3Lancam = f3Lancam;
        this.f3Docor = f3Docor;
        this.f3Icmscom = f3Icmscom;
        this.f3Ipiobs = f3Ipiobs;
        this.f3Nrlivro = f3Nrlivro;
        this.f3Icmauto = f3Icmauto;
        this.f3Baseret = f3Baseret;
        this.f3Formul = f3Formul;
        this.f3Especie = f3Especie;
        this.f3Formula = f3Formula;
        this.f3Despesa = f3Despesa;
        this.f3Pdv = f3Pdv;
        this.f3Basimp1 = f3Basimp1;
        this.f3Basimp2 = f3Basimp2;
        this.f3Basimp3 = f3Basimp3;
        this.f3Basimp4 = f3Basimp4;
        this.f3Basimp5 = f3Basimp5;
        this.f3Basimp6 = f3Basimp6;
        this.f3Alqimp1 = f3Alqimp1;
        this.f3Alqimp2 = f3Alqimp2;
        this.f3Alqimp3 = f3Alqimp3;
        this.f3Alqimp4 = f3Alqimp4;
        this.f3Alqimp5 = f3Alqimp5;
        this.f3Alqimp6 = f3Alqimp6;
        this.f3Valimp1 = f3Valimp1;
        this.f3Valimp2 = f3Valimp2;
        this.f3Valimp3 = f3Valimp3;
        this.f3Valimp4 = f3Valimp4;
        this.f3Valimp5 = f3Valimp5;
        this.f3Valimp6 = f3Valimp6;
        this.f3Retimp1 = f3Retimp1;
        this.f3Retimp2 = f3Retimp2;
        this.f3Retimp3 = f3Retimp3;
        this.f3Retimp4 = f3Retimp4;
        this.f3Retimp5 = f3Retimp5;
        this.f3Retimp6 = f3Retimp6;
        this.f3Dtlanc = f3Dtlanc;
        this.f3Filial = f3Filial;
        this.f3Dtcanc = f3Dtcanc;
        this.f3Docpre = f3Docpre;
        this.f3Issst = f3Issst;
        this.f3Crdpctr = f3Crdpctr;
        this.f3Credst = f3Credst;
        this.f3Crdpres = f3Crdpres;
        this.f3Crprele = f3Crprele;
        this.f3Mdcat79 = f3Mdcat79;
        this.f3Nfemp = f3Nfemp;
        this.f3Icmsdif = f3Icmsdif;
        this.f3Trficm = f3Trficm;
        this.f3Obsicm = f3Obsicm;
        this.f3Obssol = f3Obssol;
        this.f3Soltrib = f3Soltrib;
        this.f3Cfoext = f3Cfoext;
        this.f3Simples = f3Simples;
        this.f3Crdzfm = f3Crdzfm;
        this.f3Reciss = f3Reciss;
        this.f3Identft = f3Identft;
        this.f3Crprst = f3Crprst;
        this.f3Cpprode = f3Cpprode;
        this.f3Tpprode = f3Tpprode;
        this.f3Vfestmt = f3Vfestmt;
        this.f3Vfecpmt = f3Vfecpmt;
        this.f3Croutgo = f3Croutgo;
        this.f3Valanti = f3Valanti;
        this.f3Valtpdp = f3Valtpdp;
        this.f3Cstiss = f3Cstiss;
        this.f3Ecf = f3Ecf;
        this.f3Cfps = f3Cfps;
        this.f3Crdtran = f3Crdtran;
        this.f3Desczfr = f3Desczfr;
        this.f3Baseps3 = f3Baseps3;
        this.f3Aliqps3 = f3Aliqps3;
        this.f3Valps3 = f3Valps3;
        this.f3Basecf3 = f3Basecf3;
        this.f3Aliqcf3 = f3Aliqcf3;
        this.f3Valcf3 = f3Valcf3;
        this.f3Eminfe = f3Eminfe;
        this.f3Numini = f3Numini;
        this.f3Numfim = f3Numfim;
        this.f3Crdest = f3Crdest;
        this.f3Numrps = f3Numrps;
        this.f3Crprero = f3Crprero;
        this.f3Estcred = f3Estcred;
        this.f3Cnae = f3Cnae;
        this.f3Crednfe = f3Crednfe;
        this.f3Credacu = f3Credacu;
        this.f3Isssub = f3Isssub;
        this.f3Vfecpst = f3Vfecpst;
        this.f3Hornfe = f3Hornfe;
        this.f3Valfet = f3Valfet;
        this.f3Valfab = f3Valfab;
        this.f3Valfac = f3Valfac;
        this.f3Cprespr = f3Cprespr;
        this.f3Valfecp = f3Valfecp;
        this.f3Crprsim = f3Crprsim;
        this.f3Anticms = f3Anticms;
        this.f3Codnfe = f3Codnfe;
        this.f3Codrsef = f3Codrsef;
        this.f3Chvnfe = f3Chvnfe;
        this.f3Nfeletr = f3Nfeletr;
        this.f3Descret = f3Descret;
        this.f3Codret = f3Codret;
        this.f3Bsrein = f3Bsrein;
        this.f3Vreint = f3Vreint;
        this.f3Vfecpmg = f3Vfecpmg;
        this.f3Vfecprn = f3Vfecprn;
        this.f3Vfestrn = f3Vfestrn;
        this.f3Credpre = f3Credpre;
        this.f3Vfestmg = f3Vfestmg;
        this.f3Clasco = f3Clasco;
        this.f3Client = f3Client;
        this.f3Lojent = f3Lojent;
        this.f3Valtst = f3Valtst;
        this.f3Basetst = f3Basetst;
        this.f3Issmat = f3Issmat;
        this.f3Valfds = f3Valfds;
        this.f3Crprepr = f3Crprepr;
        this.f3Cat102 = f3Cat102;
        this.f3Crprepe = f3Crprepe;
        this.f3Crpresp = f3Crpresp;
        this.f3Croutsp = f3Croutsp;
        this.f3Ok = f3Ok;
        this.f3Valfum = f3Valfum;
        this.f3Vlsenar = f3Vlsenar;
        this.f3Vlincmg = f3Vlincmg;
        this.f3Ds43080 = f3Ds43080;
        this.f3Vl43080 = f3Vl43080;
        this.dELET = dELET;
    }

    public String getF3Reproc() {
        return f3Reproc;
    }

    public void setF3Reproc(String f3Reproc) {
        this.f3Reproc = f3Reproc;
    }

    public String getF3Entrada() {
        return f3Entrada;
    }

    public void setF3Entrada(String f3Entrada) {
        this.f3Entrada = f3Entrada;
    }

    public String getF3Nfiscal() {
        return f3Nfiscal;
    }

    public void setF3Nfiscal(String f3Nfiscal) {
        this.f3Nfiscal = f3Nfiscal;
    }

    public String getF3Serie() {
        return f3Serie;
    }

    public void setF3Serie(String f3Serie) {
        this.f3Serie = f3Serie;
    }

    public String getF3Cliefor() {
        return f3Cliefor;
    }

    public void setF3Cliefor(String f3Cliefor) {
        this.f3Cliefor = f3Cliefor;
    }

    public String getF3Loja() {
        return f3Loja;
    }

    public void setF3Loja(String f3Loja) {
        this.f3Loja = f3Loja;
    }

    public String getF3Cfo() {
        return f3Cfo;
    }

    public void setF3Cfo(String f3Cfo) {
        this.f3Cfo = f3Cfo;
    }

    public String getF3Codiss() {
        return f3Codiss;
    }

    public void setF3Codiss(String f3Codiss) {
        this.f3Codiss = f3Codiss;
    }

    public String getF3Estado() {
        return f3Estado;
    }

    public void setF3Estado(String f3Estado) {
        this.f3Estado = f3Estado;
    }

    public String getF3Emissao() {
        return f3Emissao;
    }

    public void setF3Emissao(String f3Emissao) {
        this.f3Emissao = f3Emissao;
    }

    public String getF3Conta() {
        return f3Conta;
    }

    public void setF3Conta(String f3Conta) {
        this.f3Conta = f3Conta;
    }

    public double getF3Aliqicm() {
        return f3Aliqicm;
    }

    public void setF3Aliqicm(double f3Aliqicm) {
        this.f3Aliqicm = f3Aliqicm;
    }

    public double getF3Valcont() {
        return f3Valcont;
    }

    public void setF3Valcont(double f3Valcont) {
        this.f3Valcont = f3Valcont;
    }

    public double getF3Baseicm() {
        return f3Baseicm;
    }

    public void setF3Baseicm(double f3Baseicm) {
        this.f3Baseicm = f3Baseicm;
    }

    public double getF3Valicm() {
        return f3Valicm;
    }

    public void setF3Valicm(double f3Valicm) {
        this.f3Valicm = f3Valicm;
    }

    public double getF3Isenicm() {
        return f3Isenicm;
    }

    public void setF3Isenicm(double f3Isenicm) {
        this.f3Isenicm = f3Isenicm;
    }

    public double getF3Outricm() {
        return f3Outricm;
    }

    public void setF3Outricm(double f3Outricm) {
        this.f3Outricm = f3Outricm;
    }

    public double getF3Baseipi() {
        return f3Baseipi;
    }

    public void setF3Baseipi(double f3Baseipi) {
        this.f3Baseipi = f3Baseipi;
    }

    public double getF3Valipi() {
        return f3Valipi;
    }

    public void setF3Valipi(double f3Valipi) {
        this.f3Valipi = f3Valipi;
    }

    public double getF3Isenipi() {
        return f3Isenipi;
    }

    public void setF3Isenipi(double f3Isenipi) {
        this.f3Isenipi = f3Isenipi;
    }

    public double getF3Outripi() {
        return f3Outripi;
    }

    public void setF3Outripi(double f3Outripi) {
        this.f3Outripi = f3Outripi;
    }

    public String getF3Observ() {
        return f3Observ;
    }

    public void setF3Observ(String f3Observ) {
        this.f3Observ = f3Observ;
    }

    public double getF3Valobse() {
        return f3Valobse;
    }

    public void setF3Valobse(double f3Valobse) {
        this.f3Valobse = f3Valobse;
    }

    public double getF3Icmsret() {
        return f3Icmsret;
    }

    public void setF3Icmsret(double f3Icmsret) {
        this.f3Icmsret = f3Icmsret;
    }

    public String getF3Tipo() {
        return f3Tipo;
    }

    public void setF3Tipo(String f3Tipo) {
        this.f3Tipo = f3Tipo;
    }

    public String getF3Lancam() {
        return f3Lancam;
    }

    public void setF3Lancam(String f3Lancam) {
        this.f3Lancam = f3Lancam;
    }

    public String getF3Docor() {
        return f3Docor;
    }

    public void setF3Docor(String f3Docor) {
        this.f3Docor = f3Docor;
    }

    public double getF3Icmscom() {
        return f3Icmscom;
    }

    public void setF3Icmscom(double f3Icmscom) {
        this.f3Icmscom = f3Icmscom;
    }

    public double getF3Ipiobs() {
        return f3Ipiobs;
    }

    public void setF3Ipiobs(double f3Ipiobs) {
        this.f3Ipiobs = f3Ipiobs;
    }

    public String getF3Nrlivro() {
        return f3Nrlivro;
    }

    public void setF3Nrlivro(String f3Nrlivro) {
        this.f3Nrlivro = f3Nrlivro;
    }

    public double getF3Icmauto() {
        return f3Icmauto;
    }

    public void setF3Icmauto(double f3Icmauto) {
        this.f3Icmauto = f3Icmauto;
    }

    public double getF3Baseret() {
        return f3Baseret;
    }

    public void setF3Baseret(double f3Baseret) {
        this.f3Baseret = f3Baseret;
    }

    public String getF3Formul() {
        return f3Formul;
    }

    public void setF3Formul(String f3Formul) {
        this.f3Formul = f3Formul;
    }

    public String getF3Especie() {
        return f3Especie;
    }

    public void setF3Especie(String f3Especie) {
        this.f3Especie = f3Especie;
    }

    public String getF3Formula() {
        return f3Formula;
    }

    public void setF3Formula(String f3Formula) {
        this.f3Formula = f3Formula;
    }

    public double getF3Despesa() {
        return f3Despesa;
    }

    public void setF3Despesa(double f3Despesa) {
        this.f3Despesa = f3Despesa;
    }

    public String getF3Pdv() {
        return f3Pdv;
    }

    public void setF3Pdv(String f3Pdv) {
        this.f3Pdv = f3Pdv;
    }

    public double getF3Basimp1() {
        return f3Basimp1;
    }

    public void setF3Basimp1(double f3Basimp1) {
        this.f3Basimp1 = f3Basimp1;
    }

    public double getF3Basimp2() {
        return f3Basimp2;
    }

    public void setF3Basimp2(double f3Basimp2) {
        this.f3Basimp2 = f3Basimp2;
    }

    public double getF3Basimp3() {
        return f3Basimp3;
    }

    public void setF3Basimp3(double f3Basimp3) {
        this.f3Basimp3 = f3Basimp3;
    }

    public double getF3Basimp4() {
        return f3Basimp4;
    }

    public void setF3Basimp4(double f3Basimp4) {
        this.f3Basimp4 = f3Basimp4;
    }

    public double getF3Basimp5() {
        return f3Basimp5;
    }

    public void setF3Basimp5(double f3Basimp5) {
        this.f3Basimp5 = f3Basimp5;
    }

    public double getF3Basimp6() {
        return f3Basimp6;
    }

    public void setF3Basimp6(double f3Basimp6) {
        this.f3Basimp6 = f3Basimp6;
    }

    public double getF3Alqimp1() {
        return f3Alqimp1;
    }

    public void setF3Alqimp1(double f3Alqimp1) {
        this.f3Alqimp1 = f3Alqimp1;
    }

    public double getF3Alqimp2() {
        return f3Alqimp2;
    }

    public void setF3Alqimp2(double f3Alqimp2) {
        this.f3Alqimp2 = f3Alqimp2;
    }

    public double getF3Alqimp3() {
        return f3Alqimp3;
    }

    public void setF3Alqimp3(double f3Alqimp3) {
        this.f3Alqimp3 = f3Alqimp3;
    }

    public double getF3Alqimp4() {
        return f3Alqimp4;
    }

    public void setF3Alqimp4(double f3Alqimp4) {
        this.f3Alqimp4 = f3Alqimp4;
    }

    public double getF3Alqimp5() {
        return f3Alqimp5;
    }

    public void setF3Alqimp5(double f3Alqimp5) {
        this.f3Alqimp5 = f3Alqimp5;
    }

    public double getF3Alqimp6() {
        return f3Alqimp6;
    }

    public void setF3Alqimp6(double f3Alqimp6) {
        this.f3Alqimp6 = f3Alqimp6;
    }

    public double getF3Valimp1() {
        return f3Valimp1;
    }

    public void setF3Valimp1(double f3Valimp1) {
        this.f3Valimp1 = f3Valimp1;
    }

    public double getF3Valimp2() {
        return f3Valimp2;
    }

    public void setF3Valimp2(double f3Valimp2) {
        this.f3Valimp2 = f3Valimp2;
    }

    public double getF3Valimp3() {
        return f3Valimp3;
    }

    public void setF3Valimp3(double f3Valimp3) {
        this.f3Valimp3 = f3Valimp3;
    }

    public double getF3Valimp4() {
        return f3Valimp4;
    }

    public void setF3Valimp4(double f3Valimp4) {
        this.f3Valimp4 = f3Valimp4;
    }

    public double getF3Valimp5() {
        return f3Valimp5;
    }

    public void setF3Valimp5(double f3Valimp5) {
        this.f3Valimp5 = f3Valimp5;
    }

    public double getF3Valimp6() {
        return f3Valimp6;
    }

    public void setF3Valimp6(double f3Valimp6) {
        this.f3Valimp6 = f3Valimp6;
    }

    public double getF3Retimp1() {
        return f3Retimp1;
    }

    public void setF3Retimp1(double f3Retimp1) {
        this.f3Retimp1 = f3Retimp1;
    }

    public double getF3Retimp2() {
        return f3Retimp2;
    }

    public void setF3Retimp2(double f3Retimp2) {
        this.f3Retimp2 = f3Retimp2;
    }

    public double getF3Retimp3() {
        return f3Retimp3;
    }

    public void setF3Retimp3(double f3Retimp3) {
        this.f3Retimp3 = f3Retimp3;
    }

    public double getF3Retimp4() {
        return f3Retimp4;
    }

    public void setF3Retimp4(double f3Retimp4) {
        this.f3Retimp4 = f3Retimp4;
    }

    public double getF3Retimp5() {
        return f3Retimp5;
    }

    public void setF3Retimp5(double f3Retimp5) {
        this.f3Retimp5 = f3Retimp5;
    }

    public double getF3Retimp6() {
        return f3Retimp6;
    }

    public void setF3Retimp6(double f3Retimp6) {
        this.f3Retimp6 = f3Retimp6;
    }

    public String getF3Dtlanc() {
        return f3Dtlanc;
    }

    public void setF3Dtlanc(String f3Dtlanc) {
        this.f3Dtlanc = f3Dtlanc;
    }

    public String getF3Filial() {
        return f3Filial;
    }

    public void setF3Filial(String f3Filial) {
        this.f3Filial = f3Filial;
    }

    public String getF3Dtcanc() {
        return f3Dtcanc;
    }

    public void setF3Dtcanc(String f3Dtcanc) {
        this.f3Dtcanc = f3Dtcanc;
    }

    public String getF3Docpre() {
        return f3Docpre;
    }

    public void setF3Docpre(String f3Docpre) {
        this.f3Docpre = f3Docpre;
    }

    public String getF3Issst() {
        return f3Issst;
    }

    public void setF3Issst(String f3Issst) {
        this.f3Issst = f3Issst;
    }

    public double getF3Crdpctr() {
        return f3Crdpctr;
    }

    public void setF3Crdpctr(double f3Crdpctr) {
        this.f3Crdpctr = f3Crdpctr;
    }

    public String getF3Credst() {
        return f3Credst;
    }

    public void setF3Credst(String f3Credst) {
        this.f3Credst = f3Credst;
    }

    public double getF3Crdpres() {
        return f3Crdpres;
    }

    public void setF3Crdpres(double f3Crdpres) {
        this.f3Crdpres = f3Crdpres;
    }

    public double getF3Crprele() {
        return f3Crprele;
    }

    public void setF3Crprele(double f3Crprele) {
        this.f3Crprele = f3Crprele;
    }

    public String getF3Mdcat79() {
        return f3Mdcat79;
    }

    public void setF3Mdcat79(String f3Mdcat79) {
        this.f3Mdcat79 = f3Mdcat79;
    }

    public String getF3Nfemp() {
        return f3Nfemp;
    }

    public void setF3Nfemp(String f3Nfemp) {
        this.f3Nfemp = f3Nfemp;
    }

    public double getF3Icmsdif() {
        return f3Icmsdif;
    }

    public void setF3Icmsdif(double f3Icmsdif) {
        this.f3Icmsdif = f3Icmsdif;
    }

    public double getF3Trficm() {
        return f3Trficm;
    }

    public void setF3Trficm(double f3Trficm) {
        this.f3Trficm = f3Trficm;
    }

    public double getF3Obsicm() {
        return f3Obsicm;
    }

    public void setF3Obsicm(double f3Obsicm) {
        this.f3Obsicm = f3Obsicm;
    }

    public double getF3Obssol() {
        return f3Obssol;
    }

    public void setF3Obssol(double f3Obssol) {
        this.f3Obssol = f3Obssol;
    }

    public double getF3Soltrib() {
        return f3Soltrib;
    }

    public void setF3Soltrib(double f3Soltrib) {
        this.f3Soltrib = f3Soltrib;
    }

    public String getF3Cfoext() {
        return f3Cfoext;
    }

    public void setF3Cfoext(String f3Cfoext) {
        this.f3Cfoext = f3Cfoext;
    }

    public double getF3Simples() {
        return f3Simples;
    }

    public void setF3Simples(double f3Simples) {
        this.f3Simples = f3Simples;
    }

    public double getF3Crdzfm() {
        return f3Crdzfm;
    }

    public void setF3Crdzfm(double f3Crdzfm) {
        this.f3Crdzfm = f3Crdzfm;
    }

    public String getF3Reciss() {
        return f3Reciss;
    }

    public void setF3Reciss(String f3Reciss) {
        this.f3Reciss = f3Reciss;
    }

    public String getF3Identft() {
        return f3Identft;
    }

    public void setF3Identft(String f3Identft) {
        this.f3Identft = f3Identft;
    }

    public double getF3Crprst() {
        return f3Crprst;
    }

    public void setF3Crprst(double f3Crprst) {
        this.f3Crprst = f3Crprst;
    }

    public double getF3Cpprode() {
        return f3Cpprode;
    }

    public void setF3Cpprode(double f3Cpprode) {
        this.f3Cpprode = f3Cpprode;
    }

    public String getF3Tpprode() {
        return f3Tpprode;
    }

    public void setF3Tpprode(String f3Tpprode) {
        this.f3Tpprode = f3Tpprode;
    }

    public double getF3Vfestmt() {
        return f3Vfestmt;
    }

    public void setF3Vfestmt(double f3Vfestmt) {
        this.f3Vfestmt = f3Vfestmt;
    }

    public double getF3Vfecpmt() {
        return f3Vfecpmt;
    }

    public void setF3Vfecpmt(double f3Vfecpmt) {
        this.f3Vfecpmt = f3Vfecpmt;
    }

    public double getF3Croutgo() {
        return f3Croutgo;
    }

    public void setF3Croutgo(double f3Croutgo) {
        this.f3Croutgo = f3Croutgo;
    }

    public double getF3Valanti() {
        return f3Valanti;
    }

    public void setF3Valanti(double f3Valanti) {
        this.f3Valanti = f3Valanti;
    }

    public double getF3Valtpdp() {
        return f3Valtpdp;
    }

    public void setF3Valtpdp(double f3Valtpdp) {
        this.f3Valtpdp = f3Valtpdp;
    }

    public String getF3Cstiss() {
        return f3Cstiss;
    }

    public void setF3Cstiss(String f3Cstiss) {
        this.f3Cstiss = f3Cstiss;
    }

    public String getF3Ecf() {
        return f3Ecf;
    }

    public void setF3Ecf(String f3Ecf) {
        this.f3Ecf = f3Ecf;
    }

    public String getF3Cfps() {
        return f3Cfps;
    }

    public void setF3Cfps(String f3Cfps) {
        this.f3Cfps = f3Cfps;
    }

    public double getF3Crdtran() {
        return f3Crdtran;
    }

    public void setF3Crdtran(double f3Crdtran) {
        this.f3Crdtran = f3Crdtran;
    }

    public double getF3Desczfr() {
        return f3Desczfr;
    }

    public void setF3Desczfr(double f3Desczfr) {
        this.f3Desczfr = f3Desczfr;
    }

    public double getF3Baseps3() {
        return f3Baseps3;
    }

    public void setF3Baseps3(double f3Baseps3) {
        this.f3Baseps3 = f3Baseps3;
    }

    public double getF3Aliqps3() {
        return f3Aliqps3;
    }

    public void setF3Aliqps3(double f3Aliqps3) {
        this.f3Aliqps3 = f3Aliqps3;
    }

    public double getF3Valps3() {
        return f3Valps3;
    }

    public void setF3Valps3(double f3Valps3) {
        this.f3Valps3 = f3Valps3;
    }

    public double getF3Basecf3() {
        return f3Basecf3;
    }

    public void setF3Basecf3(double f3Basecf3) {
        this.f3Basecf3 = f3Basecf3;
    }

    public double getF3Aliqcf3() {
        return f3Aliqcf3;
    }

    public void setF3Aliqcf3(double f3Aliqcf3) {
        this.f3Aliqcf3 = f3Aliqcf3;
    }

    public double getF3Valcf3() {
        return f3Valcf3;
    }

    public void setF3Valcf3(double f3Valcf3) {
        this.f3Valcf3 = f3Valcf3;
    }

    public String getF3Eminfe() {
        return f3Eminfe;
    }

    public void setF3Eminfe(String f3Eminfe) {
        this.f3Eminfe = f3Eminfe;
    }

    public String getF3Numini() {
        return f3Numini;
    }

    public void setF3Numini(String f3Numini) {
        this.f3Numini = f3Numini;
    }

    public String getF3Numfim() {
        return f3Numfim;
    }

    public void setF3Numfim(String f3Numfim) {
        this.f3Numfim = f3Numfim;
    }

    public double getF3Crdest() {
        return f3Crdest;
    }

    public void setF3Crdest(double f3Crdest) {
        this.f3Crdest = f3Crdest;
    }

    public String getF3Numrps() {
        return f3Numrps;
    }

    public void setF3Numrps(String f3Numrps) {
        this.f3Numrps = f3Numrps;
    }

    public double getF3Crprero() {
        return f3Crprero;
    }

    public void setF3Crprero(double f3Crprero) {
        this.f3Crprero = f3Crprero;
    }

    public double getF3Estcred() {
        return f3Estcred;
    }

    public void setF3Estcred(double f3Estcred) {
        this.f3Estcred = f3Estcred;
    }

    public String getF3Cnae() {
        return f3Cnae;
    }

    public void setF3Cnae(String f3Cnae) {
        this.f3Cnae = f3Cnae;
    }

    public double getF3Crednfe() {
        return f3Crednfe;
    }

    public void setF3Crednfe(double f3Crednfe) {
        this.f3Crednfe = f3Crednfe;
    }

    public String getF3Credacu() {
        return f3Credacu;
    }

    public void setF3Credacu(String f3Credacu) {
        this.f3Credacu = f3Credacu;
    }

    public double getF3Isssub() {
        return f3Isssub;
    }

    public void setF3Isssub(double f3Isssub) {
        this.f3Isssub = f3Isssub;
    }

    public double getF3Vfecpst() {
        return f3Vfecpst;
    }

    public void setF3Vfecpst(double f3Vfecpst) {
        this.f3Vfecpst = f3Vfecpst;
    }

    public String getF3Hornfe() {
        return f3Hornfe;
    }

    public void setF3Hornfe(String f3Hornfe) {
        this.f3Hornfe = f3Hornfe;
    }

    public double getF3Valfet() {
        return f3Valfet;
    }

    public void setF3Valfet(double f3Valfet) {
        this.f3Valfet = f3Valfet;
    }

    public double getF3Valfab() {
        return f3Valfab;
    }

    public void setF3Valfab(double f3Valfab) {
        this.f3Valfab = f3Valfab;
    }

    public double getF3Valfac() {
        return f3Valfac;
    }

    public void setF3Valfac(double f3Valfac) {
        this.f3Valfac = f3Valfac;
    }

    public double getF3Cprespr() {
        return f3Cprespr;
    }

    public void setF3Cprespr(double f3Cprespr) {
        this.f3Cprespr = f3Cprespr;
    }

    public double getF3Valfecp() {
        return f3Valfecp;
    }

    public void setF3Valfecp(double f3Valfecp) {
        this.f3Valfecp = f3Valfecp;
    }

    public double getF3Crprsim() {
        return f3Crprsim;
    }

    public void setF3Crprsim(double f3Crprsim) {
        this.f3Crprsim = f3Crprsim;
    }

    public String getF3Anticms() {
        return f3Anticms;
    }

    public void setF3Anticms(String f3Anticms) {
        this.f3Anticms = f3Anticms;
    }

    public String getF3Codnfe() {
        return f3Codnfe;
    }

    public void setF3Codnfe(String f3Codnfe) {
        this.f3Codnfe = f3Codnfe;
    }

    public String getF3Codrsef() {
        return f3Codrsef;
    }

    public void setF3Codrsef(String f3Codrsef) {
        this.f3Codrsef = f3Codrsef;
    }

    public String getF3Chvnfe() {
        return f3Chvnfe;
    }

    public void setF3Chvnfe(String f3Chvnfe) {
        this.f3Chvnfe = f3Chvnfe;
    }

    public String getF3Nfeletr() {
        return f3Nfeletr;
    }

    public void setF3Nfeletr(String f3Nfeletr) {
        this.f3Nfeletr = f3Nfeletr;
    }

    public String getF3Descret() {
        return f3Descret;
    }

    public void setF3Descret(String f3Descret) {
        this.f3Descret = f3Descret;
    }

    public String getF3Codret() {
        return f3Codret;
    }

    public void setF3Codret(String f3Codret) {
        this.f3Codret = f3Codret;
    }

    public double getF3Bsrein() {
        return f3Bsrein;
    }

    public void setF3Bsrein(double f3Bsrein) {
        this.f3Bsrein = f3Bsrein;
    }

    public double getF3Vreint() {
        return f3Vreint;
    }

    public void setF3Vreint(double f3Vreint) {
        this.f3Vreint = f3Vreint;
    }

    public double getF3Vfecpmg() {
        return f3Vfecpmg;
    }

    public void setF3Vfecpmg(double f3Vfecpmg) {
        this.f3Vfecpmg = f3Vfecpmg;
    }

    public double getF3Vfecprn() {
        return f3Vfecprn;
    }

    public void setF3Vfecprn(double f3Vfecprn) {
        this.f3Vfecprn = f3Vfecprn;
    }

    public double getF3Vfestrn() {
        return f3Vfestrn;
    }

    public void setF3Vfestrn(double f3Vfestrn) {
        this.f3Vfestrn = f3Vfestrn;
    }

    public double getF3Credpre() {
        return f3Credpre;
    }

    public void setF3Credpre(double f3Credpre) {
        this.f3Credpre = f3Credpre;
    }

    public double getF3Vfestmg() {
        return f3Vfestmg;
    }

    public void setF3Vfestmg(double f3Vfestmg) {
        this.f3Vfestmg = f3Vfestmg;
    }

    public String getF3Clasco() {
        return f3Clasco;
    }

    public void setF3Clasco(String f3Clasco) {
        this.f3Clasco = f3Clasco;
    }

    public String getF3Client() {
        return f3Client;
    }

    public void setF3Client(String f3Client) {
        this.f3Client = f3Client;
    }

    public String getF3Lojent() {
        return f3Lojent;
    }

    public void setF3Lojent(String f3Lojent) {
        this.f3Lojent = f3Lojent;
    }

    public double getF3Valtst() {
        return f3Valtst;
    }

    public void setF3Valtst(double f3Valtst) {
        this.f3Valtst = f3Valtst;
    }

    public double getF3Basetst() {
        return f3Basetst;
    }

    public void setF3Basetst(double f3Basetst) {
        this.f3Basetst = f3Basetst;
    }

    public double getF3Issmat() {
        return f3Issmat;
    }

    public void setF3Issmat(double f3Issmat) {
        this.f3Issmat = f3Issmat;
    }

    public double getF3Valfds() {
        return f3Valfds;
    }

    public void setF3Valfds(double f3Valfds) {
        this.f3Valfds = f3Valfds;
    }

    public double getF3Crprepr() {
        return f3Crprepr;
    }

    public void setF3Crprepr(double f3Crprepr) {
        this.f3Crprepr = f3Crprepr;
    }

    public String getF3Cat102() {
        return f3Cat102;
    }

    public void setF3Cat102(String f3Cat102) {
        this.f3Cat102 = f3Cat102;
    }

    public double getF3Crprepe() {
        return f3Crprepe;
    }

    public void setF3Crprepe(double f3Crprepe) {
        this.f3Crprepe = f3Crprepe;
    }

    public double getF3Crpresp() {
        return f3Crpresp;
    }

    public void setF3Crpresp(double f3Crpresp) {
        this.f3Crpresp = f3Crpresp;
    }

    public double getF3Croutsp() {
        return f3Croutsp;
    }

    public void setF3Croutsp(double f3Croutsp) {
        this.f3Croutsp = f3Croutsp;
    }

    public String getF3Ok() {
        return f3Ok;
    }

    public void setF3Ok(String f3Ok) {
        this.f3Ok = f3Ok;
    }

    public double getF3Valfum() {
        return f3Valfum;
    }

    public void setF3Valfum(double f3Valfum) {
        this.f3Valfum = f3Valfum;
    }

    public double getF3Vlsenar() {
        return f3Vlsenar;
    }

    public void setF3Vlsenar(double f3Vlsenar) {
        this.f3Vlsenar = f3Vlsenar;
    }

    public double getF3Vlincmg() {
        return f3Vlincmg;
    }

    public void setF3Vlincmg(double f3Vlincmg) {
        this.f3Vlincmg = f3Vlincmg;
    }

    public double getF3Ds43080() {
        return f3Ds43080;
    }

    public void setF3Ds43080(double f3Ds43080) {
        this.f3Ds43080 = f3Ds43080;
    }

    public double getF3Vl43080() {
        return f3Vl43080;
    }

    public void setF3Vl43080(double f3Vl43080) {
        this.f3Vl43080 = f3Vl43080;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sf3010)) {
            return false;
        }
        Sf3010 other = (Sf3010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Sf3010[ rECNO=" + rECNO + " ]";
    }
    
}
