/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement 
@Entity
@Table(name = "SEE010",  schema = "dbo")
public class See010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "EE_FILIAL")
    private String eeFilial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "EE_CODIGO")
    private String eeCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "EE_AGENCIA")
    private String eeAgencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "EE_CONTA")
    private String eeConta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_DVAGE")
    private String eeDvage;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_DVCTA")
    private String eeDvcta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "EE_SUBCTA")
    private String eeSubcta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "EE_ULTDSK")
    private String eeUltdsk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "EE_NUMBCO")
    private String eeNumbco;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_DC")
    private String eeDc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_NUMAUT")
    private String eeNumaut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_CEPDEP")
    private String eeCepdep;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "EE_OPER")
    private String eeOper;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EE_IOF")
    private double eeIof;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "EE_INSTPRI")
    private String eeInstpri;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "EE_INSTSEC")
    private String eeInstsec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "EE_EXTEN")
    private String eeExten;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "EE_FAXINI")
    private String eeFaxini;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "EE_FAXFIM")
    private String eeFaxfim;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "EE_FAXATU")
    private String eeFaxatu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "EE_CODEMP")
    private String eeCodemp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "EE_TABELA")
    private String eeTabela;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_DESPCRD")
    private String eeDespcrd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "EE_LOTE")
    private String eeLote;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EE_NRBYTES")
    private double eeNrbytes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EE_TIPODAT")
    private double eeTipodat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "EE_LOTECP")
    private String eeLotecp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EE_BYTESXT")
    private double eeBytesxt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_COBELET")
    private String eeCobelet;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "EE_CODCOBE")
    private String eeCodcobe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_FIMLIN")
    private String eeFimlin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_RETAUT")
    private String eeRetaut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_AGLCTB")
    private String eeAglctb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_ATUMOE")
    private String eeAtumoe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EE_DIRPAG")
    private String eeDirpag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EE_DIRREC")
    private String eeDirrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "EE_CFGPAG")
    private String eeCfgpag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "EE_CFGREC")
    private String eeCfgrec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EE_BKPPAG")
    private String eeBkppag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EE_BKPREC")
    private String eeBkprec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_DESCOM")
    private String eeDescom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_CNABPG")
    private String eeCnabpg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_CNABRC")
    private String eeCnabrc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_PROCFL")
    private String eeProcfl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_MULTNT")
    private String eeMultnt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_CTBTRF")
    private String eeCtbtrf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EE_RETBCO")
    private String eeRetbco;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "EE_FORMEN1")
    private String eeFormen1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "EE_FORMEN2")
    private String eeFormen2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "EE_FOREXT1")
    private String eeForext1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "EE_FOREXT2")
    private String eeForext2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "EE_DIASPRT")
    private String eeDiasprt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "EE_CODCART")
    private String eeCodcart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "EE_UMSG1")
    private String eeUmsg1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "EE_UMSG2")
    private String eeUmsg2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "R_E_C_D_E_L_")
//    private int rECDEL;

    @Size(max = 50)
    @Column(name = "EE_UCGC")
    private String eeUCgc;


    @Size(max = 50)
    @Column(name = "EE_UDSCED")
    private String eeUdsCed;
    @OneToMany(mappedBy = "see010")
    @JsonManagedReference(value="see010")
    private List<Se1010> se1010s;

    public See010() {
    }

    public See010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public See010(Integer rECNO, String eeFilial, String eeCodigo, String eeAgencia, String eeDvage, String eeConta, String eeDvcta, String eeSubcta, String eeUltdsk, String eeNumbco, String eeDc, String eeNumaut, String eeCepdep, String eeOper, double eeIof, String eeInstpri, String eeInstsec, String eeExten, String eeFaxini, String eeFaxfim, String eeFaxatu, String eeCodemp, String eeTabela, String eeDespcrd, String eeLote, double eeNrbytes, double eeTipodat, String eeLotecp, double eeBytesxt, String eeCobelet, String eeCodcobe, String eeFimlin, String eeRetaut, String eeAglctb, String eeAtumoe, String eeDirpag, String eeDirrec, String eeCfgpag, String eeCfgrec, String eeBkppag, String eeBkprec, String eeDescom, String eeCnabpg, String eeCnabrc, String eeProcfl, String eeMultnt, String eeCtbtrf, String eeRetbco, String eeFormen1, String eeFormen2, String eeForext1, String eeForext2, String eeDiasprt, String eeCodcart, String eeUmsg1, String eeUmsg2, String dELET, int rECDEL) {
        this.rECNO = rECNO;
        this.eeFilial = eeFilial;
        this.eeCodigo = eeCodigo;
        this.eeAgencia = eeAgencia;
        this.eeDvage = eeDvage;
        this.eeConta = eeConta;
        this.eeDvcta = eeDvcta;
        this.eeSubcta = eeSubcta;
        this.eeUltdsk = eeUltdsk;
        this.eeNumbco = eeNumbco;
        this.eeDc = eeDc;
        this.eeNumaut = eeNumaut;
        this.eeCepdep = eeCepdep;
        this.eeOper = eeOper;
        this.eeIof = eeIof;
        this.eeInstpri = eeInstpri;
        this.eeInstsec = eeInstsec;
        this.eeExten = eeExten;
        this.eeFaxini = eeFaxini;
        this.eeFaxfim = eeFaxfim;
        this.eeFaxatu = eeFaxatu;
        this.eeCodemp = eeCodemp;
        this.eeTabela = eeTabela;
        this.eeDespcrd = eeDespcrd;
        this.eeLote = eeLote;
        this.eeNrbytes = eeNrbytes;
        this.eeTipodat = eeTipodat;
        this.eeLotecp = eeLotecp;
        this.eeBytesxt = eeBytesxt;
        this.eeCobelet = eeCobelet;
        this.eeCodcobe = eeCodcobe;
        this.eeFimlin = eeFimlin;
        this.eeRetaut = eeRetaut;
        this.eeAglctb = eeAglctb;
        this.eeAtumoe = eeAtumoe;
        this.eeDirpag = eeDirpag;
        this.eeDirrec = eeDirrec;
        this.eeCfgpag = eeCfgpag;
        this.eeCfgrec = eeCfgrec;
        this.eeBkppag = eeBkppag;
        this.eeBkprec = eeBkprec;
        this.eeDescom = eeDescom;
        this.eeCnabpg = eeCnabpg;
        this.eeCnabrc = eeCnabrc;
        this.eeProcfl = eeProcfl;
        this.eeMultnt = eeMultnt;
        this.eeCtbtrf = eeCtbtrf;
        this.eeRetbco = eeRetbco;
        this.eeFormen1 = eeFormen1;
        this.eeFormen2 = eeFormen2;
        this.eeForext1 = eeForext1;
        this.eeForext2 = eeForext2;
        this.eeDiasprt = eeDiasprt;
        this.eeCodcart = eeCodcart;
        this.eeUmsg1 = eeUmsg1;
        this.eeUmsg2 = eeUmsg2;
        this.dELET = dELET;
    }

    public String getEeFilial() {
        return eeFilial;
    }

    public void setEeFilial(String eeFilial) {
        this.eeFilial = eeFilial;
    }

    public String getEeCodigo() {
        return eeCodigo;
    }

    public void setEeCodigo(String eeCodigo) {
        this.eeCodigo = eeCodigo;
    }

    public String getEeAgencia() {
        return eeAgencia;
    }

    public void setEeAgencia(String eeAgencia) {
        this.eeAgencia = eeAgencia;
    }

    public String getEeDvage() {
        return eeDvage;
    }

    public void setEeDvage(String eeDvage) {
        this.eeDvage = eeDvage;
    }

    public String getEeConta() {
        return eeConta;
    }

    public void setEeConta(String eeConta) {
        this.eeConta = eeConta;
    }

    public String getEeDvcta() {
        return eeDvcta;
    }

    public void setEeDvcta(String eeDvcta) {
        this.eeDvcta = eeDvcta;
    }

    public String getEeSubcta() {
        return eeSubcta;
    }

    public void setEeSubcta(String eeSubcta) {
        this.eeSubcta = eeSubcta;
    }

    public String getEeUltdsk() {
        return eeUltdsk;
    }

    public void setEeUltdsk(String eeUltdsk) {
        this.eeUltdsk = eeUltdsk;
    }

    public String getEeNumbco() {
        return eeNumbco;
    }

    public void setEeNumbco(String eeNumbco) {
        this.eeNumbco = eeNumbco;
    }

    public String getEeDc() {
        return eeDc;
    }

    public void setEeDc(String eeDc) {
        this.eeDc = eeDc;
    }

    public String getEeNumaut() {
        return eeNumaut;
    }

    public void setEeNumaut(String eeNumaut) {
        this.eeNumaut = eeNumaut;
    }

    public String getEeCepdep() {
        return eeCepdep;
    }

    public void setEeCepdep(String eeCepdep) {
        this.eeCepdep = eeCepdep;
    }

    public String getEeOper() {
        return eeOper;
    }

    public void setEeOper(String eeOper) {
        this.eeOper = eeOper;
    }

    public double getEeIof() {
        return eeIof;
    }

    public void setEeIof(double eeIof) {
        this.eeIof = eeIof;
    }

    public String getEeInstpri() {
        return eeInstpri;
    }

    public void setEeInstpri(String eeInstpri) {
        this.eeInstpri = eeInstpri;
    }

    public String getEeInstsec() {
        return eeInstsec;
    }

    public void setEeInstsec(String eeInstsec) {
        this.eeInstsec = eeInstsec;
    }

    public String getEeExten() {
        return eeExten;
    }

    public void setEeExten(String eeExten) {
        this.eeExten = eeExten;
    }

    public String getEeFaxini() {
        return eeFaxini;
    }

    public void setEeFaxini(String eeFaxini) {
        this.eeFaxini = eeFaxini;
    }

    public String getEeFaxfim() {
        return eeFaxfim;
    }

    public void setEeFaxfim(String eeFaxfim) {
        this.eeFaxfim = eeFaxfim;
    }

    public String getEeFaxatu() {
        return eeFaxatu;
    }

    public void setEeFaxatu(String eeFaxatu) {
        this.eeFaxatu = eeFaxatu;
    }

    public String getEeCodemp() {
        return eeCodemp;
    }

    public void setEeCodemp(String eeCodemp) {
        this.eeCodemp = eeCodemp;
    }

    public String getEeTabela() {
        return eeTabela;
    }

    public void setEeTabela(String eeTabela) {
        this.eeTabela = eeTabela;
    }

    public String getEeDespcrd() {
        return eeDespcrd;
    }

    public void setEeDespcrd(String eeDespcrd) {
        this.eeDespcrd = eeDespcrd;
    }

    public String getEeLote() {
        return eeLote;
    }

    public void setEeLote(String eeLote) {
        this.eeLote = eeLote;
    }

    public double getEeNrbytes() {
        return eeNrbytes;
    }

    public void setEeNrbytes(double eeNrbytes) {
        this.eeNrbytes = eeNrbytes;
    }

    public double getEeTipodat() {
        return eeTipodat;
    }

    public void setEeTipodat(double eeTipodat) {
        this.eeTipodat = eeTipodat;
    }

    public String getEeLotecp() {
        return eeLotecp;
    }

    public void setEeLotecp(String eeLotecp) {
        this.eeLotecp = eeLotecp;
    }

    public double getEeBytesxt() {
        return eeBytesxt;
    }

    public void setEeBytesxt(double eeBytesxt) {
        this.eeBytesxt = eeBytesxt;
    }

    public String getEeCobelet() {
        return eeCobelet;
    }

    public void setEeCobelet(String eeCobelet) {
        this.eeCobelet = eeCobelet;
    }

    public String getEeCodcobe() {
        return eeCodcobe;
    }

    public void setEeCodcobe(String eeCodcobe) {
        this.eeCodcobe = eeCodcobe;
    }

    public String getEeFimlin() {
        return eeFimlin;
    }

    public void setEeFimlin(String eeFimlin) {
        this.eeFimlin = eeFimlin;
    }

    public String getEeRetaut() {
        return eeRetaut;
    }

    public void setEeRetaut(String eeRetaut) {
        this.eeRetaut = eeRetaut;
    }

    public String getEeAglctb() {
        return eeAglctb;
    }

    public void setEeAglctb(String eeAglctb) {
        this.eeAglctb = eeAglctb;
    }

    public String getEeAtumoe() {
        return eeAtumoe;
    }

    public void setEeAtumoe(String eeAtumoe) {
        this.eeAtumoe = eeAtumoe;
    }

    public String getEeDirpag() {
        return eeDirpag;
    }

    public void setEeDirpag(String eeDirpag) {
        this.eeDirpag = eeDirpag;
    }

    public String getEeDirrec() {
        return eeDirrec;
    }

    public void setEeDirrec(String eeDirrec) {
        this.eeDirrec = eeDirrec;
    }

    public String getEeCfgpag() {
        return eeCfgpag;
    }

    public void setEeCfgpag(String eeCfgpag) {
        this.eeCfgpag = eeCfgpag;
    }

    public String getEeCfgrec() {
        return eeCfgrec;
    }

    public void setEeCfgrec(String eeCfgrec) {
        this.eeCfgrec = eeCfgrec;
    }

    public String getEeBkppag() {
        return eeBkppag;
    }

    public void setEeBkppag(String eeBkppag) {
        this.eeBkppag = eeBkppag;
    }

    public String getEeBkprec() {
        return eeBkprec;
    }

    public void setEeBkprec(String eeBkprec) {
        this.eeBkprec = eeBkprec;
    }

    public String getEeDescom() {
        return eeDescom;
    }

    public void setEeDescom(String eeDescom) {
        this.eeDescom = eeDescom;
    }

    public String getEeCnabpg() {
        return eeCnabpg;
    }

    public void setEeCnabpg(String eeCnabpg) {
        this.eeCnabpg = eeCnabpg;
    }

    public String getEeCnabrc() {
        return eeCnabrc;
    }

    public void setEeCnabrc(String eeCnabrc) {
        this.eeCnabrc = eeCnabrc;
    }

    public String getEeProcfl() {
        return eeProcfl;
    }

    public void setEeProcfl(String eeProcfl) {
        this.eeProcfl = eeProcfl;
    }

    public String getEeMultnt() {
        return eeMultnt;
    }

    public void setEeMultnt(String eeMultnt) {
        this.eeMultnt = eeMultnt;
    }

    public String getEeCtbtrf() {
        return eeCtbtrf;
    }

    public void setEeCtbtrf(String eeCtbtrf) {
        this.eeCtbtrf = eeCtbtrf;
    }

    public String getEeRetbco() {
        return eeRetbco;
    }

    public void setEeRetbco(String eeRetbco) {
        this.eeRetbco = eeRetbco;
    }

    public String getEeFormen1() {
        return eeFormen1;
    }

    public void setEeFormen1(String eeFormen1) {
        this.eeFormen1 = eeFormen1;
    }

    public String getEeFormen2() {
        return eeFormen2;
    }

    public void setEeFormen2(String eeFormen2) {
        this.eeFormen2 = eeFormen2;
    }

    public String getEeForext1() {
        return eeForext1;
    }

    public void setEeForext1(String eeForext1) {
        this.eeForext1 = eeForext1;
    }

    public String getEeForext2() {
        return eeForext2;
    }

    public void setEeForext2(String eeForext2) {
        this.eeForext2 = eeForext2;
    }

    public String getEeDiasprt() {
        return eeDiasprt;
    }

    public void setEeDiasprt(String eeDiasprt) {
        this.eeDiasprt = eeDiasprt;
    }

    public String getEeCodcart() {
        return eeCodcart;
    }

    public void setEeCodcart(String eeCodcart) {
        this.eeCodcart = eeCodcart;
    }

    public String getEeUmsg1() {
        return eeUmsg1;
    }

    public void setEeUmsg1(String eeUmsg1) {
        this.eeUmsg1 = eeUmsg1;
    }

    public String getEeUmsg2() {
        return eeUmsg2;
    }

    public void setEeUmsg2(String eeUmsg2) {
        this.eeUmsg2 = eeUmsg2;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

//    public int getRECDEL() {
//        return rECDEL;
//    }
//
//    public void setRECDEL(int rECDEL) {
//        this.rECDEL = rECDEL;
//    }

    public String getEeUCgc() {
        return eeUCgc;
    }

    public void setEeUCgc(String eeUCgc) {
        this.eeUCgc = eeUCgc;
    }

    public String getEeUdsCed() {
        return eeUdsCed;
    }

    public void setEeUdsCed(String eeUdsCed) {
        this.eeUdsCed = eeUdsCed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof See010)) {
            return false;
        }
        See010 other = (See010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.See010[ rECNO=" + rECNO + " ]";
    }
}
