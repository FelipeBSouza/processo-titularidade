package api.rest.model.integrador;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "adicional_plano")
public class AdicionalPlano implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_cliente", referencedColumnName = "cd_cliente")
    private Cliente cliente;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_adicional", referencedColumnName = "cd_adicional")
    private Adicional adicional;
    private int quantidade;
    private Double valor;
    @Column(name = "VALIDADE_MES")
    private int validadeMes = 0;
    @Column(name = "DT_INICIO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date DT_INICIO;
    @Column(name = "DT_FIM")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date DT_FIM;

    public AdicionalPlano() {
    }

    public Adicional getAdicional() {
        return adicional;
    }

    public void setAdicional(Adicional adicional) {
        this.adicional = adicional;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getDT_FIM() {
        return DT_FIM;
    }

    public void setDT_FIM(Date DT_FIM) {
        this.DT_FIM = DT_FIM;
    }

    public Date getDT_INICIO() {
        return DT_INICIO;
    }

    public void setDT_INICIO(Date DT_INICIO) {
        this.DT_INICIO = DT_INICIO;
    }

    public int getValidadeMes() {
        return validadeMes;
    }

    public void setValidadeMes(int validadeMes) {
        this.validadeMes = validadeMes;
    }

    public Double getTotal() {
        return quantidade * valor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdicionalPlano other = (AdicionalPlano) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }
}
