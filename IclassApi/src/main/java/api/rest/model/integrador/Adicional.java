package api.rest.model.integrador;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "adicional")
public class Adicional implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cd_adicional;
   
    private String ds_adicional; 
 
    private char in_add_sub;
    
    private Double valor;
    @JsonIgnore
    @OneToMany(mappedBy = "adicional",fetch= FetchType.LAZY)
    private List<AdicionalPlano> listaAdicionalPlano;

    public Adicional() {
    }

    public int getCd_adicional() {
        return cd_adicional;
    }

    public void setCd_adicional(int cd_adicional) {
        this.cd_adicional = cd_adicional;
    }

    public String getDs_adicional() {
        return ds_adicional;
    }

    public void setDs_adicional(String ds_adicional) {
        this.ds_adicional = ds_adicional;
    }

    public char getIn_add_sub() {
        return in_add_sub;
    }

    public void setIn_add_sub(char in_add_sub) {
        this.in_add_sub = in_add_sub;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<AdicionalPlano> getListaAdicionalPlano() {
        return listaAdicionalPlano;
    }

    public void setListaAdicionalPlano(List<AdicionalPlano> listaAdicionalPlano) {
        this.listaAdicionalPlano = listaAdicionalPlano;
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Adicional other = (Adicional) obj;
        if (this.cd_adicional != other.cd_adicional) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.cd_adicional;
        return hash;
    }

    @Override
    public String toString() {
        return ds_adicional;
    }

   
    
}
