package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@Entity
@Table(schema = "integrador",name = "finalidade")
public class TipoFinalidade implements Serializable{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)  
    @Column(name = "cd_finalidade")
    private int cd_finalidade;
    
    @Column(name = "ds_finalidade")
    private String ds_finalidade;
    
   
    public TipoFinalidade() {
    }

    public int getCd_finalidade() {
        return cd_finalidade;
    }

    public void setCd_finalidade(int cd_finalidade) {
        this.cd_finalidade = cd_finalidade;
    }

    public String getDs_finalidade() {
        return ds_finalidade;
    }

    public void setDs_finalidade(String ds_finalidade) {
        this.ds_finalidade = ds_finalidade;
    }
      
    
    @Override
    public String toString(){
        return this.getDs_finalidade();
    }
    
}
