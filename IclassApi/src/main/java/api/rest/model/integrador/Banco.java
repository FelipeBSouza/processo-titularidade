package api.rest.model.integrador;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador",name = "BANCO")
public class Banco implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CD_BANCO", nullable = false)
    private int CD_BANCO;
    
    @Column(name = "DS_BANCO")
    private String DS_BANCO;
    
    @Column(name = "NR_BANCO")
    private String NR_BANCO;
    
    @Column(name = "NR_BANCO_VERIFICADOR")
    private String NR_BANCO_VERIFICADOR;
    @JsonIgnore
    @OneToMany(mappedBy = "BANCO" , fetch = FetchType.LAZY)
    private List<CedenteEmpresa> cedenteEmpresas;

    public Banco() {
    }

    public int getCD_BANCO() {
        return CD_BANCO;
    }

    public void setCD_BANCO(int CD_BANCO) {
        this.CD_BANCO = CD_BANCO;
    }

    public String getDS_BANCO() {
        return DS_BANCO;
    }

    public void setDS_BANCO(String DS_BANCO) {
        this.DS_BANCO = DS_BANCO;
    }

    public String getNR_BANCO() {
        return NR_BANCO;
    }

    public void setNR_BANCO(String NR_BANCO) {
        this.NR_BANCO = NR_BANCO;
    }

    public String getNR_BANCO_VERIFICADOR() {
        return NR_BANCO_VERIFICADOR;
    }

    public void setNR_BANCO_VERIFICADOR(String NR_BANCO_VERIFICADOR) {
        this.NR_BANCO_VERIFICADOR = NR_BANCO_VERIFICADOR;
    }

    public List<CedenteEmpresa> getCedenteEmpresas() {
        return cedenteEmpresas;
    }

    public void setCedenteEmpresas(List<CedenteEmpresa> cedenteEmpresas) {
        this.cedenteEmpresas = cedenteEmpresas;
    }
    
    
      
}
