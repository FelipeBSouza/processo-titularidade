/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.equipamento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "CAD_TIPO_EQUIPAMENTO")
public class TipoEquipamento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_tipo_equipamento", nullable = false)
    private int cd_tipo_equipamento;
    @Column(name = "ds_tipo_equipamento")
    private String ds_tipo_equipamento;
    /*
     *
     */
//    @OneToMany(mappedBy = "tipoEquipamento", fetch = FetchType.LAZY)
//    private List<EquipamentoYate> equipamentos;
//    @OneToMany(mappedBy = "tipoEquipamento", fetch = FetchType.LAZY)
//    private List<Modelo> modelos = new ArrayList<Modelo>();

    public TipoEquipamento() {
    }

    public int getCd_tipo_equipamento() {
        return cd_tipo_equipamento;
    }

    public void setCd_tipo_equipamento(int cd_tipo_equipamento) {
        this.cd_tipo_equipamento = cd_tipo_equipamento;
    }

    public String getDs_tipo_equipamento() {
        return ds_tipo_equipamento;
    }

    public void setDs_tipo_equipamento(String ds_tipo_equipamento) {
        this.ds_tipo_equipamento = ds_tipo_equipamento;
    }

//    public List<EquipamentoYate> getEquipamentos() {
//        return equipamentos;
//    }
//
//    public void setEquipamentos(List<EquipamentoYate> equipamentos) {
//        this.equipamentos = equipamentos;
//    }
//    public List<Modelo> getModelos() {
//        return modelos;
//    }
//
//    public void setModelos(List<Modelo> modelos) {
//        this.modelos = modelos;
//    }

    @Override
    public String toString() {
        return ds_tipo_equipamento;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoEquipamento other = (TipoEquipamento) obj;
        if (this.cd_tipo_equipamento != other.cd_tipo_equipamento) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.cd_tipo_equipamento;
        return hash;
    }
}
