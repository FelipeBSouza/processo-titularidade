package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "integrador", name = "controle_os")
public class ControleOS implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String nr_os;
	private String nr_os_sequencial;
	private String dt_movimentacao;
	private int sequencial;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNr_os() {
		return nr_os;
	}
	public void setNr_os(String nr_os) {
		this.nr_os = nr_os;
	}
	public String getNr_os_sequencial() {
		return nr_os_sequencial;
	}
	public void setNr_os_sequencial(String nr_os_sequencial) {
		this.nr_os_sequencial = nr_os_sequencial;
	}
	public String getDt_movimentacao() {
		return dt_movimentacao;
	}
	public void setDt_movimentacao(String dt_movimentacao) {
		this.dt_movimentacao = dt_movimentacao;
	}
	public int getSequencial() {
		return sequencial;
	}
	public void setSequencial(int sequencial) {
		this.sequencial = sequencial;
	}

}
