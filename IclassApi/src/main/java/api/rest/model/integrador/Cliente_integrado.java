package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@Entity
@Table(schema = "telefonia",name = "CLIENTES")
public class Cliente_integrado implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private int id;
    
    @Column(name="nome",columnDefinition="CHAR(80)")
    private String nome;
    
    @Column(name="fantasia", columnDefinition="CHAR(80)")
    private String fantasia;
    
    @Column(name="cpf", columnDefinition="CHAR(20)")
    private String cpf;
    
    @Column(name="cnpj", columnDefinition="CHAR(20)")
    private String cnpj;
    
    @Column(name="rg", columnDefinition="CHAR(30)")
    private String rg;
    
    @Column(name="ie", columnDefinition="CHAR(30)")
    private String ie;
                
    @Column(name="endereco", columnDefinition="CHAR(80)")
    private String endereco;
    
    @Column(name="numero", columnDefinition="CHAR(20)")
    private String numero;
    
    @Column(name="complemento", columnDefinition="CHAR(20)")
    private String complemento;
    
    @Column(name="email", columnDefinition="CHAR(150)")
    private String email;

    @Column(name="bairro", columnDefinition="CHAR(50)")
    private String bairro;
    
    @Column(name="telefonecom", columnDefinition="CHAR(40)")
    private String telefonecom;
    
    @Column(name="cidade", columnDefinition="CHAR(50)")
    private String cidade;
    
    @Column(name="uf", columnDefinition="CHAR(2)")
    private String uf;
    
    @Column(name="cep", columnDefinition="CHAR(10)")
    private String cep;
    
    @Column(name="vencimento")
    private byte vencimento;

    public Cliente_integrado() {
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelefonecom() {
        return telefonecom;
    }

    public void setTelefonecom(String telefonecom) {
        this.telefonecom = telefonecom;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public byte getVencimento() {
        return vencimento;
    }

    public void setVencimento(byte vencimento) {
        this.vencimento = vencimento;
    }

    
}
