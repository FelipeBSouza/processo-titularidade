package api.rest.model.integrador;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "BOLETO")
public class BoletoClass implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CD_BOLETO", nullable = false)
    private int CD_BOLETO;
    @Column(name = "NR_DOCUMENTO")
    private String NR_DOCUMENTO;
    @Column(name = "NR_NOSSO_NUMERO")
    private String NR_NOSSO_NUMERO;
    @Column(name = "NR_NOSSO_NUMERO_DV")
    private String NR_NOSSO_NUMERO_DV;
    @Column(name = "NR_CARTEIRA")
    private int NR_CARTEIRA;
    @Column(name = "NR_AGENCIA")
    private String NR_AGENCIA;
    @Column(name = "NR_AGENCIA_VERIFICADOR")
    private String NR_AGENCIA_VERIFICADOR;
    @Column(name = "NR_CONTA")
    private String NR_CONTA;
    @Column(name = "NR_CONTA_VERIFICADOR")
    private String NR_CONTA_VERIFICADOR;
    @Column(name = "DT_EMISSAO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date DT_EMISSAO;
    @Column(name = "DT_VENCIMENTO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date DT_VENCIMENTO;
    @Column(name = "DT_PAGAMENTO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date DT_PAGAMENTO;
    @Column(name = "NR_MOEDA")
    private int NR_MOEDA;
    @Column(name = "VL_BOLETO")
    private double VL_BOLETO;
    @Column(name = "VL_MULTA")
    private double VL_MULTA;
    @Column(name = "VL_JUROS")
    private double VL_JUROS;
    @Column(name = "ESP_DOCUMENTO")
    private String ESP_DOCUMENTO;
    @Column(name = "ACEITE")
    private String ACEITE;
    @Column(name = "IN_SITUACAO")
    private String IN_SITUACAO;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CEDENTE")
    private CedenteEmpresa CEDENTE;
    @JsonIgnore
    @JoinColumn(name = "SACADO")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pessoa SACADO;
    @JsonIgnore
    @OneToOne(mappedBy = "boleto", fetch = FetchType.LAZY)
    private ContasReceber contasReceber;

    public BoletoClass() {
    }

    public String getACEITE() {
        return ACEITE;
    }

    public void setACEITE(String ACEITE) {
        this.ACEITE = ACEITE;
    }

    public int getCD_BOLETO() {
        return CD_BOLETO;
    }

    public void setCD_BOLETO(int CD_BOLETO) {
        this.CD_BOLETO = CD_BOLETO;
    }

    public CedenteEmpresa getCEDENTE() {
        return CEDENTE;
    }

    public void setCEDENTE(CedenteEmpresa CEDENTE) {
        this.CEDENTE = CEDENTE;
    }

    public Date getDT_EMISSAO() {
        return DT_EMISSAO;
    }

    public void setDT_EMISSAO(Date DT_EMISSAO) {
        this.DT_EMISSAO = DT_EMISSAO;
    }

    public Date getDT_PAGAMENTO() {
        return DT_PAGAMENTO;
    }

    public void setDT_PAGAMENTO(Date DT_PAGAMENTO) {
        this.DT_PAGAMENTO = DT_PAGAMENTO;
    }

    public Date getDT_VENCIMENTO() {
        return DT_VENCIMENTO;
    }

    public void setDT_VENCIMENTO(Date DT_VENCIMENTO) {
        this.DT_VENCIMENTO = DT_VENCIMENTO;
    }

    public String getESP_DOCUMENTO() {
        return ESP_DOCUMENTO;
    }

    public void setESP_DOCUMENTO(String ESP_DOCUMENTO) {
        this.ESP_DOCUMENTO = ESP_DOCUMENTO;
    }

    public String getIN_SITUACAO() {
        return IN_SITUACAO;
    }

    public void setIN_SITUACAO(String IN_SITUACAO) {
        this.IN_SITUACAO = IN_SITUACAO;
    }

    public String getNR_AGENCIA() {
        return NR_AGENCIA;
    }

    public void setNR_AGENCIA(String NR_AGENCIA) {
        this.NR_AGENCIA = NR_AGENCIA;
    }

    public String getNR_AGENCIA_VERIFICADOR() {
        return NR_AGENCIA_VERIFICADOR;
    }

    public void setNR_AGENCIA_VERIFICADOR(String NR_AGENCIA_VERIFICADOR) {
        this.NR_AGENCIA_VERIFICADOR = NR_AGENCIA_VERIFICADOR;
    }

    public int getNR_CARTEIRA() {
        return NR_CARTEIRA;
    }

    public void setNR_CARTEIRA(int NR_CARTEIRA) {
        this.NR_CARTEIRA = NR_CARTEIRA;
    }

    public String getNR_CONTA() {
        return NR_CONTA;
    }

    public void setNR_CONTA(String NR_CONTA) {
        this.NR_CONTA = NR_CONTA;
    }

    public String getNR_CONTA_VERIFICADOR() {
        return NR_CONTA_VERIFICADOR;
    }

    public void setNR_CONTA_VERIFICADOR(String NR_CONTA_VERIFICADOR) {
        this.NR_CONTA_VERIFICADOR = NR_CONTA_VERIFICADOR;
    }

    public String getNR_DOCUMENTO() {
        return NR_DOCUMENTO;
    }

    public void setNR_DOCUMENTO(String NR_DOCUMENTO) {
        this.NR_DOCUMENTO = NR_DOCUMENTO;
    }

    public int getNR_MOEDA() {
        return NR_MOEDA;
    }

    public void setNR_MOEDA(int NR_MOEDA) {
        this.NR_MOEDA = NR_MOEDA;
    }

    public String getNR_NOSSO_NUMERO() {
        return NR_NOSSO_NUMERO;
    }

    public void setNR_NOSSO_NUMERO(String NR_NOSSO_NUMERO) {
        this.NR_NOSSO_NUMERO = NR_NOSSO_NUMERO;
    }

    public String getNR_NOSSO_NUMERO_DV() {
        return NR_NOSSO_NUMERO_DV;
    }

    public void setNR_NOSSO_NUMERO_DV(String NR_NOSSO_NUMERO_DV) {
        this.NR_NOSSO_NUMERO_DV = NR_NOSSO_NUMERO_DV;
    }

    public Pessoa getSACADO() {
        return SACADO;
    }

    public void setSACADO(Pessoa SACADO) {
        this.SACADO = SACADO;
    }

    public double getVL_BOLETO() {
        return VL_BOLETO;
    }

    public void setVL_BOLETO(double VL_BOLETO) {
        this.VL_BOLETO = VL_BOLETO;
    }

    public double getVL_MULTA() {
        return VL_MULTA;
    }

    public void setVL_MULTA(double VL_MULTA) {
        this.VL_MULTA = VL_MULTA;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public double getVL_JUROS() {
        return VL_JUROS;
    }

    public void setVL_JUROS(double VL_JUROS) {
        this.VL_JUROS = VL_JUROS;
    }
}
