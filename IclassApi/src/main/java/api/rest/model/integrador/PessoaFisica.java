package api.rest.model.integrador;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
    @XmlRootElement
@Entity
@Table(schema = "integrador", name = "pessoa_fisica")
public class PessoaFisica implements Serializable {

    @Id
    @Column(name = "cd_pessoa", nullable = false)
    private int cd_pessoa;
    @JsonIgnore
    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_pessoa")
    private Pessoa pessoa;
    /*
     * 
     */
    @Column(name = "nr_rg")
    private String nr_rg;
    @Column(name = "nr_cpf")
    private String nr_cpf;
   
    @Column(name = "ds_sexo")
    private char ds_sexo = 'M';
    
    @Column(name = "dt_nascimento")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dt_nascimento;
    
    @Column(name = "ds_est_civil")
    private int ds_est_civil;
    @Column(name = "nm_conjugue")
    private String nm_conjugue;
    @Column(name = "nm_pai", length = 100)
    private String nm_pai;
    @Column(name = "nm_mae", length = 100)
    private String nm_mae;

    public PessoaFisica() {
    }

    public int getCd_pessoa() {
        return cd_pessoa;
    }

    public void setCd_pessoa(int cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public int getDs_est_civil() {
        return ds_est_civil;
    }

    public void setDs_est_civil(int ds_est_civil) {
        this.ds_est_civil = ds_est_civil;
    }



    public String getNm_conjugue() {
        return nm_conjugue;
    }

    public void setNm_conjugue(String nm_conjugue) {
        this.nm_conjugue = nm_conjugue;
    }

    public String getNm_mae() {
        return nm_mae;
    }

    public void setNm_mae(String nm_mae) {
        this.nm_mae = nm_mae;
    }

    public String getNm_pai() {
        return nm_pai;
    }

    public void setNm_pai(String nm_pai) {
        this.nm_pai = nm_pai;
    }

    public String getNr_cpf() {
        return nr_cpf;
    }

    public void setNr_cpf(String nr_cpf) {
        this.nr_cpf = nr_cpf;
    }

    public String getNr_rg() {
        return nr_rg;
    }

    public void setNr_rg(String nr_rg) {
        this.nr_rg = nr_rg;
    }

    public char getDs_sexo() {
        return ds_sexo;
    }

    public void setDs_sexo(char ds_sexo) {
        this.ds_sexo = ds_sexo;
    }

    public Date getDt_nascimento() {
        return dt_nascimento;
    }

    public void setDt_nascimento(Date dt_nascimento) {
        this.dt_nascimento = dt_nascimento;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.cd_pessoa;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PessoaFisica other = (PessoaFisica) obj;
        if (this.cd_pessoa != other.cd_pessoa) {
            return false;
        }
        return true;
    }
    
}
