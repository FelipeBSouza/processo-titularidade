/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author popovicz
 */
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "chassi")
public class Chassi implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_chassi", nullable = false)
    private int cd_chassi;
    @Column(name = "ip_adress")
    private String ipAdress;
    @Column(name = "ds_descricao")
    private String ds_descricao;
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "placas")
    private Integer placas;

    public Chassi() {
    }

    public int getCd_chassi() {
        return cd_chassi;
    }

    public void setCd_chassi(int cd_chassi) {
        this.cd_chassi = cd_chassi;
    }

    public String getIpAdress() {
        return ipAdress;
    }

    public void setIpAdress(String ipAdress) {
        this.ipAdress = ipAdress;
    }

    public String getDs_descricao() {
        return ds_descricao;
    }

    public void setDs_descricao(String ds_descricao) {
        this.ds_descricao = ds_descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getPlacas() {
        return placas;
    }

    public void setPlacas(Integer placas) {
        this.placas = placas;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.cd_chassi;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Chassi other = (Chassi) obj;
        if (this.cd_chassi != other.cd_chassi) {
            return false;
        }
        return true;
    }
}
