package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "email")
public class Email implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_email", nullable = false)
    private int cd_email;
    @Column(name = "ds_email", length = 60)
    private String ds_email;
    @Column(name = "ds_detalhes", length = 60)
    private String ds_detalhes;
    @Column(name = "ds_nome")
    private String ds_nome;
    @Column(name = "ds_telefone")
    private String ds_telefone;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_finalidade")
    private TipoFinalidade cd_finalidade;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_pessoa")
    private Pessoa cd_pessoa;

    public Email() {
    }

    public Email(Pessoa cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }
    
    public int getCd_email() {
        return cd_email;
    }

    public void setCd_email(int cd_email) {
        this.cd_email = cd_email;
    }

    public TipoFinalidade getCd_finalidade() {
        return cd_finalidade;
    }

    public void setCd_finalidade(TipoFinalidade cd_finalidade) {
        this.cd_finalidade = cd_finalidade;
    }

    public Pessoa getCd_pessoa() {
        return cd_pessoa;
    }

    public void setCd_pessoa(Pessoa cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public String getDs_detalhes() {
        return ds_detalhes;
    }

    public void setDs_detalhes(String ds_detalhes) {
        this.ds_detalhes = ds_detalhes;
    }

    public String getDs_email() {
        return ds_email;
    }

    public void setDs_email(String ds_email) {
        this.ds_email = ds_email;
    }

    public String getDs_nome() {
        return ds_nome;
    }

    public void setDs_nome(String ds_nome) {
        this.ds_nome = ds_nome;
    }

    public String getDs_telefone() {
        return ds_telefone;
    }

    public void setDs_telefone(String ds_telefone) {
        this.ds_telefone = ds_telefone;
    }
}
