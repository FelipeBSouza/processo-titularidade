package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "pessoa_juridica")
public class PessoaJuridica implements Serializable {

    @Id
    @Column(name = "cd_pessoa")
    private int cd_pessoa;
    @JsonIgnore
    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_pessoa")
    private Pessoa pessoa;
    /*
     * 
     */
    @Column(name = "nm_fantasia")
    private String nm_fantasia;
    @Column(name = "nr_cnpj")
    private String nr_cnpj;
    @Column(name = "nr_ie")
    private String nr_ie;

    public PessoaJuridica() {
    }

    public int getCd_pessoa() {
        return cd_pessoa;
    }

    public void setCd_pessoa(int cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getNm_fantasia() {
        return nm_fantasia;
    }

    public void setNm_fantasia(String nm_fantasia) {
        this.nm_fantasia = nm_fantasia;
    }

    public String getNr_cnpj() {
        return nr_cnpj;
    }

    public void setNr_cnpj(String nr_cnpj) {
        this.nr_cnpj = nr_cnpj;
    }

    public String getNr_ie() {
        return nr_ie;
    }

    public void setNr_ie(String nr_ie) {
        this.nr_ie = nr_ie;
    }
}
