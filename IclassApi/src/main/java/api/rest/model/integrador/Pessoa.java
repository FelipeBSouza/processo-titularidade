package api.rest.model.integrador;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement
@Entity
@Table(schema = "integrador", name = "pessoa")
public class Pessoa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_pessoa", nullable = false)
    private int cd_pessoa;
    @Column(name = "nm_pessoa", length = 100)
    private String nm_pessoa;
    @Column(name = "dt_cadastro")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dt_cadastro = new Date();
    @Column(name = "in_ativo")
    private char in_ativo = 'A';
    @Column(name = "ds_login", unique = true)
    private String ds_login;
    @Column(name = "ds_senha")
    private String ds_senha;
    @Column(name = "tipo_pessoa")
    private String tipo_pessoa = "F";
//-----------------------------
    @OneToMany(mappedBy = "cd_pessoa", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Endereco> listaEndereco = new ArrayList<Endereco>();
//-----------------------------    
    @OneToMany(mappedBy = "cd_pessoa", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Email> listaEmail = new ArrayList<Email>();
//-----------------------------    
    @OneToMany(mappedBy = "cd_pessoa", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Telefone> listaTelefone = new ArrayList<Telefone>();
    /*
     * 
     */
    @JsonIgnore
    @OneToOne(mappedBy = "pessoa", fetch = FetchType.LAZY)
    private Cliente cliente;
    @JsonIgnore
    @OneToMany(mappedBy = "SACADO", fetch = FetchType.LAZY)
    private List<BoletoClass> boletoClasss;
    /*
     * 
     */
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "cd_pessoa", referencedColumnName = "cd_pessoa")
    private PessoaFisica pessoaFisica;
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "cd_pessoa", referencedColumnName = "cd_pessoa")
    private PessoaJuridica pessoaJuridica;

//-----------------------------    
    public Pessoa() {
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getCd_pessoa() {
        return cd_pessoa;
    }

    public void setCd_pessoa(int cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public String getDs_login() {
        return ds_login;
    }

    public void setDs_login(String ds_login) {
        this.ds_login = ds_login;
    }

    public String getDs_senha() {
        return ds_senha;
    }

    public void setDs_senha(String ds_senha) {
        this.ds_senha = ds_senha;
    }

    public Date getDt_cadastro() {
        return dt_cadastro;
    }

    public void setDt_cadastro(Date dt_cadastro) {
        this.dt_cadastro = dt_cadastro;
    }

    public char getIn_ativo() {
        return in_ativo;
    }

    public void setIn_ativo(char in_ativo) {
        this.in_ativo = in_ativo;
    }

    public String getNm_pessoa() {
        return nm_pessoa;
    }

    public void setNm_pessoa(String nm_pessoa) {
        this.nm_pessoa = nm_pessoa;
    }

    public List<Endereco> getListaEndereco() {
        return listaEndereco;
    }

    public void setListaEndereco(List<Endereco> listaEndereco) {
        this.listaEndereco = listaEndereco;
    }

    public List<Email> getListaEmail() {
        return listaEmail;
    }

    public void setListaEmail(List<Email> listaEmail) {
        this.listaEmail = listaEmail;
    }

    public List<Telefone> getListaTelefone() {
        return listaTelefone;
    }

    public void setListaTelefone(List<Telefone> listaTelefone) {
        this.listaTelefone = listaTelefone;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }

    public List<BoletoClass> getBoletoClasss() {
        return boletoClasss;
    }

    public void setBoletoClasss(List<BoletoClass> boletoClasss) {
        this.boletoClasss = boletoClasss;
    }

    public String getTipo_pessoa() {
        return tipo_pessoa;
    }

    public void setTipo_pessoa(String tipo_pessoa) {
        this.tipo_pessoa = tipo_pessoa;
    }
    
    @Override
    public String toString() {
        return this.nm_pessoa;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.cd_pessoa;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa other = (Pessoa) obj;
        if (this.cd_pessoa != other.cd_pessoa) {
            return false;
        }
        return true;
    }
}
