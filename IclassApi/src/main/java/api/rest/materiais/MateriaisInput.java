package api.rest.materiais;

public class MateriaisInput {
	
	private String qtd;
	private String cod_tecnico;
	private String cod_material;
	
	public String getQtd() {
		return qtd;
	}
	public void setQtd(String qtd) {
		this.qtd = qtd;
	}
	public String getCod_tecnico() {
		return cod_tecnico;
	}
	public void setCod_tecnico(String cod_tecnico) {
		this.cod_tecnico = cod_tecnico;
	}
	public String getCod_material() {
		return cod_material;
	}
	public void setCod_material(String cod_material) {
		this.cod_material = cod_material;
	}

}
