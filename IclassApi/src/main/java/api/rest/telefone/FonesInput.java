package api.rest.telefone;

public class FonesInput {
	
	private String tipo;
	private String ddi;
	private String ddd;
	private String telefone;
	private String tipo2;
	private String complemento;
	private String titular;
	private String cpf_cnpj;
	private String novo_alteracao_fone;
	private String cod_agb;
	private String recno;
	private String nr_solicitacao;	
	private String observacao;
	
		
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDdi() {
		return ddi;
	}
	public void setDdi(String ddi) {
		this.ddi = ddi;
	}
	public String getDdd() {
		return ddd;
	}
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getTipo2() {
		return tipo2;
	}
	public void setTipo2(String tipo2) {
		this.tipo2 = tipo2;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getCpf_cnpj() {
		return cpf_cnpj;
	}
	public void setCpf_cnpj(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}
	public String getNovo_alteracao_fone() {
		return novo_alteracao_fone;
	}
	public void setNovo_alteracao_fone(String novo_alteracao_fone) {
		this.novo_alteracao_fone = novo_alteracao_fone;
	}
	public String getCod_agb() {
		return cod_agb;
	}
	public void setCod_agb(String cod_agb) {
		this.cod_agb = cod_agb;
	}
	public String getRecno() {
		return recno;
	}
	public void setRecno(String recno) {
		this.recno = recno;
	}
	public String getNr_solicitacao() {
		return nr_solicitacao;
	}
	public void setNr_solicitacao(String nr_solicitacao) {
		this.nr_solicitacao = nr_solicitacao;
	}	
	
}
