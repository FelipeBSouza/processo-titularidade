package api.rest.telefone;

public class TelefoneJsonRecebe {
	
	private String SEQUENCIAL;
	private String RECNO;
	
	public String getSEQUENCIAL() {
		return SEQUENCIAL;
	}
	public void setSEQUENCIAL(String sEQUENCIAL) {
		SEQUENCIAL = sEQUENCIAL;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

}
