package api.rest.retencao;

public class FoneTb {
	
	private String PADDDI;
	private String PADDD;
	private String PATEL;
	private String PATIPO;
	private String PAPADRAO;
	private String PAOP;
	private String CDTELEF;
	private String PATIPO2;
	private String PACOMP;
	private String PAOWNER;
	private String PACPFOW;
	
	
	public String getPADDDI() {
		return PADDDI;
	}
	public void setPADDDI(String pADDDI) {
		PADDDI = pADDDI;
	}
	public String getPADDD() {
		return PADDD;
	}
	public void setPADDD(String pADDD) {
		PADDD = pADDD;
	}
	public String getPATEL() {
		return PATEL;
	}
	public void setPATEL(String pATEL) {
		PATEL = pATEL;
	}
	public String getPATIPO() {
		return PATIPO;
	}
	public void setPATIPO(String pATIPO) {
		PATIPO = pATIPO;
	}
	public String getPAPADRAO() {
		return PAPADRAO;
	}
	public void setPAPADRAO(String pAPADRAO) {
		PAPADRAO = pAPADRAO;
	}
	public String getPAOP() {
		return PAOP;
	}
	public void setPAOP(String pAOP) {
		PAOP = pAOP;
	}
	public String getCDTELEF() {
		return CDTELEF;
	}
	public void setCDTELEF(String cDTELEF) {
		CDTELEF = cDTELEF;
	}
	public String getPATIPO2() {
		return PATIPO2;
	}
	public void setPATIPO2(String pATIPO2) {
		PATIPO2 = pATIPO2;
	}
	public String getPACOMP() {
		return PACOMP;
	}
	public void setPACOMP(String pACOMP) {
		PACOMP = pACOMP;
	}
	public String getPAOWNER() {
		return PAOWNER;
	}
	public void setPAOWNER(String pAOWNER) {
		PAOWNER = pAOWNER;
	}
	public String getPACPFOW() {
		return PACPFOW;
	}
	public void setPACPFOW(String pACPFOW) {
		PACPFOW = pACPFOW;
	}	

}
