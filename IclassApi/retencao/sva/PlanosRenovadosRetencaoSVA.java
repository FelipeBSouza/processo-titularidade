package api.rest.retencao.sva;

public class PlanosRenovadosRetencaoSVA {
	
	private String P2CODP;
	private String P2ITEM;
	private String P2CODANT;
	private String P2QUANT;
	private String P2VLMES;
	private String P2VALDES;
	private String P2MSG;
	private String P2MESINI;
	private String P2UMESCO;
	private String P2VLUNIT;
	private String P2CODPA;
	private String P2CODTAR;
	private String P2UVLCHE;
	private String P2OSOBRG;
	private String P2IDITEM;
	
	public String getP2IDITEM() {
		return P2IDITEM;
	}
	public void setP2IDITEM(String p2iditem) {
		P2IDITEM = p2iditem;
	}
	public String getP2CODP() {
		return P2CODP;
	}
	public void setP2CODP(String p2codp) {
		P2CODP = p2codp;
	}
	public String getP2ITEM() {
		return P2ITEM;
	}
	public void setP2ITEM(String p2item) {
		P2ITEM = p2item;
	}
	public String getP2CODANT() {
		return P2CODANT;
	}
	public void setP2CODANT(String p2codant) {
		P2CODANT = p2codant;
	}
	public String getP2QUANT() {
		return P2QUANT;
	}
	public void setP2QUANT(String p2quant) {
		P2QUANT = p2quant;
	}
	public String getP2VLMES() {
		return P2VLMES;
	}
	public void setP2VLMES(String p2vlmes) {
		P2VLMES = p2vlmes;
	}
	public String getP2VALDES() {
		return P2VALDES;
	}
	public void setP2VALDES(String p2valdes) {
		P2VALDES = p2valdes;
	}
	public String getP2MSG() {
		return P2MSG;
	}
	public void setP2MSG(String p2msg) {
		P2MSG = p2msg;
	}
	public String getP2MESINI() {
		return P2MESINI;
	}
	public void setP2MESINI(String p2mesini) {
		P2MESINI = p2mesini;
	}
	public String getP2UMESCO() {
		return P2UMESCO;
	}
	public void setP2UMESCO(String p2umesco) {
		P2UMESCO = p2umesco;
	}
	public String getP2VLUNIT() {
		return P2VLUNIT;
	}
	public void setP2VLUNIT(String p2vlunit) {
		P2VLUNIT = p2vlunit;
	}
	public String getP2CODPA() {
		return P2CODPA;
	}
	public void setP2CODPA(String p2codpa) {
		P2CODPA = p2codpa;
	}
	public String getP2CODTAR() {
		return P2CODTAR;
	}
	public void setP2CODTAR(String p2codtar) {
		P2CODTAR = p2codtar;
	}
	public String getP2UVLCHE() {
		return P2UVLCHE;
	}
	public void setP2UVLCHE(String p2uvlche) {
		P2UVLCHE = p2uvlche;
	}
	public String getP2OSOBRG() {
		return P2OSOBRG;
	}
	public void setP2OSOBRG(String p2osobrg) {
		P2OSOBRG = p2osobrg;
	}	
	
}
