package api.rest.repositoy;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Aa3010;

@Repository
public interface AA3Repository extends CrudRepository<Aa3010, Integer> {
	
	@Query(value = "SELECT * FROM AA3010 WHERE AA3_UMAC LIKE %?1% ORDER BY R_E_C_N_O_ DESC", nativeQuery = true)
	public List<Aa3010> buscaPorMac(String mac);
	
	@Query(value = "SELECT * FROM AA3010 WHERE AA3_CHAPA LIKE %?1% ORDER BY R_E_C_N_O_ DESC", nativeQuery = true)
	public List<Aa3010> buscaPorChapa(String chapa);
		
	@Transactional
	@Modifying
	@Query(value = "UPDATE AA3010 SET AA3_UMAC = ?1, AA3_CHAPA = ?2 WHERE AA3_CODCLI = ?3 AND AA3_CODPRO = ?4 AND AA3_NUMSER = ?5 AND R_E_C_N_O_ = ?6", nativeQuery = true)
	public void vincula_mac(String mac, String chapa, String cod_cli, String cod_prod, String numser, int recno);	
	
	 

}
