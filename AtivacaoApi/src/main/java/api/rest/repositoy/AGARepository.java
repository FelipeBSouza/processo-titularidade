package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Aga010;

@Repository
public interface AGARepository extends CrudRepository<Aga010, Integer> {

	@Query(value = "SELECT * FROM AGA010 WHERE AGA_CODIGO LIKE %?1% AND AGA_UTIPO2 != 'C' AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Aga010> buscaEnderecoInstalacao(String cod_end);	
	
	@Query(value = "SELECT * FROM AGA010 WHERE AGA_CODENT LIKE %?1% AND AGA_UTIPO2 != 'C' AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Aga010> buscaEnderecosCliente(String cod_cli);	
	
	@Query(value = "SELECT * FROM AGA010 WHERE AGA_UTIPO2 != 'C' AND AGA_UICLAS = 'S' AND AGA_ULIBIC = 'N' AND D_E_L_E_T_ = ''", nativeQuery = true)
	public List<Aga010> enderecosEnviarIclass();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE AGA010 SET AGA_UICLAS = ?1, AGA_ULIBIC = ?2, AGA_ULIBFL = ?2 WHERE AGA_CODIGO = ?3 AND R_E_C_N_O_ = ?4", nativeQuery = true)
	public void marca_endereco(String marcacao, String controle, String cod_end, int recno);	

}
 