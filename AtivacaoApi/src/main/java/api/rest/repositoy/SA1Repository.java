package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.totvs.Sa1010;

@Repository
public interface SA1Repository extends CrudRepository<Sa1010, Integer> {

	@Query(value = "SELECT * FROM SA1010 WHERE A1_COD = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public Sa1010 buscaClientePorCodigo(String cod_cli);
	
	@Query(value = "SELECT * FROM SA1010 WHERE A1_UICLAS = 'S' AND A1_ULIBIC = 'N' AND D_E_L_E_T_ = '' ", nativeQuery = true)
	public List<Sa1010> clientesASeremEnviadosIclass();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE SA1010 SET A1_UICLAS = ?1, A1_ULIBIC = ?2, A1_ULIBFL = ?2 WHERE A1_COD = ?3 AND R_E_C_N_O_ = ?4", nativeQuery = true)
	public void marca_cliente(String marcacao, String controle, String cod_cli, int recno);	

}
