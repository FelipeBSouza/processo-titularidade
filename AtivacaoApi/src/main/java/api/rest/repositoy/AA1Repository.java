package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.rest.model.totvs.Aa1010;

@Repository
public interface AA1Repository extends CrudRepository<Aa1010, Integer> {

	// SELECT * FROM AA1010 WHERE AA1_NOMTEC = ?1
	@Query(value = "SELECT * FROM AA1010 WHERE AA1_CODTEC = ?1", nativeQuery = true)
	public Aa1010 buscaArmazenPorCodigoTecnico(String nome_tecnico);
	
	@Query(value = "SELECT AA1_CODTEC FROM AA1010 WHERE AA1_LOCAL = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String buscaCodTecnicoPorArmazem(String cod_armazem);
	
}
