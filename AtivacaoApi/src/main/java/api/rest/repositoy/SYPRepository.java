package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import api.rest.model.totvs.Ab6010;
import api.rest.model.totvs.Syp010;

@Repository
public interface SYPRepository extends CrudRepository<Syp010, Integer> {

	@Query(value = "SELECT REPLACE(YP_TEXTO,'\\13\\10' ,' ') as TEXTO FROM SYP010 WHERE YP_FILIAL = 'LG' AND YP_CHAVE = ?1 ORDER BY R_E_C_N_O_ ASC", nativeQuery = true)
	public List<String> buscaObsOS(String cod_chave);		
	
}
