package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;
import api.rest.model.totvs.Ab6010;
import api.rest.model.totvs.Ab9010;
import api.rest.model.totvs.Sa1010;

@Repository
public interface AB9Repository extends CrudRepository<Ab9010, Integer> {

	@Query(value = "SELECT MAX(AB9_SEQ)+1 AS MAXSEQ FROM AB9010 WHERE AB9_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String buscaProximaSequencia(String num_os);
	
	@Query(value = "SELECT * FROM AB9010 WHERE AB9_NUMOS LIKE %?1% AND D_E_L_E_T_ = '' ORDER BY AB9_SEQ DESC", nativeQuery = true)
	public List<Ab9010> buscaDadosAtendimetoPorNumOS(String num_os);
		
}
