package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import api.rest.model.totvs.Ab6010;

@Repository
public interface AB6Repository extends CrudRepository<Ab6010, Integer> {

	// SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND D_E_L_E_T_ = ''
//	@Query(value = "SELECT * FROM AB6010 WHERE AB6_NUMOS = '174530'", nativeQuery = true) //175052
	@Query(value = "SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND D_E_L_E_T_ = '' AND AB6_STATUS != 'E' AND AB6_STATUS != 'C'", nativeQuery = true)
	public List<Ab6010> listaOsASeremCriadas();

	@Query(value = "SELECT * FROM AB6010 WHERE AB6_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public Ab6010 buscaOSPorNumeroDaOS(String num_os);

	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_ULIBIC = ?1, AB6_ULIBFL = ?4 WHERE R_E_C_N_O_ = ?2 AND AB6_NUMOS = ?3 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void marca_os(String marcacao, int recno, String num_os, String controle);

	@Query(value = "SELECT AB6_UNUMCT FROM AB6010 WHERE AB6_NUMOS = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String buscaNumeroContratoOS(String num_os);

	@Transactional
	@Modifying
	@Query(value = "UPDATE AB6010 SET AB6_STATUS = 'E' WHERE R_E_C_N_O_ = ?1 AND AB6_NUMOS = ?2 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public void encerra_os(int recno, String num_os);
	
	@Query(value = "SELECT * FROM AB6010 WHERE AB6_UICLAS = 'S' AND AB6_ULIBIC = 'N' AND D_E_L_E_T_ = '' AND (AB6_STATUS = 'E' OR AB6_STATUS = 'C')", nativeQuery = true)
	public List<Ab6010> listaOsASeremEncerradasIclass();

}
