package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;
import api.rest.model.totvs.Ab6010;
import api.rest.model.totvs.Sa1010;
import api.rest.model.totvs.Sb1010;

@Repository
public interface SB1Repository extends CrudRepository<Sb1010, Integer> {

	@Query(value = "SELECT * FROM SB1010 WHERE B1_COD = ?1", nativeQuery = true)
	public Sb1010 buscaProdutoPorCodigo(String cod_prod);
	
	@Query(value = "SELECT B1_DESC FROM SB1010 WHERE B1_COD = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public String buscaDescricaoProdutoPorCodigo(String cod_prod);
	
	@Query(value = "SELECT * FROM SB1010 WHERE B1_DESC = ?1 AND D_E_L_E_T_ = ''", nativeQuery = true)
	public Sb1010 buscaProdutoPorDescricao(String descri_prod);

}
