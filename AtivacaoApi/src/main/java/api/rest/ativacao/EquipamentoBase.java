/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.ativacao;

import api.rest.model.integrador.Cliente;
import api.rest.model.totvs.Aa3010;
import br.eng.medeiros.FTTH.Equipamentos.Equipamento;

/**
 *
 * @author ROBSON
 */
public class EquipamentoBase {

    private Equipamento equipamento;
    private Aa3010 baseAtendimento;
    private Cliente cliente;

    public EquipamentoBase() {
    }

    public EquipamentoBase(Equipamento equipamento, Aa3010 baseAtendimento) {
        this.equipamento = equipamento;
        this.baseAtendimento = baseAtendimento;
    }

    public EquipamentoBase(Equipamento equipamento, Aa3010 baseAtendimento, Cliente cliente) {
        this.equipamento = equipamento;
        this.baseAtendimento = baseAtendimento;
        this.cliente = cliente;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public Aa3010 getBaseAtendimento() {
        return baseAtendimento;
    }

    public void setBaseAtendimento(Aa3010 baseAtendimento) {
        this.baseAtendimento = baseAtendimento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
