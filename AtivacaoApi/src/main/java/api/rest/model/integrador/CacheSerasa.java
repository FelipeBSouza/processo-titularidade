package api.rest.model.integrador;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(schema = "integrador", name = "cache_serasa")
public class CacheSerasa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long cd_cache_serasa;

	@Column(name = "cpf_cnpj")
	private String cpf_cnpj;

	@Column(name = "data_busca_cache")
	private String data_busca_cache;

	@Column(name = "data_busca_serasa")
	private String data_busca_serasa;

	@Column(name = "nome")
	private String nome;

	@Column(name = "observacao")
	private String observacao;

	@Column(name = "processo")
	private String processo;

	@Column(name = "situacao_documento")
	private String situacao_documento;

	@Column(name = "status")
	private String status;

	@Column(name = "telefone_contato")
	private String telefone_contato;

	@Column(name = "total_cheques")
	private String total_cheques;

	@Column(name = "total_divida")
	private String total_divida;

	@Column(name = "total_ocorrencias")
	private String total_ocorrencias;

	@Column(name = "total_origens")
	private String total_origens;

	@Column(name = "total_protestos")
	private String total_protestos;

	public long getCd_cache_serasa() {
		return cd_cache_serasa;
	}

	public void setCd_cache_serasa(long cd_cache_serasa) {
		this.cd_cache_serasa = cd_cache_serasa;
	}

	public String getCpf_cnpj() {
		return cpf_cnpj;
	}

	public void setCpf_cnpj(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}

	public String getData_busca_cache() {
		return data_busca_cache;
	}

	public void setData_busca_cache(String data_busca_cache) {
		this.data_busca_cache = data_busca_cache;
	}

	public String getData_busca_serasa() {
		return data_busca_serasa;
	}

	public void setData_busca_serasa(String data_busca_serasa) {
		this.data_busca_serasa = data_busca_serasa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public String getSituacao_documento() {
		return situacao_documento;
	}

	public void setSituacao_documento(String situacao_documento) {
		this.situacao_documento = situacao_documento;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTelefone_contato() {
		return telefone_contato;
	}

	public void setTelefone_contato(String telefone_contato) {
		this.telefone_contato = telefone_contato;
	}

	public String getTotal_cheques() {
		return total_cheques;
	}

	public void setTotal_cheques(String total_cheques) {
		this.total_cheques = total_cheques;
	}

	public String getTotal_divida() {
		return total_divida;
	}

	public void setTotal_divida(String total_divida) {
		this.total_divida = total_divida;
	}

	public String getTotal_ocorrencias() {
		return total_ocorrencias;
	}

	public void setTotal_ocorrencias(String total_ocorrencias) {
		this.total_ocorrencias = total_ocorrencias;
	}

	public String getTotal_origens() {
		return total_origens;
	}

	public void setTotal_origens(String total_origens) {
		this.total_origens = total_origens;
	}

	public String getTotal_protestos() {
		return total_protestos;
	}

	public void setTotal_protestos(String total_protestos) {
		this.total_protestos = total_protestos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
