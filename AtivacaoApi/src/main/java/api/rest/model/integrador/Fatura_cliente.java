package api.rest.model.integrador;

import api.rest.model.telefonia.Planos;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador", name = "FATURA_CLIENTE")
public class Fatura_cliente implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @JsonIgnore
    @JoinColumn(name = "cd_cliente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cliente cd_cliente;
    @Column(name = "dt_faturamento")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dt_faturamento;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Planos plano;
    @Column(name = "vl_consumo")
    private Double vl_consumo;
    @JsonIgnore    
    @OneToOne(mappedBy = "fatura_cliente", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    private ContasReceber contasReceber;
    @Column(name = "total_internet")
    private Double total_internet;
    @Column(name = "total_telefone")
    private Double total_telefone;
    @Column(name = "total_adicionais")
    private Double total_adicionais;
    @Column(name = "total_adicional_desconto")
    private Double total_adicional_desconto;

    public Fatura_cliente() {
    }

    public Cliente getCd_cliente() {
        return cd_cliente;
    }

    public void setCd_cliente(Cliente cd_cliente) {
        this.cd_cliente = cd_cliente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDt_faturamento() {
        return dt_faturamento;
    }

    public void setDt_faturamento(Date dt_faturamento) {
        this.dt_faturamento = dt_faturamento;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public Planos getPlano() {
        return plano;
    }

    public void setPlano(Planos plano) {
        this.plano = plano;
    }

    public Double getVl_consumo() {
        return vl_consumo;
    }

    public void setVl_consumo(Double vl_consumo) {
        this.vl_consumo = vl_consumo;
    }

    public Double getTotal_internet() {
        return total_internet;
    }

    public void setTotal_internet(Double total_internet) {
        this.total_internet = total_internet;
    }

    public Double getTotal_telefone() {
        return total_telefone;
    }

    public void setTotal_telefone(Double total_telefone) {
        this.total_telefone = total_telefone;
    }

    public Double getTotal_adicionais() {
        return total_adicionais;
    }

    public void setTotal_adicionais(Double total_adicionais) {
        this.total_adicionais = total_adicionais;
    }

    public Double getTotal_adicional_desconto() {
        return total_adicional_desconto;
    }

    public void setTotal_adicional_desconto(Double total_adicional_desconto) {
        this.total_adicional_desconto = total_adicional_desconto;
    }

    public Double getTotal_Fatura() {
        try {
            return total_telefone + total_internet + vl_consumo + total_adicionais - total_adicional_desconto;
        } catch (Exception e) {
            return null;
        }
    }
}
