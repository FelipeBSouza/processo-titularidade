/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.fibra;

import api.rest.model.integrador.equipamento.Modelo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "Caixa")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class Caixa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_caixa", nullable = false)
    private int cd_caixa;
    @Column(name = "ds_caixa")
    private String ds_caixa;
    @Column(name = "tipo", length = 2)
    private String tipo;
    @Column(name = "pt_referencia")
    private String pt_referencia;
    @Column(name = "nr_aproximado")
    private String nr_aproximado;
    @Column(name = "area", length = 3)
    private String area = "CPM";
    @Column(name = "endereco1")
    private String endereco1;
    @Column(name = "cep1")
    private String cep1;
    @Column(name = "endereco2")
    private String endereco2;
    @Column(name = "cep2")
    private String cep2; 
    //
    @JsonIgnore
    @OneToMany(mappedBy = "caixa", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private List<Splitter> splitters = new ArrayList<Splitter>();
    //
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_modelo")
    private Modelo modelo;

    public Caixa() {
    }

    public int getCd_caixa() {
        return cd_caixa;
    }

    public void setCd_caixa(int cd_caixa) {
        this.cd_caixa = cd_caixa;
    }

    public String getCep1() {
        return cep1;
    }

    public void setCep1(String cep1) {
        this.cep1 = cep1;
    }

    public String getCep2() {
        return cep2;
    }

    public void setCep2(String cep2) {
        this.cep2 = cep2;
    }

    public String getDs_caixa() {
        return ds_caixa;
    }

    public void setDs_caixa(String ds_caixa) {
        this.ds_caixa = ds_caixa;
    }

    public String getEndereco1() {
        return endereco1;
    }

    public void setEndereco1(String endereco1) {
        this.endereco1 = endereco1;
    }

    public String getEndereco2() {
        return endereco2;
    }

    public void setEndereco2(String endereco2) {
        this.endereco2 = endereco2;
    }

    public String getNr_aproximado() {
        return nr_aproximado;
    }

    public void setNr_aproximado(String nr_aproximado) {
        this.nr_aproximado = nr_aproximado;
    }

    public String getPt_referencia() {
        return pt_referencia;
    }

    public void setPt_referencia(String pt_referencia) {
        this.pt_referencia = pt_referencia;
    }

    public List<Splitter> getSplitters() {
        return splitters;
    }

    public void setSplitters(List<Splitter> splitters) {
        this.splitters = splitters;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Caixa other = (Caixa) obj;
        if (this.cd_caixa != other.cd_caixa) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.cd_caixa;
        return hash;
    }

    @Override
    public String toString() {
        return ds_caixa;
    }
}
