/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador;

/**
 *
 * @author popovicz
 */
public enum TipoONT {    
    FIBER("FIBER"),
    ZHONE("ZHONE");

    private String value;

    TipoONT(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }

}
