/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@XmlRootElement
@Entity
@Table(schema = "integrador",name = "cedente")
public class CedenteEmpresa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_cedente", nullable = false)
    private int cd_cedente;
    @Column(name = "ds_cedente")
    private String ds_cedente;
    @Column(name = "cnpj")
    private String cnpj;
    @Column(name = "banco")
    private String banco;
    @Column(name = "nr_conta")
    private Integer nr_conta;
    @Column(name = "nr_conta_verificador")
    private Integer nr_conta_verificador;
    @Column(name = "nr_carteira")
    private Integer nr_carteira;
    @Column(name = "nr_agencia")
    private Integer nr_agencia;
    @Column(name = "nr_agencia_verificador")
    private Integer nr_agencia_verificador;
    
    @Column(name = "situacao", length=2)
    private String situacao;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="CD_BANCO")
    private Banco BANCO;
    
    public CedenteEmpresa() {
    }

        
    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getCd_cedente() {
        return cd_cedente;
    }

    public void setCd_cedente(int cd_cedente) {
        this.cd_cedente = cd_cedente;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getDs_cedente() {
        return ds_cedente;
    }

    public void setDs_cedente(String ds_cedente) {
        this.ds_cedente = ds_cedente;
    }

    public Integer getNr_agencia() {
        return nr_agencia;
    }

    public void setNr_agencia(Integer nr_agencia) {
        this.nr_agencia = nr_agencia;
    }

    public Integer getNr_agencia_verificador() {
        return nr_agencia_verificador;
    }

    public void setNr_agencia_verificador(Integer nr_agencia_verificador) {
        this.nr_agencia_verificador = nr_agencia_verificador;
    }

    public Integer getNr_carteira() {
        return nr_carteira;
    }

    public void setNr_carteira(Integer nr_carteira) {
        this.nr_carteira = nr_carteira;
    }

    public Integer getNr_conta() {
        return nr_conta;
    }

    public void setNr_conta(Integer nr_conta) {
        this.nr_conta = nr_conta;
    }

    public Integer getNr_conta_verificador() {
        return nr_conta_verificador;
    }

    public void setNr_conta_verificador(Integer nr_conta_verificador) {
        this.nr_conta_verificador = nr_conta_verificador;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CedenteEmpresa other = (CedenteEmpresa) obj;
        if (this.cd_cedente != other.cd_cedente) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.cd_cedente;
        return hash;
    }
}
