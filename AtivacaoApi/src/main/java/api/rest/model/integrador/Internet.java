package api.rest.model.integrador;

import api.rest.model.internet.RadUserGroup;
import api.rest.model.internet.Radcheck;
import api.rest.model.internet.Radreply;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement
@Entity
@Table(schema = "integrador", name = "Internet")
public class Internet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_internet", nullable = false)
    private int cd_internet;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_cliente")
    private Cliente cd_cliente;
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    private Radcheck cd_radcheck_senha = new Radcheck();
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    private Radcheck cd_radcheck_mac = new Radcheck();
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    private Radreply cd_radreply_velocidade = new Radreply();
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Radreply cd_radreply_ip;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cd_radusergroup")
    private RadUserGroup radUserGroup = new RadUserGroup();
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dt_ativacao;
    private Double valor;
    private String ip_antigo = "255.255.255.255";
    @Column(length = 3)
    private String area = "CPM";
    private char in_situacao = 'A';
    @OneToMany(mappedBy = "internet", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Radreply> radreplys = new ArrayList<Radreply>();

    public Internet() {
    }

    public Internet(Cliente cd_cliente) {
        this.cd_cliente = cd_cliente;
    }

    public Internet(Cliente cd_cliente, Radcheck cd_radcheck_senha, Radcheck cd_radcheck_mac, Radreply cd_radreply_velocidade) {
        this.cd_cliente = cd_cliente;
        this.cd_radcheck_senha = cd_radcheck_senha;
        this.cd_radcheck_mac = cd_radcheck_mac;
        this.cd_radreply_velocidade = cd_radreply_velocidade;
    }

    public Internet(Radcheck cd_radcheck_senha, Radcheck cd_radcheck_mac, Radreply cd_radreply_velocidade) {
        this.cd_radcheck_senha = cd_radcheck_senha;
        this.cd_radcheck_mac = cd_radcheck_mac;
        this.cd_radreply_velocidade = cd_radreply_velocidade;
    }

    public Cliente getCd_cliente() {
        return cd_cliente;
    }

    public void setCd_cliente(Cliente cd_cliente) {
        this.cd_cliente = cd_cliente;
    }

    public int getCd_internet() {
        return cd_internet;
    }

    public void setCd_internet(int cd_internet) {
        this.cd_internet = cd_internet;
    }

    public Radcheck getCd_radcheck_mac() {
        return cd_radcheck_mac;
    }

    public void setCd_radcheck_mac(Radcheck cd_radcheck_mac) {
        this.cd_radcheck_mac = cd_radcheck_mac;
    }

    public Radcheck getCd_radcheck_senha() {
        return cd_radcheck_senha;
    }

    public void setCd_radcheck_senha(Radcheck cd_radcheck_senha) {
        this.cd_radcheck_senha = cd_radcheck_senha;
    }

    public Radreply getCd_radreply_ip() {
        return cd_radreply_ip;
    }

    public void setCd_radreply_ip(Radreply cd_radreply_ip) {
        this.cd_radreply_ip = cd_radreply_ip;
    }

    public Radreply getCd_radreply_velocidade() {
        return cd_radreply_velocidade;
    }

    public void setCd_radreply_velocidade(Radreply cd_radreply_velocidade) {
        this.cd_radreply_velocidade = cd_radreply_velocidade;
    }

    public void removeCliente() {
        this.cd_cliente = new Cliente();
    }

    public Date getDt_ativacao() {
        return dt_ativacao;
    }

    public void setDt_ativacao(Date dt_ativacao) {
        this.dt_ativacao = dt_ativacao;
    }

    public RadUserGroup getRadUserGroup() {
        return radUserGroup;
    }

    public void setRadUserGroup(RadUserGroup radUserGroup) {
        this.radUserGroup = radUserGroup;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getIp_antigo() {
        return ip_antigo;
    }

    public void setIp_antigo(String ip_antigo) {
        this.ip_antigo = ip_antigo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public char getIn_situacao() {
        return in_situacao;
    }

    public void setIn_situacao(char in_situacao) {
        this.in_situacao = in_situacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.cd_internet;
        return hash;
    }

    public List<Radreply> getRadreplys() {
        return radreplys;
    }

    public void setRadreplys(List<Radreply> radreplys) {
        this.radreplys = radreplys;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Internet other = (Internet) obj;
        if (this.cd_internet != other.cd_internet) {
            return false;
        }
        return true;
    }
}
