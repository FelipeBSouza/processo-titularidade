package api.rest.model.telefonia;
// Generated 08/01/2013 14:12:16 by Hibernate Tools 3.2.1.GA

import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.Titularidade;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@Entity
@Table(schema = "telefonia", name = "numero")
public class Numero implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
  //  @Transient
  //  private Sippeers sippeers;
  //  @Column(name = "cd_sippeers")
  //  private long cd_sippeers;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_sippeers", referencedColumnName = "id")
    private Sippeers sippeers;
    
    @Pattern(regexp = "\\(?\\b([0-9]{2})\\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})\\b", message = "Telefone em formato incorreto")
    private String numero;
    private String tipo;
    private String destino;
    private String area;
    private String status;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data_ativacao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data_desativacao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data_inclusao = new Date();
    private Integer cd_empresa;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_cliente")
    private Cliente cd_cliente;
    private boolean bloqueado_entrada = false;
    private boolean bloqueado_saida = false;
    private double saldo = 0;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plano")
    private Planos plano;
    private char prepos = 'O';
    private boolean permite_intra;
    private boolean permite_fix_Local;
    private boolean permite_fix_ddd;
    private boolean permite_mov_local;
    private boolean permite_mov_ddd;
    private boolean permite_internacional;
    private boolean permite_ldn_terceiros = false;
    private boolean permite_sgsempre = false;
    private boolean permite_sgocupado = false;
    private boolean permite_sgnaoatende = false;
    private double limite = 100;
    private boolean clir = false;
    private boolean permite_a_cobrar;
    private double franquia;
    private String numerocob;
    private Boolean permite_0300;
    private String ddr;
    private String codecs = "ulaw,alaw,g729";
    private Double franquia_movel;
    private Double franquia_ldn;    
   
    @JsonIgnore
    @ManyToOne( cascade = CascadeType.ALL, fetch = FetchType.LAZY)       
    private Titularidade titular;
    
    public Numero() {
    	
    }

    public Numero(Cliente cd_cliente) {
        this.cd_cliente = cd_cliente;
    }
    
    public Numero(String numero) {
        this.numero = numero;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public boolean isBloqueado_entrada() {
        return bloqueado_entrada;
    }

    public String getBloqueado_entradaFormat() {
        if (bloqueado_entrada) {
            return "SIM";
        } else {
            return "NAO";
        }
    }

    public void setBloqueado_entrada(boolean bloqueado_entrada) {
        this.bloqueado_entrada = bloqueado_entrada;
    }

    public boolean isBloqueado_saida() {
        return bloqueado_saida;
    }

    public String getBloqueado_saidaFormat() {
        if (bloqueado_saida) {
            return "SIM";
        } else {
            return "NAO";
        }
    }

    public void setBloqueado_saida(boolean bloqueado_saida) {
        this.bloqueado_saida = bloqueado_saida;
    }

    public Cliente getCd_cliente() {
        return cd_cliente;
    }

    public void setCd_cliente(Cliente cd_cliente) {
        this.cd_cliente = cd_cliente;
    }

    public Integer getCd_empresa() {
        return cd_empresa;
    }

    public void setCd_empresa(Integer cd_empresa) {
        this.cd_empresa = cd_empresa;
    }

    public boolean isClir() {
        return clir;
    }

    public void setClir(boolean clir) {
        this.clir = clir;
    }

    public String getCodecs() {
        return codecs;
    }

    public void setCodecs(String codecs) {
        this.codecs = codecs;
    }

    public Date getData_ativacao() {
        return data_ativacao;
    }

    public void setData_ativacao(Date data_ativacao) {
        this.data_ativacao = data_ativacao;
    }

    public Date getData_desativacao() {
        return data_desativacao;
    }

    public void setData_desativacao(Date data_desativacao) {
        this.data_desativacao = data_desativacao;
    }

    public Date getData_inclusao() {
        return data_inclusao;
    }

    public void setData_inclusao(Date data_inclusao) {
        this.data_inclusao = data_inclusao;
    }

    public String getDdr() {
        return ddr;
    }

    public void setDdr(String ddr) {
        this.ddr = ddr;
    }

    public double getFranquia() {
        return franquia;
    }

    public void setFranquia(double franquia) {
        this.franquia = franquia;
    }

    public Double getFranquia_ldn() {
        return franquia_ldn;
    }

    public void setFranquia_ldn(Double franquia_ldn) {
        this.franquia_ldn = franquia_ldn;
    }

    public Double getFranquia_movel() {
        return franquia_movel;
    }

    public void setFranquia_movel(Double franquia_movel) {
        this.franquia_movel = franquia_movel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public String getNumerocob() {
        return numerocob;
    }

    public void setNumerocob(String numerocob) {
        this.numerocob = numerocob;
    }

    public Boolean getPermite_0300() {
        return permite_0300;
    }

    public void setPermite_0300(Boolean permite_0300) {
        this.permite_0300 = permite_0300;
    }

    public boolean isPermite_a_cobrar() {
        return permite_a_cobrar;
    }

    public String getPermite_a_cobrarFormat() {
        if (permite_a_cobrar) {
            return "SIM";
        } else {
            return "NAO";
        }
    }

    public void setPermite_a_cobrar(boolean permite_a_cobrar) {
        this.permite_a_cobrar = permite_a_cobrar;
    }

    public boolean isPermite_fix_Local() {
        return permite_fix_Local;
    }

    public void setPermite_fix_Local(boolean permite_fix_Local) {
        this.permite_fix_Local = permite_fix_Local;
    }

    public boolean isPermite_fix_ddd() {
        return permite_fix_ddd;
    }

    public void setPermite_fix_ddd(boolean permite_fix_ddd) {
        this.permite_fix_ddd = permite_fix_ddd;
    }

    public boolean isPermite_internacional() {
        return permite_internacional;
    }

    public void setPermite_internacional(boolean permite_internacional) {
        this.permite_internacional = permite_internacional;
    }

    public boolean isPermite_intra() {
        return permite_intra;
    }

    public void setPermite_intra(boolean permite_intra) {
        this.permite_intra = permite_intra;
    }

    public boolean isPermite_ldn_terceiros() {
        return permite_ldn_terceiros;
    }

    public void setPermite_ldn_terceiros(boolean permite_ldn_terceiros) {
        this.permite_ldn_terceiros = permite_ldn_terceiros;
    }

    public boolean isPermite_mov_ddd() {
        return permite_mov_ddd;
    }

    public String getPermite_mov_dddFormat() {
        if (permite_mov_ddd) {
            return "SIM";
        } else {
            return "NAO";
        }
    }

    public void setPermite_mov_ddd(boolean permite_mov_ddd) {
        this.permite_mov_ddd = permite_mov_ddd;
    }

    public boolean isPermite_mov_local() {
        return permite_mov_local;
    }

    public String getPermite_mov_localFormat() {
        if (permite_mov_local) {
            return "SIM";
        } else {
            return "NAO";
        }
    }

    public void setPermite_mov_local(boolean permite_mov_local) {
        this.permite_mov_local = permite_mov_local;
    }

    public Planos getPlano() {
        return plano;
    }

    public void setPlano(Planos plano) {
        this.plano = plano;
    }

    public char getPrepos() {
        return prepos;
    }

    public void setPrepos(char prepos) {
        this.prepos = prepos;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Sippeers getSippeers() {
        return sippeers;
    }

    public void setSippeers(Sippeers sippeers) {
        this.sippeers = sippeers;
    }

    public Titularidade getTitular() {
        return titular;
    }

    public void setTitular(Titularidade titular) {
        this.titular = titular;
    }

    public boolean isPermite_sgsempre() {
        return permite_sgsempre;
    }

    public void setPermite_sgsempre(boolean permite_sgsempre) {
        this.permite_sgsempre = permite_sgsempre;
    }

    public boolean isPermite_sgocupado() {
        return permite_sgocupado;
    }

    public void setPermite_sgocupado(boolean permite_sgocupado) {
        this.permite_sgocupado = permite_sgocupado;
    }

    public boolean isPermite_sgnaoatende() {
        return permite_sgnaoatende;
    }

    public void setPermite_sgnaoatende(boolean permite_sgnaoatende) {
        this.permite_sgnaoatende = permite_sgnaoatende;
    }

//    public long getCd_sippeers() {
//        return cd_sippeers;
//    }
//
//    public void setCd_sippeers(long cd_sippeers) {
//        this.cd_sippeers = cd_sippeers;
//    }

    @Override
    public String toString() {
        return this.numero;
    }

    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Numero other = (Numero) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
