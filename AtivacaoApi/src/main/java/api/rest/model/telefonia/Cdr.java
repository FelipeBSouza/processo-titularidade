package api.rest.model.telefonia;
// Generated 11/12/2012 10:01:38 by Hibernate Tools 3.2.1.GA

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@Entity
@Table(schema = "telefonia")
public class Cdr implements java.io.Serializable {

    @Id
    private long id;
    private String accountcode;
    private String src;
    private String callerid;
    private boolean acobrar;
    private String operadora;
    private Double valor;
    private Double total;
    private String tipo;
    private String status;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date data;
    private int duracao;
    private int tempotarifado;
    private String destino;
    private String csp;
    private String localidade;
    private String cnl;
    private String unico;
    private boolean acordo_tarifacao;
    @JoinColumn(name = "plano")
    @ManyToOne(fetch = FetchType.LAZY)
    private Planos plano;
    private Integer tempo_valido;
    private Double custo;
    private Long idcdrg;
    private Double vrfranqueado;
    private String uf;

    public Cdr() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccountcode() {
        return this.accountcode;
    }

    public void setAccountcode(String accountcode) {
        this.accountcode = accountcode;
    }

    public String getSrc() {
        return this.src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getCallerid() {
        return callerid;
    }

    public void setCallerid(String callerid) {
        this.callerid = callerid;
    }

    public boolean isAcobrar() {
        return this.acobrar;
    }

    public void setAcobrar(boolean acobrar) {
        this.acobrar = acobrar;
    }

    public String getOperadora() {
        return this.operadora;
    }

    public void setOperadora(String operadora) {
        this.operadora = operadora;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getTotal() {
        return this.total;
    }

    public String getTotalFormat() {
        return String.format("%.2f", this.total);
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getData() {
        return data;
    }

    public String getDataFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            return sdf.format(this.data);
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getDuracao() {
        return duracao;
    }

    public String getDuracaoFormat() throws ParseException {
        int segundos = this.duracao;
        int segundo = segundos % 60;
        int minutos = segundos / 60;
        int minuto = minutos % 60;
        int hora = minutos / 60;


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Date data1 = sdf.parse(hora + ":" + minuto + ":" + segundo);
        String data_minuto = sdf.format(data1);
        return data_minuto;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public int getTempotarifado() {
        return this.tempotarifado;
    }

    public void setTempotarifado(int tempotarifado) {
        this.tempotarifado = tempotarifado;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getCsp() {
        return this.csp;
    }

    public void setCsp(String csp) {
        this.csp = csp;
    }

    public String getLocalidade() {
        return this.localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getCnl() {
        return this.cnl;
    }

    public void setCnl(String cnl) {
        this.cnl = cnl;
    }

    public String getUnico() {
        return this.unico;
    }

    public void setUnico(String unico) {
        this.unico = unico;
    }

    public boolean isAcordo_tarifacao() {
        return acordo_tarifacao;
    }

    public void setAcordo_tarifacao(boolean acordo_tarifacao) {
        this.acordo_tarifacao = acordo_tarifacao;
    }

    public Planos getPlano() {
        return plano;
    }

    public void setPlano(Planos plano) {
        this.plano = plano;
    }

    public Integer getTempo_valido() {
        return tempo_valido;
    }

    public String getTempo_validoFormat() throws ParseException {
        int segundos = this.tempo_valido;
        int segundo = segundos % 60;
        int minutos = segundos / 60;
        int minuto = minutos % 60;
        int hora = minutos / 60;


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Date data1 = sdf.parse(hora + ":" + minuto + ":" + segundo);
        String data_minuto = sdf.format(data1);
        return data_minuto;
    }

    public void setTempo_valido(Integer tempo_valido) {
        this.tempo_valido = tempo_valido;
    }

    public Double getCusto() {
        return this.custo;
    }

    public void setCusto(Double custo) {
        this.custo = custo;
    }

    public Long getIdcdrg() {
        return this.idcdrg;
    }

    public void setIdcdrg(Long idcdrg) {
        this.idcdrg = idcdrg;
    }

    public Double getVrfranqueado() {
        return this.vrfranqueado;
    }

    public void setVrfranqueado(Double vrfranqueado) {
        this.vrfranqueado = vrfranqueado;
    }

     public String getVrfranqueadoFormat() {
        return String.format("%.2f", this.vrfranqueado);
    }

    public String getUf() {
        return this.uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
}
