/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ROBSON
 */

@Entity  
@Table(name = "SF2010", schema = "dbo")
public class Sf2010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_DOC")
    private String f2Doc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_SERIE")
    private String f2Serie;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "F2_CLIENTE", referencedColumnName = "A1_COD", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "F2_LOJA", referencedColumnName = "A1_LOJA", nullable = false, insertable = false, updatable = false),})
    private Sa1010 sa1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_COND")
    private String f2Cond;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_DUPL")
    private String f2Dupl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_EMISSAO")
    private String f2Emissao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F2_EST")
    private String f2Est;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_FRETE")
    private double f2Frete;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_SEGURO")
    private double f2Seguro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_ICMFRET")
    private double f2Icmfret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_TIPOCLI")
    private String f2Tipocli;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALBRUT")
    private double f2Valbrut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALICM")
    private double f2Valicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEICM")
    private double f2Baseicm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIPI")
    private double f2Valipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEIPI")
    private double f2Baseipi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALMERC")
    private double f2Valmerc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_NFORI")
    private String f2Nfori;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_DESCONT")
    private double f2Descont;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_SERIORI")
    private String f2Seriori;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_TIPO")
    private String f2Tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F2_ESPECI1")
    private String f2Especi1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F2_ESPECI2")
    private String f2Especi2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F2_ESPECI3")
    private String f2Especi3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F2_ESPECI4")
    private String f2Especi4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VOLUME1")
    private double f2Volume1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VOLUME2")
    private double f2Volume2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VOLUME3")
    private double f2Volume3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VOLUME4")
    private double f2Volume4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_ICMSRET")
    private double f2Icmsret;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_PLIQUI")
    private double f2Pliqui;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_PBRUTO")
    private double f2Pbruto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_TRANSP")
    private String f2Transp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_REDESP")
    private String f2Redesp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_VEND1")
    private String f2Vend1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_VEND2")
    private String f2Vend2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_VEND3")
    private String f2Vend3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_VEND4")
    private String f2Vend4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_VEND5")
    private String f2Vend5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "F2_OK")
    private String f2Ok;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_FIMP")
    private String f2Fimp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DTLANC")
    private String f2Dtlanc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DTREAJ")
    private String f2Dtreaj;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_REAJUST")
    private String f2Reajust;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DTBASE0")
    private String f2Dtbase0;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_FATORB0")
    private double f2Fatorb0;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DTBASE1")
    private String f2Dtbase1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_FATORB1")
    private double f2Fatorb1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VARIAC")
    private double f2Variac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "F2_FILIAL")
    private String f2Filial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEISS")
    private double f2Baseiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALISS")
    private double f2Valiss;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALFAT")
    private double f2Valfat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_CONTSOC")
    private double f2Contsoc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BRICMS")
    private double f2Bricms;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_FRETAUT")
    private double f2Fretaut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_ICMAUTO")
    private double f2Icmauto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_DESPESA")
    private double f2Despesa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_NEXTSER")
    private String f2Nextser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_NEXTDOC")
    private String f2Nextdoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "F2_ESPECIE")
    private String f2Especie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F2_PDV")
    private String f2Pdv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F2_MAPA")
    private String f2Mapa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_ECF")
    private String f2Ecf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_PREFIXO")
    private String f2Prefixo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASIMP1")
    private double f2Basimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASIMP2")
    private double f2Basimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASIMP3")
    private double f2Basimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASIMP4")
    private double f2Basimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASIMP5")
    private double f2Basimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASIMP6")
    private double f2Basimp6;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIMP1")
    private double f2Valimp1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIMP2")
    private double f2Valimp2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIMP3")
    private double f2Valimp3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIMP4")
    private double f2Valimp4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIMP5")
    private double f2Valimp5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIMP6")
    private double f2Valimp6;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_ORDPAGO")
    private String f2Ordpago;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "F2_NFCUPOM")
    private String f2Nfcupom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALINSS")
    private double f2Valinss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "F2_HORA")
    private String f2Hora;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_MOEDA")
    private double f2Moeda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_REGIAO")
    private String f2Regiao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALCSLL")
    private double f2Valcsll;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALCOFI")
    private double f2Valcofi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALPIS")
    private double f2Valpis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_LOTE")
    private String f2Lote;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_TXMOEDA")
    private double f2Txmoeda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "F2_CLEOK")
    private String f2Cleok;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "F2_CHVCLE")
    private String f2Chvcle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "F2_IDCLE")
    private String f2Idcle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALIRRF")
    private double f2Valirrf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_CARGA")
    private String f2Carga;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_RECFAUT")
    private String f2Recfaut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F2_SEQCAR")
    private String f2Seqcar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEINS")
    private double f2Baseins;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_PEDPEND")
    private String f2Pedpend;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_DESCCAB")
    private double f2Desccab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DTENTR")
    private String f2Dtentr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_FORMUL")
    private String f2Formul;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F2_TIPODOC")
    private String f2Tipodoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_NFEACRS")
    private String f2Nfeacrs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_TIPOREM")
    private String f2Tiporem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_SEQENT")
    private String f2Seqent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_ICMSDIF")
    private double f2Icmsdif;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALACRS")
    private double f2Valacrs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_RECISS")
    private String f2Reciss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_TIPORET")
    private String f2Tiporet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALCF3")
    private double f2Valcf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASECF3")
    private double f2Basecf3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALPS3")
    private double f2Valps3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEPS3")
    private double f2Baseps3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEIRR")
    private double f2Baseirr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_ESTCRED")
    private double f2Estcred;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALTST")
    private double f2Valtst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "F2_FILDEST")
    private String f2Fildest;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_EMINFE")
    private String f2Eminfe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASPIS")
    private double f2Baspis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "F2_CGCCLI")
    private String f2Cgccli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_FLAGDEV")
    private String f2Flagdev;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "F2_DIACTB")
    private String f2Diactb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "F2_NODIA")
    private String f2Nodia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "F2_HAWB")
    private String f2Hawb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_VEICUL2")
    private String f2Veicul2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_VEICUL1")
    private String f2Veicul1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_ORDSEP")
    private String f2Ordsep;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_TPFRETE")
    private String f2Tpfrete;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_CREDNFE")
    private double f2Crednfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 44)
    @Column(name = "F2_CHVNFE")
    private String f2Chvnfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_HORNFE")
    private String f2Hornfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "F2_CODNFE")
    private String f2Codnfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_RECOPI")
    private String f2Recopi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BSREIN")
    private double f2Bsrein;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_FORMDES")
    private String f2Formdes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "F2_NUMORC")
    private String f2Numorc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_NFELETR")
    private String f2Nfeletr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEINA")
    private double f2Baseina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALFAC")
    private double f2Valfac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALFET")
    private double f2Valfet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALINA")
    private double f2Valina;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "F2_IDSA1")
    private String f2Idsa1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "F2_IDSA2")
    private String f2Idsa2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "F2_IDSED")
    private String f2Idsed;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DTDIGIT")
    private String f2Dtdigit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_PREFORI")
    private String f2Prefori;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASCOFI")
    private double f2Bascofi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASCSLL")
    private double f2Bascsll;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VLR_FRT")
    private double f2VlrFrt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALFDS")
    private double f2Valfds;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_LOJENT")
    private String f2Lojent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_CLIENT")
    private String f2Client;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASETST")
    private double f2Basetst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "F2_NFICMST")
    private String f2Nficmst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_REFMOED")
    private double f2Refmoed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_REFTAXA")
    private double f2Reftaxa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_VEICUL3")
    private String f2Veicul3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_BASEFUN")
    private double f2Basefun;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VLSENAR")
    private double f2Vlsenar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_LOJADES")
    private String f2Lojades;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VREINT")
    private double f2Vreint;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_FORDES")
    private String f2Fordes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DTTXREF")
    private String f2Dttxref;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_TXREF")
    private double f2Txref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "F2_NTFECP")
    private String f2Ntfecp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_SERSUBS")
    private String f2Sersubs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_NFSUBST")
    private String f2Nfsubst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_DESCZFR")
    private double f2Desczfr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_VALFAB")
    private double f2Valfab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_CODRGS")
    private String f2Codrgs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "F2_DAUTNFE")
    private String f2Dautnfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_EVENFLG")
    private String f2Evenflg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "F2_FLAGRGS")
    private String f2Flagrgs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "F2_HAUTNFE")
    private String f2Hautnfe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 54)
    @Column(name = "F2_IDCCE")
    private String f2Idcce;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 54)
    @Column(name = "F2_IDRGS")
    private String f2Idrgs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_TPNFEXP")
    private String f2Tpnfexp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "F2_TOTIMP")
    private double f2Totimp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "F2_TIPIMP")
    private String f2Tipimp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "F2_SERMDF")
    private String f2Sermdf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "F2_NUMMDF")
    private String f2Nummdf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @OneToMany(mappedBy = "sf2")
    private List<Sd2010> sd2010s = new ArrayList<Sd2010>();

    public Sf2010() {
    }

    public Sf2010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getF2Doc() {
        return f2Doc;
    }

    public void setF2Doc(String f2Doc) {
        this.f2Doc = f2Doc;
    }

    
    public String getF2Serie() {
        return f2Serie;
    }

    
    public String getF2SerieFmt() {
        if (f2Serie.contains("C")) {
            return "21";
        }
        if (f2Serie.contains("D")) {
            return "22";
        }
        return f2Serie;
    }

    public void setF2Serie(String f2Serie) {
        this.f2Serie = f2Serie;
    }

    public Sa1010 getSa1() {
        return sa1;
    }

    public void setSa1(Sa1010 sa1) {
        this.sa1 = sa1;
    }

    public String getF2Cond() {
        return f2Cond;
    }

    public void setF2Cond(String f2Cond) {
        this.f2Cond = f2Cond;
    }

    public String getF2Dupl() {
        return f2Dupl;
    }

    public void setF2Dupl(String f2Dupl) {
        this.f2Dupl = f2Dupl;
    }

    public String getF2Emissao() {
        return f2Emissao;
    }

    public Date getF2EmissaoDt() throws ParseException {
        if (!f2Emissao.isEmpty()) {
            int ano = Integer.parseInt(f2Emissao.substring(0, 4));
            int mes = Integer.parseInt(f2Emissao.substring(4, 6));
            int dia = Integer.parseInt(f2Emissao.substring(6, 8));

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = dateFormat.parse(String.format("%02d", dia) + "/" + String.format("%02d", mes) + "/" + ano);
            return date;
        }
        return null;
    }

    public void setF2Emissao(String f2Emissao) {
        this.f2Emissao = f2Emissao;
    }

    public String getF2Est() {
        return f2Est;
    }

    public void setF2Est(String f2Est) {
        this.f2Est = f2Est;
    }

    public double getF2Frete() {
        return f2Frete;
    }

    public void setF2Frete(double f2Frete) {
        this.f2Frete = f2Frete;
    }

    public double getF2Seguro() {
        return f2Seguro;
    }

    public void setF2Seguro(double f2Seguro) {
        this.f2Seguro = f2Seguro;
    }

    public double getF2Icmfret() {
        return f2Icmfret;
    }

    public void setF2Icmfret(double f2Icmfret) {
        this.f2Icmfret = f2Icmfret;
    }

    public String getF2Tipocli() {
        return f2Tipocli;
    }

    public void setF2Tipocli(String f2Tipocli) {
        this.f2Tipocli = f2Tipocli;
    }

    public double getF2Valbrut() {
        return f2Valbrut;
    }

    public void setF2Valbrut(double f2Valbrut) {
        this.f2Valbrut = f2Valbrut;
    }

    public double getF2Valicm() {
        return f2Valicm;
    }

    public void setF2Valicm(double f2Valicm) {
        this.f2Valicm = f2Valicm;
    }

    public double getF2Baseicm() {
        return f2Baseicm;
    }

    public void setF2Baseicm(double f2Baseicm) {
        this.f2Baseicm = f2Baseicm;
    }

    public double getF2Valipi() {
        return f2Valipi;
    }

    public void setF2Valipi(double f2Valipi) {
        this.f2Valipi = f2Valipi;
    }

    public double getF2Baseipi() {
        return f2Baseipi;
    }

    public void setF2Baseipi(double f2Baseipi) {
        this.f2Baseipi = f2Baseipi;
    }

    public double getF2Valmerc() {
        return f2Valmerc;
    }

    public void setF2Valmerc(double f2Valmerc) {
        this.f2Valmerc = f2Valmerc;
    }

    public String getF2Nfori() {
        return f2Nfori;
    }

    public void setF2Nfori(String f2Nfori) {
        this.f2Nfori = f2Nfori;
    }

    public double getF2Descont() {
        return f2Descont;
    }

    public void setF2Descont(double f2Descont) {
        this.f2Descont = f2Descont;
    }

    public String getF2Seriori() {
        return f2Seriori;
    }

    public void setF2Seriori(String f2Seriori) {
        this.f2Seriori = f2Seriori;
    }

    public String getF2Tipo() {
        return f2Tipo;
    }

    public void setF2Tipo(String f2Tipo) {
        this.f2Tipo = f2Tipo;
    }

    public String getF2Especi1() {
        return f2Especi1;
    }

    public void setF2Especi1(String f2Especi1) {
        this.f2Especi1 = f2Especi1;
    }

    public String getF2Especi2() {
        return f2Especi2;
    }

    public void setF2Especi2(String f2Especi2) {
        this.f2Especi2 = f2Especi2;
    }

    public String getF2Especi3() {
        return f2Especi3;
    }

    public void setF2Especi3(String f2Especi3) {
        this.f2Especi3 = f2Especi3;
    }

    public String getF2Especi4() {
        return f2Especi4;
    }

    public void setF2Especi4(String f2Especi4) {
        this.f2Especi4 = f2Especi4;
    }

    public double getF2Volume1() {
        return f2Volume1;
    }

    public void setF2Volume1(double f2Volume1) {
        this.f2Volume1 = f2Volume1;
    }

    public double getF2Volume2() {
        return f2Volume2;
    }

    public void setF2Volume2(double f2Volume2) {
        this.f2Volume2 = f2Volume2;
    }

    public double getF2Volume3() {
        return f2Volume3;
    }

    public void setF2Volume3(double f2Volume3) {
        this.f2Volume3 = f2Volume3;
    }

    public double getF2Volume4() {
        return f2Volume4;
    }

    public void setF2Volume4(double f2Volume4) {
        this.f2Volume4 = f2Volume4;
    }

    public double getF2Icmsret() {
        return f2Icmsret;
    }

    public void setF2Icmsret(double f2Icmsret) {
        this.f2Icmsret = f2Icmsret;
    }

    public double getF2Pliqui() {
        return f2Pliqui;
    }

    public void setF2Pliqui(double f2Pliqui) {
        this.f2Pliqui = f2Pliqui;
    }

    public double getF2Pbruto() {
        return f2Pbruto;
    }

    public void setF2Pbruto(double f2Pbruto) {
        this.f2Pbruto = f2Pbruto;
    }

    public String getF2Transp() {
        return f2Transp;
    }

    public void setF2Transp(String f2Transp) {
        this.f2Transp = f2Transp;
    }

    public String getF2Redesp() {
        return f2Redesp;
    }

    public void setF2Redesp(String f2Redesp) {
        this.f2Redesp = f2Redesp;
    }

    public String getF2Vend1() {
        return f2Vend1;
    }

    public void setF2Vend1(String f2Vend1) {
        this.f2Vend1 = f2Vend1;
    }

    public String getF2Vend2() {
        return f2Vend2;
    }

    public void setF2Vend2(String f2Vend2) {
        this.f2Vend2 = f2Vend2;
    }

    public String getF2Vend3() {
        return f2Vend3;
    }

    public void setF2Vend3(String f2Vend3) {
        this.f2Vend3 = f2Vend3;
    }

    public String getF2Vend4() {
        return f2Vend4;
    }

    public void setF2Vend4(String f2Vend4) {
        this.f2Vend4 = f2Vend4;
    }

    public String getF2Vend5() {
        return f2Vend5;
    }

    public void setF2Vend5(String f2Vend5) {
        this.f2Vend5 = f2Vend5;
    }

    public String getF2Ok() {
        return f2Ok;
    }

    public void setF2Ok(String f2Ok) {
        this.f2Ok = f2Ok;
    }

    public String getF2Fimp() {
        return f2Fimp;
    }

    public void setF2Fimp(String f2Fimp) {
        this.f2Fimp = f2Fimp;
    }

    public String getF2Dtlanc() {
        return f2Dtlanc;
    }

    public void setF2Dtlanc(String f2Dtlanc) {
        this.f2Dtlanc = f2Dtlanc;
    }

    public String getF2Dtreaj() {
        return f2Dtreaj;
    }

    public void setF2Dtreaj(String f2Dtreaj) {
        this.f2Dtreaj = f2Dtreaj;
    }

    public String getF2Reajust() {
        return f2Reajust;
    }

    public void setF2Reajust(String f2Reajust) {
        this.f2Reajust = f2Reajust;
    }

    public String getF2Dtbase0() {
        return f2Dtbase0;
    }

    public void setF2Dtbase0(String f2Dtbase0) {
        this.f2Dtbase0 = f2Dtbase0;
    }

    public double getF2Fatorb0() {
        return f2Fatorb0;
    }

    public void setF2Fatorb0(double f2Fatorb0) {
        this.f2Fatorb0 = f2Fatorb0;
    }

    public String getF2Dtbase1() {
        return f2Dtbase1;
    }

    public void setF2Dtbase1(String f2Dtbase1) {
        this.f2Dtbase1 = f2Dtbase1;
    }

    public double getF2Fatorb1() {
        return f2Fatorb1;
    }

    public void setF2Fatorb1(double f2Fatorb1) {
        this.f2Fatorb1 = f2Fatorb1;
    }

    public double getF2Variac() {
        return f2Variac;
    }

    public void setF2Variac(double f2Variac) {
        this.f2Variac = f2Variac;
    }

    public String getF2Filial() {
        return f2Filial;
    }

    public void setF2Filial(String f2Filial) {
        this.f2Filial = f2Filial;
    }

    public double getF2Baseiss() {
        return f2Baseiss;
    }

    public void setF2Baseiss(double f2Baseiss) {
        this.f2Baseiss = f2Baseiss;
    }

    public double getF2Valiss() {
        return f2Valiss;
    }

    public void setF2Valiss(double f2Valiss) {
        this.f2Valiss = f2Valiss;
    }

    public double getF2Valfat() {
        return f2Valfat;
    }

    public void setF2Valfat(double f2Valfat) {
        this.f2Valfat = f2Valfat;
    }

    public double getF2Contsoc() {
        return f2Contsoc;
    }

    public void setF2Contsoc(double f2Contsoc) {
        this.f2Contsoc = f2Contsoc;
    }

    public double getF2Bricms() {
        return f2Bricms;
    }

    public void setF2Bricms(double f2Bricms) {
        this.f2Bricms = f2Bricms;
    }

    public double getF2Fretaut() {
        return f2Fretaut;
    }

    public void setF2Fretaut(double f2Fretaut) {
        this.f2Fretaut = f2Fretaut;
    }

    public double getF2Icmauto() {
        return f2Icmauto;
    }

    public void setF2Icmauto(double f2Icmauto) {
        this.f2Icmauto = f2Icmauto;
    }

    public double getF2Despesa() {
        return f2Despesa;
    }

    public void setF2Despesa(double f2Despesa) {
        this.f2Despesa = f2Despesa;
    }

    public String getF2Nextser() {
        return f2Nextser;
    }

    public void setF2Nextser(String f2Nextser) {
        this.f2Nextser = f2Nextser;
    }

    public String getF2Nextdoc() {
        return f2Nextdoc;
    }

    public void setF2Nextdoc(String f2Nextdoc) {
        this.f2Nextdoc = f2Nextdoc;
    }

    public String getF2Especie() {
        return f2Especie;
    }

    public void setF2Especie(String f2Especie) {
        this.f2Especie = f2Especie;
    }

    public String getF2Pdv() {
        return f2Pdv;
    }

    public void setF2Pdv(String f2Pdv) {
        this.f2Pdv = f2Pdv;
    }

    public String getF2Mapa() {
        return f2Mapa;
    }

    public void setF2Mapa(String f2Mapa) {
        this.f2Mapa = f2Mapa;
    }

    public String getF2Ecf() {
        return f2Ecf;
    }

    public void setF2Ecf(String f2Ecf) {
        this.f2Ecf = f2Ecf;
    }

    public String getF2Prefixo() {
        return f2Prefixo;
    }

    public void setF2Prefixo(String f2Prefixo) {
        this.f2Prefixo = f2Prefixo;
    }

    public double getF2Basimp1() {
        return f2Basimp1;
    }

    public void setF2Basimp1(double f2Basimp1) {
        this.f2Basimp1 = f2Basimp1;
    }

    public double getF2Basimp2() {
        return f2Basimp2;
    }

    public void setF2Basimp2(double f2Basimp2) {
        this.f2Basimp2 = f2Basimp2;
    }

    public double getF2Basimp3() {
        return f2Basimp3;
    }

    public void setF2Basimp3(double f2Basimp3) {
        this.f2Basimp3 = f2Basimp3;
    }

    public double getF2Basimp4() {
        return f2Basimp4;
    }

    public void setF2Basimp4(double f2Basimp4) {
        this.f2Basimp4 = f2Basimp4;
    }

    public double getF2Basimp5() {
        return f2Basimp5;
    }

    public void setF2Basimp5(double f2Basimp5) {
        this.f2Basimp5 = f2Basimp5;
    }

    public double getF2Basimp6() {
        return f2Basimp6;
    }

    public void setF2Basimp6(double f2Basimp6) {
        this.f2Basimp6 = f2Basimp6;
    }

    public double getF2Valimp1() {
        return f2Valimp1;
    }

    public void setF2Valimp1(double f2Valimp1) {
        this.f2Valimp1 = f2Valimp1;
    }

    public double getF2Valimp2() {
        return f2Valimp2;
    }

    public void setF2Valimp2(double f2Valimp2) {
        this.f2Valimp2 = f2Valimp2;
    }

    public double getF2Valimp3() {
        return f2Valimp3;
    }

    public void setF2Valimp3(double f2Valimp3) {
        this.f2Valimp3 = f2Valimp3;
    }

    public double getF2Valimp4() {
        return f2Valimp4;
    }

    public void setF2Valimp4(double f2Valimp4) {
        this.f2Valimp4 = f2Valimp4;
    }

    public double getF2Valimp5() {
        return f2Valimp5;
    }

    public void setF2Valimp5(double f2Valimp5) {
        this.f2Valimp5 = f2Valimp5;
    }

    public double getF2Valimp6() {
        return f2Valimp6;
    }

    public void setF2Valimp6(double f2Valimp6) {
        this.f2Valimp6 = f2Valimp6;
    }

    public String getF2Ordpago() {
        return f2Ordpago;
    }

    public void setF2Ordpago(String f2Ordpago) {
        this.f2Ordpago = f2Ordpago;
    }

    public String getF2Nfcupom() {
        return f2Nfcupom;
    }

    public void setF2Nfcupom(String f2Nfcupom) {
        this.f2Nfcupom = f2Nfcupom;
    }

    public double getF2Valinss() {
        return f2Valinss;
    }

    public void setF2Valinss(double f2Valinss) {
        this.f2Valinss = f2Valinss;
    }

    public String getF2Hora() {
        return f2Hora;
    }

    public void setF2Hora(String f2Hora) {
        this.f2Hora = f2Hora;
    }

    public double getF2Moeda() {
        return f2Moeda;
    }

    public void setF2Moeda(double f2Moeda) {
        this.f2Moeda = f2Moeda;
    }

    public String getF2Regiao() {
        return f2Regiao;
    }

    public void setF2Regiao(String f2Regiao) {
        this.f2Regiao = f2Regiao;
    }

    public double getF2Valcsll() {
        return f2Valcsll;
    }

    public void setF2Valcsll(double f2Valcsll) {
        this.f2Valcsll = f2Valcsll;
    }

    public double getF2Valcofi() {
        return f2Valcofi;
    }

    public void setF2Valcofi(double f2Valcofi) {
        this.f2Valcofi = f2Valcofi;
    }

    public double getF2Valpis() {
        return f2Valpis;
    }

    public void setF2Valpis(double f2Valpis) {
        this.f2Valpis = f2Valpis;
    }

    public String getF2Lote() {
        return f2Lote;
    }

    public void setF2Lote(String f2Lote) {
        this.f2Lote = f2Lote;
    }

    public double getF2Txmoeda() {
        return f2Txmoeda;
    }

    public void setF2Txmoeda(double f2Txmoeda) {
        this.f2Txmoeda = f2Txmoeda;
    }

    public String getF2Cleok() {
        return f2Cleok;
    }

    public void setF2Cleok(String f2Cleok) {
        this.f2Cleok = f2Cleok;
    }

    public String getF2Chvcle() {
        return f2Chvcle;
    }

    public void setF2Chvcle(String f2Chvcle) {
        this.f2Chvcle = f2Chvcle;
    }

    public String getF2Idcle() {
        return f2Idcle;
    }

    public void setF2Idcle(String f2Idcle) {
        this.f2Idcle = f2Idcle;
    }

    public double getF2Valirrf() {
        return f2Valirrf;
    }

    public void setF2Valirrf(double f2Valirrf) {
        this.f2Valirrf = f2Valirrf;
    }

    public String getF2Carga() {
        return f2Carga;
    }

    public void setF2Carga(String f2Carga) {
        this.f2Carga = f2Carga;
    }

    public String getF2Recfaut() {
        return f2Recfaut;
    }

    public void setF2Recfaut(String f2Recfaut) {
        this.f2Recfaut = f2Recfaut;
    }

    public String getF2Seqcar() {
        return f2Seqcar;
    }

    public void setF2Seqcar(String f2Seqcar) {
        this.f2Seqcar = f2Seqcar;
    }

    public double getF2Baseins() {
        return f2Baseins;
    }

    public void setF2Baseins(double f2Baseins) {
        this.f2Baseins = f2Baseins;
    }

    public String getF2Pedpend() {
        return f2Pedpend;
    }

    public void setF2Pedpend(String f2Pedpend) {
        this.f2Pedpend = f2Pedpend;
    }

    public double getF2Desccab() {
        return f2Desccab;
    }

    public void setF2Desccab(double f2Desccab) {
        this.f2Desccab = f2Desccab;
    }

    public String getF2Dtentr() {
        return f2Dtentr;
    }

    public void setF2Dtentr(String f2Dtentr) {
        this.f2Dtentr = f2Dtentr;
    }

    public String getF2Formul() {
        return f2Formul;
    }

    public void setF2Formul(String f2Formul) {
        this.f2Formul = f2Formul;
    }

    public String getF2Tipodoc() {
        return f2Tipodoc;
    }

    public void setF2Tipodoc(String f2Tipodoc) {
        this.f2Tipodoc = f2Tipodoc;
    }

    public String getF2Nfeacrs() {
        return f2Nfeacrs;
    }

    public void setF2Nfeacrs(String f2Nfeacrs) {
        this.f2Nfeacrs = f2Nfeacrs;
    }

    public String getF2Tiporem() {
        return f2Tiporem;
    }

    public void setF2Tiporem(String f2Tiporem) {
        this.f2Tiporem = f2Tiporem;
    }

    public String getF2Seqent() {
        return f2Seqent;
    }

    public void setF2Seqent(String f2Seqent) {
        this.f2Seqent = f2Seqent;
    }

    public double getF2Icmsdif() {
        return f2Icmsdif;
    }

    public void setF2Icmsdif(double f2Icmsdif) {
        this.f2Icmsdif = f2Icmsdif;
    }

    public double getF2Valacrs() {
        return f2Valacrs;
    }

    public void setF2Valacrs(double f2Valacrs) {
        this.f2Valacrs = f2Valacrs;
    }

    public String getF2Reciss() {
        return f2Reciss;
    }

    public void setF2Reciss(String f2Reciss) {
        this.f2Reciss = f2Reciss;
    }

    public String getF2Tiporet() {
        return f2Tiporet;
    }

    public void setF2Tiporet(String f2Tiporet) {
        this.f2Tiporet = f2Tiporet;
    }

    public double getF2Valcf3() {
        return f2Valcf3;
    }

    public void setF2Valcf3(double f2Valcf3) {
        this.f2Valcf3 = f2Valcf3;
    }

    public double getF2Basecf3() {
        return f2Basecf3;
    }

    public void setF2Basecf3(double f2Basecf3) {
        this.f2Basecf3 = f2Basecf3;
    }

    public double getF2Valps3() {
        return f2Valps3;
    }

    public void setF2Valps3(double f2Valps3) {
        this.f2Valps3 = f2Valps3;
    }

    public double getF2Baseps3() {
        return f2Baseps3;
    }

    public void setF2Baseps3(double f2Baseps3) {
        this.f2Baseps3 = f2Baseps3;
    }

    public double getF2Baseirr() {
        return f2Baseirr;
    }

    public void setF2Baseirr(double f2Baseirr) {
        this.f2Baseirr = f2Baseirr;
    }

    public double getF2Estcred() {
        return f2Estcred;
    }

    public void setF2Estcred(double f2Estcred) {
        this.f2Estcred = f2Estcred;
    }

    public double getF2Valtst() {
        return f2Valtst;
    }

    public void setF2Valtst(double f2Valtst) {
        this.f2Valtst = f2Valtst;
    }

    public String getF2Fildest() {
        return f2Fildest;
    }

    public void setF2Fildest(String f2Fildest) {
        this.f2Fildest = f2Fildest;
    }

    public String getF2Eminfe() {
        return f2Eminfe;
    }

    public void setF2Eminfe(String f2Eminfe) {
        this.f2Eminfe = f2Eminfe;
    }

    public double getF2Baspis() {
        return f2Baspis;
    }

    public void setF2Baspis(double f2Baspis) {
        this.f2Baspis = f2Baspis;
    }

    public String getF2Cgccli() {
        return f2Cgccli;
    }

    public void setF2Cgccli(String f2Cgccli) {
        this.f2Cgccli = f2Cgccli;
    }

    public String getF2Flagdev() {
        return f2Flagdev;
    }

    public void setF2Flagdev(String f2Flagdev) {
        this.f2Flagdev = f2Flagdev;
    }

    public String getF2Diactb() {
        return f2Diactb;
    }

    public void setF2Diactb(String f2Diactb) {
        this.f2Diactb = f2Diactb;
    }

    public String getF2Nodia() {
        return f2Nodia;
    }

    public void setF2Nodia(String f2Nodia) {
        this.f2Nodia = f2Nodia;
    }

    public String getF2Hawb() {
        return f2Hawb;
    }

    public void setF2Hawb(String f2Hawb) {
        this.f2Hawb = f2Hawb;
    }

    public String getF2Veicul2() {
        return f2Veicul2;
    }

    public void setF2Veicul2(String f2Veicul2) {
        this.f2Veicul2 = f2Veicul2;
    }

    public String getF2Veicul1() {
        return f2Veicul1;
    }

    public void setF2Veicul1(String f2Veicul1) {
        this.f2Veicul1 = f2Veicul1;
    }

    public String getF2Ordsep() {
        return f2Ordsep;
    }

    public void setF2Ordsep(String f2Ordsep) {
        this.f2Ordsep = f2Ordsep;
    }

    public String getF2Tpfrete() {
        return f2Tpfrete;
    }

    public void setF2Tpfrete(String f2Tpfrete) {
        this.f2Tpfrete = f2Tpfrete;
    }

    public double getF2Crednfe() {
        return f2Crednfe;
    }

    public void setF2Crednfe(double f2Crednfe) {
        this.f2Crednfe = f2Crednfe;
    }

    public String getF2Chvnfe() {
        return f2Chvnfe;
    }

    public void setF2Chvnfe(String f2Chvnfe) {
        this.f2Chvnfe = f2Chvnfe;
    }

    public String getF2Hornfe() {
        return f2Hornfe;
    }

    public void setF2Hornfe(String f2Hornfe) {
        this.f2Hornfe = f2Hornfe;
    }

    public String getF2Codnfe() {
        return f2Codnfe;
    }

    public void setF2Codnfe(String f2Codnfe) {
        this.f2Codnfe = f2Codnfe;
    }

    public String getF2Recopi() {
        return f2Recopi;
    }

    public void setF2Recopi(String f2Recopi) {
        this.f2Recopi = f2Recopi;
    }

    public double getF2Bsrein() {
        return f2Bsrein;
    }

    public void setF2Bsrein(double f2Bsrein) {
        this.f2Bsrein = f2Bsrein;
    }

    public String getF2Formdes() {
        return f2Formdes;
    }

    public void setF2Formdes(String f2Formdes) {
        this.f2Formdes = f2Formdes;
    }

    public String getF2Numorc() {
        return f2Numorc;
    }

    public void setF2Numorc(String f2Numorc) {
        this.f2Numorc = f2Numorc;
    }

    public String getF2Nfeletr() {
        return f2Nfeletr;
    }

    public void setF2Nfeletr(String f2Nfeletr) {
        this.f2Nfeletr = f2Nfeletr;
    }

    public double getF2Baseina() {
        return f2Baseina;
    }

    public void setF2Baseina(double f2Baseina) {
        this.f2Baseina = f2Baseina;
    }

    public double getF2Valfac() {
        return f2Valfac;
    }

    public void setF2Valfac(double f2Valfac) {
        this.f2Valfac = f2Valfac;
    }

    public double getF2Valfet() {
        return f2Valfet;
    }

    public void setF2Valfet(double f2Valfet) {
        this.f2Valfet = f2Valfet;
    }

    public double getF2Valina() {
        return f2Valina;
    }

    public void setF2Valina(double f2Valina) {
        this.f2Valina = f2Valina;
    }

    public String getF2Idsa1() {
        return f2Idsa1;
    }

    public void setF2Idsa1(String f2Idsa1) {
        this.f2Idsa1 = f2Idsa1;
    }

    public String getF2Idsa2() {
        return f2Idsa2;
    }

    public void setF2Idsa2(String f2Idsa2) {
        this.f2Idsa2 = f2Idsa2;
    }

    public String getF2Idsed() {
        return f2Idsed;
    }

    public void setF2Idsed(String f2Idsed) {
        this.f2Idsed = f2Idsed;
    }

    public String getF2Dtdigit() {
        return f2Dtdigit;
    }

    public void setF2Dtdigit(String f2Dtdigit) {
        this.f2Dtdigit = f2Dtdigit;
    }

    public String getF2Prefori() {
        return f2Prefori;
    }

    public void setF2Prefori(String f2Prefori) {
        this.f2Prefori = f2Prefori;
    }

    public double getF2Bascofi() {
        return f2Bascofi;
    }

    public void setF2Bascofi(double f2Bascofi) {
        this.f2Bascofi = f2Bascofi;
    }

    public double getF2Bascsll() {
        return f2Bascsll;
    }

    public void setF2Bascsll(double f2Bascsll) {
        this.f2Bascsll = f2Bascsll;
    }

    public double getF2VlrFrt() {
        return f2VlrFrt;
    }

    public void setF2VlrFrt(double f2VlrFrt) {
        this.f2VlrFrt = f2VlrFrt;
    }

    public double getF2Valfds() {
        return f2Valfds;
    }

    public void setF2Valfds(double f2Valfds) {
        this.f2Valfds = f2Valfds;
    }

    public String getF2Lojent() {
        return f2Lojent;
    }

    public void setF2Lojent(String f2Lojent) {
        this.f2Lojent = f2Lojent;
    }

    public String getF2Client() {
        return f2Client;
    }

    public void setF2Client(String f2Client) {
        this.f2Client = f2Client;
    }

    public double getF2Basetst() {
        return f2Basetst;
    }

    public void setF2Basetst(double f2Basetst) {
        this.f2Basetst = f2Basetst;
    }

    public String getF2Nficmst() {
        return f2Nficmst;
    }

    public void setF2Nficmst(String f2Nficmst) {
        this.f2Nficmst = f2Nficmst;
    }

    public double getF2Refmoed() {
        return f2Refmoed;
    }

    public void setF2Refmoed(double f2Refmoed) {
        this.f2Refmoed = f2Refmoed;
    }

    public double getF2Reftaxa() {
        return f2Reftaxa;
    }

    public void setF2Reftaxa(double f2Reftaxa) {
        this.f2Reftaxa = f2Reftaxa;
    }

    public String getF2Veicul3() {
        return f2Veicul3;
    }

    public void setF2Veicul3(String f2Veicul3) {
        this.f2Veicul3 = f2Veicul3;
    }

    public double getF2Basefun() {
        return f2Basefun;
    }

    public void setF2Basefun(double f2Basefun) {
        this.f2Basefun = f2Basefun;
    }

    public double getF2Vlsenar() {
        return f2Vlsenar;
    }

    public void setF2Vlsenar(double f2Vlsenar) {
        this.f2Vlsenar = f2Vlsenar;
    }

    public String getF2Lojades() {
        return f2Lojades;
    }

    public void setF2Lojades(String f2Lojades) {
        this.f2Lojades = f2Lojades;
    }

    public double getF2Vreint() {
        return f2Vreint;
    }

    public void setF2Vreint(double f2Vreint) {
        this.f2Vreint = f2Vreint;
    }

    public String getF2Fordes() {
        return f2Fordes;
    }

    public void setF2Fordes(String f2Fordes) {
        this.f2Fordes = f2Fordes;
    }

    public String getF2Dttxref() {
        return f2Dttxref;
    }

    public void setF2Dttxref(String f2Dttxref) {
        this.f2Dttxref = f2Dttxref;
    }

    public double getF2Txref() {
        return f2Txref;
    }

    public void setF2Txref(double f2Txref) {
        this.f2Txref = f2Txref;
    }

    public String getF2Ntfecp() {
        return f2Ntfecp;
    }

    public void setF2Ntfecp(String f2Ntfecp) {
        this.f2Ntfecp = f2Ntfecp;
    }

    public String getF2Sersubs() {
        return f2Sersubs;
    }

    public void setF2Sersubs(String f2Sersubs) {
        this.f2Sersubs = f2Sersubs;
    }

    public String getF2Nfsubst() {
        return f2Nfsubst;
    }

    public void setF2Nfsubst(String f2Nfsubst) {
        this.f2Nfsubst = f2Nfsubst;
    }

    public double getF2Desczfr() {
        return f2Desczfr;
    }

    public void setF2Desczfr(double f2Desczfr) {
        this.f2Desczfr = f2Desczfr;
    }

    public double getF2Valfab() {
        return f2Valfab;
    }

    public void setF2Valfab(double f2Valfab) {
        this.f2Valfab = f2Valfab;
    }

    public String getF2Codrgs() {
        return f2Codrgs;
    }

    public void setF2Codrgs(String f2Codrgs) {
        this.f2Codrgs = f2Codrgs;
    }

    public String getF2Dautnfe() {
        return f2Dautnfe;
    }

    public void setF2Dautnfe(String f2Dautnfe) {
        this.f2Dautnfe = f2Dautnfe;
    }

    public String getF2Evenflg() {
        return f2Evenflg;
    }

    public void setF2Evenflg(String f2Evenflg) {
        this.f2Evenflg = f2Evenflg;
    }

    public String getF2Flagrgs() {
        return f2Flagrgs;
    }

    public void setF2Flagrgs(String f2Flagrgs) {
        this.f2Flagrgs = f2Flagrgs;
    }

    public String getF2Hautnfe() {
        return f2Hautnfe;
    }

    public void setF2Hautnfe(String f2Hautnfe) {
        this.f2Hautnfe = f2Hautnfe;
    }

    public String getF2Idcce() {
        return f2Idcce;
    }

    public void setF2Idcce(String f2Idcce) {
        this.f2Idcce = f2Idcce;
    }

    public String getF2Idrgs() {
        return f2Idrgs;
    }

    public void setF2Idrgs(String f2Idrgs) {
        this.f2Idrgs = f2Idrgs;
    }

    public String getF2Tpnfexp() {
        return f2Tpnfexp;
    }

    public void setF2Tpnfexp(String f2Tpnfexp) {
        this.f2Tpnfexp = f2Tpnfexp;
    }

    public double getF2Totimp() {
        return f2Totimp;
    }

    public void setF2Totimp(double f2Totimp) {
        this.f2Totimp = f2Totimp;
    }

    public String getF2Tipimp() {
        return f2Tipimp;
    }

    public void setF2Tipimp(String f2Tipimp) {
        this.f2Tipimp = f2Tipimp;
    }

    public String getF2Sermdf() {
        return f2Sermdf;
    }

    public void setF2Sermdf(String f2Sermdf) {
        this.f2Sermdf = f2Sermdf;
    }

    public String getF2Nummdf() {
        return f2Nummdf;
    }

    public void setF2Nummdf(String f2Nummdf) {
        this.f2Nummdf = f2Nummdf;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getdELET() {
        return dELET;
    }

    public void setdELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getrECNO() {
        return rECNO;
    }

    public void setrECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getrECDEL() {
        return rECDEL;
    }

    public void setrECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public List<Sd2010> getSd2010s() {
        return sd2010s;
    }

    public void setSd2010s(List<Sd2010> sd2010s) {
        this.sd2010s = sd2010s;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sf2010)) {
            return false;
        }
        Sf2010 other = (Sf2010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Sf2010[ rECNO=" + rECNO + " ]";
    }
}
