/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Robson
 */
@Entity
@Table(name = "SD3010")
public class Sd3010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "D3_FILIAL")
    private String d3Filial;
    @Basic(optional = false)
    @Column(name = "D3_TM")
    private String d3Tm;
    @Basic(optional = false)
    @Column(name = "D3_COD")
    private String d3Cod;
    @Basic(optional = false)
    @Column(name = "D3_UM")
    private String d3Um;
    @Basic(optional = false)
    @Column(name = "D3_QUANT")
    private double d3Quant;
    @Basic(optional = false)
    @Column(name = "D3_CF")
    private String d3Cf;
    @Basic(optional = false)
    @Column(name = "D3_CONTA")
    private String d3Conta;
    @Basic(optional = false)
    @Column(name = "D3_OP")
    private String d3Op;
    @Basic(optional = false)
    @Column(name = "D3_LOCAL")
    private String d3Local;
    @Basic(optional = false)
    @Column(name = "D3_DOC")
    private String d3Doc;
    @Basic(optional = false)
    @Column(name = "D3_EMISSAO")
    private String d3Emissao;
    @Basic(optional = false)
    @Column(name = "D3_GRUPO")
    private String d3Grupo;
    @Basic(optional = false)
    @Column(name = "D3_CUSTO1")
    private double d3Custo1;
    @Basic(optional = false)
    @Column(name = "D3_CUSTO2")
    private double d3Custo2;
    @Basic(optional = false)
    @Column(name = "D3_CUSTO3")
    private double d3Custo3;
    @Basic(optional = false)
    @Column(name = "D3_CUSTO4")
    private double d3Custo4;
    @Basic(optional = false)
    @Column(name = "D3_CUSTO5")
    private double d3Custo5;
    @Basic(optional = false)
    @Column(name = "D3_CC")
    private String d3Cc;
    @Basic(optional = false)
    @Column(name = "D3_PARCTOT")
    private String d3Parctot;
    @Basic(optional = false)
    @Column(name = "D3_ESTORNO")
    private String d3Estorno;
    @Basic(optional = false)
    @Column(name = "D3_NUMSEQ")
    private String d3Numseq;
    @Basic(optional = false)
    @Column(name = "D3_SEGUM")
    private String d3Segum;
    @Basic(optional = false)
    @Column(name = "D3_QTSEGUM")
    private double d3Qtsegum;
    @Basic(optional = false)
    @Column(name = "D3_TIPO")
    private String d3Tipo;
    @Basic(optional = false)
    @Column(name = "D3_NIVEL")
    private String d3Nivel;
    @Basic(optional = false)
    @Column(name = "D3_USUARIO")
    private String d3Usuario;
    @Basic(optional = false)
    @Column(name = "D3_REGWMS")
    private String d3Regwms;
    @Basic(optional = false)
    @Column(name = "D3_PERDA")
    private double d3Perda;
    @Basic(optional = false)
    @Column(name = "D3_DTLANC")
    private String d3Dtlanc;
    @Basic(optional = false)
    @Column(name = "D3_TRT")
    private String d3Trt;
    @Basic(optional = false)
    @Column(name = "D3_CHAVE")
    private String d3Chave;
    @Basic(optional = false)
    @Column(name = "D3_IDENT")
    private String d3Ident;
    @Basic(optional = false)
    @Column(name = "D3_SEQCALC")
    private String d3Seqcalc;
    @Basic(optional = false)
    @Column(name = "D3_RATEIO")
    private double d3Rateio;
    @Basic(optional = false)
    @Column(name = "D3_LOTECTL")
    private String d3Lotectl;
    @Basic(optional = false)
    @Column(name = "D3_NUMLOTE")
    private String d3Numlote;
    @Basic(optional = false)
    @Column(name = "D3_DTVALID")
    private String d3Dtvalid;
    @Basic(optional = false)
    @Column(name = "D3_LOCALIZ")
    private String d3Localiz;
    @Basic(optional = false)
    @Column(name = "D3_NUMSERI")
    private String d3Numseri;
    @Basic(optional = false)
    @Column(name = "D3_CUSFF1")
    private double d3Cusff1;
    @Basic(optional = false)
    @Column(name = "D3_CUSFF2")
    private double d3Cusff2;
    @Basic(optional = false)
    @Column(name = "D3_CUSFF3")
    private double d3Cusff3;
    @Basic(optional = false)
    @Column(name = "D3_CUSFF4")
    private double d3Cusff4;
    @Basic(optional = false)
    @Column(name = "D3_CUSFF5")
    private double d3Cusff5;
    @Basic(optional = false)
    @Column(name = "D3_ITEM")
    private String d3Item;
    @Basic(optional = false)
    @Column(name = "D3_OK")
    private String d3Ok;
    @Basic(optional = false)
    @Column(name = "D3_ITEMCTA")
    private String d3Itemcta;
    @Basic(optional = false)
    @Column(name = "D3_CLVL")
    private String d3Clvl;
    @Basic(optional = false)
    @Column(name = "D3_PROJPMS")
    private String d3Projpms;
    @Basic(optional = false)
    @Column(name = "D3_TASKPMS")
    private String d3Taskpms;
    @Basic(optional = false)
    @Column(name = "D3_ORDEM")
    private String d3Ordem;
    @Basic(optional = false)
    @Column(name = "D3_SERVIC")
    private String d3Servic;
    @Basic(optional = false)
    @Column(name = "D3_STSERV")
    private String d3Stserv;
    @Basic(optional = false)
    @Column(name = "D3_OSTEC")
    private String d3Ostec;
    @Basic(optional = false)
    @Column(name = "D3_POTENCI")
    private double d3Potenci;
    @Basic(optional = false)
    @Column(name = "D3_TPESTR")
    private String d3Tpestr;
    @Basic(optional = false)
    @Column(name = "D3_REGATEN")
    private String d3Regaten;
    @Basic(optional = false)
    @Column(name = "D3_ITEMSWN")
    private String d3Itemswn;
    @Basic(optional = false)
    @Column(name = "D3_DOCSWN")
    private String d3Docswn;
    @Basic(optional = false)
    @Column(name = "D3_ITEMGRD")
    private String d3Itemgrd;
    @Basic(optional = false)
    @Column(name = "D3_STATUS")
    private String d3Status;
    @Basic(optional = false)
    @Column(name = "D3_CUSRP1")
    private double d3Cusrp1;
    @Basic(optional = false)
    @Column(name = "D3_CUSRP2")
    private double d3Cusrp2;
    @Basic(optional = false)
    @Column(name = "D3_CUSRP3")
    private double d3Cusrp3;
    @Basic(optional = false)
    @Column(name = "D3_CUSRP4")
    private double d3Cusrp4;
    @Basic(optional = false)
    @Column(name = "D3_CUSRP5")
    private double d3Cusrp5;
    @Basic(optional = false)
    @Column(name = "D3_CMRP")
    private double d3Cmrp;
    @Basic(optional = false)
    @Column(name = "D3_MOEDRP")
    private String d3Moedrp;
    @Basic(optional = false)
    @Column(name = "D3_REVISAO")
    private String d3Revisao;
    @Basic(optional = false)
    @Column(name = "D3_MOEDA")
    private String d3Moeda;
    @Basic(optional = false)
    @Column(name = "D3_CMFIXO")
    private double d3Cmfixo;
    @Basic(optional = false)
    @Column(name = "D3_PMACNUT")
    private double d3Pmacnut;
    @Basic(optional = false)
    @Column(name = "D3_PMICNUT")
    private double d3Pmicnut;
    @Basic(optional = false)
    @Column(name = "D3_EMPOP")
    private String d3Empop;
    @Basic(optional = false)
    @Column(name = "D3_GARANTI")
    private String d3Garanti;
    @Basic(optional = false)
    @Column(name = "D3_DIACTB")
    private String d3Diactb;
    @Basic(optional = false)
    @Column(name = "D3_NODIA")
    private String d3Nodia;
    @Basic(optional = false)
    @Column(name = "D3_NRBPIMS")
    private String d3Nrbpims;
    @Basic(optional = false)
    @Column(name = "D3_QTGANHO")
    private double d3Qtganho;
    @Basic(optional = false)
    @Column(name = "D3_QTMAIOR")
    private double d3Qtmaior;
    @Basic(optional = false)
    @Column(name = "D3_CHAVEF1")
    private String d3Chavef1;
    @Basic(optional = false)
    @Column(name = "D3_CODLAN")
    private String d3Codlan;
    @Basic(optional = false)
    @Column(name = "D3_OKISS")
    private String d3Okiss;
    @Basic(optional = false)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "D3_PERIMP")
    private double d3Perimp;
    @Basic(optional = false)
    @Column(name = "D3_VLRVI")
    private double d3Vlrvi;
    @Basic(optional = false)
    @Column(name = "D3_VLRPD")
    private double d3Vlrpd;
    @Basic(optional = false)
    @Column(name = "D3_USOLICI")
    private String d3Usolici;
    @Basic(optional = false)
    @Column(name = "D3_TANQUE")
    private String d3Tanque;
    @Basic(optional = false)
    @Column(name = "D3_MASSA")
    private double d3Massa;
    @Basic(optional = false)
    @Column(name = "D3_TEMPAMO")
    private double d3Tempamo;
    @Basic(optional = false)
    @Column(name = "D3_TEMPTAQ")
    private double d3Temptaq;
    @Basic(optional = false)
    @Column(name = "D3_DENSAMB")
    private double d3Densamb;
    @Basic(optional = false)
    @Column(name = "D3_QTDAMB")
    private double d3Qtdamb;
    @Basic(optional = false)
    @Column(name = "D3_FATCORR")
    private double d3Fatcorr;
    @Basic(optional = false)
    @Column(name = "D3_TPMOVAJ")
    private String d3Tpmovaj;
    @Basic(optional = false)
    @Column(name = "D3_CODFOR")
    private String d3Codfor;
    @Basic(optional = false)
    @Column(name = "D3_LOJAFOR")
    private String d3Lojafor;
    @Basic(optional = false)
    @Column(name = "D3_NFORP")
    private String d3Nforp;
    @Basic(optional = false)
    @Column(name = "D3_OBS")
    private String d3Obs;
    @Basic(optional = false)
    @Column(name = "D3_CHAVEF2")
    private String d3Chavef2;
    @Basic(optional = false)
    @Column(name = "D3_HAWB")
    private String d3Hawb;
    @Basic(optional = false)
    @Column(name = "D3_IDDCF")
    private String d3Iddcf;
    @Basic(optional = false)
    @Column(name = "D3_NUMSA")
    private String d3Numsa;
    @Basic(optional = false)
    @Column(name = "D3_NRABATE")
    private String d3Nrabate;
    @Basic(optional = false)
    @Column(name = "D3_ITEMSA")
    private String d3Itemsa;
    @Basic(optional = false)
    @Column(name = "D3_TEATF")
    private String d3Teatf;
    @Basic(optional = false)
    @Column(name = "D3_OBSERVA")
    private String d3Observa;
    @Basic(optional = false)
    @Column(name = "D3_CODSAF")
    private String d3Codsaf;
    @Basic(optional = false)
    @Column(name = "D3_FORNDOC")
    private String d3Forndoc;
    @Basic(optional = false)
    @Column(name = "D3_LOJADOC")
    private String d3Lojadoc;
    @Basic(optional = false)
    @Column(name = "D3_PERBLK")
    private String d3Perblk;
    @Basic(optional = false)
    @Column(name = "D3_FATHER")
    private String d3Father;

    public Sd3010() {
    }

    public Sd3010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Sd3010(Integer rECNO, String d3Filial, String d3Tm, String d3Cod, String d3Um, double d3Quant, String d3Cf, String d3Conta, String d3Op, String d3Local, String d3Doc, String d3Emissao, String d3Grupo, double d3Custo1, double d3Custo2, double d3Custo3, double d3Custo4, double d3Custo5, String d3Cc, String d3Parctot, String d3Estorno, String d3Numseq, String d3Segum, double d3Qtsegum, String d3Tipo, String d3Nivel, String d3Usuario, String d3Regwms, double d3Perda, String d3Dtlanc, String d3Trt, String d3Chave, String d3Ident, String d3Seqcalc, double d3Rateio, String d3Lotectl, String d3Numlote, String d3Dtvalid, String d3Localiz, String d3Numseri, double d3Cusff1, double d3Cusff2, double d3Cusff3, double d3Cusff4, double d3Cusff5, String d3Item, String d3Ok, String d3Itemcta, String d3Clvl, String d3Projpms, String d3Taskpms, String d3Ordem, String d3Servic, String d3Stserv, String d3Ostec, double d3Potenci, String d3Tpestr, String d3Regaten, String d3Itemswn, String d3Docswn, String d3Itemgrd, String d3Status, double d3Cusrp1, double d3Cusrp2, double d3Cusrp3, double d3Cusrp4, double d3Cusrp5, double d3Cmrp, String d3Moedrp, String d3Revisao, String d3Moeda, double d3Cmfixo, double d3Pmacnut, double d3Pmicnut, String d3Empop, String d3Garanti, String d3Diactb, String d3Nodia, String d3Nrbpims, double d3Qtganho, double d3Qtmaior, String d3Chavef1, String d3Codlan, String d3Okiss, String dELET, double d3Perimp, double d3Vlrvi, double d3Vlrpd, String d3Usolici, String d3Tanque, double d3Massa, double d3Tempamo, double d3Temptaq, double d3Densamb, double d3Qtdamb, double d3Fatcorr, String d3Tpmovaj, String d3Codfor, String d3Lojafor, String d3Nforp, String d3Obs, String d3Chavef2, String d3Hawb, String d3Iddcf, String d3Numsa, String d3Nrabate, String d3Itemsa, String d3Teatf, String d3Observa, String d3Codsaf, String d3Forndoc, String d3Lojadoc, String d3Perblk, String d3Father) {
        this.rECNO = rECNO;
        this.d3Filial = d3Filial;
        this.d3Tm = d3Tm;
        this.d3Cod = d3Cod;
        this.d3Um = d3Um;
        this.d3Quant = d3Quant;
        this.d3Cf = d3Cf;
        this.d3Conta = d3Conta;
        this.d3Op = d3Op;
        this.d3Local = d3Local;
        this.d3Doc = d3Doc;
        this.d3Emissao = d3Emissao;
        this.d3Grupo = d3Grupo;
        this.d3Custo1 = d3Custo1;
        this.d3Custo2 = d3Custo2;
        this.d3Custo3 = d3Custo3;
        this.d3Custo4 = d3Custo4;
        this.d3Custo5 = d3Custo5;
        this.d3Cc = d3Cc;
        this.d3Parctot = d3Parctot;
        this.d3Estorno = d3Estorno;
        this.d3Numseq = d3Numseq;
        this.d3Segum = d3Segum;
        this.d3Qtsegum = d3Qtsegum;
        this.d3Tipo = d3Tipo;
        this.d3Nivel = d3Nivel;
        this.d3Usuario = d3Usuario;
        this.d3Regwms = d3Regwms;
        this.d3Perda = d3Perda;
        this.d3Dtlanc = d3Dtlanc;
        this.d3Trt = d3Trt;
        this.d3Chave = d3Chave;
        this.d3Ident = d3Ident;
        this.d3Seqcalc = d3Seqcalc;
        this.d3Rateio = d3Rateio;
        this.d3Lotectl = d3Lotectl;
        this.d3Numlote = d3Numlote;
        this.d3Dtvalid = d3Dtvalid;
        this.d3Localiz = d3Localiz;
        this.d3Numseri = d3Numseri;
        this.d3Cusff1 = d3Cusff1;
        this.d3Cusff2 = d3Cusff2;
        this.d3Cusff3 = d3Cusff3;
        this.d3Cusff4 = d3Cusff4;
        this.d3Cusff5 = d3Cusff5;
        this.d3Item = d3Item;
        this.d3Ok = d3Ok;
        this.d3Itemcta = d3Itemcta;
        this.d3Clvl = d3Clvl;
        this.d3Projpms = d3Projpms;
        this.d3Taskpms = d3Taskpms;
        this.d3Ordem = d3Ordem;
        this.d3Servic = d3Servic;
        this.d3Stserv = d3Stserv;
        this.d3Ostec = d3Ostec;
        this.d3Potenci = d3Potenci;
        this.d3Tpestr = d3Tpestr;
        this.d3Regaten = d3Regaten;
        this.d3Itemswn = d3Itemswn;
        this.d3Docswn = d3Docswn;
        this.d3Itemgrd = d3Itemgrd;
        this.d3Status = d3Status;
        this.d3Cusrp1 = d3Cusrp1;
        this.d3Cusrp2 = d3Cusrp2;
        this.d3Cusrp3 = d3Cusrp3;
        this.d3Cusrp4 = d3Cusrp4;
        this.d3Cusrp5 = d3Cusrp5;
        this.d3Cmrp = d3Cmrp;
        this.d3Moedrp = d3Moedrp;
        this.d3Revisao = d3Revisao;
        this.d3Moeda = d3Moeda;
        this.d3Cmfixo = d3Cmfixo;
        this.d3Pmacnut = d3Pmacnut;
        this.d3Pmicnut = d3Pmicnut;
        this.d3Empop = d3Empop;
        this.d3Garanti = d3Garanti;
        this.d3Diactb = d3Diactb;
        this.d3Nodia = d3Nodia;
        this.d3Nrbpims = d3Nrbpims;
        this.d3Qtganho = d3Qtganho;
        this.d3Qtmaior = d3Qtmaior;
        this.d3Chavef1 = d3Chavef1;
        this.d3Codlan = d3Codlan;
        this.d3Okiss = d3Okiss;
        this.dELET = dELET;
        this.d3Perimp = d3Perimp;
        this.d3Vlrvi = d3Vlrvi;
        this.d3Vlrpd = d3Vlrpd;
        this.d3Usolici = d3Usolici;
        this.d3Tanque = d3Tanque;
        this.d3Massa = d3Massa;
        this.d3Tempamo = d3Tempamo;
        this.d3Temptaq = d3Temptaq;
        this.d3Densamb = d3Densamb;
        this.d3Qtdamb = d3Qtdamb;
        this.d3Fatcorr = d3Fatcorr;
        this.d3Tpmovaj = d3Tpmovaj;
        this.d3Codfor = d3Codfor;
        this.d3Lojafor = d3Lojafor;
        this.d3Nforp = d3Nforp;
        this.d3Obs = d3Obs;
        this.d3Chavef2 = d3Chavef2;
        this.d3Hawb = d3Hawb;
        this.d3Iddcf = d3Iddcf;
        this.d3Numsa = d3Numsa;
        this.d3Nrabate = d3Nrabate;
        this.d3Itemsa = d3Itemsa;
        this.d3Teatf = d3Teatf;
        this.d3Observa = d3Observa;
        this.d3Codsaf = d3Codsaf;
        this.d3Forndoc = d3Forndoc;
        this.d3Lojadoc = d3Lojadoc;
        this.d3Perblk = d3Perblk;
        this.d3Father = d3Father;
    }

    public String getD3Filial() {
        return d3Filial;
    }

    public void setD3Filial(String d3Filial) {
        this.d3Filial = d3Filial;
    }

    public String getD3Tm() {
        return d3Tm;
    }

    public void setD3Tm(String d3Tm) {
        this.d3Tm = d3Tm;
    }

    public String getD3Cod() {
        return d3Cod;
    }

    public void setD3Cod(String d3Cod) {
        this.d3Cod = d3Cod;
    }

    public String getD3Um() {
        return d3Um;
    }

    public void setD3Um(String d3Um) {
        this.d3Um = d3Um;
    }

    public double getD3Quant() {
        return d3Quant;
    }

    public void setD3Quant(double d3Quant) {
        this.d3Quant = d3Quant;
    }

    public String getD3Cf() {
        return d3Cf;
    }

    public void setD3Cf(String d3Cf) {
        this.d3Cf = d3Cf;
    }

    public String getD3Conta() {
        return d3Conta;
    }

    public void setD3Conta(String d3Conta) {
        this.d3Conta = d3Conta;
    }

    public String getD3Op() {
        return d3Op;
    }

    public void setD3Op(String d3Op) {
        this.d3Op = d3Op;
    }

    public String getD3Local() {
        return d3Local;
    }

    public void setD3Local(String d3Local) {
        this.d3Local = d3Local;
    }

    public String getD3Doc() {
        return d3Doc;
    }

    public void setD3Doc(String d3Doc) {
        this.d3Doc = d3Doc;
    }

    public String getD3Emissao() {
        return d3Emissao;
    }

    public void setD3Emissao(String d3Emissao) {
        this.d3Emissao = d3Emissao;
    }

    public String getD3Grupo() {
        return d3Grupo;
    }

    public void setD3Grupo(String d3Grupo) {
        this.d3Grupo = d3Grupo;
    }

    public double getD3Custo1() {
        return d3Custo1;
    }

    public void setD3Custo1(double d3Custo1) {
        this.d3Custo1 = d3Custo1;
    }

    public double getD3Custo2() {
        return d3Custo2;
    }

    public void setD3Custo2(double d3Custo2) {
        this.d3Custo2 = d3Custo2;
    }

    public double getD3Custo3() {
        return d3Custo3;
    }

    public void setD3Custo3(double d3Custo3) {
        this.d3Custo3 = d3Custo3;
    }

    public double getD3Custo4() {
        return d3Custo4;
    }

    public void setD3Custo4(double d3Custo4) {
        this.d3Custo4 = d3Custo4;
    }

    public double getD3Custo5() {
        return d3Custo5;
    }

    public void setD3Custo5(double d3Custo5) {
        this.d3Custo5 = d3Custo5;
    }

    public String getD3Cc() {
        return d3Cc;
    }

    public void setD3Cc(String d3Cc) {
        this.d3Cc = d3Cc;
    }

    public String getD3Parctot() {
        return d3Parctot;
    }

    public void setD3Parctot(String d3Parctot) {
        this.d3Parctot = d3Parctot;
    }

    public String getD3Estorno() {
        return d3Estorno;
    }

    public void setD3Estorno(String d3Estorno) {
        this.d3Estorno = d3Estorno;
    }

    public String getD3Numseq() {
        return d3Numseq;
    }

    public void setD3Numseq(String d3Numseq) {
        this.d3Numseq = d3Numseq;
    }

    public String getD3Segum() {
        return d3Segum;
    }

    public void setD3Segum(String d3Segum) {
        this.d3Segum = d3Segum;
    }

    public double getD3Qtsegum() {
        return d3Qtsegum;
    }

    public void setD3Qtsegum(double d3Qtsegum) {
        this.d3Qtsegum = d3Qtsegum;
    }

    public String getD3Tipo() {
        return d3Tipo;
    }

    public void setD3Tipo(String d3Tipo) {
        this.d3Tipo = d3Tipo;
    }

    public String getD3Nivel() {
        return d3Nivel;
    }

    public void setD3Nivel(String d3Nivel) {
        this.d3Nivel = d3Nivel;
    }

    public String getD3Usuario() {
        return d3Usuario;
    }

    public void setD3Usuario(String d3Usuario) {
        this.d3Usuario = d3Usuario;
    }

    public String getD3Regwms() {
        return d3Regwms;
    }

    public void setD3Regwms(String d3Regwms) {
        this.d3Regwms = d3Regwms;
    }

    public double getD3Perda() {
        return d3Perda;
    }

    public void setD3Perda(double d3Perda) {
        this.d3Perda = d3Perda;
    }

    public String getD3Dtlanc() {
        return d3Dtlanc;
    }

    public void setD3Dtlanc(String d3Dtlanc) {
        this.d3Dtlanc = d3Dtlanc;
    }

    public String getD3Trt() {
        return d3Trt;
    }

    public void setD3Trt(String d3Trt) {
        this.d3Trt = d3Trt;
    }

    public String getD3Chave() {
        return d3Chave;
    }

    public void setD3Chave(String d3Chave) {
        this.d3Chave = d3Chave;
    }

    public String getD3Ident() {
        return d3Ident;
    }

    public void setD3Ident(String d3Ident) {
        this.d3Ident = d3Ident;
    }

    public String getD3Seqcalc() {
        return d3Seqcalc;
    }

    public void setD3Seqcalc(String d3Seqcalc) {
        this.d3Seqcalc = d3Seqcalc;
    }

    public double getD3Rateio() {
        return d3Rateio;
    }

    public void setD3Rateio(double d3Rateio) {
        this.d3Rateio = d3Rateio;
    }

    public String getD3Lotectl() {
        return d3Lotectl;
    }

    public void setD3Lotectl(String d3Lotectl) {
        this.d3Lotectl = d3Lotectl;
    }

    public String getD3Numlote() {
        return d3Numlote;
    }

    public void setD3Numlote(String d3Numlote) {
        this.d3Numlote = d3Numlote;
    }

    public String getD3Dtvalid() {
        return d3Dtvalid;
    }

    public void setD3Dtvalid(String d3Dtvalid) {
        this.d3Dtvalid = d3Dtvalid;
    }

    public String getD3Localiz() {
        return d3Localiz;
    }

    public void setD3Localiz(String d3Localiz) {
        this.d3Localiz = d3Localiz;
    }

    public String getD3Numseri() {
        return d3Numseri;
    }

    public void setD3Numseri(String d3Numseri) {
        this.d3Numseri = d3Numseri;
    }

    public double getD3Cusff1() {
        return d3Cusff1;
    }

    public void setD3Cusff1(double d3Cusff1) {
        this.d3Cusff1 = d3Cusff1;
    }

    public double getD3Cusff2() {
        return d3Cusff2;
    }

    public void setD3Cusff2(double d3Cusff2) {
        this.d3Cusff2 = d3Cusff2;
    }

    public double getD3Cusff3() {
        return d3Cusff3;
    }

    public void setD3Cusff3(double d3Cusff3) {
        this.d3Cusff3 = d3Cusff3;
    }

    public double getD3Cusff4() {
        return d3Cusff4;
    }

    public void setD3Cusff4(double d3Cusff4) {
        this.d3Cusff4 = d3Cusff4;
    }

    public double getD3Cusff5() {
        return d3Cusff5;
    }

    public void setD3Cusff5(double d3Cusff5) {
        this.d3Cusff5 = d3Cusff5;
    }

    public String getD3Item() {
        return d3Item;
    }

    public void setD3Item(String d3Item) {
        this.d3Item = d3Item;
    }

    public String getD3Ok() {
        return d3Ok;
    }

    public void setD3Ok(String d3Ok) {
        this.d3Ok = d3Ok;
    }

    public String getD3Itemcta() {
        return d3Itemcta;
    }

    public void setD3Itemcta(String d3Itemcta) {
        this.d3Itemcta = d3Itemcta;
    }

    public String getD3Clvl() {
        return d3Clvl;
    }

    public void setD3Clvl(String d3Clvl) {
        this.d3Clvl = d3Clvl;
    }

    public String getD3Projpms() {
        return d3Projpms;
    }

    public void setD3Projpms(String d3Projpms) {
        this.d3Projpms = d3Projpms;
    }

    public String getD3Taskpms() {
        return d3Taskpms;
    }

    public void setD3Taskpms(String d3Taskpms) {
        this.d3Taskpms = d3Taskpms;
    }

    public String getD3Ordem() {
        return d3Ordem;
    }

    public void setD3Ordem(String d3Ordem) {
        this.d3Ordem = d3Ordem;
    }

    public String getD3Servic() {
        return d3Servic;
    }

    public void setD3Servic(String d3Servic) {
        this.d3Servic = d3Servic;
    }

    public String getD3Stserv() {
        return d3Stserv;
    }

    public void setD3Stserv(String d3Stserv) {
        this.d3Stserv = d3Stserv;
    }

    public String getD3Ostec() {
        return d3Ostec;
    }

    public void setD3Ostec(String d3Ostec) {
        this.d3Ostec = d3Ostec;
    }

    public double getD3Potenci() {
        return d3Potenci;
    }

    public void setD3Potenci(double d3Potenci) {
        this.d3Potenci = d3Potenci;
    }

    public String getD3Tpestr() {
        return d3Tpestr;
    }

    public void setD3Tpestr(String d3Tpestr) {
        this.d3Tpestr = d3Tpestr;
    }

    public String getD3Regaten() {
        return d3Regaten;
    }

    public void setD3Regaten(String d3Regaten) {
        this.d3Regaten = d3Regaten;
    }

    public String getD3Itemswn() {
        return d3Itemswn;
    }

    public void setD3Itemswn(String d3Itemswn) {
        this.d3Itemswn = d3Itemswn;
    }

    public String getD3Docswn() {
        return d3Docswn;
    }

    public void setD3Docswn(String d3Docswn) {
        this.d3Docswn = d3Docswn;
    }

    public String getD3Itemgrd() {
        return d3Itemgrd;
    }

    public void setD3Itemgrd(String d3Itemgrd) {
        this.d3Itemgrd = d3Itemgrd;
    }

    public String getD3Status() {
        return d3Status;
    }

    public void setD3Status(String d3Status) {
        this.d3Status = d3Status;
    }

    public double getD3Cusrp1() {
        return d3Cusrp1;
    }

    public void setD3Cusrp1(double d3Cusrp1) {
        this.d3Cusrp1 = d3Cusrp1;
    }

    public double getD3Cusrp2() {
        return d3Cusrp2;
    }

    public void setD3Cusrp2(double d3Cusrp2) {
        this.d3Cusrp2 = d3Cusrp2;
    }

    public double getD3Cusrp3() {
        return d3Cusrp3;
    }

    public void setD3Cusrp3(double d3Cusrp3) {
        this.d3Cusrp3 = d3Cusrp3;
    }

    public double getD3Cusrp4() {
        return d3Cusrp4;
    }

    public void setD3Cusrp4(double d3Cusrp4) {
        this.d3Cusrp4 = d3Cusrp4;
    }

    public double getD3Cusrp5() {
        return d3Cusrp5;
    }

    public void setD3Cusrp5(double d3Cusrp5) {
        this.d3Cusrp5 = d3Cusrp5;
    }

    public double getD3Cmrp() {
        return d3Cmrp;
    }

    public void setD3Cmrp(double d3Cmrp) {
        this.d3Cmrp = d3Cmrp;
    }

    public String getD3Moedrp() {
        return d3Moedrp;
    }

    public void setD3Moedrp(String d3Moedrp) {
        this.d3Moedrp = d3Moedrp;
    }

    public String getD3Revisao() {
        return d3Revisao;
    }

    public void setD3Revisao(String d3Revisao) {
        this.d3Revisao = d3Revisao;
    }

    public String getD3Moeda() {
        return d3Moeda;
    }

    public void setD3Moeda(String d3Moeda) {
        this.d3Moeda = d3Moeda;
    }

    public double getD3Cmfixo() {
        return d3Cmfixo;
    }

    public void setD3Cmfixo(double d3Cmfixo) {
        this.d3Cmfixo = d3Cmfixo;
    }

    public double getD3Pmacnut() {
        return d3Pmacnut;
    }

    public void setD3Pmacnut(double d3Pmacnut) {
        this.d3Pmacnut = d3Pmacnut;
    }

    public double getD3Pmicnut() {
        return d3Pmicnut;
    }

    public void setD3Pmicnut(double d3Pmicnut) {
        this.d3Pmicnut = d3Pmicnut;
    }

    public String getD3Empop() {
        return d3Empop;
    }

    public void setD3Empop(String d3Empop) {
        this.d3Empop = d3Empop;
    }

    public String getD3Garanti() {
        return d3Garanti;
    }

    public void setD3Garanti(String d3Garanti) {
        this.d3Garanti = d3Garanti;
    }

    public String getD3Diactb() {
        return d3Diactb;
    }

    public void setD3Diactb(String d3Diactb) {
        this.d3Diactb = d3Diactb;
    }

    public String getD3Nodia() {
        return d3Nodia;
    }

    public void setD3Nodia(String d3Nodia) {
        this.d3Nodia = d3Nodia;
    }

    public String getD3Nrbpims() {
        return d3Nrbpims;
    }

    public void setD3Nrbpims(String d3Nrbpims) {
        this.d3Nrbpims = d3Nrbpims;
    }

    public double getD3Qtganho() {
        return d3Qtganho;
    }

    public void setD3Qtganho(double d3Qtganho) {
        this.d3Qtganho = d3Qtganho;
    }

    public double getD3Qtmaior() {
        return d3Qtmaior;
    }

    public void setD3Qtmaior(double d3Qtmaior) {
        this.d3Qtmaior = d3Qtmaior;
    }

    public String getD3Chavef1() {
        return d3Chavef1;
    }

    public void setD3Chavef1(String d3Chavef1) {
        this.d3Chavef1 = d3Chavef1;
    }

    public String getD3Codlan() {
        return d3Codlan;
    }

    public void setD3Codlan(String d3Codlan) {
        this.d3Codlan = d3Codlan;
    }

    public String getD3Okiss() {
        return d3Okiss;
    }

    public void setD3Okiss(String d3Okiss) {
        this.d3Okiss = d3Okiss;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public double getD3Perimp() {
        return d3Perimp;
    }

    public void setD3Perimp(double d3Perimp) {
        this.d3Perimp = d3Perimp;
    }

    public double getD3Vlrvi() {
        return d3Vlrvi;
    }

    public void setD3Vlrvi(double d3Vlrvi) {
        this.d3Vlrvi = d3Vlrvi;
    }

    public double getD3Vlrpd() {
        return d3Vlrpd;
    }

    public void setD3Vlrpd(double d3Vlrpd) {
        this.d3Vlrpd = d3Vlrpd;
    }

    public String getD3Usolici() {
        return d3Usolici;
    }

    public void setD3Usolici(String d3Usolici) {
        this.d3Usolici = d3Usolici;
    }

    public String getD3Tanque() {
        return d3Tanque;
    }

    public void setD3Tanque(String d3Tanque) {
        this.d3Tanque = d3Tanque;
    }

    public double getD3Massa() {
        return d3Massa;
    }

    public void setD3Massa(double d3Massa) {
        this.d3Massa = d3Massa;
    }

    public double getD3Tempamo() {
        return d3Tempamo;
    }

    public void setD3Tempamo(double d3Tempamo) {
        this.d3Tempamo = d3Tempamo;
    }

    public double getD3Temptaq() {
        return d3Temptaq;
    }

    public void setD3Temptaq(double d3Temptaq) {
        this.d3Temptaq = d3Temptaq;
    }

    public double getD3Densamb() {
        return d3Densamb;
    }

    public void setD3Densamb(double d3Densamb) {
        this.d3Densamb = d3Densamb;
    }

    public double getD3Qtdamb() {
        return d3Qtdamb;
    }

    public void setD3Qtdamb(double d3Qtdamb) {
        this.d3Qtdamb = d3Qtdamb;
    }

    public double getD3Fatcorr() {
        return d3Fatcorr;
    }

    public void setD3Fatcorr(double d3Fatcorr) {
        this.d3Fatcorr = d3Fatcorr;
    }

    public String getD3Tpmovaj() {
        return d3Tpmovaj;
    }

    public void setD3Tpmovaj(String d3Tpmovaj) {
        this.d3Tpmovaj = d3Tpmovaj;
    }

    public String getD3Codfor() {
        return d3Codfor;
    }

    public void setD3Codfor(String d3Codfor) {
        this.d3Codfor = d3Codfor;
    }

    public String getD3Lojafor() {
        return d3Lojafor;
    }

    public void setD3Lojafor(String d3Lojafor) {
        this.d3Lojafor = d3Lojafor;
    }

    public String getD3Nforp() {
        return d3Nforp;
    }

    public void setD3Nforp(String d3Nforp) {
        this.d3Nforp = d3Nforp;
    }

    public String getD3Obs() {
        return d3Obs;
    }

    public void setD3Obs(String d3Obs) {
        this.d3Obs = d3Obs;
    }

    public String getD3Chavef2() {
        return d3Chavef2;
    }

    public void setD3Chavef2(String d3Chavef2) {
        this.d3Chavef2 = d3Chavef2;
    }

    public String getD3Hawb() {
        return d3Hawb;
    }

    public void setD3Hawb(String d3Hawb) {
        this.d3Hawb = d3Hawb;
    }

    public String getD3Iddcf() {
        return d3Iddcf;
    }

    public void setD3Iddcf(String d3Iddcf) {
        this.d3Iddcf = d3Iddcf;
    }

    public String getD3Numsa() {
        return d3Numsa;
    }

    public void setD3Numsa(String d3Numsa) {
        this.d3Numsa = d3Numsa;
    }

    public String getD3Nrabate() {
        return d3Nrabate;
    }

    public void setD3Nrabate(String d3Nrabate) {
        this.d3Nrabate = d3Nrabate;
    }

    public String getD3Itemsa() {
        return d3Itemsa;
    }

    public void setD3Itemsa(String d3Itemsa) {
        this.d3Itemsa = d3Itemsa;
    }

    public String getD3Teatf() {
        return d3Teatf;
    }

    public void setD3Teatf(String d3Teatf) {
        this.d3Teatf = d3Teatf;
    }

    public String getD3Observa() {
        return d3Observa;
    }

    public void setD3Observa(String d3Observa) {
        this.d3Observa = d3Observa;
    }

    public String getD3Codsaf() {
        return d3Codsaf;
    }

    public void setD3Codsaf(String d3Codsaf) {
        this.d3Codsaf = d3Codsaf;
    }

    public String getD3Forndoc() {
        return d3Forndoc;
    }

    public void setD3Forndoc(String d3Forndoc) {
        this.d3Forndoc = d3Forndoc;
    }

    public String getD3Lojadoc() {
        return d3Lojadoc;
    }

    public void setD3Lojadoc(String d3Lojadoc) {
        this.d3Lojadoc = d3Lojadoc;
    }

    public String getD3Perblk() {
        return d3Perblk;
    }

    public void setD3Perblk(String d3Perblk) {
        this.d3Perblk = d3Perblk;
    }

    public String getD3Father() {
        return d3Father;
    }

    public void setD3Father(String d3Father) {
        this.d3Father = d3Father;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sd3010)) {
            return false;
        }
        Sd3010 other = (Sd3010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gerador.Sd3010[ rECNO=" + rECNO + " ]";
    }
    
}
