/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Robson
 */
@Entity
@Table(name = "SYP010")
public class Syp010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "YP_FILIAL")
    private String ypFilial;
    @Basic(optional = false)
    @Column(name = "YP_CHAVE")
    private String ypChave;
    @Basic(optional = false)
    @Column(name = "YP_SEQ")
    private String ypSeq;
    @Basic(optional = false)
    @Column(name = "YP_TEXTO")
    private String ypTexto;
    @Basic(optional = false)
    @Column(name = "YP_CAMPO")
    private String ypCampo;
    @Basic(optional = false)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;

    public Syp010() {
    }

    public Syp010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Syp010(Integer rECNO, String ypFilial, String ypChave, String ypSeq, String ypTexto, String ypCampo, String dELET, int rECDEL) {
        this.rECNO = rECNO;
        this.ypFilial = ypFilial;
        this.ypChave = ypChave;
        this.ypSeq = ypSeq;
        this.ypTexto = ypTexto;
        this.ypCampo = ypCampo;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
    }

    public String getYpFilial() {
        return ypFilial;
    }

    public void setYpFilial(String ypFilial) {
        this.ypFilial = ypFilial;
    }

    public String getYpChave() {
        return ypChave;
    }

    public void setYpChave(String ypChave) {
        this.ypChave = ypChave;
    }

    public String getYpSeq() {
        return ypSeq;
    }

    public void setYpSeq(String ypSeq) {
        this.ypSeq = ypSeq;
    }

    public String getYpTexto() {
        return ypTexto;
    }

    public void setYpTexto(String ypTexto) {
        this.ypTexto = ypTexto;
    }

    public String getYpCampo() {
        return ypCampo;
    }

    public void setYpCampo(String ypCampo) {
        this.ypCampo = ypCampo;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Syp010)) {
            return false;
        }
        Syp010 other = (Syp010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gerador.Syp010[ rECNO=" + rECNO + " ]";
    }
    
}
