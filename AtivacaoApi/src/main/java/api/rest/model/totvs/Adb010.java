/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ROBSON
 */
@Table(schema = "dbo", name = "ADB010")
@Entity
public class Adb010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "ADB_FILIAL")
    private String adbFilial;
    @ManyToOne
    @JoinColumn(name = "ADB_NUMCTR", referencedColumnName = "ADA_NUMCTR")
    private Ada010 Ada010;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ADB_ITEM")
    private String adbItem;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ADB_CODPRO", referencedColumnName = "B1_COD", nullable = false, insertable = false, updatable = false)
    @JsonManagedReference(value = "Sb1Adb")
    private Sb1010 sb1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ADB_DESPRO")
    private String adbDespro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ADB_UM")
    private String adbUm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_QUANT")
    private double adbQuant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_PRCVEN")
    private double adbPrcven;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_TOTAL")
    private double adbTotal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ADB_TES")
    private String adbTes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ADB_UDTINI")
    private String adbUdtini;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ADB_UDTFIM")
    private String adbUdtfim;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ADB_TESCOB")
    private String adbTescob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ADB_LOCAL")
    private String adbLocal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_PRUNIT")
    private double adbPrunit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ADB_SEGUM")
    private String adbSegum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_UNSVEN")
    private double adbUnsven;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_DESC")
    private double adbDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_VALDES")
    private double adbValdes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "ADB_FILENT")
    private String adbFilent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_QTDENT")
    private double adbQtdent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_QTDEMP")
    private double adbQtdemp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADB_PEDCOB")
    private String adbPedcob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "ADB_CODCLI")
    private String adbCodcli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "ADB_LOJCLI")
    private String adbLojcli;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "ADB_CLACOM")
    private String adbClacom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADB_UVEND1")
    private String adbUvend1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_UCOM1")
    private double adbUcom1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADB_UVEND2")
    private String adbUvend2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_UCOM2")
    private double adbUcom2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADB_ULTCOM")
    private String adbUltcom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "R_E_C_D_E_L_")
//    private int rECDEL;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ADB_UMSG")
    private String adbUmsg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ADB_UNUMAT")
    private String adbUnumat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ADB_UITATE")
    private String adbUitate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_UMESIN")
    private int adbUMesin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADB_UMESCO")
    private int adbUMesco;   
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ADB_UIDAGA", referencedColumnName = "AGA_CODIGO", nullable = false, insertable = false, updatable = false)    
    private Aga010 aga010;
    @OneToMany(mappedBy = "adb", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Sz2010> sz2010s;

    public Adb010() {
    }

    public Adb010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAdbFilial() {
        return adbFilial;
    }

    public void setAdbFilial(String adbFilial) {
        this.adbFilial = adbFilial;
    }

    public Ada010 getAda010() {
        return Ada010;
    }

    public void setAda010(Ada010 Ada010) {
        this.Ada010 = Ada010;
    }

    public String getAdbItem() {
        return adbItem;
    }

    public void setAdbItem(String adbItem) {
        this.adbItem = adbItem;
    }

    public Sb1010 getSb1() {
        return sb1;
    }

    public void setSb1(Sb1010 sb1) {
        this.sb1 = sb1;
    }

    public String getAdbDespro() {
        return adbDespro;
    }

    public void setAdbDespro(String adbDespro) {
        this.adbDespro = adbDespro;
    }

    public String getAdbUm() {
        return adbUm;
    }

    public void setAdbUm(String adbUm) {
        this.adbUm = adbUm;
    }

    public double getAdbQuant() {
        return adbQuant;
    }

    public void setAdbQuant(double adbQuant) {
        this.adbQuant = adbQuant;
    }

    public double getAdbPrcven() {
        return adbPrcven;
    }

    public void setAdbPrcven(double adbPrcven) {
        this.adbPrcven = adbPrcven;
    }

    public double getAdbTotal() {
        return adbTotal;
    }

    public void setAdbTotal(double adbTotal) {
        this.adbTotal = adbTotal;
    }

    public String getAdbTes() {
        return adbTes;
    }

    public void setAdbTes(String adbTes) {
        this.adbTes = adbTes;
    }

    public String getAdbUdtini() {
        return adbUdtini;
    }

    public void setAdbUdtini(String adbUdtini) {
        this.adbUdtini = adbUdtini;
    }

    public String getAdbUdtfim() {
        return adbUdtfim;
    }

    public void setAdbUdtfim(String adbUdtfim) {
        this.adbUdtfim = adbUdtfim;
    }

    public String getAdbTescob() {
        return adbTescob;
    }

    public void setAdbTescob(String adbTescob) {
        this.adbTescob = adbTescob;
    }

    public String getAdbLocal() {
        return adbLocal;
    }

    public void setAdbLocal(String adbLocal) {
        this.adbLocal = adbLocal;
    }

    public double getAdbPrunit() {
        return adbPrunit;
    }

    public void setAdbPrunit(double adbPrunit) {
        this.adbPrunit = adbPrunit;
    }

    public String getAdbSegum() {
        return adbSegum;
    }

    public void setAdbSegum(String adbSegum) {
        this.adbSegum = adbSegum;
    }

    public double getAdbUnsven() {
        return adbUnsven;
    }

    public void setAdbUnsven(double adbUnsven) {
        this.adbUnsven = adbUnsven;
    }

    public double getAdbDesc() {
        return adbDesc;
    }

    public void setAdbDesc(double adbDesc) {
        this.adbDesc = adbDesc;
    }

    public double getAdbValdes() {
        return adbValdes;
    }

    public void setAdbValdes(double adbValdes) {
        this.adbValdes = adbValdes;
    }

    public String getAdbFilent() {
        return adbFilent;
    }

    public void setAdbFilent(String adbFilent) {
        this.adbFilent = adbFilent;
    }

    public double getAdbQtdent() {
        return adbQtdent;
    }

    public void setAdbQtdent(double adbQtdent) {
        this.adbQtdent = adbQtdent;
    }

    public double getAdbQtdemp() {
        return adbQtdemp;
    }

    public void setAdbQtdemp(double adbQtdemp) {
        this.adbQtdemp = adbQtdemp;
    }

    public String getAdbPedcob() {
        return adbPedcob;
    }

    public void setAdbPedcob(String adbPedcob) {
        this.adbPedcob = adbPedcob;
    }

    public String getAdbCodcli() {
        return adbCodcli;
    }

    public void setAdbCodcli(String adbCodcli) {
        this.adbCodcli = adbCodcli;
    }

    public String getAdbLojcli() {
        return adbLojcli;
    }

    public void setAdbLojcli(String adbLojcli) {
        this.adbLojcli = adbLojcli;
    }

    public String getAdbClacom() {
        return adbClacom;
    }

    public void setAdbClacom(String adbClacom) {
        this.adbClacom = adbClacom;
    }

    public String getAdbUvend1() {
        return adbUvend1;
    }

    public void setAdbUvend1(String adbUvend1) {
        this.adbUvend1 = adbUvend1;
    }

    public double getAdbUcom1() {
        return adbUcom1;
    }

    public void setAdbUcom1(double adbUcom1) {
        this.adbUcom1 = adbUcom1;
    }

    public String getAdbUvend2() {
        return adbUvend2;
    }

    public void setAdbUvend2(String adbUvend2) {
        this.adbUvend2 = adbUvend2;
    }

    public double getAdbUcom2() {
        return adbUcom2;
    }

    public void setAdbUcom2(double adbUcom2) {
        this.adbUcom2 = adbUcom2;
    }

    public String getAdbUltcom() {
        return adbUltcom;
    }

    public void setAdbUltcom(String adbUltcom) {
        this.adbUltcom = adbUltcom;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public List<Sz2010> getSz2010s() {
        return sz2010s;
    }

    public void setSz2010s(List<Sz2010> sz2010s) {
        this.sz2010s = sz2010s;
    }

//    public int getRECDEL() {
//        return rECDEL;
//    }
//
//    public void setRECDEL(int rECDEL) {
//        this.rECDEL = rECDEL;
//    }
    public String getAdbUmsg() {
        return adbUmsg;
    }

    public void setAdbUmsg(String adbUmsg) {
        this.adbUmsg = adbUmsg;
    }

    public String getAdbUnumat() {
        return adbUnumat;
    }

    public void setAdbUnumat(String adbUnumat) {
        this.adbUnumat = adbUnumat;
    }

    public String getAdbUitate() {
        return adbUitate;
    }

    public void setAdbUitate(String adbUitate) {
        this.adbUitate = adbUitate;
    }

    public int getAdbUMesin() {
        return adbUMesin;
    }

    public void setAdbUMesin(int adbUMesin) {
        this.adbUMesin = adbUMesin;
    }

    public int getAdbUMesco() {
        return adbUMesco;
    }

    public void setAdbUMesco(int adbUMesco) {
        this.adbUMesco = adbUMesco;
    }

    public Aga010 getAga010() {
        return aga010;
    }

    public void setAga010(Aga010 aga010) {
        this.aga010 = aga010;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adb010)) {
            return false;
        }
        Adb010 other = (Adb010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "classes.totvs.Adb010[ rECNO=" + rECNO + " ]";
    }
}
