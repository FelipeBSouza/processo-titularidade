/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Robson
 */
@Entity
@Table(name = "ABC010")
public class Abc010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "ABC_FILIAL")
    private String abcFilial;
    @Basic(optional = false)
    @Column(name = "ABC_NUMOS")
    private String abcNumos;
    @Basic(optional = false)
    @Column(name = "ABC_SUBOS")
    private String abcSubos;
    @Basic(optional = false)
    @Column(name = "ABC_CODTEC")
    private String abcCodtec;
    @Basic(optional = false)
    @Column(name = "ABC_SEQ")
    private String abcSeq;
    @Basic(optional = false)
    @Column(name = "ABC_ITEM")
    private String abcItem;
    @Basic(optional = false)
    @Column(name = "ABC_CODPRO")
    private String abcCodpro;
    @Basic(optional = false)
    @Column(name = "ABC_DESCRI")
    private String abcDescri;
    @Basic(optional = false)
    @Column(name = "ABC_QUANT")
    private double abcQuant;
    @Basic(optional = false)
    @Column(name = "ABC_VLUNIT")
    private double abcVlunit;
    @Basic(optional = false)
    @Column(name = "ABC_VALOR")
    private double abcValor;
    @Basic(optional = false)
    @Column(name = "ABC_CODSER")
    private String abcCodser;
    @Basic(optional = false)
    @Column(name = "ABC_CUSTO")
    private double abcCusto;
    @Basic(optional = false)
    @Column(name = "ABC_PREFIX")
    private String abcPrefix;
    @Basic(optional = false)
    @Column(name = "ABC_NUMERO")
    private String abcNumero;
    @Basic(optional = false)
    @Column(name = "ABC_PARCEL")
    private String abcParcel;
    @Basic(optional = false)
    @Column(name = "ABC_TIPO")
    private String abcTipo;
    @Basic(optional = false)
    @Column(name = "ABC_CODFOR")
    private String abcCodfor;
    @Basic(optional = false)
    @Column(name = "ABC_LOJFOR")
    private String abcLojfor;
    @Basic(optional = false)
    @Column(name = "ABC_CONTRT")
    private String abcContrt;
    @Basic(optional = false)
    @Column(name = "ABC_CODGRP")
    private String abcCodgrp;
    @Basic(optional = false)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @Basic(optional = false)
    @Column(name = "ABC_UARMAZ")
    private String abcUarmaz;

    public Abc010() {
    }

    public Abc010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Abc010(Integer rECNO, String abcFilial, String abcNumos, String abcSubos, String abcCodtec, String abcSeq, String abcItem, String abcCodpro, String abcDescri, double abcQuant, double abcVlunit, double abcValor, String abcCodser, double abcCusto, String abcPrefix, String abcNumero, String abcParcel, String abcTipo, String abcCodfor, String abcLojfor, String abcContrt, String abcCodgrp, String dELET, int rECDEL, String abcUarmaz) {
        this.rECNO = rECNO;
        this.abcFilial = abcFilial;
        this.abcNumos = abcNumos;
        this.abcSubos = abcSubos;
        this.abcCodtec = abcCodtec;
        this.abcSeq = abcSeq;
        this.abcItem = abcItem;
        this.abcCodpro = abcCodpro;
        this.abcDescri = abcDescri;
        this.abcQuant = abcQuant;
        this.abcVlunit = abcVlunit;
        this.abcValor = abcValor;
        this.abcCodser = abcCodser;
        this.abcCusto = abcCusto;
        this.abcPrefix = abcPrefix;
        this.abcNumero = abcNumero;
        this.abcParcel = abcParcel;
        this.abcTipo = abcTipo;
        this.abcCodfor = abcCodfor;
        this.abcLojfor = abcLojfor;
        this.abcContrt = abcContrt;
        this.abcCodgrp = abcCodgrp;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
        this.abcUarmaz = abcUarmaz;
    }

    public String getAbcFilial() {
        return abcFilial;
    }

    public void setAbcFilial(String abcFilial) {
        this.abcFilial = abcFilial;
    }

    public String getAbcNumos() {
        return abcNumos;
    }

    public void setAbcNumos(String abcNumos) {
        this.abcNumos = abcNumos;
    }

    public String getAbcSubos() {
        return abcSubos;
    }

    public void setAbcSubos(String abcSubos) {
        this.abcSubos = abcSubos;
    }

    public String getAbcCodtec() {
        return abcCodtec;
    }

    public void setAbcCodtec(String abcCodtec) {
        this.abcCodtec = abcCodtec;
    }

    public String getAbcSeq() {
        return abcSeq;
    }

    public void setAbcSeq(String abcSeq) {
        this.abcSeq = abcSeq;
    }

    public String getAbcItem() {
        return abcItem;
    }

    public void setAbcItem(String abcItem) {
        this.abcItem = abcItem;
    }

    public String getAbcCodpro() {
        return abcCodpro;
    }

    public void setAbcCodpro(String abcCodpro) {
        this.abcCodpro = abcCodpro;
    }

    public String getAbcDescri() {
        return abcDescri;
    }

    public void setAbcDescri(String abcDescri) {
        this.abcDescri = abcDescri;
    }

    public double getAbcQuant() {
        return abcQuant;
    }

    public void setAbcQuant(double abcQuant) {
        this.abcQuant = abcQuant;
    }

    public double getAbcVlunit() {
        return abcVlunit;
    }

    public void setAbcVlunit(double abcVlunit) {
        this.abcVlunit = abcVlunit;
    }

    public double getAbcValor() {
        return abcValor;
    }

    public void setAbcValor(double abcValor) {
        this.abcValor = abcValor;
    }

    public String getAbcCodser() {
        return abcCodser;
    }

    public void setAbcCodser(String abcCodser) {
        this.abcCodser = abcCodser;
    }

    public double getAbcCusto() {
        return abcCusto;
    }

    public void setAbcCusto(double abcCusto) {
        this.abcCusto = abcCusto;
    }

    public String getAbcPrefix() {
        return abcPrefix;
    }

    public void setAbcPrefix(String abcPrefix) {
        this.abcPrefix = abcPrefix;
    }

    public String getAbcNumero() {
        return abcNumero;
    }

    public void setAbcNumero(String abcNumero) {
        this.abcNumero = abcNumero;
    }

    public String getAbcParcel() {
        return abcParcel;
    }

    public void setAbcParcel(String abcParcel) {
        this.abcParcel = abcParcel;
    }

    public String getAbcTipo() {
        return abcTipo;
    }

    public void setAbcTipo(String abcTipo) {
        this.abcTipo = abcTipo;
    }

    public String getAbcCodfor() {
        return abcCodfor;
    }

    public void setAbcCodfor(String abcCodfor) {
        this.abcCodfor = abcCodfor;
    }

    public String getAbcLojfor() {
        return abcLojfor;
    }

    public void setAbcLojfor(String abcLojfor) {
        this.abcLojfor = abcLojfor;
    }

    public String getAbcContrt() {
        return abcContrt;
    }

    public void setAbcContrt(String abcContrt) {
        this.abcContrt = abcContrt;
    }

    public String getAbcCodgrp() {
        return abcCodgrp;
    }

    public void setAbcCodgrp(String abcCodgrp) {
        this.abcCodgrp = abcCodgrp;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getAbcUarmaz() {
        return abcUarmaz;
    }

    public void setAbcUarmaz(String abcUarmaz) {
        this.abcUarmaz = abcUarmaz;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Abc010)) {
            return false;
        }
        Abc010 other = (Abc010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gerador.Abc010[ rECNO=" + rECNO + " ]";
    }
    
}
