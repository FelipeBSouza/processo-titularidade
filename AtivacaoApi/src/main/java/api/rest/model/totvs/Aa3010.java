/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

/**
 *
 * @author ROBSON
 */

@Entity(name = "Aa3010")
@Table(name = "AA3010", schema = "dbo")
public class Aa3010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AA3_FILIAL")
    private String aa3Filial;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "AA3_CODCLI", referencedColumnName = "A1_COD", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "AA3_LOJA", referencedColumnName = "A1_LOJA", nullable = false, insertable = false, updatable = false),})
    private Sa1010 sa1;
    @NotNull
    @Column(name = "AA3_CODPRO")
    private String aa3Codpro;
//C    @ManyToOne(fetch = FetchType.LAZY)
//C    @JoinColumn(name = "AA3_CODPRO", referencedColumnName = "B1_COD", nullable = false, insertable = false, updatable = false)
//C    private Sb1010 sb1;
    
     
    @NaturalId
    @Column(name = "AA3_NUMSER")
    private String aa3Numser;
    
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTVEND")
    private String aa3Dtvend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTINST")
    private String aa3Dtinst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTGAR")
    private String aa3Dtgar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AA3_CBASE")
    private String aa3Cbase;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AA3_ITEM")
    private String aa3Item;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "AA3_CHAPA")
    private String aa3Chapa;
    @Basic(optional = false)
    @Column(name = "AA3_CODTEC")
    private String aa3Codtec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AA3_FORNEC")
    private String aa3Fornec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AA3_LOJAFO")
    private String aa3Lojafo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AA3_NFAQUI")
    private String aa3Nfaqui;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AA3_CODFAB")
    private String aa3Codfab;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AA3_LOJAFA")
    private String aa3Lojafa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AA3_NFVEND")
    private String aa3Nfvend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA3_CODMEM")
    private String aa3Codmem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA3_CONTRT")
    private String aa3Contrt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTCTAM")
    private String aa3Dtctam;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA3_CTAPRE")
    private String aa3Ctapre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_VLRCTR")
    private double aa3Vlrctr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "AA3_LOCAL")
    private String aa3Local;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA3_MODELO")
    private String aa3Modelo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_ACUMUL")
    private double aa3Acumul;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_MTBF")
    private double aa3Mtbf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_MTTR")
    private double aa3Mttr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA3_PLANO")
    private String aa3Plano;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTEXP")
    private String aa3Dtexp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA3_ULTNF")
    private String aa3Ultnf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_HORRP")
    private double aa3Horrp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_QTREP")
    private double aa3Qtrep;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AA3_ULTSER")
    private String aa3Ultser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_QTFAL")
    private double aa3Qtfal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "AA3_ULTFOR")
    private String aa3Ultfor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_QTREPH")
    private double aa3Qtreph;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AA3_ULTLOJ")
    private String aa3Ultloj;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_HORRPH")
    private double aa3Horrph;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AA3_ULTIT")
    private String aa3Ultit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_QTFALH")
    private double aa3Qtfalh;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_ULTPRC")
    private double aa3Ultprc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_HOROPH")
    private double aa3Horoph;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AA3_OK")
    private String aa3Ok;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AA3_STATUS")
    private String aa3Status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_HORDIA")
    private double aa3Hordia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AA3_STAANT")
    private String aa3Staant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTREFH")
    private String aa3Dtrefh;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_DIAOPE")
    private double aa3Diaope;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTMTBF")
    private String aa3Dtmtbf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_DTMTTR")
    private String aa3Dtmttr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AA3_SITE")
    private String aa3Site;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA3_CODGRP")
    private String aa3Codgrp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AA3_CODLOC")
    private String aa3Codloc;
    @JoinColumn(name = "AA3_UNUCTR", referencedColumnName = "ADA_NUMCTR", nullable = false, insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Ada010 ada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AA3_UITCTR")
    private String aa3Uitctr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA3_UHTTP")
    private String aa3Uhttp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA3_ULOGIN")
    private String aa3Ulogin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA3_USENHA")
    private String aa3Usenha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA3_UPOSIC")
    private String aa3Uposic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AA3_UMAC")
    private String aa3Umac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_UCAIXA")
    private String aa3UCaixa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_USPLT")
    private String aa3USplt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AA3_UPORTA")
    private String aa3UPorta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "R_E_C_D_E_L_")
//    private int rECDEL;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AA3_UIDAGA")
    private String aa3Uidaga;

    public Aa3010() {
    }
    
    

    public Aa3010(Sa1010 sa1, Ada010 ada) {
        this.sa1 = sa1;
        this.ada = ada;
    }

    public Aa3010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAa3Filial() {
        return aa3Filial;
    }

    public void setAa3Filial(String aa3Filial) {
        this.aa3Filial = aa3Filial;
    }

    public Sa1010 getSa1() {
        return sa1;
    }

    public void setSa1(Sa1010 sa1) {
        this.sa1 = sa1;
    }

//C    public Sb1010 getSb1() {
//C        return sb1;
//C    }
//C
//C    public void setSb1(Sb1010 sb1) {
//C        this.sb1 = sb1;
//C    }

    public String getAa3Numser() {
        return aa3Numser;
    }

    public void setAa3Numser(String aa3Numser) {
        this.aa3Numser = aa3Numser;
    }

    public String getAa3Dtvend() {
        return aa3Dtvend;
    }

    public void setAa3Dtvend(String aa3Dtvend) {
        this.aa3Dtvend = aa3Dtvend;
    }

    public String getAa3Dtinst() {
        return aa3Dtinst;
    }

    public void setAa3Dtinst(String aa3Dtinst) {
        this.aa3Dtinst = aa3Dtinst;
    }
    
    

    public String getAa3Uidaga() {
		return aa3Uidaga;
	}



	public void setAa3Uidaga(String aa3Uidaga) {
		this.aa3Uidaga = aa3Uidaga;
	}



	public String getAa3Dtgar() {
        return aa3Dtgar;
    }

    public void setAa3Dtgar(String aa3Dtgar) {
        this.aa3Dtgar = aa3Dtgar;
    }

    public String getAa3Cbase() {
        return aa3Cbase;
    }

    public void setAa3Cbase(String aa3Cbase) {
        this.aa3Cbase = aa3Cbase;
    }

    public String getAa3Item() {
        return aa3Item;
    }

    public void setAa3Item(String aa3Item) {
        this.aa3Item = aa3Item;
    }

    public String getAa3Chapa() {
        return aa3Chapa.replace(" ", "");
    }

    public void setAa3Chapa(String aa3Chapa) {
        this.aa3Chapa = aa3Chapa;
    }

    public String getAa3Codtec() {
        return aa3Codtec;
    }

    public void setAa3Codtec(String aa3Codtec) {
        this.aa3Codtec = aa3Codtec;
    }

    public String getAa3Fornec() {
        return aa3Fornec;
    }

    public void setAa3Fornec(String aa3Fornec) {
        this.aa3Fornec = aa3Fornec;
    }

    public String getAa3Lojafo() {
        return aa3Lojafo;
    }

    public void setAa3Lojafo(String aa3Lojafo) {
        this.aa3Lojafo = aa3Lojafo;
    }

    public String getAa3Nfaqui() {
        return aa3Nfaqui;
    }

    public void setAa3Nfaqui(String aa3Nfaqui) {
        this.aa3Nfaqui = aa3Nfaqui;
    }

    public String getAa3Codfab() {
        return aa3Codfab;
    }

    public void setAa3Codfab(String aa3Codfab) {
        this.aa3Codfab = aa3Codfab;
    }

    public String getAa3Lojafa() {
        return aa3Lojafa;
    }

    public void setAa3Lojafa(String aa3Lojafa) {
        this.aa3Lojafa = aa3Lojafa;
    }

    public String getAa3Nfvend() {
        return aa3Nfvend;
    }

    public void setAa3Nfvend(String aa3Nfvend) {
        this.aa3Nfvend = aa3Nfvend;
    }

    public String getAa3Codmem() {
        return aa3Codmem;
    }

    public void setAa3Codmem(String aa3Codmem) {
        this.aa3Codmem = aa3Codmem;
    }

    public String getAa3Contrt() {
        return aa3Contrt;
    }

    public void setAa3Contrt(String aa3Contrt) {
        this.aa3Contrt = aa3Contrt;
    }

    public String getAa3Dtctam() {
        return aa3Dtctam;
    }

    public void setAa3Dtctam(String aa3Dtctam) {
        this.aa3Dtctam = aa3Dtctam;
    }

    public String getAa3Ctapre() {
        return aa3Ctapre;
    }

    public void setAa3Ctapre(String aa3Ctapre) {
        this.aa3Ctapre = aa3Ctapre;
    }

    public double getAa3Vlrctr() {
        return aa3Vlrctr;
    }

    public void setAa3Vlrctr(double aa3Vlrctr) {
        this.aa3Vlrctr = aa3Vlrctr;
    }

    public String getAa3Local() {
        return aa3Local;
    }

    public void setAa3Local(String aa3Local) {
        this.aa3Local = aa3Local;
    }

    public String getAa3Modelo() {
        return aa3Modelo;
    }

    public void setAa3Modelo(String aa3Modelo) {
        this.aa3Modelo = aa3Modelo;
    }

    public double getAa3Acumul() {
        return aa3Acumul;
    }

    public void setAa3Acumul(double aa3Acumul) {
        this.aa3Acumul = aa3Acumul;
    }

    public double getAa3Mtbf() {
        return aa3Mtbf;
    }

    public void setAa3Mtbf(double aa3Mtbf) {
        this.aa3Mtbf = aa3Mtbf;
    }

    public double getAa3Mttr() {
        return aa3Mttr;
    }

    public void setAa3Mttr(double aa3Mttr) {
        this.aa3Mttr = aa3Mttr;
    }

    public String getAa3Plano() {
        return aa3Plano;
    }

    public void setAa3Plano(String aa3Plano) {
        this.aa3Plano = aa3Plano;
    }

    public String getAa3Dtexp() {
        return aa3Dtexp;
    }

    public void setAa3Dtexp(String aa3Dtexp) {
        this.aa3Dtexp = aa3Dtexp;
    }

    public String getAa3Ultnf() {
        return aa3Ultnf;
    }

    public void setAa3Ultnf(String aa3Ultnf) {
        this.aa3Ultnf = aa3Ultnf;
    }

    public double getAa3Horrp() {
        return aa3Horrp;
    }

    public void setAa3Horrp(double aa3Horrp) {
        this.aa3Horrp = aa3Horrp;
    }

    public double getAa3Qtrep() {
        return aa3Qtrep;
    }

    public void setAa3Qtrep(double aa3Qtrep) {
        this.aa3Qtrep = aa3Qtrep;
    }

    public String getAa3Ultser() {
        return aa3Ultser;
    }

    public void setAa3Ultser(String aa3Ultser) {
        this.aa3Ultser = aa3Ultser;
    }

    public double getAa3Qtfal() {
        return aa3Qtfal;
    }

    public void setAa3Qtfal(double aa3Qtfal) {
        this.aa3Qtfal = aa3Qtfal;
    }

    public String getAa3Ultfor() {
        return aa3Ultfor;
    }

    public void setAa3Ultfor(String aa3Ultfor) {
        this.aa3Ultfor = aa3Ultfor;
    }

    public double getAa3Qtreph() {
        return aa3Qtreph;
    }

    public void setAa3Qtreph(double aa3Qtreph) {
        this.aa3Qtreph = aa3Qtreph;
    }

    public String getAa3Ultloj() {
        return aa3Ultloj;
    }

    public void setAa3Ultloj(String aa3Ultloj) {
        this.aa3Ultloj = aa3Ultloj;
    }

    public double getAa3Horrph() {
        return aa3Horrph;
    }

    public void setAa3Horrph(double aa3Horrph) {
        this.aa3Horrph = aa3Horrph;
    }

    public String getAa3Ultit() {
        return aa3Ultit;
    }

    public void setAa3Ultit(String aa3Ultit) {
        this.aa3Ultit = aa3Ultit;
    }

    public double getAa3Qtfalh() {
        return aa3Qtfalh;
    }

    public void setAa3Qtfalh(double aa3Qtfalh) {
        this.aa3Qtfalh = aa3Qtfalh;
    }

    public double getAa3Ultprc() {
        return aa3Ultprc;
    }

    public void setAa3Ultprc(double aa3Ultprc) {
        this.aa3Ultprc = aa3Ultprc;
    }

    public double getAa3Horoph() {
        return aa3Horoph;
    }

    public void setAa3Horoph(double aa3Horoph) {
        this.aa3Horoph = aa3Horoph;
    }

    public String getAa3Ok() {
        return aa3Ok;
    }

    public void setAa3Ok(String aa3Ok) {
        this.aa3Ok = aa3Ok;
    }

    public String getAa3Status() {
        return aa3Status;
    }

    public void setAa3Status(String aa3Status) {
        this.aa3Status = aa3Status;
    }

    public double getAa3Hordia() {
        return aa3Hordia;
    }

    public void setAa3Hordia(double aa3Hordia) {
        this.aa3Hordia = aa3Hordia;
    }

    public String getAa3Staant() {
        return aa3Staant;
    }

    public void setAa3Staant(String aa3Staant) {
        this.aa3Staant = aa3Staant;
    }

    public String getAa3Dtrefh() {
        return aa3Dtrefh;
    }

    public void setAa3Dtrefh(String aa3Dtrefh) {
        this.aa3Dtrefh = aa3Dtrefh;
    }

    public double getAa3Diaope() {
        return aa3Diaope;
    }

    public void setAa3Diaope(double aa3Diaope) {
        this.aa3Diaope = aa3Diaope;
    }

    public String getAa3Dtmtbf() {
        return aa3Dtmtbf;
    }

    public void setAa3Dtmtbf(String aa3Dtmtbf) {
        this.aa3Dtmtbf = aa3Dtmtbf;
    }

    public String getAa3Dtmttr() {
        return aa3Dtmttr;
    }

    public void setAa3Dtmttr(String aa3Dtmttr) {
        this.aa3Dtmttr = aa3Dtmttr;
    }

    public String getAa3Site() {
        return aa3Site;
    }

    public void setAa3Site(String aa3Site) {
        this.aa3Site = aa3Site;
    }

    public String getAa3Codgrp() {
        return aa3Codgrp;
    }

    public void setAa3Codgrp(String aa3Codgrp) {
        this.aa3Codgrp = aa3Codgrp;
    }

    public String getAa3Codloc() {
        return aa3Codloc;
    }

    public void setAa3Codloc(String aa3Codloc) {
        this.aa3Codloc = aa3Codloc;
    }

    public Ada010 getAda() {
        return ada;
    }

    public void setAda(Ada010 ada) {
        this.ada = ada;
    }

    public String getAa3Uitctr() {
        return aa3Uitctr;
    }

    public void setAa3Uitctr(String aa3Uitctr) {
        this.aa3Uitctr = aa3Uitctr;
    }

    public String getAa3Uhttp() {
        return aa3Uhttp;
    }

    public void setAa3Uhttp(String aa3Uhttp) {
        this.aa3Uhttp = aa3Uhttp;
    }

    public String getAa3Ulogin() {
        return aa3Ulogin;
    }

    public void setAa3Ulogin(String aa3Ulogin) {
        this.aa3Ulogin = aa3Ulogin;
    }

    public String getAa3Usenha() {
        return aa3Usenha;
    }

    public void setAa3Usenha(String aa3Usenha) {
        this.aa3Usenha = aa3Usenha;
    }

    public String getAa3Uposic() {
        return aa3Uposic;
    }

    public void setAa3Uposic(String aa3Uposic) {
        this.aa3Uposic = aa3Uposic;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

//    public int getRECDEL() {
//        return rECDEL;
//    }
//
//    public void setRECDEL(int rECDEL) {
//        this.rECDEL = rECDEL;
//    }
    public String getAa3Umac() {
        return aa3Umac;
    }

    public void setAa3Umac(String aa3Umac) {
        this.aa3Umac = aa3Umac;
    }

    public String getAa3UCaixa() {
        return aa3UCaixa;
    }

    public void setAa3UCaixa(String aa3UCaixa) {
        this.aa3UCaixa = aa3UCaixa;
    }

    public String getAa3USplt() {
        return aa3USplt;
    }

    public void setAa3USplt(String aa3USplt) {
        this.aa3USplt = aa3USplt;
    }

    public String getAa3UPorta() {
        return aa3UPorta;
    }

    public void setAa3UPorta(String aa3UPorta) {
        this.aa3UPorta = aa3UPorta;
    }
    
    
 
    public String getAa3Codpro() {
		return aa3Codpro;
	}



	public void setAa3Codpro(String aa3Codpro) {
		this.aa3Codpro = aa3Codpro;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aa3Numser == null) ? 0 : aa3Numser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aa3010 other = (Aa3010) obj;
		if (aa3Numser == null) {
			if (other.aa3Numser != null)
				return false;
		} else if (!aa3Numser.equals(other.aa3Numser))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return "classes.totvs.Aa3010[ rECNO=" + rECNO + " ]";
    }
}
