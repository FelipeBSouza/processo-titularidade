/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(name = "SZ2010")
@XmlRootElement
public class Sz2010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "Z2_FILIAL")
    private String z2Filial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "Z2_NUMATEN")
    private String z2Numaten;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "Z2_NUMCTR", referencedColumnName = "ADB_NUMCTR", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "Z2_ITEMCTR", referencedColumnName = "ADB_ITEM", nullable = false, insertable = false, updatable = false),})
    private Adb010 adb;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Z2_PRODUTO", referencedColumnName = "B1_COD")
    private Sb1010 sb1;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "Z2_NUMOS", referencedColumnName = "AB7_NUMOS", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "Z2_ITEMOS", referencedColumnName = "AB7_ITEM", nullable = false, insertable = false, updatable = false),})
    private Ab7010 ab7;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Z2_ACAO")
    private String z2Acao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "Z2_EXECUTA")
    private String z2Executa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;

    public Sz2010() {
    }

    public Sz2010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getZ2Filial() {
        return z2Filial;
    }

    public void setZ2Filial(String z2Filial) {
        this.z2Filial = z2Filial;
    }

    public String getZ2Numaten() {
        return z2Numaten;
    }

    public void setZ2Numaten(String z2Numaten) {
        this.z2Numaten = z2Numaten;
    }

    public String getZ2Acao() {
        return z2Acao;
    }

    public void setZ2Acao(String z2Acao) {
        this.z2Acao = z2Acao;
    }

    public String getZ2Executa() {
        return z2Executa;
    }

    public void setZ2Executa(String z2Executa) {
        this.z2Executa = z2Executa;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Adb010 getAdb() {
        return adb;
    }

    public void setAdb(Adb010 adb) {
        this.adb = adb;
    }

    public Sb1010 getSb1() {
        return sb1;
    }

    public void setSb1(Sb1010 sb1) {
        this.sb1 = sb1;
    }

    public Ab7010 getAb7() {
        return ab7;
    }

    public void setAb7(Ab7010 ab7) {
        this.ab7 = ab7;
    }

    public String getdELET() {
        return dELET;
    }

    public void setdELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getrECNO() {
        return rECNO;
    }

    public void setrECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sz2010)) {
            return false;
        }
        Sz2010 other = (Sz2010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.liguetelecom.model.totvs.Sz2010[ rECNO=" + rECNO + " ]";
    }
}
