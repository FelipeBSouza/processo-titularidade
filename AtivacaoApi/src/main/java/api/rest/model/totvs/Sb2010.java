package api.rest.model.totvs;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Robson
 */
@Entity
@Table(name = "SB2010", schema = "dbo")
public class Sb2010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "B2_FILIAL")
    private String b2Filial;
    @Basic(optional = false)
    @Column(name = "B2_COD")
    private String b2Cod;
    @Basic(optional = false)
    @Column(name = "B2_QFIM")
    private double b2Qfim;
    @Basic(optional = false)
    @Column(name = "B2_LOCAL")
    private String b2Local;
    @Basic(optional = false)
    @Column(name = "B2_QATU")
    private double b2Qatu;
    @Basic(optional = false)
    @Column(name = "B2_VFIM1")
    private double b2Vfim1;
    @Basic(optional = false)
    @Column(name = "B2_VATU1")
    private double b2Vatu1;
    @Basic(optional = false)
    @Column(name = "B2_CM1")
    private double b2Cm1;
    @Basic(optional = false)
    @Column(name = "B2_VFIM2")
    private double b2Vfim2;
    @Basic(optional = false)
    @Column(name = "B2_VATU2")
    private double b2Vatu2;
    @Basic(optional = false)
    @Column(name = "B2_CM2")
    private double b2Cm2;
    @Basic(optional = false)
    @Column(name = "B2_VFIM3")
    private double b2Vfim3;
    @Basic(optional = false)
    @Column(name = "B2_VATU3")
    private double b2Vatu3;
    @Basic(optional = false)
    @Column(name = "B2_CM3")
    private double b2Cm3;
    @Basic(optional = false)
    @Column(name = "B2_VFIM4")
    private double b2Vfim4;
    @Basic(optional = false)
    @Column(name = "B2_VATU4")
    private double b2Vatu4;
    @Basic(optional = false)
    @Column(name = "B2_CM4")
    private double b2Cm4;
    @Basic(optional = false)
    @Column(name = "B2_VFIM5")
    private double b2Vfim5;
    @Basic(optional = false)
    @Column(name = "B2_VATU5")
    private double b2Vatu5;
    @Basic(optional = false)
    @Column(name = "B2_CM5")
    private double b2Cm5;
    @Basic(optional = false)
    @Column(name = "B2_QEMP")
    private double b2Qemp;
    @Basic(optional = false)
    @Column(name = "B2_QEMPN")
    private double b2Qempn;
    @Basic(optional = false)
    @Column(name = "B2_QTSEGUM")
    private double b2Qtsegum;
    @Basic(optional = false)
    @Column(name = "B2_USAI")
    private String b2Usai;
    @Basic(optional = false)
    @Column(name = "B2_RESERVA")
    private double b2Reserva;
    @Basic(optional = false)
    @Column(name = "B2_QPEDVEN")
    private double b2Qpedven;
    @Basic(optional = false)
    @Column(name = "B2_LOCALIZ")
    private String b2Localiz;
    @Basic(optional = false)
    @Column(name = "B2_NAOCLAS")
    private double b2Naoclas;
    @Basic(optional = false)
    @Column(name = "B2_SALPEDI")
    private double b2Salpedi;
    @Basic(optional = false)
    @Column(name = "B2_DINVENT")
    private String b2Dinvent;
    @Basic(optional = false)
    @Column(name = "B2_DINVFIM")
    private String b2Dinvfim;
    @Basic(optional = false)
    @Column(name = "B2_QTNP")
    private double b2Qtnp;
    @Basic(optional = false)
    @Column(name = "B2_QNPT")
    private double b2Qnpt;
    @Basic(optional = false)
    @Column(name = "B2_QTER")
    private double b2Qter;
    @Basic(optional = false)
    @Column(name = "B2_QFIM2")
    private double b2Qfim2;
    @Basic(optional = false)
    @Column(name = "B2_QACLASS")
    private double b2Qaclass;
    @Basic(optional = false)
    @Column(name = "B2_DTINV")
    private String b2Dtinv;
    @Basic(optional = false)
    @Column(name = "B2_CMFF1")
    private double b2Cmff1;
    @Basic(optional = false)
    @Column(name = "B2_CMFF2")
    private double b2Cmff2;
    @Basic(optional = false)
    @Column(name = "B2_CMFF3")
    private double b2Cmff3;
    @Basic(optional = false)
    @Column(name = "B2_CMFF4")
    private double b2Cmff4;
    @Basic(optional = false)
    @Column(name = "B2_CMFF5")
    private double b2Cmff5;
    @Basic(optional = false)
    @Column(name = "B2_VFIMFF1")
    private double b2Vfimff1;
    @Basic(optional = false)
    @Column(name = "B2_VFIMFF2")
    private double b2Vfimff2;
    @Basic(optional = false)
    @Column(name = "B2_VFIMFF3")
    private double b2Vfimff3;
    @Basic(optional = false)
    @Column(name = "B2_VFIMFF4")
    private double b2Vfimff4;
    @Basic(optional = false)
    @Column(name = "B2_VFIMFF5")
    private double b2Vfimff5;
    @Basic(optional = false)
    @Column(name = "B2_QEMPSA")
    private double b2Qempsa;
    @Basic(optional = false)
    @Column(name = "B2_QEMPPRE")
    private double b2Qemppre;
    @Basic(optional = false)
    @Column(name = "B2_SALPPRE")
    private double b2Salppre;
    @Basic(optional = false)
    @Column(name = "B2_QEMP2")
    private double b2Qemp2;
    @Basic(optional = false)
    @Column(name = "B2_QEMPN2")
    private double b2Qempn2;
    @Basic(optional = false)
    @Column(name = "B2_RESERV2")
    private double b2Reserv2;
    @Basic(optional = false)
    @Column(name = "B2_QPEDVE2")
    private double b2Qpedve2;
    @Basic(optional = false)
    @Column(name = "B2_QEPRE2")
    private double b2Qepre2;
    @Basic(optional = false)
    @Column(name = "B2_QFIMFF")
    private double b2Qfimff;
    @Basic(optional = false)
    @Column(name = "B2_SALPED2")
    private double b2Salped2;
    @Basic(optional = false)
    @Column(name = "B2_QEMPPRJ")
    private double b2Qempprj;
    @Basic(optional = false)
    @Column(name = "B2_QEMPPR2")
    private double b2Qemppr2;
    @Basic(optional = false)
    @Column(name = "B2_STATUS")
    private String b2Status;
    @Basic(optional = false)
    @Column(name = "B2_CMFIM1")
    private double b2Cmfim1;
    @Basic(optional = false)
    @Column(name = "B2_CMFIM2")
    private double b2Cmfim2;
    @Basic(optional = false)
    @Column(name = "B2_CMFIM3")
    private double b2Cmfim3;
    @Basic(optional = false)
    @Column(name = "B2_CMFIM4")
    private double b2Cmfim4;
    @Basic(optional = false)
    @Column(name = "B2_CMFIM5")
    private double b2Cmfim5;
    @Basic(optional = false)
    @Column(name = "B2_TIPO")
    private String b2Tipo;
    @Basic(optional = false)
    @Column(name = "B2_CMRP1")
    private double b2Cmrp1;
    @Basic(optional = false)
    @Column(name = "B2_VFRP1")
    private double b2Vfrp1;
    @Basic(optional = false)
    @Column(name = "B2_CMRP2")
    private double b2Cmrp2;
    @Basic(optional = false)
    @Column(name = "B2_VFRP2")
    private double b2Vfrp2;
    @Basic(optional = false)
    @Column(name = "B2_CMRP3")
    private double b2Cmrp3;
    @Basic(optional = false)
    @Column(name = "B2_VFRP3")
    private double b2Vfrp3;
    @Basic(optional = false)
    @Column(name = "B2_CMRP4")
    private double b2Cmrp4;
    @Basic(optional = false)
    @Column(name = "B2_VFRP4")
    private double b2Vfrp4;
    @Basic(optional = false)
    @Column(name = "B2_CMRP5")
    private double b2Cmrp5;
    @Basic(optional = false)
    @Column(name = "B2_VFRP5")
    private double b2Vfrp5;
    @Basic(optional = false)
    @Column(name = "B2_QULT")
    private double b2Qult;
    @Basic(optional = false)
    @Column(name = "B2_DULT")
    private String b2Dult;
    @Basic(optional = false)
    @Column(name = "B2_MSEXP")
    private String b2Msexp;
    @Basic(optional = false)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @Column(name = "R_E_C_D_E_L_")
    private int rECDEL;
    @Basic(optional = false)
    @Column(name = "B2_BLOQUEI")
    private String b2Bloquei;
    @Basic(optional = false)
    @Column(name = "B2_ECSALDO")
    private double b2Ecsaldo;
    @Basic(optional = false)
    @Column(name = "B2_XDTFIN")
    private String b2Xdtfin;
    @Basic(optional = false)
    @Column(name = "B2_XDTINI")
    private String b2Xdtini;
//    @Basic(optional = false)
//    @Column(name = "B2_HMOV")
//    private String b2Hmov;
//    @Basic(optional = false)
//    @Column(name = "B2_HULT")
//    private String b2Hult;
//    @Basic(optional = false)
//    @Column(name = "B2_DMOV")
//    private String b2Dmov;
    
    @Basic(optional = false)
    @Column(name = "B2_ULIBIC")
    private String b2Ulibic;
    
    @Basic(optional = false)
    @Column(name = "B2_UICLAS")
    private String b2Uiclass;

    public Sb2010() {
    }

    public Sb2010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Sb2010(Integer rECNO, String b2Filial, String b2Cod, double b2Qfim, String b2Local, double b2Qatu, double b2Vfim1, double b2Vatu1, double b2Cm1, double b2Vfim2, double b2Vatu2, double b2Cm2, double b2Vfim3, double b2Vatu3, double b2Cm3, double b2Vfim4, double b2Vatu4, double b2Cm4, double b2Vfim5, double b2Vatu5, double b2Cm5, double b2Qemp, double b2Qempn, double b2Qtsegum, String b2Usai, double b2Reserva, double b2Qpedven, String b2Localiz, double b2Naoclas, double b2Salpedi, String b2Dinvent, String b2Dinvfim, double b2Qtnp, double b2Qnpt, double b2Qter, double b2Qfim2, double b2Qaclass, String b2Dtinv, double b2Cmff1, double b2Cmff2, double b2Cmff3, double b2Cmff4, double b2Cmff5, double b2Vfimff1, double b2Vfimff2, double b2Vfimff3, double b2Vfimff4, double b2Vfimff5, double b2Qempsa, double b2Qemppre, double b2Salppre, double b2Qemp2, double b2Qempn2, double b2Reserv2, double b2Qpedve2, double b2Qepre2, double b2Qfimff, double b2Salped2, double b2Qempprj, double b2Qemppr2, String b2Status, double b2Cmfim1, double b2Cmfim2, double b2Cmfim3, double b2Cmfim4, double b2Cmfim5, String b2Tipo, double b2Cmrp1, double b2Vfrp1, double b2Cmrp2, double b2Vfrp2, double b2Cmrp3, double b2Vfrp3, double b2Cmrp4, double b2Vfrp4, double b2Cmrp5, double b2Vfrp5, double b2Qult, String b2Dult, String b2Msexp, String dELET, int rECDEL, String b2Bloquei, double b2Ecsaldo, String b2Xdtfin, String b2Xdtini, String b2Hmov, String b2Hult, String b2Dmov) {
        this.rECNO = rECNO;
        this.b2Filial = b2Filial;
        this.b2Cod = b2Cod;
        this.b2Qfim = b2Qfim;
        this.b2Local = b2Local;
        this.b2Qatu = b2Qatu;
        this.b2Vfim1 = b2Vfim1;
        this.b2Vatu1 = b2Vatu1;
        this.b2Cm1 = b2Cm1;
        this.b2Vfim2 = b2Vfim2;
        this.b2Vatu2 = b2Vatu2;
        this.b2Cm2 = b2Cm2;
        this.b2Vfim3 = b2Vfim3;
        this.b2Vatu3 = b2Vatu3;
        this.b2Cm3 = b2Cm3;
        this.b2Vfim4 = b2Vfim4;
        this.b2Vatu4 = b2Vatu4;
        this.b2Cm4 = b2Cm4;
        this.b2Vfim5 = b2Vfim5;
        this.b2Vatu5 = b2Vatu5;
        this.b2Cm5 = b2Cm5;
        this.b2Qemp = b2Qemp;
        this.b2Qempn = b2Qempn;
        this.b2Qtsegum = b2Qtsegum;
        this.b2Usai = b2Usai;
        this.b2Reserva = b2Reserva;
        this.b2Qpedven = b2Qpedven;
        this.b2Localiz = b2Localiz;
        this.b2Naoclas = b2Naoclas;
        this.b2Salpedi = b2Salpedi;
        this.b2Dinvent = b2Dinvent;
        this.b2Dinvfim = b2Dinvfim;
        this.b2Qtnp = b2Qtnp;
        this.b2Qnpt = b2Qnpt;
        this.b2Qter = b2Qter;
        this.b2Qfim2 = b2Qfim2;
        this.b2Qaclass = b2Qaclass;
        this.b2Dtinv = b2Dtinv;
        this.b2Cmff1 = b2Cmff1;
        this.b2Cmff2 = b2Cmff2;
        this.b2Cmff3 = b2Cmff3;
        this.b2Cmff4 = b2Cmff4;
        this.b2Cmff5 = b2Cmff5;
        this.b2Vfimff1 = b2Vfimff1;
        this.b2Vfimff2 = b2Vfimff2;
        this.b2Vfimff3 = b2Vfimff3;
        this.b2Vfimff4 = b2Vfimff4;
        this.b2Vfimff5 = b2Vfimff5;
        this.b2Qempsa = b2Qempsa;
        this.b2Qemppre = b2Qemppre;
        this.b2Salppre = b2Salppre;
        this.b2Qemp2 = b2Qemp2;
        this.b2Qempn2 = b2Qempn2;
        this.b2Reserv2 = b2Reserv2;
        this.b2Qpedve2 = b2Qpedve2;
        this.b2Qepre2 = b2Qepre2;
        this.b2Qfimff = b2Qfimff;
        this.b2Salped2 = b2Salped2;
        this.b2Qempprj = b2Qempprj;
        this.b2Qemppr2 = b2Qemppr2;
        this.b2Status = b2Status;
        this.b2Cmfim1 = b2Cmfim1;
        this.b2Cmfim2 = b2Cmfim2;
        this.b2Cmfim3 = b2Cmfim3;
        this.b2Cmfim4 = b2Cmfim4;
        this.b2Cmfim5 = b2Cmfim5;
        this.b2Tipo = b2Tipo;
        this.b2Cmrp1 = b2Cmrp1;
        this.b2Vfrp1 = b2Vfrp1;
        this.b2Cmrp2 = b2Cmrp2;
        this.b2Vfrp2 = b2Vfrp2;
        this.b2Cmrp3 = b2Cmrp3;
        this.b2Vfrp3 = b2Vfrp3;
        this.b2Cmrp4 = b2Cmrp4;
        this.b2Vfrp4 = b2Vfrp4;
        this.b2Cmrp5 = b2Cmrp5;
        this.b2Vfrp5 = b2Vfrp5;
        this.b2Qult = b2Qult;
        this.b2Dult = b2Dult;
        this.b2Msexp = b2Msexp;
        this.dELET = dELET;
        this.rECDEL = rECDEL;
        this.b2Bloquei = b2Bloquei;
        this.b2Ecsaldo = b2Ecsaldo;
        this.b2Xdtfin = b2Xdtfin;
        this.b2Xdtini = b2Xdtini;
//        this.b2Hmov = b2Hmov;
//        this.b2Hult = b2Hult;
//        this.b2Dmov = b2Dmov;
    }

    public String getB2Filial() {
        return b2Filial;
    }

    public void setB2Filial(String b2Filial) {
        this.b2Filial = b2Filial;
    }

    public String getB2Cod() {
        return b2Cod;
    }

    public void setB2Cod(String b2Cod) {
        this.b2Cod = b2Cod;
    }

    public double getB2Qfim() {
        return b2Qfim;
    }

    public void setB2Qfim(double b2Qfim) {
        this.b2Qfim = b2Qfim;
    }

    public String getB2Local() {
        return b2Local;
    }

    public void setB2Local(String b2Local) {
        this.b2Local = b2Local;
    }

    public double getB2Qatu() {
        return b2Qatu;
    }

    public void setB2Qatu(double b2Qatu) {
        this.b2Qatu = b2Qatu;
    }

    public double getB2Vfim1() {
        return b2Vfim1;
    }

    public void setB2Vfim1(double b2Vfim1) {
        this.b2Vfim1 = b2Vfim1;
    }

    public double getB2Vatu1() {
        return b2Vatu1;
    }

    public void setB2Vatu1(double b2Vatu1) {
        this.b2Vatu1 = b2Vatu1;
    }

    public double getB2Cm1() {
        return b2Cm1;
    }

    public void setB2Cm1(double b2Cm1) {
        this.b2Cm1 = b2Cm1;
    }

    public double getB2Vfim2() {
        return b2Vfim2;
    }

    public void setB2Vfim2(double b2Vfim2) {
        this.b2Vfim2 = b2Vfim2;
    }

    public double getB2Vatu2() {
        return b2Vatu2;
    }

    public void setB2Vatu2(double b2Vatu2) {
        this.b2Vatu2 = b2Vatu2;
    }

    public double getB2Cm2() {
        return b2Cm2;
    }

    public void setB2Cm2(double b2Cm2) {
        this.b2Cm2 = b2Cm2;
    }

    public double getB2Vfim3() {
        return b2Vfim3;
    }

    public void setB2Vfim3(double b2Vfim3) {
        this.b2Vfim3 = b2Vfim3;
    }

    public double getB2Vatu3() {
        return b2Vatu3;
    }

    public void setB2Vatu3(double b2Vatu3) {
        this.b2Vatu3 = b2Vatu3;
    }

    public double getB2Cm3() {
        return b2Cm3;
    }

    public void setB2Cm3(double b2Cm3) {
        this.b2Cm3 = b2Cm3;
    }

    public double getB2Vfim4() {
        return b2Vfim4;
    }

    public void setB2Vfim4(double b2Vfim4) {
        this.b2Vfim4 = b2Vfim4;
    }

    public double getB2Vatu4() {
        return b2Vatu4;
    }

    public void setB2Vatu4(double b2Vatu4) {
        this.b2Vatu4 = b2Vatu4;
    }

    public double getB2Cm4() {
        return b2Cm4;
    }

    public void setB2Cm4(double b2Cm4) {
        this.b2Cm4 = b2Cm4;
    }

    public double getB2Vfim5() {
        return b2Vfim5;
    }    

    public String getB2Ulibic() {
		return b2Ulibic;
	}

	public void setB2Ulibic(String b2Ulibic) {
		this.b2Ulibic = b2Ulibic;
	}

	public String getB2Uiclass() {
		return b2Uiclass;
	}

	public void setB2Uiclass(String b2Uiclass) {
		this.b2Uiclass = b2Uiclass;
	}

	public void setB2Vfim5(double b2Vfim5) {
        this.b2Vfim5 = b2Vfim5;
    }

    public double getB2Vatu5() {
        return b2Vatu5;
    }

    public void setB2Vatu5(double b2Vatu5) {
        this.b2Vatu5 = b2Vatu5;
    }

    public double getB2Cm5() {
        return b2Cm5;
    }

    public void setB2Cm5(double b2Cm5) {
        this.b2Cm5 = b2Cm5;
    }

    public double getB2Qemp() {
        return b2Qemp;
    }

    public void setB2Qemp(double b2Qemp) {
        this.b2Qemp = b2Qemp;
    }

    public double getB2Qempn() {
        return b2Qempn;
    }

    public void setB2Qempn(double b2Qempn) {
        this.b2Qempn = b2Qempn;
    }

    public double getB2Qtsegum() {
        return b2Qtsegum;
    }

    public void setB2Qtsegum(double b2Qtsegum) {
        this.b2Qtsegum = b2Qtsegum;
    }

    public String getB2Usai() {
        return b2Usai;
    }

    public void setB2Usai(String b2Usai) {
        this.b2Usai = b2Usai;
    }

    public double getB2Reserva() {
        return b2Reserva;
    }

    public void setB2Reserva(double b2Reserva) {
        this.b2Reserva = b2Reserva;
    }

    public double getB2Qpedven() {
        return b2Qpedven;
    }

    public void setB2Qpedven(double b2Qpedven) {
        this.b2Qpedven = b2Qpedven;
    }

    public String getB2Localiz() {
        return b2Localiz;
    }

    public void setB2Localiz(String b2Localiz) {
        this.b2Localiz = b2Localiz;
    }

    public double getB2Naoclas() {
        return b2Naoclas;
    }

    public void setB2Naoclas(double b2Naoclas) {
        this.b2Naoclas = b2Naoclas;
    }

    public double getB2Salpedi() {
        return b2Salpedi;
    }

    public void setB2Salpedi(double b2Salpedi) {
        this.b2Salpedi = b2Salpedi;
    }

    public String getB2Dinvent() {
        return b2Dinvent;
    }

    public void setB2Dinvent(String b2Dinvent) {
        this.b2Dinvent = b2Dinvent;
    }

    public String getB2Dinvfim() {
        return b2Dinvfim;
    }

    public void setB2Dinvfim(String b2Dinvfim) {
        this.b2Dinvfim = b2Dinvfim;
    }

    public double getB2Qtnp() {
        return b2Qtnp;
    }

    public void setB2Qtnp(double b2Qtnp) {
        this.b2Qtnp = b2Qtnp;
    }

    public double getB2Qnpt() {
        return b2Qnpt;
    }

    public void setB2Qnpt(double b2Qnpt) {
        this.b2Qnpt = b2Qnpt;
    }

    public double getB2Qter() {
        return b2Qter;
    }

    public void setB2Qter(double b2Qter) {
        this.b2Qter = b2Qter;
    }

    public double getB2Qfim2() {
        return b2Qfim2;
    }

    public void setB2Qfim2(double b2Qfim2) {
        this.b2Qfim2 = b2Qfim2;
    }

    public double getB2Qaclass() {
        return b2Qaclass;
    }

    public void setB2Qaclass(double b2Qaclass) {
        this.b2Qaclass = b2Qaclass;
    }

    public String getB2Dtinv() {
        return b2Dtinv;
    }

    public void setB2Dtinv(String b2Dtinv) {
        this.b2Dtinv = b2Dtinv;
    }

    public double getB2Cmff1() {
        return b2Cmff1;
    }

    public void setB2Cmff1(double b2Cmff1) {
        this.b2Cmff1 = b2Cmff1;
    }

    public double getB2Cmff2() {
        return b2Cmff2;
    }

    public void setB2Cmff2(double b2Cmff2) {
        this.b2Cmff2 = b2Cmff2;
    }

    public double getB2Cmff3() {
        return b2Cmff3;
    }

    public void setB2Cmff3(double b2Cmff3) {
        this.b2Cmff3 = b2Cmff3;
    }

    public double getB2Cmff4() {
        return b2Cmff4;
    }

    public void setB2Cmff4(double b2Cmff4) {
        this.b2Cmff4 = b2Cmff4;
    }

    public double getB2Cmff5() {
        return b2Cmff5;
    }

    public void setB2Cmff5(double b2Cmff5) {
        this.b2Cmff5 = b2Cmff5;
    }

    public double getB2Vfimff1() {
        return b2Vfimff1;
    }

    public void setB2Vfimff1(double b2Vfimff1) {
        this.b2Vfimff1 = b2Vfimff1;
    }

    public double getB2Vfimff2() {
        return b2Vfimff2;
    }

    public void setB2Vfimff2(double b2Vfimff2) {
        this.b2Vfimff2 = b2Vfimff2;
    }

    public double getB2Vfimff3() {
        return b2Vfimff3;
    }

    public void setB2Vfimff3(double b2Vfimff3) {
        this.b2Vfimff3 = b2Vfimff3;
    }

    public double getB2Vfimff4() {
        return b2Vfimff4;
    }

    public void setB2Vfimff4(double b2Vfimff4) {
        this.b2Vfimff4 = b2Vfimff4;
    }

    public double getB2Vfimff5() {
        return b2Vfimff5;
    }

    public void setB2Vfimff5(double b2Vfimff5) {
        this.b2Vfimff5 = b2Vfimff5;
    }

    public double getB2Qempsa() {
        return b2Qempsa;
    }

    public void setB2Qempsa(double b2Qempsa) {
        this.b2Qempsa = b2Qempsa;
    }

    public double getB2Qemppre() {
        return b2Qemppre;
    }

    public void setB2Qemppre(double b2Qemppre) {
        this.b2Qemppre = b2Qemppre;
    }

    public double getB2Salppre() {
        return b2Salppre;
    }

    public void setB2Salppre(double b2Salppre) {
        this.b2Salppre = b2Salppre;
    }

    public double getB2Qemp2() {
        return b2Qemp2;
    }

    public void setB2Qemp2(double b2Qemp2) {
        this.b2Qemp2 = b2Qemp2;
    }

    public double getB2Qempn2() {
        return b2Qempn2;
    }

    public void setB2Qempn2(double b2Qempn2) {
        this.b2Qempn2 = b2Qempn2;
    }

    public double getB2Reserv2() {
        return b2Reserv2;
    }

    public void setB2Reserv2(double b2Reserv2) {
        this.b2Reserv2 = b2Reserv2;
    }

    public double getB2Qpedve2() {
        return b2Qpedve2;
    }

    public void setB2Qpedve2(double b2Qpedve2) {
        this.b2Qpedve2 = b2Qpedve2;
    }

    public double getB2Qepre2() {
        return b2Qepre2;
    }

    public void setB2Qepre2(double b2Qepre2) {
        this.b2Qepre2 = b2Qepre2;
    }

    public double getB2Qfimff() {
        return b2Qfimff;
    }

    public void setB2Qfimff(double b2Qfimff) {
        this.b2Qfimff = b2Qfimff;
    }

    public double getB2Salped2() {
        return b2Salped2;
    }

    public void setB2Salped2(double b2Salped2) {
        this.b2Salped2 = b2Salped2;
    }

    public double getB2Qempprj() {
        return b2Qempprj;
    }

    public void setB2Qempprj(double b2Qempprj) {
        this.b2Qempprj = b2Qempprj;
    }

    public double getB2Qemppr2() {
        return b2Qemppr2;
    }

    public void setB2Qemppr2(double b2Qemppr2) {
        this.b2Qemppr2 = b2Qemppr2;
    }

    public String getB2Status() {
        return b2Status;
    }

    public void setB2Status(String b2Status) {
        this.b2Status = b2Status;
    }

    public double getB2Cmfim1() {
        return b2Cmfim1;
    }

    public void setB2Cmfim1(double b2Cmfim1) {
        this.b2Cmfim1 = b2Cmfim1;
    }

    public double getB2Cmfim2() {
        return b2Cmfim2;
    }

    public void setB2Cmfim2(double b2Cmfim2) {
        this.b2Cmfim2 = b2Cmfim2;
    }

    public double getB2Cmfim3() {
        return b2Cmfim3;
    }

    public void setB2Cmfim3(double b2Cmfim3) {
        this.b2Cmfim3 = b2Cmfim3;
    }

    public double getB2Cmfim4() {
        return b2Cmfim4;
    }

    public void setB2Cmfim4(double b2Cmfim4) {
        this.b2Cmfim4 = b2Cmfim4;
    }

    public double getB2Cmfim5() {
        return b2Cmfim5;
    }

    public void setB2Cmfim5(double b2Cmfim5) {
        this.b2Cmfim5 = b2Cmfim5;
    }

    public String getB2Tipo() {
        return b2Tipo;
    }

    public void setB2Tipo(String b2Tipo) {
        this.b2Tipo = b2Tipo;
    }

    public double getB2Cmrp1() {
        return b2Cmrp1;
    }

    public void setB2Cmrp1(double b2Cmrp1) {
        this.b2Cmrp1 = b2Cmrp1;
    }

    public double getB2Vfrp1() {
        return b2Vfrp1;
    }

    public void setB2Vfrp1(double b2Vfrp1) {
        this.b2Vfrp1 = b2Vfrp1;
    }

    public double getB2Cmrp2() {
        return b2Cmrp2;
    }

    public void setB2Cmrp2(double b2Cmrp2) {
        this.b2Cmrp2 = b2Cmrp2;
    }

    public double getB2Vfrp2() {
        return b2Vfrp2;
    }

    public void setB2Vfrp2(double b2Vfrp2) {
        this.b2Vfrp2 = b2Vfrp2;
    }

    public double getB2Cmrp3() {
        return b2Cmrp3;
    }

    public void setB2Cmrp3(double b2Cmrp3) {
        this.b2Cmrp3 = b2Cmrp3;
    }

    public double getB2Vfrp3() {
        return b2Vfrp3;
    }

    public void setB2Vfrp3(double b2Vfrp3) {
        this.b2Vfrp3 = b2Vfrp3;
    }

    public double getB2Cmrp4() {
        return b2Cmrp4;
    }

    public void setB2Cmrp4(double b2Cmrp4) {
        this.b2Cmrp4 = b2Cmrp4;
    }

    public double getB2Vfrp4() {
        return b2Vfrp4;
    }

    public void setB2Vfrp4(double b2Vfrp4) {
        this.b2Vfrp4 = b2Vfrp4;
    }

    public double getB2Cmrp5() {
        return b2Cmrp5;
    }

    public void setB2Cmrp5(double b2Cmrp5) {
        this.b2Cmrp5 = b2Cmrp5;
    }

    public double getB2Vfrp5() {
        return b2Vfrp5;
    }

    public void setB2Vfrp5(double b2Vfrp5) {
        this.b2Vfrp5 = b2Vfrp5;
    }

    public double getB2Qult() {
        return b2Qult;
    }

    public void setB2Qult(double b2Qult) {
        this.b2Qult = b2Qult;
    }

    public String getB2Dult() {
        return b2Dult;
    }

    public void setB2Dult(String b2Dult) {
        this.b2Dult = b2Dult;
    }

    public String getB2Msexp() {
        return b2Msexp;
    }

    public void setB2Msexp(String b2Msexp) {
        this.b2Msexp = b2Msexp;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public int getRECDEL() {
        return rECDEL;
    }

    public void setRECDEL(int rECDEL) {
        this.rECDEL = rECDEL;
    }

    public String getB2Bloquei() {
        return b2Bloquei;
    }

    public void setB2Bloquei(String b2Bloquei) {
        this.b2Bloquei = b2Bloquei;
    }

    public double getB2Ecsaldo() {
        return b2Ecsaldo;
    }

    public void setB2Ecsaldo(double b2Ecsaldo) {
        this.b2Ecsaldo = b2Ecsaldo;
    }

    public String getB2Xdtfin() {
        return b2Xdtfin;
    }

    public void setB2Xdtfin(String b2Xdtfin) {
        this.b2Xdtfin = b2Xdtfin;
    }

    public String getB2Xdtini() {
        return b2Xdtini;
    }

    public void setB2Xdtini(String b2Xdtini) {
        this.b2Xdtini = b2Xdtini;
    }

//    public String getB2Hmov() {
//        return b2Hmov;
//    }
//
//    public void setB2Hmov(String b2Hmov) {
//        this.b2Hmov = b2Hmov;
//    }

//    public String getB2Hult() {
//        return b2Hult;
//    }
//
//    public void setB2Hult(String b2Hult) {
//        this.b2Hult = b2Hult;
//    }

//    public String getB2Dmov() {
//        return b2Dmov;
//    }
//
//    public void setB2Dmov(String b2Dmov) {
//        this.b2Dmov = b2Dmov;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sb2010)) {
            return false;
        }
        Sb2010 other = (Sb2010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gerador.Sb2010[ rECNO=" + rECNO + " ]";
    }
    
}
