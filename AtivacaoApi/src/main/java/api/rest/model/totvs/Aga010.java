/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 *
 * @author Robson PC
 */
@Entity
@Table(name = "AGA010", schema = "dbo")
//@NamedQueries({@NamedQuery(name = "Aga010.findAll", query = "SELECT a FROM Aga010 a")})
@Where(clause = "D_E_L_E_T_ = ''")
public class Aga010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "AGA_FILIAL")
    private String agaFilial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "AGA_CODIGO")
    private String agaCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AGA_ENTIDA")
    private String agaEntida;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AGA_CODENT")
    private String agaCodent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGA_TIPO")
    private String agaTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGA_PADRAO")
    private String agaPadrao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "AGA_END")
    private String agaEnd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "AGA_CEP")
    private String agaCep;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "AGA_BAIRRO")
    private String agaBairro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "AGA_MUNDES")
    private String agaMundes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "AGA_EST")
    private String agaEst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "AGA_MUN")
    private String agaMun;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "AGA_PAIS")
    private String agaPais;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "AGA_COMP")
    private String agaComp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "D_E_L_E_T_")
    private String dELET;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "R_E_C_N_O_")
    private Integer rECNO;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AGA_UTIPO2")
    private String agaUtipo2;   
    @OneToMany(mappedBy = "aga010", fetch = FetchType.LAZY)    
    private List<Ada010> ada010s;
    @OneToMany(mappedBy = "aga010", fetch = FetchType.LAZY)    
    private List<Adb010> adb010s;

    public Aga010() {
    }

    public Aga010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public Aga010(Integer rECNO, String agaFilial, String agaCodigo, String agaEntida, String agaCodent, String agaTipo, String agaPadrao, String agaEnd, String agaCep, String agaBairro, String agaMundes, String agaEst, String agaMun, String agaPais, String agaComp, String dELET, String agaUtipo2) {
        this.rECNO = rECNO;
        this.agaFilial = agaFilial;
        this.agaCodigo = agaCodigo;
        this.agaEntida = agaEntida;
        this.agaCodent = agaCodent;
        this.agaTipo = agaTipo;
        this.agaPadrao = agaPadrao;
        this.agaEnd = agaEnd;
        this.agaCep = agaCep;
        this.agaBairro = agaBairro;
        this.agaMundes = agaMundes;
        this.agaEst = agaEst;
        this.agaMun = agaMun;
        this.agaPais = agaPais;
        this.agaComp = agaComp;
        this.dELET = dELET;
        this.agaUtipo2 = agaUtipo2;
    }

    public String getAgaFilial() {
        return agaFilial;
    }

    public void setAgaFilial(String agaFilial) {
        this.agaFilial = agaFilial;
    }

    public String getAgaCodigo() {
        return agaCodigo;
    }

    public void setAgaCodigo(String agaCodigo) {
        this.agaCodigo = agaCodigo;
    }

    public String getAgaEntida() {
        return agaEntida;
    }

    public void setAgaEntida(String agaEntida) {
        this.agaEntida = agaEntida;
    }

    public String getAgaCodent() {
        return agaCodent;
    }

    public void setAgaCodent(String agaCodent) {
        this.agaCodent = agaCodent;
    }

    public String getAgaTipo() {
        return agaTipo;
    }

    public void setAgaTipo(String agaTipo) {
        this.agaTipo = agaTipo;
    }

    public String getAgaPadrao() {
        return agaPadrao;
    }

    public void setAgaPadrao(String agaPadrao) {
        this.agaPadrao = agaPadrao;
    }

    public String getAgaEnd() {
        return agaEnd;
    }

    public void setAgaEnd(String agaEnd) {
        this.agaEnd = agaEnd;
    }

    public String getAgaCep() {
        return agaCep;
    }

    public void setAgaCep(String agaCep) {
        this.agaCep = agaCep;
    }

    public String getAgaBairro() {
        return agaBairro;
    }

    public void setAgaBairro(String agaBairro) {
        this.agaBairro = agaBairro;
    }

    public String getAgaMundes() {
        return agaMundes;
    }

    public void setAgaMundes(String agaMundes) {
        this.agaMundes = agaMundes;
    }

    public String getAgaEst() {
        return agaEst;
    }

    public void setAgaEst(String agaEst) {
        this.agaEst = agaEst;
    }

    public String getAgaMun() {
        return agaMun;
    }

    public void setAgaMun(String agaMun) {
        this.agaMun = agaMun;
    }

    public String getAgaPais() {
        return agaPais;
    }

    public void setAgaPais(String agaPais) {
        this.agaPais = agaPais;
    }

    public String getAgaComp() {
        return agaComp;
    }

    public void setAgaComp(String agaComp) {
        this.agaComp = agaComp;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

    public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getAgaUtipo2() {
        return agaUtipo2;
    }

    public void setAgaUtipo2(String agaUtipo2) {
        this.agaUtipo2 = agaUtipo2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aga010)) {
            return false;
        }
        Aga010 other = (Aga010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.liguetelecom.model.totvs.Aga010[ rECNO=" + rECNO + " ]";
    }
}
