/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.totvs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBSON
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
@Table(schema = "dbo", name = "SA1010")
public class Sa1010 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "A1_FILIAL")
    @NotNull
    @Size(min = 1, max = 4)
    private String a1Filial;
    @Column(name = "A1_COD")
    @NotNull
    @Size(min = 1, max = 9)
    private String a1Cod;
    @Column(name = "A1_LOJA")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Loja;
    @Column(name = "A1_PESSOA")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Pessoa;
    @Column(name = "A1_CGC")
    @NotNull
    @Size(min = 1, max = 14)
    private String a1Cgc;
    @Column(name = "A1_NOME")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Nome;
    @Column(name = "A1_NREDUZ")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Nreduz;
    @Column(name = "A1_END")
    @NotNull
    @Size(min = 1, max = 60)
    private String a1End;
    @Column(name = "A1_CEP")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Cep;
    @Column(name = "A1_EST")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Est;
    @Column(name = "A1_ESTADO")
    @NotNull
    @Size(min = 1, max = 20)
    private String a1Estado;
    @Column(name = "A1_COD_MUN")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1CodMun;
    @Column(name = "A1_MUN")
    @NotNull
    @Size(min = 1, max = 60)
    private String a1Mun;
    @Column(name = "A1_BAIRRO")
    @NotNull
    @Size(min = 1, max = 30)
    private String a1Bairro;
    @Column(name = "A1_NATUREZ")
    @NotNull
    @Size(min = 1, max = 10)
    private String a1Naturez;
    @Column(name = "A1_IBGE")
    @NotNull
    @Size(min = 1, max = 11)
    private String a1Ibge;
    @Column(name = "A1_DDI")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Ddi;
    @Column(name = "A1_DDD")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Ddd;
    @Column(name = "A1_UDDDCEL")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1uDddCel;
    @Column(name = "A1_TEL")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Tel;
    @Column(name = "A1_TELEX")
    @NotNull
    @Size(min = 1, max = 10)
    private String a1Telex;
    @Column(name = "A1_INSCR")
    @NotNull
    @Size(min = 1, max = 18)
    private String a1Inscr;
    @Column(name = "A1_PFISICA")
    @NotNull
    @Size(min = 1, max = 18)
    private String a1Pfisica;
    @Column(name = "A1_PAIS")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Pais;
    @Column(name = "A1_FAX")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Fax;
    @Column(name = "A1_TIPO")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tipo;
    @Column(name = "A1_ENDCOB")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Endcob;
    @Column(name = "A1_TRIBFAV")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tribfav;
    @Column(name = "A1_ENDREC")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Endrec;
    @Column(name = "A1_ENDENT")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Endent;
    @Column(name = "A1_CONTATO")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Contato;
    @Column(name = "A1_INSCRM")
    @NotNull
    @Size(min = 1, max = 18)
    private String a1Inscrm;
    @Column(name = "A1_VEND")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Vend;
    @Column(name = "A1_COMIS")
    @NotNull
    private double a1Comis;
    @Column(name = "A1_REGIAO")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Regiao;
    @Column(name = "A1_CONTA")
    @NotNull
    @Size(min = 1, max = 20)
    private String a1Conta;
    @Column(name = "A1_BCO1")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Bco1;
    @Column(name = "A1_BCO2")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Bco2;
    @Column(name = "A1_BCO3")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Bco3;
    @Column(name = "A1_BCO4")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Bco4;
    @Column(name = "A1_BCO5")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Bco5;
    @Column(name = "A1_TRANSP")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Transp;
    @Column(name = "A1_TPFRET")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tpfret;
    @Column(name = "A1_COND")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Cond;
    @Column(name = "A1_DESC")
    @NotNull
    private double a1Desc;
    @Column(name = "A1_PRIOR")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Prior;
    @Column(name = "A1_RISCO")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Risco;
    @Column(name = "A1_LC")
    @NotNull
    private double a1Lc;
    @Column(name = "A1_VENCLC")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Venclc;
    @Column(name = "A1_CLASSE")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Classe;
    @Column(name = "A1_LCFIN")
    @NotNull
    private double a1Lcfin;
    @Column(name = "A1_MOEDALC")
    @NotNull
    private double a1Moedalc;
    @Column(name = "A1_MSALDO")
    @NotNull
    private double a1Msaldo;
    @Column(name = "A1_MCOMPRA")
    @NotNull
    private double a1Mcompra;
    @Column(name = "A1_METR")
    @NotNull
    private double a1Metr;
    @Column(name = "A1_PRICOM")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Pricom;
    @Column(name = "A1_ULTCOM")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Ultcom;
    @Column(name = "A1_NROCOM")
    @NotNull
    private double a1Nrocom;
    @Column(name = "A1_FORMVIS")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Formvis;
    @Column(name = "A1_TEMVIS")
    @NotNull
    private double a1Temvis;
    @Column(name = "A1_ULTVIS")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Ultvis;
    @Column(name = "A1_TMPVIS")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1Tmpvis;
    @Column(name = "A1_CLASVEN")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Clasven;
    @Column(name = "A1_TMPSTD")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1Tmpstd;
    @Column(name = "A1_MENSAGE")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Mensage;
    @Column(name = "A1_SALDUP")
    @NotNull
    private double a1Saldup;
    @Column(name = "A1_RECISS")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Reciss;
    @Column(name = "A1_NROPAG")
    @NotNull
    private double a1Nropag;
    @Column(name = "A1_SALPEDL")
    @NotNull
    private double a1Salpedl;
    @Column(name = "A1_TRANSF")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Transf;
    @Column(name = "A1_SUFRAMA")
    @NotNull
    @Size(min = 1, max = 12)
    private String a1Suframa;
    @Column(name = "A1_ATR")
    @NotNull
    private double a1Atr;
    @Column(name = "A1_VACUM")
    @NotNull
    private double a1Vacum;
    @Column(name = "A1_SALPED")
    @NotNull
    private double a1Salped;
    @Column(name = "A1_TITPROT")
    @NotNull
    private double a1Titprot;
    @Column(name = "A1_CHQDEVO")
    @NotNull
    private double a1Chqdevo;
    @Column(name = "A1_DTULTIT")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Dtultit;
    @Column(name = "A1_MATR")
    @NotNull
    private double a1Matr;
    @Column(name = "A1_DTULCHQ")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Dtulchq;
    @Column(name = "A1_MAIDUPL")
    @NotNull
    private double a1Maidupl;
    @Column(name = "A1_TABELA")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Tabela;
    @Column(name = "A1_INCISS")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Inciss;
    @Column(name = "A1_SALDUPM")
    @NotNull
    private double a1Saldupm;
    @Column(name = "A1_PAGATR")
    @NotNull
    private double a1Pagatr;
    @Column(name = "A1_CXPOSTA")
    @NotNull
    @Size(min = 1, max = 20)
    private String a1Cxposta;
    @Column(name = "A1_ATIVIDA")
    @NotNull
    @Size(min = 1, max = 7)
    private String a1Ativida;
    @Column(name = "A1_CARGO1")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Cargo1;
    @Column(name = "A1_CARGO2")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Cargo2;
    @Column(name = "A1_CARGO3")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Cargo3;
    @Column(name = "A1_RTEC")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Rtec;
    @Column(name = "A1_SUPER")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Super;
    @Column(name = "A1_ALIQIR")
    @NotNull
    private double a1Aliqir;
    @Column(name = "A1_OBSERV")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Observ;
    @Column(name = "A1_RG")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Rg;
    @Column(name = "A1_CALCSUF")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Calcsuf;
    @Column(name = "A1_DTNASC")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Dtnasc;
    @Column(name = "A1_SALPEDB")
    @NotNull
    private double a1Salpedb;
    @Column(name = "A1_CLIFAT")
    @NotNull
    @Size(min = 1, max = 9)
    private String a1Clifat;
    @Column(name = "A1_GRPTRIB")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Grptrib;
    @Column(name = "A1_BAIRROC")
    @NotNull
    @Size(min = 1, max = 30)
    private String a1Bairroc;
    @Column(name = "A1_CEPC")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Cepc;
    @Column(name = "A1_MUNC")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Munc;
    @Column(name = "A1_ESTC")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Estc;
    @Column(name = "A1_CEPE")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Cepe;
    @Column(name = "A1_BAIRROE")
    @NotNull
    @Size(min = 1, max = 20)
    private String a1Bairroe;
    @Column(name = "A1_MUNE")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Mune;
    @Column(name = "A1_ESTE")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Este;
    @Column(name = "A1_SATIV1")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ1;
    @Column(name = "A1_SATIV2")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ2;
    @Column(name = "A1_TPISSRS")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Tpissrs;
    @Column(name = "A1_CODLOC")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Codloc;
    @Column(name = "A1_TPESSOA")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Tpessoa;
    @Column(name = "A1_CODPAIS")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1Codpais;
    @Column(name = "A1_SATIV3")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ3;
    @Column(name = "A1_SATIV4")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ4;
    @Column(name = "A1_SATIV5")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ5;
    @Column(name = "A1_SATIV6")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ6;
    @Column(name = "A1_SATIV7")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ7;
    @Column(name = "A1_SATIV8")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Sativ8;
    @Column(name = "A1_CODMARC")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Codmarc;
    @Column(name = "A1_CODAGE")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Codage;
    @Column(name = "A1_COMAGE")
    @NotNull
    private double a1Comage;
    @Column(name = "A1_TIPCLI")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tipcli;
    @Column(name = "A1_DEST_1")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Dest1;
    @Column(name = "A1_EMAIL")
    @NotNull
    @Size(min = 1, max = 50)
    private String a1Email;
    @Column(name = "A1_DEST_2")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Dest2;
    @Column(name = "A1_CODMUN")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1Codmun;
    @Column(name = "A1_DEST_3")
    @NotNull
    @Size(min = 1, max = 3)
    private String a1Dest3;
    @Column(name = "A1_HPAGE")
    @NotNull
    @Size(min = 1, max = 30)
    private String a1Hpage;
    @Column(name = "A1_CBO")
    @NotNull
    @Size(min = 1, max = 7)
    private String a1Cbo;
    @Column(name = "A1_CNAE")
    @NotNull
    @Size(min = 1, max = 9)
    private String a1Cnae;
    @Column(name = "A1_CONDPAG")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1Condpag;
    @Column(name = "A1_DIASPAG")
    @NotNull
    private double a1Diaspag;
    @Column(name = "A1_OBS")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Obs;
    @Column(name = "A1_AGREG")
    @NotNull
    @Size(min = 1, max = 4)
    private String a1Agreg;
    @Column(name = "A1_CODHIST")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Codhist;
    @Column(name = "A1_RECINSS")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Recinss;
    @Column(name = "A1_RECCOFI")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Reccofi;
    @Column(name = "A1_RECCSLL")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Reccsll;
    @Column(name = "A1_RECPIS")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Recpis;
    @Column(name = "A1_TIPPER")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Tipper;
    @Column(name = "A1_SALFIN")
    @NotNull
    private double a1Salfin;
    @Column(name = "A1_SALFINM")
    @NotNull
    private double a1Salfinm;
    @Column(name = "A1_CONTAB")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Contab;
    @Column(name = "A1_B2B")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1B2b;
    @Column(name = "A1_GRPVEN")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Grpven;
    @Column(name = "A1_CLICNV")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Clicnv;
    @Column(name = "A1_INSCRUR")
    @NotNull
    @Size(min = 1, max = 18)
    private String a1Inscrur;
    @Column(name = "A1_MSBLQL")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Msblql;
    @Column(name = "A1_SITUA")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Situa;
    @Column(name = "A1_NUMRA")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Numra;
    @Column(name = "A1_SUBCOD")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Subcod;
    @Column(name = "A1_CDRDES")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Cdrdes;
    @Column(name = "A1_FILDEB")
    @NotNull
    @Size(min = 1, max = 4)
    private String a1Fildeb;
    @Column(name = "A1_CODFOR")
    @NotNull
    @Size(min = 1, max = 15)
    private String a1Codfor;
    @Column(name = "A1_ABICS")
    @NotNull
    @Size(min = 1, max = 4)
    private String a1Abics;
    @Column(name = "A1_BLEMAIL")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Blemail;
    @Column(name = "A1_TIPOCLI")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Tipocli;
    @Column(name = "A1_VINCULO")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Vinculo;
    @Column(name = "A1_DTINIV")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Dtiniv;
    @Column(name = "A1_DTFIMV")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Dtfimv;
    @Column(name = "A1_LOCCONS")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Loccons;
    @Column(name = "A1_CBAIRRE")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Cbairre;
    @Column(name = "A1_CODMUNE")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1Codmune;
    @Column(name = "A1_PERFIL")
    @NotNull
    private double a1Perfil;
    @Column(name = "A1_HRTRANS")
    @NotNull
    @Size(min = 1, max = 5)
    private String a1Hrtrans;
    @Column(name = "A1_UNIDVEN")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Unidven;
    @Column(name = "A1_TIPPRFL")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tipprfl;
    @Column(name = "A1_PRF_VLD")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1PrfVld;
    @Column(name = "A1_PRF_COD")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1PrfCod;
    @Lob
    @Column(name = "A1_PRF_OBS")
    private byte[] a1PrfObs;
    @Column(name = "A1_REGPB")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Regpb;
    @Column(name = "A1_USADDA")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Usadda;
    @Column(name = "A1_SIMPLES")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Simples;
    @Column(name = "A1_CTARE")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Ctare;
    @Column(name = "A1_FRETISS")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Fretiss;
    @Column(name = "A1_CODSIAF")
    @NotNull
    @Size(min = 1, max = 4)
    private String a1Codsiaf;
    @Column(name = "A1_ENDNOT")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Endnot;
    @Column(name = "A1_CEINSS")
    @NotNull
    @Size(min = 1, max = 14)
    private String a1Ceinss;
    @Column(name = "A1_REGESIM")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Regesim;
    @Column(name = "A1_PERCATM")
    @NotNull
    private double a1Percatm;
    @Column(name = "A1_IPWEB")
    @NotNull
    @Size(min = 1, max = 20)
    private String a1Ipweb;
    @Column(name = "A1_IDHIST")
    @NotNull
    @Size(min = 1, max = 20)
    private String a1Idhist;
    @Column(name = "A1_INDRET")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Indret;
    @Column(name = "A1_NIF")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Nif;
    @Column(name = "A1_IRBAX")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Irbax;
    @Column(name = "A1_ABATIMP")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Abatimp;
    @Column(name = "A1_CONTRIB")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Contrib;
    @Column(name = "A1_TDA")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tda;
    @Column(name = "A1_COMPLEM")
    @NotNull
    @Size(min = 1, max = 50)
    private String a1Complem;
    @Column(name = "A1_TIMEKEE")
    @NotNull
    @Size(min = 1, max = 8)
    private String a1Timekee;
    @Column(name = "A1_RECIRRF")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Recirrf;
    @Column(name = "A1_ORIGEM")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Origem;
    @Column(name = "A1_FOMEZER")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Fomezer;
    @Column(name = "A1_RECFET")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Recfet;
    @Column(name = "A1_INCULT")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Incult;
    @Column(name = "A1_MINIRF")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Minirf;
    @Column(name = "A1_FILTRF")
    @NotNull
    @Size(min = 1, max = 4)
    private String a1Filtrf;
    @Column(name = "A1_MATFUN")
    @NotNull
    @Size(min = 1, max = 6)
    private String a1Matfun;
    @Column(name = "A1_OUTRMUN")
    @NotNull
    @Size(min = 1, max = 10)
    private String a1Outrmun;
    @Column(name = "A1_CODFID")
    @NotNull
    @Size(min = 1, max = 40)
    private String a1Codfid;
    @Column(name = "A1_SIMPNAC")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Simpnac;
    @Column(name = "A1_TPNFSE")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Tpnfse;
    @Column(name = "A1_ALIFIXA")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Alifixa;
    @Column(name = "A1_CRDMA")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Crdma;
    @Column(name = "A1_PRSTSER")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Prstser;
    @Column(name = "A1_RFACS")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Rfacs;
    @Column(name = "A1_RFABOV")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Rfabov;
    @Column(name = "A1_PERFECP")
    @NotNull
    private double a1Perfecp;
    @Column(name = "A1_IENCONT")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Iencont;
    @Column(name = "A1_TPDP")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tpdp;
    @Column(name = "A1_ENTID")
    @NotNull
    @Size(min = 1, max = 2)
    private String a1Entid;
    @Column(name = "A1_TPJ")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Tpj;
    @Column(name = "D_E_L_E_T_")
    @NotNull
    @Size(min = 1, max = 1)
    private String dELET;
    @Id
    @Column(name = "R_E_C_N_O_")
    @NotNull
    private Integer rECNO;
//    @NotNull
//    private int rECDEL;
    @Column(name = "A1_UNRCOB")
    @NotNull
    @Size(min = 1, max = 10)
    private String a1Unrcob;
    @Column(name = "A1_MSFIL")
    @NotNull
    @Size(min = 1, max = 4)
    private String a1Msfil;
    @Column(name = "A1_INOVAUT")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Inovaut;
    @Column(name = "A1_UDETFAT")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Udetfat;
    @Column(name = "A1_UENVNF")
    @NotNull
    @Size(min = 1, max = 1)
    private String a1Uenvnf;
    @Column(name = "A1_CEL")
    @NotNull
    @Size(min = 1, max = 10)
    private String a1Cel;
    @Column(name = "A1_ULOGIN")
    @Size(max = 255)
    private String a1Ulogin;
    @Column(name = "A1_USENHA")
    @Size(max = 255)
    private String a1Usenha;
    @OneToMany(mappedBy = "sa1010")
    @XmlElementWrapper
    @JsonManagedReference(value = "Cliente")
    private List<Ada010> ada010s = new ArrayList<Ada010>();
    @OneToMany(mappedBy = "sa1010", fetch = FetchType.LAZY)
    @XmlElementWrapper
    @JsonManagedReference(value = "se1010")
    private List<Se1010> se1010s = new ArrayList<Se1010>();
    @OneToMany(mappedBy = "sa1", fetch = FetchType.LAZY)
    @XmlElementWrapper
    @JsonManagedReference(value = "sf2010")
    private List<Sf2010> sf2010s;
    @OneToMany(mappedBy = "sa1010", fetch = FetchType.LAZY)
    @XmlElementWrapper
    @JsonManagedReference(value = "SAAB")
    private List<Ab1010> ab1010s;
    @OneToMany(mappedBy = "sa1", fetch = FetchType.LAZY)
    @XmlElementWrapper
    @JsonManagedReference(value = "Aa3010cli")
    private List<Aa3010> aa3010s;
    @OneToMany(mappedBy = "sa1", fetch = FetchType.LAZY)
    private List<Ab6010> ab6010s;

    public Sa1010() {
    }

    public Sa1010(Integer rECNO) {
        this.rECNO = rECNO;
    }

    @JsonCreator
    public Sa1010(@JsonProperty("a1Filial") String a1Filial, @JsonProperty("a1Cod") String a1Cod, @JsonProperty("a1Loja") String a1Loja, @JsonProperty("a1Pessoa") String a1Pessoa, @JsonProperty("a1Cgc") String a1Cgc, @JsonProperty("a1Nome") String a1Nome, @JsonProperty("a1Nreduz") String a1Nreduz, @JsonProperty("a1End") String a1End, @JsonProperty("a1Cep") String a1Cep, @JsonProperty("a1Est") String a1Est, @JsonProperty("a1Estado") String a1Estado, @JsonProperty("a1CodMun") String a1CodMun, @JsonProperty("a1Mun") String a1Mun, @JsonProperty("a1Bairro") String a1Bairro, @JsonProperty("a1Naturez") String a1Naturez, @JsonProperty("a1Ibge") String a1Ibge, @JsonProperty("a1Dd") String a1Ddi, @JsonProperty("a1Ddd") String a1Ddd,@JsonProperty("a1uDddCel") String a1uDddCel, @JsonProperty("a1Tel") String a1Tel, @JsonProperty("a1Telex") String a1Telex, @JsonProperty("a1Inscr") String a1Inscr, @JsonProperty("a1Pfisica") String a1Pfisica, @JsonProperty("a1Pais") String a1Pais, @JsonProperty("a1Fax") String a1Fax, @JsonProperty("a1Tipo") String a1Tipo, @JsonProperty("a1Endcob") String a1Endcob, @JsonProperty("a1Tribfav") String a1Tribfav, @JsonProperty("a1Endrec") String a1Endrec, @JsonProperty("a1Endent") String a1Endent, @JsonProperty("a1Contato") String a1Contato, @JsonProperty("a1Inscrm") String a1Inscrm, @JsonProperty("a1Vend") String a1Vend, @JsonProperty("a1Comis") double a1Comis, @JsonProperty("a1Regiao") String a1Regiao, @JsonProperty("a1Conta") String a1Conta, @JsonProperty("a1Bco1") String a1Bco1, @JsonProperty("a1Bco2") String a1Bco2, @JsonProperty("a1Bco3") String a1Bco3, @JsonProperty("a1Bco4") String a1Bco4, @JsonProperty("a1Bco5") String a1Bco5, @JsonProperty("a1Transp") String a1Transp, @JsonProperty("a1Tpfret") String a1Tpfret, @JsonProperty("a1Cond") String a1Cond, @JsonProperty("a1Desc") double a1Desc, @JsonProperty("a1Prior") String a1Prior, @JsonProperty("a1Risco") String a1Risco, @JsonProperty("a1Lc") double a1Lc, @JsonProperty("a1Venclc") String a1Venclc, @JsonProperty("a1Classe") String a1Classe, @JsonProperty("a1Lcfin") double a1Lcfin, @JsonProperty("a1Moedalc") double a1Moedalc, @JsonProperty("a1Msaldo") double a1Msaldo, @JsonProperty("a1Mcompra") double a1Mcompra, @JsonProperty("a1Metr") double a1Metr, @JsonProperty("a1Pricom") String a1Pricom, @JsonProperty("a1Ultcom") String a1Ultcom, @JsonProperty("a1Nrocom") double a1Nrocom, @JsonProperty("a1Formvis") String a1Formvis, @JsonProperty("a1Temvis") double a1Temvis, @JsonProperty("a1Ultvis") String a1Ultvis, @JsonProperty("a1Tmpvis") String a1Tmpvis, @JsonProperty("a1Clasven") String a1Clasven, @JsonProperty("a1Tmpstd") String a1Tmpstd, @JsonProperty("a1Mensage") String a1Mensage, @JsonProperty("a1Saldup") double a1Saldup, @JsonProperty("a1Reciss") String a1Reciss, @JsonProperty("a1Nropag") double a1Nropag, @JsonProperty("a1Salpedl") double a1Salpedl, @JsonProperty("a1Transf") String a1Transf, @JsonProperty("a1Suframa") String a1Suframa, @JsonProperty("a1Atr") double a1Atr, @JsonProperty("a1Vacum") double a1Vacum, @JsonProperty("a1Salped") double a1Salped, @JsonProperty("a1Titprot") double a1Titprot, @JsonProperty("a1Chqdevo") double a1Chqdevo, @JsonProperty("a1Dtultit") String a1Dtultit, @JsonProperty("a1Matr") double a1Matr, @JsonProperty("a1Dtulchq") String a1Dtulchq, @JsonProperty("a1Maidupl") double a1Maidupl, @JsonProperty("a1Tabela") String a1Tabela, @JsonProperty("a1Inciss") String a1Inciss, @JsonProperty("a1Saldupm") double a1Saldupm, @JsonProperty("a1Pagatr") double a1Pagatr, @JsonProperty("a1Cxposta") String a1Cxposta, @JsonProperty("a1Ativida") String a1Ativida, @JsonProperty("a1Cargo1") String a1Cargo1, @JsonProperty("a1Cargo2") String a1Cargo2, @JsonProperty("a1Cargo3") String a1Cargo3, @JsonProperty("a1Rtec") String a1Rtec, @JsonProperty("a1Super") String a1Super, @JsonProperty("a1Aliqir") double a1Aliqir, @JsonProperty("a1Observ") String a1Observ, @JsonProperty("a1Rg") String a1Rg, @JsonProperty("a1Calcsuf") String a1Calcsuf, @JsonProperty("a1Dtnasc") String a1Dtnasc, @JsonProperty("a1Salpedb") double a1Salpedb, @JsonProperty("a1Clifat") String a1Clifat, @JsonProperty("a1Grptrib") String a1Grptrib, @JsonProperty("a1Bairroc") String a1Bairroc, @JsonProperty("a1Cepc") String a1Cepc, @JsonProperty("a1Munc") String a1Munc, @JsonProperty("a1Estc") String a1Estc, @JsonProperty("a1Cepe") String a1Cepe, @JsonProperty("a1Bairroe") String a1Bairroe, @JsonProperty("a1Mune") String a1Mune, @JsonProperty("a1Este") String a1Este, @JsonProperty("a1Sativ1") String a1Sativ1, @JsonProperty("a1Sativ2") String a1Sativ2, @JsonProperty("a1Tpissrs") String a1Tpissrs, @JsonProperty("a1Codloc") String a1Codloc, @JsonProperty("a1Tpessoa") String a1Tpessoa, @JsonProperty("a1Codpais") String a1Codpais, @JsonProperty("a1Sativ3") String a1Sativ3, @JsonProperty("a1Sativ4") String a1Sativ4, @JsonProperty("a1Sativ5") String a1Sativ5, @JsonProperty("a1Sativ6") String a1Sativ6, @JsonProperty("a1Sativ7") String a1Sativ7, @JsonProperty("a1Sativ8") String a1Sativ8, @JsonProperty("a1Codmarc") String a1Codmarc, @JsonProperty("a1Codage") String a1Codage, @JsonProperty("a1Comage") double a1Comage, @JsonProperty("a1Tipcli") String a1Tipcli, @JsonProperty("a1Dest1") String a1Dest1, @JsonProperty("a1Email") String a1Email, @JsonProperty("a1Dest2") String a1Dest2, @JsonProperty("a1Codmun") String a1Codmun, @JsonProperty("a1Dest3") String a1Dest3, @JsonProperty("a1Hpage") String a1Hpage, @JsonProperty("a1Cbo") String a1Cbo, @JsonProperty("a1Cnae") String a1Cnae, @JsonProperty("a1Condpag") String a1Condpag, @JsonProperty("a1Diaspag") double a1Diaspag, @JsonProperty("a1Obs") String a1Obs, @JsonProperty("a1Agreg") String a1Agreg, @JsonProperty("a1Codhist") String a1Codhist, @JsonProperty("a1Recinss") String a1Recinss, @JsonProperty("a1Reccofi") String a1Reccofi, @JsonProperty("a1Reccsll") String a1Reccsll, @JsonProperty("a1Recpis") String a1Recpis, @JsonProperty("a1Tipper") String a1Tipper, @JsonProperty("a1Salfin") double a1Salfin, @JsonProperty("a1Salfinm") double a1Salfinm, @JsonProperty("a1Contab") String a1Contab, @JsonProperty("a1B2b") String a1B2b, @JsonProperty("a1Grpven") String a1Grpven, @JsonProperty("a1Clicnv") String a1Clicnv, @JsonProperty("a1Inscrur") String a1Inscrur, @JsonProperty("a1Msblql") String a1Msblql, @JsonProperty("a1Situa") String a1Situa, @JsonProperty("a1Numra") String a1Numra, @JsonProperty("a1Subcod") String a1Subcod, @JsonProperty("a1Cdrdes") String a1Cdrdes, @JsonProperty("a1Fildeb") String a1Fildeb, @JsonProperty("a1Codfor") String a1Codfor, @JsonProperty("a1Abics") String a1Abics, @JsonProperty("a1Blemail") String a1Blemail, @JsonProperty("a1Tipocli") String a1Tipocli, @JsonProperty("a1Vinculo") String a1Vinculo, @JsonProperty("a1Dtiniv") String a1Dtiniv, @JsonProperty("a1Dtfimv") String a1Dtfimv, @JsonProperty("a1Loccons") String a1Loccons, @JsonProperty("a1Cbairre") String a1Cbairre, @JsonProperty("a1Codmune") String a1Codmune, @JsonProperty("a1Perfil") double a1Perfil, @JsonProperty("a1Hrtrans") String a1Hrtrans, @JsonProperty("a1Unidven") String a1Unidven, @JsonProperty("a1Tipprf") String a1Tipprfl, @JsonProperty("a1PrfVld") String a1PrfVld, @JsonProperty("a1PrfCod") String a1PrfCod, @JsonProperty("a1Regpb") String a1Regpb, @JsonProperty("a1Usadda") String a1Usadda, @JsonProperty("a1Simples") String a1Simples, @JsonProperty("a1Ctare") String a1Ctare, @JsonProperty("a1Fretiss") String a1Fretiss, @JsonProperty("a1Codsiaf") String a1Codsiaf, @JsonProperty("a1Endnot") String a1Endnot, @JsonProperty("a1Ceinss") String a1Ceinss, @JsonProperty("a1Regesim") String a1Regesim, @JsonProperty("a1Percatm") double a1Percatm, @JsonProperty("a1Ipweb") String a1Ipweb, @JsonProperty("a1Idhist") String a1Idhist, @JsonProperty("a1Indret") String a1Indret, @JsonProperty("a1Nif") String a1Nif, @JsonProperty("a1Irbax") String a1Irbax, @JsonProperty("a1Abatimp") String a1Abatimp, @JsonProperty("a1Contrib") String a1Contrib, @JsonProperty("a1Tda") String a1Tda, @JsonProperty("a1Complem") String a1Complem, @JsonProperty("a1Timekee") String a1Timekee, @JsonProperty("a1Recirrf") String a1Recirrf, @JsonProperty("a1Origem") String a1Origem, @JsonProperty("a1Fomezer") String a1Fomezer, @JsonProperty("a1Recfet") String a1Recfet, @JsonProperty("a1Incult") String a1Incult, @JsonProperty("a1Minirf") String a1Minirf, @JsonProperty("a1Filtrf") String a1Filtrf, @JsonProperty("a1Matfun") String a1Matfun, @JsonProperty("a1Outrmun") String a1Outrmun, @JsonProperty("a1Codfid") String a1Codfid, @JsonProperty("a1Simpnac") String a1Simpnac, @JsonProperty("a1Tpnfse") String a1Tpnfse, @JsonProperty("a1Alifixa") String a1Alifixa, @JsonProperty("a1Crdma") String a1Crdma, @JsonProperty("a1Prstser") String a1Prstser, @JsonProperty("a1Rfacs") String a1Rfacs, @JsonProperty("a1Rfabov") String a1Rfabov, @JsonProperty("a1Perfecp") double a1Perfecp, @JsonProperty("a1Iencont") String a1Iencont, @JsonProperty("a1Tpdp") String a1Tpdp, @JsonProperty("a1Entid") String a1Entid, @JsonProperty("a1Tpj") String a1Tpj, @JsonProperty("dELET") String dELET, @JsonProperty("rECDEL") int rECDEL, @JsonProperty("a1Unrcob") String a1Unrcob, @JsonProperty("a1Msfil") String a1Msfil, @JsonProperty("a1Inovaut") String a1Inovaut, @JsonProperty("a1Udetfat") String a1Udetfat, @JsonProperty("a1Uenvnf") String a1Uenvnf, @JsonProperty("a1Cel") String a1Cel) {
        this.a1Filial = a1Filial;
        this.a1Cod = a1Cod;
        this.a1Loja = a1Loja;
        this.a1Pessoa = a1Pessoa;
        this.a1Cgc = a1Cgc;
        this.a1Nome = a1Nome;
        this.a1Nreduz = a1Nreduz;
        this.a1End = a1End;
        this.a1Cep = a1Cep;
        this.a1Est = a1Est;
        this.a1Estado = a1Estado;
        this.a1CodMun = a1CodMun;
        this.a1Mun = a1Mun;
        this.a1Bairro = a1Bairro;
        this.a1Naturez = a1Naturez;
        this.a1Ibge = a1Ibge;
        this.a1Ddi = a1Ddi;
        this.a1Ddd = a1Ddd;
        this.a1uDddCel = a1uDddCel;
        this.a1Tel = a1Tel;
        this.a1Telex = a1Telex;
        this.a1Inscr = a1Inscr;
        this.a1Pfisica = a1Pfisica;
        this.a1Pais = a1Pais;
        this.a1Fax = a1Fax;
        this.a1Tipo = a1Tipo;
        this.a1Endcob = a1Endcob;
        this.a1Tribfav = a1Tribfav;
        this.a1Endrec = a1Endrec;
        this.a1Endent = a1Endent;
        this.a1Contato = a1Contato;
        this.a1Inscrm = a1Inscrm;
        this.a1Vend = a1Vend;
        this.a1Comis = a1Comis;
        this.a1Regiao = a1Regiao;
        this.a1Conta = a1Conta;
        this.a1Bco1 = a1Bco1;
        this.a1Bco2 = a1Bco2;
        this.a1Bco3 = a1Bco3;
        this.a1Bco4 = a1Bco4;
        this.a1Bco5 = a1Bco5;
        this.a1Transp = a1Transp;
        this.a1Tpfret = a1Tpfret;
        this.a1Cond = a1Cond;
        this.a1Desc = a1Desc;
        this.a1Prior = a1Prior;
        this.a1Risco = a1Risco;
        this.a1Lc = a1Lc;
        this.a1Venclc = a1Venclc;
        this.a1Classe = a1Classe;
        this.a1Lcfin = a1Lcfin;
        this.a1Moedalc = a1Moedalc;
        this.a1Msaldo = a1Msaldo;
        this.a1Mcompra = a1Mcompra;
        this.a1Metr = a1Metr;
        this.a1Pricom = a1Pricom;
        this.a1Ultcom = a1Ultcom;
        this.a1Nrocom = a1Nrocom;
        this.a1Formvis = a1Formvis;
        this.a1Temvis = a1Temvis;
        this.a1Ultvis = a1Ultvis;
        this.a1Tmpvis = a1Tmpvis;
        this.a1Clasven = a1Clasven;
        this.a1Tmpstd = a1Tmpstd;
        this.a1Mensage = a1Mensage;
        this.a1Saldup = a1Saldup;
        this.a1Reciss = a1Reciss;
        this.a1Nropag = a1Nropag;
        this.a1Salpedl = a1Salpedl;
        this.a1Transf = a1Transf;
        this.a1Suframa = a1Suframa;
        this.a1Atr = a1Atr;
        this.a1Vacum = a1Vacum;
        this.a1Salped = a1Salped;
        this.a1Titprot = a1Titprot;
        this.a1Chqdevo = a1Chqdevo;
        this.a1Dtultit = a1Dtultit;
        this.a1Matr = a1Matr;
        this.a1Dtulchq = a1Dtulchq;
        this.a1Maidupl = a1Maidupl;
        this.a1Tabela = a1Tabela;
        this.a1Inciss = a1Inciss;
        this.a1Saldupm = a1Saldupm;
        this.a1Pagatr = a1Pagatr;
        this.a1Cxposta = a1Cxposta;
        this.a1Ativida = a1Ativida;
        this.a1Cargo1 = a1Cargo1;
        this.a1Cargo2 = a1Cargo2;
        this.a1Cargo3 = a1Cargo3;
        this.a1Rtec = a1Rtec;
        this.a1Super = a1Super;
        this.a1Aliqir = a1Aliqir;
        this.a1Observ = a1Observ;
        this.a1Rg = a1Rg;
        this.a1Calcsuf = a1Calcsuf;
        this.a1Dtnasc = a1Dtnasc;
        this.a1Salpedb = a1Salpedb;
        this.a1Clifat = a1Clifat;
        this.a1Grptrib = a1Grptrib;
        this.a1Bairroc = a1Bairroc;
        this.a1Cepc = a1Cepc;
        this.a1Munc = a1Munc;
        this.a1Estc = a1Estc;
        this.a1Cepe = a1Cepe;
        this.a1Bairroe = a1Bairroe;
        this.a1Mune = a1Mune;
        this.a1Este = a1Este;
        this.a1Sativ1 = a1Sativ1;
        this.a1Sativ2 = a1Sativ2;
        this.a1Tpissrs = a1Tpissrs;
        this.a1Codloc = a1Codloc;
        this.a1Tpessoa = a1Tpessoa;
        this.a1Codpais = a1Codpais;
        this.a1Sativ3 = a1Sativ3;
        this.a1Sativ4 = a1Sativ4;
        this.a1Sativ5 = a1Sativ5;
        this.a1Sativ6 = a1Sativ6;
        this.a1Sativ7 = a1Sativ7;
        this.a1Sativ8 = a1Sativ8;
        this.a1Codmarc = a1Codmarc;
        this.a1Codage = a1Codage;
        this.a1Comage = a1Comage;
        this.a1Tipcli = a1Tipcli;
        this.a1Dest1 = a1Dest1;
        this.a1Email = a1Email;
        this.a1Dest2 = a1Dest2;
        this.a1Codmun = a1Codmun;
        this.a1Dest3 = a1Dest3;
        this.a1Hpage = a1Hpage;
        this.a1Cbo = a1Cbo;
        this.a1Cnae = a1Cnae;
        this.a1Condpag = a1Condpag;
        this.a1Diaspag = a1Diaspag;
        this.a1Obs = a1Obs;
        this.a1Agreg = a1Agreg;
        this.a1Codhist = a1Codhist;
        this.a1Recinss = a1Recinss;
        this.a1Reccofi = a1Reccofi;
        this.a1Reccsll = a1Reccsll;
        this.a1Recpis = a1Recpis;
        this.a1Tipper = a1Tipper;
        this.a1Salfin = a1Salfin;
        this.a1Salfinm = a1Salfinm;
        this.a1Contab = a1Contab;
        this.a1B2b = a1B2b;
        this.a1Grpven = a1Grpven;
        this.a1Clicnv = a1Clicnv;
        this.a1Inscrur = a1Inscrur;
        this.a1Msblql = a1Msblql;
        this.a1Situa = a1Situa;
        this.a1Numra = a1Numra;
        this.a1Subcod = a1Subcod;
        this.a1Cdrdes = a1Cdrdes;
        this.a1Fildeb = a1Fildeb;
        this.a1Codfor = a1Codfor;
        this.a1Abics = a1Abics;
        this.a1Blemail = a1Blemail;
        this.a1Tipocli = a1Tipocli;
        this.a1Vinculo = a1Vinculo;
        this.a1Dtiniv = a1Dtiniv;
        this.a1Dtfimv = a1Dtfimv;
        this.a1Loccons = a1Loccons;
        this.a1Cbairre = a1Cbairre;
        this.a1Codmune = a1Codmune;
        this.a1Perfil = a1Perfil;
        this.a1Hrtrans = a1Hrtrans;
        this.a1Unidven = a1Unidven;
        this.a1Tipprfl = a1Tipprfl;
        this.a1PrfVld = a1PrfVld;
        this.a1PrfCod = a1PrfCod;
        this.a1Regpb = a1Regpb;
        this.a1Usadda = a1Usadda;
        this.a1Simples = a1Simples;
        this.a1Ctare = a1Ctare;
        this.a1Fretiss = a1Fretiss;
        this.a1Codsiaf = a1Codsiaf;
        this.a1Endnot = a1Endnot;
        this.a1Ceinss = a1Ceinss;
        this.a1Regesim = a1Regesim;
        this.a1Percatm = a1Percatm;
        this.a1Ipweb = a1Ipweb;
        this.a1Idhist = a1Idhist;
        this.a1Indret = a1Indret;
        this.a1Nif = a1Nif;
        this.a1Irbax = a1Irbax;
        this.a1Abatimp = a1Abatimp;
        this.a1Contrib = a1Contrib;
        this.a1Tda = a1Tda;
        this.a1Complem = a1Complem;
        this.a1Timekee = a1Timekee;
        this.a1Recirrf = a1Recirrf;
        this.a1Origem = a1Origem;
        this.a1Fomezer = a1Fomezer;
        this.a1Recfet = a1Recfet;
        this.a1Incult = a1Incult;
        this.a1Minirf = a1Minirf;
        this.a1Filtrf = a1Filtrf;
        this.a1Matfun = a1Matfun;
        this.a1Outrmun = a1Outrmun;
        this.a1Codfid = a1Codfid;
        this.a1Simpnac = a1Simpnac;
        this.a1Tpnfse = a1Tpnfse;
        this.a1Alifixa = a1Alifixa;
        this.a1Crdma = a1Crdma;
        this.a1Prstser = a1Prstser;
        this.a1Rfacs = a1Rfacs;
        this.a1Rfabov = a1Rfabov;
        this.a1Perfecp = a1Perfecp;
        this.a1Iencont = a1Iencont;
        this.a1Tpdp = a1Tpdp;
        this.a1Entid = a1Entid;
        this.a1Tpj = a1Tpj;
        this.dELET = dELET;
        this.a1Unrcob = a1Unrcob;
        this.a1Msfil = a1Msfil;
        this.a1Inovaut = a1Inovaut;
        this.a1Udetfat = a1Udetfat;
        this.a1Uenvnf = a1Uenvnf;
        this.a1Cel = a1Cel;
    }

    public String getA1uDddCel() {
		return a1uDddCel;
	}

	public void setA1uDddCel(String a1uDddCel) {
		this.a1uDddCel = a1uDddCel;
	}

	public Integer getRECNO() {
        return rECNO;
    }

    public void setRECNO(Integer rECNO) {
        this.rECNO = rECNO;
    }

    public String getA1Filial() {
        return a1Filial;
    }

    public void setA1Filial(String a1Filial) {
        this.a1Filial = a1Filial;
    }

    public String getA1Cod() {
        return a1Cod;
    }

    public void setA1Cod(String a1Cod) {
        this.a1Cod = a1Cod;
    }

    public String getA1Loja() {
        return a1Loja;
    }

    public void setA1Loja(String a1Loja) {
        this.a1Loja = a1Loja;
    }

    public String getA1Pessoa() {
        return a1Pessoa;
    }

    public void setA1Pessoa(String a1Pessoa) {
        this.a1Pessoa = a1Pessoa;
    }

    public String getA1Cgc() {
        return a1Cgc;
    }

    public void setA1Cgc(String a1Cgc) {
        this.a1Cgc = a1Cgc;
    }

    public String getA1Nome() {
        return a1Nome;
    }

    public void setA1Nome(String a1Nome) {
        this.a1Nome = a1Nome;
    }

    public String getA1Nreduz() {
        return a1Nreduz;
    }

    public void setA1Nreduz(String a1Nreduz) {
        this.a1Nreduz = a1Nreduz;
    }

    public String getA1End() {
        return a1End;
    }

    public void setA1End(String a1End) {
        this.a1End = a1End;
    }

    public String getA1Cep() {
        return a1Cep;
    }

    public void setA1Cep(String a1Cep) {
        this.a1Cep = a1Cep;
    }

    public String getA1Est() {
        return a1Est;
    }

    public void setA1Est(String a1Est) {
        this.a1Est = a1Est;
    }

    public String getA1Estado() {
        return a1Estado;
    }

    public void setA1Estado(String a1Estado) {
        this.a1Estado = a1Estado;
    }

    public String getA1CodMun() {
        return a1CodMun;
    }

    public void setA1CodMun(String a1CodMun) {
        this.a1CodMun = a1CodMun;
    }

    public String getA1Mun() {
        return a1Mun;
    }

    public void setA1Mun(String a1Mun) {
        this.a1Mun = a1Mun;
    }

    public String getA1Bairro() {
        return a1Bairro;
    }

    public void setA1Bairro(String a1Bairro) {
        this.a1Bairro = a1Bairro;
    }

    public String getA1Naturez() {
        return a1Naturez;
    }

    public void setA1Naturez(String a1Naturez) {
        this.a1Naturez = a1Naturez;
    }

    public String getA1Ibge() {
        return a1Ibge;
    }

    public void setA1Ibge(String a1Ibge) {
        this.a1Ibge = a1Ibge;
    }

    public String getA1Ddi() {
        return a1Ddi;
    }

    public void setA1Ddi(String a1Ddi) {
        this.a1Ddi = a1Ddi;
    }

    public String getA1Ddd() {
        return a1Ddd;
    }

    public void setA1Ddd(String a1Ddd) {
        this.a1Ddd = a1Ddd;
    }

    public String getA1Tel() {
        return a1Tel;
    }

    public void setA1Tel(String a1Tel) {
        this.a1Tel = a1Tel;
    }

    public String getA1Telex() {
        return a1Telex;
    }

    public void setA1Telex(String a1Telex) {
        this.a1Telex = a1Telex;
    }

    public String getA1Inscr() {
        return a1Inscr;
    }

    public void setA1Inscr(String a1Inscr) {
        this.a1Inscr = a1Inscr;
    }

    public String getA1Pfisica() {
        return a1Pfisica;
    }

    public void setA1Pfisica(String a1Pfisica) {
        this.a1Pfisica = a1Pfisica;
    }

    public String getA1Pais() {
        return a1Pais;
    }

    public void setA1Pais(String a1Pais) {
        this.a1Pais = a1Pais;
    }

    public String getA1Fax() {
        return a1Fax;
    }

    public void setA1Fax(String a1Fax) {
        this.a1Fax = a1Fax;
    }

    public String getA1Tipo() {
        return a1Tipo;
    }

    public void setA1Tipo(String a1Tipo) {
        this.a1Tipo = a1Tipo;
    }

    public String getA1Endcob() {
        return a1Endcob;
    }

    public void setA1Endcob(String a1Endcob) {
        this.a1Endcob = a1Endcob;
    }

    public String getA1Tribfav() {
        return a1Tribfav;
    }

    public void setA1Tribfav(String a1Tribfav) {
        this.a1Tribfav = a1Tribfav;
    }

    public String getA1Endrec() {
        return a1Endrec;
    }

    public void setA1Endrec(String a1Endrec) {
        this.a1Endrec = a1Endrec;
    }

    public String getA1Endent() {
        return a1Endent;
    }

    public void setA1Endent(String a1Endent) {
        this.a1Endent = a1Endent;
    }

    public String getA1Contato() {
        return a1Contato;
    }

    public void setA1Contato(String a1Contato) {
        this.a1Contato = a1Contato;
    }

    public String getA1Inscrm() {
        return a1Inscrm;
    }

    public void setA1Inscrm(String a1Inscrm) {
        this.a1Inscrm = a1Inscrm;
    }

    public String getA1Vend() {
        return a1Vend;
    }

    public void setA1Vend(String a1Vend) {
        this.a1Vend = a1Vend;
    }

    public double getA1Comis() {
        return a1Comis;
    }

    public void setA1Comis(double a1Comis) {
        this.a1Comis = a1Comis;
    }

    public String getA1Regiao() {
        return a1Regiao;
    }

    public void setA1Regiao(String a1Regiao) {
        this.a1Regiao = a1Regiao;
    }

    public String getA1Conta() {
        return a1Conta;
    }

    public void setA1Conta(String a1Conta) {
        this.a1Conta = a1Conta;
    }

    public String getA1Bco1() {
        return a1Bco1;
    }

    public void setA1Bco1(String a1Bco1) {
        this.a1Bco1 = a1Bco1;
    }

    public String getA1Bco2() {
        return a1Bco2;
    }

    public void setA1Bco2(String a1Bco2) {
        this.a1Bco2 = a1Bco2;
    }

    public String getA1Bco3() {
        return a1Bco3;
    }

    public void setA1Bco3(String a1Bco3) {
        this.a1Bco3 = a1Bco3;
    }

    public String getA1Bco4() {
        return a1Bco4;
    }

    public void setA1Bco4(String a1Bco4) {
        this.a1Bco4 = a1Bco4;
    }

    public String getA1Bco5() {
        return a1Bco5;
    }

    public void setA1Bco5(String a1Bco5) {
        this.a1Bco5 = a1Bco5;
    }

    public String getA1Transp() {
        return a1Transp;
    }

    public void setA1Transp(String a1Transp) {
        this.a1Transp = a1Transp;
    }

    public String getA1Tpfret() {
        return a1Tpfret;
    }

    public void setA1Tpfret(String a1Tpfret) {
        this.a1Tpfret = a1Tpfret;
    }

    public String getA1Cond() {
        return a1Cond;
    }

    public void setA1Cond(String a1Cond) {
        this.a1Cond = a1Cond;
    }

    public double getA1Desc() {
        return a1Desc;
    }

    public void setA1Desc(double a1Desc) {
        this.a1Desc = a1Desc;
    }

    public String getA1Prior() {
        return a1Prior;
    }

    public void setA1Prior(String a1Prior) {
        this.a1Prior = a1Prior;
    }

    public String getA1Risco() {
        return a1Risco;
    }

    public void setA1Risco(String a1Risco) {
        this.a1Risco = a1Risco;
    }

    public double getA1Lc() {
        return a1Lc;
    }

    public void setA1Lc(double a1Lc) {
        this.a1Lc = a1Lc;
    }

    public String getA1Venclc() {
        return a1Venclc;
    }

    public void setA1Venclc(String a1Venclc) {
        this.a1Venclc = a1Venclc;
    }

    public String getA1Classe() {
        return a1Classe;
    }

    public void setA1Classe(String a1Classe) {
        this.a1Classe = a1Classe;
    }

    public double getA1Lcfin() {
        return a1Lcfin;
    }

    public void setA1Lcfin(double a1Lcfin) {
        this.a1Lcfin = a1Lcfin;
    }

    public double getA1Moedalc() {
        return a1Moedalc;
    }

    public void setA1Moedalc(double a1Moedalc) {
        this.a1Moedalc = a1Moedalc;
    }

    public double getA1Msaldo() {
        return a1Msaldo;
    }

    public void setA1Msaldo(double a1Msaldo) {
        this.a1Msaldo = a1Msaldo;
    }

    public double getA1Mcompra() {
        return a1Mcompra;
    }

    public void setA1Mcompra(double a1Mcompra) {
        this.a1Mcompra = a1Mcompra;
    }

    public double getA1Metr() {
        return a1Metr;
    }

    public void setA1Metr(double a1Metr) {
        this.a1Metr = a1Metr;
    }

    public String getA1Pricom() {
        return a1Pricom;
    }

    public void setA1Pricom(String a1Pricom) {
        this.a1Pricom = a1Pricom;
    }

    public String getA1Ultcom() {
        return a1Ultcom;
    }

    public void setA1Ultcom(String a1Ultcom) {
        this.a1Ultcom = a1Ultcom;
    }

    public double getA1Nrocom() {
        return a1Nrocom;
    }

    public void setA1Nrocom(double a1Nrocom) {
        this.a1Nrocom = a1Nrocom;
    }

    public String getA1Formvis() {
        return a1Formvis;
    }

    public void setA1Formvis(String a1Formvis) {
        this.a1Formvis = a1Formvis;
    }

    public double getA1Temvis() {
        return a1Temvis;
    }

    public void setA1Temvis(double a1Temvis) {
        this.a1Temvis = a1Temvis;
    }

    public String getA1Ultvis() {
        return a1Ultvis;
    }

    public void setA1Ultvis(String a1Ultvis) {
        this.a1Ultvis = a1Ultvis;
    }

    public String getA1Tmpvis() {
        return a1Tmpvis;
    }

    public void setA1Tmpvis(String a1Tmpvis) {
        this.a1Tmpvis = a1Tmpvis;
    }

    public String getA1Clasven() {
        return a1Clasven;
    }

    public void setA1Clasven(String a1Clasven) {
        this.a1Clasven = a1Clasven;
    }

    public String getA1Tmpstd() {
        return a1Tmpstd;
    }

    public void setA1Tmpstd(String a1Tmpstd) {
        this.a1Tmpstd = a1Tmpstd;
    }

    public String getA1Mensage() {
        return a1Mensage;
    }

    public void setA1Mensage(String a1Mensage) {
        this.a1Mensage = a1Mensage;
    }

    public double getA1Saldup() {
        return a1Saldup;
    }

    public void setA1Saldup(double a1Saldup) {
        this.a1Saldup = a1Saldup;
    }

    public String getA1Reciss() {
        return a1Reciss;
    }

    public void setA1Reciss(String a1Reciss) {
        this.a1Reciss = a1Reciss;
    }

    public double getA1Nropag() {
        return a1Nropag;
    }

    public void setA1Nropag(double a1Nropag) {
        this.a1Nropag = a1Nropag;
    }

    public double getA1Salpedl() {
        return a1Salpedl;
    }

    public void setA1Salpedl(double a1Salpedl) {
        this.a1Salpedl = a1Salpedl;
    }

    public String getA1Transf() {
        return a1Transf;
    }

    public void setA1Transf(String a1Transf) {
        this.a1Transf = a1Transf;
    }

    public String getA1Suframa() {
        return a1Suframa;
    }

    public void setA1Suframa(String a1Suframa) {
        this.a1Suframa = a1Suframa;
    }

    public double getA1Atr() {
        return a1Atr;
    }

    public void setA1Atr(double a1Atr) {
        this.a1Atr = a1Atr;
    }

    public double getA1Vacum() {
        return a1Vacum;
    }

    public void setA1Vacum(double a1Vacum) {
        this.a1Vacum = a1Vacum;
    }

    public double getA1Salped() {
        return a1Salped;
    }

    public void setA1Salped(double a1Salped) {
        this.a1Salped = a1Salped;
    }

    public double getA1Titprot() {
        return a1Titprot;
    }

    public void setA1Titprot(double a1Titprot) {
        this.a1Titprot = a1Titprot;
    }

    public double getA1Chqdevo() {
        return a1Chqdevo;
    }

    public void setA1Chqdevo(double a1Chqdevo) {
        this.a1Chqdevo = a1Chqdevo;
    }

    public String getA1Dtultit() {
        return a1Dtultit;
    }

    public void setA1Dtultit(String a1Dtultit) {
        this.a1Dtultit = a1Dtultit;
    }

    public double getA1Matr() {
        return a1Matr;
    }

    public void setA1Matr(double a1Matr) {
        this.a1Matr = a1Matr;
    }

    public String getA1Dtulchq() {
        return a1Dtulchq;
    }

    public void setA1Dtulchq(String a1Dtulchq) {
        this.a1Dtulchq = a1Dtulchq;
    }

    public double getA1Maidupl() {
        return a1Maidupl;
    }

    public void setA1Maidupl(double a1Maidupl) {
        this.a1Maidupl = a1Maidupl;
    }

    public String getA1Tabela() {
        return a1Tabela;
    }

    public void setA1Tabela(String a1Tabela) {
        this.a1Tabela = a1Tabela;
    }

    public String getA1Inciss() {
        return a1Inciss;
    }

    public void setA1Inciss(String a1Inciss) {
        this.a1Inciss = a1Inciss;
    }

    public double getA1Saldupm() {
        return a1Saldupm;
    }

    public void setA1Saldupm(double a1Saldupm) {
        this.a1Saldupm = a1Saldupm;
    }

    public double getA1Pagatr() {
        return a1Pagatr;
    }

    public void setA1Pagatr(double a1Pagatr) {
        this.a1Pagatr = a1Pagatr;
    }

    public String getA1Cxposta() {
        return a1Cxposta;
    }

    public void setA1Cxposta(String a1Cxposta) {
        this.a1Cxposta = a1Cxposta;
    }

    public String getA1Ativida() {
        return a1Ativida;
    }

    public void setA1Ativida(String a1Ativida) {
        this.a1Ativida = a1Ativida;
    }

    public String getA1Cargo1() {
        return a1Cargo1;
    }

    public void setA1Cargo1(String a1Cargo1) {
        this.a1Cargo1 = a1Cargo1;
    }

    public String getA1Cargo2() {
        return a1Cargo2;
    }

    public void setA1Cargo2(String a1Cargo2) {
        this.a1Cargo2 = a1Cargo2;
    }

    public String getA1Cargo3() {
        return a1Cargo3;
    }

    public void setA1Cargo3(String a1Cargo3) {
        this.a1Cargo3 = a1Cargo3;
    }

    public String getA1Rtec() {
        return a1Rtec;
    }

    public void setA1Rtec(String a1Rtec) {
        this.a1Rtec = a1Rtec;
    }

    public String getA1Super() {
        return a1Super;
    }

    public void setA1Super(String a1Super) {
        this.a1Super = a1Super;
    }

    public double getA1Aliqir() {
        return a1Aliqir;
    }

    public void setA1Aliqir(double a1Aliqir) {
        this.a1Aliqir = a1Aliqir;
    }

    public String getA1Observ() {
        return a1Observ;
    }

    public void setA1Observ(String a1Observ) {
        this.a1Observ = a1Observ;
    }

    public String getA1Rg() {
        return a1Rg;
    }

    public void setA1Rg(String a1Rg) {
        this.a1Rg = a1Rg;
    }

    public String getA1Calcsuf() {
        return a1Calcsuf;
    }

    public void setA1Calcsuf(String a1Calcsuf) {
        this.a1Calcsuf = a1Calcsuf;
    }

    public String getA1Dtnasc() {
        return a1Dtnasc;
    }

    public void setA1Dtnasc(String a1Dtnasc) {
        this.a1Dtnasc = a1Dtnasc;
    }

    public double getA1Salpedb() {
        return a1Salpedb;
    }

    public void setA1Salpedb(double a1Salpedb) {
        this.a1Salpedb = a1Salpedb;
    }

    public String getA1Clifat() {
        return a1Clifat;
    }

    public void setA1Clifat(String a1Clifat) {
        this.a1Clifat = a1Clifat;
    }

    public String getA1Grptrib() {
        return a1Grptrib;
    }

    public void setA1Grptrib(String a1Grptrib) {
        this.a1Grptrib = a1Grptrib;
    }

    public String getA1Bairroc() {
        return a1Bairroc;
    }

    public void setA1Bairroc(String a1Bairroc) {
        this.a1Bairroc = a1Bairroc;
    }

    public String getA1Cepc() {
        return a1Cepc;
    }

    public void setA1Cepc(String a1Cepc) {
        this.a1Cepc = a1Cepc;
    }

    public String getA1Munc() {
        return a1Munc;
    }

    public void setA1Munc(String a1Munc) {
        this.a1Munc = a1Munc;
    }

    public String getA1Estc() {
        return a1Estc;
    }

    public void setA1Estc(String a1Estc) {
        this.a1Estc = a1Estc;
    }

    public String getA1Cepe() {
        return a1Cepe;
    }

    public void setA1Cepe(String a1Cepe) {
        this.a1Cepe = a1Cepe;
    }

    public String getA1Bairroe() {
        return a1Bairroe;
    }

    public void setA1Bairroe(String a1Bairroe) {
        this.a1Bairroe = a1Bairroe;
    }

    public String getA1Mune() {
        return a1Mune;
    }

    public void setA1Mune(String a1Mune) {
        this.a1Mune = a1Mune;
    }

    public String getA1Este() {
        return a1Este;
    }

    public void setA1Este(String a1Este) {
        this.a1Este = a1Este;
    }

    public String getA1Sativ1() {
        return a1Sativ1;
    }

    public void setA1Sativ1(String a1Sativ1) {
        this.a1Sativ1 = a1Sativ1;
    }

    public String getA1Sativ2() {
        return a1Sativ2;
    }

    public void setA1Sativ2(String a1Sativ2) {
        this.a1Sativ2 = a1Sativ2;
    }

    public String getA1Tpissrs() {
        return a1Tpissrs;
    }

    public void setA1Tpissrs(String a1Tpissrs) {
        this.a1Tpissrs = a1Tpissrs;
    }

    public String getA1Codloc() {
        return a1Codloc;
    }

    public void setA1Codloc(String a1Codloc) {
        this.a1Codloc = a1Codloc;
    }

    public String getA1Tpessoa() {
        return a1Tpessoa;
    }

    public void setA1Tpessoa(String a1Tpessoa) {
        this.a1Tpessoa = a1Tpessoa;
    }

    public String getA1Codpais() {
        return a1Codpais;
    }

    public void setA1Codpais(String a1Codpais) {
        this.a1Codpais = a1Codpais;
    }

    public String getA1Sativ3() {
        return a1Sativ3;
    }

    public void setA1Sativ3(String a1Sativ3) {
        this.a1Sativ3 = a1Sativ3;
    }

    public String getA1Sativ4() {
        return a1Sativ4;
    }

    public void setA1Sativ4(String a1Sativ4) {
        this.a1Sativ4 = a1Sativ4;
    }

    public String getA1Sativ5() {
        return a1Sativ5;
    }

    public void setA1Sativ5(String a1Sativ5) {
        this.a1Sativ5 = a1Sativ5;
    }

    public String getA1Sativ6() {
        return a1Sativ6;
    }

    public void setA1Sativ6(String a1Sativ6) {
        this.a1Sativ6 = a1Sativ6;
    }

    public String getA1Sativ7() {
        return a1Sativ7;
    }

    public void setA1Sativ7(String a1Sativ7) {
        this.a1Sativ7 = a1Sativ7;
    }

    public String getA1Sativ8() {
        return a1Sativ8;
    }

    public void setA1Sativ8(String a1Sativ8) {
        this.a1Sativ8 = a1Sativ8;
    }

    public String getA1Codmarc() {
        return a1Codmarc;
    }

    public void setA1Codmarc(String a1Codmarc) {
        this.a1Codmarc = a1Codmarc;
    }

    public String getA1Codage() {
        return a1Codage;
    }

    public void setA1Codage(String a1Codage) {
        this.a1Codage = a1Codage;
    }

    public double getA1Comage() {
        return a1Comage;
    }

    public void setA1Comage(double a1Comage) {
        this.a1Comage = a1Comage;
    }

    public String getA1Tipcli() {
        return a1Tipcli;
    }

    public void setA1Tipcli(String a1Tipcli) {
        this.a1Tipcli = a1Tipcli;
    }

    public String getA1Dest1() {
        return a1Dest1;
    }

    public void setA1Dest1(String a1Dest1) {
        this.a1Dest1 = a1Dest1;
    }

    public String getA1Email() {
        return a1Email;
    }

    public void setA1Email(String a1Email) {
        this.a1Email = a1Email;
    }

    public String getA1Dest2() {
        return a1Dest2;
    }

    public void setA1Dest2(String a1Dest2) {
        this.a1Dest2 = a1Dest2;
    }

    public String getA1Codmun() {
        return a1Codmun;
    }

    public void setA1Codmun(String a1Codmun) {
        this.a1Codmun = a1Codmun;
    }

    public String getA1Dest3() {
        return a1Dest3;
    }

    public void setA1Dest3(String a1Dest3) {
        this.a1Dest3 = a1Dest3;
    }

    public String getA1Hpage() {
        return a1Hpage;
    }

    public void setA1Hpage(String a1Hpage) {
        this.a1Hpage = a1Hpage;
    }

    public String getA1Cbo() {
        return a1Cbo;
    }

    public void setA1Cbo(String a1Cbo) {
        this.a1Cbo = a1Cbo;
    }

    public String getA1Cnae() {
        return a1Cnae;
    }

    public void setA1Cnae(String a1Cnae) {
        this.a1Cnae = a1Cnae;
    }

    public String getA1Condpag() {
        return a1Condpag;
    }

    public void setA1Condpag(String a1Condpag) {
        this.a1Condpag = a1Condpag;
    }

    public double getA1Diaspag() {
        return a1Diaspag;
    }

    public void setA1Diaspag(double a1Diaspag) {
        this.a1Diaspag = a1Diaspag;
    }

    public String getA1Obs() {
        return a1Obs;
    }

    public void setA1Obs(String a1Obs) {
        this.a1Obs = a1Obs;
    }

    public String getA1Agreg() {
        return a1Agreg;
    }

    public void setA1Agreg(String a1Agreg) {
        this.a1Agreg = a1Agreg;
    }

    public String getA1Codhist() {
        return a1Codhist;
    }

    public void setA1Codhist(String a1Codhist) {
        this.a1Codhist = a1Codhist;
    }

    public String getA1Recinss() {
        return a1Recinss;
    }

    public void setA1Recinss(String a1Recinss) {
        this.a1Recinss = a1Recinss;
    }

    public String getA1Reccofi() {
        return a1Reccofi;
    }

    public void setA1Reccofi(String a1Reccofi) {
        this.a1Reccofi = a1Reccofi;
    }

    public String getA1Reccsll() {
        return a1Reccsll;
    }

    public void setA1Reccsll(String a1Reccsll) {
        this.a1Reccsll = a1Reccsll;
    }

    public String getA1Recpis() {
        return a1Recpis;
    }

    public void setA1Recpis(String a1Recpis) {
        this.a1Recpis = a1Recpis;
    }

    public String getA1Tipper() {
        return a1Tipper;
    }

    public void setA1Tipper(String a1Tipper) {
        this.a1Tipper = a1Tipper;
    }

    public double getA1Salfin() {
        return a1Salfin;
    }

    public void setA1Salfin(double a1Salfin) {
        this.a1Salfin = a1Salfin;
    }

    public double getA1Salfinm() {
        return a1Salfinm;
    }

    public void setA1Salfinm(double a1Salfinm) {
        this.a1Salfinm = a1Salfinm;
    }

    public String getA1Contab() {
        return a1Contab;
    }

    public void setA1Contab(String a1Contab) {
        this.a1Contab = a1Contab;
    }

    public String getA1B2b() {
        return a1B2b;
    }

    public void setA1B2b(String a1B2b) {
        this.a1B2b = a1B2b;
    }

    public String getA1Grpven() {
        return a1Grpven;
    }

    public void setA1Grpven(String a1Grpven) {
        this.a1Grpven = a1Grpven;
    }

    public String getA1Clicnv() {
        return a1Clicnv;
    }

    public void setA1Clicnv(String a1Clicnv) {
        this.a1Clicnv = a1Clicnv;
    }

    public String getA1Inscrur() {
        return a1Inscrur;
    }

    public void setA1Inscrur(String a1Inscrur) {
        this.a1Inscrur = a1Inscrur;
    }

    public String getA1Msblql() {
        return a1Msblql;
    }

    public void setA1Msblql(String a1Msblql) {
        this.a1Msblql = a1Msblql;
    }

    public String getA1Situa() {
        return a1Situa;
    }

    public void setA1Situa(String a1Situa) {
        this.a1Situa = a1Situa;
    }

    public String getA1Numra() {
        return a1Numra;
    }

    public void setA1Numra(String a1Numra) {
        this.a1Numra = a1Numra;
    }

    public String getA1Subcod() {
        return a1Subcod;
    }

    public void setA1Subcod(String a1Subcod) {
        this.a1Subcod = a1Subcod;
    }

    public String getA1Cdrdes() {
        return a1Cdrdes;
    }

    public void setA1Cdrdes(String a1Cdrdes) {
        this.a1Cdrdes = a1Cdrdes;
    }

    public String getA1Fildeb() {
        return a1Fildeb;
    }

    public void setA1Fildeb(String a1Fildeb) {
        this.a1Fildeb = a1Fildeb;
    }

    public String getA1Codfor() {
        return a1Codfor;
    }

    public void setA1Codfor(String a1Codfor) {
        this.a1Codfor = a1Codfor;
    }

    public String getA1Abics() {
        return a1Abics;
    }

    public void setA1Abics(String a1Abics) {
        this.a1Abics = a1Abics;
    }

    public String getA1Blemail() {
        return a1Blemail;
    }

    public void setA1Blemail(String a1Blemail) {
        this.a1Blemail = a1Blemail;
    }

    public String getA1Tipocli() {
        return a1Tipocli;
    }

    public void setA1Tipocli(String a1Tipocli) {
        this.a1Tipocli = a1Tipocli;
    }

    public String getA1Vinculo() {
        return a1Vinculo;
    }

    public void setA1Vinculo(String a1Vinculo) {
        this.a1Vinculo = a1Vinculo;
    }

    public String getA1Dtiniv() {
        return a1Dtiniv;
    }

    public void setA1Dtiniv(String a1Dtiniv) {
        this.a1Dtiniv = a1Dtiniv;
    }

    public String getA1Dtfimv() {
        return a1Dtfimv;
    }

    public void setA1Dtfimv(String a1Dtfimv) {
        this.a1Dtfimv = a1Dtfimv;
    }

    public String getA1Loccons() {
        return a1Loccons;
    }

    public void setA1Loccons(String a1Loccons) {
        this.a1Loccons = a1Loccons;
    }

    public String getA1Cbairre() {
        return a1Cbairre;
    }

    public void setA1Cbairre(String a1Cbairre) {
        this.a1Cbairre = a1Cbairre;
    }

    public String getA1Codmune() {
        return a1Codmune;
    }

    public void setA1Codmune(String a1Codmune) {
        this.a1Codmune = a1Codmune;
    }

    public double getA1Perfil() {
        return a1Perfil;
    }

    public void setA1Perfil(double a1Perfil) {
        this.a1Perfil = a1Perfil;
    }

    public String getA1Hrtrans() {
        return a1Hrtrans;
    }

    public void setA1Hrtrans(String a1Hrtrans) {
        this.a1Hrtrans = a1Hrtrans;
    }

    public String getA1Unidven() {
        return a1Unidven;
    }

    public void setA1Unidven(String a1Unidven) {
        this.a1Unidven = a1Unidven;
    }

    public String getA1Tipprfl() {
        return a1Tipprfl;
    }

    public void setA1Tipprfl(String a1Tipprfl) {
        this.a1Tipprfl = a1Tipprfl;
    }

    public String getA1PrfVld() {
        return a1PrfVld;
    }

    public void setA1PrfVld(String a1PrfVld) {
        this.a1PrfVld = a1PrfVld;
    }

    public String getA1PrfCod() {
        return a1PrfCod;
    }

    public void setA1PrfCod(String a1PrfCod) {
        this.a1PrfCod = a1PrfCod;
    }

    public byte[] getA1PrfObs() {
        return a1PrfObs;
    }

    public void setA1PrfObs(byte[] a1PrfObs) {
        this.a1PrfObs = a1PrfObs;
    }

    public String getA1Regpb() {
        return a1Regpb;
    }

    public void setA1Regpb(String a1Regpb) {
        this.a1Regpb = a1Regpb;
    }

    public String getA1Usadda() {
        return a1Usadda;
    }

    public void setA1Usadda(String a1Usadda) {
        this.a1Usadda = a1Usadda;
    }

    public String getA1Simples() {
        return a1Simples;
    }

    public void setA1Simples(String a1Simples) {
        this.a1Simples = a1Simples;
    }

    public String getA1Ctare() {
        return a1Ctare;
    }

    public void setA1Ctare(String a1Ctare) {
        this.a1Ctare = a1Ctare;
    }

    public String getA1Fretiss() {
        return a1Fretiss;
    }

    public void setA1Fretiss(String a1Fretiss) {
        this.a1Fretiss = a1Fretiss;
    }

    public String getA1Codsiaf() {
        return a1Codsiaf;
    }

    public void setA1Codsiaf(String a1Codsiaf) {
        this.a1Codsiaf = a1Codsiaf;
    }

    public String getA1Endnot() {
        return a1Endnot;
    }

    public void setA1Endnot(String a1Endnot) {
        this.a1Endnot = a1Endnot;
    }

    public String getA1Ceinss() {
        return a1Ceinss;
    }

    public void setA1Ceinss(String a1Ceinss) {
        this.a1Ceinss = a1Ceinss;
    }

    public String getA1Regesim() {
        return a1Regesim;
    }

    public void setA1Regesim(String a1Regesim) {
        this.a1Regesim = a1Regesim;
    }

    public double getA1Percatm() {
        return a1Percatm;
    }

    public void setA1Percatm(double a1Percatm) {
        this.a1Percatm = a1Percatm;
    }

    public String getA1Ipweb() {
        return a1Ipweb;
    }

    public void setA1Ipweb(String a1Ipweb) {
        this.a1Ipweb = a1Ipweb;
    }

    public String getA1Idhist() {
        return a1Idhist;
    }

    public void setA1Idhist(String a1Idhist) {
        this.a1Idhist = a1Idhist;
    }

    public String getA1Indret() {
        return a1Indret;
    }

    public void setA1Indret(String a1Indret) {
        this.a1Indret = a1Indret;
    }

    public String getA1Nif() {
        return a1Nif;
    }

    public void setA1Nif(String a1Nif) {
        this.a1Nif = a1Nif;
    }

    public String getA1Irbax() {
        return a1Irbax;
    }

    public void setA1Irbax(String a1Irbax) {
        this.a1Irbax = a1Irbax;
    }

    public String getA1Abatimp() {
        return a1Abatimp;
    }

    public void setA1Abatimp(String a1Abatimp) {
        this.a1Abatimp = a1Abatimp;
    }

    public String getA1Contrib() {
        return a1Contrib;
    }

    public void setA1Contrib(String a1Contrib) {
        this.a1Contrib = a1Contrib;
    }

    public String getA1Tda() {
        return a1Tda;
    }

    public void setA1Tda(String a1Tda) {
        this.a1Tda = a1Tda;
    }

    public String getA1Complem() {
        return a1Complem;
    }

    public void setA1Complem(String a1Complem) {
        this.a1Complem = a1Complem;
    }

    public String getA1Timekee() {
        return a1Timekee;
    }

    public void setA1Timekee(String a1Timekee) {
        this.a1Timekee = a1Timekee;
    }

    public String getA1Recirrf() {
        return a1Recirrf;
    }

    public void setA1Recirrf(String a1Recirrf) {
        this.a1Recirrf = a1Recirrf;
    }

    public String getA1Origem() {
        return a1Origem;
    }

    public void setA1Origem(String a1Origem) {
        this.a1Origem = a1Origem;
    }

    public String getA1Fomezer() {
        return a1Fomezer;
    }

    public void setA1Fomezer(String a1Fomezer) {
        this.a1Fomezer = a1Fomezer;
    }

    public String getA1Recfet() {
        return a1Recfet;
    }

    public void setA1Recfet(String a1Recfet) {
        this.a1Recfet = a1Recfet;
    }

    public String getA1Incult() {
        return a1Incult;
    }

    public void setA1Incult(String a1Incult) {
        this.a1Incult = a1Incult;
    }

    public String getA1Minirf() {
        return a1Minirf;
    }

    public void setA1Minirf(String a1Minirf) {
        this.a1Minirf = a1Minirf;
    }

    public String getA1Filtrf() {
        return a1Filtrf;
    }

    public void setA1Filtrf(String a1Filtrf) {
        this.a1Filtrf = a1Filtrf;
    }

    public String getA1Matfun() {
        return a1Matfun;
    }

    public void setA1Matfun(String a1Matfun) {
        this.a1Matfun = a1Matfun;
    }

    public String getA1Outrmun() {
        return a1Outrmun;
    }

    public void setA1Outrmun(String a1Outrmun) {
        this.a1Outrmun = a1Outrmun;
    }

    public String getA1Codfid() {
        return a1Codfid;
    }

    public void setA1Codfid(String a1Codfid) {
        this.a1Codfid = a1Codfid;
    }

    public String getA1Simpnac() {
        return a1Simpnac;
    }

    public void setA1Simpnac(String a1Simpnac) {
        this.a1Simpnac = a1Simpnac;
    }

    public String getA1Tpnfse() {
        return a1Tpnfse;
    }

    public void setA1Tpnfse(String a1Tpnfse) {
        this.a1Tpnfse = a1Tpnfse;
    }

    public String getA1Alifixa() {
        return a1Alifixa;
    }

    public void setA1Alifixa(String a1Alifixa) {
        this.a1Alifixa = a1Alifixa;
    }

    public String getA1Crdma() {
        return a1Crdma;
    }

    public void setA1Crdma(String a1Crdma) {
        this.a1Crdma = a1Crdma;
    }

    public String getA1Prstser() {
        return a1Prstser;
    }

    public void setA1Prstser(String a1Prstser) {
        this.a1Prstser = a1Prstser;
    }

    public String getA1Rfacs() {
        return a1Rfacs;
    }

    public void setA1Rfacs(String a1Rfacs) {
        this.a1Rfacs = a1Rfacs;
    }

    public String getA1Rfabov() {
        return a1Rfabov;
    }

    public void setA1Rfabov(String a1Rfabov) {
        this.a1Rfabov = a1Rfabov;
    }

    public double getA1Perfecp() {
        return a1Perfecp;
    }

    public void setA1Perfecp(double a1Perfecp) {
        this.a1Perfecp = a1Perfecp;
    }

    public String getA1Iencont() {
        return a1Iencont;
    }

    public void setA1Iencont(String a1Iencont) {
        this.a1Iencont = a1Iencont;
    }

    public String getA1Tpdp() {
        return a1Tpdp;
    }

    public void setA1Tpdp(String a1Tpdp) {
        this.a1Tpdp = a1Tpdp;
    }

    public String getA1Entid() {
        return a1Entid;
    }

    public void setA1Entid(String a1Entid) {
        this.a1Entid = a1Entid;
    }

    public String getA1Tpj() {
        return a1Tpj;
    }

    public void setA1Tpj(String a1Tpj) {
        this.a1Tpj = a1Tpj;
    }

    public String getDELET() {
        return dELET;
    }

    public void setDELET(String dELET) {
        this.dELET = dELET;
    }

//    @Column(name = "R_E_C_D_E_L_")
//    public int getRECDEL() {
//        return rECDEL;
//    }
//
//    public void setRECDEL(int rECDEL) {
//        this.rECDEL = rECDEL;
//    }
    public String getA1Unrcob() {
        return a1Unrcob;
    }

    public void setA1Unrcob(String a1Unrcob) {
        this.a1Unrcob = a1Unrcob;
    }

    public String getA1Msfil() {
        return a1Msfil;
    }

    public void setA1Msfil(String a1Msfil) {
        this.a1Msfil = a1Msfil;
    }

    public String getA1Inovaut() {
        return a1Inovaut;
    }

    public void setA1Inovaut(String a1Inovaut) {
        this.a1Inovaut = a1Inovaut;
    }

    public String getA1Udetfat() {
        return a1Udetfat;
    }

    public void setA1Udetfat(String a1Udetfat) {
        this.a1Udetfat = a1Udetfat;
    }

    public String getA1Uenvnf() {
        return a1Uenvnf;
    }

    public void setA1Uenvnf(String a1Uenvnf) {
        this.a1Uenvnf = a1Uenvnf;
    }

    public String getA1Cel() {
        return a1Cel;
    }

    public void setA1Cel(String a1Cel) {
        this.a1Cel = a1Cel;
    }

    public String getA1Ulogin() {
        return a1Ulogin;
    }

    public void setA1Ulogin(String a1Ulogin) {
        this.a1Ulogin = a1Ulogin;
    }

    public String getA1Usenha() {
        return a1Usenha;
    }

    public void setA1Usenha(String a1Usenha) {
        this.a1Usenha = a1Usenha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rECNO != null ? rECNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sa1010)) {
            return false;
        }
        Sa1010 other = (Sa1010) object;
        if ((this.rECNO == null && other.rECNO != null) || (this.rECNO != null && !this.rECNO.equals(other.rECNO))) {
            return false;
        }
        return true;
    }

    public String getPassword() {
        return a1Usenha;
    }

    public String getUsername() {
        return a1Ulogin;
    }

    public List<Ada010> getAda010s() {
        return ada010s;
    }

    public void setAda010s(List<Ada010> ada010s) {
        this.ada010s = ada010s;
    }

    public List<Se1010> getSe1010s() {
        return se1010s;
    }

    public List<Se1010> getSe1010sAbertos() {
        List<Se1010> lista = new ArrayList<Se1010>();
        for (Se1010 s : se1010s) {
            if (s.getE1Baixa().trim().isEmpty() && s.getDELET().trim().isEmpty()) {
                if (s.getE1Ulibfat().equalsIgnoreCase("S")) {
                    lista.add(s);
                }
            }
        }
        return lista;
    }

    public List<Se1010> getSe1010sPagos() {
        List<Se1010> lista = new ArrayList<Se1010>();
        for (Se1010 s : se1010s) {
            if (!s.getE1Baixa().trim().isEmpty() && s.getDELET().trim().isEmpty()) {
                lista.add(s);
            }
        }
        return lista;
    }

    public void setSe1010s(List<Se1010> se1010s) {
        this.se1010s = se1010s;
    }

    public List<Sf2010> getSf2010s() {
        return sf2010s;
    }

    public List<Sf2010> getSf2010sNotDel() {
        List<Sf2010> lista = new ArrayList<Sf2010>();
        for (Sf2010 s : sf2010s) {
            if (s.getDELET().trim().isEmpty()) {
                if (!s.getF2Filial().equals("LT01")) {
                    lista.add(s);
                }
            }
        }
        return lista;
    }

    public void setSf2010s(List<Sf2010> sf2010s) {
        this.sf2010s = sf2010s;
    }

    public List<Ab1010> getAb1010s() {
        return ab1010s;
    }

    public void setAb1010s(List<Ab1010> ab1010s) {
        this.ab1010s = ab1010s;
    }

    public List<Aa3010> getAa3010s() {
        return aa3010s;
    }

    public List<Aa3010> getAa3010sNotDel() {
        List<Aa3010> lista = new ArrayList<Aa3010>();
        for (Aa3010 a : aa3010s) {
            if (a.getDELET().trim().isEmpty()) {
                lista.add(a);
            }
        }
        return lista;
    }

    public void setAa3010s(List<Aa3010> aa3010s) {
        this.aa3010s = aa3010s;
    }
}
