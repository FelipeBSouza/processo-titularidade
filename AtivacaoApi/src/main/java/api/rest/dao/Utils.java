package api.rest.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.util.StringUtils;

public class Utils {

	public static String replaceAll(String campo, String caracterEncontrar, String caracterSubstituir) {

		return campo.replaceAll(caracterEncontrar, caracterSubstituir);
	}

	public int dataAtual() {

		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("yyyyMMdd");

		int dt_formatada = Integer.parseInt(formatador.format(data));

		System.out.println("data formatada: " + dt_formatada);

		return dt_formatada;
	}
	
	public String dataToString(Date data, String formato) {
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        return sdf.format(data);
    }

    public Date StringToDate(String data, String formato) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(formato);
        return (Date) formatter.parse(data);
    }
    
    public String primeiraPalavraUpCase(String teste) {
        String fMaiuscula = "";
        String[] split = teste.split(" ");
        for (String string : split) {
            fMaiuscula += StringUtils.capitalize(string);
        }

        return fMaiuscula;
    }
    
    public String removeEspacosCaracteres(String str1) {
        str1 = str1.replaceAll("[|\n]", "");
        str1 = str1.replaceAll("\t", "");
        str1 = str1.replaceAll("-", "");
        str1 = str1.replaceAll(";", "");
        str1 = str1.replaceAll(" ", "");
        return str1;
    }
    
    public String gerar(int nr_digitos) {
        String[] carct = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        String senha = "";
        for (int x = 0; x < nr_digitos; x++) {
            int j = (int) (Math.random() * carct.length);
            senha += carct[j];

        }
        return senha;
    }
    
	/*
	 * METODO DE ENCERRAMENTO DE O.S  - SE PRECISAR
	 * 
	 * 	public void encerrarOsNoIclass() {
		
		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
		
		OrdemServicoWSImplService serviceImpl = new OrdemServicoWSImplService();
		OrdemServicoWS connector = serviceImpl.getOrdemServicoWSImplPort();

		EncerrarOSIn encerrarOSIn = new EncerrarOSIn();
		ComentarioIn comentarioIn = new ComentarioIn();
		Utils utils = new Utils();		

		try {

			GregorianCalendar gregory = new GregorianCalendar();
			gregory.setTime(new Date());
			XMLGregorianCalendar xmlGregorianCalendar;
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);

			System.out.println(xmlGregorianCalendar.toString());

			encerrarOSIn.setDataEncerramento(xmlGregorianCalendar);

		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			System.out.println("erro na data: " + e);
		}

		comentarioIn.setComentario("O.S ENCERRADA DE FORMA AUTOMATICA VIA INTEGRACAO");
		comentarioIn.setResponsavel("robson.stirle");
		comentarioIn.setTipo("INFORMACAO");
		comentarioIn.setVisivelEquipe(true);
		comentarioIn.setVisivelCliente(true);

		encerrarOSIn.setCodigoOS("17495302");
		encerrarOSIn.setComentario(comentarioIn);
		encerrarOSIn.setMotivo("070002");		
		
		RespostaOut out = connector.encerrarOS(encerrarOSIn, utils.credenciais_v1(),"pt_br");
		
		System.out.println("WARNINGS");
		for (WarningOut warning : out.getWarnings()) {			
			
			System.out.println(warning.getCode());
			System.out.println(warning.getDescription());			
		}

		System.out.println("ERRORS");
		for (ErrorOut error : out.getErros()) {
			System.out.println(error.getCode());
			System.out.println(error.getDescription());	
		}		

	}
	 */

}
