package api.rest.dao;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.Funcionario;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.TipoFinalidade;
import api.rest.model.integrador.equipamento.Modelo;
import api.rest.model.integrador.fibra.PortaSplitter;
import api.rest.model.internet.Radacct;
import api.rest.model.telefonia.Numero;
import api.rest.model.telefonia.Planos;
import api.rest.model.telefonia.Sippeers;
import api.rest.response.ResponseDaoLigueApi;

// CLASSE UTILIZADA PRA FAZER INTEGRACAO COM A API QUE CONTROLA O BANCO DE DADOS POSTGRES
public class DaoLigueApi {

	public Modelo consultaModeloDescricao(String ds_modelo) {

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Modelo> response = restTemplate.exchange("http://localhost:8080/Iclass/Modelo?ds_modelo=" + ds_modelo.trim(), HttpMethod.GET, null, new ParameterizedTypeReference<Modelo>() {
		});

		Modelo modelo = response.getBody();

		return modelo;

	}

	public TabInternet consultaVelocidadeNet(String cod_ibge, String prod_totvs) {

		// SELECT * FROM integrador.tabinternet WHERE cod_ibge = ?1 AND produto_totvs = ?2 ORDER BY produto_totvs

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<TabInternet> response = restTemplate.exchange("http://localhost:8080/Iclass/velNet?codIbge=" + cod_ibge + "&prod_totvs=" + prod_totvs, HttpMethod.GET, null, new ParameterizedTypeReference<TabInternet>() {
		});

		TabInternet tabInternet = response.getBody();

		return tabInternet;

	}

	public Funcionario consultaFuncionario(String cod_funcionario) {

		// SELECT * FROM integrador.funcionario WHERE cd_funcionario = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Funcionario> response = restTemplate.exchange("http://localhost:8080/Iclass/Funcionario?cod_funcionario=" + cod_funcionario, HttpMethod.GET, null, new ParameterizedTypeReference<Funcionario>() {
		});

		Funcionario funcionario = response.getBody();

		return funcionario;

	}

	public TipoFinalidade consultaTipoFinalidade(String tp_fidelidade) {

		// SELECT * FROM integrador.funcionario WHERE cd_funcionario = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<TipoFinalidade> response = restTemplate.exchange("http://localhost:8080/Iclass/Finalidade?cd_fidelidade=" + tp_fidelidade, HttpMethod.GET, null, new ParameterizedTypeReference<TipoFinalidade>() {
		});

		TipoFinalidade tipoFinalidade = response.getBody();

		return tipoFinalidade;

	}

	public PortaSplitter consultaPortaSplitter(int cod) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<PortaSplitter> response = restTemplate.exchange("http://localhost:8080/Iclass/PortaSplitter?cd_porta=" + cod, HttpMethod.GET, null, new ParameterizedTypeReference<PortaSplitter>() {
		});

		PortaSplitter portaSplitter = response.getBody();

		return portaSplitter;

	}

	public Planos consultaPlanosYate(int cod) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Planos> response = restTemplate.exchange("http://localhost:8080/Iclass/Planos?id_plano=" + cod, HttpMethod.GET, null, new ParameterizedTypeReference<Planos>() {
		});

		Planos planos = response.getBody();

		return planos;

	}

	public Numero consultaNumeroYate(String num, String area) {

		Numero numero = new Numero();

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Numero> response = restTemplate.exchange("http://localhost:8080/Iclass/FoneIclassNumero?fone=" + num + "&area=" + area, HttpMethod.GET, null, new ParameterizedTypeReference<Numero>() {
		});

		numero = response.getBody();

		System.out.println("entrou no try, numero retorno é: " + numero.getId());

		return numero;

	}

	// recebe classe cliente e transforma essa classe em json para enviar a LigueApi
	// o retorno da chamada da api informa se deu certo ou nao o cadastro do cliente la no postgres
	public ResponseDaoLigueApi salvarCliente(Cliente clienteParam) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1
		Gson gson = new Gson();
		String json = gson.toJson(clienteParam);

		System.out.println(json);

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ResponseDaoLigueApi> response = restTemplate.exchange("http://localhost:8080/cliente/cadastrar?jsonCliente=" + json, HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDaoLigueApi>() {
		});

		ResponseDaoLigueApi responseDaoLigueApi = response.getBody(); // retorno se deu erro ou nao

		return responseDaoLigueApi;

	}

	// recebe classe sippeers e transforma essa classe em json para enviar a LigueApi
	// o retorno da chamada da api informa se deu certo ou nao o cadastro la no postgres
	public ResponseDaoLigueApi salvarSippeers(Sippeers sippeersParam) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1
		Gson gson = new Gson();
		String json = gson.toJson(sippeersParam);

		System.out.println(json);

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ResponseDaoLigueApi> response = restTemplate.exchange("http://localhost:8080/sippeers/cadastrar?jsonSippeers=" + json, HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDaoLigueApi>() {
		});

		ResponseDaoLigueApi responseDaoLigueApi = response.getBody(); // retorno se deu erro ou nao

		return responseDaoLigueApi;

	}

	public ResponseDaoLigueApi alteraNumero(Numero numeroParam) {

		// SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1
		Gson gson = new Gson();
		String json = gson.toJson(numeroParam);

		System.out.println(json);

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<ResponseDaoLigueApi> response = restTemplate.exchange("http://localhost:8080/numero/altera?jsonNumero=" + json, HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDaoLigueApi>() {
		});

		ResponseDaoLigueApi responseDaoLigueApi = response.getBody(); // retorno se deu erro ou nao

		return responseDaoLigueApi;

	}

	public List<Radacct> consultaRadacct(String username) {

		// SELECT * FROM internet.radacct WHERE username = ?1 order by username

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<Radacct>> response = restTemplate.exchange("http://localhost:8080/Iclass/RadacctUsername?username=" + username, HttpMethod.GET, null, new ParameterizedTypeReference<List<Radacct>>() {
		});

		List<Radacct> radaccts = response.getBody();

		return radaccts;

	}

	public Sippeers consultaSippersPorId(Long id) {

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<Sippeers> response = restTemplate.exchange("http://localhost:8080/sippeers/consultaId?id=" + id, HttpMethod.GET, null, new ParameterizedTypeReference<Sippeers>() {
		});

		Sippeers sippeers = response.getBody();

		return sippeers;

	}

}
