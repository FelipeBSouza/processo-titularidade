package api.rest.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import api.rest.ativacao.EquipamentoBase;
import api.rest.model.auditoria.AuditaSplitter;
import api.rest.model.integrador.Chassi;
import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.Email;
import api.rest.model.integrador.Endereco;
import api.rest.model.integrador.Funcionario;
import api.rest.model.integrador.Internet;
import api.rest.model.integrador.Pessoa;
import api.rest.model.integrador.PessoaFisica;
import api.rest.model.integrador.PessoaJuridica;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.Telefone;
import api.rest.model.integrador.TipoFinalidade;
import api.rest.model.integrador.equipamento.EquipamentoYate;
import api.rest.model.integrador.fibra.Caixa;
import api.rest.model.integrador.fibra.PortaSplitter;
import api.rest.model.integrador.fibra.Splitter;
import api.rest.model.internet.RadUserGroup;
import api.rest.model.internet.Radacct;
import api.rest.model.internet.Radcheck;
import api.rest.model.internet.Radreply;
import api.rest.model.seguranca.Usuario;
import api.rest.model.telefonia.Numero;
import api.rest.model.telefonia.Planos;
import api.rest.model.telefonia.Sippeers;
import api.rest.model.totvs.Aa3010;
import api.rest.model.totvs.Ab6010;
import api.rest.model.totvs.Ada010;
import api.rest.model.totvs.Adb010;
import api.rest.model.totvs.Aga010;
import api.rest.model.totvs.Agb010;
import api.rest.model.totvs.Sa1010;
import api.rest.model.totvs.Sz2010;
import api.rest.repositoy.AA3Repository;
import api.rest.repositoy.AB6Repository;
import api.rest.repositoy.ADARepository;
import api.rest.response.ResponseAtivacao;
import api.rest.model.integrador.equipamento.Modelo;
import br.eng.medeiros.FTTH.Equipamentos.ConexaoPadrao;
import br.eng.medeiros.FTTH.Equipamentos.EquipNokia;
import br.eng.medeiros.FTTH.Equipamentos.Equipamento;
import br.eng.medeiros.FTTH.Equipamentos.Fiber;
import br.eng.medeiros.FTTH.Equipamentos.FiberHomeANM2000;
import br.eng.medeiros.FTTH.Equipamentos.Mxk;
import br.eng.medeiros.FTTH.Equipamentos.Nokia;
import br.eng.medeiros.FTTH.Equipamentos.Numeros;
import br.eng.medeiros.FTTH.Equipamentos.Zhone;

public class DaoAtivarCliente {

//    private GenericSQLFacade genericSQLFacade;
//    private GenericFacade genericFacade;
//    private User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	private List<EquipamentoBase> equipamentos = new ArrayList<EquipamentoBase>();
	private EquipamentoBase equipamentoSelected;
	private List<Chassi> chassis = new ArrayList<Chassi>();
	private String ipChassi;
	private Chassi chassi = new Chassi();
	private Cliente cliente;
	private TipoFinalidade finalidade;
	private List<EquipamentoYate> equipYates = new ArrayList<EquipamentoYate>();
	private Caixa caixa = new Caixa();
	private List<Caixa> caixas = new ArrayList<Caixa>();
	private Splitter spliter = new Splitter();
	private PortaSplitter portaSplitter = new PortaSplitter();
	private EquipamentoYate equipYateSelected = new EquipamentoYate();
	private Boolean valCaixa = true;
	private Ada010 ada1010;
	private TabInternet net;
	private List<Numero> numeros = new ArrayList<Numero>();
	private Boolean portabilidade = false;
	private Ab6010 ab6 = new Ab6010();

	@Autowired
	private AA3Repository aa3repository;
	@Autowired
	private AB6Repository ab6repository;
	@Autowired
	private ADARepository adarepository;

	Utils utils = new Utils();
	DaoLigueApi daoLigueApi = new DaoLigueApi();

	ResponseAtivacao responseAtivacao;
	List<ResponseAtivacao> listResponse = new ArrayList<>();

	public DaoAtivarCliente(AA3Repository aa3repository, AB6Repository ab6repository, ADARepository adarepository) {
		this.aa3repository = aa3repository;
		this.ab6repository = ab6repository;
		this.adarepository = adarepository;
	}

	// tenta vincular o mac em aa3, caso de algum problema retorna lista de erros
	public List<ResponseAtivacao> vincularMac(String nr_os, String mac, String serial) {

		ResponseAtivacao responseVinculaMac;
		List<ResponseAtivacao> listResponseVinculaMac = new ArrayList<>();

		try {

			Ab6010 ab6010 = ab6repository.buscaOSPorNumeroDaOS(nr_os);

			int i = 0;
			while (i < ab6010.getAb7010s().size()) {

				Aa3010 aa3 = ab6010.getAb7010s().get(i).getAa3();

				aa3repository.vincula_mac(mac.toUpperCase(), serial.toUpperCase(), aa3.getSa1().getA1Cod().trim(),
						aa3.getAa3Codpro().trim(), aa3.getAa3Numser().trim(), aa3.getRECNO());

				i++;
			}

		} catch (Exception e) {

			System.out.println("deu erro vinculacao mac: " + e.getMessage());

			responseVinculaMac = new ResponseAtivacao();
			responseVinculaMac.setCod_erro("");
			responseVinculaMac.setMac(mac.trim());
			responseVinculaMac.setMsg_erro("Não foi possivel vincular o mac");
			responseVinculaMac.setTp_erro("ERRO");
			responseVinculaMac.setMsg_erro_sistema(e.getMessage());
			listResponseVinculaMac.add(responseVinculaMac);
		}

		return listResponseVinculaMac;

	}

	// METODO PRINCIPAL ATIVACAO CLIENTE
	// ESSE METODO VAI CHAMANDO OS DEMAIS Q FOREM NECESSARIOS
	public List<ResponseAtivacao> consultarChassi(String nr_os, String macParam, String serial,
			PortaSplitter portaSplitter) {

		// LISTA DE ERROS VINCULACAO MAC
		List<ResponseAtivacao> listResponseVinculaMac = new ArrayList<>();

		// CHAMA FUNCAO QUE VINCULA MAC NO TOTVS
		listResponseVinculaMac = vincularMac(nr_os, macParam, serial);

		// SE NAO DEU NENHUM ERRO ENTAO CONSEGUIU FAZER A VINCULACAO DO MAC
		if (listResponseVinculaMac.size() <= 0) {

			try {
				equipamentos.clear();
				List<Equipamento> lista = new ArrayList<Equipamento>();

				// dados de teste
//				chassi.setTipo("F");
//				chassi.setDs_descricao("CPM-FBH");
//				chassi.setIpAdress("10.12.0.112");
//				chassi.setCd_chassi(2);

				chassi.setTipo("N"); // FX4
				chassi.setDs_descricao("TESTE");
				chassi.setIpAdress("10.3.11.3");
				chassi.setCd_chassi(9);
				chassi.setPlacas(4);

				String mac = "";
				ConexaoPadrao cx = null;

				// VERIFICA QUAL E O TIPO DO EQUIPAMENTO EX :NOKIA, FIBER
				if (chassi.getTipo().equalsIgnoreCase("F")) {
					FiberHomeANM2000 fb = new FiberHomeANM2000("1", "1", "10.3.15.19");
					cx = fb;
					Fiber fib = new Fiber(fb);
					lista = fib.listarMac(chassi.getCd_chassi(), chassi.getIpAdress(), chassi.getDs_descricao());
				} else if (chassi.getTipo().equalsIgnoreCase("N")) {
					Nokia cxNokia = new Nokia(chassi.getIpAdress(), "isadmin", "ANS#150", 22);
					EquipNokia nk = new EquipNokia(cxNokia);
					cx = cxNokia;
					lista = nk.listarMac(chassi.getCd_chassi(), chassi.getIpAdress(), chassi.getDs_descricao());
				} else {
					Mxk mxk = new Mxk(chassi.getIpAdress(), "Marcos", "007mtss");
					cx = mxk;
					Zhone zhone = new Zhone(cx);
					lista = zhone.listarMac2(chassi.getCd_chassi(), chassi.getIpAdress());
				}

				for (Equipamento eq : lista) {
					List<Aa3010> list = new ArrayList<Aa3010>();
					eq.setConexao(cx);
					System.out.println(eq.getMac());
//	                System.out.println(eq.getMac().substring(6, eq.getMac().length()).toLowerCase());

					if (chassi.getTipo().equalsIgnoreCase("Z")) {
						list = aa3repository.buscaPorMac(eq.getMac().toUpperCase().substring(2));
					} else {
						list = aa3repository.buscaPorChapa(eq.getMac().toUpperCase());
					}

					EquipamentoBase obj;

					if (!list.isEmpty()) {
						try {
							// String adaUYate = list.get(0).getAda().getAdaUYate();
							eq.setName(list.get(0).getSa1().getA1Nome());
							obj = new EquipamentoBase(eq, list.get(0));

							if (chassi.getTipo().equalsIgnoreCase("N")) {
								// obj.getBaseAtendimento().getSb1().getB1Cod();
								// ConsultaNotaComodato(eq, obj.getBaseAtendimento().getSa1().getA1Cod(),
								// obj.getBaseAtendimento().getSa1().getA1Loja());
							} else {
								eq.setModelo(consultaModelo(eq.getModelo().getDs_modelo()));
							}
						} catch (Exception e) {
							e.printStackTrace();

							responseAtivacao = new ResponseAtivacao();
							responseAtivacao.setCod_erro("");
							responseAtivacao.setMac(eq.getMac());
							responseAtivacao.setMsg_erro("Não foi possivel consultar base de atendimento(AA3) do equipamento");
							responseAtivacao.setTp_erro("ERRO");
							responseAtivacao.setMsg_erro_sistema(e.getMessage());
							listResponse.add(responseAtivacao);

							Aa3010 aa3 = new Aa3010(new Sa1010(), new Ada010());
							obj = new EquipamentoBase(eq, aa3);
						}
					} else {
						Aa3010 aa3 = new Aa3010(new Sa1010(), new Ada010());
						obj = new EquipamentoBase(eq, aa3);
					}

					equipamentos.add(obj);
				}

				if (equipamentos.isEmpty()) {

					responseAtivacao = new ResponseAtivacao();
					responseAtivacao.setCod_erro("");
					responseAtivacao.setMac("");
					responseAtivacao.setMsg_erro(
							"Consulta foi realizada porém não foi encontrado nenhum equipamento no Chassi");
					responseAtivacao.setTp_erro("INFORMACAO");
					listResponse.add(responseAtivacao);

				} else {
					// se equipamentos nao for vazio da continuidade no cadastro do cliente

					int y = 0;
					
					
					//TRANSFORMAR WHILE PRA FOR EACH
					
					while (y < equipamentos.size()) { // laco para varrer lista de equipamentos

						String nr_contrato = ab6repository.buscaNumeroContratoOS(nr_os);

						try {

							// verifica se o numero do contrato da O.S passada por parametro e igual a algum
							// contrato da lista
							// caso seja chama funcao cadastro cliente
							System.out.println("numero do contrato da OS: " + nr_contrato);
							System.out.println("numero do contrato do chassi: "
									+ equipamentos.get(y).getBaseAtendimento().getAda().getAdaNumctr().trim());

							if (nr_contrato.trim()
									.equals(equipamentos.get(y).getBaseAtendimento().getAda().getAdaNumctr().trim())) {

//								System.out.println("cliente selecionado: " + equipamentos.get(y).getCliente().getCd_cliente());

								ada1010 = adarepository.buscaContratoPorNumero(nr_contrato);

								equipamentoSelected = equipamentos.get(y);

								cliente = totvsToYate(ada1010);

								// testar essa parte se vai corrigir o erro
								//cliente.getEquipamentos().get(0).setPortaSplitter(portaSplitter);

								salvar(); // grava dados no banco

								System.out.println("Cadastro do cliente foi realizado com sucesso");

							}

						} catch (Exception e) {
							e.printStackTrace();

							System.out.println("Não foi possivel realizar o cadastro");

							responseAtivacao = new ResponseAtivacao();
							responseAtivacao.setCod_erro("");
							responseAtivacao.setMac("");
							responseAtivacao.setMsg_erro("Não foi possivel realizar o cadastro");
							responseAtivacao.setTp_erro("INFORMACAO");
							responseAtivacao.setMsg_erro_sistema(e.getMessage());
							listResponse.add(responseAtivacao);
						}

						y++;
					}

				}

			} catch (Exception e) {
				e.printStackTrace();

				System.out.println("Não foi possivel consultar o Chassi, tente novamente mais tarde");

				responseAtivacao = new ResponseAtivacao();
				responseAtivacao.setCod_erro("");
				responseAtivacao.setMac("");
				responseAtivacao.setMsg_erro("Não foi possivel consultar o Chassi, tente novamente mais tarde");
				responseAtivacao.setTp_erro("ERRO");
				responseAtivacao.setMsg_erro_sistema(e.getMessage());
				listResponse.add(responseAtivacao);
			}

		} else { // se deu erro na vinculacao do mac entao retorna a lista de erros vindo da
					// funcao vincularMac

			listResponse.addAll(listResponseVinculaMac);

		}

		return listResponse;
	}

	public br.eng.medeiros.FTTH.Equipamentos.Modelo consultaModelo(String dsModelo) {

		DaoLigueApi daoLigueApi = new DaoLigueApi();

		Modelo modelo = (Modelo) daoLigueApi.consultaModeloDescricao(dsModelo);
		br.eng.medeiros.FTTH.Equipamentos.Modelo mod = new br.eng.medeiros.FTTH.Equipamentos.Modelo();
		mod.setCd_modelo(modelo.getCd_modelo());
		mod.setDs_modelo(modelo.getDs_modelo());
		mod.setDs_formatado(modelo.getDs_formatado());
		mod.setQt_porta_internet(modelo.getQt_porta_internet());
		mod.setQt_porta_telefone(modelo.getQt_porta_telefone());
		mod.setWifi(modelo.isWifi());

		return mod;
	}

	public void verAtenuacao() {
		equipamentoSelected.getEquipamento().atenuacao(equipamentoSelected.getEquipamento(), chassi.getIpAdress());
	}

	public Cliente totvsToYate(Ada010 ada) {
		equipYates = new ArrayList<EquipamentoYate>();
		Funcionario vend = daoLigueApi.consultaFuncionario("1");
		finalidade = daoLigueApi.consultaTipoFinalidade("1");
		cliente = new Cliente();
		cliente.setCd_vendedor(vend);
		Sa1010 sa1 = ada.getSa1010();
		Pessoa p = new Pessoa();
		p.setNm_pessoa(sa1.getA1Nome());
		p.setDt_cadastro(new Date());
		p.setIn_ativo('A');
		p.setTipo_pessoa(ada.getSa1010().getA1Pessoa());

		cliente.setPessoa(p);
		cliente.setCd_totvs(ada.getAdaNumctr());

		if (ada.getSa1010().getA1Pessoa().equals("F")) {
			PessoaFisica pf = new PessoaFisica();
			pf.setNr_cpf(sa1.getA1Cgc());
			pf.setNr_rg(sa1.getA1Pfisica());
			pf.setNm_conjugue("");
			pf.setNm_pai("");
			pf.setNm_mae("");
			pf.setDs_est_civil(1);
			pf.setDs_sexo('M');

			try {
				Date dataInicial = utils.StringToDate(sa1.getA1Dtnasc().substring(6, 8) + "/"
						+ sa1.getA1Dtnasc().substring(4, 6) + "/" + sa1.getA1Dtnasc().substring(0, 4), "dd/MM/yyyy");
				pf.setDt_nascimento(dataInicial);
			} catch (ParseException ex) {
				pf.setDt_nascimento(new Date());
			}

			cliente.getPessoa().setPessoaFisica(pf);
			pf.setPessoa(cliente.getPessoa());
		} else {
			PessoaJuridica pj = new PessoaJuridica();
			pj.setNm_fantasia(sa1.getA1Nreduz());
			pj.setNr_cnpj(sa1.getA1Cgc());
			pj.setNr_ie(sa1.getA1Inscr());

			cliente.getPessoa().setPessoaJuridica(pj);
			pj.setPessoa(cliente.getPessoa());
		}

		String ibge = "";
		if (ada.getAga010() == null) {
			ibge = sa1.getA1CodMun();
			cliente.setListaInternet(adbToInternet(ada.getAdb010sNotDel(), ibge));
			cliente.getPessoa().setListaEndereco(sa1ToEndereco(sa1, cliente));
		} else {
			ibge = ada.getAga010().getAgaMun();
			cliente.setListaInternet(adbToInternet(ada.getAdb010sNotDel(), ibge));
			cliente.getPessoa().setListaEndereco(agaToEndereco(ada.getAga010(), cliente));
		}

		cliente.getPessoa().setListaTelefone(agbToTelefone(sa1, cliente));
		cliente.getPessoa().setListaEmail(emlToEmail(sa1, cliente));

		if (!ada.getAdb010sGrupo0101().isEmpty()) {
			String codPlano = retornaPlano(ada.getAdb010sGrupo0101());

			Planos plano = consultaPlanoYate(codPlano);
			if (plano != null) {
				agbToNumero(ada.getAgb010sYate(), ibge, plano);
			}
		}

		cliente.setEquipamentos(equipYates);

		System.out
				.println("teste dentro da funcao totvsToYate: " + cliente.getEquipamentos().get(0).getPortaSplitter());

		return cliente;
	}

	public List<Internet> adbToInternet(List<Adb010> adbs, String ibge) {
		List<Internet> nets = new ArrayList<Internet>();
		String area = "CPM-NAT";
		int contAdb = 0;

		for (Adb010 adb010 : adbs) {
			if (adb010.getSb1().getB1Grupo().equals("0103") && adb010.getSb1().getB1Uitcont().equals("S")
					&& contAdb == 0) {
				String pppoe = montaPPPoE(adb010);
				Radcheck radcheck_mac = new Radcheck(pppoe, "Calling-Station-Id",
						equipamentoSelected.getEquipamento().getMac());

				String senha = "";
				if (chassi.getTipo().equalsIgnoreCase("N")) {
					senha = pppoe;
				} else {
					senha = utils.gerar(8);
				}

				Radcheck radcheck_senha = new Radcheck(pppoe, "Cleartext-Password", senha);

				// Velocidade Radius
				String velocidadeNet = velocidadeNet(adb010.getSb1().getB1Cod(), "04303");

				if (!velocidadeNet.equals("")) { // valida para q se a velocidade da net nao for encontrada entao nao da
													// continuidade
					Radreply radreply_velocidade = new Radreply(pppoe, "Mikrotik-Rate-Limit", velocidadeNet);
					// Velocidade PPPoe Plus
					// String velocidadeNetPlus = velocidadeNetPlus(adb010.getSb1().getB1Cod(),
					// ibge);
					// Radreply radreply_filter = new Radreply(pppoe, "Filter-Id",
					// velocidadeNetPlus);
					ArrayList<Radreply> replys = new ArrayList<Radreply>();
					replys.add(new Radreply(pppoe, "Jnpr-IPv6-Egress-Policy-Name",
							"out-" + net.getDownload().intValue() + "M-v6"));
					replys.add(new Radreply(pppoe, "Jnpr-IPv6-Ingress-Policy-Name",
							"in-" + net.getUpload().intValue() + "M-v6"));
					replys.add(new Radreply(pppoe, "Unisphere-Egress-Policy-Name",
							"out-" + net.getDownload().intValue() + "M"));
					replys.add(new Radreply(pppoe, "Unisphere-Ingress-Policy-Name",
							"in-" + net.getUpload().intValue() + "M"));
					replys.add(new Radreply(pppoe, "Huawei-Output-Average-Rate",
							String.valueOf(net.getDownload().intValue() * 1000000)));
					replys.add(new Radreply(pppoe, "Huawei-Input-Average-Rate",
							String.valueOf(net.getUpload().intValue() * 1000000)));

					RadUserGroup rdGroup = new RadUserGroup(pppoe, area);

					Internet i = new Internet(radcheck_senha, radcheck_mac, radreply_velocidade);
					i.setCd_radcheck_mac(radcheck_mac);
					i.setCd_radcheck_senha(radcheck_senha);
					i.setCd_radreply_velocidade(radreply_velocidade);
					i.setRadUserGroup(rdGroup);
					i.setDt_ativacao(new Date());
					i.setCd_cliente(cliente);
					i.setArea(area);
					i.setValor(adb010.getAdbTotal());
					i.setRadreplys(replys);

					nets.add(i);
				}

				contAdb++;
			}
		}
		return nets;
	}

	public List<Endereco> sa1ToEndereco(Sa1010 sa1, Cliente cli) {
		List<Endereco> list = new ArrayList<Endereco>();

		Endereco end = new Endereco();
		end.setDs_bairro(sa1.getA1Bairro());
		end.setDs_cidade(sa1.getA1Mun());
		end.setDs_complemento(sa1.getA1Complem());
		end.setDs_logradouro(sa1.getA1End());
		end.setDs_uf(sa1.getA1Est());
		end.setIn_ativo('A');
		end.setIn_preferencial('S');
		end.setNr_cep(sa1.getA1Cep());
		end.setNr_numero("");
		end.setCd_pessoa(cliente.getPessoa());

		aa3ToEquipamento(end);
		list.add(end);
		return list;
	}

	public List<Endereco> agaToEndereco(Aga010 aga, Cliente cli) {
		List<Endereco> list = new ArrayList<Endereco>();

		Endereco end = new Endereco();
		end.setDs_bairro(aga.getAgaBairro());
		end.setDs_cidade(aga.getAgaMun());
		end.setDs_complemento(aga.getAgaComp());
		end.setDs_logradouro(aga.getAgaEnd());
		end.setDs_uf(aga.getAgaEst());
		end.setIn_ativo('A');
		end.setIn_preferencial('S');
		end.setNr_cep(aga.getAgaCep());
		end.setNr_numero("");
		end.setCd_pessoa(cliente.getPessoa());

		aa3ToEquipamento(end);
		list.add(end);
		return list;
	}

	public List<Telefone> agbToTelefone(Sa1010 sa1, Cliente cli) {
		List<Telefone> list = new ArrayList<Telefone>();
		Telefone fone = new Telefone();
		fone.setCd_finalidade(finalidade);
		fone.setCd_pessoa(cli.getPessoa());
		fone.setNr_telefone(sa1.getA1Ddd() + sa1.getA1Tel());

		list.add(fone);
		return list;
	}

	public List<Email> emlToEmail(Sa1010 sa1, Cliente cli) {
		List<Email> list = new ArrayList<Email>();
		Email email = new Email();
		email.setCd_finalidade(finalidade);
		email.setCd_pessoa(cli.getPessoa());
		email.setDs_email(sa1.getA1Email());

		list.add(email);
		return list;
	}

	public void agbToNumero(List<Agb010> agbs, String ibge, Planos plano) {
		int cont = 0;
		portabilidade = false;
		numeros = new ArrayList<Numero>();
		String numeroCob = "";

		for (Agb010 agb : agbs) {
			String area = retornaArea(ibge);

			String ddd = agb.getAgbDdd().replace("/[^0-9]+/g", "").trim();
			ddd = String.valueOf(Integer.parseInt(ddd)); // Remover Zero a Esquerda se tiver.
			String fone = agb.getAgbTelefo().replace("/[^0-9]+/g", "").trim();

			Numero numero;

			// validacao para nao dar erro na consulta dos numeros
			if (!ddd.equals("") && !fone.equals("") && !area.equals("")) {
				numero = daoLigueApi.consultaNumeroYate(ddd + fone, area);
			} else {
				numero = null;
			}

			if (numero != null) {
				numero.setStatus("A");
				if (cont == 0) {
					numeroCob = numero.getNumero();
				}
				numero.setTipo("SIP");
				numero.setNumerocob(numeroCob);
				numero.setDestino(numero.getNumero());
				numero.setDdr(numero.getNumero());

				numero.setPlano(plano);
				numero.setFranquia(plano.getFranquia_local());
				numero.setFranquia_ldn(plano.getFranquia_ldn());
				numero.setFranquia_movel(plano.getFranquia_celular());
				numero.setLimite(plano.retornaLimite());

				numero.setArea(area);
				numero.setPrepos('O');
				numero.setData_inclusao(new Date());
				numero.setCodecs("ulaw,alaw,g729");

				numero.setPermite_intra(true);
				numero.setPermite_fix_Local(true);
				numero.setPermite_fix_ddd(true);
				numero.setPermite_mov_ddd(true);
				numero.setPermite_mov_local(true);
				numero.setPermite_internacional(true);
				numero.setPermite_a_cobrar(true);
				numero.setPermite_0300(true);
				numero.setPermite_ldn_terceiros(true);
				numero.setPermite_sgsempre(true);
				numero.setPermite_sgnaoatende(true);
				numero.setPermite_sgocupado(true);

				if (numero.getSippeers() == null) {
					Sippeers sippeers = new Sippeers();

					sippeers.setName(numero.getNumero());
					sippeers.setAccountcode(numero.getNumero());
					sippeers.setCallerid(numero.getNumero());
					sippeers.setSecret(utils.gerar(8));

					numero.setSippeers(sippeers);
				}
				cont++;

				numero.setCd_cliente(cliente);
				numeros.add(numero);
			} else {
				portabilidade = true;
			}
		}
	}

	public String retornaArea(String ibge) {
		if (ibge.equalsIgnoreCase("04303")) {
			return "CPM";
		} else if (ibge.equalsIgnoreCase("01705")) {
			return "CPM";
		} else if (ibge.equalsIgnoreCase("05508")) {
			return "CNE";
		} else if (ibge.equalsIgnoreCase("15200")) {
			return "MGA";
		} else if (ibge.equalsIgnoreCase("07504")) {
			return "CPM";
		} else if (ibge.equalsIgnoreCase("17503")) {
			return "MGA";
		} else if (ibge.equalsIgnoreCase("14807")) {
			return "MGA";
		}
		return "";
	}

	public String retornaPlano(List<Adb010> adbs) {
		for (Adb010 adb : adbs) {
			if (!adb.getAdbUmsg().trim().isEmpty()) {
				String[] plan = adb.getAdbUmsg().split("-");
				return plan[0].trim();
			}
		}
		return "";
	}

	public Planos consultaPlanoYate(String cod) {
		try {
			Planos plano = daoLigueApi.consultaPlanosYate(Integer.parseInt(cod));
			return plano;
		} catch (Exception e) {
			return null;
		}
	}

	public void aa3ToEquipamento(Endereco end) {

		EquipamentoYate equip = new EquipamentoYate();
		equip.setEndereco(end);
		equip.setIp_http(chassi.getIpAdress() + ":");
		equip.setLogin("ADMIN");
		equip.setSenha(utils.gerar(8));
		equip.setSerial(equipamentoSelected.getEquipamento().getMac());
		equip.setMac(equipamentoSelected.getBaseAtendimento().getAa3Umac().toUpperCase());
		equip.setPosicao_mxk(chassi.getCd_chassi() + "-" + equipamentoSelected.getEquipamento().getPlaca() + "-"
				+ equipamentoSelected.getEquipamento().getPorta() + "-"
				+ equipamentoSelected.getEquipamento().getPosicao());

		if (chassi.getTipo().equalsIgnoreCase("F")) {
			equip.setCd_tipo_equipamento(1);
			equip.setCd_modelo(9);
		}
		if (chassi.getTipo().equalsIgnoreCase("N")) {
			equip.setCd_tipo_equipamento(1);
			equip.setCd_modelo(15);
		} else {
			equip.setCd_tipo_equipamento(1);
			equip.setCd_modelo(1);
		}

		try {
			String porta = equipamentoSelected.getBaseAtendimento().getAa3UPorta().trim();

			if (!porta.isEmpty()) {
				String[] split = porta.split(":");
				int cod = Integer.parseInt(split[0]);

				portaSplitter = daoLigueApi.consultaPortaSplitter(cod);

				System.out.println("porta splitter: " + portaSplitter);

				equip.setPortaSplitter(portaSplitter);

				// SETAR USUARIO
				Usuario user = new Usuario();
				user.setLogin("robson.stirle");
				user.setSenha("robson000");
				user.setTipo("S");
				user.setCd_usuario(70);
				user.setDs_usuario("robson.stirle");

				AuditaSplitter aud = new AuditaSplitter(user, "I", equip, equip.getPortaSplitter());
				equip.getAuditaSplitters().add(aud);

				spliter = portaSplitter.getSplitter();
				caixa = spliter.getCaixa();
			}
		} catch (Exception e) {

			System.out.println("erro na funcao aa3ToEquipamento: " + e.getMessage());

			responseAtivacao = new ResponseAtivacao();
			responseAtivacao.setCod_erro("");
			responseAtivacao.setMac("");
			responseAtivacao.setMsg_erro("Erro na funcao aa3ToEquipamento:");
			responseAtivacao.setTp_erro("ERRO");
			responseAtivacao.setMsg_erro_sistema(e.getMessage());
			listResponse.add(responseAtivacao);
		}

		System.out.println("porta teste: " + equipamentoSelected.getEquipamento().getPorta());

		System.out.println("dados splitter: " + equip.getPortaSplitter());

		equipYates.add(equip);
	}

	public String montaPPPoE(Adb010 adb010) {
		String pppoe = "";
		if (chassi.getTipo().equals("N")) {
			EquipNokia nk = (EquipNokia) equipamentoSelected.getEquipamento();
			nk.getConexao().login();
			nk.consultarPosicao(equipamentoSelected.getEquipamento());
			nk.getConexao().logout();

			pppoe = chassi.getDs_descricao() + " eth 1/1/";

			String placa = retornaPlacaNokia(Integer.parseInt(equipamentoSelected.getEquipamento().getPlaca()));

			pppoe += placa + "/";
			pppoe += String.format("%02d", Integer.parseInt(equipamentoSelected.getEquipamento().getPorta())) + "/";
			pppoe += equipamentoSelected.getEquipamento().getPosicao() + "/";
			pppoe += "14/1";

		} else {
			if (chassi.getTipo().equals("F")) {
				verAtenuacao();
			}

			pppoe = utils.removeEspacosCaracteres(
					utils.primeiraPalavraUpCase(adb010.getAda010().getSa1010().getA1Nome().toLowerCase()));
			if (pppoe.length() >= 12) {

				pppoe = pppoe.substring(0, 12);
			}
			pppoe += adb010.getAda010().getAdaNumctr() + adb010.getAdbItem();
		}
		return pppoe;
	}

	public String retornaPlacaNokia(Integer placa) {
		if (chassi.getPlacas() == 16 && !chassi.getDs_descricao().contains("CPM")) {
			if (placa > 8) {
				placa -= 1;
			} else {
				placa += 2;
			}
		}

		if (chassi.getPlacas() == 8) {
			placa -= 2;
		}

		if (chassi.getDs_descricao().contains("DRG")) {
			if (placa == 2) {
				placa = 4;
			}
			if (placa == 7) {
				placa = 9;
			}
			if (placa == 15) {
				placa = 14;
			}
		}

		return String.format("%02d", placa);
	}

	public String velocidadeNet(String prodTotvs, String ibge) {

		String velocidade = "";

		try {
			net = daoLigueApi.consultaVelocidadeNet(ibge, prodTotvs.replace(" ", ""));
			velocidade = String
					.valueOf(String.valueOf(net.getUpload().intValue()) + "M/" + net.getDownload().intValue()) + "M";

		} catch (Exception e) {
			System.out
					.println("INFORMACAO: Não foi possivel localizar uma velocidade de internet para o Produto Totvs :"
							+ prodTotvs);

			responseAtivacao = new ResponseAtivacao();
			responseAtivacao.setCod_erro("");
			responseAtivacao.setMac("");
			responseAtivacao.setMsg_erro(
					"Não foi possivel localizar uma velocidade de internet para o Produto Totvs :" + prodTotvs);
			responseAtivacao.setTp_erro("ERRO");
			responseAtivacao.setMsg_erro_sistema(e.getMessage());
			listResponse.add(responseAtivacao);
		}

		return velocidade;
	}

	public void salvar() {

		System.out.println("porta: " + cliente.getEquipamentos().get(0).getPortaSplitter());

		try {
			DateTime dtInicio = new DateTime();

			// dando erro aqui
//            System.out.println("porta: " + cliente.getEquipamentos().get(0).getPortaSplitter());

			Boolean valida = true;
			for (EquipamentoYate o : cliente.getEquipamentos()) {
				PortaSplitter porta = o.getPortaSplitter();
				if (porta == null) {
					valida = false;
				}
				if (porta != null && porta.getEquipamento() != null) {
					if (porta.getEquipamento().getCd_equipamento() != o.getCd_equipamento()) {
						valida = false;
					}
				}
			}

			if (valida) {

				daoLigueApi.salvarCliente(cliente);

				for (Numero n : numeros) {
					Sippeers sippeer = n.getSippeers();
					daoLigueApi.salvarSippeers(sippeer);

					n.setSippeers(sippeer);
					n.setCd_cliente(cliente);
					daoLigueApi.alteraNumero(n);
				}

//                Ab9010 ab9 = adicionaAtendimento(dtInicio);
//                genericSQLFacade.salvar(ab9);

				ada1010.setAdaUYate(String.valueOf(cliente.getCd_cliente()));
				adicionaVigencia();
				adarepository.save(ada1010);

				ativarEquipamento(cliente.getListaInternet());
				ativarTelefone(numeros);
				equipamentos.remove(equipamentoSelected);

				System.out.println("Cliente cadastrado com Sucesso ! Código do Yate " + ada1010.getAdaUYate());

				ada1010 = null;
			} else {

				System.out.println(
						"Não foi possivel continuar o Cadastro. Porta selecionada já está sendo utilizada ou não foi informada !");

				responseAtivacao = new ResponseAtivacao();
				responseAtivacao.setCod_erro("");
				responseAtivacao.setMac("");
				responseAtivacao.setMsg_erro(
						"Não foi possivel continuar o Cadastro. Porta selecionada já está sendo utilizada ou não foi informada !");
				responseAtivacao.setTp_erro("INFORMACAO");
				responseAtivacao.setMsg_erro_sistema("");
				listResponse.add(responseAtivacao);
			}
		} catch (Exception e) {
			e.printStackTrace();

			System.out.println("Não foi possivel efetuar todo o processo !");

			responseAtivacao = new ResponseAtivacao();
			responseAtivacao.setCod_erro(e.getCause().toString());
			responseAtivacao.setMac("");
			responseAtivacao.setMsg_erro("Não foi possivel efetuar todo o processo !");
			responseAtivacao.setTp_erro("ERRO");
			responseAtivacao.setMsg_erro_sistema(e.getMessage());
			listResponse.add(responseAtivacao);
		}

	}

	public void adicionaVigencia() {
		String grupoInternet = "0103";

		for (Adb010 adb : ada1010.getAdb010sNotDel()) {
			if (adb.getSb1().getB1Uitcont().equals("S")) {
				if (adb.getSb1().getB1Grupo().equals(grupoInternet)) {
					formataVigencia(adb);
				} else {
					if (!portabilidade) {
						formataVigencia(adb);
					}
				}
			} else {
				DateTime data1 = new DateTime(new Date());
				String dtIni = "";
				String dtFim = "";
				if (data1.getDayOfMonth() <= ada1010.getAdaUdiafe()) {
					DateTime data2 = data1.minusMonths(1);
					data1 = data2.plusMonths(adb.getAdbUMesco());

					dtIni = String.format("%02d", data2.getYear()) + String.format("%02d", data2.getMonthOfYear());
					dtFim = String.format("%02d", data1.getYear()) + String.format("%02d", data1.getMonthOfYear());
				} else {
					DateTime data2 = data1.plusMonths(adb.getAdbUMesco());
					dtIni = String.format("%02d", data1.getYear()) + String.format("%02d", data1.getMonthOfYear());
					dtFim = String.format("%02d", data2.getYear()) + String.format("%02d", data2.getMonthOfYear());
				}
				Double adaUdiafe = ada1010.getAdaUdiafe();
				int diaf = adaUdiafe.intValue() + 1;
				dtIni += String.format("%02d", diaf);
				dtFim += String.format("%02d", diaf);
				if (adb.getAdbUdtini().trim().isEmpty()) {
					adb.setAdbUdtini(dtIni);
				}
				if (adb.getAdbUdtfim().trim().isEmpty()) {
					adb.setAdbUdtfim(dtFim);
				}
			}
		}
	}

	public void formataVigencia(Adb010 adb) {
		String acao = "INICIO VIGENCIA";
		DateTime data1 = new DateTime(new Date());

		DateTime data2 = data1.plusMonths(adb.getAdbUMesin());

		String dtIni = String.format("%02d", data2.getYear()) + String.format("%02d", data2.getMonthOfYear())
				+ String.format("%02d", data2.getDayOfMonth());
		if (adb.getAdbUdtini().trim().isEmpty()) {
			adb.setAdbUdtini(dtIni);

			for (Sz2010 sz2 : adb.getSz2010s()) {
				if (sz2.getZ2Acao().contains(acao) && sz2.getAb7().getAb6().getAb6Numos().equals(ab6.getAb6Numos())) {
					sz2.getAb7().setAb7Tipo("5");

					if (portabilidade) {
						sz2.getAb7().getAb6().setAb6Ucodat("000159");
						sz2.getAb7().getAb6().setAb6Atend("INST - PORTABILIDADE     ");
					} else {
						sz2.getAb7().getAb6().setAb6Ucodat("000160");
						sz2.getAb7().getAb6().setAb6Atend("INST - ENCERRAMENTO DE OS");
					}
				}
			}
		}
	}

	public void ativarEquipamento(List<Internet> list) {
		for (Internet internet : list) {
			String pppoe = internet.getCd_radcheck_senha().getUsername();
			String senha = internet.getCd_radcheck_senha().getValue();
			System.out.println(pppoe);
			System.out.println(senha);

			equipamentoSelected.getEquipamento().ativarInternet(pppoe, senha, equipamentoSelected.getEquipamento(),
					chassi.getIpAdress());

			try {
				if (chassi.getTipo().equalsIgnoreCase("Z")) {
					Thread.sleep(8000);
				} else {
					Thread.sleep(1000);
				}
			} catch (InterruptedException ex) {

				System.out.println("Erro na ativacao do equipamento");

				responseAtivacao = new ResponseAtivacao();
				responseAtivacao.setCod_erro("");
				responseAtivacao.setMac("");
				responseAtivacao.setMsg_erro("Erro na ativacao do equipamento");
				responseAtivacao.setTp_erro("ERRO");
				responseAtivacao.setMsg_erro_sistema(ex.getMessage());
				listResponse.add(responseAtivacao);
			}

			List<Radacct> ccts = daoLigueApi.consultaRadacct(pppoe);

			if (!ccts.isEmpty()) {
				Radacct cct = ccts.get(ccts.size() - 1);

				responseAtivacao = new ResponseAtivacao();
				responseAtivacao.setCod_erro("");
				responseAtivacao.setMac("");
				responseAtivacao.setMsg_erro(
						"Pppoe : " + pppoe + " foi registrado na Radacct, Concentrador: " + cct.getNasipaddress()
								+ " IP : " + cct.getFramedipaddress() + " Time: " + cct.getAcctstarttime());
				responseAtivacao.setTp_erro("INFORMACAO");
				responseAtivacao.setMsg_erro_sistema(
						"Pppoe : " + pppoe + " foi registrado na Radacct, Concentrador: " + cct.getNasipaddress()
								+ " IP : " + cct.getFramedipaddress() + " Time: " + cct.getAcctstarttime());
				listResponse.add(responseAtivacao);

			} else {

				System.out.println("falhou ao autenticar na Radacct !");

				responseAtivacao = new ResponseAtivacao();
				responseAtivacao.setCod_erro("");
				responseAtivacao.setMac("");
				responseAtivacao.setMsg_erro("Pppoe : " + pppoe + " falhou ao autenticar na Radacct !");
				responseAtivacao.setTp_erro("ERRO");
				responseAtivacao.setMsg_erro_sistema("Pppoe : " + pppoe + " falhou ao autenticar na Radacct !");
				listResponse.add(responseAtivacao);
			}
		}
	}

	public void ativarTelefone(List<Numero> list) {
		List<Numeros> listPassword = new ArrayList<Numeros>();
		for (Numero numero : list) {
			Numeros num = new Numeros();
			num.setNumero(numero.getNumero());
			Long codSip = numero.getSippeers().getId();
			Sippeers sip = daoLigueApi.consultaSippersPorId(codSip);
			numero.setSippeers(sip);

			num.setPassword(numero.getSippeers().getSecret());

			listPassword.add(num);
		}
		if (!listPassword.isEmpty()) {
			equipamentoSelected.getEquipamento().ativarTelefone(listPassword, equipamentoSelected.getEquipamento(),
					chassi.getIpAdress());

			System.out.println("Pppoe : ");

			responseAtivacao = new ResponseAtivacao();
			responseAtivacao.setCod_erro("");
			responseAtivacao.setMac("");
			responseAtivacao.setMsg_erro("Pppoe : ");
			responseAtivacao.setTp_erro("INFORMACAO");
			responseAtivacao.setMsg_erro_sistema("Pppoe : ");
			listResponse.add(responseAtivacao);
		}
	}

}
