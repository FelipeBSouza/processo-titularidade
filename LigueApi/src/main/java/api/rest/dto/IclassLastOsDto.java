package api.rest.dto;

public class IclassLastOsDto {
	
	public String lastOs;
	public String status;
	
	public IclassLastOsDto(String lastOs, String status) {
		super();
		this.lastOs = lastOs;
		this.status = status;
	}
	
	public String getLastOs() {
		return lastOs;
	}
	public void setLastOs(String lastOs) {
		this.lastOs = lastOs;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
