package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.rest.model.integrador.TabInternet;

@Repository
public interface TabInternetRepository extends CrudRepository<TabInternet, Long> {
	
	@Query(value = "SELECT * FROM integrador.tabinternet WHERE cod_ibge = ?1 AND produto_totvs = ?2 ORDER BY produto_totvs", nativeQuery = true)
	public TabInternet consultaVelocidadeNet(String ibge, String prod_totvs);		
}
