package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.Produto_venda;

@Repository
public interface PlanosVendaRepository extends CrudRepository<Produto_venda, Integer>{
		
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_fim_promocao = '' and p.dt_ini_promocao = '' and p.visivel = 'S' and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.visivel = 'S' and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao and p.visivel = 'S'", nativeQuery = true)
	public List<Produto_venda> listaPorCidadeUfBairro(String cidade, String uf, String bairro);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.cod_prod_totvs = ?4 and p.dt_fim_promocao = '' and p.dt_ini_promocao = '' and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.cod_prod_totvs = ?4 and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.cod_prod_totvs = ?4 and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao order by qtd_mes_cob ASC", nativeQuery = true)
	public List<Produto_venda> buscaProdutoObrigatorio(String cidade, String uf, String bairro, String cod_produto);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.referencia = ?4 and p.visivel = 'S' and p.dt_fim_promocao = '' and p.dt_ini_promocao = '' and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.visivel = 'S' and p.referencia = ?4 and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.referencia = ?4 and p.visivel = 'S' and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao", nativeQuery = true)
	public List<Produto_venda> buscaProdutoPorReferencia(String cidade, String uf, String bairro, String cod_produto);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.valor_cheio >= ?4 and p.prod_obrig_2 = '' and p.prod_obrig_3 = '' and p.prod_obrig_4 = '' and p.prod_obrig_5 = '' and p.tp_produto = 'NET' and p.visivel = 'S' and p.dt_fim_promocao = '' and p.dt_ini_promocao = '' and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.valor_cheio >= ?4 and p.prod_obrig_2 = '' and p.prod_obrig_3 = '' and p.prod_obrig_4 = '' and p.prod_obrig_5 = '' and p.tp_produto = 'NET' and p.visivel = 'S' and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.valor_cheio >= ?4 and p.prod_obrig_2 = '' and p.prod_obrig_3 = '' and p.prod_obrig_4 = '' and p.prod_obrig_5 = '' and p.tp_produto = 'NET' and p.visivel = 'S' and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao", nativeQuery = true)
	public List<Produto_venda> buscaProdutoMaisProximo(String cidade, String uf, String bairro, String tp_produto);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and (p.combo = ?4 or p.combo = 'A') and p.tp_produto like %?5% and p.visivel = 'S' and p.dt_fim_promocao = '' and p.dt_ini_promocao = '' and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and (p.combo = ?4 or p.combo = 'A') and p.tp_produto like %?5% and p.visivel = 'S' and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and (p.combo = ?4 or p.combo = 'A') and p.tp_produto like %?5% and p.visivel = 'S' and TO_CHAR(current_date, 'YYYYMMDD') >= p.dt_ini_promocao AND TO_CHAR(current_date, 'YYYYMMDD') <= p.dt_fim_promocao", nativeQuery = true)
	public List<Produto_venda> listaPorCidadeUfBairroCombo(String cidade, String uf, String bairro, String combo, String tp_produto);
	
	
	/* MODIFICADO EM 13-09-19
	 * 
	 * 
	 * @Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.dt_ini_promocao != '' ) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.visivel = 'S'", nativeQuery = true)
	public List<Produto_venda> listaPorCidadeUfBairro(String cidade, String uf, String bairro);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.cod_prod_totvs = ?4 and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.dt_ini_promocao != '' and p.cod_prod_totvs = ?4) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != ''and p.cod_prod_totvs = ?4 order by qtd_mes_cob ASC", nativeQuery = true)
	public List<Produto_venda> buscaProdutoObrigatorio(String cidade, String uf, String bairro, String cod_produto);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.referencia = ?4 and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.dt_ini_promocao != '' and p.referencia = ?4) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != ''and p.referencia = ?4 and p.visivel = 'S'", nativeQuery = true)
	public List<Produto_venda> buscaProdutoPorReferencia(String cidade, String uf, String bairro, String cod_produto);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.valor_cheio >= ?4 and p.prod_obrig_2 = '' and p.prod_obrig_3 = '' and p.prod_obrig_4 = '' and p.prod_obrig_5 = '' and p.tp_produto = 'NET' and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.dt_ini_promocao != '' and p.valor_cheio >= ?4 and p.prod_obrig_2 = '' and p.prod_obrig_3 = '' and p.prod_obrig_4 = '' and p.prod_obrig_5 = '' and p.tp_produto = 'NET') union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.valor_cheio >= ?4 and p.prod_obrig_2 = '' and p.prod_obrig_3 = '' and p.prod_obrig_4 = '' and p.prod_obrig_5 = '' and p.tp_produto = 'NET' and p.visivel = 'S'", nativeQuery = true)
	public List<Produto_venda> buscaProdutoMaisProximo(String cidade, String uf, String bairro, String tp_produto);
	
	@Query(value="select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and (p.combo = ?4 or p.combo = 'A') and p.tp_produto like %?5% and p.cod_prod_totvs not in (select p.cod_prod_totvs from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.combo = ?4 and p.tp_produto like %?5%) union select * from integrador.produto_venda p where p.id_uf = ?2 and p.id_cidade = ?1 and p.id_bairro = ?3 and p.dt_ini_promocao != '' and p.combo = ?4 and p.tp_produto like %?5% and p.visivel = 'S'", nativeQuery = true)
	public List<Produto_venda> listaPorCidadeUfBairroCombo(String cidade, String uf, String bairro, String combo, String tp_produto);
	 * 
	 */

}
