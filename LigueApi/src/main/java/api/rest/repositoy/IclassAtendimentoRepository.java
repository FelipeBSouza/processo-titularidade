package api.rest.repositoy;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.IclassAtendimentos;
import api.rest.model.integrador.IclassCabecalho;
import api.rest.model.integrador.IclassMateriais;

@Repository
public interface IclassAtendimentoRepository extends CrudRepository<IclassAtendimentos, Integer> {
	
	
}
