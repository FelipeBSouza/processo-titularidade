package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.TipoFinalidade;


@Repository
public interface TipoFinalidadeRepository extends CrudRepository<TipoFinalidade, Long> {
	
	@Query(value = "SELECT * FROM integrador.finalidade WHERE cd_finalidade = ?1", nativeQuery = true)
	public TipoFinalidade consultaFinalidade(int cd_fidelidade);		
}
