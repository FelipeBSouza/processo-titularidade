package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.TipoFinalidade;
import api.rest.model.integrador.fibra.PortaSplitter;


@Repository
public interface PortaSplitterRepository extends CrudRepository<PortaSplitter, Long> {
	
	@Query(value = "SELECT * FROM integrador.porta_splitter WHERE cd_porta = ?1", nativeQuery = true)
	public PortaSplitter consultaPortaSplitter(int cd_porta);		
}
