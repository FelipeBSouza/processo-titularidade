package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.PlanoCorrespondente;
import api.rest.model.integrador.Products;

@Repository
public interface PlanosCorrespondentesRepository extends CrudRepository<PlanoCorrespondente, Integer> {

	@Query(value = "SELECT * FROM integrador.planos_correspondentes p where p.cod_plano_original = ?1 and p.qtd_pontos_adicionais = ?2", nativeQuery = true)
	public List<PlanoCorrespondente> listaPlanoCorrespondenteTV(String cod_plano, String qtd_pontos_adicionais);
	
	@Query(value = "SELECT * FROM integrador.planos_correspondentes p where p.cod_plano_original = ?1 and p.qtd_pontos_adicionais = ?2", nativeQuery = true)
	public List<PlanoCorrespondente> buscaTipoPlano(String cod_plano, String qtd_pontos_adicionais);

	@Query(value = "SELECT tp_plano FROM integrador.planos_correspondentes p where p.cod_plano_original = ?1 and p.qtd_pontos_adicionais = ?2", nativeQuery = true)
	public String retornaTipoPlano(String cod_plano, String qtd_pontos_adicionais);
	
}
