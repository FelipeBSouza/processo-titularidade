package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import api.rest.model.integrador.Products;

@Repository
public interface ProdutosSVARepository extends CrudRepository<Products, Integer> {

    @Query(value = "SELECT * FROM (SELECT * FROM integrador.products p WHERE p.id_uf = 'PR' AND p.id_cidade = ?1 AND p.id_bairro = ?2 AND p.dt_fim_promo = '' AND p.dt_ini_promo = '' AND p.visivel = 'S' AND p.categoria = ?6 AND p.tp_produto LIKE %?3% AND p.id_promocao = ?4 AND p.fidelidade like %?5% AND p.cod_totvs NOT IN (SELECT p.cod_totvs FROM integrador.products p WHERE p.id_uf = 'PR' AND p.id_cidade = ?1 AND p.id_bairro = ?2 AND p.visivel = 'S' AND p.categoria = ?6 AND p.tp_produto LIKE %?3% AND p.id_promocao = ?4 AND p.fidelidade like %?5% AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') >= p.dt_ini_promo AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') <= p.dt_fim_promo) UNION SELECT * FROM integrador.products p WHERE p.id_uf = 'PR' AND p.id_cidade = ?1 AND p.id_bairro = ?2 AND p.tp_produto LIKE %?3% AND p.id_promocao = ?4 AND p.fidelidade like %?5% AND p.categoria = ?6 AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') >= p.dt_ini_promo AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') <= p.dt_fim_promo) AS p ORDER BY  p.tp_produto ASC", nativeQuery = true)
    public List<Products> listaProdutosP1Geral(String cidade, String bairro, String tp_produto, String idPromo, String fidelidade, String categoria);
    
    @Query(value = "SELECT * FROM integrador.products p WHERE p.referencia = ?1", nativeQuery = true)
    public Products buscaPlanoPorReferencia(String referencia);
    
    @Query(value = "SELECT p.referencia FROM integrador.products p WHERE p.id_cidade = ?1 and p.id_bairro = ?2 and p.tp_produto = ?3 and p.id_promocao = ?4 and p.fidelidade = ?5", nativeQuery = true)
    public String buscaReferenciaPlanoPorCidade(String cidade, String bairro, String tp_produto, String id_promocao,String fidelidade);
    
    @Query(value = "SELECT * FROM integrador.products p WHERE p.cod_totvs = ?1 and p.id_cidade = ?2 and p.id_bairro = ?3 and p.fidelidade = ?4", nativeQuery = true)
    public List<Products> buscaReferenciaPlanoCorrespondente(String cod_prod, String cidade, String bairro, String fidelidade);
    
    @Query(value = "SELECT * FROM integrador.products p WHERE p.cod_totvs = ?1 and p.id_cidade = ?2 and p.id_bairro = ?3 and p.fidelidade = ?4 and p.categoria = 'P1'", nativeQuery = true)
    public List<Products> buscaReferenciaPlanoCorrespondenteP1(String cod_prod, String cidade, String bairro, String fidelidade);
    
    @Query(value = "SELECT * FROM integrador.products p WHERE p.referencia = ?1", nativeQuery = true)
    public List<Products> listPlanoPorReferencia(String referencia);
    
    @Query(value = "SELECT * FROM integrador.products p WHERE p.id_uf = 'PR' AND p.id_cidade = ?1 AND p.id_bairro = ?2 AND p.fidelidade like %?3% AND p.cod_totvs NOT IN (SELECT p.cod_totvs FROM integrador.products p WHERE p.id_uf = 'PR' AND p.id_cidade = ?1 AND p.id_bairro = ?2 AND p.fidelidade like %?3% AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') >= p.dt_ini_promo AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') <= p.dt_fim_promo) UNION (SELECT * FROM integrador.products p WHERE p.id_uf = 'PR' AND p.id_cidade = ?1 AND p.id_bairro = ?2 AND p.fidelidade like %?3% AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') >= p.dt_ini_promo AND TO_CHAR(CURRENT_DATE, 'YYYYMMDD') <= p.dt_fim_promo)", nativeQuery = true)
    public List<Products> listaProdutosGeral(String cidade, String bairro, String fidelidade);
    
    @Query(value = "SELECT p.categoria FROM integrador.products p WHERE p.referencia = ?1", nativeQuery = true)
    public String buscaCategoria(String referencia);

   }
