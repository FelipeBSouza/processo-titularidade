package api.rest.repositoy;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.equipamento.Modelo;


@Repository
public interface ModeloRepository extends CrudRepository<Modelo, Long> {
	
	@Query(value = "SELECT * FROM integrador.cad_modelo WHERE ds_modelo = ?1", nativeQuery = true)
	public Modelo consultaModeloDescricao(String ds_modelo);		
}
