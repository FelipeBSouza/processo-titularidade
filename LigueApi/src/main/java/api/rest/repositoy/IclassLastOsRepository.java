package api.rest.repositoy;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.integrador.IclassLastOs;

@Repository
public interface IclassLastOsRepository extends CrudRepository<IclassLastOs, Integer>{
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO integrador.iclass_last_os(last_os, status) VALUES (?1, 'ToProcess')", nativeQuery = true)
	public void insertLastOs(String last_os);	
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE integrador.iclass_last_os SET status = ?1 WHERE id = ?2", nativeQuery = true)
	public void updateLastOs(String status, Long id);	
	
	@Query(value = "SELECT id, last_os, status FROM integrador.iclass_last_os WHERE id = (SELECT MAX(id) FROM integrador.iclass_last_os)", nativeQuery = true)
	public IclassLastOs searchLastOs();	

}
