package api.rest.repositoy;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.rest.model.integrador.IntegracaoAutentique;

@Repository
public interface IntegracaoAutentiqueRepository extends CrudRepository<IntegracaoAutentique, Integer> {
	
	//@Query(value = "select * from integrador.integracao_autentique where ctr_ass = 'A'", nativeQuery = true) 
	//@Query(value = "select * from integrador.integracao_autentique tb1 where ctr_ass = 'A' and tb1.processo <> 'Titularidade1' and tb1.processo <> 'Titularidade2' order by cd_integracao_autentique desc", nativeQuery = true) 
	//@Query(value = "select * from integrador.integracao_autentique where ctr_ass = 'A' and processo is null order by cd_integracao_autentique desc", nativeQuery = true) 
	@Query(value = "select * from integrador.integracao_autentique where ctr_ass = 'A' and processo is null and cd_integracao_autentique >= 22912  order by cd_integracao_autentique asc", nativeQuery = true) 
	public List<IntegracaoAutentique> listaContratosASeremBuscados();	
	
	//TRANFERENCIA DE TITULARIDADE SO VAI MOVIMENTAR SE AMBOS CONTRATOS FORAM ASSINADOS
	@Query(value = "select * from integrador.integracao_autentique tb1 where tb1.ctr_ass = 'A' and (tb1.processo = 'Titularidade1' or tb1.processo = 'Titularidade2') order by tb1.cd_integracao_autentique desc", nativeQuery = true) 
	public List<IntegracaoAutentique> listaContratosASeremBuscadosTitularidade();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE integrador.integracao_autentique SET mov_sol = 'S' WHERE nr_solicitacao = ?1", nativeQuery = true)
	public void marca_solicitacao_enviada(String nr_solicitacao);

	//@Query(value = "select * from integrador.integracao_autentique where nr_solicitacao=?1", nativeQuery = true)  
	@Query(value = "select * from integrador.integracao_autentique where nr_solicitacao = ?1 and cd_integracao_autentique = (select max(cd_integracao_autentique) from integrador.integracao_autentique where nr_solicitacao = ?1)", nativeQuery = true)  
	public List<IntegracaoAutentique> buscaContratosPorSolicitacao(String nr_solicitacao);
	
	@Query(value = "select * from integrador.integracao_autentique where id_contrato = ?1 and cd_integracao_autentique = (select max(cd_integracao_autentique) from integrador.integracao_autentique where id_contrato = ?1)", nativeQuery = true)  
	public List<IntegracaoAutentique> buscaContratosPorIdContratoTitularidade(String id_contrato);
	
	@Query(value = "select distinct nr_solicitacao from integrador.integracao_autentique where id_contrato = ?1 and cd_integracao_autentique = (select max(cd_integracao_autentique) from integrador.integracao_autentique where id_contrato = ?1)", nativeQuery = true)  
	public String buscaSolicitacaoPorIdContratoTitularidade(String id_contrato);
	
	@Query(value = "select * from integrador.integracao_autentique where nr_solicitacao = ?1 and processo = 'Titularidade2'", nativeQuery = true)  
	public List<IntegracaoAutentique> buscaContratosPorSolicitacaoTitularidade2(String nr_solicitacao);
	
	@Query(value = "select * from integrador.integracao_autentique where nr_solicitacao = ?1", nativeQuery = true)  
	public List<IntegracaoAutentique> buscaTodosContratosPorSolicitacao(String nr_solicitacao);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE integrador.integracao_autentique SET contrato_assinado=?2, ctr_ass = ?3, obs_rejeitado=?4, movement_status=?5 WHERE id_contrato = ?1", nativeQuery = true)
	public void gravaLink(String id_contrato, String link_assinado, String assinado, String obs_rejeitado, String movement_status);
	
	@Query(value = "select nr_solicitacao from integrador.integracao_autentique tb1 where tb1.ctr_ass != 'A' and tb1.mov_sol = 'N' and tb1.processo is null group by tb1.nr_solicitacao having count(tb1.nr_solicitacao) = (select count(tb2.nr_solicitacao)from integrador.integracao_autentique tb2 where tb2.nr_solicitacao = tb1.nr_solicitacao and tb2.mov_sol = 'N')", nativeQuery = true)  
	public List<String> listaSolicitacoesASeremMovimentadas();
	
	//@Query(value = "select id_contrato from integrador.integracao_autentique tb1 where tb1.ctr_ass != 'A' and tb1.mov_sol = 'N' and tb1.processo = 'Titularidade1 and (select count(tb2.nr_solicitacao) from integrador.integracao_autentique tb2 where tb2.ctr_ass != 'A' and tb2.mov_sol = 'N' and tb2.processo = 'Titularidade' and tb1.nr_solicitacao = tb2.nr_solicitacao) = 2 group by tb1.nr_solicitacao having count(tb1.nr_solicitacao) = (select count(tb2.nr_solicitacao)from integrador.integracao_autentique tb2 where tb2.nr_solicitacao = tb1.nr_solicitacao and tb2.mov_sol = 'N')", nativeQuery = true)  
	@Query(value = "select tb1.id_contrato from integrador.integracao_autentique tb1 where tb1.ctr_ass != 'A' and tb1.mov_sol = 'N' and tb1.processo = 'Titularidade1' and (select count(tb2.nr_solicitacao) from integrador.integracao_autentique tb2 where tb2.ctr_ass != 'A' and tb2.mov_sol = 'N' and tb2.nr_solicitacao = tb1.nr_solicitacao) = 2 group by tb1.nr_solicitacao,tb1.id_contrato", nativeQuery = true)  
	public List<String> listaSolicitacoesASeremMovimentadasTitularidade();
	
	@Query(value = "select * from integrador.integracao_autentique where id_contrato=?1", nativeQuery = true)  
	public IntegracaoAutentique buscaContratoPeloId(String id_contrato);
	
	@Query(value = "select * from integrador.integracao_autentique where id_contrato=?1", nativeQuery = true)  
	public List<IntegracaoAutentique> listBuscaContratoPeloId(String id_contrato);
	
	@Query(value = "select * from integrador.integracao_autentique where id_anexo=?1", nativeQuery = true)  
	public IntegracaoAutentique buscaContratoPeloIdAnexo(String id_anexo);
	
	@Transactional
	@Modifying
	@Query(value = "update integrador.integracao_autentique	set ctr_ass = 'S', mov_sol = 'S', obs_rejeitado = 'cancelada via fluig'	where nr_solicitacao =?1", nativeQuery = true)
	public void alteraRegistroPorSolicitacao(String nr_solicitacao);
	
}
