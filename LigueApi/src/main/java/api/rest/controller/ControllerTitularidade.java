package api.rest.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;
import java.util.Optional;

import javax.print.attribute.standard.Media;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dao.Utils;
import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;
import api.rest.repositoy.NumeroRepository;
import api.rest.repositoy.TitularidadeRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/titularidade")
@Api(value = "API REST CADASTRO TITULAR")
public class ControllerTitularidade {
	
	@Autowired
	private TitularidadeRepository titularidadeRepository;
	
	@Autowired
	private NumeroRepository numeroRepository;
		
	Numero numero;
	
	@ApiOperation(value = "Realiza Novo Cadastro do Titular")
	@PostMapping(value = "/cadastrar", produces = "application/json")	
	public ResponseEntity<Titularidade> cadastrar(@RequestParam ("nome") String nome, @RequestParam("nr_cpf_cnpj") String nr_cpf_cnpj, @RequestParam ("nr_rg_ie") String nr_rg_ie) {			
				
		System.out.println("nome cliente funcao cadastrar: " + nome);
		
		Titularidade titularidade = new Titularidade();
		
		titularidade.setNome(Utils.replaceAll(nome, "_", " "));
		titularidade.setNr_cpf_cnpj(Utils.replaceAll(nr_cpf_cnpj, "_", " "));
		titularidade.setNr_rg_ie(Utils.replaceAll(nr_rg_ie, "_", " "));
		
		Titularidade titularidadeCadastrada = titularidadeRepository.save(titularidade);
		
		System.out.println("CADASTRO TITULARIDADE: " + titularidadeCadastrada.getCd_titular());
				
		return new ResponseEntity<Titularidade>(titularidadeCadastrada, HttpStatus.OK);		
				
	}	
	
	@ApiOperation(value = "Altera o Cadastro do Titular")
	@PutMapping(value = "/alterar", produces = "application/json")
	public ResponseEntity<Titularidade> alterar(@RequestParam ("cd_titular") int cd_titular, @RequestParam ("nome") String nome, @RequestParam("nr_cpf_cnpj") String nr_cpf_cnpj, @RequestParam ("nr_rg_ie") String nr_rg_ie) {
				
		System.out.println("nome cliente funcao alterar: " + nome);
		
		Titularidade titularidade = new Titularidade();
		
		titularidade.setCd_titular(cd_titular);
		titularidade.setNome(Utils.replaceAll(nome, "_", " "));
		titularidade.setNr_cpf_cnpj(Utils.replaceAll(nr_cpf_cnpj, "_", " "));
		titularidade.setNr_rg_ie(Utils.replaceAll(nr_rg_ie, "_", " "));
		
		Titularidade titularidadeCadastrada = titularidadeRepository.save(titularidade);
				
		return new ResponseEntity<Titularidade>(titularidadeCadastrada, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Consulta titular pelo cpf ou cnpj")
	@GetMapping(value = "/consultaCpfCnpj", produces = "application/json")
	public String titularidadePorCpfCnpj(@RequestParam ("cpf_cnpj") String cpf_cnpj) {
		
		String retorno = "";
		int i = 0;
		int maiorValor = 0;
				
		System.out.println("entrou na funcao titularidadePorCpfCnpj, dados recebidos: " + cpf_cnpj);
				
		try {						
					
			List<Titularidade> list  = (List<Titularidade>) titularidadeRepository.buscaPorCpfCnpj(cpf_cnpj);
			
			if (list.size() > 0) {
				
				while(i < list.size()) {
					
//					System.out.println("titular [" + i + "]; " + list.get(i).getCd_titular());
					
					
					if (list.get(i).getCd_titular() > maiorValor) {
						maiorValor = list.get(i).getCd_titular();
					}
					
					i++;
				}
				
				retorno = Integer.toString(maiorValor);
				
			}else {
				
				retorno = "NAO ENCONTRADO";
				
			}		
			
			System.out.println("print do retorna da funcao consulta titular pelo cpf ou cpnj: " + retorno);
			System.out.println("maior valor e: " + maiorValor);
			
		}catch(Exception e) {
			
			retorno = "ERRO";
			System.out.println("Deu erro na consulta do titular pelo cpf ou cnpj: " + e);
			
		}
			
		return retorno;	
			
	}

	
	
//	@GetMapping(value = "/{cd_titular}", produces = "application/json")
//	public ResponseEntity<Titularidade> titularidadePorId(@PathVariable (value = "cd_titular") Integer cd_titular) {
//		
//				
//		Optional<Titularidade> titularidade = titularidadeRepository.findById(cd_titular);
//						
//		System.out.println("dados da titularidade nome: " + titularidade.get().getNome());
//		System.out.println("numeros: " + titularidade.get().getNumeros());		
//		
//		
//		
//		//if (titularidade != null) {
//			//return new ResponseEntity(titularidade.get(), HttpStatus.OK);
//			
//		return new ResponseEntity<Titularidade>(titularidade.get(), HttpStatus.OK);
//		//}else {
//		//	return new ResponseEntity(titularidade.get(), HttpStatus.NOT_FOUND);
//		//}
//			
//	}
	
//	@ApiOperation(value = "Realiza consulta do titular")
//	@GetMapping(value = "/", produces = "application/json")	
//	public ResponseEntity<List<Titularidade>> titularidades(){
//		
//		List<Titularidade> list = (List<Titularidade>) titularidadeRepository.findAll();
//		
//		return new ResponseEntity<List<Titularidade>>(list, HttpStatus.OK);
//	}

}
