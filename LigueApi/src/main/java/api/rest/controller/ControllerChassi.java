package api.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.rest.model.integrador.Chassi;
import api.rest.repositoy.ChassiRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/chassi")
@Api(value = "API REST CHASSI")
public class ControllerChassi {

    @Autowired
    private ChassiRepository chassiRepository;

    @ApiOperation(value = "Consulta chassi")
    @GetMapping(value = "/consultaChassi", produces = "application/json")
    public ResponseEntity<List<Chassi>> consultaChassi() {

        System.out.println("entrou na funcao consultaChassi");

        List<Chassi> listChassi = new ArrayList<>();
        listChassi = chassiRepository.consultaChassi();

        return new ResponseEntity<List<Chassi>>(listChassi, HttpStatus.OK);

    }

    // @GetMapping(value = "/{cd_titular}", produces = "application/json")
    // public ResponseEntity<Titularidade> titularidadePorId(@PathVariable (value =
    // "cd_titular") Integer cd_titular) {
    //
    //
    // Optional<Titularidade> titularidade =
    // titularidadeRepository.findById(cd_titular);
    //
    // System.out.println("dados da titularidade nome: " +
    // titularidade.get().getNome());
    // System.out.println("numeros: " + titularidade.get().getNumeros());
    //
    //
    //
    // //if (titularidade != null) {
    // //return new ResponseEntity(titularidade.get(), HttpStatus.OK);
    //
    // return new ResponseEntity<Titularidade>(titularidade.get(), HttpStatus.OK);
    // //}else {
    // // return new ResponseEntity(titularidade.get(), HttpStatus.NOT_FOUND);
    // //}
    //
    // }

    // @ApiOperation(value = "Realiza consulta do titular")
    // @GetMapping(value = "/", produces = "application/json")
    // public ResponseEntity<List<Titularidade>> titularidades(){
    //
    // List<Titularidade> list = (List<Titularidade>)
    // titularidadeRepository.findAll();
    //
    // return new ResponseEntity<List<Titularidade>>(list, HttpStatus.OK);
    // }

}
