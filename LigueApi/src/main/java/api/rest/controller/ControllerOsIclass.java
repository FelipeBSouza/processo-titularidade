package api.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties.Sentinel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dto.IclassLastOsDto;
import api.rest.model.integrador.IclassLastOs;
import api.rest.repositoy.IclassLastOsRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/osiclass")
@Api(value = "API DE CADASTRO E CONSULTA DE ULTIMA ORDEM ENVIADA AO ICLASS")
public class ControllerOsIclass {
	
	@Autowired
	private IclassLastOsRepository lastOsRepository;
	
	@ApiOperation(value = "Return Last Os Iclass")	
	@GetMapping(value = "/lastOs", produces = "application/json")
	public ResponseEntity<IclassLastOs> getLastOs(){
		
		// last os = 21114401
		try {
			IclassLastOs lastOs = lastOsRepository.searchLastOs();
			
			if(!lastOs.getStatus().equals("processing")) {
				lastOsRepository.updateLastOs("processing", lastOs.getId());
			}
			
			return new ResponseEntity<IclassLastOs>(lastOs, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("Error ControllerOsIclass getLastOs" + e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation(value = "Update Last Os Iclass")	
	@PutMapping(value = "/lastOsUpdate", produces = "application/json")
	public ResponseEntity<String> putLastOs(@RequestBody IclassLastOs iclassLastOs){
		
		try {
			lastOsRepository.updateLastOs(iclassLastOs.getStatus(), iclassLastOs.getId());
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("Error ControllerOsIclass putLastOs" + e);
			return new ResponseEntity<String>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation(value = "Insert Last Os Iclass")	
	@PostMapping(value = "/lastOsInsert", produces = "application/json")
	public ResponseEntity<String> PostLastOs(@RequestBody IclassLastOs iclassLastOs){
		
		try {
			lastOsRepository.insertLastOs(iclassLastOs.getLast_os());
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("Error ControllerOsIclass PostLastOs" + e);
			return new ResponseEntity<String>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
