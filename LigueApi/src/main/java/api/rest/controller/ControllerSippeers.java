package api.rest.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;
import java.util.Optional;

import javax.print.attribute.standard.Media;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import api.rest.dao.Utils;
import api.rest.model.integrador.Cliente;
import api.rest.model.integrador.TipoFinalidade;
import api.rest.model.integrador.Titularidade;
import api.rest.model.telefonia.Numero;
import api.rest.model.telefonia.Sippeers;
import api.rest.repositoy.ClienteRepository;
import api.rest.repositoy.NumeroRepository;
import api.rest.repositoy.SippeersRepository;
import api.rest.repositoy.TitularidadeRepository;
import api.rest.response.ResponseDaoLigueApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/sippeers")
@Api(value = "API REST CADASTRO SIPPEERS")
public class ControllerSippeers {
		
	@Autowired
	private SippeersRepository sippeersRepository;
			
	@ApiOperation(value = "Realiza Cadastro do Sippeers")
	@PostMapping(value = "/cadastrar", produces = "application/json")	
	public ResponseEntity<ResponseDaoLigueApi> cadastrar(@RequestParam (value = "jsonSippeers") String jsonSippeers) {			
				
		System.out.println("entrou funcao cadastrar sippeers");
		ResponseDaoLigueApi responseDaoLigueApi;
		
		try {
			Sippeers sippeers = new Gson().fromJson(jsonSippeers, Sippeers.class);	
						
			Sippeers clienteCadastrado = sippeersRepository.save(sippeers);
			
			responseDaoLigueApi = new ResponseDaoLigueApi();
			responseDaoLigueApi.setCODCLI("" + clienteCadastrado.getId());
			responseDaoLigueApi.setMSGERRO("Sippeers Cadastrado com Sucesso");
			responseDaoLigueApi.setCODERRO("201");
			
		}catch(Exception e) {
			responseDaoLigueApi = new ResponseDaoLigueApi();	
			responseDaoLigueApi.setCODCLI("");
			responseDaoLigueApi.setMSGERRO(e.getMessage());
			responseDaoLigueApi.setCODERRO(e.getCause().toString());
		}		
						
		return new ResponseEntity<ResponseDaoLigueApi>(responseDaoLigueApi, HttpStatus.OK);		
				
	}	
	
	@ApiOperation(value = "Realiza consulta Sippeers por id")
	@GetMapping(value = "/consultaId", produces = "application/json")
	public ResponseEntity<Sippeers> consultaSippeersId(@RequestParam("id") Long id) {

		Optional<Sippeers> sippeers = sippeersRepository.findById(id);

		return new ResponseEntity<Sippeers>(sippeers.get(), HttpStatus.OK);
	}

}
