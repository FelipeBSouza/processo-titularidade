package api.rest.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import api.rest.dao.DaoPlanosVenda;
import api.rest.dao.Utils;
import api.rest.dto.PlanosVendaDto;
import api.rest.model.integrador.Produto_venda;
import api.rest.repositoy.PlanosVendaRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/planos")
@Api(value = "API CADASTRA PLANOS/PROMOCOES")
public class ControllerPlanos {

    @Autowired
    private PlanosVendaRepository produto_vendaRepository;

    Utils utils = new Utils();
    DaoPlanosVenda daoPlanosVendaDto = new DaoPlanosVenda();

    @ApiOperation(value = "Consulta planos com filtros de cidade, uf, bairro")
    @GetMapping(value = "/filtro", produces = "application/json")
    public ResponseEntity<List<PlanosVendaDto>> listaPorCidadeUfBairro(@RequestParam("cidade") String cidade,
            @RequestParam("uf") String uf, @RequestParam("bairro") String bairro) {

        List<PlanosVendaDto> listFinal = new ArrayList<>();

        // busca com os filtros de uf, cidade, bairro
        listFinal = daoPlanosVendaDto.converteList((List<Produto_venda>) produto_vendaRepository.listaPorCidadeUfBairro(
                cidade.replaceAll("_", " "), uf.replaceAll("_", " "), bairro.replaceAll("_", " ")));

        // senao retornar nada na consulta acima
        if (listFinal.size() <= 0) {
            // entao filtra por uf, cidade
            listFinal = daoPlanosVendaDto.converteList((List<Produto_venda>) produto_vendaRepository
                    .listaPorCidadeUfBairro(cidade.replaceAll("_", " "), uf.replaceAll("_", " "), "TODOS"));

        }

        return new ResponseEntity<List<PlanosVendaDto>>(listFinal, HttpStatus.OK);

    }

    @ApiOperation(value = "Consulta planos com filtros de cidade, uf, bairro e codigo do plano")
    @GetMapping(value = "/plano", produces = "application/json")
    public ResponseEntity<List<PlanosVendaDto>> listaPorCodigo(@RequestParam("cidade") String cidade,
            @RequestParam("uf") String uf, @RequestParam("bairro") String bairro,
            @RequestParam("produto") String produto) {

        List<PlanosVendaDto> listFinal = new ArrayList<>();

        // busca com os filtros de uf, cidade, bairro e codigo do produto
        listFinal = daoPlanosVendaDto.converteList(
                (List<Produto_venda>) produto_vendaRepository.buscaProdutoObrigatorio(cidade.replaceAll("_", " "),
                        uf.replaceAll("_", " "), bairro.replaceAll("_", " "), produto));

        // senao retornar nada na consulta acima
        if (listFinal.size() <= 0) {
            // entao filtra por uf, cidade e codigo do produto
            listFinal = daoPlanosVendaDto.converteList((List<Produto_venda>) produto_vendaRepository
                    .buscaProdutoObrigatorio(cidade.replaceAll("_", " "), uf.replaceAll("_", " "), "TODOS", produto));

        }

        return new ResponseEntity<List<PlanosVendaDto>>(listFinal, HttpStatus.OK);

    }

    @ApiOperation(value = "Consulta planos com filtros de cidade, uf, bairro e referencia")
    @GetMapping(value = "/referencia", produces = "application/json")
    public ResponseEntity<List<PlanosVendaDto>> listaPorReferencia(@RequestParam("cidade") String cidade,
            @RequestParam("uf") String uf, @RequestParam("bairro") String bairro,
            @RequestParam("referencia") String referencia) {

        List<PlanosVendaDto> listFinal = new ArrayList<>();

        // busca com os filtros de uf, cidade, bairro e codigo do produto
        listFinal = daoPlanosVendaDto.converteList(
                (List<Produto_venda>) produto_vendaRepository.buscaProdutoPorReferencia(cidade.replaceAll("_", " "),
                        uf.replaceAll("_", " "), bairro.replaceAll("_", " "), referencia));

        // senao retornar nada na consulta acima
        if (listFinal.size() <= 0) {
            // entao filtra por uf, cidade e codigo do produto
            listFinal = daoPlanosVendaDto.converteList(
                    (List<Produto_venda>) produto_vendaRepository.buscaProdutoPorReferencia(cidade.replaceAll("_", " "),
                            uf.replaceAll("_", " "), "TODOS", referencia));

        }

        return new ResponseEntity<List<PlanosVendaDto>>(listFinal, HttpStatus.OK);

    }

    @ApiOperation(value = "Consulta planos com filtros de cidade, uf, bairro e valor cheio, retornando os planos com valores iguais ou acima do valor do parametro (planos somente de internet)")
    @GetMapping(value = "/proximo", produces = "application/json")
    public ResponseEntity<List<PlanosVendaDto>> listaPorTipoPlano(@RequestParam("cidade") String cidade,
            @RequestParam("uf") String uf, @RequestParam("bairro") String bairro, @RequestParam("valor") String valor) {

        List<PlanosVendaDto> listFinal = new ArrayList<>();

        // busca com os filtros de uf, cidade, bairro e tipo do plano
        listFinal = daoPlanosVendaDto.converteList(
                (List<Produto_venda>) produto_vendaRepository.buscaProdutoMaisProximo(cidade.replaceAll("_", " "),
                        uf.replaceAll("_", " "), bairro.replaceAll("_", " "), valor));

        // senao retornar nada na consulta acima
        if (listFinal.size() <= 0) {
            // entao filtra por uf, cidade e tipo do plano
            listFinal = daoPlanosVendaDto.converteList((List<Produto_venda>) produto_vendaRepository
                    .buscaProdutoMaisProximo(cidade.replaceAll("_", " "), uf.replaceAll("_", " "), "TODOS", valor));

        }

        return new ResponseEntity<List<PlanosVendaDto>>(listFinal, HttpStatus.OK);

    }

    @ApiOperation(value = "Realiza Novo Cadastro do Plano")
    @PostMapping(value = "/cadastrar", produces = "application/json")
    public ResponseEntity<Produto_venda> cadastrar(@RequestBody Produto_venda novoPlano) {

        System.out.println("descricao do plano cadastrar: " + novoPlano.getDescricao());

        Produto_venda novoPlanoCadastrado = produto_vendaRepository.save(novoPlano);

        return new ResponseEntity<Produto_venda>(novoPlanoCadastrado, HttpStatus.OK);

    }

    @ApiOperation(value = "Consulta planos com filtros de cidade, uf, bairro, pelo tipo se e combo ou nao (S;N)")
    @GetMapping(value = "/combo", produces = "application/json")
    public ResponseEntity<List<PlanosVendaDto>> listaPorCidadeUfBairroCombo(@RequestParam("cidade") String cidade,
            @RequestParam("uf") String uf, @RequestParam("bairro") String bairro, @RequestParam("combo") String combo,
            @RequestParam("tp_produto") String tp_produto) {

        List<PlanosVendaDto> listFinal = new ArrayList<>();

        // busca com os filtros de uf, cidade, bairro
        listFinal = daoPlanosVendaDto.converteList(
                (List<Produto_venda>) produto_vendaRepository.listaPorCidadeUfBairroCombo(cidade.replaceAll("_", " "),
                        uf.replaceAll("_", " "), bairro.replaceAll("_", " "), combo, tp_produto));

        // senao retornar nada na consulta acima
        if (listFinal.size() <= 0) {
            // entao filtra por uf, cidade
            listFinal = daoPlanosVendaDto
                    .converteList((List<Produto_venda>) produto_vendaRepository.listaPorCidadeUfBairroCombo(
                            cidade.replaceAll("_", " "), uf.replaceAll("_", " "), "TODOS", combo, tp_produto));

        }

        return new ResponseEntity<List<PlanosVendaDto>>(listFinal, HttpStatus.OK);

    }

}
