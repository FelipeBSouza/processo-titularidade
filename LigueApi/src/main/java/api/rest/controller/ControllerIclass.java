package api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dao.Utils;
import api.rest.model.integrador.ControleOS;
import api.rest.model.integrador.Funcionario;
import api.rest.model.integrador.TabInternet;
import api.rest.model.integrador.TipoFinalidade;
import api.rest.model.integrador.equipamento.Modelo;
import api.rest.model.integrador.fibra.PortaSplitter;
import api.rest.model.internet.Radacct;
import api.rest.model.telefonia.Numero;
import api.rest.model.telefonia.Planos;
import api.rest.repositoy.ControleOSRepository;
import api.rest.repositoy.FuncionarioRepository;
import api.rest.repositoy.IclassAtendimentoRepository;
import api.rest.repositoy.IclassCabecalhoRepository;
import api.rest.repositoy.IclassMateriaisRepository;
import api.rest.repositoy.ModeloRepository;
import api.rest.repositoy.NumeroRepository;
import api.rest.repositoy.PlanosRepository;
import api.rest.repositoy.PortaSplitterRepository;
import api.rest.repositoy.RadacctRepository;
import api.rest.repositoy.TabInternetRepository;
import api.rest.repositoy.TipoFinalidadeRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/Iclass")
@Api(value = "API REST FUNCOES ICLASS")
public class ControllerIclass {

    @Autowired
    private TabInternetRepository tabInternetRepository;
    @Autowired
    private FuncionarioRepository funcionarioRepository;
    @Autowired
    private TipoFinalidadeRepository finalidadeRepository;
    @Autowired
    private PortaSplitterRepository portaSplitterRepository;
    @Autowired
    private PlanosRepository planosRepository;
    @Autowired
    private ModeloRepository modeloRepository;
    @Autowired
    private NumeroRepository numeroRepository;
    @Autowired
    private RadacctRepository radacctRepository;
    @Autowired
    private ControleOSRepository controleOSRepository;
    @Autowired
    private IclassAtendimentoRepository atendimentoRepository;
    @Autowired
    private IclassMateriaisRepository iclassMateriaisRepository;
    @Autowired
    private IclassCabecalhoRepository cabecalhoRepository;

  
    @ApiOperation(value = "Realiza consulta velocidade internet")
	@GetMapping(value = "/velNet", produces = "application/json")
	public ResponseEntity<TabInternet> consultaVelocidadeNet(@RequestParam("codIbge") String codIbge, @RequestParam("prod_totvs") String prod_totvs) {

		TabInternet tabInternet = tabInternetRepository.consultaVelocidadeNet(codIbge, prod_totvs);

		return new ResponseEntity<TabInternet>(tabInternet, HttpStatus.OK);
	}

	@ApiOperation(value = "Realiza consulta funcionario por id")
	@GetMapping(value = "/Funcionario", produces = "application/json")
	public ResponseEntity<Funcionario> consultaFuncionarioCodigo(@RequestParam("cod_funcionario") int cod_funcionario) {

		Funcionario funcionario = funcionarioRepository.consultaFuncionarioCodigo(cod_funcionario);

		return new ResponseEntity<Funcionario>(funcionario, HttpStatus.OK);
	}

	@ApiOperation(value = "Realiza consulta finalidade por id")
	@GetMapping(value = "/Finalidade", produces = "application/json")
	public ResponseEntity<TipoFinalidade> consultaFinalidadeCodigo(@RequestParam("cd_fidelidade") int cd_fidelidade) {

		TipoFinalidade tipoFinalidade = finalidadeRepository.consultaFinalidade(cd_fidelidade);

		return new ResponseEntity<TipoFinalidade>(tipoFinalidade, HttpStatus.OK);
	}

	@ApiOperation(value = "Realiza consulta porta_splitter por porta")
	@GetMapping(value = "/PortaSplitter", produces = "application/json")
	public ResponseEntity<PortaSplitter> consultaPortaSplitter(@RequestParam("cd_porta") int cd_porta) {

		PortaSplitter tipoFinalidade = portaSplitterRepository.consultaPortaSplitter(cd_porta);

		return new ResponseEntity<PortaSplitter>(tipoFinalidade, HttpStatus.OK);
	}

	@ApiOperation(value = "Realiza consulta plano pelo codigo do plano")
	@GetMapping(value = "/Planos", produces = "application/json")
	public ResponseEntity<Planos> consultaPlanosPorCodigo(@RequestParam("id_plano") int id_plano) {

		Planos planos = planosRepository.consultaPlanoId(id_plano);

		return new ResponseEntity<Planos>(planos, HttpStatus.OK);
	}

	@ApiOperation(value = "Realiza consulta modelo pela descricao do modelo")
	@GetMapping(value = "/Modelo", produces = "application/json")
	public ResponseEntity<Modelo> consultaModeloPorDescricao(@RequestParam("ds_modelo") String ds_modelo) {

		Modelo modelo = modeloRepository.consultaModeloDescricao(ds_modelo);

		return new ResponseEntity<Modelo>(modelo, HttpStatus.OK);
	}

	@ApiOperation(value = "Realiza consulta fone pelo numero")
	@GetMapping(value = "/FoneIclassNumero", produces = "application/json")
	public ResponseEntity<Numero> consultaFonePorNumero(@RequestParam("fone") String fone, @RequestParam("area") String area) {

		Numero numero = new Numero();

		Utils utils = new Utils();

		if (utils.temNumero(fone)) {

			numero = numeroRepository.buscaIclassNumero(fone); // 4438010624

		} else {
			numero = numeroRepository.buscaPorArea(area);
		}

		return new ResponseEntity<Numero>(numero, HttpStatus.OK);
	}

//	@ApiOperation(value = "Realiza consulta fone pela area")
//	@GetMapping(value = "/FoneIclassArea", produces = "application/json")
//	public ResponseEntity<Numero> consultaFonePorArea(@RequestParam("area") String area) {
//		
//		Numero numero = numeroRepository.buscaPorArea(area);
//
//		return new ResponseEntity<Numero>(numero, HttpStatus.OK);
//	}	

	@ApiOperation(value = "Realiza consulta fone pela area")
	@GetMapping(value = "/RadacctUsername", produces = "application/json")
	public ResponseEntity<List<Radacct>> consultaRadacctPorUserName(@RequestParam("username") String username) {

		List<Radacct> radaccts = radacctRepository.buscaPorUserName(username);

		return new ResponseEntity<List<Radacct>>(radaccts, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Realiza consulta se existe controle para aquela OS ")
	@GetMapping(value = "/consultaOSTabela", produces = "application/json")
	public ResponseEntity<ControleOS> consultaExisteOSTabela(@RequestParam("nr_os") String nr_os) {

		List<ControleOS> list = controleOSRepository.consultaSeExisteOS(nr_os);
		ControleOS controleOS;
		
		if (list.size() > 0) {
			controleOS = list.get(0);
		}else {
			controleOS = new ControleOS();
			controleOS.setNr_os(nr_os);
			controleOS.setNr_os_sequencial(nr_os);
			controleOS.setId(0);
			controleOS.setDt_movimentacao("");
		}

		return new ResponseEntity<ControleOS>(controleOS, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Realiza consulta do sequencial atual da OS")
	@GetMapping(value = "/consultaSequencialAtualOS", produces = "application/json")
	public ResponseEntity<ControleOS> consultaSequencialAtualOS(@RequestParam("nr_os") String nr_os) {

		List<ControleOS> list = controleOSRepository.consultaSequencialAtualOS(nr_os);
		ControleOS controleOS;
		
		if (list.size() > 0) {
			controleOS = list.get(0);
		}else {
			controleOS = new ControleOS();
			controleOS.setNr_os(nr_os);
			controleOS.setNr_os_sequencial(nr_os);
			controleOS.setId(0);
			controleOS.setDt_movimentacao("");
		}

		return new ResponseEntity<ControleOS>(controleOS, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Busca proxima sequencia ControleOS")
	@PostMapping(value = "/consultaSeq", produces = "application/json")
	public ResponseEntity<ControleOS> proximaSequenciaControleOS(@RequestParam("nr_os") String nr_os) {

		List<ControleOS> list = controleOSRepository.proximaSequencia(nr_os);
		ControleOS controleOS;
		ControleOS controleOSExistente;
		ControleOS controleOSGravada;
		
		Utils utils = new Utils();
		String dt_movimentacao =  "" + utils.dataAtual();
		
		if (list.size() > 0) {
			
			controleOSExistente = list.get(0);	
			
			int seq = controleOSExistente.getSequencial() + 1;
		
			controleOS = new ControleOS();
			controleOS.setNr_os(controleOSExistente.getNr_os());
			
			controleOS.setDt_movimentacao(dt_movimentacao);
			controleOS.setSequencial(seq);
			controleOS.setNr_os_sequencial(nr_os + "A" + seq);
			controleOS.setId(null);
			
			controleOSGravada = controleOSRepository.save(controleOS);
			
		}else {
			controleOS = new ControleOS();
			controleOS.setNr_os(nr_os);
			controleOS.setNr_os_sequencial(nr_os + "A1");
			controleOS.setDt_movimentacao(dt_movimentacao);
			controleOS.setSequencial(1);
			
			controleOSGravada = controleOSRepository.save(controleOS);
		}

		return new ResponseEntity<ControleOS>(controleOSGravada, HttpStatus.OK);
	}
	
//	@ApiOperation(value = "Grava dados da OS getCloseOrdens no Postgres")
//	@PostMapping(value = "/gravaDadosOS", produces = "application/json")
//	public ResponseEntity<String> gravaDadosOSPostgres(@RequestBody String dadosOS) {
//		
//		String retorno = "";
//
//		DaoIntegracaoApiIclass daoIntegracaoApiIclass = new DaoIntegracaoApiIclass(cabecalhoRepository, atendimentoRepository, iclassMateriaisRepository);
//		
//		retorno = daoIntegracaoApiIclass.gravaDadosOSPostgres(dadosOS);		
//
//		return new ResponseEntity<String>(retorno, HttpStatus.OK);
//	}
//	
//	@ApiOperation(value = "Atualiza status dos dados do getCloseOrdens")
//	@PutMapping(value = "/atualizaStatus", produces = "application/json")
//	public ResponseEntity<String> atualizaStatusGetCloseOrdens(@RequestParam("nr_os") String nr_os, @RequestParam("status") String status, @RequestParam("fluxo") String fluxo, @RequestParam("nr_solicitacao") String nr_solicitacao) {
//		
//		cabecalhoRepository.atualizaStatus(nr_solicitacao, fluxo, status, nr_os);
//		
//		return new ResponseEntity<String>("OK", HttpStatus.OK);
//		
//	}

}
