package api.rest.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.rest.dao.DaoIntegracaoApiIclass;
import api.rest.dto.IntegracaoAutentiqueDto;
import api.rest.model.integrador.IntegracaoAutentique;
import api.rest.repositoy.IntegracaoAutentiqueRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/autentique")
@Api(value = "API REST INTEGGRACAO AUTENTIQUE")
public class ControllerIntegracaoAutentique {
	
	@Autowired
	private IntegracaoAutentiqueRepository autentiqueRepository;			
	
	@ApiOperation(value = "Lista contratos a serem buscados")
	@GetMapping(value = "/listaContratos", produces = "application/json")	
	public ResponseEntity<List<IntegracaoAutentique>> listaContratosASeremBuscados() {			
				
		List<IntegracaoAutentique> list = new ArrayList<>();
		list = autentiqueRepository.listaContratosASeremBuscados();		
				
		return new ResponseEntity<List<IntegracaoAutentique>>(list, HttpStatus.OK);		
				
	}		
	
	@ApiOperation(value = "Adiciona novo registro")
	@PostMapping(value = "/add", produces = "application/json")	
	public ResponseEntity<IntegracaoAutentique> insereNovoRegistro(@RequestParam("id_contrato") String id_contrato, @RequestParam("nr_solicitacao") String nr_solicitacao, @RequestParam("atividade") String atividade, @RequestParam("id_anexo") String id_anexo) {			
				
		IntegracaoAutentique integracaoAutentique = new IntegracaoAutentique();
		integracaoAutentique.setId_contrato(id_contrato);
		integracaoAutentique.setNr_solicitacao(nr_solicitacao);
		integracaoAutentique.setCtr_ass("A");
		integracaoAutentique.setMov_sol("N");
		integracaoAutentique.setAtividade(Integer.parseInt(atividade));
		integracaoAutentique.setId_anexo(id_anexo);
		integracaoAutentique.setMovement_status("awaiting");
		
		IntegracaoAutentique autentiqueCadastrado = autentiqueRepository.save(integracaoAutentique);
		
		// chamar aqui api q movimenta solicitacao
		//DaoIntegracaoApiIclass daoIntegracaoApiIclass = new DaoIntegracaoApiIclass();
		
		//daoIntegracaoApiIclass.chamaApiMovimentaSolicitacao(Integer.parseInt(nr_solicitacao));
				
		return new ResponseEntity<IntegracaoAutentique>(autentiqueCadastrado, HttpStatus.OK);		
				
	}	
	
	@ApiOperation(value = "Realiza marcação na solicitação para remover ela da lista de solicitações a serem movimentadas")
	@PutMapping(value = "/marcaSolicitacao", produces = "application/json")	
	public ResponseEntity<String> marca_contrato_ja_enviado(@RequestParam("id_solicitacao") String id_solicitacao) {			
				
		String retorno = "OK";
		
		try {
			autentiqueRepository.marca_solicitacao_enviada(id_solicitacao);
		}catch(Exception e) {
			retorno = "ERRO: " + e;
		}		
				
		return new ResponseEntity<String>(retorno, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Grava link do contrato assinado")
	@GetMapping(value = "/gravaLink", produces = "application/json")	
	public ResponseEntity<IntegracaoAutentiqueDto> gravaLink(@RequestParam("id_contrato") String id_contrato, @RequestParam("link_assinado") String link_assinado, @RequestParam("assinado") String assinado, @RequestParam("obs_rejeitado") String obs_rejeitado, @RequestParam("movement_status") String movement_status) {		
				
		IntegracaoAutentiqueDto integracaoAutentiqueDto = new IntegracaoAutentiqueDto();
		IntegracaoAutentique integracaoAutentique = new IntegracaoAutentique();
		
		String cod_retorno = "200";
		String desc_retorno = "OK";
		
		try {
			autentiqueRepository.gravaLink(id_contrato,link_assinado,assinado,obs_rejeitado,movement_status);
			integracaoAutentique = autentiqueRepository.buscaContratoPeloId(id_contrato);
		}catch(Exception e) {
			desc_retorno = "ERRO: " + e;
			cod_retorno = "500";
		}	
		
		integracaoAutentiqueDto.setAtividade(integracaoAutentique.getAtividade());
		integracaoAutentiqueDto.setCd_integracao_autentique(integracaoAutentique.getCd_integracao_autentique());
		integracaoAutentiqueDto.setContrato_assinado(integracaoAutentique.getContrato_assinado());
		integracaoAutentiqueDto.setCtr_ass(integracaoAutentique.getCtr_ass());		
		integracaoAutentiqueDto.setId_contrato(integracaoAutentique.getId_contrato());
		integracaoAutentiqueDto.setMov_sol(integracaoAutentique.getMov_sol());
		integracaoAutentiqueDto.setNr_solicitacao(integracaoAutentique.getNr_solicitacao());
		
		integracaoAutentiqueDto.setDesc_retorno(desc_retorno);
		integracaoAutentiqueDto.setCod_retorno(cod_retorno);
				
		return new ResponseEntity<IntegracaoAutentiqueDto>(integracaoAutentiqueDto, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Lista contratos por solicitacao")
	@GetMapping(value = "/listaContratosSolicitacao", produces = "application/json")	
	public ResponseEntity<List<IntegracaoAutentique>> listaContratosPorSolicitacao(@RequestParam("nr_solicitacao") String nr_solicitacao) {			
				
		List<IntegracaoAutentique> list = new ArrayList<>();
		list = autentiqueRepository.buscaContratosPorSolicitacao(nr_solicitacao);		
				
		return new ResponseEntity<List<IntegracaoAutentique>>(list, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Lista todos os contratos por solicitacao")
	@GetMapping(value = "/listaTodosContratosSolicitacao", produces = "application/json")	
	public ResponseEntity<List<IntegracaoAutentique>> listaTodosContratosSolicitacao(@RequestParam("nr_solicitacao") String nr_solicitacao) {			
				
		List<IntegracaoAutentique> list = new ArrayList<>();
		list = autentiqueRepository.buscaTodosContratosPorSolicitacao(nr_solicitacao);		
				
		return new ResponseEntity<List<IntegracaoAutentique>>(list, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Lista solicitações a serem movimentadas")
	@GetMapping(value = "/listaSolicitacoesASeremMovimentadas", produces = "application/json")	
	public ResponseEntity<List<String>> listaSolicitacoesASeremMovimentadas() {			
				
		List<String> list = null;
		list = autentiqueRepository.listaSolicitacoesASeremMovimentadas();			
				
		return new ResponseEntity<List<String>>(list, HttpStatus.OK);		
				
	}	

	@ApiOperation(value = "Busca contrato pelo id do anexo")
	@GetMapping(value = "/listaIdAnexo", produces = "application/json")	
	public ResponseEntity<IntegracaoAutentique> listaContratoIdAnexo(@RequestParam("id_anexo") String id_anexo) {			
				
		IntegracaoAutentique integracaoAutentique = autentiqueRepository.buscaContratoPeloIdAnexo(id_anexo);		
				
		return new ResponseEntity<IntegracaoAutentique>(integracaoAutentique, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Verifica Conexao, retorna sempre OK se disponivel")
	@GetMapping(value = "/validaConexao", produces = "application/json")	
	public ResponseEntity<String> validaConexao() {	
				
		return new ResponseEntity<String>("OK", HttpStatus.OK);		
				
	}	
		
	@ApiOperation(value = "Deleta registro contrato")
	@DeleteMapping(value = "/delete", produces = "application/json")	
	public ResponseEntity<String> deletaRegistroContrato(@RequestParam("id_contrato") String id_contrato) {	
		
		List<IntegracaoAutentique> list = new ArrayList<>();
		list = autentiqueRepository.listBuscaContratoPeloId(id_contrato);	
		
		if (list.size() > 0) {
			autentiqueRepository.delete(list.get(0));
		}		
				
		return new ResponseEntity<String>("OK", HttpStatus.OK);		
				
	}	
	
	@ApiOperation(value = "Deleta registro contrato Por Solicitacao")
	@GetMapping(value = "/deletePorSolicitacao", produces = "application/json")	
	public ResponseEntity<String> deletaRegistroContratoPorSolicitacao(@RequestParam("nr_solicitacao") String nr_solicitacao) {	
		
		try {
			autentiqueRepository.alteraRegistroPorSolicitacao(nr_solicitacao);	
		}catch(Exception e) {
			System.out.println("deu erro ao tentar atualizar registro deletePorSolicitacao: " + nr_solicitacao);
		}	
				
		return new ResponseEntity<String>("OK", HttpStatus.OK);		
				
	}
	
	//-------------------------- CONTROLLERS TRANSFERENCIA DE TITULARIDADE --------------------------------------//
	
	@ApiOperation(value = "Adiciona novo registro transf titularidade")
	@PostMapping(value = "/addTitularidade", produces = "application/json")	
	public ResponseEntity<IntegracaoAutentique> insereNovoRegistroTitularidade(@RequestParam("id_contrato") String id_contrato, @RequestParam("nr_solicitacao") String nr_solicitacao, @RequestParam("atividade") String atividade, @RequestParam("id_anexo") String id_anexo, @RequestParam("processo") String processo) {			
				
		IntegracaoAutentique integracaoAutentique = new IntegracaoAutentique();
		integracaoAutentique.setId_contrato(id_contrato);
		integracaoAutentique.setNr_solicitacao(nr_solicitacao);
		integracaoAutentique.setCtr_ass("A");
		integracaoAutentique.setMov_sol("N");
		integracaoAutentique.setAtividade(Integer.parseInt(atividade));
		integracaoAutentique.setId_anexo(id_anexo);
		integracaoAutentique.setProcesso(processo);
		integracaoAutentique.setMovement_status("awaiting");
		
		IntegracaoAutentique autentiqueCadastrado = autentiqueRepository.save(integracaoAutentique);
				
		return new ResponseEntity<IntegracaoAutentique>(autentiqueCadastrado, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Lista solicitações a serem movimentadas")
	@GetMapping(value = "/listaSolicitacoesASeremMovimentadasTitularidade", produces = "application/json")	
	public ResponseEntity<List<String>> listaSolicitacoesASeremMovimentadasTitularidade() {			
				
		List<String> list = null;
		list = autentiqueRepository.listaSolicitacoesASeremMovimentadasTitularidade();			
				
		return new ResponseEntity<List<String>>(list, HttpStatus.OK);		
				
	}	
	
	@ApiOperation(value = "Lista contratos a serem buscados")
	@GetMapping(value = "/listaContratosTitularidade", produces = "application/json")	
	public ResponseEntity<List<IntegracaoAutentique>> listaContratosASeremBuscadosTitularidade() {			
				
		List<IntegracaoAutentique> list = new ArrayList<>();
		list = autentiqueRepository.listaContratosASeremBuscadosTitularidade();		
				
		return new ResponseEntity<List<IntegracaoAutentique>>(list, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Lista contratos por solicitacao")
	@GetMapping(value = "/listaContratosIdContratoTitularidade", produces = "application/json")	
	public ResponseEntity<List<IntegracaoAutentique>> listaContratosPoridContratoTitularidade(@RequestParam("id_contrato") String id_contrato) {			
				
		List<IntegracaoAutentique> list = new ArrayList<>();
		list = autentiqueRepository.buscaContratosPorIdContratoTitularidade(id_contrato);		
				
		return new ResponseEntity<List<IntegracaoAutentique>>(list, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Lista contratos por solicitacao")
	@GetMapping(value = "/listaContratosSolicitacaoTitularidade2", produces = "application/json")	
	public ResponseEntity<List<IntegracaoAutentique>> listaContratosPorSolicitacaoTitularidade2(@RequestParam("nr_solicitacao") String nr_solicitacao) {			
				
		List<IntegracaoAutentique> list = new ArrayList<>();
		list = autentiqueRepository.buscaContratosPorSolicitacaoTitularidade2(nr_solicitacao);		
				
		return new ResponseEntity<List<IntegracaoAutentique>>(list, HttpStatus.OK);		
				
	}
	
	@ApiOperation(value = "Realiza marcação na solicitação para remover ela da lista de solicitações a serem movimentadas Titularidade")
	@PutMapping(value = "/marcaSolicitacaoTitularidade", produces = "application/json")	
	public ResponseEntity<String> marca_contrato_ja_enviado_Titularidade(@RequestParam("id_contrato") String id_contrato) {			
				
		String retorno = "OK";
		
		try {
			autentiqueRepository.marca_solicitacao_enviada(autentiqueRepository.buscaSolicitacaoPorIdContratoTitularidade(id_contrato));
		}catch(Exception e) {
			retorno = "ERRO: " + e;
		}		
				
		return new ResponseEntity<String>(retorno, HttpStatus.OK);		
				
	}
	
	//-----------------------------------------------------------------------------------------------------------//

}
