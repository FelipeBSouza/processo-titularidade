package api.rest.helpers;

import java.util.ArrayList;

public class CustomException extends Exception {

    private static final long serialVersionUID = 1L;
    private int code;
    private String message;
    private ArrayList<FluigProduct> products;

    public CustomException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
        this.products = new ArrayList<>();
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<FluigProduct> getProducts() {
        return this.products;
    }

    public void setProducts(ArrayList<FluigProduct> products) {
        this.products = products;
    }
}
