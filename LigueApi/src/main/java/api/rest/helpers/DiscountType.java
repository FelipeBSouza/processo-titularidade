package api.rest.helpers;

public enum DiscountType {
    SINGLE(1), DUAL(2), TRIPLE(3);

    private final int id;

    DiscountType(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }
}
