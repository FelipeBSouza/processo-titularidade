package api.rest.planos;

public class AuxiliarPlanosRenovados {
	
	private String id_item_contrato;
	private String cod_antigo;
	private String novo_antigo;
	private String referencia_plano;
	private String referencia_sva_obrigatorio;
	private String referencia_obrigatorios;
	private String descricao_plano_antigo;	
	private String situacao;
	private String cod_prod_anterior;	
	private String obs_item;
	
	public String getObs_item() {
		return obs_item;
	}
	public void setObs_item(String obs_item) {
		this.obs_item = obs_item;
	}
	public String getCod_prod_anterior() {
		return cod_prod_anterior;
	}
	public void setCod_prod_anterior(String cod_prod_anterior) {
		this.cod_prod_anterior = cod_prod_anterior;
	}
	public String getDescricao_plano_antigo() {
		return descricao_plano_antigo;
	}
	public void setDescricao_plano_antigo(String descricao_plano_antigo) {
		this.descricao_plano_antigo = descricao_plano_antigo;
	}
	public String getReferencia_plano() {
		return referencia_plano;
	}
	public void setReferencia_plano(String referencia_plano) {
		this.referencia_plano = referencia_plano;
	}
	public String getReferencia_sva_obrigatorio() {
		return referencia_sva_obrigatorio;
	}
	public void setReferencia_sva_obrigatorio(String referencia_sva_obrigatorio) {
		this.referencia_sva_obrigatorio = referencia_sva_obrigatorio;
	}
	public String getReferencia_obrigatorios() {
		return referencia_obrigatorios;
	}
	public void setReferencia_obrigatorios(String referencia_obrigatorios) {
		this.referencia_obrigatorios = referencia_obrigatorios;
	}
	public String getId_item_contrato() {
		return id_item_contrato;
	}
	public void setId_item_contrato(String id_item_contrato) {
		this.id_item_contrato = id_item_contrato;
	}
	public String getCod_antigo() {
		return cod_antigo;
	}
	public void setCod_antigo(String cod_antigo) {
		this.cod_antigo = cod_antigo;
	}
	public String getNovo_antigo() {
		return novo_antigo;
	}
	public void setNovo_antigo(String novo_antigo) {
		this.novo_antigo = novo_antigo;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}	

}
