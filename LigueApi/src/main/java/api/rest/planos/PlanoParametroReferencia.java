package api.rest.planos;

public class PlanoParametroReferencia {
	
	private String referencia_produto;
	private String tp_plano;
	private String equivalencia;
	private String repetivel;
	
	public String getTp_plano() {
		return tp_plano;
	}

	public void setTp_plano(String tp_plano) {
		this.tp_plano = tp_plano;
	}

	public String getReferencia_produto() {
		return referencia_produto;
	}

	public void setReferencia_produto(String referencia_produto) {
		this.referencia_produto = referencia_produto;
	}

	public String getEquivalencia() {
		return equivalencia;
	}

	public void setEquivalencia(String equivalencia) {
		this.equivalencia = equivalencia;
	}

	public String getRepetivel() {
		return repetivel;
	}

	public void setRepetivel(String repetivel) {
		this.repetivel = repetivel;
	}
	
}
