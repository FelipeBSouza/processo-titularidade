package api.rest.planos;

public class ProdutosObrigatorios {
	
	private String cod_totvs;
    private String referencia;
    private String descricao;
    private String valorCheio;
    private String valorMensal;
    private String desconto;
    private String qtd_mes_ini;
    private String qtd_mes_cob;
    private String referencia_obg;
    private String referencia_sva;
    private String tp_produto;   
    private String visivel;
    private String categoria;
    private String repetivel;
    private String download;
    private String upload;
    private String id_promocao;
    private String fidelidade;
    private String equivalencia;    
    private String id_item_contrato;
    private String cod_prod_antigo;
    private String descricao_plano_antigo;   
    private String novo_antigo;    
    private String situacao; 
    private String obs_item; 
        
	public String getObs_item() {
		return obs_item;
	}
	public void setObs_item(String obs_item) {
		this.obs_item = obs_item;
	}
	public String getNovo_antigo() {
		return novo_antigo;
	}
	public void setNovo_antigo(String novo_antigo) {
		this.novo_antigo = novo_antigo;
	}
	public String getDescricao_plano_antigo() {
		return descricao_plano_antigo;
	}
	public void setDescricao_plano_antigo(String descricao_plano_antigo) {
		this.descricao_plano_antigo = descricao_plano_antigo;
	}
	public String getEquivalencia() {
		return equivalencia;
	}
	public void setEquivalencia(String equivalencia) {
		this.equivalencia = equivalencia;
	}
	public String getValorCheio() {
		return valorCheio;
	}
	public void setValorCheio(String valorCheio) {
		this.valorCheio = valorCheio;
	}
	public String getValorMensal() {
		return valorMensal;
	}
	public void setValorMensal(String valorMensal) {
		this.valorMensal = valorMensal;
	}
	public String getCod_totvs() {
		return cod_totvs;
	}
	public void setCod_totvs(String cod_totvs) {
		this.cod_totvs = cod_totvs;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDesconto() {
		return desconto;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	public String getQtd_mes_ini() {
		return qtd_mes_ini;
	}
	public void setQtd_mes_ini(String qtd_mes_ini) {
		this.qtd_mes_ini = qtd_mes_ini;
	}
	public String getQtd_mes_cob() {
		return qtd_mes_cob;
	}
	public void setQtd_mes_cob(String qtd_mes_cob) {
		this.qtd_mes_cob = qtd_mes_cob;
	}
	public String getReferencia_obg() {
		return referencia_obg;
	}
	public void setReferencia_obg(String referencia_obg) {
		this.referencia_obg = referencia_obg;
	}
	public String getReferencia_sva() {
		return referencia_sva;
	}
	public void setReferencia_sva(String referencia_sva) {
		this.referencia_sva = referencia_sva;
	}
	public String getTp_produto() {
		return tp_produto;
	}
	public void setTp_produto(String tp_produto) {
		this.tp_produto = tp_produto;
	}
	public String getVisivel() {
		return visivel;
	}
	public void setVisivel(String visivel) {
		this.visivel = visivel;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getRepetivel() {
		return repetivel;
	}
	public void setRepetivel(String repetivel) {
		this.repetivel = repetivel;
	}
	public String getDownload() {
		return download;
	}
	public void setDownload(String download) {
		this.download = download;
	}
	public String getUpload() {
		return upload;
	}
	public void setUpload(String upload) {
		this.upload = upload;
	}
	public String getId_promocao() {
		return id_promocao;
	}
	public void setId_promocao(String id_promocao) {
		this.id_promocao = id_promocao;
	}
	public String getFidelidade() {
		return fidelidade;
	}
	public void setFidelidade(String fidelidade) {
		this.fidelidade = fidelidade;
	}	
	
	public String getId_item_contrato() {
		return id_item_contrato;
	}
	public void setId_item_contrato(String id_item_contrato) {
		this.id_item_contrato = id_item_contrato;
	}
	public String getCod_prod_antigo() {
		return cod_prod_antigo;
	}
	public void setCod_prod_antigo(String cod_prod_antigo) {
		this.cod_prod_antigo = cod_prod_antigo;
	}
		
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cod_totvs == null) ? 0 : cod_totvs.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutosObrigatorios other = (ProdutosObrigatorios) obj;
		if (cod_totvs == null) {
			if (other.cod_totvs != null)
				return false;
		} else if (!cod_totvs.equals(other.cod_totvs))
			return false;
		return true;
	}
	
	
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
//		result = prime * result + ((cod_totvs == null) ? 0 : cod_totvs.hashCode());
//		result = prime * result + ((desconto == null) ? 0 : desconto.hashCode());
//		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
//		result = prime * result + ((download == null) ? 0 : download.hashCode());
//		result = prime * result + ((equivalencia == null) ? 0 : equivalencia.hashCode());
//		result = prime * result + ((fidelidade == null) ? 0 : fidelidade.hashCode());
//		result = prime * result + ((id_promocao == null) ? 0 : id_promocao.hashCode());
//		result = prime * result + ((qtd_mes_cob == null) ? 0 : qtd_mes_cob.hashCode());
//		result = prime * result + ((qtd_mes_ini == null) ? 0 : qtd_mes_ini.hashCode());
//		result = prime * result + ((referencia == null) ? 0 : referencia.hashCode());
//		result = prime * result + ((referencia_obg == null) ? 0 : referencia_obg.hashCode());
//		result = prime * result + ((referencia_sva == null) ? 0 : referencia_sva.hashCode());
//		result = prime * result + ((repetivel == null) ? 0 : repetivel.hashCode());
//		result = prime * result + ((tp_produto == null) ? 0 : tp_produto.hashCode());
//		result = prime * result + ((upload == null) ? 0 : upload.hashCode());
//		result = prime * result + ((valorCheio == null) ? 0 : valorCheio.hashCode());
//		result = prime * result + ((valorMensal == null) ? 0 : valorMensal.hashCode());
//		result = prime * result + ((visivel == null) ? 0 : visivel.hashCode());
//		return result;
//	}
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		ProdutosObrigatorios other = (ProdutosObrigatorios) obj;
//		if (categoria == null) {
//			if (other.categoria != null)
//				return false;
//		} else if (!categoria.equals(other.categoria))
//			return false;
//		if (cod_totvs == null) {
//			if (other.cod_totvs != null)
//				return false;
//		} else if (!cod_totvs.equals(other.cod_totvs))
//			return false;
//		if (desconto == null) {
//			if (other.desconto != null)
//				return false;
//		} else if (!desconto.equals(other.desconto))
//			return false;
//		if (descricao == null) {
//			if (other.descricao != null)
//				return false;
//		} else if (!descricao.equals(other.descricao))
//			return false;
//		if (download == null) {
//			if (other.download != null)
//				return false;
//		} else if (!download.equals(other.download))
//			return false;
//		if (equivalencia == null) {
//			if (other.equivalencia != null)
//				return false;
//		} else if (!equivalencia.equals(other.equivalencia))
//			return false;
//		if (fidelidade == null) {
//			if (other.fidelidade != null)
//				return false;
//		} else if (!fidelidade.equals(other.fidelidade))
//			return false;
//		if (id_promocao == null) {
//			if (other.id_promocao != null)
//				return false;
//		} else if (!id_promocao.equals(other.id_promocao))
//			return false;
//		if (qtd_mes_cob == null) {
//			if (other.qtd_mes_cob != null)
//				return false;
//		} else if (!qtd_mes_cob.equals(other.qtd_mes_cob))
//			return false;
//		if (qtd_mes_ini == null) {
//			if (other.qtd_mes_ini != null)
//				return false;
//		} else if (!qtd_mes_ini.equals(other.qtd_mes_ini))
//			return false;
//		if (referencia == null) {
//			if (other.referencia != null)
//				return false;
//		} else if (!referencia.equals(other.referencia))
//			return false;
//		if (referencia_obg == null) {
//			if (other.referencia_obg != null)
//				return false;
//		} else if (!referencia_obg.equals(other.referencia_obg))
//			return false;
//		if (referencia_sva == null) {
//			if (other.referencia_sva != null)
//				return false;
//		} else if (!referencia_sva.equals(other.referencia_sva))
//			return false;
//		if (repetivel == null) {
//			if (other.repetivel != null)
//				return false;
//		} else if (!repetivel.equals(other.repetivel))
//			return false;
//		if (tp_produto == null) {
//			if (other.tp_produto != null)
//				return false;
//		} else if (!tp_produto.equals(other.tp_produto))
//			return false;
//		if (upload == null) {
//			if (other.upload != null)
//				return false;
//		} else if (!upload.equals(other.upload))
//			return false;
//		if (valorCheio == null) {
//			if (other.valorCheio != null)
//				return false;
//		} else if (!valorCheio.equals(other.valorCheio))
//			return false;
//		if (valorMensal == null) {
//			if (other.valorMensal != null)
//				return false;
//		} else if (!valorMensal.equals(other.valorMensal))
//			return false;
//		if (visivel == null) {
//			if (other.visivel != null)
//				return false;
//		} else if (!visivel.equals(other.visivel))
//			return false;
//		return true;
//	}	

}
