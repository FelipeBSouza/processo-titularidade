package api.rest.planos;

import java.util.List;

public class ParametrosPlanoCorrespondente {
	
	private String fidelidade;
	private String cidade;
	private String bairro;
			
	private List<AuxiliarPlanosCorrespondente> arrayPlanosContrato;
	private List<AuxiliarPlanosRenovados> arrayPlanosRenovados;
		
	public List<AuxiliarPlanosCorrespondente> getArrayPlanosContrato() {
		return arrayPlanosContrato;
	}

	public void setArrayPlanosContrato(List<AuxiliarPlanosCorrespondente> arrayPlanosContrato) {
		this.arrayPlanosContrato = arrayPlanosContrato;
	}

	public String getFidelidade() {
		return fidelidade;
	}

	public void setFidelidade(String fidelidade) {
		this.fidelidade = fidelidade;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}	

	public List<AuxiliarPlanosRenovados> getArrayPlanosRenovados() {
		return arrayPlanosRenovados;
	}

	public void setArrayPlanosRenovados(List<AuxiliarPlanosRenovados> arrayPlanosRenovados) {
		this.arrayPlanosRenovados = arrayPlanosRenovados;
	}	
		
}
