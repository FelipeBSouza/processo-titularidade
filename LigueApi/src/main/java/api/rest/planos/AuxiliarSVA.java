package api.rest.planos;

public class AuxiliarSVA {

	private String cod_plano;
	private int qtd_ponto_adicional;
	private String id_item_contrato;	
	private String situacao;	
	private String descricao_plano;	
		
	public String getDescricao_plano() {
		return descricao_plano;
	}
	public void setDescricao_plano(String descricao_plano) {
		this.descricao_plano = descricao_plano;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getId_item_contrato() {
		return id_item_contrato;
	}
	public void setId_item_contrato(String id_item_contrato) {
		this.id_item_contrato = id_item_contrato;
	}
	public String getCod_plano() {
		return cod_plano;
	}
	public void setCod_plano(String cod_plano) {
		this.cod_plano = cod_plano;
	}
	public int getQtd_ponto_adicional() {
		return qtd_ponto_adicional;
	}
	public void setQtd_ponto_adicional(int qtd_ponto_adicional) {
		this.qtd_ponto_adicional = qtd_ponto_adicional;
	}	
	
}
