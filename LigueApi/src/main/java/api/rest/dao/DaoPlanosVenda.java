package api.rest.dao;

import java.util.ArrayList;
import java.util.List;

import api.rest.dto.PlanosVendaDto;
import api.rest.model.integrador.Produto_venda;

public class DaoPlanosVenda {
    Utils utils = new Utils();

    /*
     * ENTRADA: recebe uma lista de planos do tipo classe Produto_venda SAIDA:
     * retorna uma lista de planos do tipo classe PlanosVendaDto
     */
    public List<PlanosVendaDto> converteList(List<Produto_venda> listParam) {

        List<PlanosVendaDto> listRetorno = new ArrayList<>();
        PlanosVendaDto planosVendaDto;

        int v = 0;

        while (v < listParam.size()) {

            planosVendaDto = new PlanosVendaDto();
            planosVendaDto.setAltera_desconto(listParam.get(v).getAltera_desconto());
            planosVendaDto.setCd_prod_venda(listParam.get(v).getCd_prod_venda());
            planosVendaDto.setCod_prod_totvs(listParam.get(v).getCod_prod_totvs());
            planosVendaDto.setCombo_1(listParam.get(v).getCombo_1());
            planosVendaDto.setCombo_2(listParam.get(v).getCombo_2());
            planosVendaDto.setCombo_3(listParam.get(v).getCombo_3());
            planosVendaDto.setDescricao(listParam.get(v).getDescricao());
            planosVendaDto.setDt_fim_promocao(listParam.get(v).getDt_fim_promocao());
            planosVendaDto.setDt_ini_promocao(listParam.get(v).getDt_ini_promocao());
            planosVendaDto.setId_bairro(listParam.get(v).getId_bairro());
            planosVendaDto.setId_cidade(listParam.get(v).getId_cidade());
            planosVendaDto.setId_uf(listParam.get(v).getId_uf());
            planosVendaDto.setProd_obrig_1(listParam.get(v).getProd_obrig_1());
            planosVendaDto.setProd_obrig_2(listParam.get(v).getProd_obrig_2());
            planosVendaDto.setProd_obrig_3(listParam.get(v).getProd_obrig_3());
            planosVendaDto.setProd_obrig_4(listParam.get(v).getProd_obrig_4());
            planosVendaDto.setProd_obrig_5(listParam.get(v).getProd_obrig_5());
            planosVendaDto.setQtd_mes_cob(listParam.get(v).getQtd_mes_cob());
            planosVendaDto.setQtd_mes_ini(listParam.get(v).getQtd_mes_ini());
            planosVendaDto.setTp_produto(listParam.get(v).getTp_produto());
            planosVendaDto.setValor_cheio(listParam.get(v).getValor_cheio());
            planosVendaDto.setReferencia(listParam.get(v).getReferencia());
            planosVendaDto.setCombo(listParam.get(v).getCombo());
            planosVendaDto.setDownload(listParam.get(v).getDownload());
            planosVendaDto.setUpload(listParam.get(v).getUpload());
            planosVendaDto.setVisivel(listParam.get(v).getVisivel());

            listRetorno.add(planosVendaDto);

            v++;
        }

        return listRetorno;

    }

}
