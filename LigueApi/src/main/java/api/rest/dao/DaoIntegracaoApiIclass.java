package api.rest.dao;


import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import api.rest.model.integrador.IclassAtendimentos;
import api.rest.model.integrador.IclassCabecalho;
import api.rest.model.integrador.IclassMateriais;
import api.rest.repositoy.IclassAtendimentoRepository;
import api.rest.repositoy.IclassCabecalhoRepository;
import api.rest.repositoy.IclassMateriaisRepository;

public class DaoIntegracaoApiIclass {
	
	@Autowired
	private IclassAtendimentoRepository atendimentoRepository;
	@Autowired
	private IclassMateriaisRepository iclassMateriaisRepository;
	@Autowired
	private IclassCabecalhoRepository cabecalhoRepository;

	public DaoIntegracaoApiIclass(IclassCabecalhoRepository cabecalhoRepository, IclassAtendimentoRepository atendimentoRepository, IclassMateriaisRepository iclassMateriaisRepository) {
		this.cabecalhoRepository = cabecalhoRepository;
		this.atendimentoRepository = atendimentoRepository;
		this.iclassMateriaisRepository = iclassMateriaisRepository;
	}
	
	public String chamaApiMovimentaSolicitacao(int nr_solicitacao) {

		String retorno = "";

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> response = restTemplate.exchange("http://localhost:8060/autentique/movimentaSolicitacaoID?nr_solicitacao=" + nr_solicitacao, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
		});

		retorno = response.getBody();

		return retorno;

	}

	// essa funcao recebe os dados vindos da api do iclass e grava no postgres
	public String gravaDadosOSPostgres(String dadosOS) {
		
		IclassAtendimentos atendimento = new IclassAtendimentos() ;
		IclassCabecalho cabecalho = new IclassCabecalho();
		IclassMateriais materiais = new IclassMateriais();
		
		String retorno = "";
		
		System.out.println("entrou na funcao gravaDadosOSPostgres");
				
		try{
			
			JSONObject obj = null;
			obj = new JSONObject(dadosOS.toString());		
			System.out.println(obj.toString());
					
			JSONArray jArrayORDENS = obj.getJSONArray("ordens");
			
			// cada passada no for e uma OS, ou um item da OS, mas como no iclass um item pode virar uma OS entao e como se fosse uma OS cada passada no for
			for (int i = 0; i < jArrayORDENS.length(); i++) {
				JSONObject objCabecalho = jArrayORDENS.getJSONObject(i);
				System.out.println("----------------------");
				System.out.println(objCabecalho.toString());

				// DADOS DO CABECALHO
				System.out.println("numero da OS: " + objCabecalho.get("numos"));
				
				cabecalho = new IclassCabecalho();
				
				cabecalho.setNumos(objCabecalho.get("numos").toString());
				cabecalho.setCodcli(objCabecalho.get("codcli").toString());
				cabecalho.setLoja(objCabecalho.get("loja").toString());
				cabecalho.setAtendente(objCabecalho.get("atendente").toString());
				cabecalho.setCusuario(objCabecalho.get("cusuario").toString());
				cabecalho.setPorta(objCabecalho.get("porta").toString());
				cabecalho.setCaixa(objCabecalho.get("caixa").toString());
				
				cabecalhoRepository.save(cabecalho); // grava dados do cabecalho no banco de dados
				
				// ATENDIMENTOS
				JSONArray jArrayAtendimentos = objCabecalho.getJSONArray("atendimentos");
				System.out.println("ATENDIMENTOS");
				for (int x = 0; x < jArrayAtendimentos.length(); x++) {
					
					JSONObject objAtendimento = jArrayAtendimentos.getJSONObject(x);
					System.out.println(objAtendimento.toString());		
					
					atendimento = new IclassAtendimentos();
					atendimento.setNumos(objCabecalho.get("numos").toString()); // vincula tabelas pelo numero da OS
					atendimento.setDtchegada(objAtendimento.get("dtchegada").toString());
					atendimento.setHrchegada(objAtendimento.get("hrchegada").toString());
					atendimento.setDtsaida(objAtendimento.get("dtsaida").toString());
					atendimento.setHrsaida(objAtendimento.get("hrsaida").toString());
					atendimento.setDtinicial(objAtendimento.get("dtinicial").toString());
					atendimento.setHrinicial(objAtendimento.get("hrinicial").toString());
					atendimento.setDtfinal(objAtendimento.get("dtfinal").toString());
					atendimento.setHrfinal(objAtendimento.get("hrfinal").toString());
					atendimento.setOcorr(objAtendimento.get("ocorr").toString());
					atendimento.setLaudo(objAtendimento.get("laudo").toString());
					atendimento.setAcumul(objAtendimento.get("acumul").toString());
					atendimento.setStatus(objAtendimento.get("status").toString());
					atendimento.setTrasla(objAtendimento.get("trasla").toString());
					atendimento.setHrsfats(objAtendimento.get("hrsfats").toString());
					atendimento.setCodocor(objAtendimento.get("codocor").toString());	
					
					atendimentoRepository.save(atendimento); // grava atendimento banco de dados
					
				}
				
				// MATERIAIS
				JSONArray jArrayMateriais = objCabecalho.getJSONArray("materiais");
				System.out.println("MATERIAIS");			
				for (int z = 0; z < jArrayMateriais.length(); z++) {
					
					JSONObject objMaterial = jArrayMateriais.getJSONObject(z);
					System.out.println(objMaterial.toString());
					
					if(!objMaterial.get("cdpro").toString().equals("")) {
						materiais = new IclassMateriais();
						materiais.setNumos(objCabecalho.get("numos").toString()); // vincula tabelas pelo numero da OS
						materiais.setCdpro(objMaterial.get("cdpro").toString());
						materiais.setDescpro(objMaterial.get("descpro").toString());
						materiais.setQtde(objMaterial.get("qtde").toString());
						materiais.setVlrunit(objMaterial.get("vlrunit").toString());
						materiais.setVlrtotal(objMaterial.get("vlrtotal").toString());
						materiais.setCdservico(objMaterial.get("cdservico").toString());
						materiais.setArmazem(objMaterial.get("armazem").toString());
						
						iclassMateriaisRepository.save(materiais); // grava materiais no banco de dados
					}				
				}				
				
			}
			
			retorno = "OK";
			
		}catch(Exception e) {
			System.out.println("deu erro na integracao gravaDadosOSPostgres: " + e);
			e.printStackTrace();
			retorno = "ERRO: " + e.toString();
		}
		
		return retorno;

	}	

}
