package api.rest.dao;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

	public static String replaceAll(String campo, String caracterEncontrar, String caracterSubstituir) {

		return campo.replaceAll(caracterEncontrar, caracterSubstituir);
	}

	public int dataAtual() {

		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("yyyyMMdd");

		int dt_formatada = Integer.parseInt(formatador.format(data));

		System.out.println("data formatada: " + dt_formatada);

		return dt_formatada;
	}

	public boolean temNumero(String parametro) {
		boolean retorno = false;
		int contNumero = 0;

		String numeros = "0,1,2,3,4,5,6,7,8,9";

		int i = 0;
		while (i < parametro.length()) {

			if (numeros.indexOf(parametro.charAt(i)) != -1) {
				contNumero++;
			}

			i++;
		}

		if (contNumero > 0) {
			retorno = true;
		}

		return retorno;

	}

}
