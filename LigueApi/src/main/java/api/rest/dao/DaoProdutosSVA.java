package api.rest.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.regex.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import api.rest.helpers.ListaParametrosTotal;
import api.rest.model.integrador.PlanoCorrespondente;
import api.rest.model.integrador.Products;
import api.rest.planos.AuxiliarObrigatoriosRenovacao;
import api.rest.planos.AuxiliarPlanosCorrespondente;
import api.rest.planos.AuxiliarPlanosRenovados;
import api.rest.planos.AuxiliarSVA;
import api.rest.planos.ParametrosPlanoCorrespondente;
import api.rest.planos.ProdutosObrigatorios;
import api.rest.repositoy.PlanosCorrespondentesRepository;
import api.rest.repositoy.ProdutosSVARepository;
import api.rest.response.ResponseProdutosFluig;
import api.rest.response.ResponseProdutosObrigatoriosFluig;
import api.rest.response.ResponseProdutosObrigatoriosRenovacaoFluig;

public class DaoProdutosSVA {
	
	@Autowired
	private ProdutosSVARepository productsRep;
	@Autowired
	private PlanosCorrespondentesRepository planosCorrespondentesRepository;
	
	public DaoProdutosSVA(ProdutosSVARepository productsRep, PlanosCorrespondentesRepository planosCorrespondentesRepository) {
		this.productsRep = productsRep;
		this.planosCorrespondentesRepository = planosCorrespondentesRepository;
	}
	
	private Integer buscaQuantidadePontosAdicionaisTv(String descricao) {
		
		String quantidade = descricao.replaceAll("[^0-9]", "");
		
		if(quantidade != "") {
			return Integer.parseInt(quantidade);
		}
		
		return 1;
		
	}
	
	// funcao utilizada para retornar o plano correspondente
	public ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig> retornaPlanoCorrespondente(ParametrosPlanoCorrespondente dadosParametro) {
		
		// retorno da funcao
		ResponseProdutosObrigatoriosRenovacaoFluig responseProdutosObrigatoriosFluig = new ResponseProdutosObrigatoriosRenovacaoFluig();
		
		//armazena plano correspondente
		List<PlanoCorrespondente> listPlanoCorrespondente  = new ArrayList<>();
		
		// planos q serao retornados para serem adicionados na tabela de itens a serem contratados
		List<ProdutosObrigatorios> planos = new ArrayList<>();
		
		List<ProdutosObrigatorios> listProdutoAux = new ArrayList<>();
		
		List<AuxiliarSVA> listaAuxiliar = new ArrayList<>();
		AuxiliarSVA auxiliarSVA;
		ProdutosObrigatorios produtosObrigatorios;
		
		int x = 0;
		int c = 0;
		int r = 0;
		int z = 0;
		int y = 0;
		int qtd_pontos_adicionais_tv = 0;
		int qtd_tv = 0;
		int qtd_fone = 0;
		int qtd_net = 0;
		String cod_plano_tv = "";
		String id_item_tv = "";
		String situacao_tv = "";
		String descricao_plano_tv = "";
		int combo = 0; 
		String desconto = "";
		int j = 0;
		int qtd_net_validacao = 0;
		int qtd_fone_validacao = 0;
		String validacao_fone = "";
		String msg_retorno = "";
		String cod_net = "";
		
		String id_item_removido = "";
		
		try {
			
			//System.out.println("tamanho array: " + dadosParametro.getArrayPlanosContrato().size());
			
			// array de planos existentes no contrato la do totvs
			while(c < dadosParametro.getArrayPlanosContrato().size()) {
								
				// se tiver ponto adicional sendo renovado
				if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo(), "0").equals("PONTO_ADIC") &&  (dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("R") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("D") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("I") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("A"))) {
					qtd_pontos_adicionais_tv += buscaQuantidadePontosAdicionaisTv(dadosParametro.getArrayPlanosContrato().get(c).getDescricao_plano());
				}
				
				// se tiver plano de tv sendo renovado			
				if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo(), "0").equals("TV") && (dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("R") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("D") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("I") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("A"))) {
					
					qtd_pontos_adicionais_tv += buscaQuantidadePontosAdicionaisTv(dadosParametro.getArrayPlanosContrato().get(c).getDescricao_plano());
					cod_plano_tv = dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo();
					id_item_tv = dadosParametro.getArrayPlanosContrato().get(c).getId_item_contrato();
					situacao_tv = dadosParametro.getArrayPlanosContrato().get(c).getSituacao();
					descricao_plano_tv = dadosParametro.getArrayPlanosContrato().get(c).getDescricao_plano();					

				}
				
				// se um plano que nao seja de tv ou ponto adicional estiver sendo renovado
				if(!buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo(), "0").equals("TV") && !buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo(), "0").equals("SVA")  && !buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo(), "0").equals("PONTO_ADIC") &&  (dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("R") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("D") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("I") || dadosParametro.getArrayPlanosContrato().get(c).getSituacao().equals("A"))) {
					
					String retFunc = percorrePlanosRevovados(dadosParametro, dadosParametro.getArrayPlanosContrato().get(c).getId_item_contrato(),0);
					
					if(retFunc.equals("01")) {
						auxiliarSVA = new AuxiliarSVA();
						auxiliarSVA.setCod_plano(dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo());
						auxiliarSVA.setQtd_ponto_adicional(0);
						auxiliarSVA.setId_item_contrato(dadosParametro.getArrayPlanosContrato().get(c).getId_item_contrato());
						auxiliarSVA.setSituacao(dadosParametro.getArrayPlanosContrato().get(c).getSituacao());
						auxiliarSVA.setDescricao_plano(dadosParametro.getArrayPlanosContrato().get(c).getDescricao_plano());
						listaAuxiliar.add(auxiliarSVA);
					}
					//System.out.println("situacao recebida " + dadosParametro.getArrayPlanosContrato().get(c).getSituacao());
				}
				
				//System.out.println("CONSULTA AAAA " + dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo());
				
				if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo(), "0").equals("NET")) {
					cod_net = dadosParametro.getArrayPlanosContrato().get(c).getCod_antigo();
				}
				
				c++;
			}
			
			// funcao que verifica se o sva e um sva P1, caso seja add ele pra ser renovado
			listaAuxiliar = verificaSVAP1(dadosParametro, cod_net, listaAuxiliar);
			
			// se tiver plano de tv
			if(!cod_plano_tv.equals("")) {
				
				String retFunc = percorrePlanosRevovados(dadosParametro, id_item_tv, qtd_pontos_adicionais_tv);
				
				if(retFunc.equals("03")) {
					auxiliarSVA = new AuxiliarSVA();
					auxiliarSVA.setCod_plano(cod_plano_tv);
					auxiliarSVA.setQtd_ponto_adicional(qtd_pontos_adicionais_tv);
					auxiliarSVA.setId_item_contrato(id_item_tv);
					auxiliarSVA.setSituacao(situacao_tv);
					auxiliarSVA.setDescricao_plano(descricao_plano_tv);
					listaAuxiliar.add(auxiliarSVA);
					
					id_item_removido = id_item_tv;
					
				}else if(retFunc.equals("01")) {
					auxiliarSVA = new AuxiliarSVA();
					auxiliarSVA.setCod_plano(cod_plano_tv);
					auxiliarSVA.setQtd_ponto_adicional(qtd_pontos_adicionais_tv);
					auxiliarSVA.setId_item_contrato(id_item_tv);
					auxiliarSVA.setSituacao(situacao_tv);
					auxiliarSVA.setDescricao_plano(descricao_plano_tv);
					listaAuxiliar.add(auxiliarSVA);
					
					id_item_removido = "";
				}
				
			}
			
			// percorre a lista de planos e busca o novo plano correspondente
			while(z < listaAuxiliar.size()) {
				
					//System.out.println("situacao planos ja existentes: " + listaAuxiliar.get(z).getCod_plano()  + " - " + listaAuxiliar.get(z).getQtd_ponto_adicional());
									
					listPlanoCorrespondente = planosCorrespondentesRepository.listaPlanoCorrespondenteTV(listaAuxiliar.get(z).getCod_plano(), String.valueOf(listaAuxiliar.get(z).getQtd_ponto_adicional()));
					
					//System.out.println("Passou listPlanoCorrespondente");
					if(listPlanoCorrespondente.size() > 0) {
						
						//System.out.println("Entrou no if do listPlanoCorrespondente");
						
						produtosObrigatorios = new ProdutosObrigatorios();
						
						if(listPlanoCorrespondente.get(0).getTp_plano().equals("SVA")) {
							produtosObrigatorios = conversorResponse(productsRep.buscaReferenciaPlanoCorrespondenteP1(listPlanoCorrespondente.get(0).getCod_plano_atualizado(), dadosParametro.getCidade(), dadosParametro.getBairro(), dadosParametro.getFidelidade()).get(0));

						}else {
							produtosObrigatorios = conversorResponse(productsRep.buscaReferenciaPlanoCorrespondente(listPlanoCorrespondente.get(0).getCod_plano_atualizado(), dadosParametro.getCidade(), dadosParametro.getBairro(), dadosParametro.getFidelidade()).get(0));
						}
						
						produtosObrigatorios.setId_item_contrato(listaAuxiliar.get(z).getId_item_contrato());
						produtosObrigatorios.setCod_prod_antigo(listaAuxiliar.get(z).getCod_plano());
						produtosObrigatorios.setDescricao_plano_antigo(listaAuxiliar.get(z).getDescricao_plano());
						produtosObrigatorios.setSituacao(listaAuxiliar.get(z).getSituacao());
						
						//System.out.println("situacao recebida 2 " + listaAuxiliar.get(z).getSituacao());
						
						if(listPlanoCorrespondente.get(0).getAtivo().equals("I")) {
							msg_retorno += "O plano " + listaAuxiliar.get(z).getCod_plano() + " - " + listaAuxiliar.get(z).getDescricao_plano() + " não esta mais disponivel, foi adicionado o plano " + listPlanoCorrespondente.get(0).getCod_plano_atualizado() + " - " + listPlanoCorrespondente.get(0).getDescricao_novo() + "  no lugar, favor informar o cliente" ;
						}else {
							produtosObrigatorios.setCod_prod_antigo(listPlanoCorrespondente.get(0).getCod_plano_atualizado());
						}
																
						listProdutoAux.add(produtosObrigatorios);
						
					}else {
						responseProdutosObrigatoriosFluig.setPlanos(planos);
						responseProdutosObrigatoriosFluig.setCombo("0");
						responseProdutosObrigatoriosFluig.setCodigo("404");
						responseProdutosObrigatoriosFluig.setMensagem("Produto " + listaAuxiliar.get(z).getCod_plano() + " não encontrado");
						
						return new ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig>(responseProdutosObrigatoriosFluig, HttpStatus.OK); 
					}			
				
				
				z++;
			}
			
			// array de planos ja contratados
			while (x < dadosParametro.getArrayPlanosRenovados().size()) {

				//if(dadosParametro.getArrayPlanosRenovados().get(x).getNovo_antigo().equals("N")) {		
					
					//System.out.println("renovados referencia: " + dadosParametro.getArrayPlanosRenovados().get(x).getReferencia_plano());
					
					if(!buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(x).getCod_antigo(), "0").equals("")) { 
						
						if(!dadosParametro.getArrayPlanosRenovados().get(x).getId_item_contrato().trim().equals(id_item_removido)) { // usado para remover plano de tv para ser atualizado quando necessario
							produtosObrigatorios = new ProdutosObrigatorios();
							produtosObrigatorios = conversorResponse(productsRep.listPlanoPorReferencia(dadosParametro.getArrayPlanosRenovados().get(x).getReferencia_plano()).get(0));
							
							produtosObrigatorios.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(x).getId_item_contrato());
							//produtosObrigatorios.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(x).getCod_antigo());
							produtosObrigatorios.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(x).getCod_prod_anterior());
							produtosObrigatorios.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(x).getNovo_antigo());
							//produtosObrigatorios.setDescricao_plano_antigo("");
							produtosObrigatorios.setDescricao_plano_antigo(dadosParametro.getArrayPlanosRenovados().get(x).getDescricao_plano_antigo());
							
							listProdutoAux.add(produtosObrigatorios);
						}
						
					}else {
						responseProdutosObrigatoriosFluig.setPlanos(planos);
						responseProdutosObrigatoriosFluig.setCombo("0");
						responseProdutosObrigatoriosFluig.setCodigo("404");
						responseProdutosObrigatoriosFluig.setMensagem("Produto " + dadosParametro.getArrayPlanosRenovados().get(x).getCod_antigo() + " não encontrado");
						
						return new ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig>(responseProdutosObrigatoriosFluig, HttpStatus.OK); 
					}								
					
				//}
				
				x++;
			}
			
			if(dadosParametro.getFidelidade().equals("S")){
				listProdutoAux = validaListaAuxiliar(listProdutoAux,dadosParametro);
			}			
			
			// verifica se e combo 1, combo 2 ou combo 3
			while(r < listProdutoAux.size()) {
								
				if(listProdutoAux.get(r).getTp_produto().equals("NET")) {
					qtd_net = 1;
				}
				if(listProdutoAux.get(r).getTp_produto().equals("FONE")) {
					qtd_fone = 1;		
				}
				
				if(listProdutoAux.get(r).getTp_produto().equals("TV")) {
					qtd_tv = 1;
				}
							
				r++;
			}
			
			// soma variaveis para ver qual o desconto a ser aplicado
			combo = qtd_net + qtd_fone + qtd_tv;
						
			// monta lista de planos que serao retornados pela api
			if(listProdutoAux.size() > 0) {
				
				while(y < listProdutoAux.size()) {
					
					//System.out.println("referencia add plano: " + listProdutoAux.get(y).getReferencia());
					//System.out.println("TESTE SE PASSO DESSA PORRA: " + listProdutoAux.get(y).getReferencia());
					
					Products produto = productsRep.buscaPlanoPorReferencia(listProdutoAux.get(y).getReferencia());
					
					//System.out.println("TESTE SE PASSO DESSA PORRA: " + listProdutoAux.get(y).getReferencia());
					
					if(combo == 1) {
						desconto = produto.getDesc_single();
					}else if(combo == 2) {
						desconto =  produto.getDesc_dual();
					}else if(combo == 3) {
						desconto =  produto.getDesc_triple();
					}else {
						desconto = produto.getDesc_single();
					}
									
					ProdutosObrigatorios produtoAtualizado = new ProdutosObrigatorios();
					produtoAtualizado = listProdutoAux.get(y);				
					
					produtoAtualizado.setDesconto(desconto);
					produtoAtualizado.setValorMensal(String.format("%.2f", Float.parseFloat(produto.getValor()) - Float.parseFloat(desconto)));	
					produtoAtualizado.setDescricao_plano_antigo(listProdutoAux.get(y).getDescricao_plano_antigo());
					
					produtoAtualizado.setNovo_antigo(listProdutoAux.get(y).getNovo_antigo());
					
					planos.add(produtoAtualizado);
					
					y++;
					
				}
				
			}
			
						
			if(planos.size() > 0) {
								
				while(j < planos.size()) {
					
					//System.out.println("planos: " + planos.get(j).getDescricao());
					
					if(planos.get(j).getTp_produto().equals("NET")) {
						
						qtd_net_validacao ++;
						
					}
					
					if(planos.get(j).getTp_produto().equals("FONE")) {
						
						qtd_fone_validacao ++;
						
						if(qtd_fone_validacao <= 1) {
							validacao_fone = planos.get(j).getReferencia();
							//System.out.println("entrou no if que add dados na variavel validacao_fone: " + validacao_fone);
						}else {
							if(!validacao_fone.equals(planos.get(j).getReferencia())) {
								
								//System.out.println("entrou no lugar onde vai retornar validacao do telefone");
								
								planos = new ArrayList<>();
								responseProdutosObrigatoriosFluig = new ResponseProdutosObrigatoriosRenovacaoFluig();
								planos = removePlanoArray(planos, "ASS_FTTH");
								
								responseProdutosObrigatoriosFluig.setPlanos(planos);
								responseProdutosObrigatoriosFluig.setCombo(String.valueOf(combo));
								responseProdutosObrigatoriosFluig.setCodigo("202");
								responseProdutosObrigatoriosFluig.setMensagem("Não pode haver planos diferentes de telefone no mesmo contrato");
								
								return new ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig>(responseProdutosObrigatoriosFluig, HttpStatus.OK);
							}
						}
												
					}
					
					j++;
				}
				
				// valida para nao deixar renovar plano de internet se ja foi adicionado plano de net na tabela de baixo
				if(qtd_net_validacao > 1) {
					responseProdutosObrigatoriosFluig.setPlanos(planos);
					responseProdutosObrigatoriosFluig.setCombo(String.valueOf(combo));
					responseProdutosObrigatoriosFluig.setCodigo("202");
					responseProdutosObrigatoriosFluig.setMensagem("Não pode haver mais de um plano de internet no mesmo contrato");
					
					return new ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig>(responseProdutosObrigatoriosFluig, HttpStatus.OK);
				}
				
				// se entrar nesse if retorna que o plano nao esta mais ativo e q foi adicionado outro no lugar
				if(!msg_retorno.equals("")) {
					responseProdutosObrigatoriosFluig.setPlanos(planos);
					responseProdutosObrigatoriosFluig.setCombo(String.valueOf(combo));
					responseProdutosObrigatoriosFluig.setCodigo("203");	
					responseProdutosObrigatoriosFluig.setMensagem(msg_retorno);
				}
				else { //se entrar no else retorna o retorno padrao q deu certo
					responseProdutosObrigatoriosFluig.setPlanos(planos);
					responseProdutosObrigatoriosFluig.setCombo(String.valueOf(combo));
					responseProdutosObrigatoriosFluig.setCodigo("201");	
					responseProdutosObrigatoriosFluig.setMensagem("OK");
				}
				
				
			}else {
				
				if(qtd_pontos_adicionais_tv > 0) {
					
					responseProdutosObrigatoriosFluig.setPlanos(planos);
					responseProdutosObrigatoriosFluig.setCombo(String.valueOf(combo));
					responseProdutosObrigatoriosFluig.setCodigo("204");
					responseProdutosObrigatoriosFluig.setMensagem("Renovado só ponto adicional");
					
				}else {
					responseProdutosObrigatoriosFluig.setPlanos(planos);
					responseProdutosObrigatoriosFluig.setCombo(String.valueOf(combo));
					responseProdutosObrigatoriosFluig.setCodigo("202");
					responseProdutosObrigatoriosFluig.setMensagem("Nenhum Plano Renovado");
				}
				
			}			
			
		}catch(Exception e) {
			responseProdutosObrigatoriosFluig.setPlanos(planos);
			responseProdutosObrigatoriosFluig.setCombo("0");
			responseProdutosObrigatoriosFluig.setCodigo("501");
			responseProdutosObrigatoriosFluig.setMensagem("Erro na consulta dos produtos, tente novamente mais tarde!");
			
			//System.out.println("deu erro api renovacao-retencao retornaPlanoCorrespondente: " + e.getMessage());
			e.printStackTrace();
		}		
		
		return new ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig>(responseProdutosObrigatoriosFluig, HttpStatus.OK);
	}
		
	public List<ProdutosObrigatorios> validaListaAuxiliar(List<ProdutosObrigatorios> listProdutoAux, ParametrosPlanoCorrespondente dadosParametro) {

		int h = 0;
		int g = 0;
		int k = 0;
		
		String ids_item_planos = "";
		String lista_sva_obg = "";
		
		List<ProdutosObrigatorios> listProdutoAuxRetorno = new ArrayList<>();
		
		while(h < dadosParametro.getArrayPlanosContrato().size()) {
			
			if(!dadosParametro.getArrayPlanosContrato().get(h).getId_item_contrato().trim().equals("") && dadosParametro.getArrayPlanosContrato().get(h).getSituacao().equals("C")) {
				ids_item_planos += dadosParametro.getArrayPlanosContrato().get(h).getId_item_contrato().trim() + ";";
			}			
			
			h++;			
		}
		
		while(g < listProdutoAux.size()) {
			
			if(ids_item_planos.indexOf(listProdutoAux.get(g).getId_item_contrato().trim()) != -1) {
				
				lista_sva_obg += listProdutoAux.get(g).getReferencia_sva();
				
			}
			
			g++;
		}
		
		while(k < listProdutoAux.size()) {			
				
			if(ids_item_planos.indexOf(listProdutoAux.get(k).getId_item_contrato().trim()) == -1 && lista_sva_obg.indexOf(listProdutoAux.get(k).getReferencia().trim()) == -1) {
				
				listProdutoAuxRetorno.add(listProdutoAux.get(k));
				
			}	
			
			k++;
		}
		
		return listProdutoAuxRetorno;
		
	}

	// aqui e verificado se o plano ja foi renovado para nao remover e add novamente, resolvendo dessa forma o problema onde por exemplo, renovando plano de 250 e trocando para 50 ao renovar outro plano qualquer  voltava pro 250 mega
//	public String percorrePlanosRevovados(ParametrosPlanoCorrespondente dadosParametro , String codProd, int qtd_ponto_adicional_parametro) {
//		
//		//System.out.println("chamou funcao percorrePlanosRevovados: " + codProd);
//
//		String retorno = "01";// 01 = pode adicionar o plano; 02 = ja existe o plano e nao deve ser adicionado; 03 = o plano atual deve ser removido e adicionado novamente atualizado
//		int v = 0;
//		int g = 0;
//		int qtd_ponto_adicional = 0;
//		int vld_ponto_adicional = 0;
//		String controle_plano = "";
//		
////		while(v < dadosParametro.getArrayPlanosRenovados() .size()) {			
////			
////			String prodAtualizado = planosCorrespondentesRepository.listaPlanoCorrespondenteTV(codProd, "0").get(0).getCod_plano_atualizado();
////			
////			if(dadosParametro.getArrayPlanosRenovados().get(v).getCod_antigo().trim().equals(prodAtualizado.trim()) || dadosParametro.getArrayPlanosRenovados().get(v).getCod_prod_anterior().trim().equals(prodAtualizado.trim()) )
////				retorno = false;
////			v++;
////		}
//		
//		// verifica se tem ponto adicional (contratos antigos sem sva)
////		while(g < dadosParametro.getArrayPlanosContrato().size()) {
////			
////			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(g).getCod_antigo(), "0").equals("PONTO_ADIC")) {
////				vld_ponto_adicional++;
////			}
////			
////			g++;
////		}
//		
//		while(v < dadosParametro.getArrayPlanosRenovados().size()) {			
//			
//			//String prodAtualizado = planosCorrespondentesRepository.listaPlanoCorrespondenteTV(codProd, "0").get(0).getCod_plano_atualizado();
//			
//			if(dadosParametro.getArrayPlanosRenovados().get(v).getId_item_contrato().trim().equals(codProd.trim())) {				
//				
////				if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(v).getCod_antigo(), "0").equals("TV") && vld_ponto_adicional > 0) {
////					controle_plano = "TV";
////					qtd_ponto_adicional += Integer.parseInt(dadosParametro.getArrayPlanosRenovados().get(v).getReferencia_plano().replaceAll("[^0-9]", ""));
////				}else {
//					retorno = "02";
////				}
//			}				
//				
//				
//			v++;
//		}
////		//System.out.println("qtd_ponto_adicional_parametro: " + qtd_ponto_adicional_parametro + " qtd_ponto_adicional: " + qtd_ponto_adicional);
//		
////		if(qtd_ponto_adicional_parametro != qtd_ponto_adicional) {
////			retorno = "03";
////		}
//		
////		//System.out.println("retorno percorrePlanosRevovados: " + retorno + " codProd: " + codProd +  " vld_ponto_adicional: " + vld_ponto_adicional);
//		
//		return retorno;		
//		
//	}
	
	public String percorrePlanosRevovados(ParametrosPlanoCorrespondente dadosParametro , String codProd, int qtd_ponto_adicional_parametro) {
		
		//System.out.println("chamou funcao percorrePlanosRevovados: " + codProd);

		String retorno = "01";// 01 = pode adicionar o plano; 02 = ja existe o plano e nao deve ser adicionado; 03 = o plano atual deve ser removido e adicionado novamente atualizado
		int v = 0;
		
		while(v < dadosParametro.getArrayPlanosRenovados().size()) {	
			
			if(dadosParametro.getArrayPlanosRenovados().get(v).getId_item_contrato().trim().equals(codProd.trim())) {				
				
				retorno = "02";
			}	
				
			v++;
		}
				
		return retorno;		
		
	}
	
	public String percorrePlanosRevovadosReferencia(ParametrosPlanoCorrespondente dadosParametro , String referencia, int qtd_ponto_adicional_parametro) {
		
		//System.out.println("chamou funcao percorrePlanosRevovadosReferencia: " + referencia);

		String retorno = "01";// 01 = pode adicionar o plano; 02 = ja existe o plano e nao deve ser adicionado; 03 = o plano atual deve ser removido e adicionado novamente atualizado
		int v = 0;
		
		while(v < dadosParametro.getArrayPlanosRenovados().size()) {	
			
			if(dadosParametro.getArrayPlanosRenovados().get(v).getReferencia_plano().trim().equals(referencia.trim())) {				
				
				retorno = "02";
			}	
				
			v++;
		}
				
		return retorno;		
		
	}

	// essa funcao recebe os dados das duas tabelas e retorna lista atualizada com tudo que precisa ser adicionado na tabela de baixo
	public ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig> servicosObrigatoriosRenovacao(ParametrosPlanoCorrespondente dadosParametro) {
		
		int d = 0;
		int e = 0;
		int x = 0;
		int g = 0;
		int u = 0;
		int j = 0;
		int s = 0;
	    int f = 0;
		int qtd_tv = 0;
		int qtd_ponto_adicional = 0;
		int qtd_ins_ftth = 0;
		int qtd_ass_ftth = 0;
		int qtd_ass_ftth_renovado = 0;
		int soma_qtd_ponto_adicional = 0;
		int cont_net = 0;
		int cont_fone = 0;
		int cont_tv = 0;
		int combo = 0;
		int qtd_planos_fone = 0;
		int diferenca_pontos_adicionais = 0;
		int controle_assinatura_ftth = 0;
		int qtd_assinatura_remocao = 0;
		int qtd_ins_roteador = 0;
		int qtd_plano_roteador = 0;
		int qtd_plano_roteador_alg_rotac = 0;
		int qtd_ins_roteador_alg_rotac = 0;
		int qtd_plano_roteador_alg_swith = 0;
		int qtd_ins_roteador_alg_swith = 0;
		
		 AuxiliarObrigatoriosRenovacao auxiliarObrigatoriosRenovacao;
		 List<AuxiliarObrigatoriosRenovacao> listObrigatorios = new ArrayList<>();		
				
		// variaveis validacao
		int qtd_ponto_adicional_contratado = 0;
		String produtos_obrigatorios=  "";
		List<String> listReferencias = new ArrayList<>(); // contem as referencias dos planos P1 passados por parametro
		List<String> listReferenciasProdutosObrigatorios = new ArrayList<>(); // contem as referencias dos produtos obrigatorios
		List<String> listProdutosContratadosRenovados = new ArrayList<>(); // contem as referencias dos produtos contratados e renovados
		String servicosObrigatorios = ""; // lista com todos os servicos obrigatorios
		List<ProdutosObrigatorios> planos = new ArrayList<>();
		
		// retorno da funcao
		ResponseProdutosObrigatoriosRenovacaoFluig responseProdutosObrigatoriosFluig = new ResponseProdutosObrigatoriosRenovacaoFluig();
		
		// percorre a tabela de dados ja contratados para ver o que o cliente ja tem
		while(d < dadosParametro.getArrayPlanosContrato().size()) {
		
			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("TV")) {
				
				qtd_tv++;
				qtd_ins_ftth++;
				//cont_tv = 1;
				
				if(dadosParametro.getArrayPlanosContrato().get(d).getDescricao_plano().replaceAll("[^0-9]", "").length() > 0) {
					qtd_ponto_adicional += Integer.parseInt(dadosParametro.getArrayPlanosContrato().get(d).getDescricao_plano().replaceAll("[^0-9]", "")) - 1;
				}
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("PONTO_ADIC") && !dadosParametro.getArrayPlanosContrato().get(d).getSituacao().equals("C")) {
				
				qtd_ponto_adicional++;
			}			
						
			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("FONE")) {
				//cont_fone = 1;
				qtd_ins_ftth++;					
			}
			if( buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("NET")) {
				//cont_net = 1;
				qtd_ins_ftth++;	
			}
			
			if( buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("ASS_FTTH")) {				
				qtd_ass_ftth++;	
			}
		
			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("ALG_ROTMESH") || buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("COMO_ROTMESH")) {				
				qtd_ins_roteador++;	
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("ALG_ROTAC")) {				
				qtd_ins_roteador_alg_rotac++;	
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(d).getCod_antigo(), "0").equals("ALG_SWITCH")) {				
				qtd_ins_roteador_alg_swith++;	
			}

			d++;
		}	
				
		// percorre tabela de baixo dos itens contratados e renovados
		while(e < dadosParametro.getArrayPlanosRenovados().size()) {
			
			//System.out.println("entrou no dados parametros v2");
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("TV")) {	
				
				cont_tv = 1;
								
				if(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano().replaceAll("[^0-9]", "").length() > 0) {
					qtd_ponto_adicional_contratado = Integer.parseInt(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano().replaceAll("[^0-9]", "")) - 1;
				}	
				
				//System.out.println("teste qtd pontos contratados: " + dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano().replaceAll("[^0-9]", ""));
						
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
			}	
									
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("PACOTE_TV")) {	
				
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());		    	
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    		
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
			}
						
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("FONE")) {
				cont_fone = 1;
				qtd_planos_fone++;
				qtd_ins_ftth++;			

				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
		    	
		    	// verifica se precisa add assinatura, so add assinatura se o plano de telefone for novo, ou seja, se nao estiver sendo renovado
		    	if(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo().equals("N")) {
		    		controle_assinatura_ftth++;
		    	}else {
		    		qtd_assinatura_remocao++;
		    	}
			}
			if( buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("NET")) {
				cont_net = 1;
				qtd_ins_ftth++;	
				
				//System.out.println("entrou no dados net v2");
				
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    			    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
				
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("ASS_FTTH")) {
				qtd_ass_ftth_renovado++;	
					
				//System.out.println("ENTROU NAS ASSINATURAS");
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
			}
						
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("SVA") && buscaCategoria(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano()).equals("P1")) {
			//if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("SVA") && dadosParametro.getFidelidade().equals("N")) {
			//if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("SVA")) {
									
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    			    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("BLQ_CHAMADA")) {
				
					
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setObs_item(dadosParametro.getArrayPlanosRenovados().get(e).getObs_item());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("COMO_ROTMESH")) {
				
				
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setObs_item(dadosParametro.getArrayPlanosRenovados().get(e).getObs_item());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
		    	
		    	qtd_plano_roteador++;
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("ALG_ROTMESH")) {
				
				
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setObs_item(dadosParametro.getArrayPlanosRenovados().get(e).getObs_item());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
		    	
		    	qtd_plano_roteador++;
		    	
			}
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("ALG_ROTAC")) {
				
				
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setObs_item(dadosParametro.getArrayPlanosRenovados().get(e).getObs_item());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
		    	
		    	qtd_plano_roteador_alg_rotac++;
		    	
			} 
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo(), "0").equals("ALG_SWITCH")) {
				
				
				auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
		    	//auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_antigo());
				auxiliarObrigatoriosRenovacao.setCod_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getCod_prod_anterior());
		    	auxiliarObrigatoriosRenovacao.setId_item_contrato(dadosParametro.getArrayPlanosRenovados().get(e).getId_item_contrato());
		    	auxiliarObrigatoriosRenovacao.setReferencia(dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_plano());
		    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getDescricao_plano_antigo());
		    	
		    	auxiliarObrigatoriosRenovacao.setSituacao(dadosParametro.getArrayPlanosRenovados().get(e).getSituacao());
		    	auxiliarObrigatoriosRenovacao.setObs_item(dadosParametro.getArrayPlanosRenovados().get(e).getObs_item());
		    	auxiliarObrigatoriosRenovacao.setNovo_antigo(dadosParametro.getArrayPlanosRenovados().get(e).getNovo_antigo());
		    	
		    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);
		    	
		    	qtd_plano_roteador_alg_swith++;
		    	
			}
			
			servicosObrigatorios = dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_obrigatorios() + ";" + dadosParametro.getArrayPlanosRenovados().get(e).getReferencia_sva_obrigatorio();
			
			listReferenciasProdutosObrigatorios.addAll(Arrays.asList(servicosObrigatorios.split(";")));	// produtos obrigatorios					
						
			e++;
			
		}					
		
		//System.out.println("LISTA DE PRODUTOS FELIPE");
		//System.out.println(listReferenciasProdutosObrigatorios);
		
		// remove itens duplicados da lista
	    List<String> listReferenciasProdutosObrigatoriosLimpo = listReferenciasProdutosObrigatorios.stream().distinct().collect(Collectors.toList());	    
	    
	    //System.out.println(listReferenciasProdutosObrigatoriosLimpo);
	    
	    listReferenciasProdutosObrigatoriosLimpo.addAll(listReferencias); 
	    
	    if(qtd_ponto_adicional_contratado > qtd_ponto_adicional) {
			diferenca_pontos_adicionais = qtd_ponto_adicional_contratado - qtd_ponto_adicional;
		}
	    
	    //System.out.println("dif ponto adi: " + diferenca_pontos_adicionais + " qtd_ponto_adicional_contratado: " + qtd_ponto_adicional_contratado + " qtd_ponto_adicional: " + qtd_ponto_adicional + " qtd_tv: " + qtd_tv);
		
		// adiciona a instalacao dos pontos adicionais de tv caso houver necessidade
	    if(diferenca_pontos_adicionais > 0) {
	    	// busca a referencia do ponto adicional de tv conforme a cidade
	    	String referenciaInstalacaoPontoAdicionalPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "INS_PONTO_ADIC", "", dadosParametro.getFidelidade());
	    	
	    	while (u < diferenca_pontos_adicionais) {
	    		
	    		listReferenciasProdutosObrigatoriosLimpo.add(referenciaInstalacaoPontoAdicionalPorCidade); // adiciona as instalacoes de ponto adicional de tv	    		
	    		u++;
	    	}	    	
	    	
	    }
	    	    
	    if(controle_assinatura_ftth - qtd_ass_ftth_renovado <= 0) {
	    	
	    	String referenciaAssinaturaFtthPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "ASS_FTTH", "", dadosParametro.getFidelidade());
	    	
	    	listReferenciasProdutosObrigatoriosLimpo = removeServicoObrigatorio(listReferenciasProdutosObrigatoriosLimpo, referenciaAssinaturaFtthPorCidade);
	    	
	    }	  
	       	    
	    if((controle_assinatura_ftth - qtd_ass_ftth_renovado - qtd_assinatura_remocao) > 1) {
	    	
	    	String referenciaAssinaturaFtthPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "ASS_FTTH", "", dadosParametro.getFidelidade());
	    	
	    	while(j < (controle_assinatura_ftth - qtd_ass_ftth_renovado - qtd_assinatura_remocao)  - 1) {
	    		
	    		listReferenciasProdutosObrigatoriosLimpo.add(referenciaAssinaturaFtthPorCidade); // adiciona assinatura para cada linha de telefone
	    		
	    		j++;
	    	}
	    	
	    }	    
 		
 		int qtd_ata =(int)qtd_planos_fone/3;
 		int w = 0;
 		
 		if(qtd_ata > 0) {
 			
 			String referenciaATAPorCidade = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "SERV_ATA", "", dadosParametro.getFidelidade());
 			
 			while(w < qtd_ata) {
 				listReferenciasProdutosObrigatoriosLimpo.add(referenciaATAPorCidade); // adiciona assinatura para cada linha de telefone
 	 			w++;
 	 		}
 			
 		} 				
	    	    
	    combo = cont_net + cont_fone + cont_tv; 
	  	   
	    while(s < listReferenciasProdutosObrigatoriosLimpo.size()) {
	    	
	    	auxiliarObrigatoriosRenovacao = new AuxiliarObrigatoriosRenovacao();
	    	auxiliarObrigatoriosRenovacao.setCod_prod_antigo("");
	    	auxiliarObrigatoriosRenovacao.setId_item_contrato("");
	    	auxiliarObrigatoriosRenovacao.setDescricao_prod_antigo("");
	    	auxiliarObrigatoriosRenovacao.setReferencia(listReferenciasProdutosObrigatoriosLimpo.get(s));
	    	
	    	listObrigatorios.add(auxiliarObrigatoriosRenovacao);	    	
	    		    	
	    	s++;
	    }	        

	    responseProdutosObrigatoriosFluig = validaPlanos(listObrigatorios, combo, diferenca_pontos_adicionais);	    	  
	    	    
	    if(qtd_ins_ftth > 0) {
	    		    	
	    	responseProdutosObrigatoriosFluig.setPlanos(removePlanoArray(responseProdutosObrigatoriosFluig.getPlanos(), "INS_FTTH"));
	    }
	    
	    if(qtd_tv > 0) {
	    	responseProdutosObrigatoriosFluig.setPlanos(removePlanoArray(responseProdutosObrigatoriosFluig.getPlanos(), "INS_TV"));
	    }	
	    
	    if(qtd_ass_ftth_renovado > 0 && qtd_planos_fone <= 0) {
	    	responseProdutosObrigatoriosFluig.setPlanos(removePlanoArray(responseProdutosObrigatoriosFluig.getPlanos(), "ASS_FTTH"));
	    }
	    
	    if(diferenca_pontos_adicionais <= 0) {
	    	responseProdutosObrigatoriosFluig.setPlanos(removePlanoArray(responseProdutosObrigatoriosFluig.getPlanos(), "INS_PONTO_ADIC"));
	    }
	    
	    if(qtd_plano_roteador <= qtd_ins_roteador) {
	    	responseProdutosObrigatoriosFluig.setPlanos(removePlanoArray(responseProdutosObrigatoriosFluig.getPlanos(), "INS_ROTMESH"));
	    }
	    	    
	    if(qtd_plano_roteador_alg_rotac <= qtd_ins_roteador_alg_rotac) {
	    	responseProdutosObrigatoriosFluig.setPlanos(removePlanoArray(responseProdutosObrigatoriosFluig.getPlanos(), "INS_ROTAC"));
	    }
	    
	    if(qtd_plano_roteador_alg_swith <= qtd_ins_roteador_alg_swith) {
	    	responseProdutosObrigatoriosFluig.setPlanos(removePlanoArray(responseProdutosObrigatoriosFluig.getPlanos(), "INS_SWITCH"));
	    }
		
		return new ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig>(responseProdutosObrigatoriosFluig, HttpStatus.OK); 
		
	}
	
	//remove a referencia do servico obrigatorio da lista passada por parametro
	public List<String> removeServicoObrigatorio(List<String> listReferenciasProdutosObrigatoriosParametro, String referenciaProduto) {
		
		List<String> listReferenciasProdutosObrigatoriosRetorno = new ArrayList<>();
		int x = 0;
		
		while(x < listReferenciasProdutosObrigatoriosParametro.size()) {
			
			if(!listReferenciasProdutosObrigatoriosParametro.get(x).equals(referenciaProduto)) {
				listReferenciasProdutosObrigatoriosRetorno.add(listReferenciasProdutosObrigatoriosParametro.get(x));
			}
			
			x++;
		}
		
		return listReferenciasProdutosObrigatoriosRetorno;
		
	}

	// busca o tipo do plano
	public String buscaTipoPlano(String cod_produto, String qtd_pontos_adicionais) {
		
		List<PlanoCorrespondente> list = new ArrayList<>();
		String retorno = "";
		
		list = planosCorrespondentesRepository.buscaTipoPlano(cod_produto, qtd_pontos_adicionais);	
		
		if(list.size() > 0) {
			retorno = list.get(0).getTp_plano();
		}
		
		return retorno;
		
	}
	
	// funcao responsavel por validar e retornar a lista de planos atualizada
	public ResponseProdutosObrigatoriosRenovacaoFluig validaPlanos(List<AuxiliarObrigatoriosRenovacao> listReferenciasProdutosObrigatoriosLimpo, int combo, int diferenca_pontos_adicionais) {
		
		ProdutosObrigatorios produtosObrigatorios;
		List<ProdutosObrigatorios> listPlanosResponse = new ArrayList<>();
		ResponseProdutosObrigatoriosRenovacaoFluig produtosObrigatoriosFluig;
		
		String desconto;
		int qtd_tv = 0;
		int qtd_planos_net = 0;
		int qtd_planos_fone_validacao = 0;
		int qtd_pacote_tv = 0;
		int qtd_assinatura = 0;
		int qtd_vld_ponto_adicional = 0;
		String msg_retorno = "OK";
		int y = 0;
		int t = 0;
		int controle_a_la_carte = 0;
		
		// aqui adiciona todos os planos e servicos obrigatorios que serao retornados pela api
	    if(listReferenciasProdutosObrigatoriosLimpo.size() > 0) {
	    	
	    	while(y < listReferenciasProdutosObrigatoriosLimpo.size()) {
	    		
	    		//System.out.println("wdwedwedwed: " + listReferenciasProdutosObrigatoriosLimpo.get(y).getReferencia());
								
				// aqui e feito consulta de cada plano para buscar seus dados e retornar a lista de todos os planos com seus respectivos valores e descontos
				Products produto = productsRep.buscaPlanoPorReferencia(listReferenciasProdutosObrigatoriosLimpo.get(y).getReferencia());	
				
				if(combo == 1) {
					desconto = produto.getDesc_single();
				}else if(combo == 2) {
					desconto = produto.getDesc_dual();
				}else if(combo == 3) {
					desconto = produto.getDesc_triple();
				}else {
					desconto = produto.getDesc_single();
					combo = 1;
				}
								
				produtosObrigatorios = new ProdutosObrigatorios();
				produtosObrigatorios.setCategoria(produto.getCategoria());				
				produtosObrigatorios.setCod_totvs(produto.getCod_totvs());
				produtosObrigatorios.setDesconto(desconto);
				produtosObrigatorios.setDescricao(produto.getDescricao());
				produtosObrigatorios.setDownload(produto.getDownload());
				produtosObrigatorios.setFidelidade(produto.getFidelidade());
				produtosObrigatorios.setId_promocao(produto.getId_promocao());
				produtosObrigatorios.setQtd_mes_cob(produto.getQtd_mes_cob());
				produtosObrigatorios.setQtd_mes_ini(produto.getQtd_mes_ini());
				produtosObrigatorios.setReferencia(produto.getReferencia());
				produtosObrigatorios.setReferencia_obg(produto.getReferencia_obg());
				produtosObrigatorios.setReferencia_sva(produto.getReferencia_sva());
				produtosObrigatorios.setRepetivel(produto.getRepetivel());
				produtosObrigatorios.setTp_produto(produto.getTp_produto());
				produtosObrigatorios.setUpload(produto.getUpload());
				produtosObrigatorios.setValorCheio(produto.getValor());
				produtosObrigatorios.setVisivel(produto.getVisivel());
				produtosObrigatorios.setEquivalencia(produto.getEquivalencia());
				produtosObrigatorios.setValorMensal(String.format("%.2f", Float.parseFloat(produto.getValor()) - Float.parseFloat(desconto)));	
				produtosObrigatorios.setCod_prod_antigo(listReferenciasProdutosObrigatoriosLimpo.get(y).getCod_prod_antigo());
				produtosObrigatorios.setId_item_contrato(listReferenciasProdutosObrigatoriosLimpo.get(y).getId_item_contrato());
				produtosObrigatorios.setObs_item(listReferenciasProdutosObrigatoriosLimpo.get(y).getObs_item());
				produtosObrigatorios.setDescricao_plano_antigo(listReferenciasProdutosObrigatoriosLimpo.get(y).getDescricao_prod_antigo());
				produtosObrigatorios.setSituacao(listReferenciasProdutosObrigatoriosLimpo.get(y).getSituacao());
				produtosObrigatorios.setNovo_antigo(listReferenciasProdutosObrigatoriosLimpo.get(y).getNovo_antigo());
				
				if(produto.getTp_produto().equals("PACOTE_TV")) {
					qtd_pacote_tv ++;
				}
				
				if(produto.getTp_produto().equals("INS_PONTO_ADIC")) {
					qtd_vld_ponto_adicional ++;
					produtosObrigatorios.setQtd_mes_cob("1");
				}
				
				if(produto.getTp_produto().equals("ASS_FTTH")) {
					qtd_assinatura ++;
				}
				
				if(produtosObrigatorios.getTp_produto().equals("TV")) {	
					if(produtosObrigatorios.getDescricao().indexOf("BRONZE") != -1 || produtosObrigatorios.getDescricao().indexOf("PRATA") != -1 || produtosObrigatorios.getDescricao().indexOf("OURO") != -1) {
						controle_a_la_carte++;
					}
				}					
											
				// validacao para nao deixar contratar planos repetidos que nao podem ser repetidos
				if(produtosObrigatorios.getRepetivel().equals("N")) {
					
					if(produto.getTp_produto().equals("NET")) {
						qtd_planos_net++;
					}
														
					// se o produto nao existir na lista entao e adicionado
					if(!listPlanosResponse.contains(produtosObrigatorios)){
						listPlanosResponse.add(produtosObrigatorios);	
					}
					
					if(qtd_planos_net > 1) {
					//if(!listPlanosResponse.contains(produtosObrigatorios)) {
						
						if(!produtosObrigatorios.getTp_produto().equals("SERV_ATA")) {
							////System.out.println("NÃ£o Ã© possivel inserir mais de um plano " + produtosObrigatorios.getDescricao() + " no mesmo contrato" );
							msg_retorno = "NÃ£o Ã© possivel inserir planos duplicados no mesmo contrato" ;	
							
						}else {
							listPlanosResponse.add(produtosObrigatorios);
						}
						
					}
					
				}else {
					
					// validacao para nao deixar contratar planos diferentes de telefone na mesma venda
					if(produtosObrigatorios.getTp_produto().equals("FONE")) {					
						
						qtd_planos_fone_validacao++;
						
						if(qtd_planos_fone_validacao <= 1) {
							listPlanosResponse.add(produtosObrigatorios);
						}else {
							if(listPlanosResponse.contains(produtosObrigatorios)) {
								listPlanosResponse.add(produtosObrigatorios);
							}else {
								
								listPlanosResponse = removePlanoArray(listPlanosResponse, "ASS_FTTH");	
								listPlanosResponse = removePlanoArray(listPlanosResponse, "FONE");	
								
								msg_retorno = "Não é possivel inserir planos diferentes de telefone no mesmo contrato" ;		
							}
						}
						
						
					}else {
						listPlanosResponse.add(produtosObrigatorios);
					}					
				}
				
				// aqui verifica se tem o ATA na lista de produtos, caso tenha é validado se realmente precisa do ATA, caso nao precise ele e removido
				if(produtosObrigatorios.getTp_produto().equals("SERV_ATA")) {					
										
					listPlanosResponse = validaServicoATA(listPlanosResponse);
				}	
				
				//System.out.println("qtd_pacote_tv: " + qtd_pacote_tv + " qtd_tv: " + qtd_tv + " controle_a_la_carte: " + controle_a_la_carte);
				
				// nao deixa contratar pacotes a la carte senao tiver adicionado plano de tv
				if(qtd_pacote_tv > 0  && qtd_tv <= 0 && controle_a_la_carte <= 0) {
					
					listPlanosResponse = removePlanoArray(listPlanosResponse, "PACOTE_TV");								
					
					msg_retorno = "Para Contratar A LA CARTE é necessário ter ao menos um plano de TV Bronze" ;	
					
				}
								
				y++;
				
			}	
	    	
	    	if(qtd_planos_net > 1) {
	    		listPlanosResponse = removePlanoArray(listPlanosResponse, "NET");	
	    		msg_retorno = "Não é possivel inserir planos duplicados no mesmo contrato" ;		
	    		//System.out.println("deu certo entrou aqui");
	    	}
	    	
	    	if(qtd_vld_ponto_adicional > 0 ) {
	    		
	    		if(qtd_vld_ponto_adicional - diferenca_pontos_adicionais > 0) {
	    			
	    			while(t < (qtd_vld_ponto_adicional - diferenca_pontos_adicionais)) {
	    				
	    				listPlanosResponse = removePlanoArray(listPlanosResponse, "INS_PONTO_ADIC");	
	    				
	    				t++;
	    			}
	    		}
	    		
	    	}
	    	
	    	if(qtd_assinatura > qtd_planos_fone_validacao) {
	    		
	    		int f = 0;
	    		while(f < qtd_assinatura - qtd_planos_fone_validacao) {
	    			
	    			listPlanosResponse = removePlanoArray(listPlanosResponse, "ASS_FTTH");	
	    			
	    			f++;
	    		}
	    		
	    	}
	    	
	    	produtosObrigatoriosFluig = new ResponseProdutosObrigatoriosRenovacaoFluig();		   
		    produtosObrigatoriosFluig.setMensagem(msg_retorno);		    
		    produtosObrigatoriosFluig.setCombo(Integer.toString(combo));
		    
		    if(msg_retorno.equals("OK")) {
		    	 produtosObrigatoriosFluig.setCodigo("201");
		    }else {
		    	 produtosObrigatoriosFluig.setCodigo("202");
		    }		    
		    		    
		    produtosObrigatoriosFluig.setPlanos(listPlanosResponse);
		    	    	
	    }else {
			produtosObrigatoriosFluig = new ResponseProdutosObrigatoriosRenovacaoFluig();
			listPlanosResponse = new ArrayList<>();
			produtosObrigatoriosFluig.setCodigo("404");
			produtosObrigatoriosFluig.setMensagem("Nenhum registro encontrado");
			produtosObrigatoriosFluig.setPlanos(listPlanosResponse);
	    }
	    
	    return produtosObrigatoriosFluig;
	}
	
	// funcao que redebce a lista de planos e remove o plano conforme tipo passado por parametro
	public List<ProdutosObrigatorios> removePlanoArray(List<ProdutosObrigatorios> listPlanosParametro, String tpPlano) {

		List<ProdutosObrigatorios> listPlanosResponse = new ArrayList<>();

		int i = 0;
		int contRemocao = 0; // controle para remover somente um item do array e nao todos que tenham aquela referencia

		while (i < listPlanosParametro.size()) {

			if (listPlanosParametro.get(i).getTp_produto().equals(tpPlano) && contRemocao <= 0) {

				contRemocao++;

			} else {
				listPlanosResponse.add(listPlanosParametro.get(i));
			}

			i++;
		}

		return listPlanosResponse;
	}
	
	// funcao que recebe a lista de planos e remove o plano conforme codigo passado por parametro
	public List<ProdutosObrigatorios> removePlanoArrayPorCodigo(List<ProdutosObrigatorios> listPlanosParametro, String cod_produto) {
		
		//System.out.println("entrou em removePlanoArrayPorCodigo");

		List<ProdutosObrigatorios> listPlanosResponse = new ArrayList<>();

		int i = 0;
		int contRemocao = 0; // controle para remover somente um item do array e nao todos que tenham aquela referencia

		while (i < listPlanosParametro.size()) {

			if (listPlanosParametro.get(i).getCod_totvs().trim().equals(cod_produto) && contRemocao <= 0) {

				contRemocao++;

			} else {
				listPlanosResponse.add(listPlanosParametro.get(i));
			}

			i++;
		}

		return listPlanosResponse;
	}
	
	// essa funcao recebe a lista de planos e valida se o servico ATA realmente deve existir nessa lista, caso nao precise do ATA entao ele e removido
	public List<ProdutosObrigatorios> validaServicoATA(List<ProdutosObrigatorios> listPlanosParametro) {

		List<ProdutosObrigatorios> listPlanosResponse = new ArrayList<>();

		int i = 0;
		int contRemocao = 0; // controle para remover somente um item do array e nao todos que tenham aquela referencia
		int contFone = 0;
		int contATA = 0;

		while (i < listPlanosParametro.size()) {

			if (listPlanosParametro.get(i).getTp_produto().equals("FONE") && contRemocao <= 0) {
				contFone++;
			}

			if (listPlanosParametro.get(i).getTp_produto().equals("SERV_ATA") && contRemocao <= 0) {
				contATA++;
			}

			i++;
		}
		
		int qtd_ata_validacao= contFone/3;
		int h = 0;
		
		//System.out.println("qtd_ata_validacao: " + qtd_ata_validacao + " contATA: " + contATA);
		
		if(contATA > qtd_ata_validacao) {
			while(h < contATA - qtd_ata_validacao) {
				listPlanosResponse = removePlanoArray(listPlanosParametro, "SERV_ATA");
				h++;
			}
		}else {
			listPlanosResponse = listPlanosParametro;
		}

//		// nesse caso o ATA deve ser removido
//		if (contFone < 3 && contATA > 0) {
//
//			listPlanosResponse = removePlanoArray(listPlanosParametro, "SERV_ATA");
//
//		} else {
//			listPlanosResponse = listPlanosParametro;
//		}

		return listPlanosResponse;
	}
	
	public String buscaCategoria(String referencia_plano) {
		
		String retono = "";
		
		retono = productsRep.buscaCategoria(referencia_plano);
		
		return retono;
	}
   
	// funcao utilizada para retornar o plano correspondente com seus respectivos servicos obrigatorios
//	public ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig> retornaPlanoCorrespondente(ParametrosPlanoCorrespondente dadosParametro) {
//		
//		ResponseProdutosObrigatoriosRenovacaoFluig responseProdutosObrigatoriosFluig = new ResponseProdutosObrigatoriosRenovacaoFluig();
//		
//		int qtd_pontos_adicionais = 0;
//		int i = 0;
//		
//		List<PlanoCorrespondente> listPlanoCorrespondente  = new ArrayList<>();
//		List<Products> listProduto = new ArrayList<>();
//		List<ProdutosObrigatorios> listProdutoAtualizado = new ArrayList<>();
//		AuxiliarPlanosCorrespondente auxiliarPlanosCorrespondente;
//		List<AuxiliarPlanosCorrespondente> listPlanosCancelar = new ArrayList<>();
//		
//		while(i < dadosParametro.getArrayPlanosExistentes().size()) {
//			
//			// VERIFICA A QUANTIDADE DE PONTOS ADICIONAIS
//			if(dadosParametro.getArrayPlanosExistentes().get(i).getTp_plano().equals("PONTO_ADIC")) {
//				
//				qtd_pontos_adicionais ++;
//			}			
//			
//			i++;
//		}
//		
//		//System.out.println("plano: " + dadosParametro.getCod_plano().toString() + " qtd: " + qtd_pontos_adicionais);
//		
//		// busca o plano correspondente
//		listPlanoCorrespondente = planosCorrespondentesRepository.listaPlanoCorrespondenteTV(dadosParametro.getCod_plano().toString(), String.valueOf(qtd_pontos_adicionais));
//				
//		if(listPlanoCorrespondente.size() > 0) {
//			
//			listProduto = productsRep.buscaReferenciaPlanoCorrespondente(listPlanoCorrespondente.get(0).getCod_plano_atualizado(), dadosParametro.getCidade(), dadosParametro.getBairro(), dadosParametro.getFidelidade());
//			
//			if(listProduto.size() > 0) {
//				Products produto = listProduto.get(0);
//				
//				ProdutosObrigatorios produtoAtualizado = new ProdutosObrigatorios();
//				
//				produtoAtualizado = conversorResponse(produto);
//				produtoAtualizado.setCod_prod_antigo(dadosParametro.getCod_plano());
//				produtoAtualizado.setId_item_contrato(dadosParametro.getId_item_contrato());
//				
//				listProdutoAtualizado.add(produtoAtualizado);
//				responseProdutosObrigatoriosFluig.setCancelarPlanos(addPlanosCancelar(dadosParametro));
//												
//				responseProdutosObrigatoriosFluig.setCodigo("201");
//				responseProdutosObrigatoriosFluig.setMensagem("OK");
//				responseProdutosObrigatoriosFluig.setCombo("1");
//				responseProdutosObrigatoriosFluig.setPlanos(listProdutoAtualizado);
//		
//			}else {
//							
//				auxiliarPlanosCorrespondente = new AuxiliarPlanosCorrespondente();
//				auxiliarPlanosCorrespondente.setCod_antigo(dadosParametro.getCod_plano());
//				auxiliarPlanosCorrespondente.setId_item_contrato(dadosParametro.getId_item_contrato());
//				auxiliarPlanosCorrespondente.setTp_plano(dadosParametro.getTp_plano());	
//				listPlanosCancelar.add(auxiliarPlanosCorrespondente);
//				
//				responseProdutosObrigatoriosFluig.setCodigo("505");
//				responseProdutosObrigatoriosFluig.setMensagem("Plano correspondente não encontrado");
//				responseProdutosObrigatoriosFluig.setCombo("1");
//				responseProdutosObrigatoriosFluig.setPlanos(listProdutoAtualizado);
//				responseProdutosObrigatoriosFluig.setCancelarPlanos(listPlanosCancelar);
//				
//			}	
//			
//		}else {
//			
//			auxiliarPlanosCorrespondente = new AuxiliarPlanosCorrespondente();
//			auxiliarPlanosCorrespondente.setCod_antigo(dadosParametro.getCod_plano());
//			auxiliarPlanosCorrespondente.setId_item_contrato(dadosParametro.getId_item_contrato());
//			auxiliarPlanosCorrespondente.setTp_plano(dadosParametro.getTp_plano());	
//			listPlanosCancelar.add(auxiliarPlanosCorrespondente);
//			
//			responseProdutosObrigatoriosFluig.setCodigo("505");
//			responseProdutosObrigatoriosFluig.setMensagem("Plano correspondente não encontrado");
//			responseProdutosObrigatoriosFluig.setCombo("1");
//			responseProdutosObrigatoriosFluig.setPlanos(listProdutoAtualizado);
//			responseProdutosObrigatoriosFluig.setCancelarPlanos(listPlanosCancelar);
//			
//		}	
//		
//		return new ResponseEntity<ResponseProdutosObrigatoriosRenovacaoFluig>(responseProdutosObrigatoriosFluig, HttpStatus.OK);
//	}
	
	// adiciona os planos que devem ser cancelados
//	public List<AuxiliarPlanosCorrespondente> addPlanosCancelar(ParametrosPlanoCorrespondente dadosParametro) {
//		
//		int x = 0;		
//		
//		AuxiliarPlanosCorrespondente auxiliarPlanosCorrespondente;
//		List<AuxiliarPlanosCorrespondente> listPlanosCancelar = new ArrayList<>();
//		
//		while(x < dadosParametro.getArrayPlanosExistentes().size()) {
//			
//			if(dadosParametro.getArrayPlanosExistentes().get(x).getTp_plano().equals("PONTO_ADIC") && dadosParametro.getTp_plano().equals("TV")) {
//				
//				auxiliarPlanosCorrespondente = new AuxiliarPlanosCorrespondente();
//				auxiliarPlanosCorrespondente.setCod_antigo(dadosParametro.getArrayPlanosExistentes().get(x).getCod_antigo());
//				auxiliarPlanosCorrespondente.setId_item_contrato(dadosParametro.getArrayPlanosExistentes().get(x).getId_item_contrato());
//				auxiliarPlanosCorrespondente.setTp_plano("PONTO_ADIC");				
//				
//				listPlanosCancelar.add(auxiliarPlanosCorrespondente);
//			}			
//			
//			x++;
//		}	
//		
//		return listPlanosCancelar;
//		
//	}
	
//	// adiciona os pontos adicionais conforme plano passado por parametro
//	public List<ProdutosObrigatorios> addPontosAdicionais(ProdutosObrigatorios produtosObrigatorios, ParametrosPlanoCorrespondente dadosParametro) {
//		
//		int x = 0;		
//		ProdutosObrigatorios produtoResponse;
//		List<ProdutosObrigatorios> listResponse = new ArrayList<>();
//		
//		while(x < dadosParametro.getArrayPlanosExistentes().size()) {
//			
//			if(dadosParametro.getArrayPlanosExistentes().get(x).getTp_plano().equals("PONTO_ADIC")) {
//				dadosParametro.getArrayPlanosExistentes().get(x).getId_item_contrato();
//				String referencia = productsRep.buscaReferenciaPlanoPorCidade(dadosParametro.getCidade(), dadosParametro.getBairro(), "INS_PONTO_ADIC", "", dadosParametro.getFidelidade());
//				
//				//System.out.println("referencia " + referencia);
//				
//				produtoResponse = conversorResponse(productsRep.buscaPlanoPorReferencia(referencia.toString()));
//				produtoResponse.setCod_prod_antigo(dadosParametro.getArrayPlanosExistentes().get(x).getCod_antigo());
//				produtoResponse.setId_item_contrato(dadosParametro.getArrayPlanosExistentes().get(x).getId_item_contrato());
//				
//				listResponse.add(produtoResponse);
//			}			
//			
//			x++;
//		}	
//		
//		return listResponse;
//		
//	}
	
	// converte os dados no formato da classe de retorno
	public ProdutosObrigatorios conversorResponse(Products produto) {
		
		//System.out.println("conversor response a");
		//System.out.println("conversor response " + produto.getCod_totvs());
		
		ProdutosObrigatorios produtoResponse = new ProdutosObrigatorios();
		
		produtoResponse.setCategoria(produto.getCategoria());
		
		produtoResponse.setCod_totvs(produto.getCod_totvs());
		produtoResponse.setDesconto(produto.getDesc_single());
		produtoResponse.setDescricao(produto.getDescricao());
		produtoResponse.setDownload(produto.getDownload());
		produtoResponse.setFidelidade(produto.getFidelidade());
		produtoResponse.setId_promocao(produto.getId_promocao());
		produtoResponse.setQtd_mes_cob(produto.getQtd_mes_cob());
		produtoResponse.setQtd_mes_ini(produto.getQtd_mes_ini());
		produtoResponse.setReferencia(produto.getReferencia());
		produtoResponse.setReferencia_obg(produto.getReferencia_obg());
		produtoResponse.setReferencia_sva(produto.getReferencia_sva());
		produtoResponse.setRepetivel(produto.getRepetivel());
		produtoResponse.setTp_produto(produto.getTp_produto());
		produtoResponse.setUpload(produto.getUpload());
		produtoResponse.setValorCheio(produto.getValor());
		produtoResponse.setVisivel(produto.getVisivel());
		produtoResponse.setEquivalencia(produto.getEquivalencia());
		produtoResponse.setValorMensal(String.format("%.2f", Float.parseFloat(produto.getValor()) - Float.parseFloat(produto.getDesc_single())));			
		
		return produtoResponse;
	}
	
	public List<AuxiliarSVA> verificaSVAP1(ParametrosPlanoCorrespondente dadosParametro, String cod_net, List<AuxiliarSVA> listaAuxiliar){
		
		int h = 0;
		String cod_net_atualizado = "";
		String lista_sva = "";
		List<Products> referencia_plano_sva;
		String referencia_auxiliar = "";
		
		// tem internet
		if(!cod_net.equals("")) {
			List<PlanoCorrespondente> listPlanoCorrespondente = planosCorrespondentesRepository.listaPlanoCorrespondenteTV(cod_net, "0");
			
			if(listPlanoCorrespondente.size() > 0) {
				cod_net_atualizado = listPlanoCorrespondente.get(0).getCod_plano_atualizado();
				
				ProdutosObrigatorios produtosObrigatorios = conversorResponse(productsRep.buscaReferenciaPlanoCorrespondente(cod_net_atualizado, dadosParametro.getCidade(), dadosParametro.getBairro(), dadosParametro.getFidelidade()).get(0));

				lista_sva = produtosObrigatorios.getReferencia_sva();	
				
			}			
			
		}
		
		//System.out.println("lista sva verificaSVAP1: " + lista_sva);
		
		while(h < dadosParametro.getArrayPlanosContrato().size()) {
			
			//System.out.println("entrou no while do planos");
			
			if(buscaTipoPlano(dadosParametro.getArrayPlanosContrato().get(h).getCod_antigo(), "0").equals("SVA") && (dadosParametro.getArrayPlanosContrato().get(h).getSituacao().equals("R") || dadosParametro.getArrayPlanosContrato().get(h).getSituacao().equals("D") || dadosParametro.getArrayPlanosContrato().get(h).getSituacao().equals("I") || dadosParametro.getArrayPlanosContrato().get(h).getSituacao().equals("A"))) {
				
				//System.out.println("Entrou no if planos");
				
				//System.out.println(dadosParametro.getArrayPlanosContrato().get(h).getCod_antigo());
				//System.out.println(dadosParametro.getCidade());
				//System.out.println(dadosParametro.getBairro());
				//System.out.println(dadosParametro.getFidelidade());
				
				referencia_plano_sva = productsRep.buscaReferenciaPlanoCorrespondente(dadosParametro.getArrayPlanosContrato().get(h).getCod_antigo(), dadosParametro.getCidade(), dadosParametro.getBairro(), dadosParametro.getFidelidade());
				
				//System.out.println("fez consulta");
				//System.out.println(referencia_plano_sva.get(0).getReferencia());
				
				String referenciaAux = referencia_plano_sva.get(0).getReferencia();
				
				//System.out.println("validacao verificaSVAP1: " + lista_sva.indexOf(referenciaAux));
				
				referencia_auxiliar = referenciaAux.replaceAll("-p1", "");
				
				// se nao for encontrado o sva na lista, ele e um sva P1
				if(lista_sva.indexOf(referencia_auxiliar) == -1) {
					
					//System.out.println("entrou no if funcao verificaSVAP1 e vai add " + referencia_plano_sva);
					
					String retFunc = percorrePlanosRevovados(dadosParametro, dadosParametro.getArrayPlanosContrato().get(h).getId_item_contrato(),0);
					String retFuncReferencia = percorrePlanosRevovadosReferencia(dadosParametro, referenciaAux.replaceAll("-p1", ""), 0);
					
					//System.out.println("retorno retFunc: " + retFunc);
					
					if(retFunc.equals("01") && retFuncReferencia.equals("01")) {
						AuxiliarSVA auxiliarSVA = new AuxiliarSVA();
						auxiliarSVA.setCod_plano(dadosParametro.getArrayPlanosContrato().get(h).getCod_antigo());
						auxiliarSVA.setQtd_ponto_adicional(0);
						auxiliarSVA.setId_item_contrato(dadosParametro.getArrayPlanosContrato().get(h).getId_item_contrato());
						auxiliarSVA.setSituacao(dadosParametro.getArrayPlanosContrato().get(h).getSituacao());
						auxiliarSVA.setDescricao_plano(dadosParametro.getArrayPlanosContrato().get(h).getDescricao_plano());
						listaAuxiliar.add(auxiliarSVA);
					}
					
					
				}
			
			}
			
			h++;
		}
		
		return listaAuxiliar;
	}
	
	public ResponseProdutosFluig verificaParametrosTotal(ListaParametrosTotal dadosParametro) {
		
		ResponseProdutosFluig responseProdutosFluig = new ResponseProdutosFluig();
		String mensagem = "";
		
		responseProdutosFluig.setCodigo(200);
		responseProdutosFluig.setMensagem("OK");
		responseProdutosFluig.setProdutos(null);
		
		if(dadosParametro.getFidelidade().equals("") || dadosParametro.getFidelidade().equals(null)) {
			mensagem += "Fidelidade Inválida !";
		}
		
		if(dadosParametro.getCidade().equals("") || dadosParametro.getCidade().equals(null)) {
			mensagem += "Cidade Inválida !";
		}
		
		if(dadosParametro.getBairro().equals("") || dadosParametro.getBairro().equals(null)) {
			mensagem += "Bairro Inválido !";
		}
		
		if(!mensagem.equals("")) {
			responseProdutosFluig.setCodigo(400);
			responseProdutosFluig.setMensagem(mensagem);
		}
		
		return responseProdutosFluig;
	}
	
	public ResponseProdutosFluig retornaTodosProdutos(ListaParametrosTotal dadosParametro) {
		
		ResponseProdutosFluig responseProdutosFluig = new ResponseProdutosFluig();
		List<Products> listFinal = new ArrayList<>();
		
		listFinal = productsRep.listaProdutosGeral(dadosParametro.getCidade(), dadosParametro.getBairro(), dadosParametro.getFidelidade());
		
		// se encontrado algum registro
		if(listFinal.size() > 0) {
			responseProdutosFluig.setCodigo(200);
			responseProdutosFluig.setMensagem("OK");
			responseProdutosFluig.setProdutos(listFinal);
			
		}else {// se nenhum registro for encontrado
			
			responseProdutosFluig.setCodigo(404);
			responseProdutosFluig.setMensagem("Nenhum registro encontrado");
			responseProdutosFluig.setProdutos(listFinal);
		}	
		
		return responseProdutosFluig;
	}
	   
}