package api.rest.response;

import java.util.List;

import api.rest.planos.AuxiliarPlanosCorrespondente;
import api.rest.planos.ProdutosObrigatorios;

public class ResponseProdutosObrigatoriosRenovacaoFluig {
	
	private List<ProdutosObrigatorios> planos;   
	private String codigo;
	private String mensagem;
	private String combo;	 		
	
	public List<ProdutosObrigatorios> getPlanos() {
		return planos;
	}
	public void setPlanos(List<ProdutosObrigatorios> planos) {
		this.planos = planos;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getCombo() {
		return combo;
	}
	public void setCombo(String combo) {
		this.combo = combo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((combo == null) ? 0 : combo.hashCode());
		result = prime * result + ((mensagem == null) ? 0 : mensagem.hashCode());
		result = prime * result + ((planos == null) ? 0 : planos.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseProdutosObrigatoriosRenovacaoFluig other = (ResponseProdutosObrigatoriosRenovacaoFluig) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (combo == null) {
			if (other.combo != null)
				return false;
		} else if (!combo.equals(other.combo))
			return false;
		if (mensagem == null) {
			if (other.mensagem != null)
				return false;
		} else if (!mensagem.equals(other.mensagem))
			return false;
		if (planos == null) {
			if (other.planos != null)
				return false;
		} else if (!planos.equals(other.planos))
			return false;
		return true;
	}
	
	
	
}
