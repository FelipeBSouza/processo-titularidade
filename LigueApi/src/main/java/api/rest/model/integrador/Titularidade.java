/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador;


import api.rest.model.telefonia.Numero;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(schema = "integrador",name = "titularidade")
public class Titularidade implements Serializable{
   
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cd_titular;
    
    private String nome;
     
    private String nr_cpf_cnpj;
    
    private String nr_rg_ie;    
    @JsonIgnore
    @OneToMany(mappedBy = "titular")  
    private List<Numero> numeros;

    public Titularidade() {
    	
    }
    
    public int getCd_titular() {
        return cd_titular;
    }

    public void setCd_titular(int cd_titular) {
        this.cd_titular = cd_titular;
    }

    public String getNr_cpf_cnpj() {
        return nr_cpf_cnpj;
    }

    public void setNr_cpf_cnpj(String nr_cpf_cnpj) {
        this.nr_cpf_cnpj = nr_cpf_cnpj;
    }

    public String getNr_rg_ie() {
        return nr_rg_ie;
    }

    public void setNr_rg_ie(String nr_rg_ie) {
        this.nr_rg_ie = nr_rg_ie;
    }

    public List<Numero> getNumeros() {
		return numeros;
	}

	public void setNumeros(List<Numero> numeros) {
		this.numeros = numeros;
	}

	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
 
    
}
