/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.fibra;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "padrao_Cor")
public class PadraoCor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_padrao", nullable = false)
    private int cd_padrao;
    @Column(name = "ds_padrao")
    private String ds_padrao;
    @Column(name = "qt_porta")
    private Integer qt_porta;
    //
    @OneToMany(mappedBy = "padraoCor", fetch = FetchType.LAZY)
    @OrderBy(value = "porta")
    private List<PadraoPorta> padraoPortas = new ArrayList<PadraoPorta>();
    @OneToMany(mappedBy = "padraoCor", fetch = FetchType.LAZY)
    private List<Splitter> splitters = new ArrayList<Splitter>();

    public PadraoCor() {
    }

    public int getCd_padrao() {
        return cd_padrao;
    }

    public void setCd_padrao(int cd_padrao) {
        this.cd_padrao = cd_padrao;
    }

    public String getDs_padrao() {
        return ds_padrao;
    }

    public void setDs_padrao(String ds_padrao) {
        this.ds_padrao = ds_padrao;
    }

    public List<PadraoPorta> getPadraoPortas() {
        return padraoPortas;
    }

    public void setPadraoPortas(List<PadraoPorta> padraoPortas) {
        this.padraoPortas = padraoPortas;
    }

    public List<Splitter> getSplitters() {
        return splitters;
    }

    public void setSplitters(List<Splitter> splitters) {
        this.splitters = splitters;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PadraoCor other = (PadraoCor) obj;
        if (this.cd_padrao != other.cd_padrao) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.cd_padrao;
        return hash;
    }

    @Override
    public String toString() {
        return ds_padrao;
    }
}
