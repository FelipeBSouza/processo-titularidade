package api.rest.model.integrador;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 *
 * @author Administrador
 */
@XmlRootElement
@Entity
@Table(schema = "integrador",name = "funcionario")
public class Funcionario implements Serializable{
    
    @Id
    @Column(name = "cd_funcionario", nullable = false)
    private int cd_funcionario;
    
    @OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name="cd_pessoa")
    private Pessoa cd_pessoa;
    
    @Column(name = "dt_admissao")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dt_admissao;
    
    @Column(name = "dt_demissao")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dt_demissao;
    
    @Column(name = "nr_ctps")
    private String nr_ctps;
    
    @JsonIgnore
    @OneToMany(mappedBy = "cd_vendedor",fetch= FetchType.LAZY)
    private List<Cliente> listaCliente;
       

    public Funcionario() {
    }

    public Date getDt_admissao() {
        return dt_admissao;
    }

    public void setDt_admissao(Date dt_admissao) {
        this.dt_admissao = dt_admissao;
    }

    public Date getDt_demissao() {
        return dt_demissao;
    }

    public void setDt_demissao(Date dt_demissao) {
        this.dt_demissao = dt_demissao;
    }

    public String getNr_ctps() {
        return nr_ctps;
    }

    public void setNr_ctps(String nr_ctps) {
        this.nr_ctps = nr_ctps;
    }

    public int getCd_funcionario() {
        return cd_funcionario;
    }

    public void setCd_funcionario(int cd_funcionario) {
        this.cd_funcionario = cd_funcionario;
    }

    public Pessoa getCd_pessoa() {
        return cd_pessoa;
    }

    public void setCd_pessoa(Pessoa cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public List<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    @Override
    public String toString() {
        return cd_pessoa.getNm_pessoa();
    }

    
    
}
