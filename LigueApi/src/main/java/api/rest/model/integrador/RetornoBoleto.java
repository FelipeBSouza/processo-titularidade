package api.rest.model.integrador;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class RetornoBoleto implements Serializable {

    private String TP_EMPRESA; //-01 CPF / 02 CNPJ
    private String NR_CNPJ; 
    private String NR_CARTEIRA; // NOSSA CARTEIRA
    private String NR_CARTEIRA2; // NOSSA CARTEIRA
    private String NR_AGENCIA; // NOSSA AGENCIA
    private String NR_AGENCIACOBRADORA; // CODIGO DA AGENCIA BANCO COBRADOR
    private String NR_CONTA; // NOSSA CONTA
    private String NR_NOSSO_NUMERO; // 
    private String NR_NOSSO_NUMERO_BANCO;
    private String OCORRENICA; // gravar ocorrencias 
    private Date DT_OCORRENCIA; // data transacao do banco
   // private Date DT_VENCIMENTO; // vencimento do boleto
    private Date DT_PAGAMENTO; // dia em que foi pago
    private double VL_BOLETO; // 
    private double VL_PAGO; // verificar com o valor do boleto
    private double VL_JUROS; 
    private double VL_DESCONTO; 
    private String NR_BANCO; //237 BRADESCO

    public RetornoBoleto() {
    }

    public Date getDT_OCORRENCIA() {
        return DT_OCORRENCIA;
    }

    public void setDT_OCORRENCIA(Date DT_OCORRENCIA) {
        this.DT_OCORRENCIA = DT_OCORRENCIA;
    }

    public Date getDT_PAGAMENTO() {
        return DT_PAGAMENTO;
    }

    public void setDT_PAGAMENTO(Date DT_PAGAMENTO) {
        this.DT_PAGAMENTO = DT_PAGAMENTO;
    }

    
    public String getNR_AGENCIA() {
        return NR_AGENCIA;
    }

    public void setNR_AGENCIA(String NR_AGENCIA) {
        this.NR_AGENCIA = NR_AGENCIA;
    }

    public String getNR_AGENCIACOBRADORA() {
        return NR_AGENCIACOBRADORA;
    }

    public void setNR_AGENCIACOBRADORA(String NR_AGENCIACOBRADORA) {
        this.NR_AGENCIACOBRADORA = NR_AGENCIACOBRADORA;
    }

    public String getNR_BANCO() {
        return NR_BANCO;
    }

    public void setNR_BANCO(String NR_BANCO) {
        this.NR_BANCO = NR_BANCO;
    }

    public String getNR_CARTEIRA() {
        return NR_CARTEIRA;
    }

    public void setNR_CARTEIRA(String NR_CARTEIRA) {
        this.NR_CARTEIRA = NR_CARTEIRA;
    }

    public String getNR_CARTEIRA2() {
        return NR_CARTEIRA2;
    }

    public void setNR_CARTEIRA2(String NR_CARTEIRA2) {
        this.NR_CARTEIRA2 = NR_CARTEIRA2;
    }

    public String getNR_CNPJ() {
        return NR_CNPJ;
    }

    public void setNR_CNPJ(String NR_CNPJ) {
        this.NR_CNPJ = NR_CNPJ;
    }

    public String getNR_CONTA() {
        return NR_CONTA;
    }

    public void setNR_CONTA(String NR_CONTA) {
        this.NR_CONTA = NR_CONTA;
    }

    public String getNR_NOSSO_NUMERO() {
        return NR_NOSSO_NUMERO;
    }

    public void setNR_NOSSO_NUMERO(String NR_NOSSO_NUMERO) {
        this.NR_NOSSO_NUMERO = NR_NOSSO_NUMERO;
    }

    public String getNR_NOSSO_NUMERO_BANCO() {
        return NR_NOSSO_NUMERO_BANCO;
    }

    public void setNR_NOSSO_NUMERO_BANCO(String NR_NOSSO_NUMERO_BANCO) {
        this.NR_NOSSO_NUMERO_BANCO = NR_NOSSO_NUMERO_BANCO;
    }

    public String getOCORRENICA() {
        return OCORRENICA;
    }

    public void setOCORRENICA(String OCORRENICA) {
        this.OCORRENICA = OCORRENICA;
    }

    public String getTP_EMPRESA() {
        return TP_EMPRESA;
    }

    public void setTP_EMPRESA(String TP_EMPRESA) {
        this.TP_EMPRESA = TP_EMPRESA;
    }

    public double getVL_BOLETO() {
        return VL_BOLETO;
    }

    public void setVL_BOLETO(double VL_BOLETO) {
        this.VL_BOLETO = VL_BOLETO;
    }

    public double getVL_DESCONTO() {
        return VL_DESCONTO;
    }

    public void setVL_DESCONTO(double VL_DESCONTO) {
        this.VL_DESCONTO = VL_DESCONTO;
    }

    public double getVL_JUROS() {
        return VL_JUROS;
    }

    public void setVL_JUROS(double VL_JUROS) {
        this.VL_JUROS = VL_JUROS;
    }

    public double getVL_PAGO() {
        return VL_PAGO;
    }

    public void setVL_PAGO(double VL_PAGO) {
        this.VL_PAGO = VL_PAGO;
    }
            
}
