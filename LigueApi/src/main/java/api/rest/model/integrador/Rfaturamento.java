
package api.rest.model.integrador;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Rfaturamento {
    
    private double vl_compra;
    private double vl_venda;
    private Cliente cliente;

    public Rfaturamento() {
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getVl_compra() {
        return vl_compra;
    }

    public void setVl_compra(double vl_compra) {
        this.vl_compra = vl_compra;
    }

    public double getVl_venda() {
        return vl_venda;
    }

    public void setVl_venda(double vl_venda) {
        this.vl_venda = vl_venda;
    }
    
    
}
