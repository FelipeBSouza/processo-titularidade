package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "integrador", name = "planos_correspondentes")
public class PlanoCorrespondente implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String cod_plano_original;
	private String qtd_pontos_adicionais;
	private String cod_plano_atualizado;	
	private String tp_plano;	
	private String ativo;	
	private String descricao_novo;	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCod_plano_original() {
		return cod_plano_original;
	}
	public void setCod_plano_original(String cod_plano_original) {
		this.cod_plano_original = cod_plano_original;
	}
	public String getQtd_pontos_adicionais() {
		return qtd_pontos_adicionais;
	}
	public void setQtd_pontos_adicionais(String qtd_pontos_adicionais) {
		this.qtd_pontos_adicionais = qtd_pontos_adicionais;
	}
	public String getCod_plano_atualizado() {
		return cod_plano_atualizado;
	}
	public void setCod_plano_atualizado(String cod_plano_atualizado) {
		this.cod_plano_atualizado = cod_plano_atualizado;
	}
	public String getTp_plano() {
		return tp_plano;
	}
	public void setTp_plano(String tp_plano) {
		this.tp_plano = tp_plano;
	}
	public String getAtivo() {
		return ativo;
	}
	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}
	public String getDescricao_novo() {
		return descricao_novo;
	}
	public void setDescricao_novo(String descricao_novo) {
		this.descricao_novo = descricao_novo;
	}
		
}
