package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "integrador", name = "integracao_autentique")
public class IntegracaoAutentique implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer cd_integracao_autentique;
	
	private String id_contrato;
	private String nr_solicitacao;
	private String ctr_ass;
	private Integer atividade;
	private String contrato_assinado;
	private String mov_sol;
	private String id_anexo;
	private String obs_rejeitado;
	private String processo;
	private String movement_status;
	private String reason_refuse_movement;
	
	public String getId_anexo() {
		return id_anexo;
	}
	public void setId_anexo(String id_anexo) {
		this.id_anexo = id_anexo;
	}
	public Integer getCd_integracao_autentique() {
		return cd_integracao_autentique;
	}
	public void setCd_integracao_autentique(Integer cd_integracao_autentique) {
		this.cd_integracao_autentique = cd_integracao_autentique;
	}
	public String getId_contrato() {
		return id_contrato;
	}
	public void setId_contrato(String id_contrato) {
		this.id_contrato = id_contrato;
	}
	public String getNr_solicitacao() {
		return nr_solicitacao;
	}
	public void setNr_solicitacao(String nr_solicitacao) {
		this.nr_solicitacao = nr_solicitacao;
	}
	public String getCtr_ass() {
		return ctr_ass;
	}
	public void setCtr_ass(String ctr_ass) {
		this.ctr_ass = ctr_ass;
	}
	public Integer getAtividade() {
		return atividade;
	}
	public void setAtividade(Integer atividade) {
		this.atividade = atividade;
	}
	public String getContrato_assinado() {
		return contrato_assinado;
	}
	public void setContrato_assinado(String contrato_assinado) {
		this.contrato_assinado = contrato_assinado;
	}
	public String getMov_sol() {
		return mov_sol;
	}
	public void setMov_sol(String mov_sol) {
		this.mov_sol = mov_sol;
	}
	public String getObs_rejeitado() {
		return obs_rejeitado;
	}
	public void setObs_rejeitado(String obs_rejeitado) {
		this.obs_rejeitado = obs_rejeitado;
	}
	public String getProcesso() {
		return processo;
	}
	public void setProcesso(String processo) {
		this.processo = processo;
	}
	public String getMovement_status() {
		return movement_status;
	}
	public void setMovement_status(String movement_status) {
		this.movement_status = movement_status;
	}
	public String getReason_refuse_movement() {
		return reason_refuse_movement;
	}
	public void setReason_refuse_movement(String reason_refuse_movement) {
		this.reason_refuse_movement = reason_refuse_movement;
	}	
}
