/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.fibra;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "fibra", name = "cad_cor")
public class Cor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_cor", nullable = false)
    private int cd_cor;
    @Column(name = "ds_cor")
    private String ds_cor;
    @Column(name = "bg_cor")
    private String bg_cor;

    public String getBg_cor() {
        return bg_cor;
    }

    public void setBg_cor(String bg_cor) {
        this.bg_cor = bg_cor;
    }

    public int getCd_cor() {
        return cd_cor;
    }

    public void setCd_cor(int cd_cor) {
        this.cd_cor = cd_cor;
    }

    public String getDs_cor() {
        return ds_cor;
    }

    public void setDs_cor(String ds_cor) {
        this.ds_cor = ds_cor;
    }

    @Override
    public String toString() {
        return ds_cor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cor other = (Cor) obj;

        if (this.cd_cor != other.cd_cor) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.cd_cor;
        return hash;
    }
}
