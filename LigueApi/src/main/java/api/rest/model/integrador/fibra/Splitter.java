/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.fibra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "Splitter")
public class Splitter implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_splitter", nullable = false)
    private int cd_splitter;
    @Column(name = "ds_splitter")
    private String ds_splitter;
    @Column(name = "posicao_mxk")
    private String posicao_mxk;
    @Column(name = "capacidade")
    private Integer capacidade;
    @Column(name = "obs")
    private String obs;
    //
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_caixa")
    private Caixa caixa;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_padrao")
    private PadraoCor padraoCor;
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_cor_tubo")
    private Cor corTubo;
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_cor_fibra")
    private Cor corFibra;
    //
    @JsonIgnore
    @OneToMany(mappedBy = "splitter", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @OrderBy(value = "nr_porta")
    private List<PortaSplitter> portaSplitters = new ArrayList<PortaSplitter>();
    //
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "cd_porta")
//    private PortaChassi portaChassi;

    public Splitter() { 
    }

    public Splitter(Caixa caixa, Integer capacidade) {
        this.capacidade = capacidade;
        this.caixa = caixa;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public Integer getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(Integer capacidade) {
        this.capacidade = capacidade;
    }

    public int getCd_splitter() {
        return cd_splitter;
    }

    public void setCd_splitter(int cd_splitter) {
        this.cd_splitter = cd_splitter;
    }

    public Cor getCorFibra() {
        return corFibra;
    }

    public void setCorFibra(Cor corFibra) {
        this.corFibra = corFibra;
    }

    public Cor getCorTubo() {
        return corTubo;
    }

    public void setCorTubo(Cor corTubo) {
        this.corTubo = corTubo;
    }

    public String getDs_splitter() {
        return ds_splitter;
    }

    public void setDs_splitter(String ds_splitter) {
        this.ds_splitter = ds_splitter;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public List<PortaSplitter> getPortaSplitters() {
        return portaSplitters;
    }

    public void setPortaSplitters(List<PortaSplitter> portaSplitters) {
        this.portaSplitters = portaSplitters;
    }

    public String getPosicao_mxk() {
        return posicao_mxk;
    }

    public void setPosicao_mxk(String posicao_mxk) {
        this.posicao_mxk = posicao_mxk;
    }

    public PadraoCor getPadraoCor() {
        return padraoCor;
    }

    public void setPadraoCor(PadraoCor padraoCor) {
        this.padraoCor = padraoCor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Splitter other = (Splitter) obj;
        if (this.cd_splitter != other.cd_splitter) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.cd_splitter;
        return hash;
    }

    @Override
    public String toString() {
        return ds_splitter;
    }
}
