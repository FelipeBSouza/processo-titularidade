package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "integrador", name = "iclass_atendimento")
public class IclassAtendimentos implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_atendimentos;
	
	private String numos;
	private String dtchegada;
	private String hrchegada;
	private String dtsaida;
	private String hrsaida;
	private String dtinicial;
	private String hrinicial;
	private String dtfinal;
	private String hrfinal;
	private String ocorr;
	private String laudo;
	private String acumul;
	private String status;
	private String trasla;
	private String hrsfats;
	private String codocor;
	
	
	public Integer getId_atendimentos() {
		return id_atendimentos;
	}
	public void setId_atendimentos(Integer id_atendimentos) {
		this.id_atendimentos = id_atendimentos;
	}
	public String getNumos() {
		return numos;
	}
	public void setNumos(String numos) {
		this.numos = numos;
	}
	public String getDtchegada() {
		return dtchegada;
	}
	public void setDtchegada(String dtchegada) {
		this.dtchegada = dtchegada;
	}
	public String getHrchegada() {
		return hrchegada;
	}
	public void setHrchegada(String hrchegada) {
		this.hrchegada = hrchegada;
	}
	public String getDtsaida() {
		return dtsaida;
	}
	public void setDtsaida(String dtsaida) {
		this.dtsaida = dtsaida;
	}
	public String getHrsaida() {
		return hrsaida;
	}
	public void setHrsaida(String hrsaida) {
		this.hrsaida = hrsaida;
	}
	public String getDtinicial() {
		return dtinicial;
	}
	public void setDtinicial(String dtinicial) {
		this.dtinicial = dtinicial;
	}
	public String getHrinicial() {
		return hrinicial;
	}
	public void setHrinicial(String hrinicial) {
		this.hrinicial = hrinicial;
	}
	public String getDtfinal() {
		return dtfinal;
	}
	public void setDtfinal(String dtfinal) {
		this.dtfinal = dtfinal;
	}
	public String getHrfinal() {
		return hrfinal;
	}
	public void setHrfinal(String hrfinal) {
		this.hrfinal = hrfinal;
	}
	public String getOcorr() {
		return ocorr;
	}
	public void setOcorr(String ocorr) {
		this.ocorr = ocorr;
	}
	public String getLaudo() {
		return laudo;
	}
	public void setLaudo(String laudo) {
		this.laudo = laudo;
	}
	public String getAcumul() {
		return acumul;
	}
	public void setAcumul(String acumul) {
		this.acumul = acumul;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTrasla() {
		return trasla;
	}
	public void setTrasla(String trasla) {
		this.trasla = trasla;
	}
	public String getHrsfats() {
		return hrsfats;
	}
	public void setHrsfats(String hrsfats) {
		this.hrsfats = hrsfats;
	}
	public String getCodocor() {
		return codocor;
	}
	public void setCodocor(String codocor) {
		this.codocor = codocor;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
