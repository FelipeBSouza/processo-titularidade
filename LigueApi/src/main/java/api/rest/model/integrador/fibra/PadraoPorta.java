/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.integrador.fibra;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "integrador", name = "padrao_porta")
public class PadraoPorta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_padrao_porta", nullable = false)
    private int cd_padrao_porta;
    @Column(name = "porta")
    private Integer porta;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_padrao")
    private PadraoCor padraoCor;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_cor_tubo")
    private Cor corTubo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_cor_fibra")
    private Cor corFibra;

    public PadraoPorta() {
    }

    public int getCd_padrao_porta() {
        return cd_padrao_porta;
    }

    public void setCd_padrao_porta(int cd_padrao_porta) {
        this.cd_padrao_porta = cd_padrao_porta;
    }

    public Cor getCorFibra() {
        return corFibra;
    }

    public void setCorFibra(Cor corFibra) {
        this.corFibra = corFibra;
    }

    public Cor getCorTubo() {
        return corTubo;
    }

    public void setCorTubo(Cor corTubo) {
        this.corTubo = corTubo;
    }

    public PadraoCor getPadraoCor() {
        return padraoCor;
    }

    public void setPadraoCor(PadraoCor padraoCor) {
        this.padraoCor = padraoCor;
    }

    public Integer getPorta() {
        return porta;
    }

    public void setPorta(Integer porta) {
        this.porta = porta;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PadraoPorta other = (PadraoPorta) obj;
        if (this.cd_padrao_porta != other.cd_padrao_porta) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.cd_padrao_porta;
        return hash;
    }
}