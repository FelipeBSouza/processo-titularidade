package api.rest.model.integrador;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement
@Entity
@Table(schema = "integrador", name = "telefone")
public class Telefone implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "cd_telefone", nullable = false)
    private int cd_telefone;
    @Column(name = "nr_telefone")
    private String nr_telefone;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cd_finalidade")
    private TipoFinalidade cd_finalidade;
    @ManyToOne
    @JoinColumn(name = "cd_tipo_telefone")
    private TipoTelefone cd_tipo_telefone;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_pessoa")
    private Pessoa cd_pessoa;

    public Telefone() {
    }

    public Telefone(Pessoa cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public TipoFinalidade getCd_finalidade() {
        return cd_finalidade;
    }

    public void setCd_finalidade(TipoFinalidade cd_finalidade) {
        this.cd_finalidade = cd_finalidade;
    }

    public Pessoa getCd_pessoa() {
        return cd_pessoa;
    }

    public void setCd_pessoa(Pessoa cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public int getCd_telefone() {
        return cd_telefone;
    }

    public void setCd_telefone(int cd_telefone) {
        this.cd_telefone = cd_telefone;
    }

    public TipoTelefone getCd_tipo_telefone() {
        return cd_tipo_telefone;
    }

    public void setCd_tipo_telefone(TipoTelefone cd_tipo_telefone) {
        this.cd_tipo_telefone = cd_tipo_telefone;
    }

    public String getNr_telefone() {
        return nr_telefone;
    }

    public void setNr_telefone(String nr_telefone) {
        this.nr_telefone = nr_telefone;
    }
}
