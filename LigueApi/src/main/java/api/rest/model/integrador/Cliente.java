package api.rest.model.integrador;

import api.rest.model.integrador.equipamento.EquipamentoYate;
import api.rest.model.telefonia.Numero;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement
@Entity
@Table(schema = "integrador", name = "cliente")
public class Cliente implements Serializable {

    private Integer cd_cliente;
    private Pessoa pessoa;
    private Funcionario cd_vendedor;
    private int dia_vencimento = 10;
    private List<Internet> listaInternet = new ArrayList<>();
    private List<Numero> numeros = new ArrayList<>();
    private List<Fatura_cliente> fatura_clientes;
    private List<ContasReceber> contasRecebers;
    private List<AdicionalPlano> adicionalPlanos;
    private List<EquipamentoYate> equipamentos = new ArrayList<>();
    private String cd_totvs;
    private double vlLimite = 200;
    
    
    public Cliente() {
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_vendedor")
    public Funcionario getCd_vendedor() {
        return cd_vendedor;
    }

    public void setCd_vendedor(Funcionario cd_vendedor) {
        this.cd_vendedor = cd_vendedor;
    }

    @Column(name = "dia_vencimento")
    public int getDia_vencimento() {
        return dia_vencimento;
    }

    public void setDia_vencimento(int dia_vencimento) {
        this.dia_vencimento = dia_vencimento;
    }

    @Id
    @Column(name = "cd_cliente", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getCd_cliente() {
        return cd_cliente;
    }

    public void setCd_cliente(Integer cd_cliente) {
        this.cd_cliente = cd_cliente;
    }
    @JsonIgnore
    @JoinColumn(name = "cd_pessoa")
    @OneToOne(cascade = CascadeType.ALL)
    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @OneToMany(mappedBy = "cd_cliente", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Internet> getListaInternet() {
        return listaInternet;
    }

    public void setListaInternet(List<Internet> listaInternet) {
        this.listaInternet = listaInternet;
    }
    @JsonIgnore
    @OneToMany(mappedBy = "cd_cliente", fetch = FetchType.LAZY)
    public List<Numero> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Numero> numeros) {
        this.numeros = numeros;
    }

    
    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<AdicionalPlano> getAdicionalPlanos() {
        return adicionalPlanos;
    }

    public void setAdicionalPlanos(List<AdicionalPlano> adicionalPlanos) {
        this.adicionalPlanos = adicionalPlanos;
    }

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<AdicionalPlano> getAdicionalPlanosADD() {
        List<AdicionalPlano> adicionais = new ArrayList<AdicionalPlano>();
        for (AdicionalPlano adicional : adicionalPlanos) {
            if (adicional.getAdicional().getIn_add_sub() == 'A') {
                adicionais.add(adicional);
            }
        }
        return adicionais;
    }
    
    public void setAdicionalPlanosADD(List<AdicionalPlano> adicionalPlanos) {
        this.adicionalPlanos = adicionalPlanos;
    }
    

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(schema = "integrador", name = "equipamento_cliente",
    joinColumns =
    @JoinColumn(name = "cd_cliente", referencedColumnName = "cd_cliente"),
    inverseJoinColumns =
    @JoinColumn(name = "cd_equipamento", referencedColumnName = "cd_equipamento"))
    public List<EquipamentoYate> getEquipamentos() {
        return equipamentos;
    }

    public void setEquipamentos(List<EquipamentoYate> equipamentos) {
        this.equipamentos = equipamentos;
    }

    @Column(name = "cd_totvs", length = 10)
    public String getCd_totvs() {
        return cd_totvs;
    }

    public void setCd_totvs(String cd_totvs) {
        this.cd_totvs = cd_totvs;
    }

    @Column(name = "vl_limite")
    public double getVlLimite() {
        return vlLimite;
    }

    public void setVlLimite(double vlLimite) {
        this.vlLimite = vlLimite;
    }

//
//    @Transient
//    public boolean isEnabled() {
//        return true;
//    }
//
//    @Transient
//    public boolean isAccountNonExpired() {
//        return true;
//    }
//
//    @Transient
//    public boolean isAccountNonLocked() {
//        return true;
//    }
//
//    @Transient
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Transient
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();
//        return result;
//    }
//
//    public String getPassword() {
//        return pessoa.getDs_senha();
//    }
//
//    public String getUsername() {
//        return pessoa.getDs_login();
//    }

    /*
     * 
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.cd_cliente != null ? this.cd_cliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (this.cd_cliente != other.cd_cliente && (this.cd_cliente == null || !this.cd_cliente.equals(other.cd_cliente))) {
            return false;
        }
        return true;
    }

    @OneToMany(mappedBy = "cd_cliente", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @OrderBy(value = "dt_faturamento")
    public List<Fatura_cliente> getFatura_clientes() {
        return fatura_clientes;
    }

    public void setFatura_clientes(List<Fatura_cliente> fatura_clientes) {
        this.fatura_clientes = fatura_clientes;
    }

    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
    public List<ContasReceber> getContasRecebers() {
        return contasRecebers;
    }

    public void setContasRecebers(List<ContasReceber> contasRecebers) {
        this.contasRecebers = contasRecebers;
    }

    @OneToMany(mappedBy = "cd_cliente", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @OrderBy(value = "dt_faturamento")
    public List<Fatura_cliente> getFatura_clientesAbertas() {
        List<Fatura_cliente> faturasAbertas = new ArrayList<Fatura_cliente>();
        for (Fatura_cliente fatura_cliente : fatura_clientes) {
            if (fatura_cliente.getContasReceber().getIN_SITUACAO() == 'A') {
                faturasAbertas.add(fatura_cliente);
            }
        }
        return faturasAbertas;
    }
    
    public void setFatura_clientesAbertas(List<Fatura_cliente> fatura_clientes) {
        this.fatura_clientes = fatura_clientes;
    }

    @OneToMany(mappedBy = "cd_cliente", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @OrderBy(value = "dt_faturamento")
    public List<Fatura_cliente> getFaturasVencidas() {
        List<Fatura_cliente> faturasVencidas = new ArrayList<Fatura_cliente>();
        try {
            for (Fatura_cliente fatura_cliente : getFatura_clientesAbertas()) {
                if (fatura_cliente.getContasReceber().getDT_RECEBER().before(new Date())) {
                    faturasVencidas.add(fatura_cliente);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return faturasVencidas;
    }
    
    public void setFaturasVencidas(List<Fatura_cliente> fatura_clientes) {
        this.fatura_clientes = fatura_clientes;
    }

    @OneToMany(mappedBy = "cd_cliente", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @OrderBy(value = "dt_faturamento")
    public List<Fatura_cliente> getFatura_clientesPagas() {
        List<Fatura_cliente> faturasPagas = new ArrayList<Fatura_cliente>();
        for (Fatura_cliente fatura_cliente : fatura_clientes) {
            if (fatura_cliente.getContasReceber().getIN_SITUACAO() == 'P') {
                faturasPagas.add(fatura_cliente);
            }
        }
        return faturasPagas;
    }
    
    public void setFatura_clientesPagas(List<Fatura_cliente> fatura_clientes) {
        this.fatura_clientes = fatura_clientes;
    }

    public String codCliente() {
        String s = "";

        try {
            if (this.pessoa.getPessoaFisica() != null) {
                s = this.pessoa.getPessoaFisica().getNr_cpf().replaceAll("[./]", "").trim().substring(0, 9) + "001";
            } else {
                s = this.pessoa.getPessoaJuridica().getNr_cnpj().replaceAll("[./-]", "").trim().substring(0, 12);
            }
            return s;
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public String toString() {
        return "Cliente{" + "cd_cliente=" + cd_cliente + ", pessoa=" + pessoa + ", cd_vendedor=" + cd_vendedor + ", dia_vencimento=" + dia_vencimento + ", listaInternet=" + listaInternet + ", numeros=" + numeros + ", fatura_clientes=" + fatura_clientes + ", contasRecebers=" + contasRecebers + ", adicionalPlanos=" + adicionalPlanos + '}';
    }
}
