package api.rest.model.integrador;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
@XmlRootElement
@Entity
@Table(schema = "integrador",name = "endereco")
public class Endereco implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)  
    @Column(name = "cd_endereco")
    private int cd_endereco;
    
    @Column(name = "ds_logradouro")
    private String ds_logradouro;
    
    @Column(name = "ds_bairro")
    private String ds_bairro;
        
    @Column(name = "nr_cep")
    private String nr_cep;
    
    @Column(name = "nr_numero")
    private String nr_numero;
    
    @Column(name = "ds_complemento")
    private String ds_complemento;
    
    @Column(name = "in_preferencial")
    private char in_preferencial = 'S';
    
    @Column(name = "in_ativo")
    private char in_ativo = 'A';
     
    @Column(name = "ds_cidade")
    private String ds_cidade;
    
    @Column(name = "ds_uf")
    private String ds_uf;
               
    @ManyToOne
    @JoinColumn(name="cd_finalidade")
    private TipoFinalidade cd_finalidade;
    
    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cd_pessoa")
    private Pessoa cd_pessoa;
    
    public Endereco() {
    }

    public Endereco(Pessoa cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }
    
    public int getCd_endereco() {
        return cd_endereco;
    }

    public void setCd_endereco(int cd_endereco) {
        this.cd_endereco = cd_endereco;
    }

    public TipoFinalidade getCd_finalidade() {
        return cd_finalidade;
    }

    public void setCd_finalidade(TipoFinalidade cd_finalidade) {
        this.cd_finalidade = cd_finalidade;
    }

    public Pessoa getCd_pessoa() {
        return cd_pessoa;
    }

    public void setCd_pessoa(Pessoa cd_pessoa) {
        this.cd_pessoa = cd_pessoa;
    }

    public String getDs_bairro() {
        return ds_bairro;
    }

    public void setDs_bairro(String ds_bairro) {
        this.ds_bairro = ds_bairro;
    }

    public String getDs_cidade() {
        return ds_cidade;
    }

    public void setDs_cidade(String ds_cidade) {
        this.ds_cidade = ds_cidade;
    }

    public String getDs_complemento() {
        return ds_complemento;
    }

    public void setDs_complemento(String ds_complemento) {
        this.ds_complemento = ds_complemento;
    }

    public String getDs_logradouro() {
        return ds_logradouro;
    }

    public void setDs_logradouro(String ds_logradouro) {
        this.ds_logradouro = ds_logradouro;
    }

    public String getDs_uf() {
        return ds_uf;
    }

    public void setDs_uf(String ds_uf) {
        this.ds_uf = ds_uf;
    }

    public char getIn_ativo() {
        return in_ativo;
    }

    public void setIn_ativo(char in_ativo) {
        this.in_ativo = in_ativo;
    }

    public char getIn_preferencial() {
        return in_preferencial;
    }

    public void setIn_preferencial(char in_preferencial) {
        this.in_preferencial = in_preferencial;
    }

    public String getNr_cep() {
        return nr_cep;
    }

    public void setNr_cep(String nr_cep) {
        this.nr_cep = nr_cep;
    }

    public String getNr_numero() {
        return nr_numero;
    }

    public void setNr_numero(String nr_numero) {
        this.nr_numero = nr_numero;
    }
 
    

    @Override
    public String toString(){
        return this.getDs_logradouro()+", "+this.getNr_numero()+", "+this.getDs_bairro()+" - "+this.getNr_cep();
    }
   

   
    
    
    
}
