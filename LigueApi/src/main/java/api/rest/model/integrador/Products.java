package api.rest.model.integrador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "integrador", name = "products")
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String cod_totvs;
    private String referencia;
    private String descricao;
    private String valor;
    private String desc_single;
    private String desc_dual;
    private String desc_triple;
    private String qtd_mes_ini;
    private String qtd_mes_cob;
    private String referencia_obg;
    private String referencia_sva;
    private String tp_produto;
    private String id_uf;
    private String id_cidade;
    private String id_bairro;
    private String dt_ini_promo;
    private String dt_fim_promo;
    private String visivel;
    private String categoria;
    private String repetivel;
    private String download;
    private String upload;
    private String id_promocao;
    private String fidelidade;
    private String equivalencia;    
    
    public String getEquivalencia() {
		return equivalencia;
	}

	public void setEquivalencia(String equivalencia) {
		this.equivalencia = equivalencia;
	}

	public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCod_totvs() {
        return this.cod_totvs;
    }

    public void setCod_totvs(String cod_totvs) {
        this.cod_totvs = cod_totvs;
    }

    public String getReferencia() {
        return this.referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDesc_single() {
        return this.desc_single;
    }

    public void setDesc_single(String desc_single) {
        this.desc_single = desc_single;
    }

    public String getDesc_dual() {
        return this.desc_dual;
    }

    public void setDesc_dual(String desc_dual) {
        this.desc_dual = desc_dual;
    }

    public String getDesc_triple() {
        return this.desc_triple;
    }

    public void setDesc_triple(String desc_triple) {
        this.desc_triple = desc_triple;
    }

    public String getQtd_mes_ini() {
        return this.qtd_mes_ini;
    }

    public void setQtd_mes_ini(String qtd_mes_ini) {
        this.qtd_mes_ini = qtd_mes_ini;
    }

    public String getQtd_mes_cob() {
        return this.qtd_mes_cob;
    }

    public void setQtd_mes_cob(String qtd_mes_cob) {
        this.qtd_mes_cob = qtd_mes_cob;
    }

    public String getReferencia_obg() {
        return this.referencia_obg;
    }

    public void setReferencia_obg(String referencia_obg) {
        this.referencia_obg = referencia_obg;
    }

    public String getReferencia_sva() {
        return this.referencia_sva;
    }

    public void setReferencia_sva(String referencia_sva) {
        this.referencia_sva = referencia_sva;
    }

    public String getTp_produto() {
        return this.tp_produto;
    }

    public void setTp_produto(String tp_produto) {
        this.tp_produto = tp_produto;
    }

    public String getId_uf() {
        return this.id_uf;
    }

    public void setId_uf(String id_uf) {
        this.id_uf = id_uf;
    }

    public String getId_cidade() {
        return this.id_cidade;
    }

    public void setId_cidade(String id_cidade) {
        this.id_cidade = id_cidade;
    }

    public String getId_bairro() {
        return this.id_bairro;
    }

    public void setId_bairro(String id_bairro) {
        this.id_bairro = id_bairro;
    }

    public String getDt_ini_promo() {
        return this.dt_ini_promo;
    }

    public void setDt_ini_promo(String dt_ini_promo) {
        this.dt_ini_promo = dt_ini_promo;
    }

    public String getDt_fim_promo() {
        return this.dt_fim_promo;
    }

    public void setDt_fim_promo(String dt_fim_promo) {
        this.dt_fim_promo = dt_fim_promo;
    }

    public String getVisivel() {
        return this.visivel;
    }

    public void setVisivel(String visivel) {
        this.visivel = visivel;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getRepetivel() {
        return this.repetivel;
    }

    public void setRepetivel(String repetivel) {
        this.repetivel = repetivel;
    }

    public String getDownload() {
        return this.download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getUpload() {
        return this.upload;
    }

    public void setUpload(String upload) {
        this.upload = upload;
    }

    public String getId_promocao() {
        return this.id_promocao;
    }

    public void setId_promocao(String id_promocao) {
        this.id_promocao = id_promocao;
    }

    public String getFidelidade() {
        return this.fidelidade;
    }

    public void setFidelidade(String fidelidade) {
        this.fidelidade = fidelidade;
    }

}
