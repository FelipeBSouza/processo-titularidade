/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.model.auditoria;

import api.rest.model.integrador.Cliente;
import api.rest.model.seguranca.Usuario;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author ROBSON
 */
@Entity
@Table(schema = "auditoria", name = "auditoria_bloqueio")
public class AuditaBloqueio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @ManyToOne
    @JoinColumn(name = "cd_usuario")
    private Usuario usuario;
    @JoinColumn(name = "cd_cliente")
    @ManyToOne
    private Cliente cliente;
    @Column(name = "acao", length = 1)
    private String acao;
    @Column(name = "tipo", length = 1)
    private String tipo;
    @Column(name = "data_atualizacao")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAtualizacao = new Date();

    public AuditaBloqueio() {
    }

    public AuditaBloqueio(Usuario usuario, Cliente cliente, String tipo, String acao) {
        this.usuario = usuario;
        this.cliente = cliente;
        this.acao = acao;
        this.tipo = tipo;
    }

    public String getAcao() {
        return acao;
    }

    public String getAcaoFormat() {
        switch (acao) {
            case "B":
                return "Bloqueio";
            case "D":
                return "Desbloqueio";
            default:
                throw new IllegalArgumentException("Unknown" + acao);
        }
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoFormat() {
        switch (tipo) {
            case "F":
                return "Telefone";
            case "N":
                return "Internet";
            default:
                throw new IllegalArgumentException("Unknown" + tipo);
        }
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuditaBloqueio other = (AuditaBloqueio) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + this.id;
        return hash;
    }
}
