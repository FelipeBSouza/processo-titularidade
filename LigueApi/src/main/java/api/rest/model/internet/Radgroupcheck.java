package api.rest.model.internet;
// Generated 11/12/2012 10:35:37 by Hibernate Tools 3.2.1.GA

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(schema="internet")
public class Radgroupcheck  implements java.io.Serializable {

     @Id
     private int id;
     private String groupname;
     private String attribute;
     private String op;
     private String value;

    public Radgroupcheck() {
    }

    public Radgroupcheck(int id, String groupname, String attribute, String op, String value) {
       this.id = id;
       this.groupname = groupname;
       this.attribute = attribute;
       this.op = op;
       this.value = value;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getGroupname() {
        return this.groupname;
    }
    
    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
    public String getAttribute() {
        return this.attribute;
    }
    
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }
    public String getOp() {
        return this.op;
    }
    
    public void setOp(String op) {
        this.op = op;
    }
    public String getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }




}


