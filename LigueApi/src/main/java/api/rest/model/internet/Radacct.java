package api.rest.model.internet;
// Generated 11/12/2012 10:35:37 by Hibernate Tools 3.2.1.GA

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(schema = "internet", name = "radacct")
public class Radacct implements java.io.Serializable {

    @Id
    private long radacctid;
    private String acctsessionid;
    private String acctuniqueid;
    private String username;
    private String groupname;
    private String realm;
    private String nasipaddress;
    private String nasportid;
    private String nasporttype;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date acctstarttime;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date acctstoptime;
    private Long acctsessiontime;
    private String acctauthentic;
    private String connectinfo_start;
    private String connectinfo_stop;
    private Long acctinputoctets;
    private Long acctoutputoctets;
    private String calledstationid;
    private String callingstationid;
    private String acctterminatecause;
    private String servicetype;
    private String xascendsessionsvrkey;
    private String framedprotocol;
    private String framedipaddress;
    private Integer acctstartdelay;
    private Integer acctstopdelay;

    public Radacct() {
    }

    public long getRadacctid() {
        return this.radacctid;
    }

    public void setRadacctid(long radacctid) {
        this.radacctid = radacctid;
    }

    public String getAcctsessionid() {
        return this.acctsessionid;
    }

    public void setAcctsessionid(String acctsessionid) {
        this.acctsessionid = acctsessionid;
    }

    public String getAcctuniqueid() {
        return this.acctuniqueid;
    }

    public void setAcctuniqueid(String acctuniqueid) {
        this.acctuniqueid = acctuniqueid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGroupname() {
        return this.groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getRealm() {
        return this.realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getNasipaddress() {
        return nasipaddress;
    }

    public void setNasipaddress(String nasipaddress) {
        this.nasipaddress = nasipaddress;
    }

    public String getFramedipaddress() {
        return framedipaddress;
    }

    public void setFramedipaddress(String framedipaddress) {
        this.framedipaddress = framedipaddress;
    }

    public String getNasportid() {
        return this.nasportid;
    }

    public void setNasportid(String nasportid) {
        this.nasportid = nasportid;
    }

    public String getNasporttype() {
        return this.nasporttype;
    }

    public void setNasporttype(String nasporttype) {
        this.nasporttype = nasporttype;
    }

    public Date getAcctstarttime() {
        return this.acctstarttime;
    }

    public void setAcctstarttime(Date acctstarttime) {
        this.acctstarttime = acctstarttime;
    }

    public Date getAcctstoptime() {
        return this.acctstoptime;
    }

    public void setAcctstoptime(Date acctstoptime) {
        this.acctstoptime = acctstoptime;
    }

    public Long getAcctsessiontime() {
        return this.acctsessiontime;
    }

    public void setAcctsessiontime(Long acctsessiontime) {
        this.acctsessiontime = acctsessiontime;
    }

    public String getAcctauthentic() {
        return this.acctauthentic;
    }

    public void setAcctauthentic(String acctauthentic) {
        this.acctauthentic = acctauthentic;
    }

    public String getConnectinfo_start() {
        return connectinfo_start;
    }

    public void setConnectinfo_start(String connectinfo_start) {
        this.connectinfo_start = connectinfo_start;
    }

    public String getConnectinfo_stop() {
        return connectinfo_stop;
    }

    public void setConnectinfo_stop(String connectinfo_stop) {
        this.connectinfo_stop = connectinfo_stop;
    }

    public Long getAcctinputoctets() {
        return this.acctinputoctets;
    }

    public String getAcctinputoctetsConverter() {
        if (acctinputoctets != null) {
            long MEGABYTE = 1024L * 1024L;
            if (this.acctinputoctets > MEGABYTE) {
                long b = this.acctinputoctets / MEGABYTE;
                return b + " MB";
            } else {
                return this.acctinputoctets + " B";
            }
        } else {
            return "";
        }
    }

    public void setAcctinputoctets(Long acctinputoctets) {
        this.acctinputoctets = acctinputoctets;
    }

    public Long getAcctoutputoctets() {
        return this.acctoutputoctets;
    }

    public String getAcctoutputoctetsConverter() {
        if (this.acctoutputoctets != null) {
            long MEGABYTE = 1024L * 1024L;
            if (this.acctoutputoctets > MEGABYTE) {
                long b = this.acctoutputoctets / MEGABYTE;
                return b + " MB";
            } else {
                return this.acctoutputoctets + " B";
            }
        } else {
            return "";
        }
    }

    public void setAcctoutputoctets(Long acctoutputoctets) {
        this.acctoutputoctets = acctoutputoctets;
    }

    public String getCalledstationid() {
        return this.calledstationid;
    }

    public void setCalledstationid(String calledstationid) {
        this.calledstationid = calledstationid;
    }

    public String getCallingstationid() {
        return this.callingstationid;
    }

    public void setCallingstationid(String callingstationid) {
        this.callingstationid = callingstationid;
    }

    public String getAcctterminatecause() {
        return this.acctterminatecause;
    }

    public void setAcctterminatecause(String acctterminatecause) {
        this.acctterminatecause = acctterminatecause;
    }

    public String getServicetype() {
        return this.servicetype;
    }

    public void setServicetype(String servicetype) {
        this.servicetype = servicetype;
    }

    public String getXascendsessionsvrkey() {
        return this.xascendsessionsvrkey;
    }

    public void setXascendsessionsvrkey(String xascendsessionsvrkey) {
        this.xascendsessionsvrkey = xascendsessionsvrkey;
    }

    public String getFramedprotocol() {
        return this.framedprotocol;
    }

    public void setFramedprotocol(String framedprotocol) {
        this.framedprotocol = framedprotocol;
    }

    public Integer getAcctstartdelay() {
        return this.acctstartdelay;
    }

    public void setAcctstartdelay(Integer acctstartdelay) {
        this.acctstartdelay = acctstartdelay;
    }

    public Integer getAcctstopdelay() {
        return this.acctstopdelay;
    }

    public void setAcctstopdelay(Integer acctstopdelay) {
        this.acctstopdelay = acctstopdelay;
    }
}
