package api.rest.model.telefonia;
// Generated 11/12/2012 10:01:38 by Hibernate Tools 3.2.1.GA

import javax.xml.bind.annotation.XmlRootElement;




/**
 * Rotaslocais generated by hbm2java
 */
@XmlRootElement
public class Rotaslocais  implements java.io.Serializable {


     private long id;
     private Interconexoes interconexoes;
     private String spid;

    public Rotaslocais() {
    }

    public Rotaslocais(long id, Interconexoes interconexoes, String spid) {
       this.id = id;
       this.interconexoes = interconexoes;
       this.spid = spid;
    }
   
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public Interconexoes getInterconexoes() {
        return this.interconexoes;
    }
    
    public void setInterconexoes(Interconexoes interconexoes) {
        this.interconexoes = interconexoes;
    }
    public String getSpid() {
        return this.spid;
    }
    
    public void setSpid(String spid) {
        this.spid = spid;
    }




}


